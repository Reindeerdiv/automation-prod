﻿======================================================================
Product Name	 : Qualitia Automation Studio
Release Version	 : 8.0.0
======================================================================
Introduction

Qualitia is a Test automation engine that creates, executes, and maintains scriptless automated tests on multiple test automation tools and technologies. It acts as an interface between test automation tools and testers, accelerating your test automation multi-fold. Qualitia provides a comprehensive solution to your automation testing requirements.

======================================================================
Product Release Notes
For more information on whats new in this release, please refer to the link:https://qualitia.atlassian.net/wiki/spaces/QAS80/pages/1718943897/Qualitia+Automation+Studio+8.0.0

======================================================================
Installation Guide
For more information about the installation or upgrade process, please refer to the link:https://qualitia.atlassian.net/wiki/spaces/QAS80/pages/1718944315/Installation+and+Upgrades

======================================================================
Product Documentation
For the detailed information on how to use Qualitia, please refer to the link: https://qualitia.atlassian.net/wiki/spaces/QAS80/overview

======================================================================
Known Issues:
For more information about the known issues, please refer to the link:
https://qualitia.atlassian.net/wiki/spaces/QAS80/pages/1718943897/Qualitia+Automation+Studio+8.0.0#Known-Issues

======================================================================
Copyright © 2020, Qualitia Software Pvt. Ltd. All rights reserved.
Qualitia Software Pvt. Ltd.