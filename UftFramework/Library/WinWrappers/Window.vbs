
'========================Code Section Begin=====================================================================
Option Explicit	'Forces explicit declaration of all variables in a script.
'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :			ActivateWindow()
'	Purpose :				Activates the Specified Window if it exist and is enabled.	
'	Return Value :		 	Integer(0/1)
'	Arguments :
'		Input Arguments :   objWindow    
'		Output Arguments :  Status
'	Function is called by : 
'	Function calls :		CheckObjectExist(),CheckObjectStatus()
'	Created on : 			28/02/2009
'	Author :				Ashok
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function ActivateWindow(ByVal objWindow)
On Error Resume Next
		'Variable Section Begin
			Dim strMethod
			Dim intActive , strEnabled

		'Variable Section End
		strMethod = strKeywordForReporting
		If CheckObjectExist(objWindow) Then
			  
				strEnabled = CheckObjectStatus(objWindow)
			  
				If strEnabled = true Then
						   objWindow.Activate  
						   If err.Number = 0 Then
								ActivateWindow  = 0
								WriteStatusToLogs "The specified Window is activated successfully."
						   Else
								ActivateWindow = 1
								WriteStatusToLogs "Window is not activated, please verify."
						   End If
				else
						   ActivateWindow = 1 
						   WriteStatusToLogs "Either an error occurred or the specified window is not enabled, hence it could not be activated."
				End If
					  
	  	Else
			  ActivateWindow = 1 
			  WriteStatusToLogs "The specified Window does not exist, please verify."
		End If
On Error GoTo 0 
End Function

'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :			CloseWindow()
'	Purpose :				Closes the specified Window.
'	Return Value :			Integer( 0/1)
'	Arguments :
'		Input Arguments :	objWindow
'		Output Arguments : 
'	Function is called by :
'	Function calls :		CheckObjectExist()
'	Created on :			03/03/2008
'	Author :				Rathna Reddy
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function CloseWindow(ByVal objWindow)
	On Error Resume Next
		'Variable Section Begin

			Dim strMethod

		'Variable Section End
			strMethod = strKeywordForReporting
			If  CheckObjectExist(objWindow) Then 
				objWindow.Close 
				If err.Number = 0 Then 
					CloseWindow = 0 
					WriteStatusToLogs "Window was Closed successfully."
				Else
					CloseWindow = 1 
					WriteStatusToLogs "Window was not Closed successfully, please verify."
					

				End If
			Else
				CloseWindow = 1 
				WriteStatusToLogs "Window does not exist, please verify."
					
		End If
	On Error GoTo 0
End Function 



'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :				 CompareWindowTitle()
'	Purpose :					 Compares the Window title with our expected title.
'	Return Value :		 		 Integer( 0/1)
'	Arguments :
'		Input Arguments :  		objWindow , strExpTitle
'		Output Arguments :      strActTitle
'	Function is called by :
'	Function calls :			 CheckObjectExist() , CompareStringValues()
'	Created on : 				 03/03/2008
'	Author :					 Rathna Reddy
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function  CompareWindowTitle(ByVal objWindow , ByVal strExpTitle)
	On Error Resume Next 
		'Variable Section Begin
			Dim strMethod
			Dim strActTitle
		'Variable Section End
			strMethod = strKeywordForReporting
			 If CheckObjectExist(objWindow) Then
					 strActTitle = objWindow.GetRoProperty("title")
                  If CompareStringValues(StrExpTitle, strActTitle)=0  Then 
						CompareWindowTitle = 0
						WriteStatusToLogs """"&"Expected Title : "&strExpTitle& """" &  " and " &""""&"Actual Title : "&strActTitle&""""&" matches as expected."

			     Else
						CompareWindowTitle = 2
						WriteStatusToLogs """"&"Expected Title : "&strExpTitle& """" &  " and " &""""&"Actual Title : "&strActTitle&""""&" does not match, please verify."
										 
				 End If
			Else
				 CompareWindowTitle = 1
				 WriteStatusToLogs "Window does not exist, please verify."
			End If
	On Error GoTo 0 
End Function

''------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :			MaximizeWindow()
'	Purpose :				Maximizes the specified Window if it exist ,isEnabled and is Maximisable.
'	Return Value :			Integer(0/1)
'	Arguments :
'		Input Arguments :	objWindow      
'		Output Arguments :	
'	Function is called by :
'	Function calls :		CheckObjectExist(), CheckObjectStatus()
'	Created on :			04/03/2008
'	Author :				Rathna Reddy
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function MaximizeWindow(ByVal objWindow)
On Error Resume Next
		'Variable Section Begin
			Dim strMethod
			Dim strEnabled , strMaximisable

		'Variable Section End
			strMethod = strKeywordForReporting
			If CheckObjectExist(objWindow) Then
				strEnabled = CheckObjectStatus(objWindow)
				If strEnabled = true Then
					strMaximisable = objWindow.getROProperty("maximizable")
					If  err.number = 0 and not isempty(strMaximisable)Then
								If  strMaximisable = true Then
									  objWindow.Maximize 
									  If err.Number = 0 Then
											MaximizeWindow  = 0
											WriteStatusToLogs "The specified Window is maximized successfully."
									   Else
											MaximizeWindow = 1
											WriteStatusToLogs "The specified Window is not maximized, please verify."
									   End If
								else
										MaximizeWindow = 1
										WriteStatusToLogs "The specified Window is not Maximizable, please verify."
								End If
											
					Else
							 MaximizeWindow  = 1
							 WriteStatusToLogs "Either an error occurred or the specified window is not maximizable, hence it cannot be maximized."
					End if
		       Else
					MaximizeWindow  = 1
					WriteStatusToLogs "Either an error occurred or the specified window is not enabled, hence it could not be Maximize."
					
			  End If
		Else
		     MaximizeWindow = 1 
			 WriteStatusToLogs "The specified Window does not exist, please verify."
					
	  End If
   
On Error GoTo 0 
End Function

'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :			MinimizeWindow()
'	Purpose :		        Minimizes the specified Window if it exist, isEnabled and is Minimisable.
'	Return Value :		 	Integer(0/1)
'	Arguments :
'		Input Arguments :	objWindow      
'		Output Arguments :	
'	Function is called by :
'	Function calls :		CheckObjectExist(), CheckObjectStatus()
'	Created on : 			04/03/2008
'	Author :				Rathna Reddy
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function MinimizeWindow(ByVal objWindow)
On Error Resume Next
		'Variable Section Begin
			Dim strMethod
			Dim strEnabled , strMinimisable

		'Variable Section End
			strMethod = strKeywordForReporting
			If CheckObjectExist(objWindow) Then
				strEnabled = CheckObjectStatus(objWindow)
				If strEnabled = true Then
					strMinimisable = objWindow.getROProperty("minimizable")
					If  err.number = 0 and not isempty(strMinimisable)Then
								If  strMinimisable = true Then
									  objWindow.Minimize 
									  If err.Number = 0 Then
											MinimizeWindow  = 0
											WriteStatusToLogs "The specified Window is minimized successfully."
									   Else
											MinimizeWindow = 1
											WriteStatusToLogs "The specified Window is not minimized, please verify."
									   End If
								else
										MinimizeWindow = 1
										WriteStatusToLogs "The specified Window is not Minimizable, please verify."
								End If
											
					Else
							 MinimizeWindow  = 1
							 WriteStatusToLogs "Either an error occurred or the specified window is not minimizable, hence it cannot be minimized."
					End if
		       Else
					MinimizeWindow  = 1
					WriteStatusToLogs "Either an error occurred or the specified window is not enabled, hence it could not be Minimize."
					
			  End If
		Else
		     MinimizeWindow = 1 
			 WriteStatusToLogs "The specified Window does not exist, please verify."
					
	  End If
   
On Error GoTo 0 
End Function


'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :				 VerifyWindowExist()
'	Purpose :					 Verifies the existance of Window in AUT.
'	Return Value :		 		  Integer( 0/1)
'	Arguments :
'		Input Arguments :  		  objWindow 
'		Output Arguments : 
'	Function is called by :
'	Function calls :			 CheckObjectExist() 
'	Created on :				      15/01/2009
'	Author :						  Rathna Reddy 
'
'------------------------------------------------------------------------------------------------------------------------------------------

Function VerifyWindowExist(ByVal objWindow)
	On Error Resume Next
		'Variable Section Begin
	
		  Dim strMethod

		'Variable Section End
		strMethod = strKeywordForReporting	
			If CheckObjectExist(objWindow) Then 
			   
					VerifyWindowExist = 0
					WriteStatusToLogs "The Window exists as expected."
					
			Else
					VerifyWindowExist = 1
					WriteStatusToLogs "The Window does not exist, please verify."
					
			End If 
	  
	
	On Error GoTo 0
End Function

'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :				 VerifyWindowNotExist()
'	Purpose :					 Verifies whether the Window is NotExist or not in AUT?.
'	Return Value :		 		  Integer( 0/1)
'	Arguments :
'		Input Arguments :  		  objWindow 
'		Output Arguments : 
'	Function is called by :
'	Function calls :			 CheckObjectExist() 
'	Created on :				   15/01/2009
'	Author :						  Rathna Reddy 
'
'------------------------------------------------------------------------------------------------------------------------------------------

Function VerifyWindowNotExist(ByVal objWindow)
	On Error Resume Next
		'Variable Section Begin
	
		   Dim strMethod

		'Variable Section End
			strMethod = strKeywordForReporting
			If  CheckObjectNotExist(objWindow) Then 
			   
					VerifyWindowNotExist = 0
					WriteStatusToLogs "The Window does not exist as expected."
					
			Else
					VerifyWindowNotExist = 2
					WriteStatusToLogs "The Window exists, please verify."
					
			End If 
	
  		
	On Error GoTo 0
End Function

'=======================Code Section End======================================================================

