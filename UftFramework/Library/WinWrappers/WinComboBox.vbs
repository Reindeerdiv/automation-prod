

'=======================Code Section Begin=============================================

Option Explicit 'Forces explicit declaration of all variables in a script. 

'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name :				  SelectListItem()
' 	Purpose :						 Selects a specified item from the ComboBox
' 	Return Value :		 		   Integer( 0/1)
' 	Arguments :
'		Input Arguments :  		objListBox , strSelectItem
'		Output Arguments :     strActItem
'	Function is called by :
'	Function calls :				CheckObjectExist()
'	Created on :				   19/03/2008
'	Author :						 Rathna Reddy 
' 
'------------------------------------------------------------------------------------------------------------------------------------------
Function SelectComboItem(ByVal objListBox , ByVal strSelectItem)
	On Error Resume Next
		'Variable Section Begin
			Dim strMethod
			Dim strActItem
	
		'Variable Section End
			strMethod = strKeywordForReporting
			If CheckObjectExist(objListBox) Then   
				objListBox.Select strSelectItem
				If Err.number = 0 Then
				
						strActItem = objListBox.GetRoProperty("text")
						If Not IsEmpty(strActItem) Then
						
								 If CompareStringValues(strSelectItem, strActItem) = 0 Then
								    SelectComboItem = 0
								   WriteStatusToLogs "Given value is selected in the ComboBox."
								 Else
										SelectComboItem = 1
					                    WriteStatusToLogs "The specific data could not be selected, please verify item exist."
								 End If
						Else
							SelectComboItem = 1
							WriteStatusToLogs "The ComboBox item could not be selected, please verify."

						End If

				Else
						SelectComboItem=1
						WriteStatusToLogs "Given List item in missing in ComboBox, please verify."
				End if
			Else
				SelectComboItem = 1
				WriteStatusToLogs "The ComboBox does not exist, please verify."
			End If
		On Error GoTo 0
End Function

'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name :				 VerifyAllComboItems()
' 	Purpose :					 Verifies  all the combo items with the expected list of items
' 	Return Value :		 		 Integer( 01)
' 	Arguments :
'		Input Arguments :  	  	 objListBox,strExpItems
'		Output Arguments :       strActItems , arrActItems() , arrExpItems()
'	Function is called by :
'	Function calls :			 CheckObjectExist() , GetArray() , MatchEachArrData()
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function VerifyAllComboItems(ByVal objListBox , ByVal arrExpItems)
On Error Resume Next
		'Variable Section Begin
			Dim strMethod
			Dim strActItems, arrActItems
			
		'Variable Section End
		strMethod = strKeywordForReporting
		If CheckObjectExist(objListBox)  Then   
				strActItems = objListBox.GetROProperty("all items")
				If Err.number = 0 then
				
						If Not IsEmpty(strActItems) Then
								arrActItems = GetArray(strActItems , chr(10))			   
							
								If MatchEachArrData(arrExpItems, arrActItems) = True Then
									VerifyAllComboItems = 0
									WriteStatusToLogs "The specified list items matches with the expected list item."
								Else
									VerifyAllComboItems = 2
									WriteStatusToLogs "The specified list items does not match the expected list item."
								End IF
						Else
								VerifyAllComboItems = 1
								WriteStatusToLogs "ComboBox does NOT contain any item, please verify."
						End If

				Else
						VerifyAllComboItems = 1
						WriteStatusToLogs "Could not retrieve all the items of the ComboBox."
				End if
		Else
				VerifyAllComboItems = 1
				WriteStatusToLogs "The ComboBox does not exist, please verify."
		End If			
On Error GoTo 0
End Function

'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name :				 VerifyListItemsInSequence()
' 	Purpose :					 Compres the list box items with our expected items in sequence order.
' 	Return Value :		 		 Integer( 0/1)
' 	Arguments :
'		Input Arguments :  		objListBox , strExpItems
'		Output Arguments :      strItems , arrActItems() , arrExpecteditems()
'	Function is called by :
'	Function calls :			  CheckObjectExist(),GetArray(),CompareArraysInSequence()
'	Created on :				   19/03/2008
'	Author :						 Rathna Reddy 
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function VerifyComboItemsInSequence(ByVal objListBox , ByVal arrExpItems)
	On Error Resume Next
		'Variable Section Begin
			Dim strMethod
			Dim strActItems , arrActItems 
	  

		'Variable Section End
		strMethod = strKeywordForReporting
		If CheckObjectExist(objListBox) Then   
				strActItems = objListBox.GetROProperty("all items")
				
				If Not IsEmpty(strActItems) Then
					arrActItems = GetArray(strActItems , chr(10))
					If CompareArraysInSequence(arrExpItems , arrActItems) = 0 Then
						VerifyComboItemsInSequence = 0
						WriteStatusToLogs "The ComboBox items sequence matches with the expected list sequence."
					Else
						VerifyComboItemsInSequence = 2
						WriteStatusToLogs "The ComboBox items are NOT in sequence with the expected list."
					End IF

				Else
					 VerifyComboItemsInSequence = 1
					 WriteStatusToLogs "The ComboBox does NOT contain any item, please verify."
				End If
				
		Else
			VerifyComboItemsInSequence = 1
			WriteStatusToLogs "The ComboBox does not exist, please verify."
		End If		
 On Error GoTo 0
End Function

'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name :				 VerifyComboBoxDisabled()
' 	Purpose :					  Verifies whether the ComboBox is Disabled.
'	Return Value :		 		  Integer( 0/1)
' 	Arguments :
'		Input Arguments :  		  objListBox 
'		Output Arguments : 
'	Function is called by :
'	Function calls :			  CheckObjectExist() , CheckObjectStatus()
'	Created on :				  14/03/2008
'	Author :					   Rathna Reddy 
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function VerifyComboBoxDisabled(ByVal objListBox)

On Error Resume Next
	'Variable Section Begin
		
		Dim strMethod

	'Variable Section End
		strMethod = strKeywordForReporting
		If CheckObjectExist(objListBox) Then
			If Not(CheckObjectStatus(objListBox)) Then
					VerifyComboBoxDisabled = 0
					WriteStatusToLogs "The ComboBox is disabled."
				Else
					VerifyComboBoxDisabled = 2
					WriteStatusToLogs "The ComboBox is not disabled, please verify."
				End If
            
		Else
				VerifyComboBoxDisabled = 1
				WriteStatusToLogs "The ComboBox does not exist, please verify."
		End if
		
	On Error GoTo 0
    
End Function

'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name :			   VerifyNoDuplicationInComboBox()
' 	Purpose :					  Verifies the duplicate items in the ComboBox
' 	Return Value :		 		Integer( 0/1)
' 	Arguments :
'		Input Arguments :  	 objListBox
'		Output Arguments : 	strItems , arrActItems() , strActItems
'	Function is called by : 
'	Function calls :			CheckObjectExist,GetArray(),VerifyDuplicateElemetsInArray()
'	Created on :				19/03/2008
'	Author :					   Rathna Reddy 
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function VerifyNoDuplicationInComboBox(ByVal objListBox)
	On Error Resume Next
		'Variable Section Begin
			Dim strMethod
			Dim strActItems , arrActItems 
			
         'Variable Section End
			strMethod = strKeywordForReporting
			If CheckObjectExist(objListBox)  Then   
				strActItems = objListBox.GetROProperty("all items")
                
					If Not IsEmpty(strActItems) Then
						arrActItems = GetArray(strActItems , chr(10))
						If Not IsNull(arrActItems) Then
					
								If VerifyDuplicateElementsInArray(arrActItems) = 0 Then
									VerifyNoDuplicationInComboBox = 0
									  WriteStatusToLogs "ComboBox has no duplicate item."
								Else
									VerifyNoDuplicationInComboBox = 2
									  WriteStatusToLogs "ComboBox contains duplicate item(s), please verify."
								End If

						 Else
								VerifyNoDuplicationInComboBox = 1
								 WriteStatusToLogs "Action failed because the listItem array received is NULL."
						 End If
					
					Else
						VerifyNoDuplicationInComboBox = 1
						WriteStatusToLogs "Action fails to get ComboBox items, please verify."
					
					End if
			Else
				VerifyNoDuplicationInComboBox = 1
				WriteStatusToLogs "ComboBox does not exist, please verify."
            End If
			'WriteStepResults VerifyNoDuplicationInComboBox 
	On Error GoTo 0
End Function
'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name :			   SelectComboItemByIndex()
' 	Purpose :					  To Select the List box item by index
' 	Return Value :		 		Integer( 0/1)
' 	Arguments :
'		Input Arguments :  	 intIndex
'		Output Arguments : 	
'	Function is called by : 
'	Function calls :			CheckObjectExist(0,
'	Created on :				19/03/2008
'	Author :					   Rathna Reddy 
'
'------------------------------------------------------------------------------------------------------------------------------------------

Function SelectComboItemByIndex(objListBox, byval intIndex)
	
	On Error Resume Next
		'Variable Section Begin
			Dim strMethod
			Dim   intGetIndex 
			
         'Variable Section End
	strMethod = strKeywordForReporting
	If CheckObjectExist(objListBox)  Then  
		intGetIndex = CInt(intIndex)
		'objListBox.Select Cstr("#"&intGetIndex)
		objListBox.Select intGetIndex
		If Err.Number = 0 Then
			SelectComboItemByIndex= 0
			WriteStatusToLogs "The item with index "& Cstr("#"&intGetIndex)&" is selected."
		Else
			SelectComboItemByIndex= 1
			WriteStatusToLogs "The Index Number provided is out of range in the specified ComboBox items Count, please verify."
		End if
	Else
	    SelectComboItemByIndex = 1
		WriteStatusToLogs "The ComboBox does not exist."
	End if
   On Error GOTO 0
End Function
'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :				 VerifyComboBoxExist()
'	Purpose :					 Verifies whether the ComboBox Exists or not?
'	Return Value :		 		  Integer( 0/1)
'	Arguments :
'		Input Arguments :  		  objListBox 
'		Output Arguments : 
'	Function is called by :
'	Function calls :			 CheckObjectExist() 
'	Created on :				   10/03/2008
'	Author :						  Rathna Reddy 
'
'------------------------------------------------------------------------------------------------------------------------------------------

Function VerifyComboBoxExist(ByVal objListBox)
	On Error Resume Next
		'Variable Section Begin
	
		  Dim strMethod

		'Variable Section End
			strMethod = strKeywordForReporting
			If CheckObjectExist(objListBox) Then 
			   
					VerifyComboBoxExist = 0
					WriteStatusToLogs "The ComboBox Exist."
					
			Else
					VerifyComboBoxExist = 2
					WriteStatusToLogs "The ComboBox does not exist, please verify."
					
			End If 
	  
	
	On Error GoTo 0
End Function

'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :				 VerifyComboBoxNotExist()
'	Purpose :					 Verifies whether the ComboBox Exists or not?
'	Return Value :		 		  Integer( 0/1)
'	Arguments :
'		Input Arguments :  		  objListBox 
'		Output Arguments : 
'	Function is called by :
'	Function calls :			 CheckObjectExist() 
'	Created on :				   10/03/2008
'	Author :						  Rathna Reddy 
'
'------------------------------------------------------------------------------------------------------------------------------------------

Function VerifyComboBoxNotExist(ByVal objListBox)
	On Error Resume Next
		'Variable Section Begin
	
		  Dim strMethod

		'Variable Section End
			strMethod = strKeywordForReporting
			If CheckObjectNotExist(objListBox) Then 
			   
					VerifyComboBoxNotExist = 0
					WriteStatusToLogs "The ComboBox does not Exist."
					
			Else
					VerifyComboBoxNotExist = 2
					WriteStatusToLogs "The ComboBox Exists, please verify."
					
			End If 
	
  		
	On Error GoTo 0
End Function


'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name :				  SelectMultipleListItems()
' 	Purpose :						 Selects multiple items from the ComboBox
' 	Return Value :		 		   Integer( 0/1)
' 	Arguments :
'		Input Arguments :  		objListBox , arrItems
'		Output Arguments :     strActItem
'	Function is called by :
'	Function calls :				CheckObjectExist()
'	Created on :				   19/01/2009
'	Author :						 Rathna Reddy 
'
'------------------------------------------------------------------------------------------------------------------------------------------
'Function SelectMultipleListItems(ByVal objListBox , ByVal arrItems)
'	On Error Resume Next
'		'Variable Section Begin
'		
'			Dim arrActItems,arrExpItems,intItem
'	
'		'Variable Section End
'		If CheckObjectExist(objListBox) Then   
'						
'					   arrExpItems = GetArray(arrItems , "^")
'					   If Not IsNull(arrExpItems) Then
'							 For  intItem=0 to Ubound(arrExpItems)
'								 If intItem = 0 Then
'									objListBox.Select arrExpItems(intItem)
'								 Else
'									objListBox.ExtendSelect arrExpItems(intItem)
'					            End If
'
'							 Next
'							If Err.Number = 0 Then
'						 
'								arrActItems = objListBox.GetRoProperty("selection")
'								arrActItems=GetArray(arrActItems,";")
'								If Err.Number=0 And Not IsNull(arrActItems) Then
'									If CompareArrays(arrExpItems , arrActItems) = 0 Then
'										SelectMultipleListItems = 0
'										WriteStatusToLogs "Action: SelectMultipleListItems"&vbtab&_
'																	  "Status:" &  "Passed" &Vbtab&_
'																	  "Message:" & "The Expected Multiple Items are selected sucessfully"
'									Else
'											SelectMultipleListItems = 2
'											 WriteStatusToLogs "Action: SelectMultipleListItems"&vbtab&_
'																	 "Status:" &  "Failed" &Vbtab&_
'																	"Message:" & "Expected Multiple Items have not been selected, please verify"
'									End If
'
'								Else
'									 SelectMultipleListItems = 1
'									 WriteStatusToLogs "Action: SelectMultipleListItems"&vbtab&_
'																  "Status:" &  "Failed" &Vbtab&_
'																	"Message:" & "Could not capture selected items from ComboBox"
'								End If
'
'						 Else
'							 SelectMultipleListItems = 1
'							 WriteStatusToLogs "Action: SelectMultipleListItems"&vbtab&_
'											  "Status:" &  "Failed" &Vbtab&_
'											 "Message:" & "Action fails to get selected items, please verify"
'						End If
'				Else
'						SelectMultipleListItems = 1
'						WriteStatusToLogs "Action: SelectMultipleListItems"&vbtab&_
'							              "Status:" &  "Failed" &Vbtab&_
'										 "Message:" & "An error occurred , the Action fails to get the array of the Expected items , please verify"
'
'				End If
'     	Else
'				SelectMultipleListItems = 1
'				WriteStatusToLogs "Action: SelectMultipleListItems"&vbtab&_
'							              "Status:" &  "Failed" &Vbtab&_
'										 "Message:" & "The specified ComboBox doesn't exist, please verify"
'		End If
'		On Error GoTo 0
'End Function

'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name :				 VerifyComboBoxNOTContainsItem	()
' 	Purpose :					 Verifies  whether the ComboBox doesn't contain the specified Item.
' 	Return Value :		 		   Integer( 0/1/2)
' 	Arguments :
'		Input Arguments :  	  	objListBox,strExpItems
'		Output Arguments :     sstrActItems , arrActItems , arrExpItems,intCount,strExpItem,intActItem
'	Function is called by :
'	Function calls :			   CheckObjectExist() , GetArray() 
'	Created on :				  23/01/2009
'	Author :						Rathna Reddy 
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function VerifyComboBoxNOTContainsItem(ByVal objListBox , ByVal strListItem)
	On Error Resume Next
		'Variable Section Begin
			Dim strMethod
			Dim strActItems , arrActItems , arrExpItems,strExpItem,intActItem, blnEqualFlag, blnstatus
			blnEqualFlag=true
			blnstatus = false
						
		'Variable Section End
			strMethod = strKeywordForReporting
			If CheckObjectExist(objListBox)  Then   
				strActItems = objListBox.GetROProperty("all items")
				
                If Err.Number = 0 Then
						
											
								arrActItems = GetArray(strActItems , chr(10))
							If Vartype(arrActItems) <> vbEmpty  And Err.Number = 0 Then
								blnstatus = true
							End If
							   If blnstatus Then
								 
 													  For intActItem = 0 to Ubound(arrActItems)
																   If strListItem = arrActItems(intActItem) Then
																		blnEqualFlag=false
																		Exit for
																   End If
                                                      Next
														 
                    										 If blnEqualFlag = true  Then
																  VerifyComboBoxNOTContainsItem = 0
																	WriteStatusToLogs "List Does not contain the specified item."
                                  
															  Else
																	 VerifyComboBoxNOTContainsItem = 2
																	  WriteStatusToLogs "List contain the specified item, please verify."
														  End If
												   
									 
							Else
								 VerifyComboBoxNOTContainsItem = 1
								 WriteStatusToLogs "An error occurred, action fails to get Actual items, please verify."
						 End If
						
				Else
						VerifyComboBoxNOTContainsItem = 1
						WriteStatusToLogs "Action Fails to get Items from ComboBox, please verify ComboBox method and property."
				End If
			
	 Else
			VerifyComboBoxNOTContainsItem = 1
			WriteStatusToLogs "ComboBox doesn't exist, please verify."

	 End If			
    On Error GoTo 0
End Function
'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name :				 VerifyListBoxContainsItem		()
' 	Purpose :					 Verifies  whether the ComboBox contains the specified Item.
' 	Return Value :		 		   Integer( 0/1/2)
' 	Arguments :
'		Input Arguments :  	  	objListBox,strExpItems
'		Output Arguments :     sstrActItems , arrActItems , arrExpItems,intCount,strExpItem,intActItem
'	Function is called by :
'	Function calls :			   CheckObjectExist() , GetArray() 
'	Created on :				  23/01/2009
'	Author :						Rathna Reddy 
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function VerifyComboBoxContainsItem	(ByVal objListBox , ByVal strListItem)
	On Error Resume Next
		'Variable Section Begin
			Dim strMethod
			Dim strActItems , arrActItems , arrExpItems,intCount,strExpItem,intActItem , blnpresentFlag, blnstatus
			blnpresentFlag=false
			blnstatus= False
			
			
		'Variable Section End
			strMethod = strKeywordForReporting
			If CheckObjectExist(objListBox)  Then   
				strActItems = objListBox.GetROProperty("all items")
				
                 If Vartype(strActItems) <> vbEmpty  And Err.Number = 0 Then
					blnstatus = true
                    
							If blnstatus Then
                            arrActItems = GetArray(strActItems , chr(10))				  
											  For intActItem = 0 to Ubound(arrActItems)
													   If strListItem = arrActItems(intActItem) Then
															blnpresentFlag=True
															Exit for
													   End If
											 Next
											 
											If blnpresentFlag = True  Then
													  VerifyComboBoxContainsItem	 = 0
														WriteStatusToLogs "ComboBox Contains the expected item."
												  Else
														 VerifyComboBoxContainsItem	 = 2
														  WriteStatusToLogs "List does not contain the expected item, please verify."
											End If
												   
									 
							Else
								 VerifyComboBoxContainsItem	 = 1
								 WriteStatusToLogs "Action fails to get Actual items into an array please verify."
						 End If
						
					Else
						VerifyComboBoxContainsItem	 = 1
						WriteStatusToLogs "Action Fails to get Items from ComboBox, please verify ComboBox method and property"
				End If
			
	 Else
	 VerifyComboBoxContainsItem	 = 1
	 WriteStatusToLogs "ComboBox doesn't exist, please verify."

	 End If
			'WriteStepResults VerifyListBoxContainsItem	
    On Error GoTo 0
End Function

'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name :				 VerifyListBoxSize()
' 	Purpose :					 Verifies the size(no.of items) of ComboBox
' 	Return Value :		 		   Integer( 0/1/2)
' 	Arguments :
'		Input Arguments :  	  	objListBox,intItemCount
'		Output Arguments :    intActCount,intCount
'	Function is called by :
'	Function calls :			   CheckObjectExist() , GetArray() 
'	Created on :				  23/01/2009
'	Author :						Rathna Reddy 
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function VerifyComboBoxSize(ByVal objListBox , ByVal intExpectedSize)
	On Error Resume Next
		'Variable Section Begin
			Dim strMethod
			Dim intActCount,intCount
			
		'Variable Section End
			strMethod = strKeywordForReporting
			If CheckObjectExist(objListBox)  Then   
				intCount=Cint(intExpectedSize)
				
				If VarType(intCount) = VbInteger Then
				
				intActCount = objListBox.GetROProperty("items count")
					If Not IsEmpty(intActCount)  Then
							If CompareStringValues(intCount,intActCount)=0 Then
								  VerifyComboBoxSize = 0
								  WriteStatusToLogs "The specified ComboBox Size is same as the expected size."
							Else
								   VerifyComboBoxSize = 2
									WriteStatusToLogs "The specified ComboBox Size is different from the expected Size, please verify."
						End If
					Else
							  VerifyComboBoxSize = 1
								WriteStatusToLogs "Action Fails to get ComboBox Items Count, please verify ComboBox and it's method and property."
				End If
		Else
		   VerifyComboBoxSize = 1
			 WriteStatusToLogs "The specified List box's expected size is not an Integer, please verify."
		

		End If
		Else
			 VerifyComboBoxSize = 1
			 WriteStatusToLogs "The specified ComboBox doesn't exist, please verify."

	 End If
			'WriteStepResults VerifyComboBoxNOTContainsItem
    On Error GoTo 0
End Function

'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name :				 VerifyComboBoxEnabled()
' 	Purpose :					 Verifies whether the ComboBox is Enabled
' 	Return Value :		 		  Integer( 0/1)
' 	Arguments :
'		Input Arguments :  		objListBox 
'		Output Arguments : 
'	Function is called by :
'	Function calls :				CheckObjectExist() , CheckObjectStatus()
'	Created on :				   11/03/2008
'	Author :						  Rathna Reddy 
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function VerifyComboBoxEnabled(ByVal objListBox)

On Error Resume Next
	'Variable Section Begin
		
		Dim strMethod

	'Variable Section End
		strMethod = strKeywordForReporting
		If CheckObjectExist(objListBox) Then
			If CheckObjectEnabled(objListBox) Then
					VerifyComboBoxEnabled = 0
					WriteStatusToLogs "The ComboBox was enabled."
				Else
					VerifyComboBoxEnabled = 2
					WriteStatusToLogs "The ComboBox was not enabled, please verify."
				End If
            
		Else
				VerifyComboBoxEnabled = 1
				WriteStatusToLogs "The ComboBox does not exist, please verify."
		End if
		
	On Error GoTo 0
    
End Function

'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :				 VerifyComboBoxVisible()
'	Purpose :					 Verifies whether the ComboBox is visible.
'	Return Value :		 		  Integer( 0/1)
'	Arguments :
'		Input Arguments :  		  objListBox 
'		Output Arguments : 
'	Function is called by :
'	Function calls :			 CheckObjectExist() , CheckObjectVisiblity()
'	Created on :				   10/03/2008
'	Author :						  Rathna Reddy 
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function VerifyComboBoxVisible(ByVal objListBox)
	On Error Resume Next
		'Variable Section Begin
		
			Dim strMethod

		'Variable Section End
			strMethod = strKeywordForReporting
			If CheckObjectExist(objListBox)  Then 
				If CheckObjectVisible(objListBox) Then
					VerifyComboBoxVisible = 0
					WriteStatusToLogs "The ComboBox was Visible."
				Else
					VerifyComboBoxVisible = 2
					WriteStatusToLogs "The ComboBox was not Visible, please verify."
				End If
			Else
					VerifyComboBoxVisible = 1
					WriteStatusToLogs "The ComboBox does not exist, please verify."
			End If
		
	On Error GoTo 0
End Function
'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :				 VerifyComboBoxInVisible()
'	Purpose :					 Verifies whether the ComboBox is Invisible.
'	Return Value :		 		  Integer( 0/1)
'	Arguments :
'		Input Arguments :  		  objListBox 
'		Output Arguments : 
'	Function is called by :
'	Function calls :			 CheckObjectExist() , CheckObjectVisiblity()
'	Created on :				   10/03/2008
'	Author :						  Rathna Reddy 
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function VerifyComboBoxInVisible(ByVal objListBox)
	On Error Resume Next
		'Variable Section Begin
		
			Dim strMethod

		'Variable Section End
			strMethod = strKeywordForReporting
			If CheckObjectExist(objListBox)  Then 
				If CheckObjectInVisible(objListBox) Then
					VerifyComboBoxInVisible = 0
					WriteStatusToLogs "The ComboBox was invisible."
				Else
					VerifyComboBoxInVisible = 2
					WriteStatusToLogs "The ComboBox was not InVisible, please verify."
				End If
			Else
					VerifyComboBoxInVisible = 1
					WriteStatusToLogs "The ComboBox does not exist, please verify."
			End If
		
	On Error GoTo 0
End Function


 '------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :				 StoreWinComboBoxSelectedItem()
'	Purpose :					 Stores the selected item
'	Return Value :		 		 Integer( 0/1)
'	Arguments :
'		Input Arguments :  		 objComboBox ,strKey  
'		Output Arguments : 
'	Function is called by :
'	Function calls :			 CheckObjectExist() , AddItemToStorageDictionary()
'	Created on :				 10/02/2012
'	Author :					 Iohbha K
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function StoreWinComboBoxSelectedItem(ByVal objComboBox, ByVal strKey)
On Error Resume Next
		'Variable Section Begin
		
			Dim strMethod, strSelection, intval

		'Variable Section End
			strMethod = strKeywordForReporting
			If IsKey(strKey) = 0 Then
					If CheckObjectExist(objComboBox) Then   
							strSelection = objComboBox.GetRoProperty("selection")
							If Err.number=0 Then
									intval = AddItemToStorageDictionary(strKey,strSelection)
									If  intval = 0 Then
											StoreWinComboBoxSelectedItem = 0
											WriteStatusToLogs "The selected item "&strSelection&" is stored successfully in the Key '"&strKey&"'" 
												  
									Else
											StoreWinComboBoxSelectedItem = 1
											WriteStatusToLogs "Failed to store the selected item in the Key '"&strKey&"'. Please verify" 
												  
									End If
							Else
									StoreWinComboBoxSelectedItem = 1
									WriteStatusToLogs "Failed to store the selected item an error occurred. Please verify. "&Err.description 
							End if
					Else
						StoreWinComboBoxSelectedItem = 1
						WriteStatusToLogs "The WinCombobox does not exist, please verify."
					End If
			Else
					StoreWinComboBoxSelectedItem = 1
					WriteStatusToLogs "The key is invalid, please verify."
			End if
On Error GoTo 0
End Function


'=======================Code Section End=============================================
