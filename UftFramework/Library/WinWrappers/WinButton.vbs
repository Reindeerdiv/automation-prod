
'=======================Code Section Begin============================================

Option Explicit		 'Forces explicit declaration of all variables in a script. 

'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :			  ClickButton()
'	Purpose :				  Performs  Click operation on a specified Button.
'	Return Value :		 	  Integer( 0/1)
'	Arguments :
'		Input Arguments :  	 objButton
'		Output Arguments : 
'	Function is called by :
'	Function calls :		     CheckObjectExist() , CheckObjectClick()
'	Created on :			    10/03/2008
'	Author :					   Rathna Reddy 
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function ClickWinButton(ByVal objButton)
	On Error Resume Next
		'Variable Section Begin
	
		Dim strMethod

		'Variable Section End
			strMethod = strKeywordForReporting
			If CheckObjectExist(objButton) Then 
			   If CheckObjectEnabled(objButton) Then
					If CheckObjectClick(objButton) Then
						ClickWinButton = 0
						WriteStatusToLogs "Click has been performed on button successfully."
					Else
						ClickWinButton = 1
						WriteStatusToLogs "Click could not be performed on button, please verify."
					End If 
				Else
					ClickWinButton = 1
					WriteStatusToLogs "The Button was disabled and click could not be performed, please verify."
				End If
			  Else
                   ClickWinButton = 1
				   WriteStatusToLogs "The Button does not exist, please verify."
			  End If
			
		
	On Error GoTo 0
End Function
'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name :				 CompareButtonName()
' 	Purpose :						Compares the Button Name with our expected Name .
' 	Return Value :		 		  Integer(0/1/2)
' 	Arguments :
'		Input Arguments :  		objButton , strExpName
'		Output Arguments : 	   strActName
'	Function is called by :
'	Function calls :				CheckObjectExist(),CompareStringValues()				
'	Created on :				   10/03/2008
'	Author :						  Rathna Reddy 
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function CompareWinButtonName(ByVal objButton , ByVal strExpName)
	On Error Resume Next
		'Variable Section Begin
			Dim strMethod
			Dim strActName, blnstatus 
			blnstatus  = False
		'Variable Section End
			strMethod = strKeywordForReporting
			If CheckObjectExist(objButton) Then         
					strActName = objButton.GetRoProperty("text")
				
					If Vartype(strActName) <> vbEmpty  And Err.Number = 0 Then
							blnstatus = true
					End If
					If blnstatus =True Then
							If CompareStringValues(strExpName,strActName) = 0  Then
							     CompareWinButtonName = 0
							     WriteStatusToLogs """"&"Expected Name : "&strExpName& """" &  " and " &""""&"Actual Name : "&strActName&""""&" matches as expected."
		
							Else
								 strActName = replace(strActName,"&","")
								 If CompareStringValues(strExpName,strActName) = 0  Then
										CompareWinButtonName = 0
										 WriteStatusToLogs """"&"Expected Name : "&strExpName& """" &  " and " &""""&"Actual Name : "&strActName&""""&" matches as expected."
			
								 Else
										CompareWinButtonName = 2
										 WriteStatusToLogs """"&"Expected Name : "&strExpName& """" &  " and " &""""&"Actual Name : "&strActName&""""&" does not match, please verify."
								  End if 
								
							End If
					Else
							CompareWinButtonName = 1
							WriteStatusToLogs "An error occurred during the method "&strMethod
					End if
			Else
					CompareWinButtonName = 1
					WriteStatusToLogs "The specified Button does not exist, please verify."
			End If
			
	On Error GoTo 0
End Function
'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name :				 VerifyButtonEnabled()
' 	Purpose :					 Verifies whether the Button is Enabled
' 	Return Value :		 		  Integer( 0/1)
' 	Arguments :
'		Input Arguments :  		objButton 
'		Output Arguments : 
'	Function is called by :
'	Function calls :				CheckObjectExist() , CheckObjectStatus()
'	Created on :				   11/03/2008
'	Author :						  Rathna Reddy 
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function VerifyWinButtonEnabled(ByVal objButton)

On Error Resume Next
	'Variable Section Begin
		
		Dim strMethod

	'Variable Section End
		strMethod = strKeywordForReporting
		If CheckObjectExist(objButton) Then
			If CheckObjectEnabled(objButton) Then
					VerifyWinButtonEnabled = 0
					WriteStatusToLogs "The Button was enabled as expected."
				Else
					VerifyWinButtonEnabled = 2
					WriteStatusToLogs "The Button was not enabled, please verify."
				End If
            
		Else
				VerifyWinButtonEnabled = 1
				WriteStatusToLogs "The Button does not exist, please verify."
		End if
		
	On Error GoTo 0
    
End Function

'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name :				 VerifyButtonDisabled()
' 	Purpose :					 Verifies whether the Button is Disabled
' 	Return Value :		 		  Integer( 0/1)
' 	Arguments :
'		Input Arguments :  		objButton 
'		Output Arguments : 
'	Function is called by :
'	Function calls :				CheckObjectExist() , CheckObjectStatus()
'	Created on :				   11/03/2008
'	Author :						  Rathna Reddy 
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function VerifyWinButtonDisabled(ByVal objButton)

On Error Resume Next
	'Variable Section Begin
		
		Dim strMethod

	'Variable Section End
	strMethod = strKeywordForReporting
		If CheckObjectExist(objButton) Then
			If CheckObjectDisabled(objButton) Then
					VerifyWinButtonDisabled = 0
					WriteStatusToLogs "The Button was disabled as expected."
				Else
					VerifyWinButtonDisabled = 2
					WriteStatusToLogs "The Button was not disabled, please verify."
				End If
            
		Else
				VerifyWinButtonDisabled = 1
				WriteStatusToLogs "The Button does not exist, please verify."
		End if
	On Error GoTo 0
End Function

'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :				 VerifyButtonVisible()
'	Purpose :					 Verifies whether the Button is visible.
'	Return Value :		 		  Integer( 0/1)
'	Arguments :
'		Input Arguments :  		  objButton 
'		Output Arguments : 
'	Function is called by :
'	Function calls :			 CheckObjectExist() , CheckObjectVisiblity()
'	Created on :				   10/03/2008
'	Author :						  Rathna Reddy 
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function VerifyWinButtonVisible(ByVal objButton)
	On Error Resume Next
		'Variable Section Begin
		
			Dim strMethod

		'Variable Section End
		strMethod = strKeywordForReporting
			If CheckObjectExist(objButton)  Then 
				If CheckObjectVisible(objButton) Then
					VerifyWinButtonVisible = 0
					WriteStatusToLogs "The Button was visible as expected."
				Else
					VerifyWinButtonVisible = 2
					WriteStatusToLogs "The Button was not visible, please verify."
				End If
			Else
					VerifyWinButtonVisible = 1
					WriteStatusToLogs "The Button does not exist, please verify."
			End If
		
	On Error GoTo 0
End Function
'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :				 VerifyButtonInVisible()
'	Purpose :					 Verifies whether the Button is Invisible.
'	Return Value :		 		  Integer( 0/1)
'	Arguments :
'		Input Arguments :  		  objButton 
'		Output Arguments : 
'	Function is called by :
'	Function calls :			 CheckObjectExist() , CheckObjectVisiblity()
'	Created on :				   10/03/2008
'	Author :						  Rathna Reddy 
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function VerifyWinButtonInVisible(ByVal objButton)
	On Error Resume Next
		'Variable Section Begin
		
			Dim strMethod

		'Variable Section End
			strMethod = strKeywordForReporting
			If CheckObjectExist(objButton)  Then 
				If CheckObjectInVisible(objButton) Then
					VerifyWinButtonInVisible = 0
					WriteStatusToLogs "The Button was in-visible as expected."
				Else
					VerifyWinButtonInVisible = 2
					WriteStatusToLogs "The Button was not in-visible, please verify."
				End If
			Else
					VerifyWinButtonInVisible = 1
					WriteStatusToLogs "The Button does not exist, please verify."
			End If
		
	On Error GoTo 0
End Function
'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :				 VerifyButtonExist()
'	Purpose :					 Verifies whether the Button Exists.
'	Return Value :		 		  Integer( 0/1)
'	Arguments :
'		Input Arguments :  		  objObject 
'		Output Arguments : 
'	Function is called by :
'	Function calls :			 CheckObjectExist() 
'	Created on :				   10/03/2008
'	Author :						  Rathna Reddy 
'
'------------------------------------------------------------------------------------------------------------------------------------------

Function VerifyWinButtonExist(ByVal objButton)
	On Error Resume Next
		'Variable Section Begin
	
		   Dim strMethod

		'Variable Section End
			strMethod = strKeywordForReporting
			If CheckObjectExist(objButton) Then 
			   
					VerifyWinButtonExist = 0
					WriteStatusToLogs "The Button exists as expected."
					
			Else
					VerifyWinButtonExist = 1
					WriteStatusToLogs "The Button does NOT exist, please verify."
					
			End If 
	  
	
	On Error GoTo 0
End Function

'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :				 VerifyButtonNotExist()
'	Purpose :					 Verifies whether the Button does NOT Exists.
'	Return Value :		 		  Integer( 0/1)
'	Arguments :
'		Input Arguments :  		  objObject 
'		Output Arguments : 
'	Function is called by :
'	Function calls :			 CheckObjectExist() 
'	Created on :				   10/03/2008
'	Author :						  Rathna Reddy 
'
'------------------------------------------------------------------------------------------------------------------------------------------

Function VerifyWinButtonNotExist(ByVal objButton)
	On Error Resume Next
		'Variable Section Begin
	
		   Dim strMethod

		'Variable Section End
			strMethod = strKeywordForReporting		
			If CheckObjectNotExist(objButton) Then 
			   
					VerifyWinButtonNotExist = 0
					WriteStatusToLogs "The Button does not exist as expected"
					
			Else
					VerifyWinButtonNotExist = 1
					WriteStatusToLogs "The Button exists, please verify."
					
			End If 
	
  		
	On Error GoTo 0
End Function

'=======================Code Section End=============================================
