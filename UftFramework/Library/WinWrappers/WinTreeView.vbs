'=======================Code Section Begin=============================================
Option Explicit 

'
'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name :				  SelectListItem()
' 	Purpose :						 Selects a specified item from the ListBox
' 	Return Value :		 		   Integer( 0/1)
' 	Arguments :
'		Input Arguments :  		objWinTree , strSelectItem
'		Output Arguments :     strActItem
'	Function is called by :
'	Function calls :				CheckObjectExist()
'	Created on :				   19/03/2008
'	Author :						 Rathna Reddy  
'------------------------------------------------------------------------------------------------------------------------------------------
Function DragItemDropInItemWTV(ByVal objWinTree , ByVal arrDragItems, byval arrDropItems)
	On Error Resume Next
		'Variable Section Begin
			Dim strMethod
			Dim blnstatus, blnDragMatch, blnDropMatch
			Dim strListContent
			Dim arrListContents
			Dim intmaxlength, intcount
            Dim strDragTreelistItems, strDropTreelistItems
			Dim arrDragTreeListItems, arrDropTreeListItems
            Dim intContentItems, intDragItems, intDropItems, intTemparrcount
			strListContent = Null
			blnstatus = false
			blnDragMatch = False
			blnDropMatch = False
       
		'Variable Section End
			strMethod = strKeywordForReporting
			If CheckObjectExist(objWinTree) Then
				strListContent = objWinTree.GetContent

				If Vartype(strListContent) <> vbNull  And Err.Number = 0 Then
					blnstatus = true
				End If
					If blnstatus  Then
					
					arrListContents = GetArray(strListContent, chr(10))
					intmaxlength = ubound(arrListContents)
					intDragItems =  Ubound(arrDragItems)
					intDropItems = Ubound(arrDropItems)
				   

					For intcount = 0 to intmaxlength
							strDragTreelistItems = arrListContents(intcount)
							arrDragTreeListItems = GetArray(strDragTreelistItems, ";")
							intTemparrcount = Ubound(arrDragTreeListItems)
							If intTemparrcount = intDragItems Then
								If CompareArraysInSequence(arrDragTreeListItems, arrDragItems) = 0 Then
									 blnDragMatch = true
								 Exit for
								   
								End If
							End If

					Next

									  If  blnDragMatch Then
											  For intcount = 0 to intmaxlength
														strDropTreelistItems = arrListContents(intcount)
														arrDropTreeListItems = GetArray(strDropTreelistItems, ";")
														intTemparrcount = Ubound(arrDropTreeListItems)
															If intTemparrcount = intDropItems Then
																 If CompareArraysInSequence(arrDropTreeListItems, arrDropItems) = 0 Then
																	blnDropMatch = true
																	 Exit for
																	
																End If
															End if
											Next
														If blnDropMatch Then
															objWinTree.DragItem strDragTreelistItems
															objWinTree.DropOnItem strDropTreelistItems
																		If err.Number = 0 Then
																		DragItemDropInItemWTV = 0
																		WriteStatusToLogs "Drag And drop item successfully."
																		Else
																		DragItemDropInItemWTV = 1
																		WriteStatusToLogs "Error Occurred during drag and drop Operation."
																		End If
							
		
															Else'blnDropMatch
																DragItemDropInItemWTV = 1
																WriteStatusToLogs "The Drop Items are not existing."					
															End If
			
											Else'blnDragMatch
											DragItemDropInItemWTV = 1
											WriteStatusToLogs "The Drag Items are not existing."
										End If   		   
						Else'blnstatus
						DragItemDropInItemWTV = 1
						WriteStatusToLogs "An error occurred while Getting the Tree List Nodes."
						End If
			Else
				DragItemDropInItemWTV = 1
				WriteStatusToLogs "The Tree View Object does not exist, please verify."
			End If
		On Error GoTo 0
End Function
'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name :				 ExpandNode()
' 	Purpose :					 Verifies  all the list items with our expected
' 	Return Value :		 		   Integer( 01)
' 	Arguments :
'		Input Arguments :  	  	objWinTree,strExpItems
'		Output Arguments :     strActItems , arrActItems() , arrExpItems()
'	Function is called by :
'	Function calls :			   CheckObjectExist() , GetArray() , MatchEachArrData()
'	Created on :				  19/03/2008
'	Author :						Rathna Reddy 
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function ExpandNode(ByVal objWinTree , ByVal arrItems)
	On Error Resume Next
		'Variable Section Begin
			Dim strMethod
			Dim blnstatus, blnMatch
			Dim strListContent, strTreelistItems
			Dim arrDropItems, arrListContents
			Dim intmaxlength, intcount
			Dim arrTreeListItems
			Dim intTemparrcount, intTreeItems

			strListContent = Null
			blnstatus = false
			blnMatch = False
			'blnDropMatch = False
	   'Variable Section End

			strMethod = strKeywordForReporting
			If CheckObjectExist(objWinTree) Then
				strListContent = objWinTree.GetContent

				If Vartype(strListContent) <> vbNull  And Err.Number = 0 Then
					blnstatus = true
				End If
					If blnstatus  Then
					intTreeItems = Ubound(arrItems)

					arrListContents = GetArray(strListContent, chr(10))
					intmaxlength = ubound(arrListContents)
					
					For intcount = 0 to intmaxlength
							strTreelistItems = arrListContents(intcount)
							arrTreeListItems = GetArray(strTreelistItems, ";")
							  intTemparrcount = Ubound(arrTreeListItems)
								If intTemparrcount = intTreeItems Then
									 If CompareArraysInSequence(arrItems, arrTreeListItems) = 0 Then
										  blnMatch = true
										 Exit for
									End If
								End If
					Next

									  If  blnMatch Then
											objWinTree.Activate
											objWinTree.Expand  strTreelistItems
											''Do not check on error number, because even after expanding the item correctly QTP throws an error	
											ExpandNode = 0
											WriteStatusToLogs "Expanded the item(s) "&strTreelistItems
									
										Else'blnMatch
										ExpandNode = 1
										WriteStatusToLogs "The Expand Items are not existing."
										End If   		   
						Else'blnstatus
						ExpandNode = 1
						WriteStatusToLogs "An error occurred while Getting the Tree List Nodes."
						End If
			Else
				ExpandNode = 1
				WriteStatusToLogs "The Tree View Object does not exist, please verify."
			End If
		On Error GoTo 0
End Function

'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name :				 CollapseNode()
' 	Purpose :					 Verifies  all the list items with our expected
' 	Return Value :		 		   Integer( 01)
' 	Arguments :
'		Input Arguments :  	  	objWinTree,strExpItems
'		Output Arguments :     strActItems , arrActItems() , arrExpItems()
'	Function is called by :
'	Function calls :			   CheckObjectExist() , GetArray() , MatchEachArrData()
'	Created on :				  19/03/2008
'	Author :						Rathna Reddy 
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function CollapseNode(ByVal objWinTree , ByVal arrItems)
	On Error Resume Next
		'Variable Section Begin
			Dim strMethod
			Dim blnstatus, blnMatch
			Dim strListContent, strTreelistItems
			Dim arrDropItems, arrListContents
			Dim intmaxlength, intcount
			Dim arrTreeListItems
			Dim intTemparrcount, intTreeItems

			strListContent = Null
			blnstatus = false
			blnMatch = False
			'blnDropMatch = False
		'Variable Section End

			strMethod = strKeywordForReporting
			If CheckObjectExist(objWinTree) Then
				strListContent = objWinTree.GetContent

				If Vartype(strListContent) <> vbNull  And Err.Number = 0 Then
					blnstatus = true
				End If
					If blnstatus  Then
					intTreeItems = Ubound(arrItems)

					arrListContents = GetArray(strListContent, chr(10))
					intmaxlength = ubound(arrListContents)
					
					For intcount = 0 to intmaxlength
							strTreelistItems = arrListContents(intcount)
							arrTreeListItems = GetArray(strTreelistItems, ";")
							  intTemparrcount = Ubound(arrTreeListItems)
								If intTemparrcount = intTreeItems Then
									 If CompareArraysInSequence(arrItems, arrTreeListItems) = 0 Then
										  blnMatch = true
										 Exit for
									End If
								End If
					Next

									  If  blnMatch Then

															objWinTree.Collapse  strTreelistItems
																		If err.Number = 0 Then
																		CollapseNode = 0
																		WriteStatusToLogs "The items are collapsed."
																		Else
																		CollapseNode = 1
																		WriteStatusToLogs "Error occurred during drag and drop operation."
																		End If
							
		
			
											Else'blnMatch
											CollapseNode = 1
											WriteStatusToLogs "The Items are not existing in the Tree."
										End If   		   
						Else'blnstatus
						CollapseNode = 1
						WriteStatusToLogs "An error occurred while Getting the Tree List Nodes."
						End If
			Else
				CollapseNode = 1
				WriteStatusToLogs "The Tree View Object does not exist, please verify."
			End If
		On Error GoTo 0
End Function



'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name :				 SelectNode()
' 	Purpose :					 Verifies  all the list items with our expected
' 	Return Value :		 		   Integer( 01)
' 	Arguments :
'		Input Arguments :  	  	objWinTree,strExpItems
'		Output Arguments :     strActItems , arrActItems() , arrExpItems()
'	Function is called by :
'	Function calls :			   CheckObjectExist() , GetArray() , MatchEachArrData()
'	Created on :				  19/03/2008
'	Author :						Rathna Reddy 
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function SelectNode(ByVal objWinTree , ByVal arrItems)
	On Error Resume Next
		'Variable Section Begin
			Dim strMethod
			Dim blnstatus, blnMatch
			Dim strListContent, strTreelistItems
			Dim arrDropItems, arrListContents
			Dim intmaxlength, intcount
			Dim arrTreeListItems
			Dim intTemparrcount, intTreeItems
			Dim strTempSelectedNode

			strListContent = Null
			blnstatus = false
			blnMatch = False
			'blnDropMatch = False
		'Variable Section End
			strMethod = strKeywordForReporting
			If CheckObjectExist(objWinTree) Then
				strListContent = objWinTree.GetContent

				If Vartype(strListContent) <> vbNull  And Err.Number = 0 Then
					blnstatus = true
				End If
					If blnstatus  Then
					intTreeItems = Ubound(arrItems)

					arrListContents = GetArray(strListContent, chr(10))
					intmaxlength = ubound(arrListContents)
					
					For intcount = 0 to intmaxlength
							strTreelistItems = arrListContents(intcount)
							arrTreeListItems = GetArray(strTreelistItems, ";")
							  intTemparrcount = Ubound(arrTreeListItems)
								If intTemparrcount = intTreeItems Then
									 If CompareArraysInSequence(arrItems, arrTreeListItems) = 0 Then
										  blnMatch = true
										 Exit for
									End If
								End If
					Next

									  If  blnMatch Then

															objWinTree.Select strTreelistItems
															If Err.Number = 0  Then
																SelectNode = 0
															'strTempSelectedNode = objWinTree.GetSelection
                                                              '          If CompareStringValues(strTreelistItems,strTempSelectedNode ) = 0 And Err.Number = 0 Then
'																		Else
'																		SelectNode = 1
'																		WriteStatusToLogs "Action: "&strMethod&Vbtab&_
'																				"Status:" &  "Failed" &Vbtab&_
'																				"Message:" & "The Selected Node and Focused node are different."
'																		End If
																Else
																  SelectNode = 1
																		WriteStatusToLogs "Error occurred during select operation."
															End If
							
		
			
											Else'blnMatch
											SelectNode = 1
											WriteStatusToLogs "The Expand Items are not existing."
										End If   		   
						Else'blnstatus
						SelectNode = 1
						WriteStatusToLogs "An error occurred while getting the Tree List nodes."
						End If
			Else
				SelectNode = 1
				WriteStatusToLogs "The Tree View Object does not exist, please verify."
			End If
		On Error GoTo 0
End Function


'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name :				 VerifyNodeExistingInHierarchy
' 	Purpose :					  Verifies whether the ListBox is Disabled.
'	Return Value :		 		  Integer( 0/1)
' 	Arguments :
'		Input Arguments :  		  objWinTree 
'		Output Arguments : 
'	Function is called by :
'	Function calls :			  CheckObjectExist() , CheckObjectStatus()
'	Created on :				  14/03/2008
'	Author :					   Rathna Reddy 
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function VerifyNodeExistingInHierarchy(ByVal objWinTree , Byval strNodeHierarchy, ByVal NodeElementName)
	On Error Resume Next
			Dim strMethod
	  		Dim blnstatus, blnMatch
			Dim strListContent, strTreelistItems
			Dim arrItems, arrDropItems, arrListContents
			Dim intmaxlength, intcount, intTemparrcount, intTreeItems
			Dim arrTreeListItems

			strListContent = Null
			blnstatus = false
			blnMatch = False
			'blnDropMatch = False
		'Variable Section End
			
			strMethod = strKeywordForReporting
			If CheckObjectExist(objWinTree) Then
				strListContent = objWinTree.GetContent

				If Vartype(strListContent) <> vbNull  And Err.Number = 0 Then
					blnstatus = true
				End If
					If blnstatus  Then
					arrItems = GetArray( strNodeHierarchy, "^")
					intTreeItems = Ubound(arrItems)

					arrListContents = GetArray(strListContent, chr(10))
					intmaxlength = ubound(arrListContents)
					
					For intcount = 0 to intmaxlength
							strTreelistItems = arrListContents(intcount)
							arrTreeListItems = GetArray(strTreelistItems, ";")
							  intTemparrcount = Ubound(arrTreeListItems)
								If (intTemparrcount-1) = intTreeItems Then
									ReDim Preserve arrItems(intTemparrcount)
									arrItems(intTemparrcount) = NodeElementName
									 If CompareArraysInSequence(arrItems, arrTreeListItems) = 0 Then
										  blnMatch = true
										 Exit for
									End If
								End If
					Next

									  If  blnMatch And Err.Number = 0 Then
											VerifyNodeExistingInHierarchy = 0
                                      Else'blnMatch
											VerifyNodeExistingInHierarchy = 1
											WriteStatusToLogs "The Item ['"&NodeElementName&"'] is not existing in Tree view."
										End If   		   
						Else'blnstatus
						VerifyNodeExistingInHierarchy = 1
						WriteStatusToLogs "An error occurred while Getting the Tree List nodes."
						End If
			Else
				VerifyNodeExistingInHierarchy = 1
				WriteStatusToLogs "The Tree View object does not exist, please verify."
			End If
		On Error GoTo 0
End Function



'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name :				 VerifyNodeExistingInHierarchy
' 	Purpose :					  Verifies whether the ListBox is Disabled.
'	Return Value :		 		  Integer( 0/1)
' 	Arguments :
'		Input Arguments :  		  objWinTree 
'		Output Arguments : 
'	Function is called by :
'	Function calls :			  CheckObjectExist() , CheckObjectStatus()
'	Created on :				  14/03/2008
'	Author :					   Rathna Reddy 
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function VerifyNodeNotExistingInHierarchy(ByVal objWinTree , Byval strNodeHierarchy, ByVal NodeElementName)
	On Error Resume Next
			Dim strMethod
	  		Dim blnstatus, blnMatch
			Dim strListContent, strTreelistItems
			Dim arrItems, arrDropItems, arrListContents
			Dim intmaxlength, intcount, intTemparrcount, intTreeItems
			Dim arrTreeListItems
			Dim intMatchCount

			strListContent = Null
			blnstatus = false
			blnMatch = False
			intMatchCount = 0
			'blnDropMatch = False
		'Variable Section End

			strMethod = strKeywordForReporting
			If CheckObjectExist(objWinTree) Then
				strListContent = objWinTree.GetContent

				If Vartype(strListContent) <> vbNull  And Err.Number = 0 Then
					blnstatus = true
				End If
					If blnstatus  Then
					arrItems = GetArray( strNodeHierarchy, "^")
					intTreeItems = Ubound(arrItems)

					arrListContents = GetArray(strListContent, chr(10))
					intmaxlength = ubound(arrListContents)
					
					For intcount = 0 to intmaxlength
							strTreelistItems = arrListContents(intcount)
							arrTreeListItems = GetArray(strTreelistItems, ";")
							  intTemparrcount = Ubound(arrTreeListItems)
								If (intTemparrcount-1) = intTreeItems Then
									ReDim Preserve arrItems(intTemparrcount)
									arrItems(intTemparrcount) = NodeElementName
									 If CompareArraysInSequence(arrItems, arrTreeListItems) = 0 Then
										  blnMatch = True
										  intMatchCount = intMatchCount+1
										  Exit for
										Else
										 blnMatch = False
									End If
								End If
					Next

									  If  intMatchCount = 0  And Err.Number = 0 Then
											VerifyNodeNotExistingInHierarchy = 0
                                      Else'blnMatch
											VerifyNodeNotExistingInHierarchy = 1
											WriteStatusToLogs "The Item ['"&NodeElementName&"'] is not existing in Tree view."
										End If   		   
						Else'blnstatus
						VerifyNodeNotExistingInHierarchy = 1
						WriteStatusToLogs "An error occurred while Getting the Tree List Nodes."
						End If
			Else
				VerifyNodeNotExistingInHierarchy = 1
				WriteStatusToLogs "The Tree View object does not exist, please verify."
			End If
		On Error GoTo 0
End Function


'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name :				 VerifyNodeExistingInHierarchy
' 	Purpose :					  Verifies whether the ListBox is Disabled.
'	Return Value :		 		  Integer( 0/1)
' 	Arguments :
'		Input Arguments :  		  objWinTree 
'		Output Arguments : 
'	Function is called by :
'	Function calls :			  CheckObjectExist() , CheckObjectStatus()
'	Created on :				  14/03/2008
'	Author :					   Rathna Reddy 
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function VerifyChildNodesExistingInParentNode(ByVal objWinTree , Byval strNodeHierarchy, ByVal ArrNodeElementName)
	On Error Resume Next
			Dim strMethod
	  		Dim blnstatus, blnMatch
			Dim strListContent, strTreelistItems
			Dim arrItems, arrDropItems, arrListContents, arrChildItems
			Dim intmaxlength, intcount, intTemparrcount, intTreeItems
			Dim arrTreeListItems
			Dim intfullPathCount, intExpCount, i, inttempExpCount

			strListContent = Null
			blnstatus = false
			blnMatch = False
			inttempExpCount = 0
			'blnDropMatch = False
		'Variable Section End

			strMethod = strKeywordForReporting
			If CheckObjectExist(objWinTree) Then
				strListContent = objWinTree.GetContent

				If Vartype(strListContent) <> vbNull  And Err.Number = 0 Then
					blnstatus = true
				End If
					If blnstatus  Then
					arrItems = GetArray(strNodeHierarchy, "^")
					arrChildItems = GetArray(ArrNodeElementName, "^")
					intTreeItems = Ubound(arrItems)
					intExpCount = Ubound(arrChildItems)+1

					arrListContents = GetArray(strListContent, chr(10))
					intmaxlength = ubound(arrListContents)
					
					For intcount = 0 to intmaxlength
							strTreelistItems = arrListContents(intcount)
							arrTreeListItems = GetArray(strTreelistItems, ";")
							  intTemparrcount = Ubound(arrTreeListItems)
							  intfullPathCount = intTreeItems+ intExpCount
								If (intTemparrcount) >= intTreeItems Then
									For i =  intTreeItems to intfullPathCount-1
										
										ReDim Preserve arrItems(i+1)
										arrItems(i+1) = arrChildItems(inttempExpCount)
										inttempExpCount = inttempExpCount+1
									Next
									inttempExpCount = 0
									 If CompareArraysInSequence(arrItems, arrTreeListItems) = 0 Then
										  blnMatch = true
										 Exit for
									End If
								End If
					Next

									  If  blnMatch And Err.Number = 0 Then
											VerifyChildNodesExistingInParentNode = 0
                                      Else'blnMatch
											VerifyChildNodesExistingInParentNode = 1
											WriteStatusToLogs "The Item ['"&NodeElementName&"'] is not existing in Tree view."
										End If   		   
						Else'blnstatus
						VerifyChildNodesExistingInParentNode = 1
						WriteStatusToLogs "An error occurred while Getting the Tree List Nodes."
						End If
			Else
				VerifyChildNodesExistingInParentNode = 1
				WriteStatusToLogs "The Tree View object does not exist, please verify."
			End If
		On Error GoTo 0
End Function

'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name :				 VerifyChildNodesNotExistingInParentNode
' 	Purpose :					  Verifies whether the ListBox is Disabled.
'	Return Value :		 		  Integer( 0/1)
' 	Arguments :
'		Input Arguments :  		  objWinTree 
'		Output Arguments : 
'	Function is called by :
'	Function calls :			  CheckObjectExist() , CheckObjectStatus()
'	Created on :				  14/03/2008
'	Author :					   Rathna Reddy 
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function VerifyChildNodesNotExistingInParentNode(ByVal objWinTree , Byval strNodeHierarchy, ByVal ArrNodeElementName)
	On Error Resume Next
			Dim strMethod
	  		Dim blnstatus, blnMatch
			Dim strListContent, strTreelistItems
			Dim arrItems, arrDropItems, arrListContents, arrChildItems
			Dim intmaxlength, intcount, intTemparrcount, intTreeItems
			Dim arrTreeListItems
			Dim intfullPathCount, intExpCount, i, inttempExpCount, intMatchCount

			strListContent = Null
			blnstatus = false
			blnMatch = False
			inttempExpCount = 0
			intMatchCount = 0
			'blnDropMatch = False
		'Variable Section End

			strMethod = strKeywordForReporting
			If CheckObjectExist(objWinTree) Then
				strListContent = objWinTree.GetContent

				If Vartype(strListContent) <> vbNull  And Err.Number = 0 Then
					blnstatus = true
				End If
					If blnstatus  Then
					arrItems = GetArray(strNodeHierarchy, "^")
					arrChildItems = GetArray(ArrNodeElementName, "^")
					intTreeItems = Ubound(arrItems)
					intExpCount = Ubound(arrChildItems)+1

					arrListContents = GetArray(strListContent, chr(10))
					intmaxlength = ubound(arrListContents)
					
					For intcount = 0 to intmaxlength
							strTreelistItems = arrListContents(intcount)
							arrTreeListItems = GetArray(strTreelistItems, ";")
							  intTemparrcount = Ubound(arrTreeListItems)
							  intfullPathCount = intTreeItems+ intExpCount
								If (intTemparrcount) >= intTreeItems Then
									For i =  intTreeItems to intfullPathCount-1
										
										ReDim Preserve arrItems(i+1)
										arrItems(i+1) = arrChildItems(inttempExpCount)
										inttempExpCount = inttempExpCount+1
									Next
									inttempExpCount = 0
									If CompareArraysInSequence(arrItems, arrTreeListItems) = 0 Then
										  blnMatch = True
										  intMatchCount = intMatchCount+1
										  Exit for
										Else
										 blnMatch = False
									End If
								End If
					Next

									  If  intMatchCount = 0  And Err.Number = 0 Then
											VerifyChildNodesNotExistingInParentNode = 0
                                      Else'blnMatch
											VerifyChildNodesNotExistingInParentNode = 1
											WriteStatusToLogs "The Item ['"&NodeElementName&"'] is not existing in Tree view."
										End If   		   
						Else'blnstatus
						VerifyChildNodesNotExistingInParentNode = 1
						WriteStatusToLogs "An error occurred while Getting the Tree List Nodes."
						End If
			Else
				VerifyChildNodesNotExistingInParentNode = 1
				WriteStatusToLogs "The Tree View object does not exist, please verify."
			End If
		On Error GoTo 0
End Function


'------------------------------------------------------------------------------------------------------------------------------------------
Function VerifyNodeSelectedWTV(ByVal objWinTree , Byval strNodeHierarchy)
	On Error Resume Next
		'Variable Section Begin
			Dim strMethod
			Dim blnstatus, blnMatch
			Dim strListContent, strTreelistItems
			Dim arrItems, arrDropItems, arrListContents
			Dim intmaxlength, intcount
			Dim arrTreeListItems
			Dim intTemparrcount, intTreeItems
			Dim strTempSelectedNode

			strListContent = Null
			blnstatus = false
			blnMatch = False
			'blnDropMatch = False
		'Variable Section End

			strMethod = strKeywordForReporting
			If CheckObjectExist(objWinTree) Then
				strListContent = objWinTree.GetContent

				If Vartype(strListContent) <> vbNull  And Err.Number = 0 Then
					blnstatus = true
				End If
					If blnstatus  Then
					arrItems = GetArray( strNodeHierarchy, "^")
					intTreeItems = Ubound(arrItems)

					arrListContents = GetArray(strListContent, chr(10))
					intmaxlength = ubound(arrListContents)
					
					For intcount = 0 to intmaxlength
							strTreelistItems = arrListContents(intcount)
							arrTreeListItems = GetArray(strTreelistItems, ";")
							  intTemparrcount = Ubound(arrTreeListItems)
								If intTemparrcount = intTreeItems Then
									 If CompareArraysInSequence(arrItems, arrTreeListItems) = 0 Then
										  blnMatch = true
										 Exit for
									End If
								End If
					Next

									  If  blnMatch Then
												strTempSelectedNode = objWinTree.GetSelection
													If CompareStringValues(strTreelistItems,strTempSelectedNode ) = 0 And Err.Number = 0 Then
                                                    VerifyNodeSelectedWTV = 0
                                                   Else
													  VerifyNodeSelectedWTV = 1
															WriteStatusToLogs "The Node is not Selected or focused."
												End If
							
		
			
											Else'blnMatch
											VerifyNodeSelectedWTV = 1
											WriteStatusToLogs "The Expand Items are not existing."
										End If   		   
						Else'blnstatus
						VerifyNodeSelectedWTV = 1
						WriteStatusToLogs "An error occurred while Getting the Tree List Nodes."
						End If
			Else
				VerifyNodeSelectedWTV = 1
				WriteStatusToLogs "The Tree View object does not exist, please verify."
			End If
		On Error GoTo 0
End Function


'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :				 VerifyTreeViewVisible()
'	Purpose :					 Verifies whether the EditBox is visible.
'	Return Value :		 		  Integer( 0/1)
'	Arguments :
'		Input Arguments :  		  objWinTree 
'		Output Arguments : 
'	Function is called by :
'	Function calls :			 CheckObjectExist() , CheckObjectVisiblity()
'	Created on :				   10/03/2008
'	Author :						  Rathna Reddy 
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function VerifyTreeViewVisible(ByVal objWinTree)
	On Error Resume Next
		'Variable Section Begin
		
			Dim strMethod

		'Variable Section End
			strMethod = strKeywordForReporting
			If CheckObjectExist(objWinTree)  Then 
				If CheckObjectVisible(objWinTree) Then
					VerifyTreeViewVisible = 0
					WriteStatusToLogs "The Tree View was visible as expected."
				Else
					VerifyTreeViewVisible = 2
					WriteStatusToLogs "The Tree View was not visible,Please verify."
				End If
			Else
					VerifyTreeViewVisible = 1
					WriteStatusToLogs "The Tree View does not exist, please verify."
			End If
		
	On Error GoTo 0
End Function
'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :				 VerifyEditBoxInVisible()
'	Purpose :					 Verifies whether the EditBox is Invisible.
'	Return Value :		 		  Integer( 0/1)
'	Arguments :
'		Input Arguments :  		  objWinTree 
'		Output Arguments : 
'	Function is called by :
'	Function calls :			 CheckObjectExist() , CheckObjectVisiblity()
'	Created on :				   10/03/2008
'	Author :						  Rathna Reddy 
'
'------------------------------------------------------------------------------------------------------------------------------------------

Function VerifyTreeViewInVisible(ByVal objWinTree)
	On Error Resume Next
		'Variable Section Begin
		
			Dim strMethod

		'Variable Section End
			strMethod = strKeywordForReporting
			If CheckObjectExist(objWinTree)  Then 
				If CheckObjectInVisible(objWinTree) Then
					VerifyTreeViewInVisible = 0
					WriteStatusToLogs "The EditBox was in-visible as expected."
				Else
					VerifyTreeViewInVisible = 2
					WriteStatusToLogs "The EditBox was not in-visible, please verify."
				End If
			Else
					VerifyTreeViewInVisible = 1
					WriteStatusToLogs "The EditBox does not exist, please verify."
			End If
		
	On Error GoTo 0
End Function


'=======================Code Section End=============================================
