

'=======================Code Section Begin=============================================

Option Explicit		'Forces explicit declaration of all variables in a script. 


'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name :				 SetValueInEditBox()
' 	Purpose :						 Enters a value into editbox
' 	Return Value :		 		   Integer( 0/1)
' 	Arguments :
'		Input Arguments :  		objEditbox , strValue
'		Output Arguments : 
'	Function is called by :
'	Function calls :			    CheckObjectExist()
'	Created on :				   12/03/2008
'	Author :						 Rathna Reddy 
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function SetValueInWinEditBox(ByVal objEditbox , ByVal strValue)
	On Error Resume Next
		'Variable Section Begin
			
				Dim strMethod

		'Variable Section End
				strMethod = strKeywordForReporting
			   If CheckObjectExist(objEditbox) Then
					If CheckObjectEnabled(objEditBox) Then
						objEditbox.Set strValue
						 If err.Number = 0 Then
							 SetValueInWinEditBox = 0
								WriteStatusToLogs "The value was set in EditBox Sucessfully."
							Else
								SetValueInWinEditBox = 1
								WriteStatusToLogs "The value was not set in EditBox, please verify."
						  
							End If
					Else
					SetValueInWinEditBox = 1
					WriteStatusToLogs "The EditBox was not Enabled, please verify."


					End If
				  
				Else
						SetValueInWinEditBox = 1
						WriteStatusToLogs "The EditBox does not exist, please verify."
				End If
	On Error GoTo 0
End Function

'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name :				VerifyEditBoxValue()
' 	Purpose :					 Verifies the editbox value with our expected whenever required
' 	Return Value :		 		  Integer( 0/1)
' 	Arguments :
'		Input Arguments :  		objEditbox,strExpValue
'		Output Arguments :     strActValue
'	Function is called by :
'	Function calls :				CheckObjectExist(),CompareStringValues()
'	Created on :				   12/03/2008
'	Author :						 Rathna Reddy 
'Note: This is not default value verification.
'------------------------------------------------------------------------------------------------------------------------------------------
Function VerifyWinEditBoxValue(ByVal objEditbox , ByVal strExpValue)
	On Error Resume Next
		 'Variable Section Begin
			Dim strMethod
			Dim strActValue, blnstatus 
			blnstatus  = False

		'Variable Section End
			strMethod = strKeywordForReporting
			If CheckObjectExist(objEditbox) Then  
				strActValue = objEditbox.GetROProperty("text")
				
				If Vartype(intActData) <> vbEmpty  And Err.Number = 0 Then
					blnstatus = true
				End If
					If blnstatus  Then
								If CompareStringValues(strExpValue,strActValue)  = 0 Then
									VerifyWinEditBoxValue = 0
									WriteStatusToLogs """"&"Expected Value : "&strExpValue& """" &  " and " &""""&"Actual Value : "&strActValue&""""&" matches as expected."
		
								Else
									VerifyWinEditBoxValue = 2
									WriteStatusToLogs """"&"Expected Value : "&strExpValue& """" &  " and " &""""&"Actual Value : "&strActValue&""""&" does not match, please verify."
		
								End If
					Else
							VerifyWinEditBoxValue = 1
							WriteStatusToLogs "An error occurred while capturing edit box value."
					End If
		Else
				VerifyWinEditBoxValue = 1
				 WriteStatusToLogs 	"EditBox does not exist, please verify."
				
		 End If
			'WriteStepResults CompareEditBoxValue
	On Error GOTO 0
End Function
'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name :				 VerifyEditBoxValueLength()
' 	Purpose :						Verifies the both minimum and maximum length of editbox Data
' 	Return Value :		 		  Integer( 0/1)
' 	Arguments :
'		Input Arguments :  	    objEditbox , intMaxLength , intMinLength
'		Output Arguments :       intLength, strValue
'	Function is called by :
'	Function calls :				CheckObjectExist()
'	Created on :				   12/03/2008
'	Author :						 Rathna Reddy 
'' 
'Note: if testcase does't specify the minimum length then Give "1"
'------------------------------------------------------------------------------------------------------------------------------------------
Function VerifyWinEditBoxValueLength(ByVal objEditbox, ByVal intMaxLength , ByVal intMinLength)
	On Error Resume Next
		 'Variable Section Begin
			Dim strMethod
			Dim intLength, strValue,intMaxLen,intMinLen

		'Variable Section End
		strMethod = strKeywordForReporting
			If CheckObjectExist(objEditbox) Then  
			intMaxLen=Cdbl(intMaxLength)
			intMinLen=Cdbl(intMinLength)

					If IsInteger(intMaxLen) = 0 Then

							If IsInteger(intMinLen) = 0 Then
										 If intMinLen<intMaxLen Then
											strValue= objEditbox.GetROProperty("text")
														If err.Number = 0 Then
															 intLength = Len(strValue)
																	If  intLength <= intMaxLen And intLength >= intMinLen Then
																		VerifyWinEditBoxValueLength = 0
																		WriteStatusToLogs "The Edit Box value length "&""""&intLength&""""&" is in between "&""""&intMaxLen&""""&" and " &""""&intMinLen&""""
																	Else
																		 VerifyWinEditBoxValueLength = 2
																			WriteStatusToLogs "The Edit Box value length "&""""&intLength&""""&" is not  in between "&""""&intMaxLen&""""&" and " &""""&intMinLen&""""
												
																	End If
															Else
																VerifyWinEditBoxValueLength = 1
																WriteStatusToLogs "Action fails to get EditBox value, please verify."
								
															End If
						
												Else
													VerifyWinEditBoxValueLength = 2
													WriteStatusToLogs "The Minium length Should be less then Maximum Length."
												End If
								Else'IsStringContainsNumericsOnly
									VerifyWinEditBoxValueLength = 1
									WriteStatusToLogs "The Given Min lengh is not an numeric value. Please enter only numeric value."
								End If 'IsStringContainsNumericsOnly

					Else'IsStringContainsNumericsOnly
						VerifyWinEditBoxValueLength = 1
						WriteStatusToLogs "The Given Max lengh is not an Numeric value. Please enter only numeric value."
					End If 'IsStringContainsNumericsOnly
            
			Else 'check object existance
				VerifyWinEditBoxValueLength = 1
				WriteStatusToLogs "The Edit Box does not exist, please verify."
								
		End If ' check object existance
	   
			'WriteStepResults VerifyEditBoxValueLength
	On Error GoTo 0
End Function


'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name :				 VerifyNumericDataInEditBox()
' 	Purpose :						Verifies whether the editbox allows the numeric data by avoiding string data.
' Possible values for Arguement:
'   			strFlags:  "TRUE" or "FALSE"
'				Purpose of strFlag: if the strFlag is "TRUE" then whether the edit box can allow the numeric values. or else you are checking the edit box validation like for validating numeric data.
' 	Return Value :		 		  Integer(0/1/2)
' 	Arguments :
'		Input Arguments :  		objEditbox	
'		Output Arguments : 		intValue
'	Function is called by :
'	Function calls :				CheckObjectExist()
'	Created on :				   12/03/2008
'	Author :						 Rathna Reddy 
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function VerifyNumericDataInWinEditBox(ByVal objEditbox)
	On Error Resume Next
		 'Variable Section Begin
			  Dim strMethod
			  Dim intValue, blnstatus
			  blnstatus = False

			'Variable Section End
			strMethod = strKeywordForReporting
				 If CheckObjectExist(objEditbox) Then   
                    
						 intValue = objEditbox.GetRoProperty("text")
						 
						 If Vartype(intValue)<> Vbempty And Err.Number =0 Then
							 blnstatus = True
						 End If
                         
						 If blnstatus Then
									If intValue <> ""  Then
								 
													If IsNumeric(Trim(intValue)) Then
													
														 VerifyNumericDataInWinEditBox = 0
														 WriteStatusToLogs "EditBox contains Numberic value as expected."
													 Else
														 VerifyNumericDataInWinEditBox = 2  
														 WriteStatusToLogs "EditBox does not contain Numeric value, please verify."
													 End If
									Else
											VerifyNumericDataInWinEditBox = 2  
											WriteStatusToLogs "EditBox does not contain Any value, please verify."
									End IF
                                 
					Else
						VerifyNumericDataInWinEditBox = 1
						WriteStatusToLogs "An error occurred while capturing Edit box value."
					End if								 
		 

				Else
						VerifyNumericDataInWinEditBox = 1
						WriteStatusToLogs "EditBox does not exist, please verify."
				End If
	On Error GoTo 0
End Function

'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name :				 VerifyEditBoxEnabled()
' 	Purpose :					 Verifies whether the EditBox is Enabled
' 	Return Value :		 		  Integer( 0/1)
' 	Arguments :
'		Input Arguments :  		objEditBox 
'		Output Arguments : 
'	Function is called by :
'	Function calls :				CheckObjectExist() , CheckObjectStatus()
'	Created on :				   11/03/2008
'	Author :						  Rathna Reddy 
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function VerifyWinEditBoxEnabled(ByVal objEditBox)

On Error Resume Next
	'Variable Section Begin
		
		Dim strMethod

	'Variable Section End
		strMethod = strKeywordForReporting
		If CheckObjectExist(objEditBox) Then
			If CheckObjectEnabled(objEditBox) Then
					VerifyWinEditBoxEnabled = 0
					WriteStatusToLogs "The EditBox was enabled as expected."
				Else
					VerifyWinEditBoxEnabled = 2
					WriteStatusToLogs "The EditBox was not enabled, please verify."
				End If
            
		Else
				VerifyWinEditBoxEnabled = 1
				WriteStatusToLogs "The EditBox does not exist, please verify."
		End if
		
	On Error GoTo 0
    
End Function

'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name :				 VerifyEditBoxDisabled()
' 	Purpose :					 Verifies whether the EditBox is Disabled
' 	Return Value :		 		  Integer( 0/1)
' 	Arguments :
'		Input Arguments :  		objEditBox 
'		Output Arguments : 
'	Function is called by :
'	Function calls :				CheckObjectExist() , CheckObjectStatus()
'	Created on :				   11/03/2008
'	Author :						  Rathna Reddy 
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function VerifyWinEditBoxDisabled(ByVal objEditBox)

On Error Resume Next
	'Variable Section Begin
		
		Dim strMethod

	'Variable Section End
		strMethod = strKeywordForReporting
		If CheckObjectExist(objEditBox) Then
			If CheckObjectDisabled(objEditBox) Then
					VerifyWinEditBoxDisabled = 0
					WriteStatusToLogs "The EditBox was disabled as expected."
				Else
					VerifyWinEditBoxDisabled = 2
					WriteStatusToLogs "The EditBox was not disabled, please verify."
				End If
            
		Else
				VerifyWinEditBoxDisabled = 1
				WriteStatusToLogs "The EditBox does not exist, please verify."
		End if
	On Error GoTo 0
End Function

'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :				 VerifyEditBoxVisible()
'	Purpose :					 Verifies whether the EditBox is visible.
'	Return Value :		 		  Integer( 0/1)
'	Arguments :
'		Input Arguments :  		  objEditBox 
'		Output Arguments : 
'	Function is called by :
'	Function calls :			 CheckObjectExist() , CheckObjectVisiblity()
'	Created on :				   10/03/2008
'	Author :						  Rathna Reddy 
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function VerifyWinEditBoxVisible(ByVal objEditBox)
	On Error Resume Next
		'Variable Section Begin
		
			Dim strMethod

		'Variable Section End
			strMethod = strKeywordForReporting
			If CheckObjectExist(objEditBox)  Then 
				If CheckObjectVisible(objEditBox) Then
					VerifyWinEditBoxVisible = 0
					WriteStatusToLogs "The EditBox was visible as expected."
				Else
					VerifyWinEditBoxVisible = 2
					WriteStatusToLogs "The EditBox was not visible,Please verify."
				End If
			Else
					VerifyWinEditBoxVisible = 1
					WriteStatusToLogs "The EditBox does not exist, please verify."
			End If
		
	On Error GoTo 0
End Function
'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :				 VerifyEditBoxInVisible()
'	Purpose :					 Verifies whether the EditBox is Invisible.
'	Return Value :		 		  Integer( 0/1)
'	Arguments :
'		Input Arguments :  		  objEditBox 
'		Output Arguments : 
'	Function is called by :
'	Function calls :			 CheckObjectExist() , CheckObjectVisiblity()
'	Created on :				   10/03/2008
'	Author :						  Rathna Reddy 
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function VerifyWinEditBoxInVisible(ByVal objEditBox)
	On Error Resume Next
		'Variable Section Begin
		
			Dim strMethod

		'Variable Section End
			strMethod = strKeywordForReporting
			If CheckObjectExist(objEditBox)  Then 
				If CheckObjectInVisible(objEditBox) Then
					VerifyWinEditBoxInVisible = 0
					WriteStatusToLogs "The EditBox was in-visible as expected."
				Else
					VerifyWinEditBoxInVisible = 2
					WriteStatusToLogs "The EditBox was not in-visible, please verify."
				End If
			Else
					VerifyWinEditBoxInVisible = 1
					WriteStatusToLogs "The EditBox does not exist, please verify."
			End If
		
	On Error GoTo 0
End Function
'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :				 VerifyEditBoxExist()
'	Purpose :					 Verifies whether the EditBox Exists or not?
'	Return Value :		 		  Integer( 0/1)
'	Arguments :
'		Input Arguments :  		  objEditBox 
'		Output Arguments : 
'	Function is called by :
'	Function calls :			 CheckObjectExist() 
'	Created on :				   10/03/2008
'	Author :						  Rathna Reddy 
'
'------------------------------------------------------------------------------------------------------------------------------------------

Function VerifyWinEditBoxExist(ByVal objEditBox)
	On Error Resume Next
		'Variable Section Begin
	
		 Dim strMethod

		'Variable Section End
			strMethod = strKeywordForReporting
			If CheckObjectExist(objEditBox) Then 
			   
					VerifyWinEditBoxExist = 0
					WriteStatusToLogs "The EditBox exists as expected."
					
			Else
					VerifyWinEditBoxExist = 2
					WriteStatusToLogs "The EditBox does NOT exist, please verify."
					
			End If 
	  
	
	On Error GoTo 0
End Function

'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :				 VerifyEditBoxNotExist()
'	Purpose :					 Verifies whether the EditBox Exists or not?
'	Return Value :		 		  Integer( 0/1)
'	Arguments :
'		Input Arguments :  		  objEditBox 
'		Output Arguments : 
'	Function is called by :
'	Function calls :			 CheckObjectExist() 
'	Created on :				   10/03/2008
'	Author :						  Rathna Reddy 
'
'------------------------------------------------------------------------------------------------------------------------------------------

Function VerifyWinEditBoxNotExist(ByVal objEditBox)
	On Error Resume Next
		'Variable Section Begin
	
		   Dim strMethod

		'Variable Section End
			strMethod = strKeywordForReporting
			If CheckObjectNotExist(objEditBox) Then 
			   
					VerifyWinEditBoxNotExist = 0
					WriteStatusToLogs "The Edit Box does not exist as expected."
					
			Else
					VerifyWinEditBoxNotExist = 2
					WriteStatusToLogs "The Edit Box exists, please verify."
					
			End If 
	
  		
	On Error GoTo 0
End Function
'=======================Code Section End=============================================
