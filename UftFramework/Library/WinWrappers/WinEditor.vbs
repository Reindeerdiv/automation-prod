

'=======================Code Section Begin=============================================

Option Explicit		'Forces explicit declaration of all variables in a script. 


'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name :				 TypeTextInEditor()
' 	Purpose :						 Enters a value into editbox
' 	Return Value :		 		   Integer( 0/1)
' 	Arguments :
'		Input Arguments :  		objEditor , strValue
'		Output Arguments : 
'	Function is called by :
'	Function calls :			    CheckObjectExist()
'	Created on :				   12/03/2008
'	Author :						 Rathna Reddy 
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function TypeTextInEditor(ByVal objEditor , ByVal strText)
	On Error Resume Next
		'Variable Section Begin
			
				Dim strMethod

		'Variable Section End
			strMethod = strKeywordForReporting
			   If CheckObjectExist(objEditor) Then
					If CheckObjectEnabled(objEditor) Then
						objEditor.type strText
						 If err.Number = 0 Then
							 TypeTextInEditor = 0
								WriteStatusToLogs "The value was set in Editor Sucessfully."
							Else
								TypeTextInEditor = 1
								WriteStatusToLogs "The value was not set in Editor, please verify."
						  
							End If
					Else
					TypeTextInEditor = 1
					WriteStatusToLogs "The Editor was not Enabled, please verify."


					End If
				  
				Else
						TypeTextInEditor = 1
						WriteStatusToLogs "The Editor does not exist, please verify."
				End If
	On Error GoTo 0
End Function

'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name :				VerifyEditorText()
' 	Purpose :					 Verifies the editbox value with our expected whenever required
' 	Return Value :		 		  Integer( 0/1)
' 	Arguments :
'		Input Arguments :  		objEditor,strExpValue
'		Output Arguments :     strActValue
'	Function is called by :
'	Function calls :				CheckObjectExist(),CompareStringValues()
'	Created on :				   12/03/2008
'	Author :						 Rathna Reddy 
'Note: This is not default value verification.
'------------------------------------------------------------------------------------------------------------------------------------------
Function VerifyEditorText(ByVal objEditor , ByVal strExpText)
	On Error Resume Next
		 'Variable Section Begin
			Dim strMethod
			Dim strActValue, blnstatus 
			blnstatus  = False

		'Variable Section End
			strMethod = strKeywordForReporting
			If CheckObjectExist(objEditor) Then  
				strActValue = objEditor.GetROProperty("text")
				
				If Vartype(strActValue) <> vbEmpty  And Err.Number = 0 Then
					blnstatus = true
				End If
					If blnstatus  Then
								If CompareStringValues(strExpText,strActValue)  = 0 Then
									VerifyEditorText = 0
									WriteStatusToLogs """"&"Expected Value : "&strExpValue& """" &  " and " &""""&"Actual Value : "&strActValue&""""&" matches as expected."
		
								Else
									VerifyEditorText = 2
									WriteStatusToLogs """"&"Expected Value : "&strExpValue& """" &  " and " &""""&"Actual Value : "&strActValue&""""&" does not match, please verify."
		
								End If
					Else
							VerifyEditorText = 1
							WriteStatusToLogs "An error occurred while capturing edit box value."
					End If
		Else
				VerifyEditorText = 1
				 WriteStatusToLogs 	"EditBox does not exist, please verify."
				
		 End If
			'WriteStepResults CompareEditBoxValue
	On Error GOTO 0
End Function
'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name :				 VerifyEditBoxValueLength()
' 	Purpose :						Verifies the both minimum and maximum length of editbox Data
' 	Return Value :		 		  Integer( 0/1)
' 	Arguments :
'		Input Arguments :  	    objEditor , intMaxLength , intMinLength
'		Output Arguments :       intLength, strValue
'	Function is called by :
'	Function calls :				CheckObjectExist()
'	Created on :				   12/03/2008
'	Author :						 Rathna Reddy 
'' 
'Note: if testcase does't specify the minimum length then Give "1"
'------------------------------------------------------------------------------------------------------------------------------------------
Function VerifyEditorMaxValueLength(ByVal objEditor, ByVal intMaxLength)
	On Error Resume Next
		 'Variable Section Begin
		Dim strMethod
		Dim intLength, strValue,intMaxLen,intMinLen
		'Variable Section End
			strMethod = strKeywordForReporting
			If CheckObjectExist(objEditor) Then  
			intMaxLen=Cdbl(intMaxLength)
					If IsInteger(intMaxLen) = 0 Then
                        strValue= objEditor.GetROProperty("text")
                        If err.Number = 0 Then
                            intLength = Len(strValue)
										If  intLength <= intMaxLen  Then
											VerifyEditorMaxValueLength = 0
											WriteStatusToLogs "The Editor value length "&""""&intLength&""""&" is in between max length:"&""""&intMaxLen&""""
											
											Else
											 VerifyEditorMaxValueLength = 2
											 WriteStatusToLogs "The Editor value length "&""""&intLength&""""&" is not in between max length:"&""""&intMaxLen&""""
											End If
                                Else
								VerifyEditorMaxValueLength = 1
								WriteStatusToLogs "Action fails to get Editor text, please verify."

								End If
						
			 
					Else'IsStringContainsNumericsOnly
						VerifyEditorMaxValueLength = 1
						WriteStatusToLogs "The Given Max lengh is not a numeric value. please enter only numeric value."
					End If 'IsStringContainsNumericsOnly
            
			Else 'check object existance
				VerifyEditorMaxValueLength = 1
				WriteStatusToLogs "The Editor does not exist, please verify."
								
		End If ' check object existance
 On Error GoTo 0
End Function

'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name :				 VerifyEditorMinValueLength()
' 	Purpose :						Verifies the both minimum and maximum length of editbox Data
' 	Return Value :		 		  Integer( 0/1)
' 	Arguments :
'		Input Arguments :  	    objEditor , intMinLen , intMinLength
'		Output Arguments :       intLength, strValue
'	Function is called by :
'	Function calls :				CheckObjectExist()
'	Created on :				   12/03/2008
'	Author :						 Rathna Reddy 
'' 
'Note: if testcase does't specify the minimum length then Give "1"
'------------------------------------------------------------------------------------------------------------------------------------------
Function VerifyEditorMinValueLength(ByVal objEditor, ByVal intMinLength)
	On Error Resume Next
		 'Variable Section Begin
		 Dim strMethod
		Dim intLength, strValue,intMinLen
		'Variable Section End
			strMethod = strKeywordForReporting
			If CheckObjectExist(objEditor) Then  
			intMinLen=Cdbl(intMinLength)
					If IsInteger(intMinLen) = 0 Then
                        strValue= objEditor.GetROProperty("text")
                        If err.Number = 0 Then
                            intLength = Len(strValue)
										If  intLength >= intMinLen  Then
											VerifyEditorMinValueLength = 0
											WriteStatusToLogs "The Editor value length "&""""&intLength&""""&" is greater than or equal to min length:"&""""&intMinLength&""""
											
											Else
											 VerifyEditorMinValueLength = 2
											 WriteStatusToLogs "The Editor value length "&""""&intLength&""""&" is not greater than or equal to min length:"&""""&intMinLength&""""
											End If
                                Else
								VerifyEditorMinValueLength = 1
								WriteStatusToLogs "Action fails to get Editor text, please verify."

								End If
						
			 
					Else'IsStringContainsNumericsOnly
						VerifyEditorMinValueLength = 1
						WriteStatusToLogs "The Given Max lengh is not a numeric value. please enter only numeric value."
					End If 'IsStringContainsNumericsOnly
            
			Else 'check object existance
				VerifyEditorMinValueLength = 1
				WriteStatusToLogs "The Editor does not exist, please verify.."
								
		End If ' check object existance
 On Error GoTo 0
End Function

'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name :				 VerifyEditorEnabled()
' 	Purpose :					 Verifies whether the Editor is Enabled
' 	Return Value :		 		  Integer( 0/1)
' 	Arguments :
'		Input Arguments :  		objEditor 
'		Output Arguments : 
'	Function is called by :
'	Function calls :				CheckObjectExist() , CheckObjectStatus()
'	Created on :				   11/03/2008
'	Author :						  Rathna Reddy 
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function VerifyEditorEnabled(ByVal objEditor)

On Error Resume Next
	'Variable Section Begin
		
		Dim strMethod

	'Variable Section End
		strMethod = strKeywordForReporting
		If CheckObjectExist(objEditor) Then
			If CheckObjectEnabled(objEditor) Then
					VerifyEditorEnabled = 0
					WriteStatusToLogs "The Editor was enabled as expected."
				Else
					VerifyEditorEnabled = 2
					WriteStatusToLogs "The Editor was not enabled, please verify."
				End If
            
		Else
				VerifyEditorEnabled = 1
				WriteStatusToLogs "The Editor does not exist, please verify."
		End if
		
	On Error GoTo 0
    
End Function

'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name :				 VerifyEditorDisabled()
' 	Purpose :					 Verifies whether the Editor is Disabled
' 	Return Value :		 		  Integer( 0/1)
' 	Arguments :
'		Input Arguments :  		objEditor 
'		Output Arguments : 
'	Function is called by :
'	Function calls :				CheckObjectExist() , CheckObjectStatus()
'	Created on :				   11/03/2008
'	Author :						  Rathna Reddy 
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function VerifyEditorDisabled(ByVal objEditor)

On Error Resume Next
	'Variable Section Begin
		
		Dim strMethod

	'Variable Section End
		strMethod = strKeywordForReporting
		If CheckObjectExist(objEditor) Then
			If CheckObjectDisabled(objEditor) Then
					VerifyEditorDisabled = 0
					WriteStatusToLogs "The Editor was disabled as expected."
				Else
					VerifyEditorDisabled = 2
					WriteStatusToLogs "The Editor was not disabled, please verify."
				End If
            
		Else
				VerifyEditorDisabled = 1
				WriteStatusToLogs "The Editor does not exist, please verify."
		End if
	On Error GoTo 0
End Function

'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :				 VerifyEditBoxVisible()
'	Purpose :					 Verifies whether the EditBox is visible.
'	Return Value :		 		  Integer( 0/1)
'	Arguments :
'		Input Arguments :  		  objEditor 
'		Output Arguments : 
'	Function is called by :
'	Function calls :			 CheckObjectExist() , CheckObjectVisiblity()
'	Created on :				   10/03/2008
'	Author :						  Rathna Reddy 
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function VerifyEditorVisible(ByVal objEditor)
	On Error Resume Next
		'Variable Section Begin
		
			Dim strMethod

		'Variable Section End
			strMethod = strKeywordForReporting
			If CheckObjectExist(objEditor)  Then 
				If CheckObjectVisible(objEditor) Then
					VerifyEditorVisible = 0
					WriteStatusToLogs "The Editor was visible as expected."
				Else
					VerifyEditorVisible = 2
					WriteStatusToLogs "The Editor was not visible,Please verify."
				End If
			Else
					VerifyEditorVisible = 1
					WriteStatusToLogs "The Editor does not exist, please verify."
			End If
		
	On Error GoTo 0
End Function
'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :				 VerifyEditorInVisible()
'	Purpose :					 Verifies whether the Editor is Invisible.
'	Return Value :		 		  Integer( 0/1)
'	Arguments :
'		Input Arguments :  		  objEditor 
'		Output Arguments : 
'	Function is called by :
'	Function calls :			 CheckObjectExist() , CheckObjectVisiblity()
'	Created on :				   10/03/2008
'	Author :						  Rathna Reddy 
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function VerifyEditorInVisible(ByVal objEditor)
	On Error Resume Next
		'Variable Section Begin
		
			Dim strMethod

		'Variable Section End
			strMethod = strKeywordForReporting
			If CheckObjectExist(objEditor)  Then 
				If CheckObjectInVisible(objEditor) Then
					VerifyEditorInVisible = 0
					WriteStatusToLogs "The Editor was in-visible as expected."
				Else
					VerifyEditorInVisible = 2
					WriteStatusToLogs "The Editor was not in-visible, please verify."
				End If
			Else
					VerifyEditorInVisible = 1
					WriteStatusToLogs "The Editor does not exist, please verify."
			End If
		
	On Error GoTo 0
End Function
'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :				 VerifyEditorExist()
'	Purpose :					 Verifies whether the Editor Exists or not?
'	Return Value :		 		  Integer( 0/1)
'	Arguments :
'		Input Arguments :  		  objEditor 
'		Output Arguments : 
'	Function is called by :
'	Function calls :			 CheckObjectExist() 
'	Created on :				   10/03/2008
'	Author :						  Rathna Reddy 
'
'------------------------------------------------------------------------------------------------------------------------------------------

Function VerifyEditorExist(ByVal objEditor)
	On Error Resume Next
		'Variable Section Begin
	
		   Dim strMethod

		'Variable Section End
			strMethod = strKeywordForReporting
			If CheckObjectExist(objEditor) Then 
			   
					VerifyEditorExist = 0
					WriteStatusToLogs "The Editor exists as expected."
					
			Else
					VerifyEditorExist = 2
					WriteStatusToLogs "The Editor does NOT exist, please verify."
					
			End If 
	  
	
	On Error GoTo 0
End Function

'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :				 VerifyEditorNotExist()
'	Purpose :					 Verifies whether the Editor Exists or not?
'	Return Value :		 		  Integer( 0/1)
'	Arguments :
'		Input Arguments :  		  objEditor 
'		Output Arguments : 
'	Function is called by :
'	Function calls :			 CheckObjectExist() 
'	Created on :				   10/03/2008
'	Author :						  Rathna Reddy 
'
'------------------------------------------------------------------------------------------------------------------------------------------

Function VerifyEditorNotExist(ByVal objEditor)
	On Error Resume Next
		'Variable Section Begin
	
		   Dim strMethod

		'Variable Section End
			strMethod = strKeywordForReporting
			If CheckObjectNotExist(objEditor) Then 
			   
					VerifyEditorNotExist = 0
					WriteStatusToLogs "The Edit Box does not exist as expected."
					
			Else
					VerifyEditorNotExist = 2
					WriteStatusToLogs "The Edit Box exists, please verify."
					
			End If 
	
  		
	On Error GoTo 0
End Function
'=======================Code Section End=============================================
