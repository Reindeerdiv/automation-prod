 Option Explicit		 'Forces explicit declaration of all variables in a script.
'=======================Code Section Begin=============================================


'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name :				  SelectRadioButton()
' 	Purpose :						 Selects a specified RadioButton from the RadioGroup
' 	Return Value :		 		   Integer( 0/1)
' 	Arguments :
'		Input Arguments :  		   objRadioButton
'		Output Arguments :
'	Function is called by :
'	Function calls :				CheckObjectExist()
'	Created on :				   18/03/2008
'	Author :						  Rathna Reddy
' Note:unselectRedioButton is no need,if we select one radiobutton,the ramaining automatically in unselectstate
'------------------------------------------------------------------------------------------------------------------------------------------
Function SelectWinRadioButton(Byval objRadioButton)
	On Error Resume Next
	'Variable Section Begin
		
			Dim strMethod

		'Variable Section End
			strMethod = strKeywordForReporting
			If CheckObjectExist(objRadioButton) Then
				objRadioButton.Set'it is "value " property of radio button
					If err.Number = 0  Then
						SelectWinRadioButton = 0
						 WriteStatusToLogs 	"Expected RadioButton Selected." 
							
					Else
							 SelectWinRadioButton = 1
							 WriteStatusToLogs 	"RadioButton not Selected, please verify." 
					End If
			Else
					SelectWinRadioButton = 1
            End If
	On Error GoTo 0
End Function
'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :				 VerifyRadioButtonExist()
'	Purpose :					 Verifies whether the RadioButton Exists or not?
'	Return Value :		 		  Integer( 0/1)
'	Arguments :
'		Input Arguments :  		  objRadioButton 
'		Output Arguments : 
'	Function is called by :
'	Function calls :			 CheckObjectExist() 
'	Created on :				   10/03/2008
'	Author :						  Rathna Reddy 
'
'------------------------------------------------------------------------------------------------------------------------------------------

Function VerifyWinRadioButtonExist(ByVal objRadioButton)
	On Error Resume Next
		'Variable Section Begin
	
		   Dim strMethod

		'Variable Section End
		strMethod = strKeywordForReporting
			If CheckObjectExist(objRadioButton) Then 
			   
					VerifyWinRadioButtonExist = 0
					WriteStatusToLogs "The RadioButton Exist."
					
			Else
					VerifyWinRadioButtonExist = 1
					WriteStatusToLogs "The RadioButton does not Exist, please verify."
					
			End If 
	  
	
	On Error GoTo 0
End Function

'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :				 VerifyRadioButtonNotExist()
'	Purpose :					 Verifies whether the RadioButton Exists or not?
'	Return Value :		 		  Integer( 0/1)
'	Arguments :
'		Input Arguments :  		  objRadioButton 
'		Output Arguments : 
'	Function is called by :
'	Function calls :			 CheckObjectExist() 
'	Created on :				   10/03/2008
'	Author :						  Rathna Reddy 
'
'------------------------------------------------------------------------------------------------------------------------------------------

Function VerifyWinRadioButtonNotExist(ByVal objRadioButton)
	On Error Resume Next
		'Variable Section Begin
	
		  Dim strMethod

		'Variable Section End
			strMethod = strKeywordForReporting
			If CheckObjectNotExist(objRadioButton) Then 
			   
					VerifyWinRadioButtonNotExist = 0
					WriteStatusToLogs "The RadioButton does not Exist."
					
			Else
					VerifyWinRadioButtonNotExist = 1
					WriteStatusToLogs "The RadioButton Exists, please verify."
					
			End If 
	
  		
	On Error GoTo 0
End Function

'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name :				 VerifyRadioButtonEnabled()
' 	Purpose :					 Verifies whether the RadioButton is Enabled
' 	Return Value :		 		  Integer( 0/1)
' 	Arguments :
'		Input Arguments :  		objRadioButton 
'		Output Arguments : 
'	Function is called by :
'	Function calls :				CheckObjectExist() , CheckObjectStatus()
'	Created on :				   11/03/2008
'	Author :						  Rathna Reddy 

'
'------------------------------------------------------------------------------------------------------------------------------------------
Function VerifyWinRadioButtonEnabled(ByVal objRadioButton)

On Error Resume Next
	'Variable Section Begin
		
		Dim strMethod

	'Variable Section End
	strMethod = strKeywordForReporting
		If CheckObjectExist(objRadioButton) Then
			If CheckObjectEnabled(objRadioButton) Then
					VerifyWinRadioButtonEnabled = 0
					WriteStatusToLogs "The RadioButton was in enabled state."
				Else
					VerifyWinRadioButtonEnabled = 1
					WriteStatusToLogs "The RadioButton was not in enabled state, please verify."
				End If
            
		Else
				VerifyWinRadioButtonEnabled = 1
				WriteStatusToLogs "The RadioButton does not exist, please verify."
		End if
		
	On Error GoTo 0
    
End Function

'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name :				 VerifyRadioButtonDisabled()
' 	Purpose :					 Verifies whether the RadioButton is Disabled
' 	Return Value :		 		  Integer( 0/1)
' 	Arguments :
'		Input Arguments :  		objRadioButton 
'		Output Arguments : 
'	Function is called by :
'	Function calls :				CheckObjectExist() , CheckObjectStatus()
'	Created on :				   11/03/2008
'	Author :						  Rathna Reddy 
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function VerifyWinRadioButtonDisabled(ByVal objRadioButton)

On Error Resume Next
	'Variable Section Begin
		
		Dim strMethod

	'Variable Section End
	strMethod = strKeywordForReporting
		If CheckObjectExist(objRadioButton) Then
			If CheckObjectDisabled(objRadioButton) Then
					VerifyWinRadioButtonDisabled = 0
					WriteStatusToLogs "The RadioButton was in Disabled state."
				Else
					VerifyWinRadioButtonDisabled = 1
					WriteStatusToLogs "The RadioButton was not in Disabled state, please verify."
				End If
            
		Else
				VerifyWinRadioButtonDisabled = 1
				WriteStatusToLogs "The RadioButton does not exist, please verify."
		End if
	On Error GoTo 0
End Function

'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :				 VerifyRadioButtonVisible()
'	Purpose :					 Verifies whether the RadioButton is visible.
'	Return Value :		 		  Integer( 0/1)
'	Arguments :
'		Input Arguments :  		  objRadioButton 
'		Output Arguments : 
'	Function is called by :
'	Function calls :			 CheckObjectExist() , CheckObjectVisiblity()
'	Created on :				   10/03/2008
'	Author :						  Rathna Reddy 
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function VerifyWinRadioButtonVisible(ByVal objRadioButton)
	On Error Resume Next
		'Variable Section Begin
		
			Dim strMethod

		'Variable Section End
		strMethod = strKeywordForReporting
			If CheckObjectExist(objRadioButton)  Then 
				If CheckObjectVisible(objRadioButton) Then
					VerifyWinRadioButtonVisible = 0
					WriteStatusToLogs "The RadioButton was in visible mode."
				Else
					VerifyWinRadioButtonVisible = 1
					WriteStatusToLogs "The RadioButton was not in visible mode, please verify."
				End If
			Else
					VerifyWinRadioButtonVisible = 1
					WriteStatusToLogs "The RadioButton does not exist, please verify."
			End If
		
	On Error GoTo 0
End Function
'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :				 VerifyRadioButtonInVisible()
'	Purpose :					 Verifies whether the RadioButton is Invisible.
'	Return Value :		 		  Integer( 0/1)
'	Arguments :
'		Input Arguments :  		  objRadioButton 
'		Output Arguments : 
'	Function is called by :
'	Function calls :			 CheckObjectExist() , CheckObjectVisiblity()
'	Created on :				   10/03/2008
'	Author :						  Rathna Reddy 
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function VerifyWinRadioButtonInVisible(ByVal objRadioButton)
	On Error Resume Next
		'Variable Section Begin
		
			Dim strMethod

		'Variable Section End
		strMethod = strKeywordForReporting
			If CheckObjectExist(objRadioButton)  Then 
				If CheckObjectInVisible(objRadioButton) Then
					VerifyWinRadioButtonInVisible = 0
					WriteStatusToLogs "The RadioButton was in InVisible mode."
				Else
					VerifyWinRadioButtonInVisible = 1
					WriteStatusToLogs "The RadioButton was not in InVisible mode, please verify."
				End If
			Else
					VerifyWinRadioButtonInVisible = 1
					WriteStatusToLogs "The RadioButton does not exist, please verify."
			End If
		
	On Error GoTo 0
End Function






'=======================Code Section End=============================================
