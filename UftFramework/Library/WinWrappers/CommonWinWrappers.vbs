'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :				 ClickWinObjectIfExists()
'	Purpose :					 Clicks the object if it exists
'	Return Value :		 		 Integer( 0/1)
'	Arguments :
'		Input Arguments :  		 objWin 
'		Output Arguments : 
'	Function is called by :
'	Function calls :			 CheckObjectExist() 
'	Created on :				 14/02/2012
'	Author :					 Iohbha K 
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function ClickWinObjectIfExists(ByVal objWin)
On Error Resume Next
		'Variable Section Begin
	
			 Dim strMethod 

		'Variable Section End
		
			
		strMethod = strKeywordForReporting
		If CheckObjectExist(objWin) Then 
				If CheckWinObjectEnabled(objWin) = 0 Then
						objWin.click
						If Err.number = 0 Then
								ClickWinObjectIfExists = 0
								WriteStatusToLogs "Click has been performed on the object successfully."
						Else
								ClickWinObjectIfExists = 1
								WriteStatusToLogs "An error occurred while clicking the object, please verify. Error:"&Err.description
						End If 
				End If
		Else
				ClickWinObjectIfExists = 0
				WriteStatusToLogs "The object did not exist."
		End If

		
On Error GoTo 0
End Function


'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :				 VerifyWinObjectEnabled()
'	Purpose :					 Verifies the object is enabled
'	Return Value :		 		 Integer(0/1/2)
'	Arguments :
'		Input Arguments :  		 objWin 
'		Output Arguments : 
'	Function is called by :
'	Function calls :			 CheckObjectExist(), CheckWinObjectEnabled() 
'	Created on :				 15/02/2012
'	Author :					 Iohbha K 
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function VerifyWinObjectEnabled(ByVal objWin)
On Error Resume Next
		'Variable Section Begin
	
			 Dim strMethod, enabilityState 

		'Variable Section End		
			
		strMethod = strKeywordForReporting
		If CheckObjectExist(objWin) Then 
				enabilityState = CheckWinObjectEnabled(objWin)
				If  enabilityState = 0 Then
						VerifyWinObjectEnabled = 0
						WriteStatusToLogs "The object is enabled."
				ElseIf enabilityState = 2 Then
						VerifyWinObjectEnabled = 2
						WriteStatusToLogs "The object is disabled"
				
				Else
						VerifyWinObjectEnabled = 1
						WriteStatusToLogs "An error occurred while validating the enability, please verify."
				End If
		Else
				VerifyWinObjectEnabled = 1
				WriteStatusToLogs "The object did not exist."
		End If
		
On Error GoTo 0
End Function

'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :				 VerifyWinObjectDisabled()
'	Purpose :					 Verifies the object is disabled
'	Return Value :		 		 Integer(0/1/2)
'	Arguments :
'		Input Arguments :  		 objWin 
'		Output Arguments : 
'	Function is called by :
'	Function calls :			 CheckObjectExist(), CheckWinObjectEnabled() 
'	Created on :				 15/02/2012
'	Author :					 Iohbha K 
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function VerifyWinObjectDisabled(ByVal objWin)
On Error Resume Next
		'Variable Section Begin
	
			 Dim strMethod, enabilityState 

		'Variable Section End		
			
		strMethod = strKeywordForReporting
		If CheckObjectExist(objWin) Then 
				enabilityState = CheckWinObjectEnabled(objWin)
				If  enabilityState = 2 Then
						VerifyWinObjectDisabled = 0
						WriteStatusToLogs "The object is disabled."
				ElseIf enabilityState = 0 Then
						VerifyWinObjectDisabled = 2
						WriteStatusToLogs "The object is enabled."
				
				Else
						VerifyWinObjectDisabled = 1
						WriteStatusToLogs "An error occurred while validating the enability, please verify."
				End If
		Else
				VerifyWinObjectDisabled = 1
				WriteStatusToLogs "The object did not exist."
		End If
		
On Error GoTo 0
End Function


Function CheckWinObjectEnabled(ByVal objWin)
On Error Resume Next
		Dim enable
		enable = objWin.getROProperty("enabled")
		If Err.number = 0 Then
				If enable Then
						'enabled
						CheckWinObjectEnabled = 0
						WriteStatusToLogs "The object is enabled."
				Else
						'disabled
						CheckWinObjectEnabled = 2
						WriteStatusToLogs "The object is disabled."
				End If
		Else
				'error occurred
				CheckWinObjectEnabled = 1
				WriteStatusToLogs "An error occurred while validating the enability of the object, please verify. Error:"&Err.description
		End if
On Error goto 0
End Function
