Option Explicit 
'=======================Code Section Begin=============================================
'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name :				  SelectListItem()
' 	Purpose :						 Selects a specified item from the ListBox
' 	Return Value :		 		   Integer( 0/1)
' 	Arguments :
'		Input Arguments :  		objStatic , strSelectItem
'		Output Arguments :     strActItem
'	Function is called by :
'	Function calls :				CheckObjectExist()
'	Created on :				   19/03/2008
'	Author :						 Rathna Reddy 
' 
'------------------------------------------------------------------------------------------------------------------------------------------
Function CompareStaticText(ByVal objStatic , ByVal strExpStaticText)
   	On Error Resume Next
		 'Variable Section Begin
	
			Dim strMethod
			Dim strActValue, blnstatus 
			blnstatus  = False

		'Variable Section End
			strMethod = strKeywordForReporting
			If CheckObjectExist(objStatic) Then  
				strActValue = objStatic.GetROProperty("text")
				
				If Vartype(strActValue) <> vbEmpty  And Err.Number = 0 Then
					blnstatus = true
				End If
					If blnstatus  Then
								If CompareStringValues(strExpStaticText, strActValue)  = 0 Then
									CompareStaticText = 0
									WriteStatusToLogs """"&"Expected Value : "&strExpValue& """" &  " and " &""""&"Actual Value : "&strActValue&""""&" matches as expected."
		
								Else
									CompareStaticText = 2
									WriteStatusToLogs """"&"Expected Value : "&strExpValue& """" &  " and " &""""&"Actual Value : "&strActValue&""""&" does not match, please verify."
		
								End If
					Else
							CompareStaticText = 1
							WriteStatusToLogs "An error occurred while capturing Static text value."
					End If
		Else
				CompareStaticText = 1
				 WriteStatusToLogs 	"EditBox does not exist, please verify."
				
		 End If
			'WriteStepResults CompareEditBoxValue
	On Error GOTO 0
End Function


'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :				 VerifyEditBoxVisible()
'	Purpose :					 Verifies whether the EditBox is visible.
'	Return Value :		 		  Integer( 0/1)
'	Arguments :
'		Input Arguments :  		  objStatic 
'		Output Arguments : 
'	Function is called by :
'	Function calls :			 CheckObjectExist() , CheckObjectVisiblity()
'	Created on :				   10/03/2008
'	Author :						  Rathna Reddy 
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function VerifyStaticTextVisible(ByVal objStatic)
	On Error Resume Next
		'Variable Section Begin
			Dim strMethod
			

		'Variable Section End
			strMethod = strKeywordForReporting
			If CheckObjectExist(objStatic)  Then
				If CheckObjectVisible(objStatic) Then
					VerifyStaticTextVisible = 0
					WriteStatusToLogs "The Static Text is visible as expected."
				Else
					VerifyStaticTextVisible = 2
					WriteStatusToLogs "The Static Text is not visible, please verify."
				End If
			Else
					VerifyStaticTextVisible = 1
					WriteStatusToLogs "The Static Text does not exist, please verify."
			End If
		
	On Error GoTo 0
End Function

'=======================Code Section End=============================================
