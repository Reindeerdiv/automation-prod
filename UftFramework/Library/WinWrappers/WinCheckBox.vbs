
'=======================Code Section Begin=============================================

Option Explicit 		'Forces explicit declaration of all variables in a script. 
'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :				  SelectCheckBox()
' 	Purpose :						Selects a specified checkbox.
' 	Return Value :		 		   Integer( 0/1)
' 	Arguments :
'		Input Arguments :  		objCheckBox 
'		Output Arguments :      
'	Function is called by :
'	Function calls :				CheckObjectExist()
'	Created on :				   11/03/2008
'	Author :						  Rathna Reddy 
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function SelectWinCheckBox(ByVal objChckBox)
	On Error Resume Next
		'Variable Section Begin
		Dim strMethod
		Dim intActValue

		'Variable Section End
		strMethod = strKeywordForReporting
		If CheckObjectExist(objChckBox) Then
            	 								   
					objChckBox.Set "ON"
					intActValue = objChckBox.GetROProperty("checked")
					If Err.Number = 0 Then
							If intActValue = "ON" Then
								SelectWinCheckBox = 0
								WriteStatusToLogs "The CheckBox got selected sucessfully."
							Else
								SelectWinCheckBox = 1
								WriteStatusToLogs "The CheckBox could not get selected, please verify."
							End If
				  
					Else
							SelectWinCheckBox = 1
							WriteStatusToLogs "The Error occured during value set to ON, please verify."
					End If
					
			Else
					SelectWinCheckBox = 1
					WriteStatusToLogs "The CheckBox does not exist, please verify."
			End If
					
	 On Error GoTo 0
End Function



'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :				  ClearCheckBox()
' 	Purpose :						Clears a specified checkbox.
' 	Return Value :		 		   Integer( 0/1)
' 	Arguments :
'		Input Arguments :  		objCheckBox 
'		Output Arguments : 
'	Function is called by :
'	Function calls :				CheckObjectExist()
'	Created on :				   11/03/2008
'	Author :						  Rathna Reddy 
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function ClearWinCheckBox(ByVal objCheckBox)
	On Error Resume Next
		'Variable Section Begin
		Dim strMethod
		Dim  intActValue, blnstatus 
			blnstatus = False
		'Variable Section End
		 strMethod = strKeywordForReporting
		 If CheckObjectExist(objCheckBox) Then
            If CheckObjectEnabled(objCheckBox) Then
				 intActValue = objCheckBox.GetROProperty("checked")
				 If Vartype(intActValue) <> vbEmpty  Then
					blnstatus = true
				 End If
				 
				 If  blnstatus Then
    					If intActValue = UCase("ON") Then
								objCheckBox.Set "Off"
								
									intActValue = objCheckBox.GetROProperty("checked")
									 If  err.Number = 0 Then
										If UCase(intActValue) = Ucase("OFF") Then
											ClearWinCheckBox = 0
											WriteStatusToLogs	"The CheckBox got de-selected sucessfully."
										Else
											ClearWinCheckBox = 1
											WriteStatusToLogs	"The CheckBox could not get de-selected, please verify."
										End If
									Else
										ClearWinCheckBox = 1
										  WriteStatusToLogs	"Could not capture checkbox state, please verify."

									End If
						ElseIf UCase(intActValue) = UCase("OFF") Then
							
							ClearWinCheckBox = 0
							WriteStatusToLogs	"The CheckBox was already de-selected."
						else
							ClearWinCheckBox = 1
							WriteStatusToLogs	"The CheckBox was in different state, please verify."
						End If
					Else
						ClearWinCheckBox = 1
					    WriteStatusToLogs	"Could not capture checkbox state, please verify."
						
					End If
                Else
					ClearWinCheckBox = 1
					WriteStatusToLogs	"The CheckBox was not enabled, please verify."
				End If
					
			Else
				ClearWinCheckBox = 1
					WriteStatusToLogs	"The CheckBox does not exist, please verify."
			End If
				
	 On Error GoTo 0
End Function
'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :				  VerifyCheckBoxChecked()
'	Purpose :					  Verifies whether the checkbox is in checked state
'	Return Value :		 		   Integer( 0/1)
'	Arguments :
'		Input Arguments :  		  objCheckBox 
'		Output Arguments : 	      intActValue,intExpValue
'	Function is called by :
'	Function calls :				CheckObjectExist()
'	Created on :				   11/03/2008
'	Author :						  Rathna Reddy 
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function VerifyWinCheckBoxChecked(ByVal objCheckBox)
	On Error Resume Next
		'Variable Section Begin
			Dim strMethod
			Dim intActValue,intExpValue, blnstatus
                blnstatus = False
		'Variable Section End
			strMethod = strKeywordForReporting
			If CheckObjectExist(objCheckBox) Then
			    intExpValue = "ON"
				 intActValue = objCheckBox.GetROProperty("checked") 
						 If Vartype(intActValue) <> vbEmpty  And Err.Number = 0 Then
							blnstatus = true
						 End If

						 If blnstatus  Then
								 If intActValue = intExpValue Then
										VerifyWinCheckBoxChecked = 0
										WriteStatusToLogs "CheckBox was checked as expected."
									Else
										VerifyWinCheckBoxChecked = 2
										WriteStatusToLogs 	"CheckBox was not checked, please verify."
								End if
						Else
						VerifyWinCheckBoxChecked = 1
								WriteStatusToLogs	"Could not capture checkbox state, please verify."
						End If
					
			Else
			VerifyWinCheckBoxChecked = 1
			WriteStatusToLogs	"The CheckBox does not exist, please verify."

			End If
		
	 On Error GoTo 0
End Function	 

'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :				  VerifyCheckBoxUnChecked()
'	Purpose :					  Verifies whether the checkbox is in UnChecked state
'	Return Value :		 		   Integer( 0/1)
'	Arguments :
'		Input Arguments :  		objCheckBox 
'		Output Arguments : 	   intActValue,intExpVal
'	Function is called by :
'	Function calls :				CheckObjectExist()
'	Created on :				   11/03/2008
'	Author :						  Rathna Reddy 
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function VerifyWinCheckBoxUnChecked(ByVal objCheckBox)
	On Error Resume Next
		'Variable Section Begin
			Dim strMethod
			Dim intActValue,intExpValue, blnstatus 
			blnstatus  =False
			
		'Variable Section End	
			strMethod = strKeywordForReporting
			If CheckObjectExist(objCheckBox) Then
			    intExpValue = "OFF"
				 intActValue = objCheckBox.GetROProperty("checked") 

					If Vartype(intActValue) <> vbEmpty  And Err.Number = 0 Then
						blnstatus = true
					 End If

					 If blnstatus  Then				 
					 
								 If intActValue = intExpValue Then
										VerifyWinCheckBoxUnChecked = 0
										WriteStatusToLogs 	"CheckBox was Un-checked as expected."
									Else
										VerifyWinCheckBoxUnChecked = 2
										WriteStatusToLogs 	"CheckBox was checked, please verify."
								End if
					Else
					VerifyWinCheckBoxUnChecked = 1
					WriteStatusToLogs	"Could not capture checkbox state, please verify."
	
					
					End if 
			
			Else
					VerifyWinCheckBoxUnChecked = 1
					WriteStatusToLogs	"The CheckBox does not exist, please verify."
		End If
		
	 On Error GoTo 0
End Function

'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name :				 VerifyCheckBoxEnabled()
' 	Purpose :					 Verifies whether the CheckBox is Enabled
' 	Return Value :		 		  Integer( 0/1)
' 	Arguments :
'		Input Arguments :  		objCheckBox 
'		Output Arguments : 
'	Function is called by :
'	Function calls :				CheckObjectExist() , CheckObjectStatus()
'	Created on :				   11/03/2008
'	Author :						  Rathna Reddy 
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function VerifyWinCheckBoxEnabled(ByVal objCheckBox)

On Error Resume Next
	'Variable Section Begin
		
		Dim strMethod

	'Variable Section End
		strMethod = strKeywordForReporting
		If CheckObjectExist(objCheckBox) Then
			If CheckObjectEnabled(objCheckBox) Then
					VerifyWinCheckBoxEnabled = 0
					WriteStatusToLogs "The CheckBox was enabled as expected."
				Else
					VerifyWinCheckBoxEnabled = 2
					WriteStatusToLogs "The CheckBox was not enabled, please verify."
				End If
            
		Else
				VerifyWinCheckBoxEnabled = 1
				WriteStatusToLogs "The CheckBox does not exist, please verify."
		End if
		
	On Error GoTo 0
    
End Function

'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name :				 VerifyCheckBoxDisabled()
' 	Purpose :					 Verifies whether the CheckBox is Disabled
' 	Return Value :		 		  Integer( 0/1)
' 	Arguments :
'		Input Arguments :  		objCheckBox 
'		Output Arguments : 
'	Function is called by :
'	Function calls :				CheckObjectExist() , CheckObjectStatus()
'	Created on :				   11/03/2008
'	Author :						  Rathna Reddy 
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function VerifyWinCheckBoxDisabled(ByVal objCheckBox)

On Error Resume Next
	'Variable Section Begin
		
		Dim strMethod

	'Variable Section End
		strMethod = strKeywordForReporting
		If CheckObjectExist(objCheckBox) Then
			If CheckObjectDisabled(objCheckBox) Then
					VerifyWinCheckBoxDisabled = 0
					WriteStatusToLogs "The CheckBox was disabled as expected."
				Else
					VerifyWinCheckBoxDisabled = 2
					WriteStatusToLogs "The CheckBox was not disabled, please verify."
				End If
            
		Else
				VerifyWinCheckBoxDisabled = 1
				WriteStatusToLogs "The CheckBox does not exist, please verify."
		End if
	On Error GoTo 0
End Function

'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :				 VerifyCheckBoxVisible()
'	Purpose :					 Verifies whether the CheckBox is visible.
'	Return Value :		 		  Integer( 0/1)
'	Arguments :
'		Input Arguments :  		  objCheckBox 
'		Output Arguments : 
'	Function is called by :
'	Function calls :			 CheckObjectExist() , CheckObjectVisiblity()
'	Created on :				   10/03/2008
'	Author :						  Rathna Reddy 
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function VerifyWinCheckBoxVisible(ByVal objCheckBox)
	On Error Resume Next
		'Variable Section Begin
		
			Dim strMethod

		'Variable Section End
			strMethod = strKeywordForReporting
			If CheckObjectExist(objCheckBox)  Then 
				If CheckObjectVisible(objCheckBox) Then
					VerifyWinCheckBoxVisible = 0
					WriteStatusToLogs "The CheckBox was visible as expected."
				Else
					VerifyWinCheckBoxVisible = 2
					WriteStatusToLogs "The CheckBox was not visible, please verify."
				End If
			Else
					VerifyWinCheckBoxVisible = 1
					WriteStatusToLogs "The CheckBox does not exist, please verify."
			End If
		
	On Error GoTo 0
End Function
'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :				 VerifyCheckBoxInVisible()
'	Purpose :					 Verifies whether the CheckBox is Invisible.
'	Return Value :		 		  Integer( 0/1)
'	Arguments :
'		Input Arguments :  		  objCheckBox 
'		Output Arguments : 
'	Function is called by :
'	Function calls :			 CheckObjectExist() , CheckObjectVisiblity()
'	Created on :				   10/03/2008
'	Author :						  Rathna Reddy 
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function VerifyWinCheckBoxInVisible(ByVal objCheckBox)
	On Error Resume Next
		'Variable Section Begin
		
			Dim strMethod

		'Variable Section End
			strMethod = strKeywordForReporting
			If CheckObjectExist(objCheckBox)  Then 
				If CheckObjectInVisible(objCheckBox) Then
					VerifyWinCheckBoxInVisible = 0
					WriteStatusToLogs "The CheckBox was In-visible as expected."
				Else
					VerifyWinCheckBoxInVisible = 2
					WriteStatusToLogs "The CheckBox was not In-visible, please verify."
				End If
			Else
					VerifyWinCheckBoxInVisible = 1
					WriteStatusToLogs "The CheckBox does not exist, please verify."
			End If
		
	On Error GoTo 0
End Function

'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :				 VerifyCheckBoxExist()
'	Purpose :					 Verifies whether the CheckBox Exists or not?
'	Return Value :		 		  Integer( 0/1)
'	Arguments :
'		Input Arguments :  		  objCheckBox 
'		Output Arguments : 
'	Function is called by :
'	Function calls :			 CheckObjectExist() 
'	Created on :				   10/03/2008
'	Author :						  Rathna Reddy 
'
'------------------------------------------------------------------------------------------------------------------------------------------

Function VerifyWinCheckBoxExist(ByVal objCheckBox)
	On Error Resume Next
		'Variable Section Begin
	
		   Dim strMethod

		'Variable Section End
			strMethod = strKeywordForReporting
			If CheckObjectExist(objCheckBox) Then 
			   
					VerifyWinCheckBoxExist = 0
					WriteStatusToLogs "The CheckBox exists as expected."
					
			Else
					VerifyWinCheckBoxExist = 2
					WriteStatusToLogs "The CheckBox does NOT exist, please verify."
					
			End If 
	  
	
	On Error GoTo 0
End Function

'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :				 VerifyCheckBoxNotExist()
'	Purpose :					 Verifies whether the CheckBox Exists or not?
'	Return Value :		 		  Integer( 0/1)
'	Arguments :
'		Input Arguments :  		  objCheckBox 
'		Output Arguments : 
'	Function is called by :
'	Function calls :			 CheckObjectExist() 
'	Created on :				   10/03/2008
'	Author :						  Rathna Reddy 
'
'------------------------------------------------------------------------------------------------------------------------------------------

Function VerifyWinCheckBoxNotExist(ByVal objCheckBox)
	On Error Resume Next
		'Variable Section Begin
	
		   Dim strMethod

		'Variable Section End
			strMethod = strKeywordForReporting
			If CheckObjectNotExist(objCheckBox) Then 
			   
					VerifyWinCheckBoxNotExist = 0
					WriteStatusToLogs "The CheckBox does not exist as expected"
					
			Else
					VerifyWinCheckBoxNotExist = 2
					WriteStatusToLogs "The CheckBox exists, please verify."
					
			End If 
	
  		
	On Error GoTo 0
End Function

'=======================Code Section End=============================================
