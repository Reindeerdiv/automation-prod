Option Explicit

'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name :				  ClickWinToolBarItemByName()
' 	Purpose :					  Clicks a specified item in the toolbar based on the item's name
' 	Return Value :		 		  Integer(0/1)
' 	Arguments :
'		Input Arguments :  		  objListBox , strSelectItem
'		Output Arguments :        strActItem
'	Function is called by :
'	Function calls :			  CheckObjectExist()
'	Created on :				  14/02/2012
'	Author :					  Iohbha K 
' 
'------------------------------------------------------------------------------------------------------------------------------------------
Function ClickWinToolBarItemByName(byval objToolBar,byval strItemName)          
On Error Resume Next			

	'Variable Section Begin

			Dim strMethod

	'Variable Section End

	strMethod = strKeywordForReporting

	If IsEmpty(strItemName) Or IsNull(strItemName) Or Trim(strItemName) = "" Then
			ClickWinToolBarItemByName = 1
			WriteStatusToLogs "The item is either null/empty, Please verify. This parameter should be a valid item's name/tooltip."			
	End If
	
	If CheckObjectExist(objToolBar) Then		
			objToolBar.Press strItemName
			If Err.number = 0 Then
					ClickWinToolBarItemByName = 0
					WriteStatusToLogs "The item "&strItemName&" is clicked."					
			Else
					ClickWinToolBarItemByName = 1
					WriteStatusToLogs "Failed to click the item "&strItemName&" , an error occurred, please verify. Error:"&Err.description
			End if
	Else
			ClickWinToolBarItemByName = 1
			WriteStatusToLogs "The object does not exist, please verify."
	End If
	
On Error Goto 0
End Function
