
'========================Code Section Begin=====================================================================
Option Explicit	'Forces explicit declaration of all variables in a script.
'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :				 VerifyFrameExist()
'	Purpose :					 Verifies the existance of Frame in AUT.
'	Return Value :		 		  Integer( 0/1)
'	Arguments :
'		Input Arguments :  		  objFrame 
'		Output Arguments : 
'	Function is called by :
'	Function calls :			 CheckObjectExist() 
'	Created on :				      15/01/2009
'	Author :						  Rathna Reddy 
'
'------------------------------------------------------------------------------------------------------------------------------------------

Function VerifyFrameExist(ByVal objFrame)
	On Error Resume Next
		'Variable Section Begin
	
			Dim strMethod
		  

		'Variable Section End
			strMethod = strKeywordForReporting
			If CheckObjectExist(objFrame) Then 
			   
					VerifyFrameExist = 0
					WriteStatusToLogs "The Frame Exists."
					
			Else
					VerifyFrameExist = 1
					WriteStatusToLogs "The Frame doesn't exist, please verify."
					
			End If 
	  
	
	On Error GoTo 0
End Function

'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :				 VerifyFrameNotExist()
'	Purpose :					 Verifies whether the Frame Exist or not in AUT?.
'	Return Value :		 		  Integer( 0/1)
'	Arguments :
'		Input Arguments :  		  objFrame 
'		Output Arguments : 
'	Function is called by :
'	Function calls :			 CheckObjectExist() 
'	Created on :				   15/01/2009
'	Author :						  Rathna Reddy 
'
'------------------------------------------------------------------------------------------------------------------------------------------

Function VerifyFrameNotExist(ByVal objFrame)
	On Error Resume Next
		'Variable Section Begin
	
		  Dim strMethod

		'Variable Section End
		strMethod = strKeywordForReporting
		
			If  CheckObjectNotExist(objFrame) Then 
			   
					VerifyFrameNotExist = 0
					WriteStatusToLogs "The Frame doesn't exist."
					
			Else
					VerifyFrameNotExist = 1
					WriteStatusToLogs "The Frame exists, please verify."
					
			End If 
	
  		
	On Error GoTo 0
End Function

'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :				 CompareFrameTitle()
'	Purpose :					 Compares the Frame title with our expected title.
'	Return Value :		 		 Integer( 0/2/1)
'	Arguments :
'		Input Arguments :  		objFrame , strExpTitle
'		Output Arguments :      strActTitle
'	Function is called by :
'	Function calls :			 CheckObjectExist() , CompareStringValues()
'	Created on : 				    15/01/2009
'	Author :					 Rathna Reddy
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function  CompareFrameTitle(ByVal objFrame , ByVal strExpTitle)
	On Error Resume Next 
		'Variable Section Begin
		 Dim strMethod
			Dim strActTitle
		'Variable Section End
		strMethod = strKeywordForReporting
			If CheckObjectExist(objFrame) Then
					strActTitle = objFrame.GetRoProperty("title")
				If Err.number=0 then
					 If CompareStringValues(strExpTitle,strActTitle)=0  Then 
						CompareFrameTitle = 0
						 WriteStatusToLogs """"&"Expected Title : "&strExpTitle& """" &  "   and   " &""""&"Actual Title : "&strActTitle&""""&"  are equal."

					 Else
						CompareFrameTitle = 2
						 WriteStatusToLogs """"&"Expected Title : "&strExpTitle& """" &  "   and   " &""""&"Actual Title : "&strActTitle&""""&"  are not equal, please verify."
					 End If

				 Else
					CompareFrameTitle = 1
					 WriteStatusToLogs "Action fails to Capture Frame Title, please verify Frame Properties."
				  
			    End If

			Else
				 CompareFrameTitle = 1
				 WriteStatusToLogs "Frame Doesn't exist, please verify."
			End If
	On Error GoTo 0 
End Function

'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :				 CompareFrameURL()
'	Purpose :					 Compares the current URL of  the Frame with our expected URL
'	Return Value :		 		  Integer( 0/1/2)
'	Arguments :
'		Input Arguments :  		 objFrame , strExpURL
'		Output Arguments :       strActURL
'	Function is called by :
'	Function calls :                CheckObjectExist() , CompareObjectURL()
'	Created on : 				      15/01/2009
'	Author :						 Rathna Reddy
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function CompareFrameURL(ByVal objFrame , ByVal strExpURL)
	On Error Resume Next
		'Variable Section Begin
			Dim strMethod
			Dim strActURL

		'Variable Section End
			strMethod = strKeywordForReporting
			If CheckObjectExist(objFrame) Then
				strActURL = objFrame.GetRoProperty("url")
				If Err.number=0 then
					If	CompareStringValues(strExpURL,strActURL)= 0 Then 
						CompareFrameURL = 0
						WriteStatusToLogs """"&"Expected URL : "&strExpURL& """" &  "   and   " &""""&"Actual URL : "&strActURL&""""&"  are equal."
					Else
						CompareFrameURL = 2
						WriteStatusToLogs """"&"Expected URL : "&strExpURL& """" &  "   and   " &""""&"Actual URL : "&strActURL&""""&"  are not equal, please verify."
					End If
				 Else
					CompareFrameURL = 1 
					WriteStatusToLogs "Action fails to Capture Frame URL, please verify Frame URL Properties."

				End If
		 Else
			  CompareFrameURL = 1
			  WriteStatusToLogs "Frame doesn't exist, please verify."
		 End If
	On Error GoTo 0 
End Function



'========================Code Section End=====================================================================
