

'=======================Code Section Begin=============================================

Option Explicit 

'
'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name :				  SelectListItem()
' 	Purpose :						 Selects a specified item from the ListBox
' 	Return Value :		 		   Integer( 0/1)
' 	Arguments :
'		Input Arguments :  		objListBox , strSelectItem
'		Output Arguments :     strActItem
'	Function is called by :
'	Function calls :				CheckObjectExist()
'	Created on :				   19/03/2008
'	Author :						 Rathna Reddy 
' 
'------------------------------------------------------------------------------------------------------------------------------------------
Function SelectWinListItem(ByVal objListBox , ByVal strSelectItem)
	On Error Resume Next
		'Variable Section Begin
			Dim strMethod
			Dim strActItem, strallitems
			Dim arritems
			Dim blnstatus 
			blnstatus  = false

		'Variable Section End
		strMethod = strKeywordForReporting
		If CheckObjectExist(objListBox) Then
				strallitems = objListBox.GetRoProperty("all items")
		  
				If  vartype(strallitems ) <> vbEmpty   Then
						blnstatus = True					
				End If

				If blnstatus  Then
						arritems = Split(strallitems, chr(10))
						If IsItemExistInArray(arritems,strSelectItem)  Then
								objListBox.Select strSelectItem							
								 If Err.number = 0 Then												
										strActItem = objListBox.GetSelection
										If Not IsEmpty(strActItem) Then
												 If CompareStringValues(strSelectItem, strActItem) = 0 Then
														SelectWinListItem = 0
														WriteStatusToLogs "Given item '"&strSelectItem&"' is selected in the ListBox"
												 Else
														SelectWinListItem = 1
														WriteStatusToLogs "The specific item "&strSelectItem&" could not be selected though it exist, please verify."
												 End If
										Else
											SelectWinListItem = 1
											WriteStatusToLogs "The ListBox item could not be selected, please verify."
				
										End If
								Else
										SelectWinListItem=1
										WriteStatusToLogs "Fail to select the item "&strSelectItem&". Either the item does not exist or  an error occurred ,Please verify. Error: "&Err.description
								End If
						Else
								SelectWinListItem = 1
								WriteStatusToLogs "The specific item "&strSelectItem&" does not exist, please verify."
						End if
						
				Else'blnstatus
						SelectWinListItem=1
						WriteStatusToLogs "List item does not contains any item, please verify."
				End if'blnstatus
		Else
			SelectWinListItem = 1
			WriteStatusToLogs "The ListBox does not exist, please verify."
		End If

On Error GoTo 0
End Function
'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name :				 VerifyAllListItems()
' 	Purpose :					 Verifies  all the list items with the expected list
' 	Return Value :		 		 Integer( 01)
' 	Arguments :
'		Input Arguments :  	  	 objListBox,strExpItems
'		Output Arguments :       strActItems , arrActItems() , arrExpItems()
'	Function is called by :
'	Function calls :			 CheckObjectExist() , GetArray() , MatchEachArrData()
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function VerifyAllWinListItems(ByVal objListBox , ByVal arrExpItems)
	On Error Resume Next
		'Variable Section Begin
			Dim strMethod
			Dim strActItems , arrActItems
			
		'Variable Section End
		strMethod = strKeywordForReporting
		If CheckObjectExist(objListBox)  Then   
				strActItems = objListBox.GetContent
				If Err.number = 0 then				
						If Not IsEmpty(strActItems) Then
								arrActItems = GetArray(strActItems , chr(10))

								If MatchEachArrData(arrExpItems, arrActItems)  Then
									VerifyAllWinListItems = 0
									WriteStatusToLogs "The specified list items matches with the expected list item."
								Else
									VerifyAllWinListItems = 2
									WriteStatusToLogs "The specified list items does not match the expected list item."
								End IF
						Else
								VerifyAllWinListItems = 1
								WriteStatusToLogs "ListBox does NOT contain any item, please verify."
						End If

				Else
					VerifyAllWinListItems = 1
					WriteStatusToLogs "Could not retrieve all the items of the ListBox."
				End if
		Else
				VerifyAllWinListItems = 1
				WriteStatusToLogs "The ListBox does not exist, please verify."
		End If
			
    On Error GoTo 0
End Function


'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name :				 VerifyListBoxDisabled()
' 	Purpose :					  Verifies whether the ListBox is Disabled.
'	Return Value :		 		  Integer( 0/1)
' 	Arguments :
'		Input Arguments :  		  objListBox 
'		Output Arguments : 
'	Function is called by :
'	Function calls :			  CheckObjectExist() , CheckObjectStatus()
'	Created on :				  14/03/2008
'	Author :					   Rathna Reddy 
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function VerifyWinListBoxDisabled(ByVal objListBox)

On Error Resume Next
	'Variable Section Begin
		
		Dim strMethod

	'Variable Section End
	strMethod = strKeywordForReporting
		If CheckObjectExist(objListBox) Then
			If Not(CheckObjectStatus(objListBox)) Then
					VerifyWinListBoxDisabled = 0
					WriteStatusToLogs "The Listbox is disabled."
				Else
					VerifyWinListBoxDisabled = 2
					WriteStatusToLogs "The Listbox is not disabled, please verify."
				End If
            
		Else
				VerifyWinListBoxDisabled = 1
				WriteStatusToLogs "The ListBox does not exist, please verify."
		End if
		
	On Error GoTo 0
    
End Function

'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name :			   VerifyNoDuplicationInListBox()
' 	Purpose :					  Verifies the duplicate items in the listbox
' 	Return Value :		 		Integer( 0/1)
' 	Arguments :
'		Input Arguments :  	 objListBox
'		Output Arguments : 	strItems , arrActItems() , strActItems
'	Function is called by : 
'	Function calls :			CheckObjectExist,GetArray(),VerifyDuplicateElemetsInArray()
'	Created on :				19/03/2008
'	Author :					   Rathna Reddy 
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function VerifyNoDuplicationInWinListBox(ByVal objListBox)
	On Error Resume Next
		'Variable Section Begin
			Dim strMethod
			Dim strActItems , arrActItems 
			
         'Variable Section End
		 strMethod = strKeywordForReporting
			If CheckObjectExist(objListBox)  Then   
				strActItems = objListBox.GetROProperty("all items")
                
					If Not IsEmpty(strActItems) Then
						arrActItems = GetArray(strActItems , chr(10))
						If Not IsNull(arrActItems) Then
					
								If VerifyDuplicateElementsInArray(arrActItems) = 0 Then
									VerifyNoDuplicationInWinListBox = 0
									  WriteStatusToLogs "ListBox has no duplicate item."
								Else
									VerifyNoDuplicationInWinListBox = 2
									  WriteStatusToLogs "ListBox contains duplicate item(s), please verify."
								End If

						 Else
								VerifyNoDuplicationInWinListBox = 1
								 WriteStatusToLogs "Action failed because the listItem array received is NULL."
						 End If
					
					Else
						VerifyNoDuplicationInWinListBox = 1
						WriteStatusToLogs "Action fails to get ListBox items, please verify."
					
					End if
			Else
				VerifyNoDuplicationInWinListBox = 1
				WriteStatusToLogs "ListBox does not exist, please verify."
            End If			
	On Error GoTo 0
End Function
'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name :			   SelectListItemByIndex()
' 	Purpose :					  To Select the List box item by index
' 	Return Value :		 		Integer( 0/1)
' 	Arguments :
'		Input Arguments :  	 intIndex
'		Output Arguments : 	
'	Function is called by : 
'	Function calls :			CheckObjectExist(0,
'	Created on :				19/03/2008
'	Author :					   Rathna Reddy 
'
'------------------------------------------------------------------------------------------------------------------------------------------

Function SelectWinListItemByIndex(objListBox, byval intIndex)
	
	On Error Resume Next
		'Variable Section Begin
			Dim strMethod
			Dim   intGetIndex, intItemCount 
			
         'Variable Section End
		 strMethod = strKeywordForReporting
	If CheckObjectExist(objListBox)  Then  
		intGetIndex = CInt(intIndex)
		'objListBox.Select Cstr("#"&intGetIndex)
		If intGetIndex >= 0 Then
				intItemCount = CInt(objListBox.GetROProperty("items count"))
				If intGetIndex < intItemCount Then
						objListBox.Select intGetIndex
						If Err.Number = 0 Then
							SelectWinListItemByIndex= 0
							WriteStatusToLogs "The item with index "& Cstr("#"&intGetIndex)&" is selected."
						Else
							SelectWinListItemByIndex= 1
							WriteStatusToLogs "Error occured while selecting list item with index :' "&intGetIndex&" ', please verify."
						End If
				Else
						SelectWinListItemByIndex= 1
						WriteStatusToLogs "The Index Number provided is out of range in the specified ListBox items Count, please verify."
							
				End If
		Else
				SelectWinListItemByIndex= 1
				WriteStatusToLogs "The index ' "& intGetIndex&" ' is not a positive integer, please verify."
		End If
	Else
	    SelectWinListItemByIndex = 1
		WriteStatusToLogs "The Listbox does not exist."
	End if
   On Error GOTO 0
End Function
'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :				 VerifyListBoxExist()
'	Purpose :					 Verifies whether the ListBox Exists or not?
'	Return Value :		 		  Integer( 0/1)
'	Arguments :
'		Input Arguments :  		  objListBox 
'		Output Arguments : 
'	Function is called by :
'	Function calls :			 CheckObjectExist() 
'	Created on :				   10/03/2008
'	Author :						  Rathna Reddy 
'
'------------------------------------------------------------------------------------------------------------------------------------------

Function VerifyWinListBoxExist(ByVal objListBox)
	On Error Resume Next
		'Variable Section Begin
	
		  Dim strMethod

		'Variable Section End
			strMethod = strKeywordForReporting
			If CheckObjectExist(objListBox) Then 
			   
					VerifyWinListBoxExist = 0
					WriteStatusToLogs "The ListBox Exist."
					
			Else
					VerifyWinListBoxExist = 2
					WriteStatusToLogs "The ListBox does not exist, please verify."
					
			End If 
	  
	
	On Error GoTo 0
End Function

'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :				 VerifyListBoxNotExist()
'	Purpose :					 Verifies whether the ListBox Exists or not?
'	Return Value :		 		  Integer( 0/1)
'	Arguments :
'		Input Arguments :  		  objListBox 
'		Output Arguments : 
'	Function is called by :
'	Function calls :			 CheckObjectExist() 
'	Created on :				   10/03/2008
'	Author :						  Rathna Reddy 
'
'------------------------------------------------------------------------------------------------------------------------------------------

Function VerifyWinListBoxNotExist(ByVal objListBox)
	On Error Resume Next
		'Variable Section Begin
	
		   Dim strMethod

		'Variable Section End
			strMethod = strKeywordForReporting
			If CheckObjectNotExist(objListBox) Then 
			   
					VerifyWinListBoxNotExist = 0
					WriteStatusToLogs "The ListBox does not Exist."
					
			Else
					VerifyWinListBoxNotExist = 2
					WriteStatusToLogs "The ListBox Exists, please verify."
					
			End If 
	
  		
	On Error GoTo 0
End Function


'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name :				  SelectMultipleListItems()
' 	Purpose :						 Selects multiple items from the ListBox
' 	Return Value :		 		   Integer( 0/1)
' 	Arguments :
'		Input Arguments :  		objListBox , arrItems
'		Output Arguments :     strActItem
'	Function is called by :
'	Function calls :				CheckObjectExist()
'	Created on :				   19/01/2009
'	Author :						 Rathna Reddy 
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function SelectMultipleWinListItems(ByVal objListBox , ByVal arrExpItems)
	On Error Resume Next
		'Variable Section Begin
			Dim strMethod
			Dim arrActItems,intItem
			Dim intarritemscount, iCount
			Dim bStatusExistance
			Dim intStatusExistance
			Dim stritemNotExists
			Dim strallitems, strActAllListItems
			Dim blnstatus 
			Dim strreportitem

			Dim arrActAllListItems
			
			blnstatus  = False
			intStatusExistance = 0
			bStatusExistance = Null
			stritemNotExists = Null
	
		'Variable Section End
		strMethod = strKeywordForReporting
		If CheckObjectExist(objListBox) Then   
			 strActAllListItems = objListBox.GetRoProperty("all items")
			   
				If  vartype(strActAllListItems) <> vbEmpty   Then
					blnstatus = True
				End If
				If blnstatus  Then

							 arrActAllListItems = Split(strActAllListItems, chr(10))
						
					   intarritemscount = Ubound(arrExpItems)
					   For iCount = 0 to intarritemscount
								If IsItemExistInArray(arrActAllListItems , arrExpItems(iCount)) Then
									bStatusExistance = True
								Else
									bStatusExistance = False
									stritemNotExists = arrExpItems(iCount)
								End if
								
								If bStatusExistance = False Then
									intStatusExistance = intStatusExistance+1
									strreportitem = arrExpItems(iCount)
								End If
					   Next
					     
					   If intStatusExistance =  0 Then
	   
									   If Not IsNull(arrExpItems) Then
											 For  intItem=0 to Ubound(arrExpItems)
												 If intItem = 0 Then
													objListBox.Select arrExpItems(intItem)
												 Else
													objListBox.ExtendSelect arrExpItems(intItem)
												End If
				
											 Next
													If Err.Number = 0 Then
														arrActItems = objListBox.GetRoProperty("selection")
														arrActItems=GetArray(arrActItems,chr(10))
																	If Err.Number=0 And Not IsNull(arrActItems) Then
																				If CompareArrays(arrExpItems , arrActItems) = 0 Then
																					SelectMultipleWinListItems = 0
																					WriteStatusToLogs "The Expected Multiple Items are selected sucessfully."
																				Else
																						SelectMultipleWinListItems = 2
																						 WriteStatusToLogs "Expected Multiple Items have not been selected, please verify."
																				End If
									
																	Else
																		 SelectMultipleWinListItems = 1
																		 WriteStatusToLogs "Could not capture selected items from ListBox."
																	End If
												 Else
													 SelectMultipleWinListItems = 1
													 WriteStatusToLogs "Action fails to get selected items, please verify."
												End If
												
								Else
										SelectMultipleWinListItems = 1
										WriteStatusToLogs "An error occurred, the Action fails to get the array of the Expected items , please verify."
								End If
								
				  Else
						SelectMultipleWinListItems = 1
						WriteStatusToLogs "List item ["&strreportitem &"] not Exist in the list box."

				End If
		Else'blnstatus
		SelectListItem=1
		WriteStatusToLogs "List item are Empty ListBox, please verify."
		End if'blnstatus
	Else
			SelectMultipleWinListItems = 1
			WriteStatusToLogs "The specified ListBox doesn't exist, please verify."
	End If
		On Error GoTo 0
End Function

'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name :				  VerifyMultipleSelectedListBoxItems()
' 	Purpose :					  Verifies the  selected items in ListBox
' 	Return Value :		 		  Integer( 0/1)
' 	Arguments :
'		Input Arguments :  		  objListBox , arrExpItems
'		Output Arguments :		  strActItem
'	Function is called by :
'	Function calls :			  CheckObjectExist()
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function VerifyMultipleSelectedWinListBoxItems(ByVal objListBox , ByVal arrExpItems)
	On Error Resume Next
		'Variable Section Begin
			Dim strMethod
			Dim arrActItems,intItem, blnstatus
			blnstatus = False
	
		'Variable Section End
		strMethod = strKeywordForReporting
		If CheckObjectExist(objListBox) Then
				If Not IsNull (arrExpItems) And Vartype(arrExpItems) <> vbEmpty Then
						arrActItems = objListBox.GetRoProperty("selection")
						
						If Vartype(arrActItems) <> vbEmpty  And Err.Number = 0 Then
							blnstatus = true
						End If
							
							
					   If blnstatus Then
								arrActItems=GetArray(arrActItems,chr(10))
								If MatchEachArrData(arrExpItems , arrActItems) = True Then
										VerifyMultipleSelectedWinListBoxItems = 0
										WriteStatusToLogs "The ListBox mulptiple selected items match the expected."
								Else
										VerifyMultipleSelectedWinListBoxItems = 2
										WriteStatusToLogs "ListBox selected items are different from the expected, please verify."
								End If

						Else
								 VerifyMultipleSelectedWinListBoxItems = 1
								 WriteStatusToLogs "Action fails to get actual list items into an array, please verify."
						End If

				Else
						VerifyMultipleSelectedWinListBoxItems = 1
						WriteStatusToLogs "The expected array items are either null/empty, please verify."
				End If
		Else
				VerifyMultipleSelectedWinListBoxItems = 1
				WriteStatusToLogs "The specified ListBox doesn't exist, please verify."
		End If

On Error GoTo 0
End Function

'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name :				 VerifyListBoxNOTContainsItem	()
' 	Purpose :					 Verifies  whether the ListBox doesn't contain the specified Item.
' 	Return Value :		 		   Integer( 0/1/2)
' 	Arguments :
'		Input Arguments :  	  	objListBox,strExpItems
'		Output Arguments :     sstrActItems , arrActItems , arrExpItems,intCount,strExpItem,intActItem
'	Function is called by :
'	Function calls :			   CheckObjectExist() , GetArray() 
'	Created on :				  23/01/2009
'	Author :						Rathna Reddy 
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function VerifyWinListBoxNOTContainsItem(ByVal objListBox , ByVal strListItem)
	On Error Resume Next
		'Variable Section Begin
			Dim strMethod
			Dim strActItems , arrActItems , arrExpItems,strExpItem,intActItem, blnEqualFlag, blnstatus
			blnEqualFlag=true
			blnstatus = false
						
		'Variable Section End
		strMethod = strKeywordForReporting
			If CheckObjectExist(objListBox)  Then   
				strActItems = objListBox.GetROProperty("all items")
				
                If Err.Number = 0 Then
						
											
								arrActItems = GetArray(strActItems , chr(10))
							If Vartype(arrActItems) <> vbEmpty  And Err.Number = 0 Then
								blnstatus = true
							End If
							   If blnstatus Then
								 
 													  For intActItem = 0 to Ubound(arrActItems)
																   If strListItem = arrActItems(intActItem) Then
																		blnEqualFlag=false
																		Exit for
																   End If
                                                      Next
														 
                    										 If blnEqualFlag = true  Then
																  VerifyWinListBoxNOTContainsItem = 0
																	WriteStatusToLogs "List Does not contain the specified item."
                                  
															  Else
																	 VerifyWinListBoxNOTContainsItem = 2
																	  WriteStatusToLogs "List contain the specified item, please verify."
														  End If
												   
									 
							Else
								 VerifyWinListBoxNOTContainsItem = 1
								 WriteStatusToLogs "An error occurred, action fails to get Actual items, please verify."
						 End If
						
				Else
						VerifyWinListBoxNOTContainsItem = 1
						WriteStatusToLogs "Action Fails to get Items from ListBox, please verify ListBox method and property."
				End If
			
	 Else
			VerifyWinListBoxNOTContainsItem = 1
			WriteStatusToLogs "ListBox doesn't exist, please verify."

	 End If			
    On Error GoTo 0
End Function
'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name :				 VerifyListBoxContainsItem		()
' 	Purpose :					 Verifies  whether the listBox contains the specified Item.
' 	Return Value :		 		   Integer( 0/1/2)
' 	Arguments :
'		Input Arguments :  	  	objListBox,strExpItems
'		Output Arguments :     sstrActItems , arrActItems , arrExpItems,intCount,strExpItem,intActItem
'	Function is called by :
'	Function calls :			   CheckObjectExist() , GetArray() 
'	Created on :				  23/01/2009
'	Author :						Rathna Reddy 
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function VerifyWinListBoxContainsItem(ByVal objListBox , ByVal strListItem)
	On Error Resume Next
		'Variable Section Begin
			Dim strMethod
			Dim strActItems , arrActItems , arrExpItems,intCount,strExpItem,intActItem , blnpresentFlag, blnstatus
			blnpresentFlag=false
			blnstatus= False
			
			
		'Variable Section End
			strMethod = strKeywordForReporting
			If CheckObjectExist(objListBox)  Then   
				strActItems = objListBox.GetROProperty("all items")
				
                 If Vartype(strActItems) <> vbEmpty  And Err.Number = 0 Then
							blnstatus = true
                    
							If blnstatus Then
									arrActItems = GetArray(strActItems , chr(10))				  
									If IsItemExistInArray(arrActItems,strListItem)  Then
											  VerifyWinListBoxContainsItem	 = 0
												WriteStatusToLogs "ListBox Contains the expected item '"&strListItem&"'"
									Else
												 VerifyWinListBoxContainsItem	 = 2
												  WriteStatusToLogs "List does not contain the expected item '"&strListItem&"', please verify."
									End If
												   
									 
							Else
									 VerifyWinListBoxContainsItem	 = 1
									 WriteStatusToLogs "Action fails to get Actual items into an array, please verify."
						 End If
						
					Else
						VerifyWinListBoxContainsItem	 = 1
						WriteStatusToLogs "Action Fails to get Items from ListBox, please verify ListBox method and property."
				End If
			
	 Else
	 VerifyWinListBoxContainsItem	 = 1
	 WriteStatusToLogs "ListBox doesn't exist, please verify."

	 End If			
    On Error GoTo 0
End Function

'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name :				 VerifyListBoxSize()
' 	Purpose :					 Verifies the size(no.of items) of ListBox
' 	Return Value :		 		   Integer( 0/1/2)
' 	Arguments :
'		Input Arguments :  	  	objListBox,intItemCount
'		Output Arguments :    intActCount,intCount
'	Function is called by :
'	Function calls :			   CheckObjectExist() , GetArray() 
'	Created on :				  23/01/2009
'	Author :						Rathna Reddy 
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function VerifyWinListBoxSize(ByVal objListBox , ByVal intExpectedSize)
	On Error Resume Next
		'Variable Section Begin
			Dim strMethod
			Dim intActCount,intCount
			
		'Variable Section End
			strMethod = strKeywordForReporting
			If CheckObjectExist(objListBox)  Then   
				intCount=Cint(intExpectedSize)
				
				If VarType(intCount) = VbInteger Then
				
				intActCount = objListBox.GetROProperty("items count")
					If Not IsEmpty(intActCount)  Then
							If CompareStringValues(intCount,intActCount)=0 Then
								  VerifyWinListBoxSize = 0
								  WriteStatusToLogs "The specified ListBox Size is same as the expected size."
							Else
								   VerifyWinListBoxSize = 2
									WriteStatusToLogs "The specified ListBox Size is different from the expected Size, please verify."
						End If
					Else
							  VerifyWinListBoxSize = 1
								WriteStatusToLogs "Action Fails to get ListBox Items Count, please verify ListBox and its method and property"
				End If
		Else

		   VerifyWinListBoxSize = 1
			 WriteStatusToLogs "The specified List box's expected size is not an Integer, please verify."
		

		End If
		Else
			 VerifyWinListBoxSize = 1
			 WriteStatusToLogs "The specified ListBox doesn't exist, please verify."

	 End If
			'WriteStepResults VerifyListBoxNOTContainsItem
    On Error GoTo 0
End Function

'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name :				  DeSelectListBoxItem()
' 	Purpose :					  Deselect one Selected item  in  the ListBox.The listbox can have multiple selected items.
' 	Return Value :		 		  Integer( 0/1)
' 	Arguments :
'		Input Arguments :  		objListBox , strListItem
'		Output Arguments :     strSelectedItem
'	Function is called by :
'	Function calls :				CheckObjectExist()
'	Created on :				   19/03/2008
'	Author :						 Rathna Reddy 
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function DeSelectWinListBoxItem(ByVal objListBox , ByVal strListItem)
	On Error Resume Next
		'Variable Section Begin
			Dim strMethod
			Dim strSelectedItem,arrExp, arrAct
			Dim strListDeselectedItem
			Dim arrListDeselectedItems
		
	
		'Variable Section End
			strMethod = strKeywordForReporting
			If CheckObjectExist(objListBox) Then   
          
					strSelectedItem = Trim(objListBox.GetRoProperty("Selection"))
					
					If Not IsEmpty(strSelectedItem) Then
						 
							arrAct=GetArray(strSelectedItem,chr(10))
							If Not IsNull(arrAct) Then
									If IsItemExistInArray(arrAct,strListItem)  Then 
											objListBox.Deselect Trim(strListItem)
											
											If Err.Number = 0 Then
													 strListDeselectedItem = Trim(objListBox.GetRoProperty("Selection"))
													 arrListDeselectedItems = GetArray(strListDeselectedItem,chr(10))
													If Not  IsItemExistInArray(arrListDeselectedItems, strListItem)Then
																DeSelectWinListBoxItem = 0
																WriteStatusToLogs "The "&strListItem&" List Item is deselected successfully."
														
													Else
																DeSelectWinListBoxItem = 2
																WriteStatusToLogs "List item is not deselected successfully, please verify."
													End If
											Else
												   DeSelectWinListBoxItem = 1
													 WriteStatusToLogs "Error occurred during the deselection of list item."

											End IF

									Else
											 DeSelectWinListBoxItem = 1
											 WriteStatusToLogs "Either the expected item is not in selection mode OR the item does not exist in the list item, please verify."

									End If
							Else
									DeSelectWinListBoxItem = 1
									WriteStatusToLogs "An error occurred, action fails to get the Array of the Expected and Actual selected item, please verify."
							End IF
						
					Else
							DeSelectWinListBoxItem = 1
							WriteStatusToLogs "Either the Action fails to get the selected list item OR no item is selected in the list, please verify."
					End If
			 Else
						
					   
						DeSelectWinListBoxItem = 1
					    WriteStatusToLogs "ListBox does not exist, please verify."
						
			 End If
	On Error GoTo 0
End Function
'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name :				  DeSelectMultipleListBoxItems()
' 	Purpose :						 DeSelects  selected multiple items from the ListBox
' 	Return Value :		 		   Integer( 0/1)
' 	Arguments :
'		Input Arguments :  		objListBox , arrItems
'		Output Arguments :     strActItem
'	Function is called by :
'	Function calls :				CheckObjectExist()
'	Created on :				   19/01/2009
'	Author :						 Rathna Reddy 
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function DeSelectMultipleWinListBoxItems(ByVal objListBox , ByVal arrExpItems)
	On Error Resume Next
		'Variable Section Begin
			Dim strMethod
			Dim arrActItems,intRes,strExpItem
			Dim intCountBeforeDeselect,intCountAfterDeselect,intCount,intExpItemsSize
			
	
		'Variable Section End
		strMethod = strKeywordForReporting
		If CheckObjectExist(objListBox) Then
				If Not IsNull(arrExpItems) Then
						'Before Deselect
						arrActItems = objListBox.GetRoProperty("selection")
						
						If arrActItems <> Empty Then
								arrActItems=GetArray(arrActItems,chr(10))
								intCountBeforeDeselect=UBound(arrActItems) + 1 'objListBox.GetRoProperty("selected items count")
								If Not IsNull(arrActItems) Then
										If  CompareArrays(arrExpItems, arrActItems) = 0 Then
												For Each strExpItem in arrExpItems
														objListBox.Deselect Trim(strExpItem)
												Next

												arrActItems = objListBox.GetRoProperty("selection")
												arrActItems=GetArray(arrActItems,chr(10))
												intCountAfterDeselect= UBound(arrActItems) + 1'objListBox.GetRoProperty("selected items count")  
												intCount = intCountBeforeDeselect - intCountAfterDeselect
												intExpItemsSize=Ubound(arrExpItems)

												If IsEmpty(arrActItems)  or  intExpItemsSize = intCount-1  Then
														DeSelectMultipleWinListBoxItems = 0
														WriteStatusToLogs "The expected List items are deselected successfully."

												Else
														 DeSelectMultipleWinListBoxItems =  2
														 WriteStatusToLogs "The expected List items are not deselected successfully, please verify."
												End If 

										Else
												DeSelectMultipleWinListBoxItems =  1
												WriteStatusToLogs "The Expected few/all item(s) are either not in selected mode or does not exist, please verify."
										End If	 															
								 Else
										DeSelectMultipleWinListBoxItems =  1
										WriteStatusToLogs "Action fails to get ListItems into an array, please verify."
																   
								 End If
							 
						Else
								DeSelectMultipleWinListBoxItems =  1
								WriteStatusToLogs "Failed to retrieve the actual selected list items. Either all/no items are selected to be deselect or an error occurred, please verify."
						End If

				Else
						DeSelectMultipleWinListBoxItems =  1
						WriteStatusToLogs "The expected liste items are either null/empty, please verify."
				End If

		Else
				DeSelectMultipleWinListBoxItems =  1
				WriteStatusToLogs "The ListBox doesn't exist, please verify."
		End If 

On Error GoTo 0
End Function
'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name :				 VerifyListBoxEnabled()
' 	Purpose :					 Verifies whether the ListBox is Enabled
' 	Return Value :		 		  Integer( 0/1)
' 	Arguments :
'		Input Arguments :  		objListBox 
'		Output Arguments : 
'	Function is called by :
'	Function calls :				CheckObjectExist() , CheckObjectStatus()
'	Created on :				   11/03/2008
'	Author :						  Rathna Reddy 
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function VerifyWinListBoxEnabled(ByVal objListBox)

On Error Resume Next
	'Variable Section Begin
		
		Dim strMethod

	'Variable Section End
		strMethod = strKeywordForReporting
		If CheckObjectExist(objListBox) Then
			If CheckObjectEnabled(objListBox) Then
					VerifyWinListBoxEnabled = 0
					WriteStatusToLogs "The ListBox was enabled."
				Else
					VerifyWinListBoxEnabled = 2
					WriteStatusToLogs "The ListBox was not enabled, please verify."
				End If
            
		Else
				VerifyWinListBoxEnabled = 1
				WriteStatusToLogs "The ListBox does not exist, please verify."
		End if
		
	On Error GoTo 0
    
End Function

'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :				 VerifyListBoxVisible()
'	Purpose :					 Verifies whether the ListBox is visible.
'	Return Value :		 		  Integer( 0/1)
'	Arguments :
'		Input Arguments :  		  objListBox 
'		Output Arguments : 
'	Function is called by :
'	Function calls :			 CheckObjectExist() , CheckObjectVisiblity()
'	Created on :				   10/03/2008
'	Author :						  Rathna Reddy 
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function VerifyWinListBoxVisible(ByVal objListBox)
	On Error Resume Next
		'Variable Section Begin
		
			Dim strMethod

		'Variable Section End
			strMethod = strKeywordForReporting
			If CheckObjectExist(objListBox)  Then 
				If CheckObjectVisible(objListBox) Then
					VerifyWinListBoxVisible = 0
					WriteStatusToLogs "The ListBox was Visible."
				Else
					VerifyWinListBoxVisible = 2
					WriteStatusToLogs "The ListBox was not Visible, please verify."
				End If
			Else
					VerifyWinListBoxVisible = 1
					WriteStatusToLogs "The ListBox does not exist, please verify."
			End If
		
	On Error GoTo 0
End Function
'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :				 VerifyListBoxInVisible()
'	Purpose :					 Verifies whether the ListBox is Invisible.
'	Return Value :		 		  Integer( 0/1)
'	Arguments :
'		Input Arguments :  		  objListBox 
'		Output Arguments : 
'	Function is called by :
'	Function calls :			 CheckObjectExist() , CheckObjectVisiblity()
'	Created on :				   10/03/2008
'	Author :						  Rathna Reddy 
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function VerifyWinListBoxInVisible(ByVal objListBox)
	On Error Resume Next
		'Variable Section Begin
		
			Dim strMethod

		'Variable Section End
			strMethod = strKeywordForReporting
			If CheckObjectExist(objListBox)  Then 
				If CheckObjectInVisible(objListBox) Then
					VerifyWinListBoxInVisible = 0
					WriteStatusToLogs "The ListBox was InVisible."
				Else
					VerifyWinListBoxInVisible = 2
					WriteStatusToLogs "The ListBox was not InVisible, please verify."
				End If
			Else
					VerifyWinListBoxInVisible = 1
					WriteStatusToLogs "The ListBox does not exist, please verify."
			End If
		
	On Error GoTo 0
End Function




'=======================Code Section End=============================================


