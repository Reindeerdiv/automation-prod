

'=======================Code Section Begin============================================

Option Explicit		 'Forces explicit declaration of all variables in a script. 


'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :				 VerifyDialogExist()
'	Purpose :					 Verifies the existence of Dialog in AUT.
'	Return Value :		 		  Integer( 0/1)
'	Arguments :
'		Input Arguments :  		  objDialog 
'		Output Arguments : 
'	Function is called by :
'	Function calls :			 CheckObjectExist() 
'	Created on :				      15/01/2009
'	Author :						  Rathna Reddy 
'
'------------------------------------------------------------------------------------------------------------------------------------------

Function VerifyWinDialogExist(ByVal objDialog)
	On Error Resume Next
		'Variable Section Begin
	
		  Dim strMethod

		'Variable Section End
			strMethod = strKeywordForReporting
			If CheckObjectExist(objDialog) Then 
			   
					VerifyWinDialogExist = 0
					WriteStatusToLogs "The Dialog Exists."
					
			Else
					VerifyWinDialogExist = 1
					WriteStatusToLogs "The Dialog doesn't exist, please verify."
					
			End If 
	  
	
	On Error GoTo 0
End Function

'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :				 VerifyDialogNotExist()
'	Purpose :					 Verifies whether the Dialog is NotExist or not in AUT?.
'	Return Value :		 		  Integer( 0/1)
'	Arguments :
'		Input Arguments :  		  objDialog 
'		Output Arguments : 
'	Function is called by :
'	Function calls :			 CheckObjectExist() 
'	Created on :				   15/01/2009
'	Author :						  Rathna Reddy 
'
'------------------------------------------------------------------------------------------------------------------------------------------

Function VerifyWinDialogNotExist(ByVal objDialog)
	On Error Resume Next
		'Variable Section Begin
	
		   Dim strMethod

		'Variable Section End
			strMethod = strKeywordForReporting
			If  CheckObjectNotExist(objDialog) Then 
			   
					VerifyWinDialogNotExist = 0
					WriteStatusToLogs "The Dialog doesn't exist."
					
			Else
					VerifyWinDialogNotExist = 1
					WriteStatusToLogs "The Dialog exists, Please verify."
					
			End If 
	
  		
	On Error GoTo 0
End Function

'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :			ActivateDialog()
'	Purpose :				Activates the Specified Dialog.	
'	Return Value :		 	Integer( 0/1)
'	Arguments :
'		Input Arguments :   objDialog    
'		Output Arguments :  intVal
'	Function is called by : 
'	Function calls :		CheckObjectExist()
'	Created on : 			04/03/2008
'	Author :				Rathna Reddy
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function ActivateWinDialog(ByVal objDialog)
	On Error Resume Next
		'Variable Section Begin
			Dim strMethod
			Dim intVal,strTitle

		'Variable Section End
			strMethod = strKeywordForReporting
			If CheckObjectExist(objDialog) Then
				 intVal=objDialog.GetROProperty("hwnd")
				  
				 Window("hwnd:=" & intVal).Activate
				 
				   If Err.Number = 0 Then
						ActivateWinDialog  = 0
						WriteStatusToLogs "The Dialog was activated successfully."
				   Else
						ActivateWinDialog = 1 
						WriteStatusToLogs "The Dialog was not activated successfully, please verify."
				   End If
			
			Else
				ActivateWinDialog = 1 
				WriteStatusToLogs "The Dialog doesn't exist, please verify."
			End If
	  
   On Error GoTo 0 
End Function

