

'=======================Code Section Begin=============================================

Option Explicit 

'
'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name :				  SelectListItem()
' 	Purpose :						 Selects a specified item from the ListBox
' 	Return Value :		 		   Integer( 0/1)
' 	Arguments :
'		Input Arguments :  		objMenuControl , strSelectItem
'		Output Arguments :     strActItem
'	Function is called by :
'	Function calls :				CheckObjectExist()
'	Created on :				   19/03/2008
'	Author :						 Rathna Reddy 
' 
'------------------------------------------------------------------------------------------------------------------------------------------
Function SelectMenuItem(ByVal objMenuControl , ByVal arrMenuitems)
	On Error Resume Next
		'Variable Section Begin
			Dim strMethod
			Dim strActItem, strallitems
			Dim blnstatus 
			Dim intitemcount
			Dim strGetBuildPath
			Dim strMenuitemswithComma
			Dim strsingleMenu
			Dim intcountSelectItem
			Dim blnSelectStatus
			Dim ItemPath
			Dim ItemSelectionPath
			
			blnSelectStatus = False
			intcountSelectItem = 0
			intitemcount = 0
			blnstatus  = false
			strGetBuildPath = Null
			'strMenuitemswithComma = Null
			ItemSelectionPath = Null
			strMenuitemswithComma  = ""
	
		'Variable Section End
		strMethod = strKeywordForReporting
		If IsNull(arrMenuitems) Then
				SelectMenuItem = 1
				WriteStatusToLogs "The data provided to select is empty, please verify."	
		Else
				If CheckObjectExist(objMenuControl) Then  			

						For intitemcount = 0 to ubound(arrMenuitems)
									strsingleMenu = Trim( arrMenuitems(intitemcount))
									strMenuitemswithComma = strMenuitemswithComma&strsingleMenu
									
									ItemPath = objMenuControl.BuildMenuPath(strMenuitemswithComma) 
									'If objMenuControl.GetItemProperty (ItemPath,"Exists") Then
											'blnSelectStatus = True
											'ItemSelectionPath = ItemPath
									'ElseIf intitemcount = ubound(arrMenuitems) Then
										'blnSelectStatus = True
									'Else
										'blnSelectStatus = False
										'End if
										' report for the list menu item not existing
									'End If
									blnSelectStatus = True
									If Not blnSelectStatus Then
										intcountSelectItem = intcountSelectItem+1
										Exit for
									End If
									strMenuitemswithComma = strMenuitemswithComma&";"
						Next

						If intcountSelectItem = 0  Then
								ItemPath = replace(ItemPath, "/t",Vbtab)
								objMenuControl.Select ItemPath
								If Err.Number = 0  Then
									   SelectMenuItem = 0
									   WriteStatusToLogs "The Items "&ItemPath&" are selected."
								Else
									   SelectMenuItem = 1
									   WriteStatusToLogs "Failed to select the Items "&ItemPath&" . An error occurred, please verify. Error: "&Err.description
								End If
						Else
								SelectMenuItem = 1
								WriteStatusToLogs "Either the menu item is not available in the list or an error occured, please verify."
						End If
			   
		
				Else
					SelectMenuItem = 1
					WriteStatusToLogs "The Item does not exist, please verify."
				End If
		End If
		On Error GoTo 0
End Function
'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name :				 VerifyAllListItems()
' 	Purpose :					 Verifies  all the list items with our expected
' 	Return Value :		 		   Integer( 01)
' 	Arguments :
'		Input Arguments :  	  	objMenuControl,strExpItems
'		Output Arguments :     strActItems , arrActItems() , arrExpItems()
'	Function is called by :
'	Function calls :			   CheckObjectExist() , GetArray() , MatchEachArrData()
'	Created on :				  19/03/2008
'	Author :						Rathna Reddy 
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function VerifySubListItemsInMenu(ByVal objMenuControl , Byval MenuItem, ByVal strExpMenuItems)
	On Error Resume Next
		'Variable Section Begin
		
			Dim strMethod
			Dim arrMenuitems
			Dim blnSubItem
			Dim intSutMenuCount
			Dim n
			Dim arrSubMenuitems
			Dim Paths
			blnSubItem =False
            'Variable Section End
			strMethod = strKeywordForReporting
			If CheckObjectExist(objMenuControl)  Then
                arrMenuitems = GetArray( strExpMenuItems, "^")

				If IsMenuItemExistsInMenuObject(objMenuControl, MenuItem)  Then
				arrSubMenuitems = GetSubMenuItems(objMenuControl, MenuItem)
				If  VarType(arrSubMenuitems) <> VbEmpty Then
					If CompareArraysInSequence(strExpMenuItems, arrSubMenuitems) = 0 Then
					VerifySubListItemsInMenu = 0
					Else
					VerifySubListItemsInMenu = 2
				End If
			Else
			VerifySubListItemsInMenu = 1
			End If

					  
				Else
				 VerifySubListItemsInMenu = 1
				 WriteStatusToLogs "The Header Menu is not existing im menu list."
				End If

			 Else
			VerifySubListItemsInMenu = 1
			WriteStatusToLogs "The Menu box is not Existing, please verify."
			End If
			'WriteStepResults VerifyListItems
    On Error GoTo 0
End Function


'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name :				 VerifyListBoxDisabled()
' 	Purpose :					  Verifies whether the ListBox is Disabled.
'	Return Value :		 		  Integer( 0/1)
' 	Arguments :
'		Input Arguments :  		  objMenuControl 
'		Output Arguments : 
'	Function is called by :
'	Function calls :			  CheckObjectExist() , CheckObjectStatus()
'	Created on :				  14/03/2008
'	Author :					   Rathna Reddy 
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function VerifySubListItemsActivatedInMenu(ByVal objMenuControl , Byval MenuItem, ByVal strExpMenuItems)
	On Error Resume Next
		'Variable Section Begin
		
			Dim strMethod
			Dim arrMenuitems
			Dim blnSubItem
			Dim intSutMenuCount
			Dim n
			Dim arrSubMenuitems
			Dim Paths
			blnSubItem =False
            'Variable Section End
			strMethod = strKeywordForReporting
			If CheckObjectExist(objMenuControl)  Then
                arrMenuitems = GetArray( strExpMenuItems, "^")

				If IsMenuItemExistsInMenuObject(objMenuControl, MenuItem)  Then
				arrSubMenuitems = GetActivatedSubMenuItems(objMenuControl, MenuItem)
				If  VarType(arrSubMenuitems) <> VbEmpty Then
					If CompareArraysInSequence(strExpMenuItems, arrSubMenuitems) = 0 Then
					VerifySubListItemsActivatedInMenu = 0
					Else
					VerifySubListItemsActivatedInMenu = 2
				End If
			Else
			VerifySubListItemsActivatedInMenu = 1
			End If

					  
				Else
				 VerifySubListItemsActivatedInMenu = 1
				 WriteStatusToLogs "The Header Menu is not existing im menu list."
				End If

			 Else
			VerifySubListItemsActivatedInMenu = 1
			WriteStatusToLogs "The Menu box is not Existing, please verify."
			End If
			'WriteStepResults VerifyListItems
    On Error GoTo 0
End Function



'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name :				 VerifySubListItemsNotActivatedInMenu()
' 	Purpose :					  Verifies whether the ListBox is Disabled.
'	Return Value :		 		  Integer( 0/1)
' 	Arguments :
'		Input Arguments :  		  objMenuControl 
'		Output Arguments : 
'	Function is called by :
'	Function calls :			  CheckObjectExist() , CheckObjectStatus()
'	Created on :				  14/03/2008
'	Author :					   Rathna Reddy 
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function VerifySubListItemsNotActivatedInMenu(ByVal objMenuControl , Byval MenuItem, ByVal strExpMenuItems)
	On Error Resume Next
		'Variable Section Begin
		
			Dim strMethod
			Dim arrMenuitems
			Dim blnSubItem
			Dim intSutMenuCount
			Dim n
			Dim arrSubMenuitems
			Dim Paths
			blnSubItem =False
            'Variable Section End

			strMethod = strKeywordForReporting
			If CheckObjectExist(objMenuControl)  Then
                arrMenuitems = GetArray( strExpMenuItems, "^")

				If IsMenuItemExistsInMenuObject(objMenuControl, MenuItem)  Then
				arrSubMenuitems = GetNonActivatedSubMenuItems(objMenuControl, MenuItem)
				If  VarType(arrSubMenuitems) <> VbEmpty Then
					If CompareArraysInSequence(strExpMenuItems, arrSubMenuitems) = 0 Then
					VerifySubListItemsNotActivatedInMenu = 0
					Else
					VerifySubListItemsNotActivatedInMenu = 2
				End If
			Else
			VerifySubListItemsNotActivatedInMenu = 1
			End If

					  
				Else
				 VerifySubListItemsNotActivatedInMenu = 1
				 WriteStatusToLogs "The Header Menu is not existing im menu list."
				End If

			 Else
			VerifySubListItemsNotActivatedInMenu = 1
			WriteStatusToLogs "The Menu box is not Existing, please verify."
			End If
			'WriteStepResults VerifyListItems
    On Error GoTo 0
End Function


'=======================Code Section End=============================================
