'=======================Code Section Begin=============================================
Option Explicit 

'
'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name :				  DoubleClickOnNodeInList()
' 	Purpose :						 Selects a specified item from the ListBox
' 	Return Value :		 		   Integer( 0/1)
' 	Arguments :
'		Input Arguments :  		objListView , strSelectItem
'		Output Arguments :     strActItem
'	Function is called by :
'	Function calls :				CheckObjectExist()
'	Created on :				   19/03/2008
'	Author :						 Rathna Reddy  
'------------------------------------------------------------------------------------------------------------------------------------------
Function DoubleClickOnNodeInList(ByVal objListView , ByVal strNodeListName)
	On Error Resume Next
		'Variable Section Begin
			Dim strMethod
			Dim blnstatus, blnMatch
			Dim strListContent, strTemplistItems
			Dim arrListContents
			Dim intmaxlength, intcount
			
			
			blnstatus = false
			strListContent = Null
			blnMatch = False
			intmaxlength  = 0
			
       
		'Variable Section End
		strMethod = strKeywordForReporting
			If CheckObjectExist(objListView) Then
				strListContent = objListView.GetContent

				If Vartype(strListContent) <> vbNull  And Err.Number = 0 Then
					blnstatus = true
				End If
					If blnstatus  Then
					arrListContents = GetArray(strListContent, chr(10))
					intmaxlength = ubound(arrListContents)
                    

					For intcount = 0 to intmaxlength
							strTemplistItems = arrListContents(intcount)
                            If Ucase(Trim(strTemplistItems))  = UCase(Trim(strNodeListName)) Then
								blnMatch = True
								Exit for
							End If
					Next

									  If  blnMatch Then
										  objListView.Activate strNodeListName
										  If Err.Number = 0 Then
											  DoubleClickOnNodeInList = 0
											  Else
											  DoubleClickOnNodeInList = 1
										  End If
										Else'blnMatch
										DoubleClickOnNodeInList = 1
										WriteStatusToLogs "The Node Items['"&strNodeListName&"'] is not existing."
										End If   		   
						Else'blnstatus
						DoubleClickOnNodeInList = 1
						WriteStatusToLogs "An error occurred while Click operation in ListView."
						End If
			Else
				DoubleClickOnNodeInList = 1
				WriteStatusToLogs "The List  View Object  does not exist, please verify."
			End If
		On Error GoTo 0
End Function

'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name :				 SelectNodeInList()
' 	Purpose :					 Verifies  all the list items with our expected
' 	Return Value :		 		   Integer( 01)
' 	Arguments :
'		Input Arguments :  	  	objListView,strExpItems
'		Output Arguments :     strActItems , arrActItems() , arrExpItems()
'	Function is called by :
'	Function calls :			   CheckObjectExist() , GetArray() , MatchEachArrData()
'	Created on :				  19/03/2008
'	Author :						Rathna Reddy 
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function SelectNodeInList(ByVal objListView , ByVal strNodeListName)
	On Error Resume Next
		'Variable Section Begin
			Dim strMethod
			Dim blnstatus, blnMatch
			Dim strListContent, strTemplistItems, strTempNode
			Dim arrListContents
			Dim intmaxlength, intcount
			
		  
			blnstatus = false
			blnMatch = False
			strListContent = Null
			
		'Variable Section End
		strMethod = strKeywordForReporting
			If CheckObjectExist(objListView) Then
				strListContent = objListView.GetContent

				If Vartype(strListContent) <> vbNull  And Err.Number = 0 Then
					blnstatus = true
				End If
					If blnstatus  Then

					arrListContents = GetArray(strListContent, chr(10))
					intmaxlength = ubound(arrListContents)
					
					For intcount = 0 to intmaxlength
							strTemplistItems = UCase(Trim(arrListContents(intcount)))
							strTempNode = Ucase(Trim(strNodeListName))
							
						   If strTemplistItems= strTempNode  Then
							   blnMatch = True
							   Exit for
						   End If
                           Next

									  If  blnMatch Then
															objListView.Select strNodeListName
															If Err.Number = 0  Then
																SelectNodeInList = 0
																Else
																  SelectNodeInList = 1
																		WriteStatusToLogs "Error Occured during Node['"&strNodeListName&"'] Select Operation."
															End If
											Else'blnMatch
											SelectNodeInList = 1
											WriteStatusToLogs "The Expand Items are not existing."
										End If   		   
						Else'blnstatus
						SelectNodeInList = 1
						WriteStatusToLogs "An error occurred while Getting the ListView Nodes."
						End If
			Else
				SelectNodeInList = 1
				WriteStatusToLogs "The Tree View Object does not exist, please verify."
			End If
		On Error GoTo 0
End Function


'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name :				 DeSelectNodeInList()
' 	Purpose :					 Verifies  all the list items with our expected
' 	Return Value :		 		   Integer( 01)
' 	Arguments :
'		Input Arguments :  	  	objListView,strExpItems
'		Output Arguments :     strActItems , arrActItems() , arrExpItems()
'	Function is called by :
'	Function calls :			   CheckObjectExist() , GetArray() , MatchEachArrData()
'	Created on :				  19/03/2008
'	Author :						Rathna Reddy 
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function DeSelectNodeInList(ByVal objListView , ByVal strNodeListName)
	On Error Resume Next
		'Variable Section Begin
			Dim strMethod
			Dim blnstatus, blnMatch
			Dim strListContent, strTemplistItems, strTempNode
			Dim arrListContents
			Dim intmaxlength, intcount
			
		  
			blnstatus = false
			blnMatch = False
			strListContent = Null
			
		'Variable Section End
			strMethod = strKeywordForReporting
			If CheckObjectExist(objListView) Then
					strListContent = objListView.GetContent

					If Vartype(strListContent) <> vbNull  And Err.Number = 0 Then
							blnstatus = true
					End If

					If blnstatus  Then

							arrListContents = GetArray(strListContent, chr(10))
							intmaxlength = ubound(arrListContents)

							For intcount = 0 to intmaxlength
									strTemplistItems = UCase(Trim(arrListContents(intcount)))
									strTempNode = Ucase(Trim(strNodeListName))
									
									If CompareStringValues(strTemplistItems,strTempNode) = 0  Then
									   blnMatch = True
									   Exit for
								   End If
							Next

							  If  blnMatch Then
										objListView.Deselect  strNodeListName
										If Err.Number = 0  Then
												 DeSelectNodeInList = 0
												 WriteStatusToLogs "The item '"&strNodeListName&"' is deselected."
										Else
												 DeSelectNodeInList = 1
												 WriteStatusToLogs "Failed to deselect the item '"&strNodeListName&"'. An error occurred, please verify. Error: "&Err.description
										End If
								Else'blnMatch
										DeSelectNodeInList = 1
										WriteStatusToLogs "The item '"&strNodeListName&"' does not exist."
								End If   		   
						Else'blnstatus
						DeSelectNodeInList = 1
						WriteStatusToLogs "An error occurred while Getting the ListView Nodes."
						End If
			Else
					DeSelectNodeInList = 1
					WriteStatusToLogs "The Tree View Object does not exist, please verify."
			End If

On Error GoTo 0
End Function

'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name :				 VerifyNodeExistingInListView
' 	Purpose :					  Verifies whether the ListBox is Disabled.
'	Return Value :		 		  Integer( 0/1)
' 	Arguments :
'		Input Arguments :  		  objListView 
'		Output Arguments : 
'	Function is called by :
'	Function calls :			  CheckObjectExist() , CheckObjectStatus()
'	Created on :				  14/03/2008
'	Author :					   Rathna Reddy 
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function VerifyNodeExistingInListView(ByVal objListView , ByVal NodeElementName)
	On Error Resume Next
	'Variable Section Begin
		Dim strMethod
		Dim blnstatus, blnMatch
		Dim strListContent , strTemplistItems
		Dim arrListContents
		Dim intmaxlength, intcount

		strListContent = Null
		blnstatus = false
		blnMatch = False

	'Variable Section End

		strMethod = strKeywordForReporting
		If CheckObjectExist(objListView) Then
				strListContent = objListView.GetContent

				If Vartype(strListContent) <> vbNull  And Err.Number = 0  Then
						arrListContents = GetArray(strListContent, chr(10))
						intmaxlength = ubound(arrListContents)

						For intcount = 0 to intmaxlength
								strTemplistItems = arrListContents(intcount)
								If  Ucase(Trim(strTemplistItems)) =Ucase(Trim(NodeElementName)) Then
										blnMatch = true
										Exit for
								End If
						Next

						If  blnMatch And Err.Number = 0 Then
								VerifyNodeExistingInListView = 0
								WriteStatusToLogs "The item '"&NodeElementName&"' exist."
						Else'blnMatch
								VerifyNodeExistingInListView = 1
								WriteStatusToLogs "The item '"&NodeElementName&"' does not exist, please verify. "&Err.description
						End If   		   
				Else'blnstatus
						VerifyNodeExistingInListView = 1
						WriteStatusToLogs "An error occurred while Getting the ListView Node Items, please verify. Error: "&Err.description
				End If
		Else
			VerifyNodeExistingInListView = 1
			WriteStatusToLogs "The Tree View Object does not exist, please verify."
		End If

On Error GoTo 0
End Function



'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name :				 VerifyNodeNotExistingInListView
' 	Purpose :					  Verifies whether the ListBox is Disabled.
'	Return Value :		 		  Integer( 0/1)
' 	Arguments :
'		Input Arguments :  		  objListView 
'		Output Arguments : 
'	Function is called by :
'	Function calls :			  CheckObjectExist() , CheckObjectStatus()
'	Created on :				  14/03/2008
'	Author :					   Rathna Reddy 
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function VerifyNodeNotExistingInListView(ByVal objListView , ByVal NodeElementName)
	On Error Resume Next
			Dim strMethod
	  		Dim blnstatus, blnMatch
			Dim strListContent, strTempListNode
			Dim arrListContents
			Dim intmaxlength, intcount, intTemparrcount, intTreeItems, intMatchCount
			
			

			strListContent = Null
			blnstatus = false
			blnMatch = False
			intMatchCount = 0
			'blnDropMatch = False
		'Variable Section End
		strMethod = strKeywordForReporting
			If CheckObjectExist(objListView) Then
				strListContent = objListView.GetContent

				If Vartype(strListContent) <> vbNull  And Err.Number = 0 Then
					blnstatus = true
				End If
					If blnstatus  Then
                        arrListContents = GetArray(strListContent, chr(10))
                        intmaxlength = ubound(arrListContents)
					
					For intcount = 0 to intmaxlength
							strTempListNode = arrListContents(intcount)
								If UCase(Trim(strTempListNode)) = UCase(Trim(NodeElementName)) Then
                                    blnMatch = True
									 intMatchCount = intMatchCount+1
                                     Exit for
                                     Else
										 blnMatch = False
									End If
					Next

									  If  intMatchCount = 0  And Err.Number = 0 Then
											VerifyNodeNotExistingInListView = 0
                                      Else'blnMatch
											VerifyNodeNotExistingInListView = 1
											WriteStatusToLogs "The Item ['"&NodeElementName&"'] is not existing in Tree view."
										End If   		   
						Else'blnstatus
						VerifyNodeNotExistingInListView = 1
						WriteStatusToLogs "An error occurred while Getting the Tree List Nodes."
						End If
			Else
				VerifyNodeNotExistingInListView = 1
				WriteStatusToLogs "The Tree View Object does not exist, please verify."
			End If
		On Error GoTo 0
End Function


'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name :				  SelectMultipleItemsInListView()
' 	Purpose :					  Selects multiple items from the ListView
' 	Return Value :		 		  Integer( 0/1)
' 	Arguments :
'		Input Arguments :  		  objListBox , arrItems
'		Output Arguments :        strActItem
'	Function is called by :
'	Function calls :			  CheckObjectExist()
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function SelectMultipleItemsInListView(ByVal objListView , ByVal arrExpItems)
	On Error Resume Next
		'Variable Section Begin
			Dim strMethod
			Dim arrActItems, arrListContents
			Dim strTempListNode, strListContent
			Dim intItem, intmaxlength, intExpLenth, intfailCount, intActLenth, intActcount
			Dim blnMatch, blnstatus
			Dim NodeElementName
			blnMatch = False
			intfailCount = 0
	
		'Variable Section End
		strMethod = strKeywordForReporting
		If CheckObjectExist(objListView) Then
				
				If Not IsNull(arrExpItems) Or Not IsEmpty(arrExpItems) Then
						strListContent = objListView.GetContent

						If Vartype(strListContent) <> vbNull  And Err.Number = 0 Then
								blnstatus = true
						End If

						If blnstatus  Then
								arrListContents = GetArray(strListContent, chr(10))
								intActLenth = Ubound(arrListContents) 
								
								For each NodeElementName in arrExpItems
										For intActcount = 0 to intActLenth
												strTempListNode = arrListContents(intActcount)
												If UCase(Trim(strTempListNode)) = UCase(Trim(NodeElementName)) Then
														blnMatch = True
														Exit for
												End If
										Next
									
										If Not blnMatch Then									
												intfailCount = intfailCount+1
												WriteToInfoLog "The Expected item['"&NodeElementName &"'] Not in the List"									
										End If
										blnMatch = False
								Next

								If intfailCount = 0 Then														   
									   
										For  intItem=0 to Ubound(arrExpItems)
												If intItem = 0 Then
														objListView.Select arrExpItems(intItem)
												Else
														objListView.ExtendSelect arrExpItems(intItem)
												End If
										Next
										If Err.Number = 0 Then
								 
												arrActItems = objListView.GetRoProperty("selection")
												arrActItems=GetArray(arrActItems,chr(10))
												If Err.Number=0 And Not IsNull(arrActItems) Then
														If CompareArrays(arrExpItems , arrActItems) = 0 Then
																SelectMultipleItemsInListView = 0
																WriteStatusToLogs "The Expected Multiple Items are selected sucessfully."
														Else
																SelectMultipleItemsInListView = 2
																 WriteStatusToLogs "Expected Multiple Items have not been selected, please verify."
														End If

												Else'Err.Number=0 And Not IsNull(arrActItems)
														 SelectMultipleItemsInListView = 1
														 WriteStatusToLogs "Could not capture selected items from ListBox."
												End If

										Else'Err.Number = 0
												SelectMultipleItemsInListView = 1
												WriteStatusToLogs "An error occurred while selecting the items. Error:"&Err.description&", please verify."
										End If
										
								Else' intfailcount
										SelectMultipleItemsInListView = 1
										WriteStatusToLogs "Some of the items to be selected are not existing in ListView, please verify."

								End if
						Else'blnstatus
								SelectMultipleItemsInListView = 1
								WriteStatusToLogs "An error occurred while retrieving the Tree List Nodes, please verify."
						End If
				Else'Not IsNull(arrExpItems) 
						SelectMultipleItemsInListView = 1
						WriteStatusToLogs "The expected items are either null/empty, please verify."
				End If
		Else
				SelectMultipleItemsInListView = 1
				WriteStatusToLogs "The specified ListBox doesn't exist, please verify."
		End If

On Error GoTo 0
End Function


'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name :				 VerifyChildNodesNotExistingInParentNode
' 	Purpose :					  Verifies whether the ListBox is Disabled.
'	Return Value :		 		  Integer( 0/1)
' 	Arguments :
'		Input Arguments :  		  objListView 
'		Output Arguments : 
'	Function is called by :
'	Function calls :			  CheckObjectExist() , CheckObjectStatus()
'	Created on :				  14/03/2008
'	Author :					   Rathna Reddy 
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function RenameNodeName(ByVal objListView , ByVal NodeElementName, byval NodeNewName)
	On Error Resume Next
			Dim strMethod
	  		Dim blnstatus, blnMatch, blnRenameMatch
			Dim strListContent , strTemplistItems
			Dim arrListContents
			Dim intmaxlength, intcount, intrenamelength

			strListContent = Null
			blnstatus = false
			blnMatch = False
			blnRenameMatch = False
			
		'Variable Section End
			strMethod = strKeywordForReporting
			If CheckObjectExist(objListView) Then
				strListContent = objListView.GetContent

				If Vartype(strListContent) <> vbNull  And Err.Number = 0 Then
					blnstatus = true
				End If
					If blnstatus  Then
                        arrListContents = GetArray(strListContent, chr(10))
						intmaxlength = ubound(arrListContents)
					
					For intcount = 0 to intmaxlength
							strTemplistItems = arrListContents(intcount)
								If  Ucase(Trim(strTemplistItems)) =Ucase(Trim(NodeElementName)) Then
                                   blnMatch = true
										 Exit for
								End If
					Next
					
					For intcount = 0 to intrenamelength
							strTemplistItems = arrListContents(intcount)
								If  Ucase(Trim(strTemplistItems)) =Ucase(Trim(NodeNewName)) Then
                                   blnRenameMatch = true
										 Exit for
								End If
					Next
					
					If blnRenameMatch Then
						RenameNodeName = 1
						WriteStatusToLogs "An error occurred folder Name Already existing."
						
						Exit Function
					End If
									  If  blnMatch  Then
										  objListView.EditLabel NodeElementName
										  objListView.WinEdit("nativeclass:=Edit").Set NodeNewName
										  If Err.Number = 0  Then
											  RenameNodeName = 0
											  WriteStatusToLogs "Renamed node successfully."
										  Else
											  RenameNodeName = 1
											  WriteStatusToLogs "An error occurred while renaming node name. Error:"&err.description
										  End If

                                        Else'blnMatch
											RenameNodeName = 1
											WriteStatusToLogs "The Item ['"&NodeElementName&"'] is not existing in Tree view."
										End If   		   
						Else'blnstatus
						RenameNodeName = 1
						WriteStatusToLogs "An error occurred while Getting the ListView Node Items."
						End If
			Else
				RenameNodeName = 1
				WriteStatusToLogs "The Tree View Object does not exist, please verify."
			End If
		On Error GoTo 0


End Function


'------------------------------------------------------------------------------------------------------------------------------------------
Function VerifyNodeSelected(ByVal objListView , Byval strNodeElement)
	On Error Resume Next
		'Variable Section Begin
			Dim strMethod
			Dim blnstatus, blnMatch
			Dim strListContent, strTreelistItems
			Dim arrItems, arrDropItems, arrListContents
			Dim intmaxlength, intcount
			Dim arrTreeListItems
			Dim intTemparrcount, intTreeItems
			Dim strTempSelectedNode

			strListContent = Null
			blnstatus = false
			blnMatch = False
			'blnDropMatch = False
		'Variable Section End
			strMethod = strKeywordForReporting
			If CheckObjectExist(objListView) Then
				strListContent = objListView.GetContent

				If Vartype(strListContent) <> vbNull  And Err.Number = 0 Then
					blnstatus = true
				End If
					If blnstatus  Then
                    arrListContents = GetArray(strListContent, chr(10))
					intmaxlength = ubound(arrListContents)
					
					For intcount = 0 to intmaxlength
						If UCase(Trim(arrListContents(intcount)))  = UCase(Trim(strNodeElement))Then
							blnMatch =True
							Exit for
						End If
					Next

									  If  blnMatch Then
												strTempSelectedNode = objListView.GetSelection
													If CompareStringValues(strTempSelectedNode, strNodeElement ) = 0 And Err.Number = 0 Then
                                                    VerifyNodeSelected = 0
													WriteStatusToLogs "Verified that the Node "&strNodeElement&" is selected."
                                                   Else
													  VerifyNodeSelected = 1
															WriteStatusToLogs "The Node is not selected or focused."
												End If
							
		
			
											Else'blnMatch
											VerifyNodeSelected = 1
											WriteStatusToLogs "The Expand Items are not existing."
										End If   		   
						Else'blnstatus
						VerifyNodeSelected = 1
						WriteStatusToLogs "An error occurred while Getting the Tree List Nodes."
						End If
			Else
				VerifyNodeSelected = 1
				WriteStatusToLogs "The List View Object does not exist, please verify."
			End If
		On Error GoTo 0
End Function


'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :				 VerifyListViewVisible()
'	Purpose :					 Verifies whether the EditBox is visible.
'	Return Value :		 		  Integer( 0/1)
'	Arguments :
'		Input Arguments :  		  objListView 
'		Output Arguments : 
'	Function is called by :
'	Function calls :			 CheckObjectExist() , CheckObjectVisiblity()
'	Created on :				   10/03/2008
'	Author :						  Rathna Reddy 
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function VerifyListViewVisible(ByVal objListView)
	On Error Resume Next
		'Variable Section Begin
		
			Dim strMethod

		'Variable Section End
			strMethod = strKeywordForReporting
			If CheckObjectExist(objListView)  Then 
				If CheckObjectVisible(objListView) Then
					VerifyListViewVisible = 0
					WriteStatusToLogs "The ListView was visible as expected."
				Else
					VerifyListViewVisible = 2
					WriteStatusToLogs "The ListView was not visible,Please verify."
				End If
			Else
					VerifyListViewVisible = 1
					WriteStatusToLogs "The ListView does not exist, please verify."
			End If
		
	On Error GoTo 0
End Function
'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :				 VerifyListViewInVisible()
'	Purpose :					 Verifies whether the ListView is Invisible.
'	Return Value :		 		  Integer( 0/1)
'	Arguments :
'		Input Arguments :  		  objListView 
'		Output Arguments : 
'	Function is called by :
'	Function calls :			 CheckObjectExist() , CheckObjectVisiblity()
'	Created on :				   10/03/2008
'	Author :						  Rathna Reddy 
'
'------------------------------------------------------------------------------------------------------------------------------------------

Function VerifyListViewInVisible(ByVal objListView)
	On Error Resume Next
		'Variable Section Begin
		
			Dim strMethod

		'Variable Section End
			strMethod = strKeywordForReporting
			If CheckObjectExist(objListView)  Then 
				If CheckObjectInVisible(objListView) Then
					VerifyListViewInVisible = 0
					WriteStatusToLogs "The ListView was in-visible as expected."
				Else
					VerifyListViewInVisible = 2
					WriteStatusToLogs "The ListView was not in-visible, please verify."
				End If
			Else
					VerifyListViewInVisible = 1
					WriteStatusToLogs "The ListView does not exist, please verify."
			End If
		
	On Error GoTo 0
End Function


'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name :				 VerifyListViewEnabled()
' 	Purpose :					 Verifies whether the Button is Enabled
' 	Return Value :		 		  Integer( 0/1)
' 	Arguments :
'		Input Arguments :  		objButton 
'		Output Arguments : 
'	Function is called by :
'	Function calls :				CheckObjectExist() , CheckObjectStatus()
'	Created on :				   11/03/2008
'	Author :						  Rathna Reddy 
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function VerifyListViewEnabled(ByVal objListView)

On Error Resume Next
	'Variable Section Begin
		
		Dim strMethod

	'Variable Section End
		strMethod = strKeywordForReporting
		If CheckObjectExist(objListView) Then
			If CheckObjectEnabled(objListView) Then
					VerifyListViewEnabled = 0
					WriteStatusToLogs "The ListView was enabled as expected."
				Else
					VerifyListViewEnabled = 2
					WriteStatusToLogs "The ListView was not enabled, please verify."
				End If
            
		Else
				VerifyListViewEnabled = 1
				WriteStatusToLogs "The ListView does not exist, please verify."
		End if
		
	On Error GoTo 0
    
End Function

'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name :				 VerifyListViewDisabled()
' 	Purpose :					 Verifies whether the ListView is Disabled
' 	Return Value :		 		  Integer( 0/1)
' 	Arguments :
'		Input Arguments :  		objListView 
'		Output Arguments : 
'	Function is called by :
'	Function calls :				CheckObjectExist() , CheckObjectStatus()
'	Created on :				   11/03/2008
'	Author :						  Rathna Reddy 
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function VerifyListViewDisabled(ByVal objListView)

On Error Resume Next
	'Variable Section Begin
		
		Dim strMethod

	'Variable Section End
		strMethod = strKeywordForReporting
		If CheckObjectExist(objListView) Then
			If CheckObjectDisabled(objListView) Then
					VerifyListViewDisabled = 0
					WriteStatusToLogs "The ListView was disabled as expected."
				Else
					VerifyListViewDisabled = 2
					WriteStatusToLogs "The ListView was not disabled, please verify."
				End If
            
		Else
				VerifyListViewDisabled = 1
				WriteStatusToLogs "The ListView does not exist, please verify."
		End if
	On Error GoTo 0
End Function


'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name :				  SelectListItem()
' 	Purpose :						 Selects a specified item from the ListBox
' 	Return Value :		 		   Integer( 0/1)
' 	Arguments :
'		Input Arguments :  		objWinTree , strSelectItem
'		Output Arguments :     strActItem
'	Function is called by :
'	Function calls :				CheckObjectExist()
'	Created on :				   19/03/2008
'	Author :						 Rathna Reddy  
'------------------------------------------------------------------------------------------------------------------------------------------
Function DragItemDropInItem(ByVal objListView , ByVal strDragitem, byval strDropItem)
	On Error Resume Next
		'Variable Section Begin
			Dim strMethod
			Dim blnstatus, blnDragMatch, blnDropMatch
			Dim strListContent
			Dim arrDragItems, arrDropItems, arrListContents
			Dim intmaxlength, intcount
            Dim strDragTreelistItems, strDropTreelistItems
			Dim arrDragTreeListItems, arrDropTreeListItems
            Dim intContentItems, intDragItems, intDropItems, intTemparrcount
			strListContent = Null
			blnstatus = false
			blnDragMatch = False
			blnDropMatch = False
       
		'Variable Section End
			strMethod = strKeywordForReporting
			If CheckObjectExist(objListView) Then
				strListContent = objListView.GetContent

				If Vartype(strListContent) <> vbNull  And Err.Number = 0 Then
					blnstatus = true
				End If
					If blnstatus  Then

					arrListContents = GetArray(strListContent, chr(10))
					intmaxlength = ubound(arrListContents)
                    
					For intcount = 0 to intmaxlength
						strDragTreelistItems = arrListContents(intcount)
								If UCase(Trim(strDragTreelistItems)) = Ucase(Trim(strDragitem))Then
									 blnDragMatch = true
								 Exit for
								End If
	 
					Next

									  If  blnDragMatch Then
											  For intcount = 0 to intmaxlength
												  strDropTreelistItems = arrListContents(intcount)
																 If Ucase(Trim(strDropTreelistItems)) = UCase(Trim(strDropItem)) Then
																	blnDropMatch = true
																	 Exit for
																End If
 
											Next
											
														If blnDropMatch Then
															'objListView.Select strDragTreelistItems
															objListView.DragItem strDragTreelistItems
															objListView.DropOnItem strDropTreelistItems
																		If err.Number = 0 Then
																				DragItemDropInItem = 0
																				WriteStatusToLogs "The item "&strDragitem&" is drag and drop on item "&strDropItem
																		Else
																				DragItemDropInItem = 1
																				WriteStatusToLogs "Error Occured during drag and drop Operation."
																		End If
							
		
															Else'blnDropMatch
																DragItemDropInItem = 1
																WriteStatusToLogs "The Drop Items are not existing."					
															End If
			
											Else'blnDragMatch
											DragItemDropInItem = 1
											WriteStatusToLogs "The Drag Items are not existing."
										End If   		   
						Else'blnstatus
						DragItemDropInItem = 1
						WriteStatusToLogs "An error occurred while Getting the Tree List Nodes."
						End If
			Else
				DragItemDropInItem = 1
				WriteStatusToLogs "The Tree View Object does not exist, please verify."
			End If
		On Error GoTo 0
End Function


'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name :				  StoreWinListViewRowCount()
' 	Purpose :					  Stores the row count of the ListView
' 	Return Value :		 		  Integer( 0/1)
' 	Arguments :
'		Input Arguments :  		  objListView , strKey
'		Output Arguments :        
'	Function is called by :
'	Function calls :			  CheckObjectExist()
'	Created on :				  10/02/2012
'	Author :					  Iohbha K 
'------------------------------------------------------------------------------------------------------------------------------------------
Function StoreWinListViewRowCount(ByVal objListView, ByVal strKey)

On Error Resume Next
	'Variable Section Begin
		
		Dim strMethod, intActRowCnt, intval

	'Variable Section End
		If IsKey(strKey) = 0 Then
				If CheckObjectExist(objListView) Then
						
						intActRowCnt = getWinListViewRowCount(objListView)
						If intActRowCnt > -1 Then 
								intval = AddItemToStorageDictionary(strKey,intActRowCnt)
								If  intval = 0 Then
										StoreWinListViewRowCount = 0
										WriteStatusToLogs "The row count '"&intActRowCnt&"'  is stored successfully in the Key '"&strKey&"'." 
											  
								Else
										StoreWinListViewRowCount = 1
										WriteStatusToLogs "Failed to store the row count in the Key '"&strKey&"', please verify."  
												  
								End If
						Else
								StoreWinListViewRowCount = 1
								WriteStatusToLogs "Failed to retrive the row count an error occurred, please verify. "&Err.description 		
						End if		
					
				Else
						StoreWinListViewRowCount = 1
						WriteStatusToLogs "The ListView does not exist, please verify."
				End If
		Else
				StoreWinComboBoxSelectedItem = 1
				WriteStatusToLogs "The key is invalid, please verify."
		End if
		
	On Error GoTo 0
    
End Function


'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :				 StoreWinListViewCellText()
'	Purpose :					 Stores the data in the cell specified by the row number and column name
'	Return Value :		 		 Integer(0/1)
'	Arguments :
'		Input Arguments :  		 objListView,strRowNum,strColName,strKey
'		Output Arguments :       
'	Function is called by :
'	Function calls :			 CheckObjectNotExist(),getWinListViewRowCount 
'	Created on :				 10/02/2012
'	Author :					 Iohbha K 
'
'------------------------------------------------------------------------------------------------------------------------------------------

Function StoreWinListViewCellText (Byval objListView,Byval strRowNum , strColName,Byval strKey)

   On Error Resume Next


    'Variable Section Begin
		
		 Dim strMethod	
		 Dim intRowNo,intActualRowCount , strCellVal  

	'Variable Section End

		strMethod = strKeywordForReporting
		If IsPositiveInteger(strRowNum) <> 0 Then
				 StoreWinListViewCellText = 1
				 WriteStatusToLogs  "The Row Number "&intRowNum&" is invalid, it can be any number >0, please verify"	
				 Exit function
		End If
		intRowNo = CInt(strRowNum)

		If IsNull(strColName) Or IsEmpty(strColName) Then
				 StoreWinListViewCellText = 1
				 WriteStatusToLogs  "The Column Name is either null/empty, please verify."	
				 Exit function
		End If

		If IsKey(strKey) <> 0 Then
				 StoreWinListViewCellText = 1
				 WriteStatusToLogs  "The key is invalid, please verify."	
				 Exit function
		End If


		If CheckObjectExist(objListView) Then 
				intActualRowCount = getWinListViewRowCount(objListView) 
				If intActualRowCount > -1 Then
						If intActualRowCount = 0 Then
								StoreWinListViewCellText = 1
								WriteStatusToLogs  "The ListView have "&intActualRowCount&" rows, please verify."
						Else
								If intRowNo > intActualRowCount Then
										StoreWinListViewCellText = 1
										WriteStatusToLogs  "Failed to find row:"&intRowNo&". The ListView have "&intActualRowCount&" rows, please verify."
								Else
										strCellVal = objListView.GetSubItem(intRowNo-1,strColName)
										If Err.number = 0 Then
												intval = AddItemToStorageDictionary(strKey,strCellVal)
												If  intval = 0 Then
														StoreWinListViewCellText = 0
														WriteStatusToLogs "Successfully stored the cell data:"&strCellVal&" of the row:"&strRowNum&", Column Name:"&strColName
															  
												Else
														StoreWinListViewCellText = 1
														WriteStatusToLogs "Failed to store the cell data:"&strCellVal&" of the row:"&strRowNum&", Column Name:"&strColName&", please verify. "&err.description
																  
												End If
										Else
												StoreWinListViewCellText = 1
												WriteStatusToLogs  "Failed to retrieve the cell data of the row:"&strRowNum&", Column Name:"&strColName&", please verify. "&err.description
												
										End if
								End if
						End if
						
				Else
						StoreWinListViewCellText = 1
						WriteStatusToLogs  "Failed to check the row count,Please verify."
				End if
		Else
				StoreWinListViewCellText = 1
				WriteStatusToLogs "The ListView does not exist,Please verify."


		End If
					  		
 On Error GoTo 0  
End Function


'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :				 StoreWinListViewInstanceOfDataInColumn()
'	Purpose :					 Stores the instance of the data in the specified column
'	Return Value :		 		 Integer(0/1)
'	Arguments :
'		Input Arguments :  		 objListView,strRowNum,strColName,strKey
'		Output Arguments :       
'	Function is called by :
'	Function calls :			 CheckObjectNotExist(),getWinListViewRowCount 
'	Created on :				 10/02/2012
'	Author :					 Iohbha K 
'
'------------------------------------------------------------------------------------------------------------------------------------------

Function StoreWinListViewInstanceOfDataInColumn (Byval objListView,strColName,Byval strCellData, ByVal strKey)
On Error Resume Next


    'Variable Section Begin
		
		 Dim strMethod	
		 Dim row, intActualRowCount, strCellVal, intInstance, intval  

	'Variable Section End
		intInstance = 0
		If IsNull(strColName) Or IsEmpty(strColName) Then
				 StoreWinListViewInstanceOfDataInColumn = 1
				 WriteStatusToLogs  "The Column Name is either null/empty, please verify."	
				 Exit function
		End If

		If IsNull(strCellData) Or IsEmpty(strCellData) <> 0 Then
				 StoreWinListViewInstanceOfDataInColumn = 1
				 WriteStatusToLogs  "The cell data is invalid, please verify."	
				 Exit function
		End If

		If IsKey(strKey) <> 0 Then
				 StoreWinListViewInstanceOfDataInColumn = 1
				 WriteStatusToLogs "The key is invalid, please verify."	
				 Exit function
		End If


		If CheckObjectExist(objListView) Then 
				intActualRowCount = getWinListViewRowCount(objListView) 
				If intActualRowCount >0 Then 
						
						For row= 0 To (intActualRowCount-1)
								strCellVal = objListView.GetSubItem(row,strColName)
								If Err.number = 0 Then
										If CompareStringValues(strCellVal ,strCellData) = 0 Then
												intInstance = intInstance + 1
										End If
								Else
										StoreWinListViewInstanceOfDataInColumn = 1
										WriteStatusToLogs "An error occurred. Probably reasons are: The column name is incorrect, or no data exist in the List, please verify."
										Exit Function
							
								End if
						Next
						
						intval = AddItemToStorageDictionary(strKey,intInstance)
						If  intval = 0 Then
								StoreWinListViewInstanceOfDataInColumn = 0
								WriteStatusToLogs "Successfully stored the instance "&intInstance&" of the cell data:"&strCellData&" in the Column Name:"&strColName
									  
						Else
								StoreWinListViewInstanceOfDataInColumn = 1
								WriteStatusToLogs "Failed to store the instance "&intInstance&" of the cell data:"&strCellData&" in the Column Name:"&strColName
										  
						End If
				Else
						StoreWinListViewInstanceOfDataInColumn = 1
						WriteStatusToLogs  "Either the ListView does not have any data or an error occurred, please verify."
				End if
		Else
				StoreWinListViewInstanceOfDataInColumn = 1
				WriteStatusToLogs  "The ListView does not exist, please verify."


		End If
					  		
On Error GoTo 0  
End Function




Function getWinListViewRowCount(objListView)
On Error Resume Next
		Dim intActRowCnt
		intActRowCnt = objListView.GetItemsCount  
		If Err.number <> 0 Or IsNull(intActRowCnt) Or IsEmpty(intActRowCnt) Then
				getWinListViewRowCount = -1
		Else
				getWinListViewRowCount = CInt(intActRowCnt)
		End iF
On Error goto 0
End function
'=======================Code Section End=============================================
