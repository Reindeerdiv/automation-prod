Function OpenSAPApplication(ByVal AppPath,byVal strValue)
	On Error Resume Next
		'Variable Section Begin
			
		Dim strMethod 
		
		If trim(AppPath) = "" or trim(strValue) = "" Then
			OpenSAPApplication = 1
			WriteStatusToLogs "Parameter value is empty."
			Exit function
		End If 
		err.number = 0
		strMethod = strKeywordForReporting  
		SystemUtil.Run AppPath
		 		  
		If err.Number = 0 Then
			Sapguiutil.OpenConnection strValue 
			If err.Number = 0 Then
				OpenSAPApplication = 0
				WriteStatusToLogs "The '"&strValue&"' SAP application has opened successfully."
			Else
				OpenSAPApplication = 1
				WriteStatusToLogs "Could not open SAP application. Verify parameter 'Value'"
			End If
		Else
			OpenSAPApplication = 1
			WriteStatusToLogs "Could not open SAP application. Verify AppPath."
	  
		End If
 
	On Error GoTo 0
End Function
'----------------------------------------------------------------------------

Function CSelectJTreeNode(byval objJavaTree)
on error resume next

	Dim strMethod
	Dim strSearchNodeText, ObjSAPGuiTree, ObjKeyValues, intNodeCount, blnFlag, i, strNodeText, SAP_Search_And_Activate_Node_In_SAPGuiTree, intErrNo
	strSearchNodeText =""

     strMethod = strKeywordForReporting

	Set ObjSAPGuiTree = objJavaTree.Object
	Set ObjKeyValues = ObjSAPGuiTree.GetAllNodeKeys 
	intNodeCount = ObjKeyValues.Count 
	
  	blnFlag=False
  
    	'Iterate through the nodes of the tree
   		 For i = 0 to intNodeCount-1

        		'Get the node text
       			 strNodeText=ObjSAPGuiTree.GetNodeTextByKey(ObjKeyValues(i))     			
	
        			'Check if the match was found for the key that you are looking for
        			'if yes then activate the item

        			If Instr(strNodeText,strSearchNodeText)>0  Then

            			'Select the node and double click on it
            			'This is equivalent to ActivateItem
            			 ObjSAPGuiTree.SelectNode ObjKeyValues(i)
            			 ObjSAPGuiTree.DoubleClickNode ObjKeyValues(i)
            			 blnFlag=True 'set a flag to indicate the macth was found
						  Exit For
        			End If
    
    	 Next

		If not blnFlag Then
			CSelectJTreeNode = 1
			WriteStatusToLogs "Node not found in the tree."
			Exit function
		End If

		'Release the objects
		 Set ObjKeyValues=Nothing
		 Set ObjSAPGuiTree=Nothing
		intErrNo = err.number
		If intErrNo = 0 Then
			CSelectJTreeNode = 0
			WriteStatusToLogs "The specified SAP Tree node is selected."
		Else 
			CSelectJTreeNode = 1
			WriteStatusToLogs "Error in selecting node, please verify parameters."
		End If
On error goto 0
End Function



Function SAPGetCellData(byVal objSAP,byval strKey, byval strRow, byval strCol)
		On error resume next
		Dim cellValue, strMethod, intval
		strMethod = strKeywordForReporting
		
		If (trim(strRow) = "" or trim(strCol) = "" or trim(strKey) = "") Then
			SAPGetCellData= 1
					WriteStatusToLogs "Empty parameters."
			Exit function
		End if 

		If CheckObjectExist(objSAP) Then   

			cellValue = objSAP.GetCellData(strRow,strCol)	
				If not Err.Number=0 Then
					 WriteStatusToLogs "Unable to fetch data from cell. Verify parameters."
					 SAPGetCellData= 1
					 Exit function
				Else
					intval = AddItemToStorageDictionary(strKey,cellValue)
					If intval = 0 Then
							 SAPGetCellData = 0
							 WriteStatusToLogs "The value  '"& cellValue &"'  is stored successfully in the Key '"&strKey&"'" 
					Else
								  SAPGetCellData = 1
						  WriteStatusToLogs "The value  '"& cellValue &"'  is NOT stored successfully in the Key '"&strKey&"'" 
					End If
				End If
		Else
				SAPGetCellData = 1
				WriteStatusToLogs "Object does not exist, please verify."
		End If
	On error goto 0
End Function

Function SAPSelectAll(byVal objSAP)
		On Error Resume Next

		Dim strMethod 

		strMethod = strKeywordForReporting
		  If CheckObjectExist(objSAP) Then
		  		    objSAP.SelectAll
						If err.Number = 0 Then
							 SAPSelectAll = 0
								WriteStatusToLogs "The SAPGuiGrid object was select successfully."
						Else
							SAPSelectAll = 1
							WriteStatusToLogs "The SAPGuiGrid object was not selected, Please verify."
						End If
				Else
						SAPSelectAll = 1
						WriteStatusToLogs "The SAPGuiGrid does not exist, Please verify."
				End If
	On Error GoTo 0
End Function

Function SAPClick(byVal objSAP)
	On Error Resume Next
	
		'Variable Section Begin
		Dim strMethod 
		'Variable Section End
		
		strMethod = strKeywordForReporting
		
		If CheckObjectExist(objSAP) Then
				objSAP.Click
				If err.number = 0Then
				 SAPClick = 0
					WriteStatusToLogs "successfully clicked."
				Else
					SAPClick = 1
					WriteStatusToLogs "Error occurred while clicking. "&  err.description
				End If
		Else
				SAPClick = 1
					WriteStatusToLogs "Object does not exist. "
		End If

	On Error GoTo 0
End Function

Function SAPStoreRowCount(byVal objSAP,byVal strKey)
	On Error Resume Next
		'Variable Section Begin
			
		Dim strMethod
		Dim	strValue, sRow, intval

		'Variable Section End
		strMethod = strKeywordForReporting
		
		If trim(strKey) = "" Then
			SAPStoreRowCount= 1
					WriteStatusToLogs "Empty parameters."
			Exit function
		End if 
		
		If CheckObjectExist(objSAP) Then
			 sRow=objSAP.GetROProperty("rowcount")
			 strValue=sRow

			 intval = AddItemToStorageDictionary(strKey,strValue)

			If  intval = 0 Then
						SAPStoreRowCount = 0
						WriteStatusToLogs "The string '"&strValue&"'  is stored successfully in the Key '"&strKey&"'" 
								  
			Else
				SAPStoreRowCount = 1
				WriteStatusToLogs "The string '"&strValue&"'  is not stored successfully in the Key '"&strKey&"'" 
						  
			End If
				     				  
		Else
			SAPStoreRowCount = 1
			WriteStatusToLogs "Object does not exist."
		End If
	On Error GoTo 0
End Function

Function SAPOpenPossibleEntries(byVal objSAP)
	On Error Resume Next
		'Variable Section Begin
			
		Dim strMethod 

		'Variable Section End
		strMethod = strKeywordForReporting
		  If CheckObjectExist(objSAP) Then
		  
		  				    objSAP.OpenPossibleEntries
						 If err.Number = 0 Then
							SAPOpenPossibleEntries = 0
								WriteStatusToLogs "Window successfully opened."
							Else
								SAPOpenPossibleEntries = 1
								WriteStatusToLogs "Error occurred while OpenPossibleEntries, Please verify parameters."
						  
							End If
				     				  
				Else
						SAPOpenPossibleEntries = 1
						WriteStatusToLogs "Object does not exist, Please verify."
				End If
	On Error GoTo 0
End Function
'------------------------------------------------------------------------------------------------------
		
Function SAPStoreValFromGrid(byVal objSAP,byVal skey)
 On Error Resume Next
		'Variable Section Begin
		Dim i,j,Row_count,Col_Count,arr_size,c,sValue
		Dim arr()
		c=1
		sValue=""
		'Variable Section End
			
		If trim(skey) = "" Then
			SAPStoreValFromGrid= 1
					WriteStatusToLogs "Empty parameters."
			Exit function
		End if 
			
			If CheckObjectExist(objSAP) Then
			
				Row_count=objSAP.RowCount
				Col_Count=objSAP.ColumnCount
				
				If Row_count < 1 or Col_Count < 1 Then
					SAPStoreValFromGrid = 1
					WriteStatusToLogs "Number of rows/ columns are less than 1."	
					Exit Function
				End If
				arr_size=Row_count*Col_Count
				ReDim arr(arr_size)

					For i = 1 To Row_count
						For j = 1 To Col_Count
						arr(c)=objSAP.GetCellData(i,"#"&j)
						sValue=sValue&","&arr(c)
						
						c=c+1  
						Next
					Next
					intval = AddItemToStorageDictionary(sKey,sValue)
					If intval = 0 Then
							SAPStoreValFromGrid = 0
								WriteStatusToLogs "'"&sValue&"' has been store successfully in key'"&sKey&"' "
							Else
								SAPStoreValFromGrid = 1
								WriteStatusToLogs "Error occurred while storing value in key '"&sKey&"', Varify parameters. "						  
							End If
			Else
						SAPStoreValFromGrid = 1
						WriteStatusToLogs "Object does not exist."
		
			End If	
On Error GoTo 0			
End Function


Function SAPSelectTab(byVal objSAP,byVal strValue)
	On Error Resume Next
		'Variable Section Begin
			
		Dim strMethod 
		
		If trim(strValue) = "" Then
			SAPSelectTab= 1
					WriteStatusToLogs "Empty parameters."
			Exit function
		End if 
		
		strMethod = strKeywordForReporting
		 
		 If CheckObjectExist(objSAP) Then
		  
		  				    objSAP.Select strValue
		  				    'msgbox strValue
						 If err.Number = 0 Then
							 SAPSelectTab = 0
								WriteStatusToLogs "Tab '"&strValue&"' selected sucessfully."
							Else
								SAPSelectTab = 1
								WriteStatusToLogs "The '"&strValue&"' was not select successfully, Please verify."
						  
							End If
				     				  
				Else
						CSelectTab = 1
						WriteStatusToLogs "Object does not exist, Please verify."
				End If
	On Error GoTo 0
End Function

Function SelectSAPTreeNode(byval objSAP,byval strValue)
on error resume next

'msgbox objJavaTree
    'Variable Section Begin
 Dim strMethod, ObjSAPGuiTree, ObjKeyValues, intNodeCount, i, strNodeText
 Dim strSearchNodeText
 strSearchNodeText =strValue
    'Variable Section End
			
     strMethod = strKeywordForReporting
	 
	            If CheckObjectExist(objSAP) Then

						Set ObjSAPGuiTree = objSAP.Object
						Set ObjKeyValues = ObjSAPGuiTree.GetAllNodeKeys 
						intNodeCount = ObjKeyValues.Count
 
					blnFlag=False
  
								'Iterate through the nodes of the tree
									For i = 0 to intNodeCount-1

											'Get the node text
											strNodeText=ObjSAPGuiTree.GetNodeTextByKey(ObjKeyValues(i))        
 
											'Check if the match was found for the key that you are looking for
											'if yes then activate the item

									If Instr(strNodeText,strSearchNodeText)>0  Then
									'msgbox strNodeText
									
											'Select the node and double click on it
											'This is equivalent to ActivateItem
												ObjSAPGuiTree.SelectNode ObjKeyValues(i)
												ObjSAPGuiTree.DoubleClickNode ObjKeyValues(i)
												
									blnFlag=True 'set a flag to indicate the macth was found
							
							Exit For
							End If
    
								Next

								'Chk the flag and return values to function
								
							If not blnFlag Then
										SelectSAPTreeNode = 1
										WriteStatusToLogs "Node not found in the treee."
										Exit function   
							End If

								'Release the objects
									Set ObjKeyValues=Nothing
									Set ObjSAPGuiTree=Nothing
										intErrNo = err.number
									If intErrNo = 0 Then
										SelectSAPTreeNode = 0
										WriteStatusToLogs "The specified SAP Tree node is selected."
									Else 
										SelectSAPTreeNode = 1
										WriteStatusToLogs "The specified SAP Tree node is not select, please verify"
									End If
						Else
						SelectSAPTreeNode = 1
						WriteStatusToLogs "The '"&objSAP&"' does not exist, Please verify."	
					End If
	On error goto 0
	End Function
'-----------------------------------------------------------------
Function SelectSAPRadioBtn(ByVal SAPbtn)
	On Error Resume Next
		'Variable Section Begin
			
				Dim strMethod 

		'Variable Section End
		strMethod = strKeywordForReporting  
		  				SAPbtn.Set
		  				 
						  
						 If err.Number = 0 Then
							SelectSAPRadioBtn = 0
								WriteStatusToLogs "The '"&strValue&"' Radio button selected successfully."
							Else
								SelectSAPRadioBtn = 1
								WriteStatusToLogs "The '"&strValue&"' Failed to select radio button."
						  
							End If
				 
	On Error GoTo 0
End Function
'------------------------------------------------------------------------------------------
Function CheckSAPCheckbox(ByVal SAPchk,byVal strValue)
	On Error Resume Next
		'Variable Section Begin
			
		Dim strMethod 
		
		If ucase(trim(strValue)) = "ON" or ucase(trim(strValue)) = "OFF" Then
		
		Else
			CheckSAPCheckbox = 1
					WriteStatusToLogs "Invalid input. select ON or OFF to select or deselect the checkbox."
			Exit function
		End if

		'Variable Section End
		strMethod = strKeywordForReporting  
		SAPchk.Set strValue
				  
		 If err.Number = 0 Then
			CheckSAPCheckbox = 0
				WriteStatusToLogs "The '"&strValue&"' Checkbox checked successfully."
			Else
				CheckSAPCheckbox = 1
				WriteStatusToLogs "The '"&strValue&"' Failed to check checkbox."
		  
			End If
				 
	On Error GoTo 0
End Function
'----------------------------------------------------------------------------
Function SetValueInEditBox(ByVal objEditbox , ByVal strValue)
	On Error Resume Next

	Dim strMethod

	strMethod = strKeywordForReporting

	objEditbox.Click
	
	If err.Number = 0 Then
			objEditbox.Set strValue
			If err.number = 0 then
				SetValueInEditBox = 0
				WriteStatusToLogs "The value: "&strValue&" is set in EditBox successfully."
			Else
				SetValueInEditBox = 1
					WriteStatusToLogs "Error occurred while entering value in edit box."
			End If
	  Else
			SetValueInEditBox = 1
			WriteStatusToLogs "Error occurred. " & error.description
	  
	 End If

	On Error GoTo 0
End Function
'--------------------------------------------------------------------------------------------------------------------
Function RestJsonResp(ByVal URL, ByVal method, ByVal filepath, ByVal key, ByVal response)
On Error Resume Next
	Dim objFileToRead,strFileText, objXmlHttpMain, str,a, y, i,b,z, objJSONDoc, objResult, found

	If trim(URL) = "" or trim(method) = "" or trim(filepath) = "" or trim(key) = "" or trim(response) = "" Then
		RestJsonResp= 1
				WriteStatusToLogs "One or more parameters are empty."
		Exit function
	End if 
	
	Set objFileToRead = CreateObject("Scripting.FileSystemObject").OpenTextFile(filepath,1)
	strFileText = objFileToRead.ReadAll()
	objFileToRead.Close
	Set objFileToRead = Nothing


'URL="http://payment.biomedcentral.com.sandbox.test/manuscripts/1159802909180723" 
	Set objXmlHttpMain = CreateObject("Msxml2.ServerXMLHTTP") 
	'msgbox "check"
	'on error resume next 
		objXmlHttpMain.open method,URL, False 

		'objXmlHttpMain.setRequestHeader "Authorization", "Bearer <api secret id>"
		objXmlHttpMain.setRequestHeader "Content-Type", "application/json"
		objXmlHttpMain.send strFileText
		str = objXmlHttpMain.responseText
	'msgbox str
	AddItemToStorageDictionary response,str
	a=Split(str,",")
	y = ubound(a)
	
	For i = 0 to y
		b = a(i)

		If instr(b,key)>0 Then
			z = Split(b,""":")
			c = Replace(z(1),"""","")
			'msgbox c
			'	msgbox b
			AddItemToStorageDictionary response,c
			found = "1"
			RestJsonResp=0
			WriteStatusToLogs "The expected '"&c&"' has been store successfully in '"&response&"' "
				Exit For
		End If

	next

		If found <> "1" then
		RestJsonResp=1
					WriteStatusToLogs "Response is not found."
		End if

set objJSONDoc = nothing 
set objResult = nothing

On Error GoTo 0
End Function
'-------------------------------------------------------------------------------
Function SAPStorePropVal(byVal objSAP,byVAl objprop, byVal strKey)
	On Error Resume Next
		'Variable Section Begin
			
	Dim strMethod
	Dim	strValue

		'Variable Section End
	strMethod = strKeywordForReporting
		
	If trim(objprop) = "" or trim(strKey) = "" Then
		SAPStorePropVal= 1
				WriteStatusToLogs "One or more parameters are empty."
		Exit function
	End if 
		
	  If CheckObjectExist(objSAP) Then
				 sRow=objSAP.GetROProperty (objprop)
				 intval = AddItemToStorageDictionary(strKey,sRow)

				 If  intval = 0 Then
							SAPStorePropVal = 0
							WriteStatusToLogs "The string '"&sRow&"'  is stored successfully in the Key '"&strKey&"'" 
									  
						Else
							SAPStorePropVal = 1
							WriteStatusToLogs "The string '"&sRow&"'  is not stored successfully in the Key '"&strKey&"'" 
									  
						End If
								  
			Else
					SAPStorePropVal = 1
					WriteStatusToLogs "The '"& objSAP&"' object does not exist, Please verify."
			End If
	On Error GoTo 0
End Function
'-----------------------------------------------------------------------------------------------
Function DoubleClickSAP(ByVal obj)
	On Error Resume Next
	
	Dim DeviceReplay, target, abs_x, abs_y

	Set DeviceReplay = CreateObject ("Mercury.DeviceReplay")
	Set target = obj
	
	If CheckObjectExist(target) Then
		abs_x = target.GetROProperty("abs_x")
		abs_y = target.GetROProperty("abs_y")

		DeviceReplay.MouseMove abs_x, abs_y
		DeviceReplay.MouseDblClick abs_x, abs_y, 0
		 
		 If err.number = 0 then
			 DoubleClickSAP=0
			 WriteStatusToLogs "Double click is successful." 
		 Else
			 DoubleClickSAP=1
			  WriteStatusToLogs "Double click is failed. Check parameters." 
		 End If
	Else
		 DoubleClickSAP=1
			 WriteStatusToLogs "Object does not exist." 
	End If
	Set DeviceReplay = Nothing


	On Error GoTo 0
End Function
'--------------------------------------------------
Function SAPSelectRow(ByVal obj, ByVal row)
	On Error Resume Next
	
	Dim strMethod
	Dim	strValue

	If cint(row) <1 then
		WriteStatusToLogs "Invalid row number." 
		SAPSelectRow = 1 
	End If
		'Variable Section End
	strMethod = SAPSelectRow
	
	If CheckObjectExist(obj) Then
		obj.SelectRow row
		
		if err.number = 0 Then
		WriteStatusToLogs "Row number '"&row&"'  is selected." 
		
		SAPSelectRow=0
		Else
			WriteStatusToLogs "Invalid row number." 
			SAPSelectRow=1
		End If
	Else
		WriteStatusToLogs "Object does not exist." 
			SAPSelectRow=1
	End If
	
On Error GoTo 0
End Function
'-------------------------------------------------------------------------------------------
Function SAPSelectCell(ByVal obj, ByVal row, ByVal col)
	On Error Resume Next
	
	Dim strMethod
			Dim	strValue

	'Variable Section End
	strMethod = SAPSelectCell
	If trim(row) = "" or trim(col) = "" Then
		SAPSelectCell= 1
				WriteStatusToLogs "One or more parameters are empty."
		Exit function
	End if 
	obj.SelectCell row,col
	
	If err.number = 0 Then
	SAPSelectCell=0
	WriteStatusToLogs "The cell  is selected successfully." 
	Else
	SAPSelectCell=1
	WriteStatusToLogs "Error in execution. Invalid parameters." 
	End If

On Error GoTo 0
End Function
'-------------------------------------------------------------------------------------------------------------
Function SAPGetCellData(ByVal obj, ByVal strKey, ByVal row, ByVal col)
	On Error Resume Next
	
	Dim strMethod
	Dim	strValue, strK, intval

	If trim(strKey) = "" or trim(row) = ""  or trim(col) = "" Then
		SAPGetCellData= 1
				WriteStatusToLogs "One or more parameters are empty."
		Exit function
	End if 
	
		'Variable Section End
		strMethod = strKeywordForReporting

	If CheckObjectExist(obj) Then
		strK = obj.GetCellData(row,col)

		intval = AddItemToStorageDictionary(strKey,strK)
		
		If err.number = 0 Then
		SAPGetCellData=0
		WriteStatusToLogs "The value '"& strK &"'  is stored selected successfully." 
		Else
		SAPGetCellData=1
		WriteStatusToLogs "Error in execution. Invalid parameters." 
		End If
	Else
		SAPGetCellData=1
		WriteStatusToLogs "Object does not exist." 
	End If
	


On Error GoTo 0
End Function

'--------------------------------------------------------------------
'--------------------------------------------------------------------


Function SAPAutoLogon (ServerDescription,Client,User, Password,Language)
	On error resume next
	Dim strMethod
	strMethod = strKeywordForReporting
	
	If trim(ServerDescription) = "" or trim(Client) = ""  or trim(User) = "" or trim(Password) = "" or trim(Language) = "" Then
		SAPAutoLogon= 1
				WriteStatusToLogs "One or more parameters are empty."
		Exit function
	End if
	
	SAPGuiUtil.AutoLogon ServerDescription, Client, User, Password, Language
	
	If Err.number = 0  Then
	SAPAutoLogon = 0
		WriteStatusToLogs "SAPAutoLogon is successful."
	Else
		SAPAutoLogon = 1
		WriteStatusToLogs "SAPAutoLogon is unsuccessful. Please verify parameters."
	End If
	
	On error goto 0
End Function


Function SAPAutoLogonByIP (ConnectionString, Client, User , Password, Language)
	
	
	On error resume next
	Dim strMethod
	strMethod = strKeywordForReporting
	
	If trim(ConnectionString) = "" or trim(Client) = ""  or trim(User) = "" or trim(Password) = "" or trim(Language) = "" Then
		SAPAutoLogonByIP= 1
				WriteStatusToLogs "One or more parameters are empty."
		Exit function
	End if
	
	SAPGuiUtil.AutoLogonByIP ConnectionString, Client, User, Password, Language
	
	If Err.number = 0  Then
	SAPAutoLogonByIP = 0
		WriteStatusToLogs strMethod & " is successful."
	Else
		SAPAutoLogonByIP = 1
		WriteStatusToLogs "Error in execution. Please verify parameters."
	End If
	
	On error goto 0
End Function

Function SAPAutoLogonByTargetSystem (ConnectionString, Client, User , Password, Language)
	On error resume next
	Dim strMethod
	strMethod = strKeywordForReporting
	
	If trim(ConnectionString) = "" or trim(Client) = ""  or trim(User) = "" or trim(Password) = "" or trim(Language) = "" Then
		SAPAutoLogonByTargetSystem= 1
				WriteStatusToLogs "One or more parameters are empty."
		Exit function
	End if
	
	
	SAPGuiUtil.AutoLogonByTargetSystem ConnectionString, Client, User, Password, Language
	
	If Err.number = 0  Then
	SAPAutoLogonByTargetSystem = 0
		WriteStatusToLogs strMethod & " is successful."
	Else
		SAPAutoLogonByTargetSystem = 1
		WriteStatusToLogs "Error in execution. Please verify parameters."
	End If
	
	On error goto 0
End Function


Function SAPOpenConnection (ServerDescription)
	On error resume next
	Dim strMethod
	strMethod = strKeywordForReporting
	
	If trim(ServerDescription) = "" Then
		SAPOpenConnection= 1
				WriteStatusToLogs "One or more parameters are empty."
		Exit function
	End if
	
	SAPGuiUtil.OpenConnection ServerDescription
	
	If Err.number = 0  Then
	SAPOpenConnection = 0
		WriteStatusToLogs strMethod & " is successful."
	Else
		SAPOpenConnection = 1
		WriteStatusToLogs "Error in execution. Please verify parameters."
	End If
	
	On error goto 0
End Function



Function SAPOpenConnectionByIP (ConnectionString)
	On error resume next
	Dim strMethod
	strMethod = strKeywordForReporting
	
	If trim(ConnectionString) = "" Then
		SAPOpenConnectionByIP= 1
				WriteStatusToLogs "One or more parameters are empty."
		Exit function
	End if
	
	
	SAPGuiUtil.OpenConnectionByIP  ConnectionString
	
	If Err.number = 0  Then
	SAPOpenConnectionByIP = 0
		WriteStatusToLogs strMethod & " is successful."
	Else
		SAPOpenConnectionByIP = 1
		WriteStatusToLogs "Error in execution. Please verify parameters."
	End If
	
	On error goto 0
End Function


Function SAPOpenConnectionByTargetSystem (TargetSystem)
	On error resume next
	Dim strMethod
	strMethod = strKeywordForReporting
	
	If trim(TargetSystem) = "" Then
		SAPOpenConnectionByTargetSystem= 1
				WriteStatusToLogs "One or more parameters are empty."
		Exit function
	End if
	
	SAPGuiUtil.OpenConnectionByTargetSystem TargetSystem
	
	If Err.number = 0  Then
	SAPOpenConnectionByTargetSystem = 0
		WriteStatusToLogs strMethod & " is successful."
	Else
		SAPOpenConnectionByTargetSystem = 1
		WriteStatusToLogs "Error in execution. Please verify parameters."
	End If
	
	On error goto 0
End Function