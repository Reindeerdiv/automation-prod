Set fso=createobject("Scripting.FileSystemObject")
Winpath = fso.GetParentFolderName(Environment.Value("TestDir")) & "\"
Set fso= Nothing

Executefile(Winpath & "Library\WinWrappers\Frame.vbs")
Executefile(Winpath & "Library\WinWrappers\Static.vbs")
Executefile(Winpath & "Library\WinWrappers\WinButton.vbs")
Executefile(Winpath & "Library\WinWrappers\WinCheckBox.vbs")
Executefile(Winpath & "Library\WinWrappers\WinComboBox.vbs")
Executefile(Winpath & "Library\WinWrappers\WinDialog.vbs")
Executefile(Winpath & "Library\WinWrappers\Window.vbs")
Executefile(Winpath & "Library\WinWrappers\WinEditBox.vbs")
Executefile(Winpath & "Library\WinWrappers\WinEditor.vbs")
Executefile(Winpath & "Library\WinWrappers\WinListBox.vbs")
Executefile(Winpath & "Library\WinWrappers\WinListView.vbs")
Executefile(Winpath & "Library\WinWrappers\WinMenu.vbs")
Executefile(Winpath & "Library\WinWrappers\WinRadioButton.vbs")
Executefile(Winpath & "Library\WinWrappers\WinTreeView.vbs")
Executefile(Winpath & "Library\WinWrappers\CommonWinWrappers.vbs")
Executefile(Winpath & "Library\WinWrappers\WinToolbar.vbs")	
Executefile(Winpath & "Library\SAP\SapActions.vbs")	  
If Err.Number <> 0 Then
	MsgBox "Fail to Load the Win keywords : "& Err.Description
End If 
