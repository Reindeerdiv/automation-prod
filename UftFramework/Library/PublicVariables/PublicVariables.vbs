Option explicit

Dim LOG_PATH,TEMP_QUALITIAOFFLINEPROPERTY_PATH
LOG_PATH = "\Common\Config\QTPEngine.config"

Environment.Value("ActionTest") = False

Dim QualitiaFolderPath, BasePath
Dim ORpath, MapXMLPath, Port,ElementHighlight
QualitiaFolderPath = GetParentFld()

TEMP_QUALITIAOFFLINEPROPERTY_PATH=QualitiaFolderPath & "\temp\temp_qualitiaOffline.properties"

Dim logs, conn, configObj,qualitiaPropertiesPath
'Dim dotNetCom
Dim dotNetMapXml
Dim strKeywordForReporting
'Set logs = CreateObject("QualitiaCore.QLogger")
'Set conn = CreateObject("QualitiaCore.DatabaseConnectionHelper")
'Set configObj = CreateObject("QualitiaCore.FileHandlers.PropertiesFile")


BasePath = GetCurrentPath()

Set dotNetCom = DotNetFactory.CreateInstance("UftSocketController.UftCommandController",BasePath&"\DotNetLibrary\UftSocketController.dll" )
Set logs= DotNetFactory.CreateInstance("UftSocketController.QLogger",BasePath&"\DotNetLibrary\UftSocketController.dll" )
'Set dotNetMapXml=DotNetFactory.CreateInstance("DotNetFactoryUFT.SuiteMap", "D:\qtprepo\DotNetFactoryUFT\bin\x86\Debug\DotNetFactoryUFT.dll" )
qualitiaPropertiesPath= GetTempFolderPath() & "\Config\qualitia-qtp.properties"

dotNetCom.LoadQualitiaPropertiesFile(qualitiaPropertiesPath)

ORpath=dotNetCom.GetValueFromQualitiaProperties("REPOSITORYPATH")
'MapXMLPath=dotNetCom.GetValueFromQualitiaProperties("xmlPath")&"\Map.xml"
Port=dotNetCom.GetValueFromQualitiaProperties("SOCKETPORT")
SYNCTIMEINSECONDS=dotNetCom.GetValueFromQualitiaProperties("SYNCTIME")
SYNCCMDTIME=dotNetCom.GetValueFromQualitiaProperties("SYNCCOMMANDTIME")
ElementHighlight=dotNetCom.GetValueFromQualitiaProperties("EXECUTIONHIGHLIGHT")
logs.SetConfigFile


'************************************************************************
'Log Constants
'Variable to store Logs information
'************************************************************************
Dim oDictLOGPATH
Dim oDictKeywordParamCollection 
Dim oDictKeywordMandatoryDetails

Set oDictKeywordParamCollection = CreateObject("Scripting.Dictionary")
Set oDictKeywordMandatoryDetails = CreateObject("Scripting.Dictionary")
Set oDictLOGPATH					= CreateObject("Scripting.Dictionary")

Const LOGPATH						= "Logpath"
Const TCLOGPATH						= "TCLogpath"
Const BITMAPLOGPATH					= "BitmapLogpath"

Const INFOLOGFILE					= "InfoLog.html"
Const DEBUGLOGFILE					= "DebugLog.html"
Const ERRORLOGFILE					= "ErrorLog.html"
Const REPORTFILE					= "Report.txt"
Const CAPTUREBITMAP					= "Capture_Images"

Const SNAPSHOTFILENAME				= "Snapshot"
Const SNAPSHOTFILETYPE				= "png"

Dim objLog 'For Creating the log files
Set objLog = CreateObject("Scripting.FileSystemObject")
Dim Debuglogobj, Errorlogobj, Infologobj, Statuslogobj 'Log objects handler


'************************************************************************
'Database Constants
'These are the constants For table names then they added to the Dictonary
'Table are prefix by TABLE
'Columns are prefix by Table acronym
'Eg: tblsummaryreport = TSR , test_execution = TTE
'************************************************************************
Const TABLE_TBLSUMMARYREPORT		=   "tblsummaryreport"

Const TSR_SUITEEXECUTIONID		    =   "SuiteExecutionID"
Const TSR_SCENARIONAME				=   "ScenarioName"
Const TSR_TCSEQ						=   "TCSEQ"
Const TSR_RUNID 					=   "RUNID"
Const TSR_MANUALTCID				=   "ManualTCID"
Const TSR_TCNAME					=   "TCName"
Const TSR_DESCRIPTION				=   "Description"
Const TSR_STARTTIME					=   "StartTime"
Const TSR_ENDTIME					=   "Endtime"
Const TSR_STATUS					=   "Status"
Const TSR_SUITEID			    	=   "SuiteID"
Const TSR_SUITENAME					=   "SuiteName"
Const TSR_SCENARIOID				=   "ScenarioID"
Const TSR_TCID						=   "TCID"

Const TABLE_TEST_EXECUTION			=   "test_execution"

Const TTE_SUITEEXECUTIONID			=   "SuiteExecutionID"
Const TTE_RELEASENUMBER 			=   "ReleaseNumber"
Const TTE_BUILDNUMBER               =   "BuildNumber"
Const TTE_HOST 						=   "Host"
Const TTE_USERID					=   "UserID"
Const TTE_ITERATIONNUMBER 			=   "IterationNumber"
Const TTE_SUITENAME 				=   "SuiteName"
Const TTE_SCHEDULENAME              =   "ScheduleName"
Const TTE_GENERATEDON               =   "GeneratedOn"
Const TTE_LOGPATH                   =   "LogPath"
Const TTE_TOOL                      =   "Tool"
Const TTE_SHAREDPATH 				=   "SharedPath"


'**************************************************************************
'Config Constants
'Following are Global variables that will hold the respective config values throughout the execution
'**************************************************************************
Dim DBPASSWORD
Dim DBUSERNAME
Dim DBSERVERPORT
Dim CREATEINFOLOG
Dim CREATEDEBUGLOG
Dim CREATEERRORLOG
Dim SYNCTIMEINSECONDS
Dim SYNCCMDTIME
Dim OFFLINEMODE
Dim DRIVERPATH
Dim PROJECTNAME
Dim QUSERNAME
Dim DBSERVER
Dim CAPTUREPASSBITMAP
Dim CAPTUREFAILBITMAP
Dim CAPTUREDEFECTBITMAP
Dim MAINLOGPATH
Dim XMLPATH
Dim DRYRUN 
Dim BUILDNO
Dim RELEASENO
Dim HOSTNAME
Dim PROJECT_RESULTDB

'___________________________________________________
'Following are Place Holders for the XML config keys

Const PH_DBPASSWORD					=  "DBPassword"
Const PH_DBUSERNAME					=  "DBUserName"
Const PH_DBSERVERPORT				=  "ServerPort"
Const PH_CREATEINFOLOG				=  "CreateInfoLog"	
Const PH_CREATEDEBUGLOG				=  "CreateDebugLog"
Const PH_CREATEERRORLOG				=  "CreateErrorLog"
Const PH_SYNCTIMEINSECONDS			=  "SyncTimeInSeconds"
Const PH_OFFLINEMODE				=  "OfflineMode"
Const PH_DRIVERPATH					=  "DriverPath"
Const PH_PROJECTNAME				=  "ProjectName"	
Const PH_QUSERNAME					=  "UserName"
Const PH_SERVER						=  "Server"
Const PH_CAPTUREPASSBITMAP			=  "CapturePassBitMap"
Const PH_CAPTUREFAILBITMAP			=  "CaptureFailBitMap"
Const PH_CAPTUREDEFECTBITMAP		=  "CaptureDefectBitMap"
Const PH_LOGPATH					=  "LogPath"
Const PH_XMLPATH					=  "xmlPath"
Const PH_DRYRUN						=  "DryRun"
Const PH_BUILDNO					=  "BuildNum"
Const PH_RELEASENO					=  "ReleaseNum"
Const PH_HOSTNAME					=  "HostName"
Const PH_PROJECT_RESULTDB			=  "ProjectResultdb"
Const PH_HILIGHT					=  "ExecutionHighlight"
Const PH_DBTYPE						=  "DBType"
Const PH_WINDOWSAUTHENTICATION      = "WindowsAuthentication"
Const PH_MASTERDB                	= "MasterDB"
Const PH_REPOSITORYPATH	            = "RepositoryPath"
'**************************************************************************
'Special Character Constants
'**************************************************************************
Dim StrPlaceHolderBegin
Dim StrPlaceHolderEnd
Dim StrEscapeChar
Dim strReturnError
Dim strDataSeparator
Dim oDictStorage
Dim oDictEscapeChars

StrPlaceHolderBegin					= "{"
StrPlaceHolderEnd					= "}"
StrEscapeChar						= "~"
strReturnError						= "{"
strDataSeparator					= "^"

' Dictionary used for storing global keys and its relevant data
Set oDictStorage = CreateObject("Scripting.Dictionary")
' Dictionary to store all the characters used as placeholders or separators
Set oDictEscapeChars = CreateObject("Scripting.Dictionary")
' The variables are used in ProcessData functions.

oDictEscapeChars.add "1" , StrEscapeChar
oDictEscapeChars.add "2" , StrPlaceHolderBegin
oDictEscapeChars.add "3" , StrPlaceHolderEnd
oDictEscapeChars.add "4" , strDataSeparator


'**************************************************************************
'Keyword Signature Constants
'Following are const that will be used for Declaring a function signature
'The same values should be used in keyword_parameter table also
'**************************************************************************
Const STRINGDATATYPE				= "String"
Const OBJECTDATATYPE				= "Object"
Const ARRAYDATATYPE					= "Array"
Const KEYDATATYPE					= "Key"
Const BOOLEANDATATYPE				= "Boolean"
Const INTEGERDATATYPE				= "Integer"


'**************************************************************************
'Keyword Status Constants
''*************************************************************************
Const STATUS_PASSED					= 0
Const STATUS_FAILED					= 1
Const STATUS_DEFECT					= 2
Const STATUS_DEFAULT				= -1


'**************************************************************************
'IF-ELSE logic Implementations Constants
''*************************************************************************
Const LINE_SAPERATOR				= "_"
Const COND_BLOCK_SAPERATOR			= ";"
Dim TASK_COND_STMT

'**************************************************************************
'Error Handler
''*************************************************************************
Const EXIT_TASK						= "ExitTask"
Const EXIT_TC_ITERATION				= "ExitTCIteration"
Const EXIT_SUIT						= "ExitSuite"
Const CONTINUE						= "Continue"
Dim intTestCaseCounter 
intTestCaseCounter = 0
'**************************************************************************
'Framework Constants
''*************************************************************************
Dim strExeKeywordName 'Keyword name use for reporting in HTML logs

Const REPORT_SEPARATOR	= "|"

Dim ResultDBConnection 'Variable to hold the result db connection

Const CONFIGFILEPATH = "Qualitia.exe.config"
Const QUALITIAREPORT = "QualitiaReport.properties"



Dim REPORTPATH 
REPORTPATH = QualitiaFolderPath & "\Common\Reporter"
   


Const SampleTimestamp = "yyyy-MM-dd 00:00:00.000"
Const SampleTime = "00:00:00.000"

Dim SUITEITERATIONNO 'Stores the suite Iteration No
Dim SUITEEXECUTIONID 'Stores the suite execution id

Dim oDictSuiteExecution 'Dictionary Object of (TCID, TcInfo(xmlpath,path))
Set oDictSuiteExecution = CreateObject("Scripting.Dictionary")

Dim oDictSuiteInfo 'Dictionary for (suiteprop, Suite Attribute), basically stores Suitename,Suite Schedule Name
Set oDictSuiteInfo = CreateObject("Scripting.Dictionary")

Dim oDictTestCaseIterations	'Dictionary for (TcID,TcIterations(TCIteration, TCDataSetTagName) )
Set oDictTestCaseIterations = CreateObject("Scripting.Dictionary")

Dim oDictTCTaskWorkFlowIterationsCollection	'Dictionary for (TcID,TaskIterations(TaskIteration, strTcIteration &"_"& strTaskinfoSequence, TaskIterationRootNode.length))
Set oDictTCTaskWorkFlowIterationsCollection = CreateObject("Scripting.Dictionary")

Dim ODictTestCaseParameterNameCollection 'Dictionary of (TestCaseID, TCParameterNameInfo(strTcDataKey, StepParameterNameInfo))
Set ODictTestCaseParameterNameCollection = CreateObject("Scripting.Dictionary")	

Dim oDictTaskIterationsNamesCollection 'Dictionary of (argTestCaseID, TaskIterNameInTestCase)
Set oDictTaskIterationsNamesCollection = CreateObject("Scripting.Dictionary") 

Dim oDictValidTestCaseInSuite 'Dictionary for valid TC of (TestCaseID, TCName)
Set oDictValidTestCaseInSuite= CreateObject("Scripting.Dictionary")

Dim oDictTaskIdNameCollection 'Dictionary of (TestCaseID, TaskIDTaskName) 
Set oDictTaskIdNameCollection= CreateObject("Scripting.Dictionary")

'--------------------------Added By Rajneesh
Dim oDictTaskCond 'Dictionary to hold Task id and corresponding conditional block sequence
Set oDictTaskCond= CreateObject("Scripting.Dictionary")

Dim oDictStepCond 'Dictionary to hold setep id and corresponding conditional block sequence
Set oDictStepCond= CreateObject("Scripting.Dictionary")

Dim oDicTaskSeqDisc, oDicTaskSeqExpression, oDicStepIdStepSeqMap, oDicTaskInfo, dictTaskInfoPerNode, dictTaskInfoPerNodeStep, dictKeywordToolFunction
Set oDicTaskSeqDisc = CreateObject("Scripting.Dictionary")  'This will hold Task level sequesnce and its Disc
Set oDicTaskSeqExpression = CreateObject("Scripting.Dictionary") 'NOT USED
'Set objTaskData = CreateObject("Scripting.Dictionary") 'Copied it from function : ValidateTestCaseXML
Set oDicStepIdStepSeqMap = CreateObject("Scripting.Dictionary")
Set oDicTaskInfo = CreateObject("Scripting.Dictionary") 'holds the task attributes info for each <TaskName> from the execution xml
Set dictTaskInfoPerNode = CreateObject("Scripting.Dictionary")
Set dictTaskInfoPerNodeStep = CreateObject("Scripting.Dictionary") 
Set dictKeywordToolFunction= CreateObject("Scripting.Dictionary") 
Dim dictActionState
Set dictActionState= CreateObject("Scripting.Dictionary")
Dim iterSuiteXML
Dim oDictAsciiMapping 'Dictionary for (suiteprop, Suite Attribute), basically stores Suitename,Suite Schedule Name
Set oDictAsciiMapping = CreateObject("Scripting.Dictionary")


'--------------------------
	
Dim oDictTestCaseTaskWorkFlowCollection	'Dictionary of (TCId, oDictTestCaseTaskInfo)
Dim oDictTestCaseDataCollection	'Dictionary of (TCId, oDictTestCaseDataInfo)	
Dim oDictTestCaseTaskStepsCollection 'Dictionary of (TaskID, TestCaseTaskStepsInfo)
Dim oDictConfiguration 'Dictionary Object to store configuration (key,value)
Dim oDictParentCollection 'Dictionary object of (object ID, InnerDictionary(parentID, ObjectClass))
Dim oDictPropertyValueCollection 'Dictionary object of (objectID, InnerDict(propName, propvalue))
Dim oDictKeywordCollection 'Dictionary Object of (keywordID, functionName)
Dim oDictSuiteInfoCollection 'Dictionary for storing suite info of(suite id, oDictSuiteInfo(prop,propvalue))
Dim oDictScenarioIdNameCollection 'Dictionary Object of (ScenarioID, ScenarioName)
Dim oDictTestcaseIdNameCollection 'Dictionary Object of (TCID, TCName)
Dim oDictkeywordIdNameCollection 'Dictionary Object of (KeywordID, KwdName)
Dim oDictObjectIdNameCollection 'Dictionary Object of (ObjID, ObjName)
Dim oDictObjectIsQTP 'Dictionary Object of (ObjID, IsQTP)
'Dim oDictKeywordMandatoryDetails 'Dictionary Object of (KeywordID, (argSeq,mandatory))

'**************************************************************************
'XML Constants
''*************************************************************************
Const AT_ACTIONID					= "ActionId"
Const AT_ACTIONNAME					= "ActionName"
Const AT_QCLASS						= "QClass"		
Const AT_STEPSEQNO					= "StepSeqNo"
Const AT_SUITEID					= "SuiteID"
Const AT_SUITENAME					= "SuiteName"
Const AT_TCDESCRIPTION				= "Desc"
Const AT_SCENARIOID					= "ScenarioId"
Const AT_SCENARIONAME				= "ScenarioName"
Const AT_TESTCASEID					= "TCId"
Const AT_TESTCASENAME				= "TCName"
Const AT_TCMANUALTCID				= "ManualTCID"
Const AT_SUITEMANUALTCID			= "ManualTCId"
Const AT_SEQID						= "SeqId"
Const AT_OBJECTID					= "ObjectID"
Const AT_OBJECTNAME					= "ObjectName"
Const AT_PARENTID					= "ParentID"
Const AT_OBJECTCLASS				= "ObjectClass"
Const AT_STARTTIME					= "STARTTIME"
Const AT_ENDTIME					= "ENDTIME"
Const AT_STATUS						= "STATUS"
Const AT_RUNID						= "RunId"
Const AT_TASKSEQ					= "TaskSeq"
Const AT_TASKID						= "ID"
Const AT_ITERATION					= "Iteration"
Const AT_PARAMNAME					= "Name"
Const AT_SCHEDULENAME				= "ScheduleName"
Const AT_ONERROR					= "OnError"
Const AT_ISSYSTEMTASK				= "IsSystemTask"
Const AT_TASKLINENUMBER 			= "LineNumber"
Const AT_TASKDESC   				= "Desc"
Const AT_SUITEONERROR   			= "OnError"
Const AT_ACTIONSTATE                = "ActionState"

'**************************************************************************
'Error Constants
''*************************************************************************
Const ERR_CONTINUE   				= "Continue"
Const ERR_EXITTASK  				= "ExitTask"
Const ERR_EXITTCITERATION   		= "ExitTCIteration"
Const ERR_EXITSUITE  				= "ExitSuite"
''*************************************************************************
 
'logs.Debug  "***************************************Qualitia QTP core engine started*******************************************"
'logs.Debug "Current execution is running on QTP/UFT version >>"& Environment.Value("ProductVer")
'logs.Debug "QualitiaFolderPath >> " & QualitiaFolderPath
'logs.Debug "LOG_PATH >> " & LOG_PATH
'logs.Debug "ConfigFile path>> " & QualitiaFolderPath & LOG_PATH
'logs.Debug "public variables loaded"
