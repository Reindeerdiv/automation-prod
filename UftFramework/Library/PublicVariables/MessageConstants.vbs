Const MSG_OBJECT_NOT_EXISTS = "The specified {0} does not exist, Please verify." 'Ex: The specified ComboBox does not exist, Please verify.

Const MSG_OBJECT_EXISTS_PASS = "The specified object exists as expected."
Const MSG_OBJECT_EXISTS_FAIL = "The specified object exists, Please verify."
Const MSG_OBJECT_NOT_EXISTS_PASS = "The object does not exists as expected."

Const MSG_GENERIC_OBJECT_EXISTS_PASS = "The specified {0} exists as expected."
Const MSG_GENERIC_OBJECT_EXISTS_FAIL = "The specified {0} exists, Please verify."
Const MSG_GENERIC_OBJECT_NOT_EXIST_PASS = "The specified {0} does not exist as expected."

Const MSG_OPERATION_SUCCESSFUL = "The {0} operation on {1} was successfull."	' Ex: The Click operation on Button was successful.
Const MSG_OPERATION_UNSUCCESSFUL = "The {0} operation on {1} was unsuccessfull."  ' Ex: The Click operation on Button was unsuccessful.
Const MSG_OPERATION_FAILED_DISABLED = "The {0} operation failed; {1} was disabled, Please verify." ' Ex: The Click operation failed; Button was disabled, Please verify.
Const MSG_VALUE_MATCHES = "The expected value ""{0}"" and actual value ""{1}"" matches."
Const MSG_VALUE_NOT_MATCHES = "The expected value ""{0}"" and actual value ""{1}"" does not match, Please verify."
Const MSG_ITEMS_MATCHES = "The specified {0} matches with the expected {1}." 'Ex: " The specified ComboBox items matches with the expected ComboBox items." 
Const MSG_ITEMS_NOT_MATCHES = "The specified {0} do not match with the expected {1}." 'Ex: " The specified ComboBox items do not match with the expected ComboBox items." 
Const MSG_ERR_NOT_NUMERIC = "Operation failed; specified {0} is empty or not a numeric value." ' Ex: An error occurred; expected value is empty or not a numeric value.
Const MSG_ERR_NOT_STRING = "Operation failed; specified {0} is empty or not a String." ' Ex: An error occurred; expected value is empty or not a numeric value.
Const MSG_ERR_EMPTY = "Operation failed; {0} is empty."  ' Ex: Operation failed; ComboBox is empty.
Const MSG_ERR_NOT_AVAILABLE = "Operation failed; the specified item is not available." 
Const MSG_ERR_FETCH_DATA = "Operation failed; Unable to retrieve {0}." ' Ex: Operation failed; Unable to retrieve ComboBox items.

Const MSG_OPERATION_FAILED_MISSING = "The {0} operation failed, Please verify {1} exists." ' Ex: The Select operation failed, Please verify Item exists.

Const MSG_NO_DUPLICATES = "The {0} contains no duplicate {1}."	' Ex: The ComboBox contains no duplicate items.
Const MSG_DUPLICATES = "The {0} contains duplicate {1}, Please verify."  ' Ex: The ComboBox contains duplicate items.
Const MSG_OUT_OF_RANGE = "The specified {0} is out of range, Please verify." ' Ex: The specified index is out of range.

Const MSG_CONTAINS_SPECIFIC_PASS = "The {0} contains the specified {1}."  'Ex: The ComboBox contains the specified item.
Const MSG_CONTAINS_SPECIFIC_FAIL = "The {0} contains the specified {1}, Please verify."  'Ex: The ComboBox contains the specified item, Please verify.
Const MSG_NOT_CONTAINS_SPECIFIC_PASS = "The {0} does not contain the specified {1}."  'Ex: The ComboBox does not contain the specified item.
Const MSG_NOT_CONTAINS_SPECIFIC_FAIL = "The {0} does not contain the specified {1}, Please verify."  'Ex: The ComboBox does not contain the specified item, Please verify.
Const MSG_CONTAINS_PASS = "The {0} contains {1}."  ' Ex: The Edit Box contains numeric value.
Const MSG_NOT_CONTAINS_FAIL = "The {0} does not contain {1}, Please verify."  ' Ex: The Edit Box does not contain numeric value, Please verify.

Const MSG_VISIBLE_PASS = "The {0} are visible as expected."  'Ex: The check boxes are visible as expected.
Const MSG_VISIBLE_FAIL = "The {0} are visible, Please verify."  'Ex: The check boxes are visible, Please verify.
Const MSG_NOT_VISIBLE_PASS = "The {0} are not visible as expected."  'Ex: The check boxes are not visible as expected.
Const MSG_NOT_VISIBLE_FAIL = "The {0} are not visible, Please verify."  'Ex: The check boxes are not visible, Please verify.

Const MSG_VALID_RANGE = "The {0} '{1}' is within specified range." 'Ex: The EditBox value length "3" is within specified range.
Const MSG_INVALID_RANGE = "The {0} '{1}' is not within specified range." 'Ex: The EditBox value length "3" is not within specified range.
Const MSG_INVALID_RANGE_SEQUENCE = "The Minimum length should be less than Maximum length, Please verify."

Const MSG_STATE_ALREADY_SET = "The {0} is already in {1} state, Please verify." ' Ex: The Check Box is already in checked state, Please verify.

Const MSG_INVALID_ACTION="The specified action is not valid on {0}, Please verify."

Const MSG_UNSUPPORTED_ACTION = "Operation failed; {0} action is not supported for the specified {1}." 'Ex: Operation failed; Maximize action is not supported for the specified Window.

Const MSG_OPERATION_FAILED_GENERIC = "Operation failed; {0} is {1}, Please verify." ' Ex: Operation failed; Window is Minimized, Please verify.

Const MSG_ERR_INVALID_FORMAT = "Operation failed; {0} specified is in invalid format." ' Ex: Operation failed; Time specified is in invalid format.

Const MSG_EXPECTED_PASS = "The specified {0} is {1} as expected."  'Ex: The specified toolbar item is enabled as expected.
Const MSG_EXPECTED_FAIL = "The specified {0} is {1}, Please verify."  'Ex: The specified toolbar item is enabled, Please verify.

'--------------------------Added By Rajneesh
Dim oDictErrorMessages
Set oDictErrorMessages= CreateObject("Scripting.Dictionary")
oDictErrorMessages.Add "-2147024809", "The parameter is incorrect."
oDictErrorMessages.Add "-2147220983", "Cannot identify the specified item of the Tree object. Confirm that the specified item is included in the object's item collection."
oDictErrorMessages.Add "-2147220975", "The statement contains one or more invalid function arguments."
oDictErrorMessages.Add "6", "Overflow."
oDictErrorMessages.Add "5", "Invalid procedure call or argument"
oDictErrorMessages.Add "NoParentFoundInHierarchy", "None of the parent objects exist on application for the current test-object."
oDictErrorMessages.Add "ParentFoundInHierarchy", "For current test-object, last parent that is found on application is: "
oDictErrorMessages.Add "ParentFoundInHierarchyDP", "For current DP object, last parent that is found on application has description: "
Const MSG_GENERIC_FAIL="Possible reasons could be as follow:<br><br> 1. Source code not found, please verify the custom action files.<br>2. Unable to execute the function, please verify the syntax or signature of the function."

	





