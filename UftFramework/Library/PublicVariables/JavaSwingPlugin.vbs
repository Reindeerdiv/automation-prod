Set fso=createobject("Scripting.FileSystemObject")
Swingpath = fso.GetParentFolderName(Environment.Value("TestDir")) & "\"
Set fso= Nothing

Executefile(Swingpath & "Library\JavaSwingWrappers\JButton.vbs")
Executefile(Swingpath & "Library\JavaSwingWrappers\JCheckBox.Vbs")
Executefile(Swingpath & "Library\JavaSwingWrappers\JDialogue.vbs")
Executefile(Swingpath & "Library\JavaSwingWrappers\JEditBox.vbs")
Executefile(Swingpath & "Library\JavaSwingWrappers\JInternalFrame.vbs")
Executefile(Swingpath & "Library\JavaSwingWrappers\JList.vbs")
Executefile(Swingpath & "Library\JavaSwingWrappers\JMenu.vbs")
Executefile(Swingpath & "Library\JavaSwingWrappers\JRadioButton.vbs")
Executefile(Swingpath & "Library\JavaSwingWrappers\Jslider.vbs")
Executefile(Swingpath & "Library\JavaSwingWrappers\JSpin.vbs")
Executefile(Swingpath & "Library\JavaSwingWrappers\JStaticText.vbs")
Executefile(Swingpath & "Library\JavaSwingWrappers\JTab.vbs")
Executefile(Swingpath & "Library\JavaSwingWrappers\JTable.vbs")
Executefile(Swingpath & "Library\JavaSwingWrappers\JToolBar.vbs")
Executefile(Swingpath & "Library\JavaSwingWrappers\JTreeView.vbs")
Executefile(Swingpath & "Library\JavaSwingWrappers\JWindow.vbs")
Executefile(Swingpath & "Library\JavaSwingWrappers\JObject.vbs")
Executefile(Swingpath & "Library\JavaSwingWrappers\JApplet.vbs")
Executefile(Swingpath & "Library\Utilities\CommonMethodJavaSwing.vbs")
Executefile(Swingpath & "Library\JavaSwingWrappers\JLink.vbs")
Executefile(Swingpath & "Library\JavaSwingWrappers\JCalender.vbs")
Executefile(Swingpath & "Library\JavaSwingWrappers\JExpandBar.vbs")

If Err.Number <> 0 Then
	MsgBox "Fail to load the Javaswing keywords : "& Err.Description
End If 
