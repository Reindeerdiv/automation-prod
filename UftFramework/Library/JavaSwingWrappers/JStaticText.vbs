
'Functions List
''Function VerifyJStaticTextexist(ByVal objStaticText)
''Function VerifyJStaticTextNotexist(ByVal objStaticText)
''Function VerifyJStaticTextValue(ByVal objStaticText , ByVal strExpValue)
'=======================Code Section Begin=============================================

'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name				:	VerifyJStaticTextexist()
'	Purpose						:	Verifies whether the StaticText exists or not?
'	Return Value				:	Integer( 0/2)
'	Arguments					:
'		Input Arguments			:   objStaticText 
'		Output Arguments		:	 
'	Function is called by		:
'	Function calls				:	CheckObjectexist() 
'	Created on					:	06/07/2009
'	Author						:	Vidyasagar
'
'------------------------------------------------------------------------------------------------------------------------------------------

Function VerifyJStaticTextexist(ByVal objStaticText)
	On Error Resume Next
		'Variable Section Begin
	
		 Dim strMethod 

		'Variable Section End
		strMethod = strKeywordForReporting
			If CheckObjectexist(objStaticText) Then 
			   
				VerifyJStaticTextexist = 0
				WriteStatusToLogs "The StaticText exist."					
			Else
				VerifyJStaticTextexist = 2
				WriteStatusToLogs "The StaticText does not exist,  please verify."					
			End If	  
	
	On Error GoTo 0
End Function

'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name			:  VerifyJStaticTextNotexist()
'	Purpose					:  Verifies whether the StaticText exists or not?
'	Return Value			:  Integer( 0/2)
'	Arguments				:
'		Input Arguments		:  objStaticText 
'		Output Arguments	: 
'	Function is called by	:
'	Function calls			:  CheckObjectexist() 
'	Created on				:  06/07/2009
'	Author					:  Vidyasagar
'
'------------------------------------------------------------------------------------------------------------------------------------------

Function VerifyJStaticTextNotexist(ByVal objStaticText)
	On Error Resume Next
		'Variable Section Begin
	
		  Dim strMethod 

		'Variable Section End
		strMethod = strKeywordForReporting
		
			If CheckObjectNotexist(objStaticText) Then			   
				VerifyJStaticTextNotexist = 0
				WriteStatusToLogs "The JavaStaticText does not exist."					
			Else
				VerifyJStaticTextNotexist = 2
				WriteStatusToLogs "The JavaStaticText exists, please verify."					
			End If	
  		
	On Error GoTo 0
End Function

'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name			:	VerifyJStaticTextValue()
' 	Purpose					:	Compares Java StaticText value of specified with our expected value
' 	Return Value			:	Integer( 0/2/1)
' 	Arguments				:
'		Input Arguments		:  	objStaticText,strExpValue
'		Output Arguments	:	strActStaticText 
'	Function is called by	:     
'	Function calls			:	CheckObjectexist()
'	Created on				:	06/07/2009
'	Author					:	Vidyasagar
'
'------------------------------------------------------------------------------------------------------------------------------------------

Function VerifyJStaticTextValue(ByVal objStaticText , ByVal strExpValue)
	On Error Resume Next
		'Variable Section Begin
		
			Dim strMethod
			Dim strActStaticText, blnpresentFlag
			blnpresentFlag=false
			
			
		'Variable Section End
		strMethod = strKeywordForReporting
			If CheckObjectexist(objStaticText)  Then   
				strActStaticText= objStaticText.GetROProperty("value")

					If Err.Number = 0 Then
						
								   If Not IsNull (strActStaticText) Then

										 If Trim(strExpValue) =Trim(strActStaticText) Then
														blnpresentFlag=True
										 End If
												 
											 If blnpresentFlag = true  Then
													VerifyJStaticTextValue = 0
													WriteStatusToLogs "JavaStaticText contains the expected value."
											  Else
													VerifyJStaticTextValue = 2
													WriteStatusToLogs "JavaStaticText does not contain the expected value, please verify."
											 End If
										 
								   Else
										 VerifyJStaticTextValue = 1
										 WriteStatusToLogs "Action fails to get actual value,  please verify."
								   End If
							
					Else
							VerifyJStaticTextValue = 1
							WriteStatusToLogs "Action Fails to get actual value,  please verify JavaStaticText method and property."
					End If
			
			Else
				VerifyJStaticTextValue = 1
				WriteStatusToLogs "JavaStaticText doesn't exist, please verify."

			End If
			'WriteStepResults VerifyListBoxcontainsItem	
    On Error GoTo 0
End Function



'=======================Code Section End=============================================
