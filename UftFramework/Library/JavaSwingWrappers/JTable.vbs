'List of Functions:
'''Function StoreJColumnNumber(ByVal objJTable, ByVal strKey, ByVal strColName)
'''Function StoreInstanceOfDataFromJColumn(ByVal objJTable , ByVal strKey , ByVal intColNo , ByVal strCellData)
'''Function ClickOnJCell(Byval objJTable , Byval intRowNum , ByVal intColNum)
'''Function StoreJColumnCount(byval objObject, byval strKey)
'''Function StoreJRowCount(byval objObject, byval strKey )
'''Function VerifyJTableCellData(Byval objTable,Byval intRowNum , ByVal intColNum,Byval strExpData)StoreJCellData
'''Function VerifyJTableRowCount(Byval objTable,Byval intTotalRowCount )
'''Function VerifyDataExistInJColumn (Byval objTable,Byval intColNumber , Byval strData)
'''Function VerifyDataNotExistInJColumn (Byval objTable,Byval intColNumber , Byval strData)
'''Function CompareJTableRowData(ByVal objTable,  ByVal intRowNumber, ByVal arrExpData)
'''Function CompareJTableColumnData(ByVal objTable,  ByVal intColNumber, ByVal arrExpData)
'''Function VerifyJTableExist(ByVal objTable)
'''Function VerifyJTableNotExist(ByVal objTable)
'''Function StoreJRowNumberHavingCellData(objJTable,strKey,intColNo, strCellData , instanc , increment)




'=======================Code Section Begin=============================================

Option Explicit		'Forces explicit declaration of all variables in a script. 
'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :			  StoreJColumnNumber()
'	Purpose :				  Stores the column number of the specified column header of the java table	
'	Return Value :		 	  Integer(0/1)
'	Arguments :
'		Input Arguments :  	  objObject , strKey , strColName 
'		Output Arguments : 
'	Function is called by :
'	Function calls :		  GetJColumnNumber()
'	Created on :			  01/07/2009
'	Author :				  Iohbha K
'
'------------------------------------------------------------------------------------------------------------------------------------------

Function StoreJColumnNumber(ByVal objJTable, ByVal strKey, ByVal strColName)
On error resume Next
	'Variable Section Begin
	
		Dim strMethod
		Dim intVal

	'Variable Section End
	strMethod = strKeywordForReporting
	intVal = GetJColumnNumber(objJTable, strKey, strColName)
	If intVal = 0 Then
		StoreJColumnNumber = 0
		WriteStatusToLogs "Column Count of the specified header is stored successfully  in the Key '"&Trim(strKey)&"'"

    ElseIf intVal = 1 then
		StoreJColumnNumber = 1
		WriteStatusToLogs "The specified java table does not exist."	
	ElseIf intVal = 2 then
		StoreJColumnNumber = 1
		WriteStatusToLogs "The column header '"&strColName&"' is not available in the specified java table."
	ElseIf intVal = 3 then
		StoreJColumnNumber = 1
		WriteStatusToLogs "The Key passed is invalid , please verify."	
	ElseIf intVal = 4 then
		StoreJColumnNumber = 1
		WriteStatusToLogs "The column header passed is either null or empty, please verify."	
	ElseIf intVal = 5 then
		StoreJColumnNumber = 1
		WriteStatusToLogs "Failed to retrieve the specified java table data."
	End if

On error goto 0
End Function



'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :			  StoreInstanceOfDataFromJColumn()
'	Purpose :				  Store the number of occurence of the strCellData in the particular column.
'							  The column number starts from 1. The comparison is casesensitive.
'							  Occurence 0 indicates that the data does not exist in the column , and the data will be pushed in the dictionary
'	Return Value :		 	  Integer(0/1)
'	Arguments :
'		Input Arguments :  	  objJTable , strKey , intColNo , strCellData
'		Output Arguments : 
'	Function is called by :
'	Function calls :		  GetInstanceOfDataInJColumn()
'	Created on :			  01/07/2009
'	Author :				  Iohbha K
'
'------------------------------------------------------------------------------------------------------------------------------------------

Function StoreInstanceOfDataFromJColumn(ByVal objJTable , ByVal strKey , ByVal intColNo , ByVal strCellData)
On error resume Next
	'Variable Section Begin
	
		Dim strMethod
		Dim intVal

	'Variable Section End
	strMethod = strKeywordForReporting
	intVal = GetInstanceOfDataInJColumn(objJTable , strKey , intColNo , strCellData)
	If intVal = 0 Then
		StoreInstanceOfDataFromJColumn = 0
		WriteStatusToLogs "The data occurrence in the specified column is stored successfully."

    ElseIf intVal = 1 then
		StoreInstanceOfDataFromJColumn = 1
		WriteStatusToLogs "The specified java table does not exist."	
	ElseIf intVal = 2 then
		StoreInstanceOfDataFromJColumn = 1
		WriteStatusToLogs "An error occurred in the action StoreInstanceOfDataFromJColumn. Please verify."
	ElseIf intVal = 3 then
		StoreInstanceOfDataFromJColumn = 1
		WriteStatusToLogs "The Key passed is invalid, please verify."
	ElseIf intVal = 4 then
		StoreInstanceOfDataFromJColumn = 1
		WriteStatusToLogs "The cell data passed is either null or empty, please verify."	
	ElseIf intVal = 5 then
		StoreInstanceOfDataFromJColumn = 1
		WriteStatusToLogs "The column number '"&intColNo&"' is not a whole number, please verify."
	ElseIf intVal = 6 then
		StoreInstanceOfDataFromJColumn = 1
		WriteStatusToLogs "Failed to retrieve data from the specified java table, please verify."
	ElseIf intVal = 7 then
		StoreInstanceOfDataFromJColumn = 1
		WriteStatusToLogs "The column number '"&intColNo&"' does not exist in the specified table, please verify. Column index starts from 1."
	
	End if

On error goto 0
End Function


'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :			  ClickOnJCell()
'	Purpose :				  Clicks on the specified cell of the java table.	
'	Return Value :		 	  Integer(0/1)
'	Arguments :
'		Input Arguments :  	  objJTable , intRowNum , intColNum
'		Output Arguments : 
'	Function is called by :
'	Function calls :		  IsWholeNumber(),CheckObjectExist(),CheckJavaObjectVisible(),CheckJavaObjectEnabled() 
'	Created on :			  30/06/2009
'	Author :				  Iohbha K
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function ClickOnJCell(Byval objJTable , Byval intRowNum , ByVal intColNum)
On Error Resume next
	'Variable Section Begin
		Dim strMethod
		Dim intRow , intCol , intActRowCount , intActColCount

	'Variable Section End
	strMethod = strKeywordForReporting

	intRow = IsWholeNumber(intRowNum)
	intCol = IsWholeNumber(intColNum)
	If intRow =0 And intCol = 0 Then

		If CheckObjectExist(objJTable)  Then 

			If CheckJavaObjectVisible(objJTable) Then 

				If CheckJavaObjectEnabled(objJTable) Then

					intActRowCount = objJTable.getROProperty("rows")
					intActColCount = objJTable.getROProperty("cols")
					'MsgBox "intActColCount = "&intActColCount & " col to click = "&intColNum
					intRowNum = CInt(intRowNum)
					intActRowCount = CInt(intActRowCount)
					intColNum = CInt(intColNum)
					intActColCount = CInt(intActColCount)


					If Err.number =0 And Not IsNull(intActRowCount) And Not IsEmpty(intActRowCount) And intActRowCount<>"" And Not IsNull(intActColCount) And Not IsEmpty(intActColCount) And intActColCount<>"" Then 				
						
						If intRowNum > 0 And intRowNum <= intActRowCount And intColNum>0 And intColNum<=intActColCount Then
							
							objJTable.ClickCell intRowNum-1,intColNum-1
							If Err.number = 0 Then
								ClickOnJCell = 0
								WriteStatusToLogs "Click operation is performed successfully on the cell in the row :"&intRowNum&" and column :"&intColNum
							Else
								ClickOnJCell = 1
								WriteStatusToLogs "Click operation is not performed successfully on the cell in the row :"&intRowNum&" and column :"&intColNum
							
							End If
							
						ElseIf (intRowNum <1 or intRowNum > intActRowCount ) And (intColNum<1 or intColNum>intActColCount) Then 
							ClickOnJCell = 1
							WriteStatusToLogs "The row number '"&intRowNum&"' and column number '"&intColNum&"' does not exist in the specified Java Table, please verify. Row/Column index starts from 1."
						ElseIf intRowNum <1 or intRowNum > intActRowCount Then 
							ClickOnJCell = 1
							WriteStatusToLogs "The row number '"&intRowNum&"' does not exist in the specified Java Table, please verify. Row index starts from 1."
							
						ElseIf intColNum<1 or intColNum>intActColCount Then 
							ClickOnJCell = 1
							WriteStatusToLogs "The column number '"&intColNum&"' does not exist in the specified Java Table, please verify.Column index starts from 1."
							
						End If

					Else
						ClickOnJCell = 1
						WriteStatusToLogs "Action failed to fetch the Java Table data. Please verify."
					End If

				Else
					ClickOnJCell = 1
					WriteStatusToLogs "The specified Java Table is not enabled, please verify."	
				End If

			Else
				ClickOnJCell = 1
				WriteStatusToLogs "The specified Java Table is not visible, please verify."	
			End If

		Else
			ClickOnJCell = 1
			WriteStatusToLogs "The specified Java Table does not exist, please verify."	
		End If
		
	ElseIf intRow = 1 And intCol = 1 then
		ClickOnJCell = 1
		WriteStatusToLogs 	"Given row '"&intRowNum&"' and column '"&intColNum&"' are not natural numbers, please verify."	
	ElseIf intRow = 1 Then
		ClickOnJCell = 1
		WriteStatusToLogs 	"Given row '"&intRowNum&"' is a not natural number, please verify."	
	
	ElseIf intCol = 1 Then
		ClickOnJCell = 1
		WriteStatusToLogs 	"Given column '"&intColNum&"' is not a natural number, please verify."	
	End if
On Error goto 0
End Function


'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :			  StoreJColumnCount()
'	Purpose :				  Stores the column count of the specified row of the java table	
'	Return Value :		 	  Integer(0/1)
'	Arguments :
'		Input Arguments :  	  objObject , strKey 
'		Output Arguments : 
'	Function is called by :
'	Function calls :		  GetJTableColCount()
'	Created on :			  30/06/2009
'	Author :				  Iohbha K
'
'------------------------------------------------------------------------------------------------------------------------------------------

Function StoreJColumnCount(byval objObject, byval strKey)
On error resume Next
	'Variable Section Begin
	
		Dim strMethod
		Dim intVal

	'Variable Section End
	strMethod = strKeywordForReporting
	intVal = GetJTableColCount(objObject ,strKey)
	If intVal = 0 Then
		StoreJColumnCount = 0
		WriteStatusToLogs "Column Count of the specified java table is stored successfully  in the Key '"&Trim(strKey)&"'"

    ElseIf intVal = 1 then
		StoreJColumnCount = 1
		WriteStatusToLogs "The specified java table does not exist."	
	ElseIf intVal = 2 then
		StoreJColumnCount = 1
		WriteStatusToLogs "An error occurred while retrieving the specified java table's Column Count"
	ElseIf intVal = 3 then
		StoreJColumnCount = 1
		WriteStatusToLogs "The Key passed is invalid , please verify."	
	End if

On error goto 0
End Function



'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :			  StoreJRowCount()
'	Purpose :				  Stores the row count of the specified table	
'	Return Value :		 	  Integer(0/1)
'	Arguments :
'		Input Arguments :  	  objObject , strKey
'		Output Arguments : 
'	Function is called by :
'	Function calls :		  GetJTableRowCount()
'	Created on :			  30/06/2009
'	Author :				  Iohbha K
'
'------------------------------------------------------------------------------------------------------------------------------------------

Function StoreJRowCount(byval objObject, byval strKey )
On error resume Next
	'Variable Section Begin
	
	Dim strMethod
	Dim intVal

	'Variable Section End
	strMethod = strKeywordForReporting
	intVal = GetJTableRowCount(objObject ,strKey )
	If intVal = 0 Then
		StoreJRowCount = 0
		WriteStatusToLogs "Row Count of the specified java table is stored successfully  in the Key '"&Trim(strKey)&"'"

    ElseIf intVal = 1 then
		StoreJRowCount = 1
		WriteStatusToLogs "The specified Java Table does not exist."	
	ElseIf intVal = 2 then
		StoreJRowCount = 1
		WriteStatusToLogs "An error occurred while retrieving the specified java table's Row Count"	
	ElseIf intVal = 3 then
		StoreJRowCount = 1
		WriteStatusToLogs "The Key passed is invalid , please verify."	
	End if

On error goto 0
End Function



'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name :				 VerifyJTableCellData ()
' 	Purpose :					 Verifies the table cell data with the expected.
'								 The comparison is case sensitive.
'	Return Value :		 		 Integer(0/1/2)
'   Arguements:	
'		Input Arguments :  		 objTable,intRowNum ,intColNumber,strExpData
'		Output Arguments :       
'	Function is called by :
'	Function calls :			 IsWholeNumber(),CheckObjectExist(),CompareStringValuesCaseSensitive()
'	Created on :				 30/06/2009
'	Author :				     Iohbha K
'
'------------------------------------------------------------------------------------------------------------------------------------------

Function VerifyJTableCellData(Byval objTable,Byval intRowNum , ByVal intColNum,Byval strExpData)
On Error Resume Next

	'Variable Section Begin
			
		Dim strMethod 
		Dim intRow , intCol , intActRowCount , intActColCount
		Dim strTempData, msg
    
	'Variable Section End
	strMethod = strKeywordForReporting
	
	intRow = IsWholeNumber(intRowNum)
	intCol = IsWholeNumber(intColNum)
	If intRow =0 And intCol = 0 Then
		
		If CheckObjectExist(objTable)  Then 

			intActRowCount = objTable.getROProperty("rows")
			intActColCount = objTable.getROProperty("cols")

			If Err.number =0 And Not IsNull(intActRowCount) And Not IsEmpty(intActRowCount) And intActRowCount<>"" And Not IsNull(intActColCount) And Not IsEmpty(intActColCount) And intActColCount<>"" Then 				
				
				intRowNum = CInt(intRowNum)
				intActRowCount = CInt(intActRowCount)
				intColNum = CInt(intColNum)
				intActColCount = CInt(intActColCount)
					If Err.number =0 Then
							If intRowNum > 0 And intRowNum <= intActRowCount And intColNum>0 And intColNum<=intActColCount Then
								
								strTempData = objTable.GetCelldata(intRowNum-1,intColNum-1)
								If CompareStringValuesCaseSensitive(strExpData,strTempData) = 0 Then
									VerifyJTableCellData = 0
									WriteStatusToLogs "Expected cell data: "&strExpData& """" &  "   and   " &""""&"Actual cell data : "&strTempData&""""&"  are equal."
							
								Else
									VerifyJTableCellData = 2
									WriteStatusToLogs "Expected cell data: "&strExpData& """" &  "   and   " &""""&"Actual cell data : "&strTempData&""""&"  are not  equal, please verify."
							
								End if

							ElseIf (intRowNum <1 or intRowNum > intActRowCount) and (intColNum<1 or intColNum>intActColCount) Then 
								VerifyJTableCellData = 1
								WriteStatusToLogs "The row number '"&intRowNum&"' and column number '"&intColNum&"' does not exist in the specified Java Table, please verify. Row/Column index starts from 1."
							ElseIf intRowNum <1 or intRowNum > intActRowCount Then 
								VerifyJTableCellData = 1
								WriteStatusToLogs "The row number '"&intRowNum&"' does not exist in the specified Java Table, please verify. Row index starts from 1."
								
							ElseIf intColNum<1 or intColNum>intActColCount Then 
								VerifyJTableCellData = 1
								WriteStatusToLogs "The column number '"&intColNum&"' does not exist in the specified Java Table, please verify. Column index starts from 1."
								
							End If

					Else
						If Err.number =6 Then
							msg = "Row/Column Number provided is not a valid integer OR out of range of the valid integer. Please verify."
						Else
							msg = "An error occurred during action. Please verify."
						End If
						VerifyJTableRowCount = 1
						WriteStatusToLogs msg

					End If
				
			Else
				VerifyJTableCellData = 1
				WriteStatusToLogs "Action failed to fetch the Java Table data. Please verify."
			End If
		Else
			VerifyJTableCellData  = 1
			WriteStatusToLogs "Specified Java Table does not exist, please verify."
		End If
		
	ElseIf intRow = 1 And intCol = 1 then
		VerifyJTableCellData = 1
		WriteStatusToLogs "Given row '"&intRowNum&"' and column '"&intColNum&"' are not natural numbers, please verify."	
	ElseIf intRow = 1 Then
		VerifyJTableCellData = 1
		WriteStatusToLogs "Given row '"&intRowNum&"' is a not natural number, please verify."	
	
	ElseIf intCol = 1 Then
		VerifyJTableCellData = 1
		WriteStatusToLogs "Given column '"&intColNum&"' is not a natural number, please verify."	
	End if

On Error goto 0
End function


'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name :				 VerifyJTableRowCount()
' 	Purpose :					 Verifies the table row count with the expected row count
'	Return Value :		 		 Integer(0/1/2)
'   Arguements:	
'		Input Arguments :  		 objTable,intTotalRowCount
'		Output Arguments :       
'	Function is called by :
'	Function calls :			 CheckObjectExist() , IsWholeNumber()
'	Created on :				 30/06/2009
'	Author :				     Iohbha K
'
'------------------------------------------------------------------------------------------------------------------------------------------

Function VerifyJTableRowCount(Byval objTable,Byval intTotalRowCount )
On Error Resume Next

	'Variable Section Begin
			
		Dim strMethod
		Dim  intTotalRows, msg
    
	'Variable Section End
	strMethod = strKeywordForReporting

	If IsWholeNumber(intTotalRowCount) = 0 Then 
		If CheckObjectExist(objTable)  Then 
			intTotalRows=objTable.getROProperty("rows")
			If Err.number =0 And Not IsNull(intTotalRows) And Not IsEmpty(intTotalRows) And intTotalRows<>"" Then 				
				intTotalRowCount = CInt(intTotalRowCount)
				intTotalRows = CInt(intTotalRows)
				If Err.number =0 Then
						If intTotalRowCount = intTotalRows Then
							VerifyJTableRowCount = 0
							WriteStatusToLogs "The Actual row count :'"&intTotalRows&"' matches the Expected row count :'"&intTotalRowCount&"'"
						Else
							VerifyJTableRowCount = 2
							WriteStatusToLogs "The Actual row count :'"&intTotalRows&"' does not match the Expected row count :'"&intTotalRowCount&"' , please verify."
						
						End If
				Else
						If Err.number =6 Then
							msg = "Expected RowCount is not a valid integer OR out of range of the valid integer. Please verify."
						Else
							msg = "An error occurred during action. Please verify."
						End If
						VerifyJTableRowCount = 1
						WriteStatusToLogs msg

				End If
			Else
				VerifyJTableRowCount = 1
				WriteStatusToLogs "Action failed to fetch the Java Table data. Please verify."
			End if

		Else
			VerifyJTableRowCount  = 1
			WriteStatusToLogs "Specified Java Table does not exist, please verify."
		End If

	Else
		VerifyJTableRowCount = 1
		WriteStatusToLogs "Given row data '"&intTotalRowCount&"' is not a natural number, please verify."	
	End if

		
On Error GoTo 0  
End Function


'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name :				 VerifyDataExistInJColumn()
' 	Purpose :					 Verifies the expected data should not exist in specified table's column.
'								 The comparison is case sensitive.
'	Return Value :		 		 Integer(0/1/2)
'   Arguements:	
'		Input Arguments :  		 objTable,intColNumber,strData
'		Output Arguments :       
'	Function is called by :
'	Function calls :			 CheckObjectExist(), IsWholeNumber(),GetJTableColData(), CompareStringValuesCaseSensitive()
'	Created on :				 29/06/2009
'	Author :					 Iohbha k
'
'------------------------------------------------------------------------------------------------------------------------------------------

Function VerifyDataExistInJColumn (Byval objTable,Byval intColNumber , Byval strData)
On Error Resume Next

	'Variable Section Begin
		
		Dim strMethod
		Dim intActCol , intCnt
		Dim arrActData , strCompRes
		Dim blnFound
		blnFound = false
		
	'Variable Section End
	strMethod = strKeywordForReporting

	If IsWholeNumber(intColNumber) = 0 Then
		If CheckObjectExist(objTable)  Then
			intActCol = objTable.getROProperty("cols") 	
			If Err.number =0 And Not IsNull(intActCol) And Not IsEmpty(intActCol) And intActCol<>"" Then
				
				intColNumber = CInt(intColNumber)
				intActCol = CInt(intActCol)

				If intColNumber>0 And intColNumber <= intActCol Then
					
					arrActData = GetJTableColData(objTable,intColNumber-1)
					If Not IsNull(intColNumber) And Not IsEmpty(intColNumber) Then
						
						For intCnt = 0 To UBound(arrActData)
							strCompRes = CompareStringValuesCaseSensitive(strData,arrActData(intCnt))
							If strCompRes = 0 Then
								blnFound = True
								Exit for
							End if
						Next

						If blnFound = True Then 
							VerifyDataExistInJColumn = 0
							WriteStatusToLogs "The data '"&strData&"' exist in the column number '"&intColNumber&"'"
						Else
							VerifyDataExistInJColumn = 2
							WriteStatusToLogs "The data '"&strData&"' does not exist in the column number '"&intColNumber&"' , please verify."
						
						End if

					Else
						VerifyDataExistInJColumn = 1
						WriteStatusToLogs "Action failed to fetch the Java Table column data. Please verify."
					End If
					
				Else
					VerifyDataExistInJColumn = 1
					WriteStatusToLogs "The column number '"&intColNumber&"' does not exist in the specified Java Table, please verify. Column index starts from 1."
				End If
				
			Else
				VerifyDataExistInJColumn = 1
				WriteStatusToLogs "Action failed to fetch the Java Table data. Please verify."
			End if
			
		Else
			VerifyDataExistInJColumn = 1
			WriteStatusToLogs "The specified table does not exist, please verify."
			
		End If
		
	Else
		VerifyDataExistInJColumn = 1
		WriteStatusToLogs "Given column data '"&intColNumber&"' is not a natural number, please verify."	
	End if

 
On Error GoTo 0  
End Function


'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name :				 VerifyDataNotExistInJColumn()
' 	Purpose :					 Verifies the expected data should not exist in specified table's column
'								 The comparison is case-sensitive.
'	Return Value :		 		 Integer(0/1/2)
'   Arguements:	
'		Input Arguments :  		 objTable,intColNumber,strData
'		Output Arguments :       
'	Function is called by :
'	Function calls :			 CheckObjectExist(), IsWholeNumber(),GetJTableColData(), CompareStringValuesCaseSensitive()
'	Created on :				 29/06/2009
'	Author :					 Iohbha k
'
'------------------------------------------------------------------------------------------------------------------------------------------

Function VerifyDataNotExistInJColumn (Byval objTable,Byval intColNumber , Byval strData)
On Error Resume Next

	'Variable Section Begin

		Dim strMethod	
		Dim intActCol , intCnt
		Dim arrActData , strCompRes
		Dim blnFound
		blnFound = false
		
	'Variable Section End
	strMethod = strKeywordForReporting

	If IsWholeNumber(intColNumber) = 0 Then
		If CheckObjectExist(objTable)  Then
			intActCol = objTable.getROProperty("cols") 	
			If Err.number =0 And Not IsNull(intActCol) And Not IsEmpty(intActCol) And intActCol<>"" Then
				
				intColNumber = CInt(intColNumber)
				intActCol = CInt(intActCol)

				If intColNumber>0 And intColNumber <= intActCol Then
					arrActData = GetJTableColData(objTable,intColNumber-1)
					If Not IsNull(intColNumber) And Not IsEmpty(intColNumber) Then
						
						For intCnt = 0 To UBound(arrActData)
							strCompRes = CompareStringValuesCaseSensitive(strData,arrActData(intCnt))
							If strCompRes = 0 Then
								blnFound = True
								Exit for
							End if
						Next

						If blnFound = True Then 
							VerifyDataNotExistInJColumn = 2
							WriteStatusToLogs "The data '"&strData&"' exist in the column number '"&intColNumber&"', please verify."
						Else
							VerifyDataNotExistInJColumn = 0
							WriteStatusToLogs "The data '"&strData&"' does not exist in the column number '"&intColNumber&"'"
						
						End if

					Else
						VerifyDataNotExistInJColumn = 1
						WriteStatusToLogs "Action failed to fetch the Java Table column data. Please verify."
					End If
					
				Else
					VerifyDataNotExistInJColumn = 1
					WriteStatusToLogs "The column number '"&intColNumber&"' does not exist in the specified Java Table, please verify. Column index starts from 1."
				End If
				
			Else
				VerifyDataNotExistInJColumn = 1
				WriteStatusToLogs "Action failed to fetch the Java Table data. Please verify."
			End if
			
		Else
			VerifyDataNotExistInJColumn = 1
			WriteStatusToLogs "The specified table does not exist, please verify."
			
		End If
		
	Else
		VerifyDataNotExistInJColumn = 1
		WriteStatusToLogs "Given column data '"&intColNumber&"' is not a natural number, please verify."	
	End if

 
On Error GoTo 0  
End Function


'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name :				 CompareJTableRowData()
' 	Purpose :					 Compares expected column Data with given data in a specified java table.
'								 the column number starts from 1. The comparison is non-case sensitive.
'	Return Value :		 		 Integer(0/1/2)
'   Arguements:	
'		Input Arguments :  		 objTable,intRowNumber,arrExpData
'		Output Arguments :       
'	Function is called by :
'	Function calls :			 IsWholeNumber(),CheckObjectExist(), GetJTableRowData(),MatchEachArrData()
'	Created on :				 29/06/2009
'	Author :					 Iohbha K
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function CompareJTableRowData(ByVal objTable,  ByVal intRowNumber, ByVal arrExpData)
On Error Resume Next

	'Variable Section Begin
		
		 Dim strMethod
		 Dim intActRow , arrRowData , blnRes
				
	'Variable Section End
	 strMethod = strKeywordForReporting

	If IsWholeNumber(intRowNumber)= 0 Then
		If CheckObjectExist(objTable)  Then 
			intActRow = objTable.getROProperty("rows")
			If Err.number =0 And Not IsNull(intActRow) And Not IsEmpty(intActRow) And intActRow<>"" Then
				
				intRowNumber = CInt(intRowNumber)
				intActRow = CInt(intActRow)

				If intRowNumber >0 And  intRowNumber<=intActRow Then
					arrRowData = GetJTableRowData(objTable ,intRowNumber-1)
					If Not IsNull(arrRowData) And Not IsEmpty(arrRowData) Then
						blnRes = MatchEachArrData(arrExpData, arrRowData)
						If blnRes = True Then
							CompareJTableRowData = 0
							WriteStatusToLogs 	"Table row Data is same as expected."
					
						Else
							CompareJTableRowData = 2
							WriteStatusToLogs 	"Table row Data does not match with expected data."

						End if
					Else
						CompareJTableRowData = 1
						WriteStatusToLogs "Action fails to retrieve the java table row data, please verify."
				
					End if
				Else
					CompareJTableRowData = 1
					WriteStatusToLogs "The row number '"&intRowNumber&"' does not exist in the specified Java Table, please verify. Row index starts from 1."
				End if
			Else
				CompareJTableRowData = 1
				WriteStatusToLogs 	"Action Fails to get the specified java table row data, please verify."
			End if
		Else
			CompareJTableRowData = 1
			WriteStatusToLogs 	"Specified Java Table does not exist, please verify."

		End If

	Else
		CompareJTableRowData = 1
		WriteStatusToLogs 	"Row number "& intRowNumber &" is not a valid positive integer, please verify."

	End If
 	
On Error GoTo 0
End Function






'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name :				 CompareJTableColumnData()
' 	Purpose :					 Compares expected column Data with given data in a specified java table.
'								 the column number starts from 1. The comparison is non-case sensitive.
'								 The actual and expected data are trimmed before comparison.
'	Return Value :		 		 Integer(0/1/2)
'   Arguements:	
'		Input Arguments :  		 objTable,intColNumber,arrExpData
'		Output Arguments :       
'	Function is called by :
'	Function calls :			 IsWholeNumber(),CheckObjectExist(), GetJTableColData() , MatchEachArrData()
'	Created on :				 29/06/2009
'	Author :					 Iohbha K
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function CompareJTableColumnData(ByVal objTable,  ByVal intColNumber, ByVal arrExpData)
On Error Resume Next

	'Variable Section Begin
		
		 Dim strMethod
		 Dim intActCol , arrColumnData , blnRes, msg
				
	'Variable Section End
	strMethod = strKeywordForReporting

	If IsWholeNumber(intColNumber)= 0 Then
		If CheckObjectExist(objTable)  Then 
			intActCol = objTable.getROProperty("cols")
			If Err.number =0 And Not IsNull(intActCol) And Not IsEmpty(intActCol) And intActCol<>"" Then
				
				intColNumber = CInt(intColNumber)
				intActCol = CInt(intActCol)
				If Err.number =0 Then
						If intColNumber >0 And  intColNumber<=intActCol Then
							arrColumnData = GetJTableColData(objTable ,intColNumber-1)
							If Not IsNull(arrColumnData) And Not IsEmpty(arrColumnData) Then
								blnRes = MatchEachArrData(arrExpData, arrColumnData)
								If blnRes = True Then
									CompareJTableColumnData = 0
									WriteStatusToLogs 	"Table column Data is same as expected."
							
								Else
									CompareJTableColumnData = 2
									WriteStatusToLogs 	"Table column Data does not match with expected value."

								End If
								
							Else
								CompareJTableColumnData = 1
								WriteStatusToLogs 	"Action fails to retrieve the java table column data, please verify."
						
							End if
						Else
							CompareJTableColumnData = 1
							WriteStatusToLogs 	"The column number '"&intColNumber&"' does not exist in the specified Java Table, please verify. Column index starts from 1."
						End If
					
				Else
						If Err.number =6 Then
							msg = "Coulmn number provided is not a valid integer OR out of range of the valid integer. Please verify."
						Else
							msg = "An error occurred during action. Please verify."
						End If
						VerifyJTableRowCount = 1
						WriteStatusToLogs msg

				End If
			Else
				CompareJTableColumnData = 1
				WriteStatusToLogs 	"Action Fails to get the specified java table column data, please verify."
			End if
		Else
			CompareJTableColumnData = 1
			WriteStatusToLogs 	"Specified Java Table does not exist, please verify."

		End If

	Else
		CompareJTableColumnData = 1
		WriteStatusToLogs 	"Given column Number is not an integer, please verify."

	End If
 	
On Error GoTo 0
End Function

'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :				 VerifyJTableExist()
'	Purpose :					 Verifies whether the Java table exist.
'	Return Value :		 		 Integer(0/2)
'	Arguments :
'		Input Arguments :  		 objTable
'		Output Arguments : 
'	Function is called by :
'	Function calls :			 CheckObjectExist() 
'	Created on :				 29/06/2009
'	Author :					 Iohbha K
'
'------------------------------------------------------------------------------------------------------------------------------------------

Function VerifyJTableExist(ByVal objTable)
On Error Resume Next
	'Variable Section Begin

	   Dim strMethod

		'Variable Section End
		strMethod = strKeywordForReporting
	
	If CheckObjectExist(objTable) Then 
	   
			VerifyJTableExist = 0
			WriteStatusToLogs "The Java table Exist."
			
	Else
			VerifyJTableExist = 2
			WriteStatusToLogs "The Java Table Does not Exist, please verify."
			
	End If 
	  
	
On Error GoTo 0
End Function

'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :				 VerifyJTableNotExist()
'	Purpose :					 Verifies that the table should not exist.
'	Return Value :		 		 Integer(0/2)
'	Arguments :
'		Input Arguments :  		 objTable
'		Output Arguments : 
'	Function is called by :
'	Function calls :			 CheckObjectNotExist() 
'	Created on :				 29/06/2009
'	Author :					 Iohbha K
'
'------------------------------------------------------------------------------------------------------------------------------------------

Function VerifyJTableNotExist(ByVal objTable)
On Error Resume Next
	'Variable Section Begin

	  	Dim strMethod 

		'Variable Section End
		strMethod = strKeywordForReporting
	
	If CheckObjectNotExist(objTable) Then 
	   
			VerifyJTableNotExist = 0
			WriteStatusToLogs "The specified Java table does not exist."
			
	Else
			VerifyJTableNotExist = 2
			WriteStatusToLogs "The specified Java table Exist, please verify."
			
	End If 
	
On Error GoTo 0
End Function


'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :				 StoreJRowNumberHavingCellData()
'	Purpose :					 Stores the row number of the Nth occurence of the specified cell data within a specified column
'	Return Value :		 		 Integer(0/1/2)
'	Arguments :
'		Input Arguments :  		 objTable, strKey, intColNo, strCellData, instanc, increment
'		Output Arguments : 
'	Function is called by :
'	Function calls :			 IsWholeNumber(), GetRowNumberForNInstanceofDataInJColumn(), WriteStatusToLogs
'	Created on :				 03/07/2009
'	Author :					 Vaibhav Kelkar
'
'------------------------------------------------------------------------------------------------------------------------------------------

Function StoreJRowNumberHavingCellData(objJTable,strKey,intColNo, strCellData , instanc , increment)

   On error resume Next
	'Variable Section Begin
		Dim strMethod
		Dim intVal
		Dim intN
		Dim strReportOccur
		intVal = 9
	'Variable Section End
	strMethod = strKeywordForReporting
	
    If IsWholeNumber(instanc) = 0  and IsWholeNumber(increment) = 0 Then
	     intN = cint(instanc) + cint(increment)
		 intVal = GetRowNumberForNInstanceofDataInJColumn(objJTable , strKey , intColNo , strCellData, intN)
	End If
	If intN = 1 Then
		strReportOccur = "1st "
	Elseif intN = 2 Then
		strReportOccur = "2nd  "
   Elseif intN = 3 Then
		 strReportOccur = "3rd  "
   Else
		 strReportOccur = intN&"th  "
	End If
	If intVal = 0 Then
		StoreJRowNumberHavingCellData = 0
		WriteStatusToLogs "The Row Number for the "& strReportOccur&" occurence of the expected Cell Data : '"&strCellData& "', in the specified column, is stored successfully."

    ElseIf intVal = 1 then
		StoreJRowNumberHavingCellData = 1
		WriteStatusToLogs "The specified java table does not exist."	
	ElseIf intVal = 2 then
		StoreJRowNumberHavingCellData = 2
		WriteStatusToLogs strReportOccur&" occurence of the expected Cell Data : '"&strCellData& "' was not found in the table. Please verify."
	ElseIf intVal = 3 then
		StoreJRowNumberHavingCellData = 1
		WriteStatusToLogs "The Key passed is invalid , please verify."
	ElseIf intVal = 4 then
		StoreJRowNumberHavingCellData = 1
		WriteStatusToLogs "The cell data passed is either null or empty , please verify."	
	ElseIf intVal = 5 then
		StoreJRowNumberHavingCellData = 1
		WriteStatusToLogs "The column number '"&intColNo&"' is not a whole number, please verify."
	ElseIf intVal = 6 then
		StoreJRowNumberHavingCellData = 1
		WriteStatusToLogs "Failed to retrieve data from the specified java table, please verify."
	ElseIf intVal = 7 then
		StoreJRowNumberHavingCellData = 1
		WriteStatusToLogs "The column number '"&intColNo&"' does not exist in the specified table, please Verify. Column index starts from 1."
	Elseif  intVal = 9 then
		StoreJRowNumberHavingCellData = 1
		WriteStatusToLogs "The Instance or the increment argument is not valid whole number, please verify."
	End if

On error goto 0

End Function
'=======================Code Section End=============================================

'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :			  StoreJCellData()
'	Purpose :				  Stores the cell data in a given string
'	Return Value :		 	  Integer(0/1)
'	Arguments :
'		Input Arguments :  	 
'							  1. strKey: The unique key to store the value
'							  2. intRow: The start posion of the string
'							  3. intCol: the number of charecters to be extracted
'		Output Arguments : 
'	Function is called by :	  	
'	Function calls :		  WriteStatusToLogs()
'
'------------------------------------------------------------------------------------------------------------------------------------------


Function StoreJCellData(byval objJTable , byval strKey , byval intRowNum, byval intColNum)
On Error Resume Next
	'Variable Section Begin

		Dim strMethod
		Dim strTableCellData , intRow , intCol , intActRowCount , intActColCount
	   
	'Variable Section End
	strMethod = strKeywordForReporting
	if not (intRowNum = cstr(cint((intRowNum)))) or not (intColNum = cstr(cint((intColNum)))) then
		StoreJCellData =1
	  	WriteStatusToLogs "Invalid RowNumber or ColumnNumber."
		exit function
	end if 
	
	 If Isempty(intRowNum) or isnull(intRowNum)  or trim(intRowNum)="" Then
	     StoreJCellData = 1
	 Else
		If IsKey(strKey) = 0 Then
			intRow = IsWholeNumber(intRowNum)
			intCol = IsWholeNumber(intColNum)
			If intRow =0 And intCol = 0 Then
				If CheckObjectExist(objJTable)  Then
					intActRowCount = objJTable.getROProperty("rows")
					intActColCount = objJTable.getROProperty("cols")
					intRowNum = CInt(intRowNum)
					intActRowCount = CInt(intActRowCount)
					intColNum = CInt(intColNum)
					intActColCount = CInt(intActColCount)


					If Err.number =0 And Not IsNull(intActRowCount) And Not IsEmpty(intActRowCount) And intActRowCount<>"" And Not IsNull(intActColCount) And Not IsEmpty(intActColCount) And intActColCount<>"" Then 				
						
						If intRowNum > 0 And intRowNum <= intActRowCount And intColNum>0 And intColNum<=intActColCount Then
						
							strTableCellData = objJTable.GetCellData(intRowNum-1,intColNum-1)
							strTableCellData = PrefixEscape(strTableCellData)
							AddItemToStorageDictionary strKey , strTableCellData
							If err.Number = 0 Then
								StoreJCellData = 0
								WriteStatusToLogs "The Cell Data '"& strTableCellData &"'   is stored sucessfully."
							Else
								StoreJCellData = 1
								WriteStatusToLogs "The Cell Data  '"& strTableCellData &"' is Not stored sucessfully."
							End If
						ElseIf (intRowNum <1 or intRowNum > intActRowCount ) And (intColNum<1 or intColNum>intActColCount) Then 
							StoreJCellData = 1
							WriteStatusToLogs 	"The row number '"&intRowNum&"' and column number '"&intColNum&"' does not exist in the specified Java Table, please verify. Row/Column index starts from 1."
						ElseIf intRowNum <1 or intRowNum > intActRowCount Then 
							StoreJCellData = 1
							WriteStatusToLogs 	"The row number '"&intRowNum&"' does not exist in the specified Java Table, please verify. Row index starts from 1."
							
						ElseIf intColNum<1 or intColNum>intActColCount Then 
							StoreJCellData = 1
							WriteStatusToLogs 	"The column number '"&intColNum&"' does not exist in the specified Java Table, please verify.Column index starts from 1."
						End if
					Else
						StoreJCellData = 1
						WriteStatusToLogs 	"Action failed to fetch the Java Table data. Please verify."
					End If
			
				Else
					StoreJCellData = 1
					WriteStatusToLogs 	"The specified Java Table does not exist, please verify."	
				End if 
			Else
				StoreJCellData = 1
				WriteStatusToLogs 	"Given row '"&intRowNum&"' and/or column '"&intColNum&"' is not a positive number, please verify."	

			End if
		Else
			StoreJCellData = 1
			WriteStatusToLogs "The key passed is invalid. Please verify."
		End if
	End if
	   		
On Error GoTo 0
End Function

'-------------------------------------------------------------------------------------------------
'	Function Name :			  StoreJRowNumberContainsMaximumValue()
'	Purpose :				  Stores row number in a given string which contains the maximum number
'	Return Value :		 	  Integer(0/1)
'	Arguments :
'		Input Arguments :  	  1. strKey :The first numeric number
'							  2. intColNumber: The column number which contains the maximu value
'			
'		Output Arguments : 
'	Function is called by :	  	
'	Function calls :		  WriteStatusToLogs()
'-------------------------------------------------------------------------------------------------

Function StoreJRowNumberContainsMaximumValue(Byval objTable, Byval strKey, ByVal intColNumber)
   On Error Resume Next
   	'Variable Section Begin
		
		Dim strMethod
		Dim intLoopRow , intLoopCol			
		Dim intRowCount , intColCount  
		Dim strActCellData , blnFound , intRowFound
		Dim intMaxNum, intMinNum 
		Dim intMaxRowNumber
		 intMaxNum = 0
		 intMinNum = 0
		 intMaxRowNumber = 0
		blnFound = false
		
	'Variable Section End
	strMethod = strKeywordForReporting

   If IsInteger(intColNumber) = 0 Then
		If CheckObjectExist(objTable)  Then
			intRowCount = objTable.GetROProperty("rows")
			
			If intRowCount > 0 Or Not IsEmpty(intRowCount) Or Not IsNull(intRowCount)Then
				intColCount = objTable.GetROProperty("cols")
				If Err.number = 0 Then
					If Cint(intColNumber) >0 And intColNumber<=intColCount Then
						For intLoopRow = 0 To intRowCount-1
							strActCellData = objTable.GetCellData(intLoopRow,intColNumber-1)
							If IsNumeric(strActCellData)  Then
								If strActCellData <> "" And strActCellData >= intMaxNum Then
									intMaxNum = strActCellData
									intMaxRowNumber = intLoopRow+1
								End if
							End If
						Next
						
						If Err.number = 0 Then
							AddItemToStorageDictionary strKey , intMaxRowNumber 			    
							StoreJRowNumberContainsMaximumValue= 0
							WriteStatusToLogs "The row number '"&intMaxRowNumber&"' is stored successfully in the key '"&strKey&"' ."
						Else
							StoreJRowNumberContainsMaximumValue= 2
							WriteStatusToLogs "Fail to stored the row number in the key '"&strKey&"' .Please verify."

						End if
						
					Else
						StoreJRowNumberContainsMaximumValue = 1
						WriteStatusToLogs "The column number '"&intColNumber&"' does not exist in the specified Java Table, please verify.Column index starts from 1."
						Exit function
					End If

				Else
					StoreJRowNumberContainsMaximumValue = 1
					WriteStatusToLogs "An error occurred while capturing the table cell data, please verify."	
					Exit function
				End If

			Else
				StoreJRowNumberContainsMaximumValue = 1
				WriteStatusToLogs "The specified table does not have rows, please verify."
			End If
			
		Else
			StoreJRowNumberContainsMaximumValue = 1
			WriteStatusToLogs "The specified table does not exist, please verify."
			
		End If
		
	Else
		StoreJRowNumberContainsMaximumValue = 1
		WriteStatusToLogs "Given column data '"&intColNumber&"' is not a natural number, please verify."	
	End if

 
On Error GoTo 0  
End Function



'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :				 StoreJRowNumberHavingPartialCellData()
'	Purpose :					 Stores the row number of the Nth occurence of the specified cell data within a specified column
'	Return Value :		 		 Integer(0/1/2)
'	Arguments :
'		Input Arguments :  		 objTable, strKey, intColNo, strCellData, instanc, increment
'		Output Arguments : 
'	Function is called by :
'	Function calls :			 IsWholeNumber(), GetRowNumberForNInstanceofDataInJColumn(), WriteStatusToLogs
'	Created on :				 03/07/2009
'	Author :					 Vaibhav Kelkar
'
'------------------------------------------------------------------------------------------------------------------------------------------

Function StoreJRowNumberHavingPartialCellData(objJTable,strKey,intColNo, strCellData , instanc , increment)

   On error resume Next
	'Variable Section Begin
		Dim strMethod
		Dim intVal
		Dim intN
		Dim strReportOccur
		intVal = 9
	'Variable Section End
	strMethod = strKeywordForReporting
	
    If IsWholeNumber(instanc) = 0  and IsWholeNumber(increment) = 0 Then
	     intN = cint(instanc) + cint(increment)
		 intVal = GetJRowNumberForNInstanceofPartialDataInColumn(objJTable , strKey , intColNo , strCellData, intN)
	End If
	If intN = 1 Then
		strReportOccur = "1st "
	Elseif intN = 2 Then
		strReportOccur = "2nd  "
   Elseif intN = 3 Then
		 strReportOccur = "3rd  "
   Else
		 strReportOccur = intN&"th  "
	End If
	If intVal = 0 Then
		StoreJRowNumberHavingPartialCellData = 0
		WriteStatusToLogs "the Row Number for the "& strReportOccur&" occurence of the expected Cell Data : '"&strCellData& "', in the specified column, is stored successfully."

    ElseIf intVal = 1 then
		StoreJRowNumberHavingPartialCellData = 1
		WriteStatusToLogs "The specified  table does not exist."	
	ElseIf intVal = 2 then
		StoreJRowNumberHavingPartialCellData = 2
		WriteStatusToLogs strReportOccur&" occurence of the expected Cell Data : '"&strCellData& "' was not found in the table. Please verify."
	ElseIf intVal = 3 then
		StoreJRowNumberHavingPartialCellData = 1
		WriteStatusToLogs "The Key passed is invalid , please verify."
	ElseIf intVal = 4 then
		StoreJRowNumberHavingPartialCellData = 1
		WriteStatusToLogs "The cell data passed is either null or empty , please verify."	
	ElseIf intVal = 5 then
		StoreJRowNumberHavingPartialCellData = 1
		WriteStatusToLogs "The column number '"&intColNo&"' is not a whole number, please verify."
	ElseIf intVal = 6 then
		StoreJRowNumberHavingPartialCellData = 1
		WriteStatusToLogs "Failed to retrieve data from the specified  table, please verify."
	ElseIf intVal = 7 then
		StoreJRowNumberHavingPartialCellData = 1
		WriteStatusToLogs "The column number '"&intColNo&"' does not exist in the specified table, please verify."
	Elseif  intVal = 9 then
		StoreJRowNumberHavingPartialCellData = 1
		WriteStatusToLogs "The Instance or the increment argument is not valid whole number, please verify."
	End if

On error goto 0

End Function

'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name			:	GetJRowNumberForNInstanceofPartialDataInColumn()
' 	Purpose					:	Stores the row number of the Nth occurence of the specified cell data within a specified column
' 	Return Value			:	Boolean( 0/1/2/3/4/5/6/7)
' 	Arguments				:
'		Input Arguments		:  	objJTable, strKey, intColNo, strCellData, n
'		Output Arguments	:   
'	Function is called by	:   StoreJRowNumberHavingCellData()
'	Function calls			:   IsWholeNumber(), CheckObjectExist(), CompareStringValuesCaseSensitive(), AddItemToStorageDictionary()
'	Created on				:	03/07/2009
'	Author					:	Vaibhav Kelkar
'
'------------------------------------------------------------------------------------------------------------------------------------------

Function GetJRowNumberForNInstanceofPartialDataInColumn(ByVal objJTable , ByVal strKey , ByVal intColNo , ByVal strCellData, byval n)
On Error Resume Next
	
	'Variable Section Begin
		Dim strMethod
		Dim intActRowCount	, arrColData, intActColCount, intCntFound
		Dim intInstance , intCnt , intCompare
		Dim intN
		Dim blnFound
		Dim strActCellData
		intInstance = 0
		blnFound = false 
	'Variable Section End
	'Variable Section End
	strMethod = strKeywordForReporting

	If IsKey(strKey) = 0 Then
		If Not IsNull(strCellData) And Not IsEmpty(strCellData) Then
			If IsWholeNumber(intColNo) = 0 Then
				If CheckObjectExist(objJTable)  Then 
				   
				 	intActColCount = objJTable.GetROProperty("cols")
					If Err.number = 0 And Not IsEmpty(intActColCount) And Not IsNull(intActColCount) Then
						intActColCount = cint(intActColCount)
						 intActRowCount = objJTable.GetROProperty("rows")
						  If Err.number = 0 And Not IsEmpty(intActRowCount) And Not IsNull(intActRowCount) Then
								intColNo = CInt(intColNo)
								intActRowCount = CInt(intActRowCount)
								intN = Cint(n)						
		
								If intColNo>0 And intColNo <= intActColCount Then
								  
									
										For intCnt = 1 To intActRowCount
											strActCellData = objJTable.GetCellData(intCnt-1 , intColNo-1)

											 intCompare =  Instr(1 , strActCellData , strCellData  )
											If intCompare > 0 Then
											
												intInstance = intInstance + 1
												If  intInstance = intN Then
													intCntFound = intCnt 
													AddItemToStorageDictionary strKey , intCntFound
													blnFound = true
													Exit for
														
												End If
											End if
										Next

										If  blnFound Then
											   GetJRowNumberForNInstanceofPartialDataInColumn = 0
											   WriteStatusToLogs "The row '"&intCntFound&"' is stored successfully." 
										Else
											   GetJRowNumberForNInstanceofPartialDataInColumn = 2
										End If
										
								Else
									GetJRowNumberForNInstanceofPartialDataInColumn = 7
								End If
						Else
								 GetJRowNumberForNInstanceofPartialDataInColumn = 6
						End If
						
					Else
						GetJRowNumberForNInstanceofPartialDataInColumn = 6
					End If
					
   
				Else
					GetJRowNumberForNInstanceofPartialDataInColumn = 1
				End If
				
			Else
				GetJRowNumberForNInstanceofPartialDataInColumn = 5
			End if
		Else
			GetJRowNumberForNInstanceofPartialDataInColumn = 4
		End if
		
	Else
		GetJRowNumberForNInstanceofPartialDataInColumn = 3
	End if

On Error goto 0
End Function

'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :			  SetJCellData()
'	Purpose :				  Set data on the specified cell of the java table.	
'	Return Value :		 	  Integer(0/1)
'	Arguments :
'		Input Arguments :  	  objJTable , intRowNum , intColNum , data
'		Output Arguments : 
'	Function is called by :
'	Function calls :		  IsWholeNumber(),CheckObjectExist(),CheckJavaObjectVisible(),CheckJavaObjectEnabled() 
'	Created on :			  30/06/2009
'	Author :				  Iohbha K
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function SetJCellData(Byval objJTable , Byval intRowNum , ByVal intColNum , ByVal data)
On Error Resume next
	'Variable Section Begin
		Dim strMethod
		Dim intRow , intCol , intActRowCount , intActColCount

	'Variable Section End
	strMethod = strKeywordForReporting

	intRow = IsWholeNumber(intRowNum)
	intCol = IsWholeNumber(intColNum)
	If intRow =0 And intCol = 0 Then

		If CheckObjectExist(objJTable)  Then 

			'If CheckJavaObjectVisible(objJTable) Then 

				'If CheckJavaObjectEnabled(objJTable) Then

					intActRowCount = objJTable.getROProperty("rows")
					intActColCount = objJTable.getROProperty("cols")
					intRowNum = CInt(intRowNum)
					intActRowCount = CInt(intActRowCount)
					intColNum = CInt(intColNum)
					intActColCount = CInt(intActColCount)


					If Err.number =0 And Not IsNull(intActRowCount) And Not IsEmpty(intActRowCount) And intActRowCount<>"" And Not IsNull(intActColCount) And Not IsEmpty(intActColCount) And intActColCount<>"" Then 				
						
						If intRowNum > 0 And intRowNum <= intActRowCount And intColNum>0 And intColNum<=intActColCount Then

                            objJTable.SetCellData intRowNum-1,intColNum-1,data
							If Err.number = 0 Then
								SetJCellData = 0
								WriteStatusToLogs "The data '"&data&"' is set successfully in the cell (row :"&intRowNum&" , column :"&intColNum&")"
							Else
								SetJCellData = 1
								WriteStatusToLogs "Failed to set the data '"&data&"' in the cell (row :"&intRowNum&" , column :"&intColNum&"). Please verify."
							
							End If
							
						ElseIf (intRowNum <1 or intRowNum > intActRowCount ) And (intColNum<1 or intColNum>intActColCount) Then 
							SetJCellData = 1
							WriteStatusToLogs "The row number '"&intRowNum&"' and column number '"&intColNum&"' does not exist in the specified Java Table, please verify."
						ElseIf intRowNum <1 or intRowNum > intActRowCount Then 
							SetJCellData = 1
							WriteStatusToLogs "The row number '"&intRowNum&"' does not exist in the specified Java Table, please verify."
							
						ElseIf intColNum<1 or intColNum>intActColCount Then 
							SetJCellData = 1
							WriteStatusToLogs "The column number '"&intColNum&"' does not exist in the specified Java Table, please verify."
							
						End If

					Else
						SetJCellData = 1
						WriteStatusToLogs "Action failed to fetch the Java Table data. Please verify."
					End If



		Else
			SetJCellData = 1
			WriteStatusToLogs "The specified Java Table does not exist, please verify."	
		End If
		
	ElseIf intRow = 1 And intCol = 1 then
		SetJCellData = 1
		WriteStatusToLogs "Given row '"&intRowNum&"' and column '"&intColNum&"' are not natural numbers, please verify."	
	ElseIf intRow = 1 Then
		SetJCellData = 1
		WriteStatusToLogs "Given row '"&intRowNum&"' is a not natural number, please verify."	
	
	ElseIf intCol = 1 Then
		SetJCellData = 1
		WriteStatusToLogs "Given column '"&intColNum&"' is not a natural number, please verify."	
	End if
On Error goto 0
End Function

'------------------------------------------------------------------------------------------------------------------------------------------
' Function Name :			ActivateCell()
' Purpose :					Clicks on the specified cell of the java table. 
' Return Value :			Integer(0/1)
' Arguments :
'  Input Arguments :		objJTable , intRowNum , intColNum
'  Output Arguments : 
' Function is called by :
' Function calls :			IsWholeNumber(),CheckObjectExist(),CheckJavaObjectVisible(),CheckJavaObjectEnabled() 
' Created on :				30/06/2009
' Author :					Iohbha K
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function ActivateCell (Byval objJTable , Byval intRowNum , ByVal intColNum)
On Error Resume next
	 'Variable Section Begin
	
	  Dim strMethod
	  Dim intRow , intCol , intActRowCount , intActColCount
	 'Variable Section End
	 strMethod = strKeywordForReporting 

	 intRow = IsWholeNumber(intRowNum)
	 intCol = IsWholeNumber(intColNum)
	 If intRow =0 And intCol = 0 Then
	  If CheckObjectExist(objJTable)  Then 
	   'If CheckJavaObjectVisible(objJTable) Then 
		'If CheckJavaObjectEnabled(objJTable) Then
		 intActRowCount = objJTable.getROProperty("rows")
		 intActColCount = objJTable.getROProperty("cols")
		 intRowNum = CInt(intRowNum)
		 intActRowCount = CInt(intActRowCount)
		 intColNum = CInt(intColNum)
		 intActColCount = CInt(intActColCount)

		 If Err.number =0 And Not IsNull(intActRowCount) And Not IsEmpty(intActRowCount) And intActRowCount<>"" And Not IsNull(intActColCount) And Not IsEmpty(intActColCount) And intActColCount<>"" Then     
		  
		  If intRowNum > 0 And intRowNum <= intActRowCount And intColNum>0 And intColNum<=intActColCount Then
		   
		   objJTable.ActivateCell intRowNum-1,intColNum-1
		   If Err.number = 0 Then
			ActivateCell = 0
			WriteStatusToLogs "Click operation is performed successfully on the cell in the row :"&intRowNum&" and column :"&intColNum
		   Else
			ActivateCell = 1
			WriteStatusToLogs "Click operation is not performed successfully on the cell in the row :"&intRowNum&" and column :"&intColNum
		   
		   End If
		   
		  ElseIf (intRowNum <1 or intRowNum > intActRowCount ) And (intColNum<1 or intColNum>intActColCount) Then 
		   ActivateCell = 1
		   WriteStatusToLogs "The row number '"&intRowNum&"' and column number '"&intColNum&"' does not exist in the specified Java Table, please verify."
		  ElseIf intRowNum <1 or intRowNum > intActRowCount Then 
		   ActivateCell = 1
		   WriteStatusToLogs "The row number '"&intRowNum&"' does not exist in the specified Java Table, please verify."
		   
		  ElseIf intColNum<1 or intColNum>intActColCount Then 
		   ActivateCell = 1
		   WriteStatusToLogs "The column number '"&intColNum&"' does not exist in the specified Java Table, please verify."
		   
		  End If
		 Else
		  ActivateCell = 1
		  WriteStatusToLogs "Action failed to fetch the Java Table data. Please verify."
		 End If


	  Else
	   ActivateCell = 1
	   WriteStatusToLogs "The specified Java Table does not exist, please verify." 
	  End If
	  
	 ElseIf intRow = 1 And intCol = 1 then
	  ActivateCell = 1
	  WriteStatusToLogs "Given row '"&intRowNum&"' and column '"&intColNum&"' are not natural numbers, please verify." 
	 ElseIf intRow = 1 Then
	  ActivateCell = 1
	  WriteStatusToLogs "Given row '"&intRowNum&"' is a not natural number, please verify." 
	 
	 ElseIf intCol = 1 Then
	  ActivateCell = 1
	  WriteStatusToLogs "Given column '"&intColNum&"' is not a natural number, please verify." 
	 End if
On Error goto 0
End Function

''------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name :				 StoreJTableColumnData()
' 	Purpose :					 Stores the data of the specified column
'	Return Value :		 		 Integer(0/1)
'   Arguements:	
'		Input Arguments :  		 objTable,intColNumber,intColNumber
'		Output Arguments :       intRes
'	Function is called by :
'	Function calls :			 CheckObjectExist(),GetJTableColData()
'	Created on :				 28/03/2011
'	Author :					 Iohbha K
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function StoreJTableColumnData(ByVal objTable,  ByVal intColNumber, ByVal strKey)
On Error Resume Next
	'Variable Section Begin
		
		 Dim strMethod , i
		 Dim arrActualdata, intColNum,intTotalCols , strArrayData , actualColNo , intval
				
	'Variable Section End
	 strMethod = strKeywordForReporting

	If IsNull(strKey) or Isempty(strKey) or Trim(strKey)="" Then
		StoreJTableColumnData = 1
		WriteStatusToLogs "The key is invalid. Please verify." 
    Else
		If CheckObjectExist(objTable)  Then 			
			If IsInteger(intColNumber)= 0 Then
				intColNum = CInt(intColNumber)
				actualColNo = objTable.getROProperty("cols")
				intTotalCols= Cint(actualColNo)
			
				If intTotalCols>=intColNum and  intColNum>0 Then
					arrActualdata = GetJTableColData(objTable,intColNum-1)
					For i = 0 To UBound(arrActualdata) 
						 arrActualdata(i) = PrefixEscape(arrActualdata(i))
					Next
					If  Not IsNull(arrActualdata)  Then	
						'convert the array items into strings dilimited by array character ^, eg: abc^xyz^12abc
						strArrayData = PrintArrayElements(arrActualdata)	
						intval = AddItemToStorageDictionary(strKey,strArrayData)							
						If  intval = 0 Then
							StoreJTableColumnData = 0
							WriteStatusToLogs "The column "&intColNumber&" data is stored successfully in the Key '"&strKey&"'. The data stored are:"&strArrayData 
									  
						Else
							StoreJTableColumnData = 1
							WriteStatusToLogs "The column "&intColNumber&" data is NOT stored successfully in the Key '"&strKey&"'" 
									  
						End If	
					Else
						StoreJTableColumnData = 1
						WriteStatusToLogs "Action Fails to get table column "&intColNum&" data, please verify."
					End If
				Else
					StoreJTableColumnData = 1
					WriteStatusToLogs "Column Number :" &""""&intColNum&""""&" was not found in Table, there are "&intTotalCols&" columns only. Please verify."
				End If
			Else
				StoreJTableColumnData = 1
				WriteStatusToLogs "Given column number "&intColNumber&" is not a valid Column index, please verify. Column index starts from 1."
			End If
		Else
			StoreJTableColumnData = 1
			WriteStatusToLogs "Table does not exist, please verify."
	
		End If
	End IF
 	
On Error GoTo 0
End Function
'---------------------------------------------------------------------------------------------------
' 	Function Name :				 ActivateJRow(byVal obj, byVal key)
' 	Purpose :					 Activates a javatable row.
'	Return Value :		 		 Integer(0/1)
'   Arguements:	
'		Input Arguments :  		 obj,key
'		Output Arguments :       
'	Function is called by :
'	Function calls :			 
'	Created on :				 30-Aug-2016
'	Author :					 Rajneesh Kumar

Function ActivateJRow(byVal objTable, byVal rowNum)
	On error resume next
	
	Dim strMethod
	
	strMethod = strrowNumwordForReporting
	If isNull(rowNum) or isempty(rowNum) or cint(rowNum) < 0 Then
		WriteStatusToLogs "parameter is incorrect." 
		ActivateJRow= 1
		Exit Function
	End if
	
	If not IsNumeric(rowNum) Then
		WriteStatusToLogs "Row is not a number." 
		ActivateJRow= 1
		Exit Function
	End if
	
	If CheckObjectExist(objTable)  Then
		
		err.number = 0
		objTable.ActivateRow rowNum-1
		
		If err.number = 0 Then
			ActivateJRow= 0
			WriteStatusToLogs "Row activated successfully: " &rowNum
		
		else
			ActivateJRow= 1
			WriteStatusToLogs "Error occurred. "& err.description   'oDictErrorMessages.Item(cstr(err.number))
		End If
				
	Else
		WriteStatusToLogs "Specified table is not found." 
		ActivateJRow= 1
		
	End if

	on error goto 0

End Function

'---------------------------------------------------------------------------------------------------
' 	Function Name :				 DoubleClickJCell(byVal objTable, byVal row, byVal col)
' 	Purpose :					 Double clicks on a cell
'	Return Value :		 		 Integer(0/1)
'   Arguements:	
'		Input Arguments :  		 objTable,row,col
'		Output Arguments :       intRes
'	Function is called by :
'	Function calls :			 
'	Created on :				 30-Aug-2016
'	Author :					 Rajneesh Kumar


Function DoubleClickJCell(byVal objTable, byVal row, byVal col)
	On error resume next
	'msgbox "DoubleClickJCell"
	Dim strMethod
	strMethod = strKeywordForReporting
	
	If isNull(row) or isempty(row) or isNull(col) or not IsNumeric(cint(col)) or isempty(col) or cint(row) < 0 or cint(col) < 0   Then
		WriteStatusToLogs "Parameter(s) is incorrect." 
		DoubleClickJCell= 1
		on error goto 0
		Exit Function
	End if
	
	If CheckObjectExist(objTable) Then
		err.number = 0
		
		objTable.DoubleClickCell row-1,cint(col) -1
		
		If err.number = 0 Then
			DoubleClickJCell=0
			WriteStatusToLogs "Double clicking on Cell successful at row : "& row &",and column: "& col
	
		else
			 
			DoubleClickJCell=1
			WriteStatusToLogs "Error occurred. "& err.description 'oDictErrorMessages.Item(cstr(err.number))
		End if
	Else
		WriteStatusToLogs "Specified table is not found on application." 
		DoubleClickJCell= 1
		
	End if
	
	on error goto 0

End Function
'---------------------------------------------------------------------------------------------------
' 	Function Name :				 ClickCellHavingText(byval objTable, byval col, byval txt)
' 	Purpose :					 Click on a cell having specific text.
'	Return Value :		 		 Integer(0/1)
'   Arguements:	
'		Input Arguments :  		 objTable,col,txt
'		Output Arguments :       intRes
'	Function is called by :
'	Function calls :			 
'	Created on :				 30-Aug-2016
'	Author :					 Rajneesh Kumar


Function ClickCellHavingText(byval objTable, byval col, byval txt)
	 On Error Resume Next
	 Dim  rows,r,val, found, strMethod
	 strMethod = strKeywordForReporting
	 found = 0
	 	
	If isNull(col) or isempty(col) or not IsNumeric((col)) or isNull(txt) or cint(col) < 0 Then 'txt can be empty
		WriteStatusToLogs "Parameter(s) is incorrect." 
		ClickCellHavingText= 1
		on error goto 0
		Exit Function
	End if
	

	 If CheckObjectExist(objTable)  Then
		  rows = objTable.GetROProperty("rows")
			err.number = 0
			For r = 0 To rows-1
		  
			   val = objTable.GetCellData(r,cint(col)-1)
			   
			   	If StrComp(txt,val,1) = 0 Then
						objTable.ClickCell r, cint(col)-1
						found = 1
						Exit for
				Elseif err.number <> 0 then
						ClickCellHavingText = 1
						
						WriteStatusToLogs "Error occurred." & err.description 'oDictErrorMessages.Item(cstr(err.number))
						
						on error goto 0
						exit function
				End If 
			Next
		  
			  If found = 1 Then
			   ClickCellHavingText = 0
				  WriteStatusToLogs "Clicked on cell at row number: " & r+1
			  Else
			   ClickCellHavingText = 1
				WriteStatusToLogs "Could not find the specified cell in table. "
			  End If
	 Else
	    ClickCellHavingText = 1
	    WriteStatusToLogs "Table object not found. "
	 End If
	on error goto 0
End function

Function Q_GetColumnName(ByVal objObject,ByVal ColumnIndex,ByVal key )
	On Error Resume Next

		'Variable Section Begin
	
			Dim strMethod 
			Dim strValue, colname
		'Variable Section End
		strMethod = strKeywordForReporting
		
		If Not(isnumeric(ColumnIndex)) or key = "" or isempty(key) or isnull(key) Then
			Q_GetColumnName = 1
			WriteStatusToLogs "Invalid parameters."
			exit function
		End If
			If CheckObjectExist(objObject) Then 
			  
			     If Not Isempty(ColumnIndex) or Not isnull(ColumnIndex) And Not Isempty(ColumnIndex) or Not isnull(ColumnIndex) Then
				 
							colname = objObject.GetColumnName (ColumnIndex-1)
							
					If Err.Number = 0 Then
						Q_GetColumnName = 0
						WriteStatusToLogs "Column name is" & colname	
						AddItemToStorageDictionary key,colname										  
					Else
						Q_GetColumnName = 1
						WriteStatusToLogs "Error occurred. "& err.description
					End If
					
				Else
						Q_GetColumnName = 1
						WriteStatusToLogs "Invalid parameters."
				End If

			  Else
                    Q_GetColumnName = 1
				   WriteStatusToLogs "The object does not exist, please verify."
			  End If
			
		
	On Error GoTo 0
End Function


Function Q_SelectCell(Byval objJTable , Byval intRowNum , ByVal intColNum)
On Error Resume next
	'Variable Section Begin
		Dim strMethod
		Dim intRow , intCol , intActRowCount , intActColCount, colName

	'Variable Section End
	strMethod = strKeywordForReporting
	
	intRow = IsWholeNumber(intRowNum)
	intCol = IsWholeNumber(intColNum)
	If intRow =0 And intCol = 0 Then

		If CheckObjectExist(objJTable)  Then 

			'If CheckJavaObjectVisible(objJTable) Then 

				'If CheckJavaObjectEnabled(objJTable) Then

					intActRowCount = objJTable.getROProperty("rows")
					intActColCount = objJTable.getROProperty("cols")
					
					intRowNum = CInt(intRowNum)
					intActRowCount = CInt(intActRowCount)
					intColNum = CInt(intColNum)
					intActColCount = CInt(intActColCount)


					If Not IsNull(intActRowCount) or Not IsEmpty(intActRowCount) And intActRowCount<>"" And Not IsNull(intActColCount) or Not IsEmpty(intActColCount) And intActColCount<>"" Then 				
						
						If intRowNum > 0 And intRowNum <= intActRowCount And intColNum>0 And intColNum<=intActColCount Then
							colName = objJTable.GetColumnName (intColNum-1)
							objJTable.SelectCell intRowNum-1, colName
							If Err.number = 0 Then
													  
								Q_SelectCell = 0
								WriteStatusToLogs "Select operation is performed successfully on the cell in the row :"&intRowNum&" and column :"&intColNum
							Else
								Q_SelectCell = 1
								WriteStatusToLogs "The operation cannot be Performed"
							
							End If
							
						ElseIf (intRowNum <1 or intRowNum > intActRowCount ) And (intColNum<1 or intColNum>intActColCount) Then 
							Q_SelectCell = 1
							WriteStatusToLogs "The row number '"&intRowNum&"' and column number '"&intColNum&"' does not exist in the specified Java Table, please verify. Row/Column index starts from 1."
						ElseIf intRowNum <1 or intRowNum > intActRowCount Then 
							Q_SelectCell = 1
							WriteStatusToLogs "The row number '"&intRowNum&"' does not exist in the specified Java Table, please verify. Row index starts from 1."
							
						ElseIf intColNum<1 or intColNum>intActColCount Then 
							Q_SelectCell = 1
							WriteStatusToLogs "The column number '"&intColNum&"' does not exist in the specified Java Table, please verify.Column index starts from 1."
							
						End If

					Else
						Q_SelectCell = 1
						WriteStatusToLogs "Action failed to fetch the Java Table data. Please verify."
					End If




		Else
			Q_SelectCell = 1
			WriteStatusToLogs "The specified Java Table does not exist, please verify."	
		End If
		
	ElseIf intRow = 1 And intCol = 1 then
		Q_SelectCell = 1
		WriteStatusToLogs "Given row '"&intRowNum&"' and column '"&intColNum&"' are not natural numbers, please verify."	
	ElseIf intRow = 1 Then
		Q_SelectCell = 1
		WriteStatusToLogs "Given row '"&intRowNum&"' is a not natural number, please verify."	
	
	ElseIf intCol = 1 Then
		Q_SelectCell = 1
		WriteStatusToLogs "Given column '"&intColNum&"' is not a natural number, please verify."	
	End if
On Error goto 0
End Function