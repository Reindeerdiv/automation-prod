'Function SelectJCheckBox(ByVal objCheckBox)
'Function ClearJCheckBox(ByVal objCheckBox)
'Function verify.CheckBoxChecked(ByVal objCheckBox)
'Function verify.CheckBoxUnChecked(ByVal objCheckBox)
'Function Q_verify.Existence(ByVal objObject,ByVal strExistence)
'Function Q_verify.Enability(ByVal objCheckBox,ByVal blnEnability)

'=======================Code Section Begin=============================================

Option Explicit 		'Forces explicit declaration of all variables in a script. 
'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :				  SelectJCheckBox()
' 	Purpose :						Selects a specified checkbox.
' 	Return Value :		 		   Integer(0/1/2)
' 	Arguments :
'		Input Arguments :  		objCheckBox 
'		Output Arguments :      
'	Function is called by :
'	Function calls :				CheckObjectExist()
'	Created on :				   11/03/2008
'	Author :						  Rathna Reddy 
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function SelectJCheckBox(ByVal objCheckBox)
	On Error Resume Next
		'Variable Section Begin
		
		Dim strMethod 
		Dim  intActValue, blnstatus

		'Variable Section End
		strMethod = strKeywordForReporting
		blnstatus = False
        If CheckObjectExist(objCheckBox) Then
            If CheckJavaObjectEnabled(objCheckBox) Then
				 intActValue = objCheckBox.GetROProperty("value")
				 
				 If intActValue = "1" Then 
					SelectJCheckBox = 0
					WriteStatusToLogs	"CheckBox already selected."
					Exit function
				 End If
				 
				 If Vartype(intActValue) <> vbEmpty  Then
					blnstatus = true
				 End If
				 
				 If blnstatus  Then
				   
								objCheckBox.Set "ON"
								intActValue = objCheckBox.GetROProperty("value")
								
								If Err.Number = 0 Then
										If intActValue = "1" Then
											SelectJCheckBox = 0
											WriteStatusToLogs "The CheckBox got selected successfully."
										Else
											SelectJCheckBox = 1
											WriteStatusToLogs "The CheckBox could not get selected, please verify."
										End If
						  
							Else
								SelectJCheckBox = 1
								WriteStatusToLogs	"The error occurred during value set to ON, please verify."
							End If
					Else
					  SelectJCheckBox = 1
					  WriteStatusToLogs	"Could not capture checkbox state, please verify."
					End If
                Else
					SelectJCheckBox = 1
					WriteStatusToLogs	"The CheckBox was not enabled, please verify."
				End If
					
			Else
				SelectJCheckBox = 1
					WriteStatusToLogs	"The CheckBox does not exist, please verify."
			End If
					
	 On Error GoTo 0
End Function

'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :				  ClearJCheckBox()
' 	Purpose :						Clears a specified checkbox.
' 	Return Value :		 		   Integer( 0/1)
' 	Arguments :
'		Input Arguments :  		objCheckBox 
'		Output Arguments : 
'	Function is called by :
'	Function calls :				CheckObjectExist(), CheckObjectEnabled()
'	Created on :				   11/03/2008
'	Author :						  Ashok Jakkani
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function ClearJCheckBox(ByVal objCheckBox)
	On Error Resume Next
		'Variable Section Begin
			Dim strMethod
            Dim  intActValue, blnstatus 
			blnstatus = False
		'Variable Section End
		strMethod = strKeywordForReporting
		
		 If CheckObjectExist(objCheckBox) Then
            If CheckJavaObjectEnabled(objCheckBox) Then
				 intActValue = objCheckBox.GetROProperty("value")
				 If Vartype(intActValue) <> vbEmpty  Then
					blnstatus = true
				 End If
				 
				 If  blnstatus Then
					  If intActValue = UCase("1") Then
                        objCheckBox.Set "OFF"
                        intActValue = objCheckBox.GetROProperty("value")
                                If  err.Number = 0 Then
										If UCase(intActValue) = Ucase("0") Then
											ClearJCheckBox = 0
											WriteStatusToLogs	"The CheckBox got de-selected successfully."
										Else
											ClearJCheckBox = 1
											WriteStatusToLogs	"The CheckBox could not get de-selected, please verify."
										End If
								Else			'err.Number = 0
									ClearJCheckBox = 1
									  WriteStatusToLogs	"The error occurred during value set to OFF, please verify."
								End If
                            ElseIf UCase(intActValue) = UCase("0") Then                      ' If intActValue = UCase("1")
							ClearJCheckBox = 0
							WriteStatusToLogs	"The CheckBox was already de-selected"
							Else																										' If intActValue = UCase("1")
							ClearJCheckBox = 1
							WriteStatusToLogs	"The CheckBox was in different state , please verify."
						End If

					Else			 'Vartype(intActValue) <> vbEmpty   
						ClearJCheckBox = 1
					    WriteStatusToLogs	"Could not capture checkbox state, please verify."
						
					End If
                Else				'CheckJavaObjectEnabled
					ClearJCheckBox = 1
					WriteStatusToLogs	"The CheckBox was not enabled, please verify."
				End If
					
			Else				'CheckObjectExist
				ClearJCheckBox = 1
					WriteStatusToLogs	"The CheckBox does not exist, please verify."
			End If
				
	 On Error GoTo 0
End Function
'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :				  verify.CheckBoxChecked()
'	Purpose :					  Verifies whether the checkbox is in checked state
'	Return Value :		 		   Integer( 0/1)
'	Arguments :
'		Input Arguments :  		  objCheckBox 
'		Output Arguments : 	      intActValue,intExpValue
'	Function is called by :
'	Function calls :				CheckObjectExist()
'	Created on :				   14/08/2008
'	Author :						  Ashok
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function VerifyCheckBoxChecked(ByVal objCheckBox)
	On Error Resume Next
		'Variable Section Begin
			Dim strMethod
			Dim intActValue,intExpValue, blnstatus
                blnstatus = False
		'Variable Section End	
		 strMethod = strKeywordForReporting
			If CheckObjectExist(objCheckBox) Then
			    intExpValue = "1"
				 intActValue = objCheckBox.GetROProperty("value") 
						 If Vartype(intActValue) <> vbEmpty  And Err.Number = 0 Then
							blnstatus = true
						 End If

						 If blnstatus  Then
								 If intActValue = intExpValue Then
										verify.CheckBoxChecked = 0
										WriteStatusToLogs 	"CheckBox was checked as expected."
									Else
										verify.CheckBoxChecked = 2
										WriteStatusToLogs 	"CheckBox was not checked, please verify."
								End if
						Else
						verify.CheckBoxChecked = 1
								WriteStatusToLogs	"Could not capture checkbox state, please verify."
						End If
					
			Else
			verify.CheckBoxChecked = 1
			WriteStatusToLogs	"The CheckBox does not exist, please verify."

			End If
		
	 On Error GoTo 0
End Function	 

'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :				  verify.CheckBoxUnChecked()
'	Purpose :					  Verifies whether the checkbox is in UnChecked state
'	Return Value :		 		   Integer( 0/1)
'	Arguments :
'		Input Arguments :  		objCheckBox 
'		Output Arguments : 	   intActValue,intExpVal
'	Function is called by :
'	Function calls :				CheckObjectExist()
'	Created on :				   11/03/2008
'	Author :						  Ashok Jakkani 
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function VerifyCheckBoxUnChecked(ByVal objCheckBox)
	On Error Resume Next
		'Variable Section Begin
			Dim strMethod
			Dim intActValue,intExpValue, blnstatus 
			blnstatus  =False
			
		'Variable Section End
		strMethod = strKeywordForReporting
			If CheckObjectExist(objCheckBox) Then
			    intExpValue = "0"
				 intActValue = objCheckBox.GetROProperty("value") 

					If Vartype(intActValue) <> vbEmpty  And Err.Number = 0 Then
						blnstatus = true
					 End If

					 If blnstatus  Then				 
					 
								 If intActValue = intExpValue Then
										verify.CheckBoxUnChecked = 0
										WriteStatusToLogs "CheckBox was Un-checked as expected."
									Else
										verify.CheckBoxUnChecked = 2
														WriteStatusToLogs "CheckBox was checked, please verify."
								End if
					Else
					verify.CheckBoxUnChecked = 1
					WriteStatusToLogs "Could not capture checkbox state, please verify."
	
					
					End if 
			
			Else
					verify.CheckBoxUnChecked = 1
					WriteStatusToLogs "The CheckBox does not exist, please verify."
		End If
		
	 On Error GoTo 0
End Function
'----------------------------------------------------------------------------------------------------------------
'	Function Name :				  Q_CheckIfTrue()
'	Purpose :					  keyword checks the checkbox, only if the parameter passed is true.
'	Return Value :		 		   Integer( 0/1)
'	Arguments :
'		Input Arguments :  		objCheckBox,blnCheck 
'		Output Arguments : 	   intActValue
'	Function is called by :
'	Function calls :				CheckObjectExist(),CheckJavaObjectEnabled()
'	Created on :				   29/07/2016
'	Author :					   Qualitia 
'----------------------------------------------------------------------------------------------------------------
Function Q_CheckIfTrue(ByVal objCheckBox,ByVal blnCheck)
	On Error Resume Next
		'Variable Section Begin
		
		Dim strMethod 
		Dim  intActValue, blnstatus

		'Variable Section End
		strMethod = strKeywordForReporting
		blnstatus = False
		If isNull(blnCheck) or IsEmpty(blnCheck) or trim(blnCheck)= " "Then
			Q_CheckIfTrue = 1 
			WriteStatusToLogs  "The parameter is either null or empty, please verify."
		Exit Function
		End If		
        If CheckObjectExist(objCheckBox) Then
            If CheckJavaObjectEnabled(objCheckBox) Then
								
					intActValue = objCheckBox.GetROProperty("value")
					
					If Vartype(intActValue) <> vbEmpty  Then
						blnstatus = blnCheck
					End If
					
					If blnstatus  Then				  		
					objCheckBox.Set "ON"
						Q_CheckIfTrue = 0
						WriteStatusToLogs "CheckBox selected successfully."
					Else
						Q_CheckIfTrue = 0
						WriteStatusToLogs "Checkbox is not select, please verify."
					End If
            Else
				Q_CheckIfTrue = 1
				WriteStatusToLogs "The CheckBox was not enabled, please verify."
			End If
					
		Else
			Q_CheckIfTrue = 1
			WriteStatusToLogs "The CheckBox does not exist, please verify."
		End If
					
	 On Error GoTo 0
End Function

'=======================Code Section End=============================================