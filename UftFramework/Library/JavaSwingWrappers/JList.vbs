'#Functions List
''Function CompareJListBoxDefaultValue(ByVal objListBox , ByVal arrExpValue)
''Function SelectJListItem(ByVal objListBox , ByVal strSelectItem)
''Function VerifyAllJListItems(ByVal objListBox , ByVal arrExpItems)
''Function VerifyJListItemsInSequence(ByVal objListBox , ByVal arrExpItems)
''Function VerifyJListBoxDisabled(ByVal objListBox)
''Function VerifyNoDuplicationInJListBox(ByVal objListBox)
''Function SelectJListItemByIndex(objListBox, byval intIndex)
''Function VerifyJListBoxExist(ByVal objListBox)
''Function VerifyJListBoxNotExist(ByVal objListBox)
''Function VerifyJListBoxNOTContainsItem(ByVal objListBox , ByVal strListItem)
''Function VerifyJListBoxContainsItem	(ByVal objListBox , ByVal strListItem)
''Function VerifyJListBoxSize(ByVal objListBox , ByVal intExpectedSize)
''Function VerifyJListBoxEnabled(ByVal objListBox)
''Function VerifyJListBoxVisible(ByVal objListBox)
''Function VerifyJListBoxInVisible(ByVal objListBox)
''Function DeSelectJListBoxItem(ByVal objListBox , ByVal strListItem)
''Function VerifyJListItemsSelected(ByVal objListBox , ByVal strExpItems)

'=======================Code Section Begin=============================================

Option Explicit 'Forces explicit declaration of all variables in a script. 

'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name			:	CompareJListBoxDefaultValue()
' 	Purpose					:	Compares Default value of specified Listbox with our expected value
' 	Return Value			:	Integer( 0/2/1)
' 	Arguments				:
'		Input Arguments		:  	objListBox,arrExpValue
'		Output Arguments	:	strActValue 
'	Function is called by	:     
'	Function calls			:	CheckObjectExist(),MatchEachArrData()
'	Created on				:	06/07/2009
'	Author					:	Vidyasagar
'
'------------------------------------------------------------------------------------------------------------------------------------------

Function CompareJListBoxDefaultValue(ByVal objListBox , ByVal arrExpValue)
	On Error Resume Next
		'Variable Section Begin
		
			Dim strMethod
			Dim strActValue , arrActItem
			Dim cnt , intItem , i
			Dim strMsgExpArray
           
		'Variable Section End
		strMethod = strKeywordForReporting
		     If CheckObjectExist(objListBox) Then  

				 strActValue=objListBox.GetRoProperty("value")
					
					If not Isempty(strActValue) and Err.Number =0 Then

						arrActItem = GetArray(strActValue ,  "")				
						If IsArray(arrExpValue)  Then

										For i=0 to  Ubound(arrExpValue) 
											If  trim(arrExpValue(i)) = "" Then
												cnt = objListBox.GetROProperty("items count")
												For intItem=1 to cnt
													If trim(objListBox.getItem(intItem)) = "" then
														arrExpValue(i) = "#"& (intItem-1)
														Exit for
													End if
												Next									   
											End If
										Next

										strMsgExpArray = PrintArrayElements(arrExpValue)
										If MatchEachArrData(arrExpValue, arrActItem )= true Then   
											 CompareJListBoxDefaultValue = 0
											 WriteStatusToLogs "The ListBox item(s): '"&strActValue&"' match the expected default value '"&strMsgExpArray&"'"
										Else
											 CompareJListBoxDefaultValue = 2
											 WriteStatusToLogs "The ListBox item(s): '"&strActValue&"' does not match the expected default value '"&strMsgExpArray&"'"
										End If	

							Else
								CompareJListBoxDefaultValue=1
								WriteStatusToLogs "The expected value is not an array, please verify."
							End If

						 
					Else
						CompareJListBoxDefaultValue=1
						WriteStatusToLogs "Could not capture ListBox default value, please verify."
					End If

			 Else
				 CompareJListBoxDefaultValue = 1
				 WriteStatusToLogs "The ListBox does not exist, please verify."
            End If
			 
    On Error GoTo 0
End Function

'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name			:	SelectJListItem()
' 	Purpose					:	Selects a specified item from the ListBox
' 	Return Value			:	Integer(0/1)
' 	Arguments				:
'		Input Arguments		:  	objListBox , strSelectItem
'		Output Arguments	:   strActItem
'	Function is called by	:
'	Function calls			:	CheckObjectExist(),CheckObjectStatus()
'	Created on				:	03/07/2009
'	Author					:	Vidyasagar
' 
'------------------------------------------------------------------------------------------------------------------------------------------

Function SelectJListItem(ByVal objListBox , ByVal strSelectItem)
	On Error Resume Next
		'Variable Section Begin
		
			Dim strMethod
			Dim strActItem, strActItems, arrActItems, arrExpItems,arrItems()
			Dim flag, innerflag, cnt, intItem, newindex,intIndex,intItemsCount
			Dim strItemToBeSelect , intCnt
	
		'Variable Section End
		strMethod = strKeywordForReporting
			flag = True
			intIndex=0
			 strItemToBeSelect = trim(strSelectItem)
			If CheckObjectExist(objListBox) Then   
				If CheckJavaObjectStatus(objListBox) = True then
					intItemsCount = objListBox.GetROProperty("items count")
					If intItemsCount = 0 Then
						SelectJListItem = 1
						WriteStatusToLogs "The list is empty, please verify."
						Exit Function
					End If
					
					For intCnt=0 to intItemsCount-1
						strActItems = objListBox.GetItem(intCnt)
						ReDim Preserve arrItems(intIndex)
						arrItems(intIndex) = Trim(strActItems)
						intIndex=intIndex+1 
					Next  

						If Err.number = 0 then
						
							If Not IsEmpty(arrItems) Then
								 arrExpItems = GetArray(strSelectItem , ";")

								 if CompareArrays(arrExpItems,arrItems) = 0 Then
										flag = True
										cnt = objListBox.GetROProperty("items count")
										For  intItem=0 to cnt-1
											 If UCase(Trim(objListBox.getItem(intItem))) = UCase(Trim(strSelectItem)) Then
												newindex = intItem
												strSelectItem = "#"&newindex
												Exit For
											 End If
										Next
											
								  Else
									flag = False
								  End If
							End If
						End If
					If flag Then
					objListBox.Select strSelectItem
					End If
					
					If Err.number = 0 And flag = True Then					
							strActItem = objListBox.GetRoProperty("value")
							If Not IsEmpty(strActItem) Then
									 If strSelectItem = "" Then
										strItemToBeSelect = objListBox.GetRoProperty("value")
									 End If
									 If CompareStringValues(strItemToBeSelect, strActItem) = 0 Then
										SelectJListItem = 0
										WriteStatusToLogs "Given value is selected in the ListBox"
									 Else
											SelectJListItem = 1
											WriteStatusToLogs "The specific data could not be selected, please verify item exist."
									 End If
							Else
								SelectJListItem = 1
								WriteStatusToLogs "The ListBox item could not be selected, please verify."

							End If

					Else
							SelectJListItem=1
							WriteStatusToLogs "The List item '"&Trim(strSelectItem)&"' does not exist, please verify."
					End If
				Else
							SelectJListItem=1
							WriteStatusToLogs "The specified List is disabled, please verify."
				End if
			Else
				SelectJListItem = 1
				WriteStatusToLogs "The ListBox does not exist, please verify."
			End If
		On Error GoTo 0
End Function

'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name			:   VerifyAllJListItems()
' 	Purpose					:	Verifies  all the list items with our expected
' 	Return Value			:	Integer( 0/1/2)
' 	Arguments				:
'		Input Arguments		:  	objListBox,arrExpItems
'		Output Arguments	:   strActItems , arrActItems() , arrExpItems()
'	Function is called by	:
'	Function calls			:	CheckObjectExist() , GetArray() , MatchEachArrData()
'	Created on				:	06/07/2009
'	Author					:	Vidyasagar 
'
'------------------------------------------------------------------------------------------------------------------------------------------

Function VerifyAllJListItems(ByVal objListBox , ByVal arrExpItems)
	On Error Resume Next
		'Variable Section Begin
		
			Dim strMethod
			Dim strActItems , arrActItems, arrItems()
			Dim intItemsCount,intCnt,intIndex
				intIndex=0
			
			
		'Variable Section End
		strMethod = strKeywordForReporting
			If CheckObjectExist(objListBox)  Then
                intItemsCount = objListBox.GetROProperty("items count")
				For intCnt=0  to intItemsCount-1
					strActItems = objListBox.GetItem(intCnt)
					ReDim Preserve arrItems(intIndex)
					arrItems(intIndex) = Trim(strActItems)
					intIndex=intIndex+1 
				Next  				
				If Err.number = 0 then
					If Not IsEmpty(arrItems) Then
					   arrActItems = arrItems
							   If IsArray(arrExpItems) Then
										If MatchEachArrData(arrExpItems, arrActItems) = True Then
											VerifyAllJListItems = 0
											WriteStatusToLogs "The specified list items match with the expected list item."
										Else
											VerifyAllJListItems = 2
											WriteStatusToLogs "The specified list items does not match the expected list item."
										End If
							   Else
								   VerifyAllJListItems = 1
								   WriteStatusToLogs "The arrExpItems is Not an array, please verify."
							   End If					
					Else
						VerifyAllJListItems = 1
						WriteStatusToLogs "ListBox does NOT contain any item, please verify."
					End If

				Else
					VerifyAllJListItems = 1
					WriteStatusToLogs "Could not retrieve all the items of the ListBox"
				End if
			Else
			      VerifyAllJListItems = 1
				  WriteStatusToLogs "The ListBox does not exist, please verify."
			End If
			'WriteStepResults VerifyListItems
    On Error GoTo 0
End Function


'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name			:   VerifyJListItemsInSequence()
' 	Purpose					:	Compres the list box items with our expected items in sequence order.
' 	Return Value			:	Integer( 0/1/2)
' 	Arguments				:
'		Input Arguments		:  	objListBox , arrExpItems
'		Output Arguments	:   strItems , arrActItems() , arrExpecteditems()
'	Function is called by	:
'	Function calls			:	CheckObjectExist(),GetArray(),CompareArraysInSequence()
'	Created on				:	06/07/2009
'	Author					:	Vidyasagar
'
'------------------------------------------------------------------------------------------------------------------------------------------

Function VerifyJListItemsInSequence(ByVal objListBox , ByVal arrExpItems)
	On Error Resume Next
		'Variable Section Begin
		
			Dim strMethod
			Dim strActItems , arrActItems , arrItems()
			Dim intItemsCount,intCnt,intIndex
			intIndex=0

		'Variable Section End
		strMethod = strKeywordForReporting
	If CheckObjectExist(objListBox) Then   
			intItemsCount = objListBox.GetROProperty("items count")
			For intCnt=0 to intItemsCount-1
				strActItems = objListBox.GetItem(intCnt)
				ReDim Preserve arrItems(intIndex)
				arrItems(intIndex) = Trim(strActItems)
				intIndex=intIndex+1 
			Next
			
			If Not IsEmpty(arrItems) Then
				arrActItems = arrItems
					If IsArray(arrExpItems) Then
							If CompareArraysInSequence(arrExpItems , arrActItems) = 0 Then
								VerifyJListItemsInSequence = 0
								WriteStatusToLogs "The ListBox items sequence matches with the expected list sequence."
							Else
								VerifyJListItemsInSequence = 2
								WriteStatusToLogs "The ListBox items are NOT in sequence with the expected list."
							End If
					
					Else
						VerifyJListItemsInSequence = 1
						WriteStatusToLogs "The arrExpItems is not an array, please verify."
					End If

			Else
				 VerifyJListItemsInSequence = 1
				 WriteStatusToLogs "The ListBox does NOT contain any item, please verify."
			End If
			
	Else
		VerifyJListItemsInSequence = 1
		WriteStatusToLogs "The ListBox does not exist, please verify."
	End If
		'WriteStepResults VerifyListItemsInSequence
 On Error GoTo 0
End Function

'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name			:	VerifyJListBoxDisabled()
' 	Purpose					:	Verifies whether the ListBox is Disabled.
'	Return Value			:	Integer( 0/1/2)
' 	Arguments				:
'		Input Arguments		:  	objListBox 
'		Output Arguments	: 
'	Function is called by	:
'	Function calls			:	CheckObjectExist() , CheckObjectStatus()
'	Created on				:	06/07/2009
'	Author					:	Vidyasagar
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function VerifyJListBoxDisabled(ByVal objListBox)

On Error Resume Next
	'Variable Section Begin
		
		Dim strMethod 

		'Variable Section End
		strMethod = strKeywordForReporting
		If CheckObjectExist(objListBox) Then
			If Not(CheckJavaObjectStatus(objListBox)) Then
					VerifyJListBoxDisabled = 0
					WriteStatusToLogs "The Listbox is disabled."
			Else
					VerifyJListBoxDisabled = 2
					WriteStatusToLogs "The Listbox is not disabled, please verify."
			End If
            
		Else
				VerifyJListBoxDisabled = 1
				WriteStatusToLogs "The ListBox does not exist, please verify."
		End if
		
	On Error GoTo 0
    
End Function

'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name			: VerifyNoDuplicationInJListBox()
' 	Purpose					: Verifies the duplicate items in the listbox
' 	Return Value			: Integer( 0/1/2)
' 	Arguments				:
'		Input Arguments		: objListBox
'		Output Arguments	: strItems , arrActItems() , strActItems
'	Function is called by	: 
'	Function calls			: CheckObjectExist,GetArray(),VerifyDuplicateElemetsInArray()
'	Created on				: 06/07/2009
'	Author					: Vidyasagar
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function VerifyNoDuplicationInJListBox(ByVal objListBox)
	On Error Resume Next
		'Variable Section Begin
			Dim strMethod
			Dim strActItems , arrActItems ,arrItems()
			Dim intItemsCount,intCnt,intIndex
			intIndex=0
			
         'Variable Section End
		 strMethod = strKeywordForReporting
			If CheckObjectExist(objListBox)  Then   
				intItemsCount = objListBox.GetROProperty("items count")

				For intCnt=0 to intItemsCount-1
					strActItems = objListBox.GetItem(intCnt)
					ReDim Preserve arrItems(intIndex)
					arrItems(intIndex) = Trim(strActItems)
					intIndex=intIndex+1 
				Next
                
					If Not IsEmpty(arrItems) Then
						arrActItems = arrItems
						If Not IsNull(arrActItems) Then
					
								If VerifyDuplicateElementsInArray(arrActItems) = 0 Then
									VerifyNoDuplicationInJListBox = 0
									WriteStatusToLogs "ListBox has no duplicate item"
								Else
									VerifyNoDuplicationInJListBox = 2
									WriteStatusToLogs "ListBox contains duplicate item(s), please verify."
								End If

						 Else
								VerifyNoDuplicationInJListBox = 1
								WriteStatusToLogs "Action failed because the listItem array received is NULL."
						 End If
					
					Else
						VerifyNoDuplicationInJListBox = 1
						WriteStatusToLogs "Action fails to get ListBox items, please verify."
					
					End if
			Else
				VerifyNoDuplicationInJListBox = 1
				WriteStatusToLogs "ListBox does not exist, please verify."
            End If
			'WriteStepResults VerifyNoDuplicationInListBox 
	On Error GoTo 0
End Function

'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name			:	SelectJListItemByIndex()
' 	Purpose					:	To Select the List box item by index
' 	Return Value			:	Integer(0/1)
' 	Arguments				:
'		Input Arguments		:  	intIndex
'		Output Arguments	: 	
'	Function is called by	: 
'	Function calls			:	CheckObjectExist(),CheckObjectStatus()
'	Created on				:	06/07/2009
'	Author					:	Vidyasagar 
'
'------------------------------------------------------------------------------------------------------------------------------------------

Function SelectJListItemByIndex(objListBox, byval intIndex)
	
	On Error Resume Next
		'Variable Section Begin
		
			Dim strMethod
			Dim   intGetIndex, intItemCount 
			
         'Variable Section End
		 strMethod = strKeywordForReporting
	If CheckObjectExist(objListBox)  Then  
		If CheckJavaObjectStatus(objListBox) = True then
				If IsNumeric(intIndex) then
						intGetIndex = CInt(intIndex)
						'objListBox.Select Cstr("#"&intGetIndex)
						If intGetIndex >=0 Then
								intItemCount = CInt(objListBox.GetROProperty("items count"))
								If intGetIndex < intItemCount Then
										objListBox.Select "#"&(intGetIndex-1)
										If Err.Number = 0 Then
											SelectJListItemByIndex= 0
											WriteStatusToLogs "The item with index "& intGetIndex&" is selected"
										Else
											SelectJListItemByIndex= 1
											WriteStatusToLogs "Error occured while selecting list item with index :' "&intGetIndex&" ' , please verify."
										End If
								Else
										SelectJListItemByIndex= 1
										WriteStatusToLogs "The Index Number provided is out of range in the specified ListBox. There are only "&intItemCount&" items listed , please verify."
											
								End If
						Else
								SelectJListItemByIndex= 1
								WriteStatusToLogs "The index ' "& intGetIndex&" ' is not a valid index. Please verify. Index starts from 1."
						End If
				Else
					SelectJListItemByIndex= 1
					WriteStatusToLogs "The index ' "& intIndex&" ' is not a numeric. Please verify"
				End If
		Else
			SelectJListItemByIndex = 1
			WriteStatusToLogs "The specied Listbox is disabled , please verify"
		End if
	Else
	    SelectJListItemByIndex = 1
		WriteStatusToLogs "The Listbox does not exist"
	End if
   On Error GOTO 0
End Function


'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name			: VerifyJListBoxExist()
'	Purpose					: Verifies whether the ListBox Exists or not?
'	Return Value			: Integer( 0/2)
'	Arguments				:
'		Input Arguments		: objListBox 
'		Output Arguments	: 
'	Function is called by	:
'	Function calls			: CheckObjectExist() 
'	Created on				: 08/07/2009
'	Author					: Vidyasagar
'
'------------------------------------------------------------------------------------------------------------------------------------------

Function VerifyJListBoxExist(ByVal objListBox)
	On Error Resume Next
		'Variable Section Begin
	
		   Dim strMethod 

		'Variable Section End
		strMethod = strKeywordForReporting
			If CheckObjectExist(objListBox) Then 
			   
					VerifyJListBoxExist = 0
					WriteStatusToLogs "The ListBox Exist."
					
			Else
					VerifyJListBoxExist = 2
					WriteStatusToLogs "The ListBox does not exist, please verify."
					
			End If 
	  
	
	On Error GoTo 0
End Function

'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name			:	VerifyJListBoxNotExist()
'	Purpose					:	Verifies whether the ListBox Exists or not?
'	Return Value			:	Integer( 0/2)
'	Arguments				:
'		Input Arguments		:  	objListBox 
'		Output Arguments	: 
'	Function is called by	:
'	Function calls			:	CheckObjectExist() 
'	Created on				:	08/07/2009
'	Author					:	Vidyasagar
'
'------------------------------------------------------------------------------------------------------------------------------------------

Function VerifyJListBoxNotExist(ByVal objListBox)
	On Error Resume Next
		'Variable Section Begin
	
		  Dim strMethod 

		'Variable Section End
		strMethod = strKeywordForReporting
		
			If CheckObjectNotExist(objListBox) Then 
			   
					VerifyJListBoxNotExist = 0
					WriteStatusToLogs "The ListBox does not Exist."
					
			Else
					VerifyJListBoxNotExist = 2
					WriteStatusToLogs "The ListBox Exists, please verify."
					
			End If 
	
  		
	On Error GoTo 0
End Function

'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name			:	VerifyJListBoxNOTContainsItem()
' 	Purpose					:	Verifies  whether the ListBox doesn't contain the specified Item.
' 	Return Value			:	Integer( 0/1/2)
' 	Arguments				:
'		Input Arguments		:  	objListBox,strExpItems
'		Output Arguments	:   sstrActItems , arrActItems , arrExpItems,intCount,strExpItem,intActItem
'	Function is called by	:
'	Function calls			:	CheckObjectExist() , GetArray() 
'	Created on				:	06/07/2009
'	Author					:	Vidyasagar
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function VerifyJListBoxNOTContainsItem(ByVal objListBox , ByVal strListItem)
	On Error Resume Next
		'Variable Section Begin
			Dim strMethod
			Dim strActItems , arrActItems , arrExpItems,strExpItem,intActItem, blnEqualFlag, arrItems()
			Dim intItemsCount,intIndex,intCnt
			intIndex=0
			blnEqualFlag=true
						
		'Variable Section End
		strMethod = strKeywordForReporting
			If CheckObjectExist(objListBox)  Then   
				intItemsCount = objListBox.GetROProperty("items count")
				For intCnt=0 to intItemsCount-1
					strActItems = objListBox.GetItem(intCnt)
					ReDim Preserve arrItems(intIndex)
					arrItems(intIndex) = Trim(strActItems)
					intIndex=intIndex+1 
				Next
				
                If Err.Number = 0 Then						
											
								arrActItems = arrItems
							   If Not IsNull (arrActItems) Then								 
									 For intActItem = 0 to Ubound(arrActItems)
										   If strListItem = arrActItems(intActItem) Then
												blnEqualFlag=false
												Exit for
										   End If
									 Next
										 
									 If blnEqualFlag = true  Then
										  VerifyJListBoxNOTContainsItem = 0
										  WriteStatusToLogs "List Does not contain the specified item."
		  
									 Else
										 VerifyJListBoxNOTContainsItem = 2
										 WriteStatusToLogs "List contain the specified item, please verify."
									 End If										   
							Else
								 VerifyJListBoxNOTContainsItem = 1
								 WriteStatusToLogs "An error occurred , Action fails to get Actual items , please verify."
						 End If
						
				Else
					VerifyJListBoxNOTContainsItem = 1
					WriteStatusToLogs "Action Fails to get Items from ListBox, please verify ListBox method and property"
				End If
			
	 Else
		VerifyJListBoxNOTContainsItem = 1
		WriteStatusToLogs "ListBox doesn't exist, please verify."

	 End If
			'WriteStepResults VerifyListBoxNOTContainsItem
    On Error GoTo 0
End Function


'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name			:	VerifyJListBoxContainsItem()
' 	Purpose					:	Verifies  whether the listBox contains the specified Item.
' 	Return Value			:	Integer( 0/1/2)
' 	Arguments				:
'		Input Arguments		:  	objListBox,strExpItems
'		Output Arguments	:   sstrActItems , arrActItems , arrExpItems,intCount,strExpItem,intActItem
'	Function is called by	:
'	Function calls			:	CheckObjectExist() , GetArray() 
'	Created on				:	06/07/2009
'	Author					:	Vidyasagar
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function VerifyJListBoxContainsItem	(ByVal objListBox , ByVal strListItem)
	On Error Resume Next
		'Variable Section Begin
			Dim strMethod
			Dim strActItems , arrActItems , arrExpItems,intCount,strExpItem,intActItem , blnpresentFlag, arrItems()
			Dim intItemsCount,intCnt,intIndex
			intIndex=0
			blnpresentFlag=false			
			
		'Variable Section End
		strMethod = strKeywordForReporting
	If CheckObjectExist(objListBox)  Then   
		intItemsCount = objListBox.GetROProperty("items count")
			For intCnt=0 to intItemsCount-1
				strActItems = objListBox.GetItem(intCnt)
				ReDim Preserve arrItems(intIndex)
				arrItems(intIndex) = Trim(strActItems)
				intIndex=intIndex+1 
			Next
						
				If Err.Number = 0 Then	
					   arrActItems = arrItems
							If Not IsNull (arrActItems) Then			 
								  For intActItem = 0 to Ubound(arrActItems)
										   If strListItem = arrActItems(intActItem) Then
												blnpresentFlag=True
												Exit for
										   End If
								  Next
								  If blnpresentFlag = true  Then
									  VerifyJListBoxContainsItem	 = 0
									  WriteStatusToLogs "ListBox Contains  the expected item."
								  Else
										 VerifyJListBoxContainsItem	 = 2
										 WriteStatusToLogs "List does not contain the expected item, please verify."
								  End If												   
							Else
								 VerifyJListBoxContainsItem	 = 1
								 WriteStatusToLogs "Action fails to get Actual items into an array please verify."
							End If
				Else
					VerifyJListBoxContainsItem	 = 1
					WriteStatusToLogs "Action Fails to get Items from ListBox, please verify ListBox method and property"
				End If
	 Else
		 VerifyJListBoxContainsItem = 1
		 WriteStatusToLogs "ListBox doesn't exist, please verify."
	 End If
			'WriteStepResults VerifyListBoxContainsItem	
    On Error GoTo 0
End Function

'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name			:	VerifyJListBoxSize()
' 	Purpose					:	Verifies the size(no.of items) of ListBox
' 	Return Value			:	Integer( 0/1/2)
' 	Arguments				:
'		Input Arguments		:  	objListBox,intItemCount
'		Output Arguments	:   intActCount,intCount
'	Function is called by	:
'	Function calls			:	CheckObjectExist() , GetArray() 
'	Created on				:	08/07/2009
'	Author					:	Vidyasagar
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function VerifyJListBoxSize(ByVal objListBox , ByVal intExpectedSize)
	On Error Resume Next
		'Variable Section Begin
			Dim strMethod
			Dim intActCount,intCount
			
		'Variable Section End
		strMethod = strKeywordForReporting
			If CheckObjectExist(objListBox)  Then   
				intCount=Cint(intExpectedSize)
				
				If VarType(intCount) = VbInteger Then				
					intActCount = objListBox.GetROProperty("items count")

					If Not IsEmpty(intActCount)  Then
							If CompareStringValues(intCount,intActCount)=0 Then
								  VerifyJListBoxSize = 0
								  WriteStatusToLogs "The specified ListBox Size is same as the expected size."
							Else
								   VerifyJListBoxSize = 2
									WriteStatusToLogs "The specified ListBox Size is  different from the expected Size, please verify."
							End If
					Else
					  VerifyJListBoxSize = 1
						WriteStatusToLogs "Action Fails to get ListBox Items Count, please verify ListBox and its method and property"
					End If
				Else
				   VerifyJListBoxSize = 1
					 WriteStatusToLogs "The specified List box's expected size is not an Integer, please verify."
				End If
			Else
				 VerifyJListBoxSize = 1
				 WriteStatusToLogs "The specified ListBox doesn't exist, please verify."

			End If
			'WriteStepResults VerifyListBoxNOTContainsItem
    On Error GoTo 0
End Function


'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name			:  VerifyJListBoxEnabled()
' 	Purpose					:  Verifies whether the ListBox is Enabled
' 	Return Value			:  Integer( 0/1/2)
' 	Arguments				:
'		Input Arguments		:  objListBox 
'		Output Arguments	: 
'	Function is called by	:
'	Function calls			:  CheckObjectExist() , CheckObjectStatus()
'	Created on				:  06/07/2009
'	Author					:  Vidyasagar
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function VerifyJListBoxEnabled(ByVal objListBox)

On Error Resume Next
	'Variable Section Begin
		
		Dim strMethod 

		'Variable Section End
		strMethod = strKeywordForReporting
	If CheckObjectExist(objListBox) Then
		If CheckJavaObjectEnabled(objListBox) Then
			VerifyJListBoxEnabled = 0
			WriteStatusToLogs "The ListBox was enabled."
		Else
			VerifyJListBoxEnabled = 2
			WriteStatusToLogs "The ListBox was not enabled, please verify."
		End If
		
	Else
		VerifyJListBoxEnabled = 1
		WriteStatusToLogs "The ListBox does not exist, please verify."
	End if
		
	On Error GoTo 0
    
End Function

'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name			:	VerifyJListBoxVisible()
'	Purpose					:	Verifies whether the ListBox is visible.
'	Return Value			:	Integer( 0/1/2)
'	Arguments				:
'		Input Arguments		:  	objListBox 
'		Output Arguments	: 
'	Function is called by	:
'	Function calls			:	CheckObjectExist() , CheckObjectVisiblity()
'	Created on				:	06/07/2009
'	Author					:	Vidyasagar
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function VerifyJListBoxVisible(ByVal objListBox)
	On Error Resume Next
		'Variable Section Begin
		
			Dim strMethod 

		'Variable Section End
		strMethod = strKeywordForReporting
			If CheckObjectExist(objListBox)  Then 
				If CheckJavaObjectVisible(objListBox) Then
					VerifyJListBoxVisible = 0
					WriteStatusToLogs "The ListBox was Visible."
				Else
					VerifyJListBoxVisible = 2
					WriteStatusToLogs "The ListBox was not Visible, please verify."
				End If
			Else
				VerifyJListBoxVisible = 1
				WriteStatusToLogs "The ListBox does not exist, please verify."
			End If
		
	On Error GoTo 0
End Function

'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name			:  VerifyJListBoxInVisible()
'	Purpose					:  Verifies whether the ListBox is Invisible.
'	Return Value			:  Integer( 0/1/2)
'	Arguments				:
'		Input Arguments		:  objListBox 
'		Output Arguments	: 
'	Function is called by	:
'	Function calls			:  CheckObjectExist() , CheckObjectVisiblity()
'	Created on				:  06/07/2009
'	Author					:  Vidyasagar
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function VerifyJListBoxInVisible(ByVal objListBox)
	On Error Resume Next
		'Variable Section Begin
		
			Dim strMethod 

		'Variable Section End
		strMethod = strKeywordForReporting
			If CheckObjectExist(objListBox)  Then 
				If CheckJavaObjectInVisible(objListBox) Then
					VerifyJListBoxInVisible = 0
					WriteStatusToLogs "The ListBox was InVisible."
				Else
					VerifyJListBoxInVisible = 2
					WriteStatusToLogs "The ListBox was not InVisible, please verify."
				End If
			Else
				VerifyJListBoxInVisible = 1
				WriteStatusToLogs "The ListBox does not exist, please verify."
			End If
		
	On Error GoTo 0
End Function

'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name			:  DeSelectJListBoxItem()
'	Purpose					:  Clears the selection of an item from a list.
'	Return Value			:  Integer( 0/1/2)
'	Arguments				:
'		Input Arguments		:  objListBox , strDeSelectItem
'		Output Arguments	: 
'	Function is called by	:
'	Function calls			:  CheckObjectExist() , CheckObjectVisiblity()
'	Created on				:  15/08/2009
'	Author					:  Ashok Jakkani
'
'------------------------------------------------------------------------------------------------------------------------------------------

Function DeSelectJListBoxItem(ByVal objListBox , ByVal strListItem)
	On Error Resume Next
		'Variable Section Begin
		
			Dim strMethod
			Dim strSelectedItem,arrExp(0), arrAct
		
	
		'Variable Section End
		strMethod = strKeywordForReporting
			If CheckObjectExist(objListBox) Then   
				If CheckObjectStatus(objListBox) = True then          
						strSelectedItem = Trim(objListBox.GetRoProperty("value"))
						If Not IsEmpty(strSelectedItem) Then
								arrAct=GetArray(strSelectedItem,"")
								arrExp(0)=strListItem
								If Not IsNull(arrExp) And Not IsNull(arrAct) Then
										If CompareArrays(arrExp,arrAct) = 0 Then 
												objListBox.Deselect Trim(strListItem)
												strSelectedItem = Trim(objListBox.GetRoProperty("value"))
												If strSelectedItem="" Then
														DeSelectJListBoxItem = 0
														WriteStatusToLogs "The "&strListItem&" List Item  is deselected  successfully"
												Else
														arrAct=GetArray(strSelectedItem,"")
														If  CompareArrays(arrExp,arrAct) <> 0 Then
														  
																DeSelectJListBoxItem = 0
																WriteStatusToLogs "The "&strListItem&" List Item  is deselected  successfully"
														Else
																DeSelectJListBoxItem = 2
																WriteStatusToLogs "List item is not deselected successfully, please verify."
														End If

												End IF

										Else
												 DeSelectJListBoxItem = 2
												 WriteStatusToLogs "Either the expected item is not in selection mode OR the item does not exist in the list item, please verify."

										End If
								Else
										DeSelectJListBoxItem = 1
										WriteStatusToLogs "An error occurred, action fails to get the Array of the expected and actual selected item , please verify."
								End IF
							
						Else
								DeSelectJListBoxItem = 1
								WriteStatusToLogs "Either the Action fails to get the selected list item OR no item is selected in the list, please verify."
						End If
				 Else
			   
						DeSelectJListBoxItem = 1
					    WriteStatusToLogs "The specified ListBox is disabled, please verify."
				End If
			 Else
			   
						DeSelectJListBoxItem = 1
					    WriteStatusToLogs "ListBox does not exist, please verify."
			 End If
	On Error GoTo 0
End Function


'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name :			VerifyJListItemsSelected()
' 	Purpose :					 Verifies  Selected items are matching with items expected 
' 	Return Value :		 		   Integer( 0/1/2)
' 	Arguments :
'		Input Arguments :  	  	objListBox,strExpItems
'		Output Arguments :     strActItems , arrActItems() , arrExpItems()
'	Function is called by :
'	Function calls :			   CheckObjectExist() , GetArray() , MatchEachArrData()
'	Created on :				  19/03/2008
'	Author :						Rathna Reddy 
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function VerifyJListItemsSelected(ByVal objListBox , ByVal strExpItems)
	On Error Resume Next
		'Variable Section Begin
		
			Dim strMethod
			Dim strActItems , arrActItems , arrExpItems
			
		'Variable Section End
		strMethod = strKeywordForReporting
			If CheckObjectExist(objListBox)  Then   
				strActItems = objListBox.GetROProperty("value")
				If Err.number = 0 then
				
					If Not IsEmpty(strActItems) Then
					   arrActItems = GetArray(strActItems , "")
					   If IsArray(strExpItems) then
							arrExpItems = strExpItems
						ElseIf Not IsObject(strExpItems) then
							arrExpItems = GetArray(strExpItems , "^")
						End If
					
						If MatchEachArrData(arrExpItems, arrActItems) = True Then
							VerifyJListItemsSelected = 0
							WriteStatusToLogs "The specified list items match with the expected list item."
						Else
							VerifyJListItemsSelected = 2
							WriteStatusToLogs "The specified list items does not match the expected list item."
						End IF
					Else
						VerifyJListItemsSelected = 1
						WriteStatusToLogs "ListBox does NOT contain any item, please verify."
					End If

				Else
					VerifyJListItemsSelected = 1
					WriteStatusToLogs "Could not retrieve all the items of the ListBox"
				End if
			Else
			      VerifyJListItemsSelected = 1
				  WriteStatusToLogs "The ListBox does not exist, please verify."
			End If
			'WriteStepResults VerifyListItems
    On Error GoTo 0
End Function

'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name			:	SelectJListItem()
' 	Purpose					:	Selects a specified item from the ListBox
' 	Return Value			:	Integer(0/1)
' 	Arguments				:
'		Input Arguments		:  	objListBox , strSelectItem
'		Output Arguments	:   strActItem
'	Function is called by	:
'	Function calls			:	CheckObjectExist(),CheckObjectStatus()
'	Created on				:	03/07/2009
'	Author					:	Vidyasagar
' 
'------------------------------------------------------------------------------------------------------------------------------------------

'Function SelectJListItem(ByVal objListBox , ByVal strSelectItem)
'	On Error Resume Next
'		'Variable Section Begin
'		
'			Dim strActItem, strActItems, arrActItems, arrExpItems,arrItems()
'			Dim flag, innerflag, cnt, intItem, newindex,intIndex,intItemsCount
'			Dim strItemToBeSelect , intCnt '[iohbha:declared the variable intCnt]
'	
'		'Variable Section End
'			flag = True
'			intIndex=0
'			 strItemToBeSelect = trim(strSelectItem)
'			If CheckObjectExist(objListBox) Then   
'				If CheckJavaObjectStatus(objListBox) = True then
'					intItemsCount = objListBox.GetROProperty("items count")
'					
'					For intCnt=0 to intItemsCount-1
'						strActItems = objListBox.GetItem(intCnt)
'						ReDim Preserve arrItems(intIndex)
'						arrItems(intIndex) = Trim(strActItems)
'						intIndex=intIndex+1 
'					Next  
'					
'						If Err.number = 0 then
'						
'							If Not IsEmpty(arrItems) Then
'								   arrExpItems = GetArray(strSelectItem , ";")
'
'								  if CompareArrays(arrExpItems,arrItems) = 0 Then
'										flag = True
'										cnt = objListBox.GetROProperty("items count")
'										
'										For  intItem=0 to cnt-1
'											 If UCase(Trim(objListBox.getItem(intItem))) = UCase(Trim(strSelectItem)) Then
'												newindex = intItem
'												strSelectItem = "#"&newindex
'												Exit For
'											 End If
'										Next
'										
'								  Else
'									flag = False
'								  End If
'							End If
'						End If
'					If flag Then
'					objListBox.Select strSelectItem
'					End If
'
'					If Err.number = 0 And flag = True Then					
'							strActItem = objListBox.GetRoProperty("value")
'							If Not IsEmpty(strActItem) Then
'									 If strSelectItem = "" Then
'										strItemToBeSelect = objListBox.GetRoProperty("value")
'									 End If
'									 If CompareStringValues(strItemToBeSelect, strActItem) = 0 Then
'										SelectJListItem = 0
'										WriteStatusToLogs "Action: SelectJListItem"&Vbtab&_
'													"Status: Passed"&Vbtab&_
'													"Message: Given value is selected in the ListBox"
'									 Else
'											SelectJListItem = 1
'											WriteStatusToLogs "Action: SelectJListItem"&Vbtab&_
'													"Status: Failed"&Vbtab&_
'													"Message: The specific data could not be selected, please verify item exist."
'									 End If
'							Else
'								SelectJListItem = 1
'								WriteStatusToLogs "Action: SelectJListItem"&Vbtab&_
'												"Status: Failed"&Vbtab&_
'												"Message: The ListBox item could not be selected, please verify."
'
'							End If
'
'					Else
'							SelectJListItem=1
'							WriteStatusToLogs "Action: SelectJListItem"&Vbtab&_
'									"Status: Failed"&Vbtab&_
'									"Message: The List item '"&Trim(strSelectItem)&"' does not exist, please verify."
'					End If
'				Else
'							SelectJListItem=1
'							WriteStatusToLogs "Action: SelectJListItem"&Vbtab&_
'									"Status: Failed"&Vbtab&_
'									"Message: The specified List is disabled, please verify."
'				End if
'			Else
'				SelectJListItem = 1
'				WriteStatusToLogs "Action: SelectJListItem"&Vbtab&_
'								"Status: Failed"&Vbtab&_
'								"Message: The ListBox does not exist, please verify."
'			End If
'		On Error GoTo 0
'End Function

'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name			:	StoreSelectedItems()
' 	Purpose					:	Store selected value from Jlist in key
' 	Return Value			:	Integer(0/1)
' 	Arguments				:
'		Input Arguments		:  	objListBox , strKey
'		Output Arguments	:   
'	Function is called by	:
'	Function calls			:	CheckObjectExist(),CheckObjectStatus()
'	Created on				:	29/06/2016
'	Author					:	Qualitia
' 
'------------------------------------------------------------------------------------------------------------------------------------------
Function Q_StoreSelectedItems(ByVal objListBox,ByVal strKey)

	On Error Resume Next
		'Variable Section Begin
		Dim strMethod
		Dim strActValue
		'Variable Section End
		strMethod = strKeywordForReporting
		
		If isNull(strKey) or IsEmpty(strKey) or trim(strKey)= " "Then
			Q_StoreSelectedItems = 1 
			WriteStatusToLogs  "The parameter is either null or empty, please verify."
		Exit Function	
		End If
		
			If CheckObjectExist(objListBox) Then
			 	If CheckJavaObjectStatus(objListBox) = True then
			 	
			 	 		strActValue = objListBox.GetROProperty("value")
			 	 		AddItemToStorageDictionary strKey,strActValue
			 	 		
			 			If err.number=0 Then
			 			Q_StoreSelectedItems=0
						WriteStatusToLogs "The selected '"&strActValue&"' is stored in '"&strKey&"' successfully"
						Else
						Q_StoreSelectedItems=1					
						WriteStatusToLogs "The selected '"&strActValue&"' is not able to  stored in '"&strKey&"', please verify."
						            
						End if
			 	Else    
						 	Q_StoreSelectedItems=1
							WriteStatusToLogs "The specified List is disabled, please verify."
									
				End if
               Else
				Q_StoreSelectedItems = 1
				WriteStatusToLogs "The ListBox does not exist, please verify."
								
			End If
									
		On Error GoTo 0
End Function



Function Q_StoreItems(ByVal objListBox,ByVal strKey)
	On Error Resume Next
		'Variable Section Begin
		
			Dim strMethod,intCnt,arrActItems()
			Dim intItemsCount,strActItems
		'Variable Section End
		strMethod = strKeywordForReporting
		
		If isNull(strKey) or IsEmpty(strKey) or trim(strKey)= " "Then
			Q_StoreItems = 1 
			WriteStatusToLogs  "The parameter is either null or empty, please verify."
		Exit Function	
		End If
		
	If CheckObjectExist(objListBox) Then 
		If CheckJavaObjectStatus(objListBox) = True then
					
			intItemsCount=objListBox.GetRoProperty("items count")
					
				If Not IsEmpty(intItemsCount) Then
				
							ReDim arrActItems(intItemsCount - 1)
							For intCnt=0 to intItemsCount-1
							arrActItems(intCnt) = objListBox.GetItem(intCnt)
							Next
					
					If Not IsEmpty(strActItem) Then
					
							strActItems=join(arrActItems,";")
							AddItemToStorageDictionary strKey,strActItems
							
								If err.number=0 Then								
									Q_StoreItems=0
									WriteStatusToLogs "The ListItems '"&strActItems&"' is stored in '"&strKey&"' successfully"
								Else
									Q_StoreItems=1					
									WriteStatusToLogs "The ListItems '"&strActItems&"' is not able to  stored in '"&strKey&"', please verify."
						            
						        End if							
					Else    
						Q_StoreItems=1
						WriteStatusToLogs "The specified JavaList is empty, please verify."
									
					End if

				Else    
					Q_StoreItems=1
					WriteStatusToLogs "Could not able to get value from list, please verify."
									
				End if	
		Else    
			Q_StoreItems=1
			WriteStatusToLogs "The specified List is disabled, please verify."
									
		End if	
	Else
		Q_StoreItems = 1
		WriteStatusToLogs "The ListBox does not exist, please verify."
    End If			
		
  On Error GoTo 0
End Function		

'=======================Code Section End=============================================
