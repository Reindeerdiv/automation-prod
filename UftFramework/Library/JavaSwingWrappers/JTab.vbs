''Function SelectJTab(ByVal ObjTab, Byval strTabItem)
''Function CloseJTab(ByVal ObjTab, Byval strTabItem)
''Function VerifyJTabItemSelected(ByVal ObjTab, Byval strTabItem)
''Function VerifyNumberOfJTabItems(ByVal ObjTab, Byval intExpectedSize)



'=======================Code Section Begin=============================================

Option Explicit 

'
'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name :				  SelectJTab()
' 	Purpose :						 Selects the specified tab panel
' 	Return Value :		 		   Integer( 0/1)
' 	Arguments :
'		Input Arguments :  		ObjTab
'		Output Arguments :     
'	Function is called by :
'	Function calls :				CheckObjectExist()
'	Created on :				   18/06/2009
'	Author :						 Vaibhav Kelkar
' 
'------------------------------------------------------------------------------------------------------------------------------------------
Function SelectJTab(ByVal ObjTab, Byval strTabItem)
  	On Error Resume Next
		'Variable Section Begin
			Dim strMethod
			Dim strItemToBeSelect, intItemscount, intCnt, intIndex, strTabSelected 
			Dim arrItems
			Dim blnStatus
			Dim strActItems
		'Variable Section End
		strMethod = strKeywordForReporting
			blnStatus = True

			 strItemToBeSelect = trim(strTabItem)
			If CheckObjectExist(ObjTab) Then
				 intItemscount = ObjTab.GetROProperty("items count")
				 If intItemscount > 0 Then
					strTabSelected = trim (ObjTab.GetROProperty("value"))
					
					If blnStatus  Then
					If strTabSelected <> strItemToBeSelect Then
                    ObjTab.Select strItemToBeSelect
							If Err.number = 0  Then	
							strTabSelected = trim (ObjTab.GetROProperty("value"))
									If  strTabSelected  = strItemToBeSelect Then
									SelectJTab = 0
										WriteStatusToLogs "The given tab is selected."
									Else
											SelectJTab = 1
											WriteStatusToLogs "The error, "& err.description
									End If
                            Else
								SelectJTab=1
								WriteStatusToLogs "The Tab item '"& strItemToBeSelect &"' not selected successfully, please verify."
							End If
						ElseIf UCase(strTabSelected) = UCase(strItemToBeSelect) Then                      ' strTabSelected <> strItemToBeSelect
							SelectJTab = 0
							WriteStatusToLogs	"The Java Tab was already selected."
							End if		
                    Else
					If strItemToBeSelect = "" or IsEmpty (strItemToBeSelect) or IsNull(strItemToBeSelect)  Then
								strItemToBeSelect = "Empty"
					End If
						SelectJTab=1
						WriteStatusToLogs "The Tab item '"& strItemToBeSelect &"' does not exist, please verify."
					End If
					Else
					 SelectJTab=1
						WriteStatusToLogs "There were no tab items exiting in Tab object."
					End If
			Else															
				SelectJTab = 1
				WriteStatusToLogs "The Tab does not exist, please verify."
			End If
	On Error GoTo 0
End Function
'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name			: CloseJTab()
'	Purpose					: Closes the specified tab (for tab controls that support closeable tabs).
'	Return Value			: Integer( 0/2)
'	Arguments				:
'		Input Arguments		: ObjTab 
'		Output Arguments	: 
'	Function is called by	:
'	Function calls			: CheckObjectExist() 
'	Created on				: 08/07/2009
'	Author					: Vidyasagar
'ObjTab.CloseJTab strItemToBeClose
'------------------------------------------------------------------------------------------------------------------------------------------
Function CloseJTab(ByVal ObjTab, Byval strTabItem)
 On Error Resume Next
		'Variable Section Begin
			Dim strMethod
			Dim strItemToBeClose, intItemscount, intCnt, intIndex, strTabSelected 
			Dim arrItems , strActItems
			Dim blnStatus
		'Variable Section End
		strMethod = strKeywordForReporting
			blnStatus = True

			 strItemToBeClose = trim(strTabItem)
			If CheckObjectExist(ObjTab) Then
				 intItemscount = ObjTab.GetROProperty("items count")
				 If intItemscount > 0 Then
  
							If blnStatus  Then
							ObjTab.CloseTab strTabItem
							
									If Err.number = 0  Then	
										CloseJTab = 0
										WriteStatusToLogs "The given tab is closed."
									Else
									CloseJTab = 1
									WriteStatusToLogs "The Error occured, "& err.number
									End If
							Else
								CloseJTab=1
								WriteStatusToLogs "The Tab item '"& strItemToBeClose &"' does not exist, please verify."
							End If
					Else
					 CloseJTab=1
						WriteStatusToLogs "There were no tab items exiting in Tab object."
					End If
			Else 					'CheckObjectExist
				CloseJTab = 1
				WriteStatusToLogs "The Tab does not exist, please verify."
			End If
		On Error GoTo 0
End Function
'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name			: VerifyJTabItemSelected
'	Purpose					: Verify  Specified Tab item selected or not?
'	Return Value			: Integer( 0/2)
'	Arguments				:
'		Input Arguments		: ObjTab 
'		Output Arguments	: 
'	Function is called by	:
'	Function calls			: CheckObjectExist() 
'	Created on				: 08/07/2009
'	Author					: Vidyasagar
'ObjTab.CloseJTab strItemToBeClose
'------------------------------------------------------------------------------------------------------------------------------------------
Function VerifyJTabItemSelected(ByVal ObjTab, Byval strTabItem)
  	On Error Resume Next
		'Variable Section Begin
			Dim strMethod
			Dim strItemToBeSelect, intItemscount, intCnt, intIndex, strTabSelected 
			Dim arrItems
			Dim blnStatus
		'Variable Section End
		strMethod = strKeywordForReporting
			blnStatus = False

			 strItemToBeSelect = trim(strTabItem)
			If CheckObjectExist(ObjTab) Then
				 intItemscount = ObjTab.GetROProperty("items count")
						If intItemscount > 0 Then
						strTabSelected = trim (ObjTab.GetROProperty("value"))
							For intCnt=0 to intItemscount-1
							strActItems = ObjTab.Object.getTitleAt(intCnt)
							strActItems = Trim (strActItems)
								If strItemToBeSelect  = strActItems  Then
									blnStatus = True
									Exit for
								End If
							Next  
							If blnStatus  Then
									If  strTabSelected  = strItemToBeSelect Then
									VerifyJTabItemSelected = 0
										WriteStatusToLogs "The given tab is selected."
									Else
											VerifyJTabItemSelected = 2
											WriteStatusToLogs "The Expected Tab Item '"& strItemToBeSelect &"' was not selected, please verify."
									End If
						Else
						If strItemToBeSelect = "" or IsEmpty (strItemToBeSelect) or IsNull(strItemToBeSelect)  Then
									strItemToBeSelect = "Empty"
						End If
							VerifyJTabItemSelected=1
							WriteStatusToLogs "The Tab item '"& strItemToBeSelect &"' does not exist, please verify."
						End If
					Else
					 VerifyJTabItemSelected=1
						WriteStatusToLogs "There were no tab items exiting in Tab object."
					End If
			Else															'CheckObjectExist
				VerifyJTabItemSelected = 1
				WriteStatusToLogs "The Tab does not exist, please verify."
			End If
		On Error GoTo 0
End Function

'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name			: VerifyNumberOfJTabItems
'	Purpose					: Closes the specified tab (for tab controls that support closeable tabs).
'	Return Value			: Integer( 0/2)
'	Arguments				:
'		Input Arguments		: ObjTab 
'		Output Arguments	: 
'	Function is called by	:
'	Function calls			: CheckObjectExist() 
'	Created on				: 08/07/2009
'	Author					: Vidyasagar
'ObjTab.CloseJTab strItemToBeClose
'------------------------------------------------------------------------------------------------------------------------------------------
Function VerifyNumberOfJTabItems(ByVal ObjTab, Byval intExpectedSize)
  	On Error Resume Next
		'Variable Section Begin
		
			Dim strMethod
			Dim intActcount,intcount
			
		'Variable Section End
		strMethod = strKeywordForReporting
			If CheckObjectExist(ObjTab)  Then   
				intcount=Cint(intExpectedSize)
				
				If VarType(intcount) = VbInteger Then				
					intActcount = ObjTab.GetROProperty("items count")

					If Not IsEmpty(intActcount)  Then
							If CompareStringValues(intcount,intActcount)=0 Then
								  VerifyNumberOfJTabItems = 0
								  WriteStatusToLogs "The specified Tab count is same as the expected size."
							Else
								   VerifyNumberOfJTabItems = 2
									WriteStatusToLogs "The specified Tab count is different from the expected size, please verify."
							End If
					Else
					  VerifyNumberOfJTabItems = 1
						WriteStatusToLogs "Action Fails to Get Tab count, please Verify Tab and its method and property."
					End If
				Else
				   VerifyNumberOfJTabItems = 1
					 WriteStatusToLogs "The specified Tab count, expected size is not an Integer, please verify."
				End If
			Else
				 VerifyNumberOfJTabItems = 1
				 WriteStatusToLogs "The specified Tab doesn't exist, please verify."

			End If
			'WriteStepResults VerifyListBoxNOTContainsItem
    On Error GoTo 0

End Function
'=======================Code Section End=============================================


