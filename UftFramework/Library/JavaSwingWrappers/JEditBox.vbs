''Function SetValueInJEditBox(ByVal objEditbox , ByVal strValue)
''Function VerifyJEditBoxValue(ByVal objEditbox , ByVal strExpValue)
''Function ClickLinkInJEditBox (ByVal ObjEditbox, ByVal strExpLinkName)
'Function Q_StoreText(ByVal objEditbox,ByVal strKey)
'Function Q_DeleteText(Byval objEditbox , Byval intRowStart , ByVal intColStart, Byval intRowEnd, Byval intColEnd)
'Function Q_InsertText(Byval objEditbox ,Byval strValue, Byval intRowNum , ByVal intColNum)
'Function Q_SetSelection(Byval objEditbox , Byval intRowStart , ByVal intColStart, Byval intRowEnd, Byval intColEnd)
'Function Q_SetCursorPosition(Byval objEditbox,Byval intRowNum,Byval intColNum)
'Function Q_ReplaceText(Byval objEditbox , Byval intRowStart , ByVal intColStart, Byval intRowEnd, Byval intColEnd,ByVal strValue)

'=======================Code Section Begin=============================================

Option Explicit		'Forces explicit declaration of all variables in a script. 
'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name :				 SetValueInJEditBox()
' 	Purpose :						 Enters a value into editbox
' 	Return Value :		 		   Integer(0/1)
' 	Arguments :
'		Input Arguments :  		objEditbox , strValue
'		Output Arguments : 
'	Function is called by :
'	Function calls :			    CheckObjectExist()
'	Created on :				   12/03/2008
'	Author :						 Rathna Reddy 
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function SetValueInJEditBox(ByVal objEditbox , ByVal strValue)
	On Error Resume Next
		'Variable Section Begin
			
				Dim strMethod 

		'Variable Section End
		strMethod = strKeywordForReporting
		   If CheckObjectExist(objEditbox) Then
				   If CheckJavaObjectenabled(objEditBox) Then
						objEditbox.Set strValue
						 If err.Number = 0 Then
							 SetValueInJEditBox = 0
								WriteStatusToLogs "The value was set in EditBox successfully."
							Else
								SetValueInJEditBox = 1
								WriteStatusToLogs "The value was not set in EditBox, please verify."
						  
							End If
				    Else
					SetValueInJEditBox = 1
					WriteStatusToLogs "The EditBox was not enabled, please verify."


   			End If
				  
				Else
						SetValueInJEditBox = 1
						WriteStatusToLogs "The EditBox does not exist, please verify."
				End If
	On Error GoTo 0
End Function
'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name :				VerifyJEditBoxValue()
' 	Purpose :					 Verifies the editbox value with our expected whenever required
' 	Return Value :		 		  Integer( 0/1)
' 	Arguments :
'		Input Arguments :  		objEditbox,strExpValue
'		Output Arguments :     strActValue
'	Function is called by :
'	Function calls :				CheckObjectExist(),CompareStringValues()
'	Created on :				   12/03/2008
'	Author :						 Rathna Reddy 
'Note: This is not default value verification.
'------------------------------------------------------------------------------------------------------------------------------------------
Function VerifyJEditBoxValue(ByVal objEditbox , ByVal strExpValue)
	On Error Resume Next
		 'Variable Section Begin
		
			Dim strMethod
			Dim strActValue, blnstatus 
			blnstatus  = False

		'Variable Section End
		strMethod = strKeywordForReporting
			If CheckObjectExist(objEditbox) Then  
				strActValue = objEditbox.GetROProperty("value")
				
				If Vartype(strActValue) <> vbEmpty  And Err.Number = 0 Then
					blnstatus = true
				End If
					If blnstatus  Then
								If CompareStringValues(strExpValue,strActValue)  = 0 Then
									VerifyJEditBoxValue = 0
									WriteStatusToLogs "Expected value : "&strExpValue& """" &  " and " &""""&"Actual Value : "&strActValue&""""&" matches as expected."
		
								Else
									VerifyJEditBoxValue = 2
									WriteStatusToLogs "Expected value : "&strExpValue& """" &  " and " &""""&"Actual Value : "&strActValue&""""&" does not match, please verify."
		
								End If
					Else
							VerifyJEditBoxValue = 1
							WriteStatusToLogs "An error occurred while capturing edit box value."
					End If
		Else
				VerifyJEditBoxValue = 1
				 WriteStatusToLogs 	"EditBox does not exist, please verify."
				
		 End If
    On Error GOTO 0
End Function

'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name :				ClickLinkInJEditBox()
' 	Purpose :						'Clicks the link with the specified name in the edit box.
' 	Return Value :		 		  Integer( 0/1)
' 	Arguments :
'		Input Arguments :  		objEditbox
'		Output Arguments :    
'	Function is called by :
'	Function calls :				CheckObjectExist()
'	Created on :				   15/08/2009
'	Author :						 Ashok Jakkani
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function ClickLinkInJEditBox (ByVal ObjEditbox, ByVal strExpLinkName)
	On Error Resume Next
		'Variable Section Begin
			Dim strMethod
			Dim blnstatus 
			blnstatus  = False

		'Variable Section End
		 strMethod = strKeywordForReporting
			If  CheckObjectExist(ObjEditbox)  Then
				If IsNull(strExpLinkName) Then
					blnstatus = True
				End If
				If len(strExpLinkName) > 0 Then
                    ObjEditbox.ClickLink strExpLinkName
					If Err.Number = 0  Then
                    ClickLinkInJEditBox = 0
						WriteStatusToLogs "Click operation was performed on the specified Link."
					Else
						ClickLinkInJEditBox = 1
						WriteStatusToLogs "Click operation was not performed on the specified Link, please verify."
					End If 
			    Else										' len(strExpLinkName) > 0 
						ClickLinkInJEditBox = 1
						WriteStatusToLogs "The specified Link is Empty, please Verify."
				End If
			Else											'CheckObjectExist
					ClickLinkInJEditBox = 1
					WriteStatusToLogs "The specified Link does not exist."
			End If
		   
	On Error GoTo 0
End Function


'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :			  VerifyJEditBoxExistence()
'	Purpose :				  Verifies if a edit box exists for not
'	Return Value :		 	  Integer(0/1/2)
'	Arguments :
'		Input Arguments :  	  objEditBox , existence
'		Output Arguments : 
'	Function is called by :
'	Function calls :		    CheckObjectExist() 
'	Created on :			    01/02/2011
'	Author :					Sanam Chugh
'
'------------------------------------------------------------------------------------------------------------------------------------------

Function VerifyJEditBoxExistence(ByVal objEditBox, ByVal existence)
	On Error Resume Next

		'Variable Section Begin
	
			Dim strMethod 

		'Variable Section End
		strMethod = strKeywordForReporting

		
			
			
		If StrComp(existence,"") =1 Then
		 If IsBoolean(existence) = 0 then
			If CheckObjectExist(objEditBox) Then 
			 
			   If existence Then
						VerifyJEditBoxExistence = 0
						WriteStatusToLogs "The edit box exists."
				Else
						VerifyJEditBoxExistence = 2
						WriteStatusToLogs "The edit box exists."
				End If 
			Else 
				If Not existence then 
					VerifyJEditBoxExistence = 0
					WriteStatusToLogs "The edit box does not exist."
				
				Else
                   VerifyJEditBoxExistence = 2
				   WriteStatusToLogs "The edit box does not exists."
			  End If
			End If

			Else
			
				WriteStatusToLogs "The existence parameter must be boolean 'true' or 'false'. Please verify"
				VerifyJEditBoxExistence=1

			End if
			
		Else
		
			VerifyJEditBoxExistence=1
			WriteStatusToLogs "The existence parameter is empty. Please verify"

		End if
	On Error GoTo 0
End Function




'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :			  VerifyJEditBoxVisibility()
'	Purpose :				  Verifies if a edit box is visible or not
'	Return Value :		 	  Integer(0/1/2)
'	Arguments :
'		Input Arguments :  	  objEditBox , visiblity
'		Output Arguments : 
'	Function is called by :
'	Function calls :		    VerifyObjectVisible() 
'	Created on :			    01/02/2011
'	Author :					Sanam Chugh
'
'------------------------------------------------------------------------------------------------------------------------------------------

Function VerifyJEditBoxVisibility(ByVal objEditBox, ByVal visiblity)
	On Error Resume Next

		'Variable Section Begin
	
			Dim strMethod

		'Variable Section End
		strMethod = strKeywordForReporting

		
			
	If StrComp(visiblity,"") =1 Then
	 If IsBoolean(visiblity)= 0 then
		If CheckObjectExist(objEditBox) then
			If CheckJavaObjectVisible(objEditBox) Then 
			   If visiblity then
						VerifyJEditBoxVisibility = 0
						WriteStatusToLogs "The edit box is visible."
				Else
						VerifyJEditBoxVisibility = 2
						WriteStatusToLogs "The edit box is visible."
				End If 
			Else 
				If Not visiblity then
					VerifyJEditBoxVisibility = 0
					WriteStatusToLogs "The edit box is invisible."
				
				Else
                   VerifyJEditBoxVisibility = 2
				   WriteStatusToLogs "The edit box is invisible."
			  End If
			end If
			
			else
				VerifyJEditBoxVisibility = 1
						WriteStatusToLogs "The specified edit box does not exist, please verify."

			end If

			Else
			
				WriteStatusToLogs "The visibility parameter must be boolean 'true' or 'false'. Please verify"
				VerifyJEditBoxVisibility=1

			End if
			
		   else
			VerifyJEditBoxVisibility=1
			WriteStatusToLogs "The visiblity parameter is empty. Please verify"

		End If
		
	On Error GoTo 0
End Function


'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :			  VerifyJEditBoxEnability()
'	Purpose :				  Verifies if a edit box is enabled or not
'	Return Value :		 	  Integer(0/1/2)
'	Arguments :
'		Input Arguments :  	  objEditbox , enability
'		Output Arguments : 
'	Function is called by :
'	Function calls :		    CheckJavaObjectenabled() 
'	Created on :			    01/02/2011
'	Author :					Sanam Chugh
'
'------------------------------------------------------------------------------------------------------------------------------------------

Function VerifyJEditBoxEnability(ByVal objEditbox, ByVal enability)
	On Error Resume Next

		'Variable Section Begin
	
		Dim strMethod

		'Variable Section End
		strMethod = strKeywordForReporting

		
			
	If StrComp(enability,"") =1 Then
	 If IsBoolean(enability)= 0 then
		If CheckObjectExist(objEditBox) then
			If CheckJavaObjectenabled(objEditbox) Then 
			   If enability then
						VerifyJEditBoxEnability = 0
						WriteStatusToLogs "The edit box is enabled."
				Else
						VerifyJEditBoxEnability = 2
						WriteStatusToLogs "The edit box is enabled ."
				End If 
			Else 
				If Not enability then
					VerifyJEditBoxEnability = 0
					WriteStatusToLogs "The edit box is disabled."
				
				Else
                   VerifyJEditBoxEnability = 2
				   WriteStatusToLogs "The edit box is disabled."
			  End If
			
		end If
		
		else
				VerifyJEditBoxEnability = 1
						WriteStatusToLogs "The specified edit box does not exist, please verify."

		end If

		Else
			
				WriteStatusToLogs "The enability parameter must be boolean 'true' or 'false'. Please verify"
				VerifyJEditBoxEnability=1

			End if
		
		Else
		VerifyJEditBoxEnability=1
			WriteStatusToLogs "The enability parameter is empty. Please verify"

		End If
	On Error GoTo 0
End Function


'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name :				 Q_ReplaceText()
' 	Purpose :					 Replaces text in specified boundaries with new text.
' 	Return Value :		 		   Integer(0/1)
' 	Arguments :
'		Input Arguments :  		objEditbox , intRowStart,intColStart,intRowEnd,intColEnd,strValue
'		Output Arguments : 
'	Function is called by :
'	Function calls :			    CheckObjectExist()
'	Created on :				   30/06/2016
'	Author :						 Qualitia
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function Q_ReplaceText(Byval objEditbox , Byval intRowStart , ByVal intColStart, Byval intRowEnd, Byval intColEnd,ByVal strValue)
On Error Resume next
	'Variable Section Begin
		Dim strMethod
		Dim intRowS, intRowE, intColS, intColE
		Dim edtTxt, strLen
		
	'Variable Section End
	strMethod = strKeywordForReporting
	
	intRowS = IsWholeNumber(intRowStart)
	intColS = IsWholeNumber(intColStart)
	intRowE = IsWholeNumber(intRowEnd)
	intCOlE = IsWholeNumber(intColEnd)
	
	If Not (Isnumeric(intRowStart) or Isnumeric(intColStart) or Isnumeric(intRowEnd) or Isnumeric(intColEnd)) or cint(intRowStart) <1 or cint(intColStart)<1 or cint(intRowEnd) <1 or cint(intColEnd) <1  Then
		Q_ReplaceText = 1
		WriteStatusToLogs 	"Parameters are not +ve numbers."
		exit function
	End If
	
	
	edtTxt = objEditbox.getroproperty("text")
	strLen = Len(edtTxt)
	
	If cint(intColStart) > strLen Then
		Q_ReplaceText = 1
		WriteStatusToLogs 	"Parameter 'ColumnStart' is beyond length of the text in editbox."
		Exit Function
	Elseif cint(intColEnd) > strLen Then
		Q_ReplaceText = 1
		WriteStatusToLogs 	"Parameter 'ColumnEnd' is beyond length of the text in editbox."
		Exit Function
	Elseif cint(intColStart) > cint(intColEnd) Then
		Q_ReplaceText = 1
		WriteStatusToLogs 	"ColumnStart is greater than ColumnEnd."
		Exit Function
	
	End If
	
	If intRowS =0 And intColS = 0 And intRowE =0 And intColE =0 Then
		
		If CheckObjectExist(objEditbox)  Then 
				
			If CheckJavaObjectVisible(objEditbox) Then 
					
				If CheckJavaObjectenabled(objEditbox) Then
					
					intRowStart = CInt(intRowStart)
					intRowEnd = CInt(intRowEnd)
					intColStart = CInt(intColStart)
					intColEnd = CInt(intColEnd)
					
					If Not IsNull(intRowStart) or Not IsEmpty(intColStart) And Not IsNull(intRowEnd) or Not IsEmpty(intColEnd) And Not IsNull(strValue) or Not IsEmpty(strValue) Then 				
						
						If intRowStart >= 0 And intRowEnd >= intRowStart And intColStart >= 0 And  intColEnd  >= intColStart Then
							
							objEditbox.Replace (intRowStart-1), (intColStart-1), (intRowEnd-1), (intColEnd-1),strValue

								If Err.number <> 0 Then
								Q_ReplaceText = 1
								WriteStatusToLogs 	"Error occurred, "&err.description
													  
							Else
								Q_ReplaceText = 0
								WriteStatusToLogs 	"Replaced previous text Value with new '"&strValue&"' successfully."
							
							End If
							
						ElseIf (intRowEnd < intRowStart) And (intColEnd<intColStart)Then 
							Q_ReplaceText = 1
							WriteStatusToLogs 	"The row number ':"&intRowStart&"' columnstart :'"& intColStart&"' and rowends: '"& intRowEnd&"' columnends: '"&intColEnd&"' does not exist, please verify"

						ElseIf (intRowEnd < intRowStart)Then 
							Q_ReplaceText = 1
							WriteStatusToLogs 	"The row number '"&intRowStart&"' and '"&intRowEnd&"' does not exist, please verify."
							
						ElseIf (intColEnd < intColStart)Then 
							Q_ReplaceText = 1
							WriteStatusToLogs 	"The column number '"&intColStart&"' and '"&intColEnd&"' does not exist, please verify."
							
						End If

					ElseIf intRowStart = 1 And intColStart = 1 And intRowEnd =1 And intColEnd =1 then
						Q_ReplaceText = 1
						WriteStatusToLogs 	"Action failed to fetch the Java Table data. Please Verify."
					ElseIf	strValue = 1 Then
						Q_ReplaceText = 1
						WriteStatusToLogs 	"'"&strValue&"' is Null or Empty. Please Verify."					
					End If

				Else
					Q_ReplaceText = 1
					WriteStatusToLogs 	"The specified Java EditBox is not enabled, please verify."	
				End If

			Else
				Q_ReplaceText = 1
				WriteStatusToLogs 	"The specified Java EditBox is not visible, please verify."	
			End If

		Else
			Q_ReplaceText = 1
			WriteStatusToLogs 	"The specified Java EditBox does not exist, please verify."	
		End If
		
	ElseIf intRowStart = 1 And intColStart = 1 And intRowEnd =1 And intColEnd =1 then
		Q_ReplaceText = 1
		WriteStatusToLogs 	"Given rowstart '"&intRowStart&"' columnstart '"&intColStart&"' and rowend '"&intRowEnd&"' columnend '"&intColEnd&"are not natural numbers, please verify."	
	ElseIf intRowStart = 1 Then
		Q_ReplaceText = 1
		WriteStatusToLogs 	"Given rowstart '"&intRowStart&"' is a not natural number, please verify."	
	
	ElseIf intColStart = 1 Then
		Q_ReplaceText = 1
		WriteStatusToLogs 	"Given columnstart '"&intColStart&"' is not a natural number, please verify."	

     ElseIf intRowEnd = 1 Then
		Q_ReplaceText = 1
		WriteStatusToLogs 	"Given rowend '"&intRowEnd&"' is a not natural number, please verify."	
	
	ElseIf intColEnd = 1 Then
		Q_ReplaceText = 1
		WriteStatusToLogs 	"Given columnend '"&intColEnd&"' is not a natural number, please verify."	
	End if
On Error goto 0
End Function
'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name :				 PlaceCursorInJEditBox()
' 	Purpose :					 Places the cursor in the specified location in an edit box
' 	Return Value :		 		   Integer(0/1)
' 	Arguments :
'		Input Arguments :  		objEditbox , intRowStart,intColStart,intRowEnd,intColEnd,strValue
'		Output Arguments : 
'	Function is called by :
'	Function calls :			    CheckObjectExist()
'	Created on :				   30/06/2016
'	Author :						 Qualitia 
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function Q_SetCursorPosition(Byval objEditbox,Byval intRowNum,Byval intColNum)
On Error Resume next
	'Variable Section Begin
		Dim strMethod
		Dim intRowS, intRowE, intColS
		dim edtTxt, strLen

	'Variable Section End
	strMethod = strKeywordForReporting
	
	intRowS = IsWholeNumber(intRowNum)
	intColS = IsWholeNumber(intColNum)
	
	If Not (IsNumeric(intRowNum) or IsNumeric(intColNum)) or cint(intRowNum) <1 or cint(intColNum) <1  Then
		Q_SetCursorPosition =1
		WriteStatusToLogs 	"Parameters are not +ve numbers."	
		Exit function
	End If
	
	
	edtTxt = objEditbox.getroproperty("text")
	strLen = Len(edtTxt)
	
	If cint(intColNum) > strLen Then
		Q_SetCursorPosition = 1
		WriteStatusToLogs 	"Parameter 'ColumnNumber' is beyond the length of text in editbox."
		Exit Function
	End If
	
	If intRowS =0 And intColS = 0 Then		
			If CheckObjectExist(objEditbox)  Then 
				If CheckJavaObjectVisible(objEditbox) Then
					If CheckJavaObjectenabled(objEditbox) Then
						intRowNum = CInt(intRowNum)
						intColNum = CInt(intColNum)
						If Not IsNull(intRowNum) or Not IsEmpty(intColNum) Then
							
							If intRowNum >= 0 And intColNum >= 0 Then
								intRowNum = intRowNum - 1
								intColNum = intColNum -1
								objEditbox.SetCaretPos intRowNum, intColNum
									If Err.number <> 0 Then
									Q_SetCursorPosition = 1
									WriteStatusToLogs 	"Error occurred: "& err.description													  
									Else
									Q_SetCursorPosition = 0
									WriteStatusToLogs 	"Cursor is placed in the specified location in the edit box."							
									End If							
						Else
							Q_SetCursorPosition = 1
							WriteStatusToLogs 	"The row number ':"&intRowNum&"' column Number :'"& intColNum&"' does not exist, please verify."											
						End If
						Else
					Q_SetCursorPosition = 1
					WriteStatusToLogs 	"Parameters passed are not valid, please verify."	
					End If
					Else
					Q_SetCursorPosition = 1
					WriteStatusToLogs 	"The specified Java EditBox is not enabled, please verify."	
					End If
				Else
				Q_SetCursorPosition = 1
				WriteStatusToLogs 	"The specified Java EditBox is not visible, please verify."	
				End If
			Else
			Q_SetCursorPosition = 1
			WriteStatusToLogs 	"The specified Java EditBox does not exist, please verify."	
			End If		
	ElseIf intRowNum = 1 And intColNum = 1 then
		Q_SetCursorPosition = 1
		WriteStatusToLogs 	"Given row number '"&intRowNum&"' column number '"&intColNum&"'are not natural numbers, please verify."	
	ElseIf intRowNum = 1 Then
		Q_SetCursorPosition = 1
		WriteStatusToLogs 	"Given row number '"&intRowNum&"' is a not natural number, please verify."		
	ElseIf intColNum = 1 Then
		Q_SetCursorPosition = 1
		WriteStatusToLogs 	"Given column number '"&intColNum&"' is not a natural number, please verify."	
	
	End if
On Error goto 0
End Function

'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name :				 Q_SetSelection()
' 	Purpose :					 Selects the specified text in the edit box.
' 	Return Value :		 		   Integer(0/1)
' 	Arguments :
'		Input Arguments :  		objEditbox , intRowStart, intColStart, intRowEnd, intColEnd
'		Output Arguments : 
'	Function is called by :
'	Function calls :			    CheckObjectExist()
'	Created on :				   30/06/2016
'	Author :						 Qualitia 
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function Q_SetSelection(Byval objEditbox , Byval intRowStart , ByVal intColStart, Byval intRowEnd, Byval intColEnd)
On Error Resume next
	'Variable Section Begin
		Dim strMethod
		Dim intRowS, intRowE, intColS, intColE
		dim edtTxt, strLen
	'Variable Section End
	strMethod = strKeywordForReporting
	
	intRowS = IsWholeNumber(intRowStart)
	intColS = IsWholeNumber(intColStart)
	intRowE = IsWholeNumber(intRowEnd)
	intCOlE = IsWholeNumber(intColEnd)
	
	If Not(Isnumeric(intRowStart) or Isnumeric(intColStart) or Isnumeric(intRowEnd) or Isnumeric(intColEnd)) or cint(intRowStart)<1 or cint(intColStart)<1 or cint(intRowEnd)<1 or cint(intColEnd)<1  Then
		Q_SetSelection = 1
		WriteStatusToLogs 	"Parameters are not +ve numbers."
		exit function
	End If
	
	edtTxt = objEditbox.getroproperty("text")
	strLen = Len(edtTxt)
	
	If cint(intColStart) > strLen Then
		Q_SetSelection = 1
		WriteStatusToLogs 	"Parameter 'ColumnStart' is beyond length of the text in editbox."
		Exit Function
	Elseif cint(intColEnd) > strLen Then
		Q_SetSelection = 1
		WriteStatusToLogs 	"Parameter 'ColumnEnd' is beyond length of the text in editbox."
		Exit Function
	Elseif cint(intColStart) > cint(intColEnd) Then
		Q_SetSelection = 1
		WriteStatusToLogs 	"ColumnStart is greater than ColumnEnd."
		Exit Function
	
	End If
	
	If intRowS =0 And intColS = 0 And intRowE =0 And intColE =0 Then

		If CheckObjectExist(objEditbox)  Then 

			If CheckJavaObjectVisible(objEditbox) Then 

				If CheckJavaObjectenabled(objEditbox) Then

					intRowStart = CInt(intRowStart)
					intRowEnd = CInt(intRowEnd)
					intColStart = CInt(intColStart)
					intColEnd = CInt(intColEnd)
					
					If Not IsNull(intRowStart) or Not IsEmpty(intColStart) And Not IsNull(intRowEnd) or Not IsEmpty(intColEnd) Then 				
						
						If intRowStart >= 0 And intRowEnd >= intRowStart And intColStart >= 0 And  intColEnd  >= intColStart Then
							intRowStart = intRowStart -1
							intColStart = intColStart -1
							intRowEnd = intRowEnd-1
							intColEnd = intColEnd -1
							
							objEditbox.SetSelection intRowStart, intColStart, intRowEnd, intColEnd

								If Err.number <> 0 Then
								Q_SetSelection = 1
								WriteStatusToLogs 	"Error occurred, "&err.description
													  '"Message:" & "The Operation cannot be Performed,Please Verify."
													  
							Else
							
								Q_SetSelection = 0
								WriteStatusToLogs 	"the specified text is selected in the edit box successfully."
							
							End If
							
						ElseIf (intRowStart < 1) And (intColStart < 1) And (intRowEnd < intRowStart) And (intColEnd<intColStart)Then 
							Q_SetSelection = 1
							WriteStatusToLogs 	"The row number ':"&intRowStart&"' columnstart :'"& intColStart&"' and rowends: '"& intRowEnd&"' columnends: '"&intColEnd&"' does not exist, please verify. "

						End If

					Else
						Q_SetSelection = 1
						WriteStatusToLogs 	"Action failed While perform. Please Verify."
					End If

				Else
					Q_SetSelection = 1
					WriteStatusToLogs 	"The specified Java EditBox is not enabled, please verify."	
				End If

			Else
				Q_SetSelection = 1
				WriteStatusToLogs 	"The specified Java EditBox is not visible, please verify."	
			End If

		Else
			Q_SetSelection = 1
			WriteStatusToLogs 	"The specified Java EditBox does not exist, please verify."	
		End If
		
	ElseIf intRowStart = 1 And intColStart = 1 And intRowEnd =1 And intColEnd =1 then
		Q_SetSelection = 1
		WriteStatusToLogs 	"Given rowstart '"&intRowStart&"' columnstart '"&intColStart&"' and rowend '"&intRowEnd&"' columnend '"&intColEnd&"are not natural numbers, please verify."	
	ElseIf intRowStart = 1 Then
		Q_SetSelection = 1
		WriteStatusToLogs 	"Given rowstart '"&intRowStart&"' is a not natural number, please verify."	
	
	ElseIf intColStart = 1 Then
		Q_SetSelection = 1
		WriteStatusToLogs 	"Given columnstart '"&intColStart&"' is not a natural number, please verify."	

     ElseIf intRowEnd = 1 Then
		Q_SetSelection = 1
		WriteStatusToLogs 	"Given rowend '"&intRowEnd&"' is a not natural number, please verify."	
	
	ElseIf intColEnd = 1 Then
		Q_SetSelection = 1
		WriteStatusToLogs 	"Given columnend '"&intColEnd&"' is not a natural number, please verify."	
	End if
On Error goto 0
End Function
'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name :				 Q_InsertText()
' 	Purpose :					 Inserts a text string into the specified location in a edit box.
' 	Return Value :		 		   Integer(0/1)
' 	Arguments :
'		Input Arguments :  		objEditbox , intRowNum, intRowNum
'		Output Arguments : 
'	Function is called by :
'	Function calls :			    CheckObjectExist()
'	Created on :				   30/06/2016
'	Author :						 Qualitia 
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function Q_InsertText(Byval objEditbox ,Byval strValue, Byval intRowNum , ByVal intColNum)
On Error Resume next
	'Variable Section Begin
		Dim strMethod
		Dim intRowS, intRowE
		dim edtTxt, strLen, intColS
	'Variable Section End
	strMethod = strKeywordForReporting
	
	If Not(Isnumeric(intRowNum) or Isnumeric(intColNum)) or cint(intRowNum)<1 or cint(intColNum)<1 Then
		Q_InsertText = 1
		WriteStatusToLogs 	"Parameters are not +ve numbers."
		exit function
	End If
	
	intRowS = IsWholeNumber(intRowNum)
	intColS = IsWholeNumber(intColNum)
	
	edtTxt = objEditbox.getroproperty("text")
	strLen = Len(edtTxt)
	
	If cint(intColNum) > strLen Then
		Q_InsertText = 1
		WriteStatusToLogs 	"Parameter 'ColumnNumber' is beyond length of the text in editbox."
		Exit Function

	End If
	
	If intRowS =0 And intColS = 0 Then

		If CheckObjectExist(objEditbox)  Then 

			If CheckJavaObjectVisible(objEditbox) Then 

				If CheckJavaObjectenabled(objEditbox) Then

					intRowNum = CInt(intRowNum)
					intColNum = CInt(intColNum)
					
					
					If Not IsNull(intRowNum) or Not IsEmpty(intRowNum) And Not IsNull(intColNum) or Not IsEmpty(intColNum) And Not Isnull(strValue) or Not IsEmpty(strValue) Then 				
						
						If intRowNum >= 0 And intColNum >= 0 Then
							
							objEditbox.Insert  strValue, intRowNum-1, intColNum

								If Err.number <> 0 Then
								Q_InsertText = 1
								WriteStatusToLogs 	"Error occurred, "&err.description
													  '"Message:" & "The Operation cannot be Performed,Please Verify."
													  
								Else
								Q_InsertText = 0
								WriteStatusToLogs 	"'"&strValue&"' is Insert into specified location in an edit box successfully."
							
								End If
							
						Else
							Q_InsertText = 1
							WriteStatusToLogs 	"The row number ':"&intRowNum&"' column Number :'"& intColNum&"' does not exist , please verify."						
							
						End If

					ElseIf IsNull(intRowNum) or IsEmpty(intRowNum) And IsNull(intColNum) or IsEmpty(intColNum) Then
						Q_InsertText = 1
						WriteStatusToLogs 	"Action failed while performed, please Verify."
					ElseIf IsNull(strValue) or IsEmpty(strValue) Then
						Q_InsertText = 1
						WriteStatusToLogs 	"'"&strValue&"' is null or empty, please Verify."
					End If

				Else
					Q_InsertText = 1
					WriteStatusToLogs 	"The specified Java EditBox is not enabled, please verify."	
				End If

			Else
				Q_InsertText = 1
				WriteStatusToLogs 	"The specified Java EditBox is not visible, please verify."	
			End If

		Else
			Q_InsertText = 1
			WriteStatusToLogs 	"The specified Java EditBox does not exist, please verify."	
		End If
		
	ElseIf intRowNum = 1 And intColNum = 1 then
		Q_InsertText = 1
		WriteStatusToLogs 	"Given row number '"&intRowNum&"' column number '"&intColNum&"'are not natural numbers, please verify."	
	ElseIf intRowNum = 1 Then
		Q_InsertText = 1
		WriteStatusToLogs 	"Given row number '"&intRowNum&"' is a not natural number, please verify."	
	
	ElseIf intColNum = 1 Then
		Q_InsertText = 1
		WriteStatusToLogs 	"Given column number '"&intColNum&"' is not a natural number, please verify."	
	
	End if
On Error goto 0
End Function
'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name :				 Q_DeleteTextInJEditBox()
' 	Purpose :					 Delete a text string into the specified location in a edit box.
' 	Return Value :		 		   Integer(0/1)
' 	Arguments :
'		Input Arguments :  		objEditbox , intRowStart, intColStart, intRowEnd, intColEnd
'		Output Arguments : 
'	Function is called by :
'	Function calls :			    CheckObjectExist()
'	Created on :				   30/06/2016
'	Author :						 Qualitia 
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function Q_DeleteText(Byval objEditbox , Byval intRowStart , ByVal intColStart, Byval intRowEnd, Byval intColEnd)
On Error Resume next
	'Variable Section Begin
		Dim strMethod
		Dim intRowS, intRowE, intColS, intColE
		Dim edtTxt,strLen
	'Variable Section End
	strMethod = strKeywordForReporting
	
	If Not(Isnumeric(intRowStart) or Isnumeric(intColStart) or Isnumeric(intRowEnd) or Isnumeric(intColEnd)) or cint(intRowStart)<1 or cint(intColStart) <1 or cint(intRowEnd) <1 or cint(intColEnd)<1 Then
		Q_DeleteText = 1
		WriteStatusToLogs 	"Parameters are not +ve numbers."
		exit function
	End If
	
	edtTxt = objEditbox.getroproperty("text")
	strLen = Len(edtTxt)
	
	If cint(intColStart) > strLen Then
		Q_DeleteText = 1
		WriteStatusToLogs 	"Parameter 'ColumnStart' is beyond length of the text in editbox."
		Exit Function
	Elseif cint(intColEnd) > strLen Then
		Q_DeleteText = 1
		WriteStatusToLogs 	"Parameter 'ColumnEnd' is beyond length of the text in editbox."
		Exit Function
	Elseif cint(intColStart) > cint(intColEnd) Then
		Q_DeleteText = 1
		WriteStatusToLogs 	"ColumnStart is greater than ColumnEnd."
		Exit Function
	
	End If
	If err.number = 0 Then

		If CheckObjectExist(objEditbox)  Then 

			If CheckJavaObjectVisible(objEditbox) Then 

				If CheckJavaObjectenabled(objEditbox) Then

					intRowStart = CInt(intRowStart)
					intRowEnd = CInt(intRowEnd)
					intColStart = CInt(intColStart)
					intColEnd = CInt(intColEnd)
					
					If Not IsNull(intRowStart) or Not IsEmpty(intColStart) And Not IsNull(intRowEnd) or Not IsEmpty(intColEnd) Then 				
						
						If intRowStart >= 0 And intRowEnd >= intRowStart And intColStart >= 0 And  intColEnd  >= intColStart Then
							
							objEditbox.Delete cint(intRowStart)-1, cint(intColStart)-1, cint(intRowEnd)-1, cint(intColEnd)-1

								If Err.number <> 0 Then
								Q_DeleteText = 1
								WriteStatusToLogs 	"Error occurred, "& err.description
													  
							ElseIf err.number = 0 Then
								Q_DeleteText = 0
								WriteStatusToLogs 	"Delete specified text in the edit box successfully."
							
							End If
							
						Else 
							Q_DeleteText = 1
							WriteStatusToLogs 	"The row number ':"&intRowStart&"' columnstart :'"& intColStart&"' and rowends: '"& intRowEnd&"' columnends: '"&intColEnd&"' does not exist, please verify."
							
						End If

					Else
						Q_DeleteText = 1
						WriteStatusToLogs 	"Action failed While perform. Please Verify."
					End If

				Else
					Q_DeleteText = 1
					WriteStatusToLogs 	"The specified Java EditBox is not enabled, please verify."	
				End If

			Else
				Q_DeleteText = 1
				WriteStatusToLogs 	"The specified Java EditBox is not visible, please verify."	
			End If

		Else
			Q_DeleteText = 1
			WriteStatusToLogs 	"The specified Java EditBox does not exist, please verify."	
		End If
		
	ElseIf intRowStart = 1 And intColStart = 1 And intRowEnd =1 And intColEnd =1 then
		Q_DeleteText = 1
		WriteStatusToLogs 	"Given rowstart '"&intRowStart&"' columnstart '"&intColStart&"' and rowend '"&intRowEnd&"' columnend '"&intColEnd&"are not natural numbers, please verify."	
	ElseIf intRowStart = 1 Then
		Q_DeleteText = 1
		WriteStatusToLogs 	"Given rowstart '"&intRowStart&"' is a not natural number, please verify."	
	
	ElseIf intColStart = 1 Then
		Q_DeleteText = 1
		WriteStatusToLogs 	"Given columnstart '"&intColStart&"' is not a natural number, please verify."	

     ElseIf intRowEnd = 1 Then
		Q_DeleteText = 1
		WriteStatusToLogs 	"Given rowend '"&intRowEnd&"' is not a natural number, please verify."	
	
	ElseIf intColEnd = 1 Then
		Q_DeleteText = 1
		WriteStatusToLogs 	"Given columnend '"&intColEnd&"' is not a natural number, please verify."	
	End If
On Error goto 0
End Function
'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :			  Q_StoreText()	
'	Purpose :				  Store Text from object
'	Return Value :		 	  Integer(0/1)
'	Arguments :
'		Input Arguments :  	  objEditbox,strKey
'		Output Arguments : 
'	Function is called by :
'	Function calls :		    CheckObjectExist(),CheckJavaObjectenabled()
'	Created on :			    21/06/2016
'	Author :					Qualitia 
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function Q_StoreText(ByVal objEditbox,ByVal strKey)
	On Error Resume Next

		'Variable Section Begin
	
			Dim strMethod 
			Dim strValue
		'Variable Section End
		strMethod = strKeywordForReporting
			If CheckObjectExist(objEditbox) Then 
			   If CheckJavaObjectenabled(objEditbox) Then
			   
						strValue=objEditbox.GetROProperty("value")
						AddItemToStorageDictionary strKey,strValue
						
					If Err.Number = 0 Then
						Q_StoreText = 0
						WriteStatusToLogs "The Text '"& strValue &"' is stored successfully in the Key '"&strKey&"'"
										
					Else
						Q_StoreText = 1
						WriteStatusToLogs "Error occurred, "&err.description
										 '"Message:" & "The Text ' "& strValue &" ' is not stored successfully in the Key ' "&strKey&" ',Please verify."
										 
										 
					End If
				Else
					Q_StoreText = 1
					WriteStatusToLogs "The '"& objObject &"' was disabled and click could not be performed, please verify."
				End If
			  Else
                    Q_StoreText = 1
				   WriteStatusToLogs "The '"& objObject &"' does not exist, please verify."
			  End If
		
		
	On Error GoTo 0
End Function
'=======================Code Section End=============================================
