'=======================Code Section Begin=============================================

Option Explicit		'Forces explicit declaration of all variables in a script. 
'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name :				 Click()
' 	Purpose :						 Clicks the specified link.
' 	Return Value :		 		   Integer(0/1)
' 	Arguments :
'		Input Arguments :  		objEditbox , strLink
'		Output Arguments : 
'	Function is called by :
'	Function calls :			    CheckObjectExist(),CheckJavaObjectEnabled()
'	Created on :				   30/06/2016
'	Author :						Qualitia
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function Q_Click(ByVal objJLink , ByVal strLink)
	On Error Resume Next
		'Variable Section Begin
			
				Dim strMethod 

		'Variable Section End
		strMethod = strKeywordForReporting
		If CheckObjectExist(objJLink) Then
		    If CheckJavaObjectEnabled(objJLink) Then
					If  Not isnull(strLink) or IsEmpty(strLink)	Then
					
							objJLink.ClickLink strLink
								
								If err.Number = 0 Then
								Q_Click = 0
								WriteStatusToLogs "Clicks the specified link '"&strLink&"' successfully."
								Else
								Q_Click = 1
								WriteStatusToLogs "Not able to Clicks the specified link '"&strLink&"', please verify."
						  
								End If
					Else
					Q_Click = 1
					WriteStatusToLogs "The '"&strLink&"' is null or invalid, please verify."
					End If					
			Else
			Q_Click = 1
			WriteStatusToLogs "The '"&objJLink&"' was not Enabled, please verify."


   			End If
				  
		Else
		Q_Click = 1
		WriteStatusToLogs "The '"&objJLink&"' does not exist, please verify."
				End If
	On Error GoTo 0
End Function

