
'Functions List

'Function VerifyJDialogExist(ByVal objDialog)
'Function VerifyJDialogNotExist(ByVal objDialog)
'Function ActivateJDialog(ByVal objDialog)
'Function CloseJDialog(ByVal objDialog)
'Function VerifyJDialogTitle(ByVal objDialog , ByVal strExpTitle)



'=======================Code Section Begin============================================

Option Explicit		 'Forces explicit declaration of all variables in a script.

'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name			:  VerifyJDialogExist()
'	Purpose					:  Verifies the existence of Dialog in AUT.
'	Return Value			:  Integer( 0/1)
'	Arguments				:
'		Input Arguments		:  objDialog 
'		Output Arguments	: 
'	Function is called by	:
'	Function calls			:  CheckObjectExist() 
'	Created on				:  07/07/2009
'	Author					:  Vidyasagar
'
'------------------------------------------------------------------------------------------------------------------------------------------

Function VerifyJDialogExist(ByVal objDialog)
	On Error Resume Next
		'Variable Section Begin
	
		 Dim strMethod 

		'Variable Section End
		strMethod = strKeywordForReporting
			If CheckObjectExist(objDialog) Then			   
				VerifyJDialogExist = 0
				WriteStatusToLogs "The Dialog exists."					
			Else
				VerifyJDialogExist = 1
				WriteStatusToLogs "The Dialog doesn't exist, please verify."					
			End If	  
	
	On Error GoTo 0
End Function

'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name			: VerifyJDialogNotExist()
'	Purpose					: Verifies whether the Dialog is NotExist or not in AUT?.
'	Return Value			: Integer( 0/1)
'	Arguments				:
'		Input Arguments		: objDialog 
'		Output Arguments	: 
'	Function is called by	:
'	Function calls			: CheckObjectExist() 
'	Created on				: 07/07/2009
'	Author					: Vidyasagar
'
'------------------------------------------------------------------------------------------------------------------------------------------

Function VerifyJDialogNotExist(ByVal objDialog)
	On Error Resume Next
		'Variable Section Begin
	
		   Dim strMethod

		'Variable Section End
		strMethod = strKeywordForReporting
		
			If  CheckObjectNotExist(objDialog) Then 
			   
					VerifyJDialogNotExist = 0
					WriteStatusToLogs "The Dialog doesn't exist."
					
			Else
					VerifyJDialogNotExist = 1
					WriteStatusToLogs "The Dialog exists, please verify."
					
			End If 
	
  		
	On Error GoTo 0
End Function

'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name			: ActivateJDialog()
'	Purpose					: Activates the Specified Dialog.	
'	Return Value			: Integer( 0/1)
'	Arguments				:
'		Input Arguments		: objDialog    
'		Output Arguments	: intVal
'	Function is called by	: 
'	Function calls			: CheckObjectExist()
'	Created on				: 07/07/2009
'	Author					: Vidyasagar
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function ActivateJDialog(ByVal objDialog)
	On Error Resume Next
		'Variable Section Begin
		
			Dim strMethod
			Dim intVal,strTitle

		'Variable Section End
		strMethod = strKeywordForReporting
		
			If CheckObjectExist(objDialog) Then
				 intVal=objDialog.GetROProperty("hwnd")				  
				 Window("hwnd:=" & intVal).Activate
				 
				   If Err.Number = 0 Then
						ActivateJDialog  = 0
						WriteStatusToLogs "The Dialog was activated successfully."
				   Else
						ActivateJDialog = 1 
						WriteStatusToLogs "The Dialog was not activated successfully, please verify."
				   End If
			
			Else
				ActivateJDialog = 1 
				WriteStatusToLogs "The Dialog doesn't exist, please verify."
			End If
	  
   On Error GoTo 0 
End Function

'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :			CloseJDialog()
'	Purpose :				Activates the Specified Dialog.	
'	Return Value :		 	Integer( 0/1)
'	Arguments :
'		Input Arguments :   objDialog    
'		Output Arguments :  intVal
'	Function is called by : 
'	Function calls :		CheckObjectExist()
'	Created on : 			04/03/2008
'	Author :				Rathna Reddy
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function CloseJDialog(ByVal objDialog)
	On Error Resume Next
		'Variable Section Begin
		
			Dim strMethod
			Dim intVal,strTitle

		'Variable Section End
		strMethod = strKeywordForReporting
		
			If CheckObjectExist(objDialog) Then
				 intVal=objDialog.GetROProperty("hwnd")
				 Window("hwnd:=" & intVal).Activate
				 objDialog.Close
				   If Err.Number = 0 Then
						CloseJDialog  = 0
						WriteStatusToLogs "The Dialog was Closed successfully."
				   Else
						CloseJDialog = 1 
						WriteStatusToLogs "The Error occurred during Close Dialog, please verify."
				   End If
			
			Else
				CloseJDialog = 1 
				WriteStatusToLogs "The Dialog doesn't exist, please verify."
			End If
	  
   On Error GoTo 0 
End Function


'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name				:	VerifyJDialogTitle()
' 	Purpose						:	Verify the Dialog title with the expected
' 	Return Value				:   Integer( 0/1)
' 	Arguments					:
'		Input Arguments			:  	objDialog,strExpTitle
'		Output Arguments		:   strActTitle
'	Function is called by		:
'	Function calls				:   CheckObjectExist() , GetArray() 
'	Created on					:   17/08/2009
'	Author						:	Vidyasagar
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function VerifyJDialogTitle(ByVal objDialog , ByVal strExpTitle)
	On Error Resume Next
		'Variable Section Begin	
			Dim strMethod
			Dim strActTitle		
		'Variable Section End
		strMethod = strKeywordForReporting
		If CheckObjectExist(objDialog)  Then
			strActTitle = objDialog.GetROProperty("title")
				If CompareStringValues(strExpTitle,strActTitle)=0 Then
					VerifyJDialogTitle = 0
					WriteStatusToLogs "The Dialog title is same as the expected."
				Else
					VerifyJDialogTitle = 2
					WriteStatusToLogs "The Dialog title is different from the expected, please verify."
				End If
		Else
			VerifyJDialogTitle = 1
			WriteStatusToLogs "The specified Dialog Window doesn't exist, please Verify."
		End If			
    On Error GoTo 0
End Function


'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name				:	CloseJDialogIfExist()
' 	Purpose						:	If the JavaDialog exists, then close it. Keep checking the dialog till timeout 
' 	Return Value				:   Integer(0/1)
' 	Arguments					:
'		Input Arguments			:  	objDialog,timeout
'		Output Arguments		:   
'	Function is called by		:
'	Function calls				:   CheckObjectExist()
'	Created on					:   28/03/2011
'	Author						:	Iohbha K
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function CloseJDialogIfExist(ByVal objDialog,ByVal timeout)
	On Error Resume Next
		'Variable Section Begin
	
		   Dim strMethod
		   Dim sync , counter , intTimeout

		'Variable Section End
		strMethod = strKeywordForReporting
		If IsInteger(timeout) = 0 Then

			intTimeout = CInt(timeout)
			sync = 2
			counter = 0
			Do
				If CheckObjectExist(objDialog) Then			   
					objDialog.Close
					counter = counter + 1					
				End IF
				wait(2)
				sync = sync + 2
			Loop while (intTimeout > sync)
			
			CloseJDialogIfExist = 0
			WriteStatusToLogs "The dialog existed "&counter&" times, and was closed when exist."	
			
		Else
			CloseJDialogIfExist = 1
			WriteStatusToLogs "The timeout parameter "&timeout&" is not a valid positive integer, please verify."					
		End If	 
	
	On Error GoTo 0
End Function
'=======================Code Section End======================================================================
