''Function DragJSliderToNewPosition (ByVal objSlider,ByVal intDrgPos)
''Function DragJSliderFromStartPosition (ByVal objSlider,ByVal intDrgUnits)



'=======================Code Section Begin=============================================

Option Explicit 		'Forces explicit declaration of all variables in a script. 
'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name				:	DragJSliderToNewPosition ()
' 	Purpose						:	Drags the slider to a new position
' 	Return Value				:   Integer( 0/1)
' 	Arguments					:
'		Input Arguments			:  	objSlider,intDrgPos
'		Output Arguments		:   
'	Function is called by		:
'	Function calls				:   CheckObjectExist()
'	Created on					:   19/08/2009
'	Author						:	Vidyasagar
'
'---------------------------------------------------------------------------------------------------------------------------------

Function DragJSliderToNewPosition (ByVal objSlider,ByVal intDrgPos)
	On Error Resume Next
		 'Variable Section Begin
		
			  Dim strMethod
			  Dim intMin,intMax

		 'Variable Section End
		 If not IsNumeric(intDrgPos) Then
			DragJSliderToNewPosition = 1
				 WriteStatusToLogs "Invalid parameter."		
				Exit function 
		 End If
		 
		 strMethod = strKeywordForReporting
				 If CheckObjectExist(objSlider) Then
				 
 					 intMin = objSlider.GetRoProperty("min")                    
					 intMax = objSlider.GetRoProperty("max")						 
						 
						If  VarType(intMin)<>VbEmpty And VarType(intMax)<>VbEmpty Then
						     intMin=cdbl(intMin)
							 intMax=cdbl(intMax)
							   
								 If cdbl(intDrgPos) >= intMin And cdbl(intDrgPos) <= intMax Then
									 objSlider.Drag intDrgPos
									 DragJSliderToNewPosition  = 0
									 WriteStatusToLogs "Slider is dragged to the given position as expected."
								 Else
									DragJSliderToNewPosition = 2  
									WriteStatusToLogs "Slider is not dragged to the given position, please verify."
								 End If                                 
								 
						Else
							DragJSliderToNewPosition = 1
							WriteStatusToLogs  "Verify the Slider Range values."						
						End if

				 Else
					DragJSliderToNewPosition = 1
					WriteStatusToLogs "Slider does not exist, please verify."
				 End If
	On Error GoTo 0
End Function

'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name				:	DragJSliderFromStartPosition()
' 	Purpose						:	Drags the slider the specified number of units from its start position.
' 	Return Value				:   Integer( 0/1)
' 	Arguments					:
'		Input Arguments			:  	objSlider,intDrgUnits
'		Output Arguments		:   
'	Function is called by		:
'	Function calls				:   CheckObjectExist()
'	Created on					:   19/08/2009
'	Author						:	Vidyasagar
'
'---------------------------------------------------------------------------------------------------------------------------------

Function DragJSliderFromStartPosition (ByVal objSlider,ByVal intDrgUnits)
	On Error Resume Next
		 'Variable Section Begin
		
			  Dim strMethod
			  Dim intMax

		 'Variable Section End
		 
		
		 	
		 If not isnumeric(intDrgUnits) Then
		 			DragJSliderFromStartPosition  = 1
							WriteStatusToLogs  "Invalid parameter."	
							Exit function
		 End If
		 strMethod = strKeywordForReporting
				 If CheckObjectExist(objSlider) Then
				 
 					 intMax = objSlider.GetRoProperty("max")						 
						 
						If VarType(intMax)<>VbEmpty Then						     
							 intMax=cdbl(intMax)
							   
								 If cdbl(intDrgUnits) <= intMax Then
									 objSlider.DragFromStart intDrgUnits
									 DragJSliderFromStartPosition  = 0
									 WriteStatusToLogs "Slider is draged to the given no. of units as expected."
								 Else
									DragJSliderFromStartPosition  = 2  
									WriteStatusToLogs "Slider is not draged to the given no. of units, please verify."
								 End If                                 
								 
						Else
							DragJSliderFromStartPosition  = 1
							WriteStatusToLogs  "Verify the Slider Range values"						
						End if

				 Else
					DragJSliderFromStartPosition  = 1
					WriteStatusToLogs "Slider does not exist, please verify."
				 End If
	On Error GoTo 0
End Function

'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name				:	Q_DragPage ()
' 	Purpose						:	Drags the slider the specified number of pages.
' 	Return Value				:   Integer( 0/1)
' 	Arguments					:
'		Input Arguments			:  	objSlider,intDrgPge
'		Output Arguments		:   
'	Function is called by		:
'	Function calls				:   CheckObjectExist()
'	Created on					:   16/08/2016
'	Author						:	Qualitia
'
'---------------------------------------------------------------------------------------------------------------------------------

Function Q_DragPage(ByVal objSlider,ByVal intDrgPge)
	On Error Resume Next
		 'Variable Section Begin
		
			  Dim strMethod
			  Dim intMin,intMax
			  
		If not isnumeric(intDrgPge) then
			Q_DragPage = 1  
			WriteStatusToLogs "Invalid parameter."
			exit function
		End If

		 'Variable Section End
		 strMethod = strKeywordForReporting
				 If CheckObjectExist(objSlider) Then
				 
 					 intMin = objSlider.GetRoProperty("min")                    
					 intMax = objSlider.GetRoProperty("max")						 
						 
						If  VarType(intMin)<>VbEmpty And VarType(intMax)<>VbEmpty Then
						     intMin=cdbl(intMin)
							 intMax=cdbl(intMax)
							   
								 If cdbl(intDrgPge) >= intMin And cdbl(intDrgPge) <= intMax Then
									 objSlider.DragPage intDrgPge
									 If err.number = 0 Then
										 Q_DragPage  = 0
										 WriteStatusToLogs "Slider is draged to the given page as expected."
									Else
										Q_DragPage  = 1
										 WriteStatusToLogs "Error occurred. "& err.description
									End if
									
								 Else
									Q_DragPage = 2  
									WriteStatusToLogs "Parameter beyond valid page range, please verify."
								 End If                                 
								 
						Else
							Q_DragPage = 1
							WriteStatusToLogs  "Verify the Slider Range values."						
						End if

				 Else
					Q_DragPage = 1
					WriteStatusToLogs "Slider does not exist, please verify."
				 End If
	On Error GoTo 0
End Function
