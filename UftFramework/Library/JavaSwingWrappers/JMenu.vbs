'Function SelectJMenuItem(ByVal objMenuControl)
'Function VerifySubMenuItemsInJMenu(ByVal objMenu, ByVal arrExpItems)


'=======================Code Section Begin=============================================

Option Explicit 

'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name :				  SelectJMenuItem()
' 	Purpose :						 Selects a specified item from the Menu
' 	Return Value :		 		   Integer( 0/1)
' 	Arguments :
'		Input Arguments :  		objMenuControl
'		Output Arguments :     
'	Function is called by :
'	Function calls :				CheckObjectExist()
'	Created on :				   18/06/2009
'	Author :						 Vaibhav Kelkar
' 
'------------------------------------------------------------------------------------------------------------------------------------------
Function SelectJMenuItem(ByVal objMenuControl)
	On Error Resume Next
		'Variable Section Begin
		Dim strMethod 

		'Variable Section End
		strMethod = strKeywordForReporting
		 If CheckObjectExist(objMenuControl) Then
				If CheckJavaObjectenabled(objMenuControl) Then			
						objMenuControl.Select
						If err.Number = 0 Then
							 SelectJMenuItem = 0
							 WriteStatusToLogs "The MenuItem was selected successfully."
						Else
							SelectJMenuItem = 1
							WriteStatusToLogs "The MenuItem was not selected successfully. Please verify."
					  
						End If
				Else
						SelectJMenuItem = 1
						WriteStatusToLogs "The MenuItem was not enabled. Please verify."

				End If
		 Else
				SelectJMenuItem = 1
				WriteStatusToLogs "The MenuItem does not exist. Please verify."
			
		 End If
			
		On Error GoTo 0
End Function

'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name			:   VerifySubMenuItemsInJMenu()
' 	Purpose					:	Verifies the Menu items with our expected
' 	Return Value			:	Integer( 0/1/2)
' 	Arguments				:
'		Input Arguments		:  	objMenu,arrExpItems
'		Output Arguments	:   strActItems , arrActItems() , arrExpItems()
'	Function is called by	:
'	Function calls			:	CheckObjectExist() , GetArray() , MatchEachArrData()
'	Created on				:	18/08/2009
'	Author					:	Vidyasagar 
'
'------------------------------------------------------------------------------------------------------------------------------------------

Function VerifySubMenuItemsInJMenu(ByVal objMenu, ByVal arrExpItems)
	On Error Resume Next
		'Variable Section Begin
			
			Dim strMethod
			Dim strActItems , arrActItems, arrItems()
			Dim intItemsCount,intCnt,intIndex,objDesc,strobj
				intIndex=0			
			
		'Variable Section End
		strMethod = strKeywordForReporting
			If CheckObjectExist(objMenu)  Then

				Set objDesc=Description.Create()
				objDesc("to_class").value="JavaMenu"
				Set strobj=objMenu.ChildObjects(objDesc)
                intItemsCount = strobj.Count

				For intCnt=0  to intItemsCount-1
					strActItems = strobj(intCnt).GetROProperty("label")
					ReDim Preserve arrItems(intIndex)
					arrItems(intIndex) = Trim(strActItems)
					intIndex=intIndex+1 
				Next  				
				If Err.number = 0 And intItemsCount <> 0 then
					If not IsEmpty(arrItems) Then
					   arrActItems = arrItems
							   If IsArray(arrExpItems) Then
										If MatchEachArrData(arrExpItems, arrActItems) = True Then
											VerifySubMenuItemsInJMenu = 0
											WriteStatusToLogs "The specified Menu items match with the expected Menu item."
										Else
											VerifySubMenuItemsInJMenu = 2
											WriteStatusToLogs "The specified Menu items does not match the expected Menu item."
										End If
							   Else
								   VerifySubMenuItemsInJMenu = 1
								   WriteStatusToLogs "The arrExpItems is not an array. Please verify."
							   End If					
					Else
						VerifySubMenuItemsInJMenu = 1
						WriteStatusToLogs "Menu does not contain any item. Please verify."
					End If

				Else
					VerifySubMenuItemsInJMenu = 1
					WriteStatusToLogs "Could not retrieve all the items of the Menu."
				End if
			Else
			      VerifySubMenuItemsInJMenu = 1
				  WriteStatusToLogs "The Window does not exist. Please verify."
			End If			
    On Error GoTo 0
End Function



'=======================Code Section End=============================================

