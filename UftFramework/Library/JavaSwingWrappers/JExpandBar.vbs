
'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name :				 CollapseJExpandBar
' 	Purpose :					  Collapses the specified bar.
'	Return Value :		 		  Integer( 0/1)
' 	Arguments :
'		Input Arguments :  		  objJExpandBar,arrCollapseExpandBar
'		Output Arguments : 
'	Function is called by :
'	Function calls :			  CheckObjectExist()
'	Created on :				  29/06/2016
'	Author :					   Qualitia
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function CollapseJExpandBar(ByVal objJExpandBar , ByVal arrCollapseExpandBar)
     On Error Resume Next
        'Variable Section Begin
            Dim strMethod
             Dim strJValidExpandBar
           
        'Variable Section End    
        strMethod = strKeywordForReporting
            strJEBarString = ""
                     
            If CheckObjectExist(objJExpandBar) Then
				If Not IsNull(arrCollapseExpandBar) or Not Isempty(arrCollapseExpandBar)Then
                    If  IsArray(arrCollapseExpandBar) Then
                                    For intCnt =0 to ubound(arrCollapseExpandBar)
										strJEBarString =  strJEBarString&  arrCollapseExpandBar(intCnt) & ";"  										
									Next

								strJEBarString = Left(strJEBarString , Len(strJEBarString) - 1)

      					Elseif not Isobject(arrCollapseExpandBar) Then 
								
								strJEBarString = arrCollapseExpandBar
								
						End If
							 objJExpandBar.Collapse strJEBarString
							 intErrNo = err.numbers
                        		If intErrNo=0 Then
                            	CollapseJExpandBar = 0
								WriteStatusToLogs "Collapse Operation successfully."
                        Else
								CollapseJExpandBar = 1
								WriteStatusToLogs "An error occurred in the action 'CollapseJExpandBar' "	   
						End If
                                   			
                Else
					CollapseJExpandBar = 1
					WriteStatusToLogs "The specified Label of bar item is invalid, please verify."  
                    End IF    
            Else
                CollapseJExpandBar = 1
                WriteStatusToLogs "The Expand bar Object  does not exist, please verify."
            End If
        On Error GoTo 0
End Function


'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name :				 ExpandJExpandBar
' 	Purpose :					 Expand the specified bar.
'	Return Value :		 		  Integer( 0/1)
' 	Arguments :
'		Input Arguments :  		  objJExpandBar,arrCollapseExpandBar
'		Output Arguments : 
'	Function is called by :
'	Function calls :			  CheckObjectExist()
'	Created on :				  29/06/2016
'	Author :					   Qualitia
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function ExpandJExpandBar(ByVal objJExpandBar , ByVal arrCollapseExpandBar)
     On Error Resume Next
        'Variable Section Begin
            Dim strMethod
            Dim strJValidExpandBar
           
        'Variable Section End    
        strMethod = strKeywordForReporting
            strJEBarString = ""
                     
            If CheckObjectExist(objJExpandBar) Then
				If Not IsNull(arrCollapseExpandBar) or Not Isempty(arrCollapseExpandBar)Then
                    If  IsArray(arrCollapseExpandBar) Then
                                    For intCnt =0 to ubound(arrCollapseExpandBar)
										strJEBarString =  strJEBarString&  arrCollapseExpandBar(intCnt) & ";"  										
									Next

								strJEBarString = Left(strJEBarString , Len(strJEBarString) - 1)

      					Elseif not Isobject(arrCollapseExpandBar) Then 
								
								strJEBarString = arrCollapseExpandBar
								
						End If
							 objJExpandBar.Expand strJEBarString
							 intErrNo = err.numbers
                        		If intErrNo=0 Then
                            	ExpandJExpandBar = 0
								WriteStatusToLogs "Expand Operation successfully."
                        Else
								ExpandJExpandBar = 1
								WriteStatusToLogs "An error occurred in the action 'ExpandJExpandBar' "	   
						End If
                                   			
                    Else
						ExpandJExpandBar = 1
								WriteStatusToLogs "The specified Label of bar item is invalid, please verify."  
                    End IF    
            Else
                ExpandJExpandBar = 1
                WriteStatusToLogs "The Expand bar Object  does not exist, please verify."
            End If
        On Error GoTo 0
End Function

'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name :				 StoreJExpandBarLabel
' 	Purpose :					 store the specified bar label
'	Return Value :		 		  Integer( 0/1)
' 	Arguments :
'		Input Arguments :  		  objJExpandBar,strKey,intIndexNum
'		Output Arguments : 
'	Function is called by :
'	Function calls :			  CheckObjectExist()
'	Created on :				  29/06/2016
'	Author :					   Qualitia
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function StoreJExpandBarLabel(Byval objJExpandBar, Byval strKey,Byval intIndexNum)
		On Error Resume Next
		
			'Variable Section Begin
			
				Dim	strMethod
                Dim intEBarLabel
				
			'Variable Section End
			
		strMethod = strKeywordForReporting
		
	If Not Isempty(strKey) or Not isnull(strKey)  or trim(strKey)="" Then
           	If Not IsNull(intIndexNum) or Not IsEmpty(intIndexNum) Then
                           
				If CheckObjectExist(objJExpandBar)  Then 

                        strEBarLabel = objJExpandBar.GetItem(intIndexNum)
						AddItemToStorageDictionary strKey , strEBarLabel
											
						If err.number =0  Then
                        StoreJExpandBarLabel = 0
						WriteStatusToLogs "The Label '"&strEBarLabel&"' of specified index bar '"&intIndexNum&"' is stored in '"&strKey&"' successfully."
					Else
						StoreJExpandBarLabel = 1
						WriteStatusToLogs "The Label '"&strEBarLabel&"' of specified index bar '"&intIndexNum&"' is not able to store in '"&strKey&"', please verify."
					End if
                Else
                         StoreJExpandBarLabel = 1
                         WriteStatusToLogs "The Expand bar Object  does not exist, please verify."
                  End If
            Else
					StoreJExpandBarLabel = 1
					WriteStatusToLogs "The specified Label of bar item is invalid, please verify."  
									   
                    End IF

         Else
			StoreJExpandBarLabel = 1
			WriteStatusToLogs "The key passed is invalid. Please verify."
		End if
On Error GoTo 0
End Function
