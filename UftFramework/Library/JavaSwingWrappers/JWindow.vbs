''Function VerifyJWindowExist(ByVal objWindow)
''Function VerifyJWindowNotExist(ByVal objWindow)
''Function SelectJMenuInWindow(Byval objWindow,Byval arrMenuValues)
''Function VerifyJWindowTitle(ByVal objWindow , ByVal strExpTitle)


'========================Code Section Begin=====================================================================
Option Explicit	'Forces explicit declaration of all variables in a script.

'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :				 VerifyJWindowExist()
'	Purpose :					 Verifies the existance of Window in AUT.
'	Return Value :		 		  Integer( 0/1)
'	Arguments :
'		Input Arguments :  		  objWindow 
'		Output Arguments : 
'	Function is called by :
'	Function calls :			 CheckObjectExist() 
'	Created on :				      15/01/2009
'	Author :						  Rathna Reddy 
'
'------------------------------------------------------------------------------------------------------------------------------------------

Function VerifyJWindowExist(ByVal objWindow)
	On Error Resume Next
		'Variable Section Begin
	
		 Dim strMethod

		'Variable Section End

		strMethod = strKeywordForReporting

			If CheckObjectExist(objWindow) Then 
			   
					VerifyJWindowExist = 0
					WriteStatusToLogs "The Window exists as expected."
					
			Else
					VerifyJWindowExist = 1
					WriteStatusToLogs "The Window does not exist, please verify"
					
			End If 
	  
	
	On Error GoTo 0
End Function

'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :				 VerifyJWindowNotExist()
'	Purpose :					 Verifies whether the Window is NotExist or not in AUT?.
'	Return Value :		 		  Integer( 0/1)
'	Arguments :
'		Input Arguments :  		  objWindow 
'		Output Arguments : 
'	Function is called by :
'	Function calls :			 CheckObjectExist() 
'	Created on :				   15/01/2009
'	Author :						  Rathna Reddy 
'
'------------------------------------------------------------------------------------------------------------------------------------------

Function VerifyJWindowNotExist(ByVal objWindow)
	On Error Resume Next
		'Variable Section Begin
	
		   Dim strMethod

		'Variable Section End

		strMethod = strKeywordForReporting
		
			If  CheckObjectNotExist(objWindow) Then 
			   
					VerifyJWindowNotExist = 0
					WriteStatusToLogs "The Window does not exist as expected."
					
			Else
					VerifyJWindowNotExist = 1
					WriteStatusToLogs "The Window exists, please verify."
					
			End If 
	
  		
	On Error GoTo 0
End Function

'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name             :		SelectJMenuInWindow()
' 	Purpose                   :		Selects a specified item from the MenuList
' 	Return Value              :		Integer( 0/1)
' 	Arguments                 :
'		Input Arguments       :  	objMenuControl , arrMenuValues
'		Output Arguments      :     strActItem
'	Function is called by     :
'	Function calls            :		CheckObjectExist()
'	Created on                :		30/06/2009
'	Author                    :		Vidyasagar
'------------------------------------------------------------------------------------------------------------------------------------------

Function SelectJMenuInWindow(Byval objWindow,Byval arrMenuValues)
    On Error Resume Next

   'Variable Section Begin
    Dim strMethod
	Dim strJMenu,strJWindow,strJWName,strHrchy,strMenuHrchy,strObjRq,strObjRqForm,strlen
	Dim intUbound,i,blnstatus
	
	'Variable Section End

	strMethod = strKeywordForReporting

	blnstatus = false	
    strJMenu="JavaMenu"
    strJWindow="JavaWindow"

	If CheckObjectExist(objWindow) Then	   		
		strJWName=objWindow.GetROProperty("label")
		intUbound=Ubound(arrMenuValues)

			If  Not IsNull(intUbound)Then
				blnstatus = true
			End If

				If blnstatus Then
						For i=0 to intUbound
							strMenuHrchy=strJMenu & Chr(40) & chr(34) & "label" & ":=" &  arrMenuValues(i)  & chr(34) & Chr(41) & Chr(46)
							strHrchy=strHrchy &  strMenuHrchy
						Next
						
						strObjRq=strJWindow & Chr(40) &  chr(34) & strJWName & chr(34) & Chr(41) & Chr(46) &  strHrchy	 
						strlen=len(strObjRq)
						strObjRqForm=Mid(strObjRq,1,strlen-1)
						Eval(strObjRqForm).Select

						If Err.Number = 0 Then
						   SelectJMenuInWindow = 0
						   WriteStatusToLogs "The Java Menu selection is done as expected."
						Else
						   SelectJMenuInWindow = 1
						   WriteStatusToLogs "The Java Menu selection Failed, please verify."
						End If

				Else
					SelectJMenuInWindow = 1
					WriteStatusToLogs "Java Menu does not contain any Menu Items"
				End If  'blnstatus

	Else
		SelectJMenuInWindow = 1
		WriteStatusToLogs "The JavaWindow Object does not exist, please verify."
	End If

End Function

'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name				:	VerifyJWindowTitle()
' 	Purpose						:	Verify the Dialog title with the expected
' 	Return Value				:   Integer( 0/1)
' 	Arguments					:
'		Input Arguments			:  	objWindow,strExpTitle
'		Output Arguments		:   strActTitle
'	Function is called by		:
'	Function calls				:   CheckObjectExist() , GetArray() 
'	Created on					:   17/08/2009
'	Author						:	Vidyasagar
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function VerifyJWindowTitle(ByVal objWindow , ByVal strExpTitle)
	On Error Resume Next
		'Variable Section Begin	
			Dim strMethod
			Dim strActTitle		
		'Variable Section End

		strMethod = strKeywordForReporting

		If CheckObjectExist(objWindow)  Then
			strActTitle = objWindow.GetROProperty("title")
				If CompareStringValues(strExpTitle,strActTitle)=0 Then
					VerifyJWindowTitle = 0
					WriteStatusToLogs "The Window title is same as the expected."
				Else
					VerifyJWindowTitle = 2
					WriteStatusToLogs "The Window title is different from the expected, please verify."
				End If
		Else
			VerifyJWindowTitle = 1
			WriteStatusToLogs "The specified  Window doesn't exist, please Verify."
		End If			
    On Error GoTo 0
End Function


'----------------------------------------------------------------------------------------------------------------

' 	Function Name :				Q_Activate()
' 	Purpose :					 Activate the specified Java Window
' 	Return Value :		 		
' 	Arguments :
'		Input Arguments :  	   objObject
'		Output Arguments : 
'	Function calls :
'	Created on :				 10/08/2016
'	Author :					   Qualitia
'	Comment:                    Added Q_Activate keyword
'------------------------------------------------------------------------------------------------------------------------------------------
Function Q_Activate(ByVal objObject)
	On Error Resume Next
	
	'Variable Section Begin
	 Dim objexistence, strMethod
	'Variable Section End
	strMethod = strKeywordForReporting
	
	If CheckObjectExist(objObject) Then 
	
					objObject.Activate
					
					If Err.Number = 0 Then
						Q_Activate=0
						WriteStatusToLogs "Object activated."
					Else
						Q_Activate = 1
						WriteStatusToLogs "Object could not be activated."
					End If
					
		 Else
			Q_Activate = 1
		    WriteStatusToLogs "The '"& objObject &"' does not exist, please verify."
		End If			
		 
	On Error Goto 0
End Function
'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name				:	TypeKeys()
' 	Purpose						:	Types the keys
' 	Return Value				:   Integer( 0/1)
' 	Arguments					:
'		Input Arguments			:  	objWindow,strkeys
'		Output Arguments		:   
'	Function is called by		:
'	Function calls				:   CheckObjectExist()
'	Created on					:   16/03/2011
'	Author						:	Iohbha
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function TypeKeys(ByVal objWindow , byval strkeys)
	On Error Resume Next
		'Variable Section Begin
	
		   Dim strMethod

		'Variable Section End

		strMethod = strKeywordForReporting

			If CheckObjectExist(objWindow) Then 

					If IsNull(strkeys) Then
							TypeKeys = 1
							 WriteStatusToLogs "The key(s) is invalid "&strkeys	  
					End If
					
					objWindow.Activate
					objWindow.Type strkeys
					
					If err.number=0 Then
							TypeKeys = 0
							 WriteStatusToLogs "The keys "&strkeys&" are typed"
					else
							TypeKeys = 1
							 WriteStatusToLogs "Failed to type the keys "&strkeys&". Please verify. "&err.description	  
					End If
					
			Else
					TypeKeys = 1
					WriteStatusToLogs "The Window does not exist, please verify"
					
			End If 
	
	On Error GoTo 0
End Function

'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name				:	Q_TypeKeys()
' 	Purpose						:	Types the keys
' 	Return Value				:   Integer( 0/1)
' 	Arguments					:
'		Input Arguments			:  	objWindow,strkeys
'		Output Arguments		:   
'	Function is called by		:
'	Function calls				:   CheckObjectExist()
'	Created on					:   16/03/2011
'	Author						:	Iohbha
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function Q_TypeKeys(ByVal objWindow , byval strkeys)
	On Error Resume Next
		'Variable Section Begin
	
		   Dim strMethod

		'Variable Section End

		strMethod = strKeywordForReporting

			If CheckObjectExist(objWindow) Then 

					If IsNull(strkeys) Then
							Q_TypeKeys = 1
							 WriteStatusToLogs "The key(s) is invalid "&strkeys	  
					End If
					
					objWindow.Activate
					objWindow.Type strkeys
					
					If err.number=0 Then
							Q_TypeKeys = 0
							 WriteStatusToLogs "The keys "&strkeys&" are typed"
					else
							Q_TypeKeys = 1
							 WriteStatusToLogs "Failed to type the keys "&strkeys&". Please verify. "&err.description	  
					End If
					
			Else
					Q_TypeKeys = 1
					WriteStatusToLogs "The Window does not exist, please verify"
					
			End If 
	
	On Error GoTo 0
End Function

'=======================Code Section End======================================================================

