
'Functions List

''Function ClickOnJToolITemName(ByVal objtool , ByVal strlItemName)
''Function ClickOnJToolITemByIndex(ByVal objtool , ByVal strToolIndex)
''Function VerifyJToolbarItemNotExisting(ByVal objtool , ByVal strlItemName)
''Function VerifyJToolbarItemExisting(ByVal objtool , ByVal strlItemName)
''Function VerifyJToolbarVisible(ByVal objtool)
''Function VerifyJToolbarInVisible(ByVal objtool)
''Function VerifyJToolbarItemEnabled(ByVal objtool, byval ItemName)
''Function VerifyJToolbarItemDisabled(ByVal objtool, byval ItemName)
''Function VerifyJToolbarItemSelected(ByVal objtool, byval ItemName)
''Function VerifyJToolbarItemNotSelected(ByVal objtool, byval ItemName)


'=======================Code Section Begin=============================================

Option Explicit
'
'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name             :		ClickOnJToolITemName()
' 	Purpose                   :		Clicks a specified item of Toolbar by its Name
' 	Return Value              :		Integer( 0/1)
' 	Arguments                 :
'		Input Arguments       :  	objtool , strlItemName
'		Output Arguments      :     strActItem
'	Function is called by     :
'	Function calls            :		CheckObjectExist()
'	Created on                :		30/06/2009
'	Author                    :		Vidyasagar
'------------------------------------------------------------------------------------------------------------------------------------------
Function ClickOnJToolITemName(ByVal objtool , ByVal strlItemName)
	On Error Resume Next
		'Variable Section Begin
			Dim strMethod
			Dim blnstatus, blnMatch, blnCount, blnenabled
			Dim  strTemplistItems, strFindToollItem, strenanabled
			Dim arrListContents
			Dim intmaxlength, intcount, intitemsCount, intItemIndex
					'Variable Section End
					strMethod = strKeywordForReporting
			
			blnstatus = false
			blnenabled =False
			intmaxlength  = 0
			intItemIndex = 1       

			If CheckObjectExist(objtool) Then
			strFindToollItem = GetItemFromJavaObject(objtool, strlItemName)	

				If  Not IsNull(strFindToollItem)Then
					blnstatus = true
				End If
					If blnstatus  Then
								strenanabled = GetObjectItemPropertyName(objtool, strFindToollItem, "enabled")
								If Not IsNull(strenanabled) Then
									blnenabled =True
								End If
									'	 If objtool.GetItemProperty(intItemIndex, "enabled")  Then
									If  blnenabled Then
                                        objtool.Press strlItemName
											 If Err.Number = 0 Then
												ClickOnJToolITemName = 0
												Else
												ClickOnJToolITemName = 1
											End If
								 Else
									 ClickOnJToolITemName = 1
									 WriteStatusToLogs "The Toolbar item is not enabled."
								 End If
						Else'blnstatus
							ClickOnJToolITemName = 1
							WriteStatusToLogs "An error occurred while Click operation in Toolbar."
						End If
			Else
				ClickOnJToolITemName = 1
				WriteStatusToLogs "The Toolbar Object  does not exist, please verify."
			End If
		On Error GoTo 0
End Function

'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name			:	ClickOnJToolITemByIndex()
' 	Purpose					:	Clicks on a specified item of Toolbar by its Index value
' 	Return Value			:	Integer( 01)
' 	Arguments				:
'		Input Arguments		:  	objtool,strToolIndex
'		Output Arguments	:   strActItems , arrActItems() , arrExpItems()
'	Function is called by	:
'	Function calls			:	CheckObjectExist() , GetArray() , MatchEachArrData(),GetItemProperty()
'	Created on				:	30/06/2009
'	Author					:	Vidyasagar
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function ClickOnJToolITemByIndex(ByVal objtool , ByVal strToolIndex)

	On Error Resume Next
		'Variable Section Begin
			Dim strMethod
			Dim blnstatus, blnMatch
			Dim strListContent, strTemplistItems, strTempNode
			Dim arrListContents
			Dim intmaxlength, intcount, intindex
			
		  
			blnstatus = false
			blnMatch = False
			strListContent = Null
			
		'Variable Section End
		strMethod = strKeywordForReporting
			If CheckObjectExist(objtool) Then
			   strListContent = objtool.GetContent
			   intindex = Cint(strToolIndex)             

					If Vartype(strListContent) <> vbNull  And Err.Number = 0 Then
						blnstatus = true
					End If
					If blnstatus  Then
					   arrListContents = GetArray(strListContent, ";") 'Modified by Vidya
					   intmaxlength = ubound(arrListContents)

								If intindex <= intmaxlength+1 Then
								
									 If objtool.GetItemProperty(intindex, "enabled")  Then
										objtool.press intindex

											If Err.Number = 0  Then
											   ClickOnJToolITemByIndex = 0
											Else
											   ClickOnJToolITemByIndex = 1
											   WriteStatusToLogs "Error occurred during Toolbar ['"& intindex &"'] select operation."
											End If
									 Else
										 ClickOnJToolITemByIndex = 1
										 WriteStatusToLogs "The Toolbar item is not enabled."
									 End IF
								Else'blnMatch
									ClickOnJToolITemByIndex = 1
									WriteStatusToLogs "The Expand Items are not existing."&_
															  "Actual Array items:"&PrintArrayElements (arrListContents)
								End If   		   
					Else
						ClickOnJToolITemByIndex = 1
						WriteStatusToLogs "The expected index value is more than the Actual index value."&_
															"The expected index value is:["&strToolIndex&"] The Actual index value is:["&intmaxlength&"]"
					End If

			Else
				ClickOnJToolITemByIndex = 1
				WriteStatusToLogs "The Toolbar Object  does not exist, please verify."
			End If
		On Error GoTo 0
End Function

'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name              :	VerifyJToolbarItemNotExisting
' 	Purpose                    :	Verifies whether the Toolbar Item Not existing
'	Return Value               :	Integer( 0/1)
' 	Arguments                  :
'		Input Arguments        :  	objtool 
'		Output Arguments       :  
'	Function is called by      :
'	Function calls             :	CheckObjectExist() , CheckObjectStatus()
'	Created on                 :	30/06/2009
'	Author                     :	Vidyasagar
'
'------------------------------------------------------------------------------------------------------------------------------------------

Function VerifyJToolbarItemNotExisting(ByVal objtool , ByVal strlItemName)
   On Error Resume next
		'Variable Section Begin
			Dim strMethod
			Dim blnstatus, blnMatch, blnCount, blnenabled
			Dim  strTemplistItems, strFindToollItem, strenanabled
			Dim arrListContents
			
		'Variable Section End
		strMethod = strKeywordForReporting
			
			blnstatus = false
			blnenabled =False       

			If CheckObjectExist(objtool) Then
				strFindToollItem = GetItemFromJavaObject(objtool, strlItemName)	

				If  IsNull(strFindToollItem)Then
					blnstatus = true
				End If
					If blnstatus  Then
						VerifyJToolbarItemNotExisting= 0
						WriteStatusToLogs "The item "&strlItemName&" not found in toolbar."

					Else'blnstatus
						VerifyJToolbarItemNotExisting = 1
						WriteStatusToLogs "Item found in toolbar."
					End If
						
			Else'CheckObjectExist
				VerifyJToolbarItemNotExisting = 1
				WriteStatusToLogs "The Toolbar Object  does not exist, please verify."
			End If
		On Error GoTo 0
End Function

'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name            :	 VerifyJToolbarItemExisting
' 	Purpose                  :	 Verifies whether the ToolbarItem is existing or not
'	Return Value             :	 Integer( 0/1)
' 	Arguments :
'		    Input Arguments  :   objtool 
'		    Output Arguments : 
'	Function is called by    :
'	Function calls           :	 CheckObjectExist() , CheckObjectStatus()
'	Created on               :	 30/06/2009
'	Author                   :	 Vidyasagar
'
'---------------------------------------------------------------------------

Function VerifyJToolbarItemExisting(ByVal objtool , ByVal strlItemName)
   On Error Resume next
		'Variable Section Begin
			Dim strMethod
			Dim blnstatus, blnMatch, blnCount, blnenabled
			Dim  strTemplistItems, strFindToollItem, strenanabled
			Dim arrListContents
			
		'Variable Section End
		strMethod = strKeywordForReporting
			
			blnstatus = false
			blnenabled =False       

			If CheckObjectExist(objtool) Then
				strFindToollItem = GetItemFromJavaObject(objtool, strlItemName)	

				If  Not IsNull(strFindToollItem)Then
					blnstatus = true
				End If
					If blnstatus  Then
						If CompareStringValues(strlItemName,strFindToollItem) = 0 Then
							VerifyJToolbarItemExisting =0
						Else
							VerifyJToolbarItemExisting =1
						End If
						
					Else'blnstatus
						VerifyJToolbarItemExisting =1
						WriteStatusToLogs "Item " &strlItemName& " is not found in toolbar."'"An error occurred while Click operation in Toolbar."
					End If			
			Else'CheckObjectExist
				VerifyJToolbarItemExisting =1    
				WriteStatusToLogs "The Toolbar Object  does not exist, please verify."
			End If
		On Error GoTo 0
End Function


'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name			: VerifyJToolbarVisible()
'	Purpose					: Verifies whether the Toolbar is visible.
'	Return Value			: Integer( 0/1)
'	Arguments				:
'		Input Arguments		: objtool 
'		Output Arguments	: 
'	Function is called by	:
'	Function calls			: CheckObjectExist() , CheckObjectVisiblity()
'	Created on				: 30/06/2009
'	Author					: Vidyasagar
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function VerifyJToolbarVisible(ByVal objtool)
	On Error Resume Next
		'Variable Section Begin

		Dim strMethod 

		'Variable Section End
		strMethod = strKeywordForReporting
		
			If CheckObjectExist(objtool)  Then 

				If CheckJavaObjectVisible(objtool) Then
					VerifyJToolbarVisible = 0
					WriteStatusToLogs "The Toolbar is visible as expected."
				Else
					VerifyJToolbarVisible = 2
					WriteStatusToLogs "The Toolbar was not visible, please verify."
				End If
			Else
				VerifyJToolbarVisible = 1
				WriteStatusToLogs "The Toolbar does not exist, please verify."
			End If
		
	On Error GoTo 0
End Function
'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name			:  VerifyJToolbarInVisible()
'	Purpose					:  Verifies whether the Toolbar is Invisible.
'	Return Value			:  Integer( 0/1)
'	Arguments				:
'		Input Arguments		:  objtool 
'		Output Arguments	: 
'	Function is called by	:
'	Function calls			:  CheckObjectExist() , CheckObjectVisiblity()
'	Created on				:  01/07/2009
'	Author					:  Vidyasagar
'
'------------------------------------------------------------------------------------------------------------------------------------------

Function VerifyJToolbarInVisible(ByVal objtool)
	On Error Resume Next
		'Variable Section Begin
		
			Dim strMethod

		'Variable Section End
		strMethod = strKeywordForReporting
			If CheckObjectExist(objtool)  Then
			
				If CheckJavaObjectInVisible(objtool) Then
					VerifyJToolbarInVisible = 0
					WriteStatusToLogs "The toolbar is not visible."
				Else
					VerifyJToolbarInVisible = 2
					WriteStatusToLogs "The toolbar is visible."
				End If

			Else
				VerifyJToolbarInVisible = 1
				WriteStatusToLogs "The toolbar does not exist, please verify."
			End If
		
	On Error GoTo 0
End Function

'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name             :		VerifyJToolbarItemEnabled()
'	Purpose                   :		Verifies whether the Toolbar Item is enabled
'	Return Value              :		Integer( 0/1)
'	Arguments                 :
'		Input Arguments       :  	objtool 
'		Output Arguments      : 
'	Function is called by     :
'	Function calls            :		CheckObjectExist() , CheckObjectVisiblity()
'	Created on                :		30/06/2009
'	Author                    :		Vidyasagar
'
'------------------------------------------------------------------------------------------------------------------------------------------
'VerifyToolbarItemEnabled
Function VerifyJToolbarItemEnabled(ByVal objtool, byval ItemName)
	On Error Resume Next
		'Variable Section Begin
		Dim strMethod
		Dim blnpropertyvalue, blnfind
		Dim tempItemName		

		'Variable Section End
		strMethod = strKeywordForReporting
		blnfind = False
		
			If CheckObjectExist(objtool)  Then 
				'blnfind = IsItemFindInObject(objtool, ItemName)
				tempItemName = GetItemFromJavaObject(objtool, ItemName)
				If Not IsNull(tempItemName) Then
					blnfind =True
				End If
					If  blnfind Then
						 blnpropertyvalue = GetObjectItemPropertyName(objtool, tempItemName, "enabled")
						 'blnpropertyvalue = GetObjectItemPropertyName(objtool, tempItemName, "Visible")

								If Not IsNull(blnpropertyvalue) Then
										If  Not blnpropertyvalue Then
											VerifyJToolbarItemEnabled = 0
											WriteStatusToLogs "The Toolbar was visible as expected."
										Else
											VerifyJToolbarItemEnabled = 2
											WriteStatusToLogs "The Toolbar was not enabled, please verify."
										End If
								Else'Nulll
									VerifyJToolbarItemEnabled = 1
									WriteStatusToLogs "The Toolbar was not enabled, please verify."
								End If
					Else
						VerifyJToolbarItemEnabled = 1
					End If
			 Else
				 VerifyJToolbarItemEnabled = 1
				 WriteStatusToLogs "The Toolbar does not exist, please verify."
			 End If
		
	On Error GoTo 0
End Function
'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name              :	 VerifyJToolbarItemDisabled()
'	Purpose                    :	 Verifies whether the Toolbar is disabled
'	Return Value               :	 Integer( 0/1)
'	Arguments                  :
'		Input Arguments        :  	 objtool 
'		Output Arguments       : 
'	Function is called by      :
'	Function calls             :	 CheckObjectExist() , GetItemFromJavaObject()
'	Created on                 :	 30/06/2009
'	Author                     :	 Vidyasagar
'
'------------------------------------------------------------------------------------------------------------------------------------------

Function VerifyJToolbarItemDisabled(ByVal objtool, byval ItemName)
	On Error Resume Next
		'Variable Section Begin
		Dim strMethod
		Dim blnpropertyvalue, blnfind
		Dim tempItemName		

		'Variable Section End
		strMethod = strKeywordForReporting
		blnfind = False
		
			If CheckObjectExist(objtool)  Then 
				'blnfind = IsItemFindInObject(objtool, ItemName)
				tempItemName = GetItemFromJavaObject(objtool, ItemName)
				If Not IsNull(tempItemName) Then
					blnfind =True
				End If
				
					If  blnfind Then
						 blnpropertyvalue = GetObjectItemPropertyName(objtool, tempItemName, "enabled")
							If Not IsNull(blnpropertyvalue) Then
									If  blnpropertyvalue Then
										VerifyJToolbarItemDisabled = 2
										WriteStatusToLogs "The Toolbar was Enabled, please verify."

									Else
										VerifyJToolbarItemDisabled = 0
										WriteStatusToLogs "The Toolbar was disabled as expected."
									End If
							Else'Nulll
								VerifyJToolbarItemDisabled = 1
								WriteStatusToLogs "The Toolbar was Enabled, please verify."
							End If
					Else
						VerifyJToolbarItemDisabled=1
					End If
			Else
				VerifyJToolbarItemDisabled = 1
				WriteStatusToLogs "The Toolbar does not exist, please verify."
			End If
		
	On Error GoTo 0
End Function

'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name              :	VerifyJToolbarItemSelected()
'	Purpose                    :	Verifies whether the Toolbar Item selected
'	Return Value               :	Integer( 0/1)
'	Arguments                  :
'		Input Arguments        :  	objtool 
'		Output Arguments       : 
'	Function is called by      :
'	Function calls             :	CheckObjectExist() , GetItemFromJavaObject()
'	Created on                 :	30/06/2009
'	Author                     :	Vidyasagar
'
'------------------------------------------------------------------------------------------------------------------------------------------
'VerifyToolbarItemSelected
Function VerifyJToolbarItemSelected(ByVal objtool, byval ItemName)
	On Error Resume Next
		'Variable Section Begin
		Dim strMethod
		Dim blnpropertyvalue, blnfind
		Dim tempItemName		

		'Variable Section End
		strMethod = strKeywordForReporting
		 blnfind = False
		
			If CheckObjectExist(objtool)  Then			
				'blnfind = IsItemFindInObject(objtool, ItemName)
				tempItemName = GetItemFromJavaObject(objtool, ItemName)
				If Not IsNull(tempItemName) Then
					blnfind =True
				End If
					If  blnfind Then
						 blnpropertyvalue = GetObjectItemPropertyName(objtool, tempItemName, "selected")
						 'blnpropertyvalue = GetObjectItemPropertyName(objtool, tempItemName, "Visible")
								If Not IsNull(blnpropertyvalue) Then

										If  Not blnpropertyvalue Then
											 VerifyJToolbarItemSelected = 0
											 WriteStatusToLogs "The Toolbar was Selected as expected."
										Else
											VerifyJToolbarItemSelected = 2
											WriteStatusToLogs "The Toolbar was not selected, please verify."
										End If
								Else'Nulll
									VerifyJToolbarItemSelected = 1
									WriteStatusToLogs "The Toolbar was not selected, please verify."
								End If
					Else
						VerifyJToolbarItemSelected = 1
					End If
			Else
				VerifyJToolbarItemSelected = 1
				WriteStatusToLogs "The Toolbar does not exist, please verify."
			End If
		
	On Error GoTo 0
End Function
'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name			:	 VerifyJToolbarItemNotSelected()
'	Purpose					:	 Verifies whether the EditBox is visible.
'	Return Value			:	 Integer( 0/1)
'	Arguments				:
'		Input Arguments		:  	 objtool 
'		Output Arguments	: 
'	Function is called by	:
'	Function calls			:	 CheckObjectExist(),GetItemFromJavaObject()
'	Created on				:	 30/06/2009
'	Author					:	 Vidyasagar
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function VerifyJToolbarItemNotSelected(ByVal objtool, byval ItemName)
	On Error Resume Next
		'Variable Section Begin
		Dim strMethod
		Dim blnpropertyvalue, blnfind
		Dim tempItemName		

		'Variable Section End
		strMethod = strKeywordForReporting
		blnfind = False
		
			If CheckObjectExist(objtool)  Then 
				'blnfind = IsItemFindInObject(objtool, ItemName)
				tempItemName = GetItemFromJavaObject(objtool, ItemName)

					If Not IsNull(tempItemName) Then
						blnfind =True
					End If
				
					If  blnfind Then
						 blnpropertyvalue = GetObjectItemPropertyName(objtool, tempItemName, "selected")
							If Not IsNull(blnpropertyvalue) Then
									If  blnpropertyvalue Then
										 VerifyJToolbarItemNotSelected = 0
										 WriteStatusToLogs "The Toolbar was Selected as expected."
									Else
										VerifyJToolbarItemNotSelected = 2
										WriteStatusToLogs "The Toolbar was not selected, please verify."
									End If
							Else'Nulll
								VerifyJToolbarItemNotSelected = 1
								WriteStatusToLogs "The Toolbar was not selected, please verify."
							End If
					Else
						VerifyJToolbarItemNotSelected = 1
					End If
			Else
				VerifyJToolbarItemNotSelected = 1
				WriteStatusToLogs "The Toolbar does not exist, please verify."
			End If
		
	On Error GoTo 0
End Function

'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name             :		Q_StoreItemCount()
' 	Purpose                   :		Store Returns the number of buttons in the toolbar.
' 	Return Value              :		Integer( 0/1)
' 	Arguments                 :
'		Input Arguments       :  	objtool , strKey,intItem
'		Output Arguments      :     
'	Function is called by     :
'	Function calls            :		CheckObjectExist(),GetItemFromJavaObject(),GetObjectItemPropertyName()
'	Created on                :		30/06/2009
'	Author                    :		Qualitia
'------------------------------------------------------------------------------------------------------------------------------------------
Function Q_StoreItemCount(ByVal objtool ,ByVal strKey)
	On Error Resume Next
		'Variable Section Begin
			Dim strMethod,strtotalItem
			'Variable Section End
			strMethod = strKeywordForReporting
			
			If CheckObjectExist(objtool) Then
			
						If  Not isNull(strKey) or Not IsEmpty(strKey) or trim(strKey)="" then
						
							      		strtotalItem = objtool.GetItemsCount()
                                   		AddItemToStorageDictionary strKey , strtotalItem
									
											 If Err.Number = 0 Then
												Q_StoreItemCount = 0
												WriteStatusToLogs "Toolbar button counts '"&strtotalItem&"'stored in key '"&strKey&"' successfully."
												Else
												Q_StoreItemCount = 1
												WriteStatusToLogs "Not able to store toolbar count in key, please Verify"
											End If
						Else
						Q_StoreItemCount = 1
						WriteStatusToLogs "Key passed is null oe not valid, please verify."
						End If	
			Else
				Q_StoreItemCount = 1
				WriteStatusToLogs "The Toolbar Object  does not exist, please verify."
			End If
		On Error GoTo 0
End Function

'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name             :		Q_OpenDropDown()
' 	Purpose                   :		Opens the dropdown menu associated with the specified toolbar 
' 	Return Value              :		Integer( 0/1)
' 	Arguments                 :
'		Input Arguments       :  	objtool ,strlItemName
'		Output Arguments      :     
'	Function is called by     :
'	Function calls            :		CheckObjectExist(),GetItemFromJavaObject(),GetObjectItemPropertyName()
'	Created on                :		30/06/2009
'	Author                    :		Qualitia
'------------------------------------------------------------------------------------------------------------------------------------------

Function Q_OpenDropDown(ByVal objtool ,ByVal strlItemName)
	On Error Resume Next
		'Variable Section Begin
			Dim strMethod
			Dim blnstatus, blnMatch,blnenabled
			Dim strTemplistItems, strFindToollItem, strenanabled
			Dim strOpenItem
			
			'Variable Section End
			strMethod = strKeywordForReporting
			
			blnstatus = false
			blnenabled =False
			
			If CheckObjectExist(objtool) Then
			strFindToollItem = GetItemFromJavaObject(objtool, strlItemName)	
								
				If  Not IsNull(strFindToollItem)Then
					blnstatus = true
				End If
					If blnstatus  Then
					
								strenanabled = GetObjectItemPropertyName(objtool, strFindToollItem, "enabled")
								
								If Not IsNull(strenanabled) Then
									blnenabled =True
								End If
									If  blnenabled Then
                                   		objtool.ShowDropdown "#"&strlItemName                                   		                                      
											 If Err.Number = 0 Then
												Q_OpenDropDown = 0
												WriteStatusToLogs "DropDown menu associated Toolbar is open successfully"
												Else
												Q_OpenDropDown = 1
												WriteStatusToLogs "DropDown menu associated Toolbar could not open, please Verify"
											End If
								 Else
									 Q_OpenDropDown = 1
									 WriteStatusToLogs "The Toolbar item   is not enabled.."
								 End If
						Else'blnstatus
							Q_OpenDropDown = 1
							WriteStatusToLogs "An error occurred while open operation in Toolbar."
						End If
			Else
				Q_OpenDropDown = 1
				WriteStatusToLogs "The Toolbar Object  does not exist, please verify."
			End If
		On Error GoTo 0
End Function
'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name             :		Q_VerifyToolBarItemEnability()
' 	Purpose                   :		Verifies whether the Toolbar item is Enabled or not .
' 	Return Value              :		Integer( 0/1)
' 	Arguments                 :
'		Input Arguments       :  	objtool , strItemName,blnenability
'		Output Arguments      :     
'	Function is called by     :
'	Function calls            :		CheckObjectExist(),GetItemFromJavaObject(),GetObjectItemPropertyName()
'	Created on                :		16/08/2016
'	Author                    :		Qualitia
'------------------------------------------------------------------------------------------------------------------------------------------
Function Q_VerifyToolBarItemEnability(ByVal objtool, byval strItemName,Byval blnenability)
	On Error Resume Next
		'Variable Section Begin
		Dim strMethod
		Dim blnpropertyvalue, blnfind
		Dim tempItemName		

		'Variable Section End
		strMethod = strKeywordForReporting
		blnfind = False
		
			If CheckObjectExist(objtool)  Then 
				'tempItemName = GetItemFromJavaObject(objtool,strItemName)				
				
				blnfind =blnenability
				
						If  blnfind Then
								blnpropertyvalue = GetObjectItemPropertyName(objtool, strItemName, "enabled")
								
									If Not IsNull(blnpropertyvalue) Then	
											Q_VerifyToolBarItemEnability = 0
											WriteStatusToLogs "The Toolbar Item is enabled as expected."
									End If
								Else
									Q_VerifyToolBarItemEnability = 0
									WriteStatusToLogs "The Toolbar is not enabled, please verify."
							End If					
			 Else
				 Q_VerifyToolBarItemEnability = 1
				 WriteStatusToLogs "The Toolbar does not exist, please verify."
			 End If
		
	On Error GoTo 0
End Function
'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name             :		Q_VerifyToolBarItemExistence()
' 	Purpose                   :		Verifies whether the Toolbar item is Existence or not .
' 	Return Value              :		Integer( 0/1)
' 	Arguments                 :
'		Input Arguments       :  	objtool , strlItemName,blnExistence
'		Output Arguments      :     
'	Function is called by     :
'	Function calls            :		CheckObjectExist(),GetItemFromJavaObject(),GetObjectItemPropertyName()
'	Created on                :		16/08/2016
'	Author                    :		Qualitia
'------------------------------------------------------------------------------------------------------------------------------------------

Function Q_VerifyToolBarItemExistence(ByVal objtool , ByVal strItemName,Byval blnExistence)
   On Error Resume next
		'Variable Section Begin
			Dim strMethod
			Dim blnstatus, blnMatch,strFindToollItem,blnpropertyvalue
			
			
		'Variable Section End
		strMethod = strKeywordForReporting
			
			blnstatus = false
					
			If CheckObjectExist(objtool) Then
			
				blnpropertyvalue = objtool.GetItemProperty(strItemName,"Exist")
					
					If Not Isnull(blnpropertyvalue) then
					blnstatus = blnExistence
					End If	
					
					If blnstatus  Then
							Q_VerifyToolBarItemExistence =0
							WriteStatusToLogs "ToolBar Item '"&strItemName&"' exist."			
					Else
						Q_VerifyToolBarItemExistence =0
						WriteStatusToLogs "ToolBar Item doesn't exist."
					End If			
			Else
				Q_VerifyToolBarItemExistence =1    
				WriteStatusToLogs "The Toolbar Object  does not exist, please verify."
			End If
		On Error GoTo 0
End Function

'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name             :		Q_VerifyToolbarItemVisibility()
' 	Purpose                   :		Verifies whether the Toolbar item is Visible or not .
' 	Return Value              :		Integer( 0/1)
' 	Arguments                 :
'		Input Arguments       :  	objtool , strlItemName,blnVisible
'		Output Arguments      :     
'	Function is called by     :
'	Function calls            :		CheckObjectExist(),GetItemFromJavaObject(),GetObjectItemPropertyName()
'	Created on                :		16/08/2016
'	Author                    :		Qualitia
'------------------------------------------------------------------------------------------------------------------------------------------
Function Q_VerifyToolbarItemVisibility(ByVal objtool,ByVal strItemName,ByVal blnVisibility)
	On Error Resume Next
		'Variable Section Begin

		Dim strMethod,strstatus,blnstatus

		'Variable Section End
		strMethod = strKeywordForReporting
		blnstatus=False
		
			If CheckObjectExist(objtool)Then			
						strstatus = objtool.GetItemProperty(strItemName,"enabled")						
				If Vartype(strstatus) <> vbEmpty  And Err.Number = 0 Then				
					blnstatus = blnVisibility	
				End If					
					If blnstatus Then
						Q_VerifyToolbarItemVisibility = 0
						WriteStatusToLogs "The Toolbar Item is visible as expected."
					Else
						Q_VerifyToolbarItemVisibility = 0
						WriteStatusToLogs "The Toolbar Item was not visible, please verify."
					End If
			Else
				Q_VerifyToolbarItemVisibility = 1
				WriteStatusToLogs "The Toolbar does not exist, please verify."
			End If
		
	On Error GoTo 0
End Function
'=======================Code Section End=============================================
'=======================Code Section End=============================================


'-------------------------------------------------------------------