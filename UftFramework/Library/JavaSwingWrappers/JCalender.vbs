'=======================Code Section Begin=============================================
'added Q_Storeselectedtime,Q_StoreselectedDate keyword on 19/08/2016
Option Explicit		'Forces explicit declaration of all variables in a script. 
'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name :				 Q_SetDate()
' 	Purpose :						 Sets the calendar to the specified date.
' 	Return Value :		 		   Integer(0/1)
' 	Arguments :
'		Input Arguments :  		objCalender , strDate
'		Output Arguments : 
'	Function is called by :
'	Function calls :			    CheckObjectExist(),CheckJavaObjectEnabled()
'	Created on :				   30/06/2016
'	Author :						Qualitia
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function Q_SetDate(ByVal objCalender , ByVal strDate)
	On Error Resume Next
		'Variable Section Begin
			
				Dim strMethod 

		'Variable Section End
		strMethod = strkeywordForReporting
		If CheckObjectExist(objCalender) Then
		    If CheckJavaObjectEnabled(objCalender) Then
					If  Not isnull(strDate) or Not IsEmpty(strDate)	Then
					
							objCalender.SetDate strDate
								
								If err.Number = 0 Then
								Q_SetDate = 0
								WriteStatusToLogs "The Calender is set to specified date successfully."
								Else
								Q_SetDate = 1
								WriteStatusToLogs "The Calender is not able to set specified date, please verify."
						  
								End If
					Else
					Q_SetDate = 1
					WriteStatusToLogs "The '"&strDate&"' is null or invalid, please verify."
					End If					
			Else
			Q_SetDate = 1
			WriteStatusToLogs "The '"&objCalender&"' was not enabled, please verify."


   			End If
				  
		Else
		Q_SetDate = 1
		WriteStatusToLogs "The '"&objCalender&"' does not exist, please verify."
				End If
	On Error GoTo 0
End Function

'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name :				 Q_Settime()
' 	Purpose :						 Sets the calendar to the specified time.
' 	Return Value :		 		   Integer(0/1)
' 	Arguments :
'		Input Arguments :  		objCalender , strtime
'		Output Arguments : 
'	Function is called by :
'	Function calls :			    CheckObjectExist(),CheckJavaObjectEnabled()
'	Created on :				   30/06/2016
'	Author :						 Qualitia 
 
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function Q_Settime(ByVal objCalender , ByVal strtime)
	On Error Resume Next
		'Variable Section Begin
			
				Dim strMethod 

		'Variable Section End
		strMethod = strkeywordForReporting
		If CheckObjectExist(objCalender) Then
		    If CheckJavaObjectEnabled(objCalender) Then
					If  Not isnull(strtime) or Not IsEmpty(strtime)	Then
					
							objCalender.SetDate strtime
								
								If err.Number = 0 Then
								Q_Settime = 0
								WriteStatusToLogs "The Calender is set to specified time successfully."
								Else
								Q_Settime = 1
								WriteStatusToLogs "The Calender is not able to set specified time, please verify."
						  
								End If
					Else
					Q_Settime = 1
					WriteStatusToLogs "The '"&strtime&"' is null or invalid, please verify."
					End If					
			Else
			Q_Settime = 1
			WriteStatusToLogs "The '"&objCalender&"' was not enabled, please verify."


   			End If
				  
		Else
		Q_Settime = 1
		WriteStatusToLogs "The '"&objCalender&"' does not exist, please verify."
				End If
	On Error GoTo 0
End Function

'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name :				 Q_StoreselectedDate()
' 	Purpose :						 Sets the calendar to the specified time.
' 	Return Value :		 		   Integer(0/1)
' 	Arguments :
'		Input Arguments :  		objCalender , strkey
'		Output Arguments : 
'	Function is called by :
'	Function calls :			    CheckObjectExist(),CheckJavaObjectEnabled()
'	Created on :				   19/08/2016
'	Author :						 Qualitia 
 '
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function Q_StoreselectedDate(ByVal objCalender , ByVal strkey)
	On Error Resume Next
		'Variable Section Begin
			
				Dim strMethod 
				Dim strActDate
		'Variable Section End
		strMethod = strkeywordForReporting
		If CheckObjectExist(objCalender) Then
		    If CheckJavaObjectEnabled(objCalender) Then
					If  Not isnull(strkey) or Not IsEmpty(strkey)	Then
							strActDate=objCalender.GetROProperty("text")
							If  Not isnull(strActDate)Then	
								AddItemToStorageDictionary strkey,strActDate
									If err.Number = 0 Then
									Q_StoreselectedDate = 0
									WriteStatusToLogs "The selected Date '"&strActDate&"' is stored successfully in key '"&strkey&"'"
								Else
									Q_StoreselectedDate = 1
									WriteStatusToLogs "The could not able to stored, please verify."
						  
									End If
							Else
								Q_StoreselectedDate = 1
								WriteStatusToLogs "The could not able to store selected date, please verify."
						    End If	
					Else
					Q_StoreselectedDate = 1
					WriteStatusToLogs "The '"&strkey&"' is null or invalid, please verify."
					End If					
			Else
			Q_StoreselectedDate = 1
			WriteStatusToLogs "The '"&objCalender&"' was not enabled, please verify."


   			End If
				  
		Else
		Q_StoreselectedDate = 1
		WriteStatusToLogs "The '"&objCalender&"' does not exist, please verify."
				End If
	On Error GoTo 0
End Function

'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name :				 Q_Storeselectedtime()
' 	Purpose :						 Sets the calendar to the specified time.
' 	Return Value :		 		   Integer(0/1)
' 	Arguments :
'		Input Arguments :  		objCalender , strkey
'		Output Arguments : 
'	Function is called by :
'	Function calls :			    CheckObjectExist(),CheckJavaObjectEnabled()
'	Created on :				   19/08/2016
'	Author :						 Qualitia 

'
'------------------------------------------------------------------------------------------------------------------------------------------
Function Q_Storeselectedtime(ByVal objCalender , ByVal strkey)
	On Error Resume Next
		'Variable Section Begin
			
				Dim strMethod 
				Dim strActtime

		'Variable Section End
		strMethod = strkeywordForReporting
		If CheckObjectExist(objCalender) Then
		    If CheckJavaObjectEnabled(objCalender) Then
					If  Not isnull(strkey) or Not IsEmpty(strkey)	Then
							strActtime=objCalender.GetROProperty("text")
							If  Not isnull(strActDate)Then	
								AddItemToStorageDictionary strkey,strActtime
									If err.Number = 0 Then
									Q_Storeselectedtime = 0
									WriteStatusToLogs "The selected Date '"&strActtime&"' is stored successfully in key '"&strkey&"'"
								Else
									Q_Storeselectedtime = 1
									WriteStatusToLogs "The could not able to stored, please verify."
						  
									End If
							Else
								Q_Storeselectedtime = 1
								WriteStatusToLogs "The could not able to store selected time, please verify."
						    End If	
					Else
					Q_Storeselectedtime = 1
					WriteStatusToLogs "The '"&strkey&"' is null or invalid, please verify."
					End If					
			Else
			Q_Storeselectedtime = 1
			WriteStatusToLogs "The '"&objCalender&"' was not enabled, please verify."


   			End If
				  
		Else
		Q_Storeselectedtime = 1
		WriteStatusToLogs "The '"&objCalender&"' does not exist, please verify."
				End If
	On Error GoTo 0
End Function

