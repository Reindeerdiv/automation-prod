''Function SetValueInJSpinItem(ByVal objSpin , ByVal strItem)
''Function VerifyJSpinCureentValue(ByVal objSpin , ByVal strExpItem)
''Function SetJSpinPrevItem(ByVal objSpin)
''Function SetJSpinNextItem(ByVal objSpin)


'=======================Code Section Begin=============================================
Option Explicit 		'Forces explicit declaration of all variables in a script. 
'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name			:  SetValueInJSpin()
' 	Purpose					:  Sets the value of the spin object
' 	Return Value			:  Integer( 0/1)
' 	Arguments				:
'		Input Arguments		:  objSpin , strItem
'		Output Arguments	:  strActItem
'	Function is called by	:
'	Function calls			:  CheckObjectExist()
'	Created on				:  17/08/2009
'	Author					:  Vidyasagar 
' 
'------------------------------------------------------------------------------------------------------------------------------------------

Function SetValueInJSpin(ByVal objSpin , ByVal strItem)
	On Error Resume Next
		'Variable Section Begin
		 Dim strMethod
		 Dim strActItem
            
		'Variable Section End
		strMethod = strKeywordForReporting
			If CheckObjectExist(objSpin) Then
				objSpin.Set strItem
				 If Err.number = 0 Then
					 strActItem = objSpin.GetROProperty("value")
						If Not IsEmpty(strActItem) Then
							 'f CompareStringValues(strItem, strActItem,0) = 0 Then
							 If StrComp (strItem, strActItem,0) = 0 Then
								 SetValueInJSpin = 0
								 WriteStatusToLogs  "Given value is set in the Box."
							 Else
								SetValueInJSpin = 1
								WriteStatusToLogs "The specific data could not be selected, please verify item exist."
							 End If
						Else
							SetValueInJSpin = 1
							WriteStatusToLogs "The Item could not set, please verify."
						End If
				 Else
					 SetValueInJSpin=1
					 WriteStatusToLogs "Given Item is not existing, please verify"
				 End if
			Else
				SetValueInJSpin = 1
				WriteStatusToLogs "The JavaSpin Object  does not exist, please verify."
			End If
		On Error GoTo 0
End Function


'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name			:  VerifyJSpinCureentValue()
' 	Purpose					:  Verifies the current item
' 	Return Value			:  Integer( 0/1)
' 	Arguments				:
'		Input Arguments		:  objSpin , strExpItem
'		Output Arguments	:  strActItem
'	Function is called by	:
'	Function calls			:  CheckObjectExist()
'	Created on				:  17/08/2009
'	Author					:  Vidyasagar 
' 
'------------------------------------------------------------------------------------------------------------------------------------------

Function VerifyJSpinCureentValue(ByVal objSpin , ByVal strExpItem)
	On Error Resume Next
		'Variable Section Begin		
		Dim strMethod
		 Dim strActItem            
		'Variable Section End
		strMethod = strKeywordForReporting
			If CheckObjectExist(objSpin) Then				
				 If Err.number = 0 Then
					 strActItem = objSpin.GetROProperty("value")
						If Not IsEmpty(strActItem) Then
							 If CompareStringValues(strExpItem, strActItem) = 0 Then
								 VerifyJSpinCureentValue = 0
								 WriteStatusToLogs  "Expected value is matched."
							 Else
								 VerifyJSpinCureentValue = 1
								 WriteStatusToLogs "Expected value is not matched, please verify item exist."
							 End If
						Else
							VerifyJSpinCureentValue = 1
							WriteStatusToLogs "The Item could not set, please verify."
						End If
				 Else
					VerifyJSpinCureentValue=1
					WriteStatusToLogs "Given Item is not existing, please verify."
				 End if
			Else
				VerifyJSpinCureentValue = 1
				WriteStatusToLogs "The JavaSpin Object does not exist, please verify."
			End If
		On Error GoTo 0
End Function

'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name			:  SetJSpinPrevItem()
' 	Purpose					:  Sets a Previous item to the Grid
' 	Return Value			:  Integer( 0/1)
' 	Arguments				:
'		Input Arguments		:  objSpin
'		Output Arguments	:  
'	Function is called by	:
'	Function calls			:  CheckObjectExist()
'	Created on				:  17/08/2009
'	Author					:  Vidyasagar 
' 
'------------------------------------------------------------------------------------------------------------------------------------------

Function SetJSpinPrevItem(ByVal objSpin)
	On Error Resume Next
		'Variable Section Begin
	
			Dim strMethod

		'Variable Section End
		strMethod = strKeywordForReporting
			If CheckObjectExist(objSpin) Then
				objSpin.Prev     'sets the previous item
					 If Err.number = 0 Then
						SetJSpinPrevItem = 0
						WriteStatusToLogs "Previous Item is set to the Grid."	
					 Else
						SetJSpinPrevItem=2
						WriteStatusToLogs "Previous Item is not existing, please verify."
					 End if
			Else
				SetJSpinPrevItem = 1
				WriteStatusToLogs "The JavaSpin Object does not exist, please verify."
			End If
		On Error GoTo 0
End Function

'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name			:  SetJSpinNextItem()
' 	Purpose					:  Sets a next item to the Grid
' 	Return Value			:  Integer( 0/1)
' 	Arguments				:
'		Input Arguments		:  objSpin
'		Output Arguments	:  
'	Function is called by	:
'	Function calls			:  CheckObjectExist()
'	Created on				:  17/08/2009
'	Author					:  Vidyasagar 
' 
'------------------------------------------------------------------------------------------------------------------------------------------

Function SetJSpinNextItem(ByVal objSpin)
	On Error Resume Next	
		'Variable Section Begin
	
			Dim strMethod 

		'Variable Section End
		strMethod = strKeywordForReporting
			If CheckObjectExist(objSpin) Then
				objSpin.Next             'sets the next item
					 If Err.number = 0 Then
						SetJSpinNextItem = 0
						WriteStatusToLogs "Next Item is set to the Grid"	
					 Else
						SetJSpinNextItem=2
						WriteStatusToLogs "Next Item is not existing, please verify"
					 End if
			Else
				SetJSpinNextItem = 1
				WriteStatusToLogs "The JavaSpin Object does not exist, please verify."
			End If
		On Error GoTo 0
End Function
'=======================Code Section End=============================================