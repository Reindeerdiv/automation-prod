''
''Function ClickJButton(ByVal objButton)
''Function VerifyJButtonName(ByVal objButton , ByVal strExpName)

'=======================Code Section Begin============================================

Option Explicit		 'Forces explicit declaration of all variables in a script. 

'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :			  ClickJButton()
'	Purpose :				  Performs  Click operation on a specified Button.
'	Return Value :		 	  Integer(0/1/2)
'	Arguments :
'		Input Arguments :  	 objButton
'		Output Arguments : 
'	Function is called by :
'	Function calls :		     CheckObjectExist() , CheckObjectClick()
'	Created on :			    10/03/2008
'	Author :					   Rathna Reddy 
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function ClickJButton(ByVal objButton)
	On Error Resume Next

		'Variable Section Begin
	
			Dim strMethod 

		'Variable Section End
		strMethod = strKeywordForReporting
			If CheckObjectExist(objButton) Then 
			   If CheckJavaObjectEnabled(objButton) Then
					If CheckObjectClick(objButton) Then
						ClickJButton = 0
						WriteStatusToLogs "Click has been performed on button successfully."
					Else
						ClickJButton = 1
						WriteStatusToLogs "Click could not be performed on button, please verify."
					End If 
				Else
					ClickJButton = 1
					WriteStatusToLogs "The Button was disabled and click could not be performed, please verify."
				End If
			  Else
                   ClickJButton = 1
				   WriteStatusToLogs "The Button does not exist, please verify."
			  End If
			
		
	On Error GoTo 0
End Function





'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :			  VerifyJButtonExistence()
'	Purpose :				  Verifies if a button exists for not
'	Return Value :		 	  Integer(0/1/2)
'	Arguments :
'		Input Arguments :  	  objButton , existence
'		Output Arguments : 
'	Function is called by :
'	Function calls :		    CheckObjectExist() 
'	Created on :			    01/02/2011
'	Author :					Sanam Chugh
'
'------------------------------------------------------------------------------------------------------------------------------------------

Function VerifyJButtonExistence(ByVal objButton, ByVal existence)
	On Error Resume Next

		'Variable Section Begin
	
			Dim strMethod  

		'Variable Section End
		strMethod = strKeywordForReporting

		
				
		'If VarType(existence) <>vbEmpty Then
		If StrComp(existence,"") =1 Then
			If IsBoolean(existence) = 0 Then
			
			  If CheckObjectExist(objButton) Then 
			   If existence then
						VerifyJButtonExistence = 0
						WriteStatusToLogs "The button exists."
				Else
						VerifyJButtonExistence = 2
						WriteStatusToLogs "The button exists."
				End If 
			Else 
				If Not existence then
					VerifyJButtonExistence = 0
					WriteStatusToLogs "The button does not exist."
				
				Else
                   VerifyJButtonExistence = 2
				   WriteStatusToLogs "The specified Button does not exists."
			  End If

			
			end If

		Else
			
				WriteStatusToLogs "The existence parameter must be boolean 'true' or 'false'. Please verify."
				VerifyJButtonExistence=1

		End if
		Else
	
			WriteStatusToLogs "The existence parameter is empty. Please verify."
				VerifyJButtonExistence=1
		End If
	On Error GoTo 0
End Function



'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :			  VerifyJButtonVisibility()
'	Purpose :				  Verifies if a button is visible or not
'	Return Value :		 	  Integer(0/1/2)
'	Arguments :
'		Input Arguments :  	  objButton , visiblity
'		Output Arguments : 
'	Function is called by :
'	Function calls :		    VerifyObjectVisible() 
'	Created on :			    01/02/2011
'	Author :					Sanam Chugh
'
'------------------------------------------------------------------------------------------------------------------------------------------

Function VerifyJButtonVisibility(ByVal objButton, ByVal visiblity)
	On Error Resume Next

		'Variable Section Begin
	
			Dim strMethod  

		'Variable Section End
		strMethod = strKeywordForReporting

		
				

	If StrComp(visiblity,"") =1 Then
	 If IsBoolean(visiblity) = 0 then
		If CheckObjectExist(objButton) then
			If CheckJavaObjectVisible(objButton) Then 
			   If visiblity then
						VerifyJButtonVisibility = 0
						WriteStatusToLogs "The button is visible."
				Else
						VerifyJButtonVisibility = 2
						WriteStatusToLogs "The button is visible ."
				End If 
			Else 
				If Not visiblity then
					VerifyJButtonVisibility = 0
					WriteStatusToLogs "The button is invisible."
				
				Else
                   VerifyJButtonVisibility = 2
				   WriteStatusToLogs "The button is invisible."
			  End If
				end if
			else
				VerifyJButtonVisibility = 1
						WriteStatusToLogs "The specified Button does not exist, please verify."

			end If

			Else
			
				WriteStatusToLogs "The visibility parameter must be boolean 'true' or 'false'. Please verify."
				VerifyJButtonVisibility=1

		End if
			
			Else
				VerifyJButtonVisibility=1
					WriteStatusToLogs "The visiblity parameter is empty. Please verify."

		   End If
		
	On Error GoTo 0
End Function




'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :			  VerifyJButtonEnability()
'	Purpose :				  Verifies if a button is enabled or not
'	Return Value :		 	  Integer(0/1/2)
'	Arguments :
'		Input Arguments :  	  objButton , enability
'		Output Arguments : 
'	Function is called by :
'	Function calls :		    CheckJavaObjectEnabled() 
'	Created on :			    01/02/2011
'	Author :					Sanam Chugh
'
'------------------------------------------------------------------------------------------------------------------------------------------

Function VerifyJButtonEnability(ByVal objButton, ByVal enability)
	On Error Resume Next

		'Variable Section Begin
	
			Dim strMethod  

		'Variable Section End
		strMethod = strKeywordForReporting

		
			
If StrComp(enability,"") =1 Then
	If IsBoolean(enability) = 0 then
		If CheckObjectExist(objButton) Then
		 
			If CheckJavaObjectEnabled(objButton) Then 
			   If enability then 
						VerifyJButtonEnability = 0
						WriteStatusToLogs "The button is enabled."
				Else
						VerifyJButtonEnability = 2
						WriteStatusToLogs "The button is enabled ."
				End If 
			Else 
				If Not enability then
					VerifyJButtonEnability = 0
					WriteStatusToLogs "The button is disabled."
				
				Else
                   VerifyJButtonEnability = 2
				   WriteStatusToLogs "The button is disabled."
			  End If
			
		end If 'CheckJavaObjectEnabled
		
		else
				VerifyJButtonEnability = 1
						WriteStatusToLogs "The specified Button does not exist, please verify."

		end If'CheckObjectExist

		Else
			
				WriteStatusToLogs "The enability parameter must be boolean 'true' or 'false'. Please verify."
				VerifyJButtonEnability=1

		End if
			
	Else
			
			VerifyJButtonEnability=1
			WriteStatusToLogs "The enability parameter is empty. Please verify"

	End If

	On Error GoTo 0
End Function



'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name :				 VerifyJButtonName()
' 	Purpose :						Compares the Button Name with expected Name.
' 	Return Value :		 		  Integer(0/1/2)
' 	Arguments :
'		Input Arguments :  		objButton , strExpName
'		Output Arguments : 	   strActName
'	Function is called by :
'	Function calls :				CheckObjectExist(),CompareStringValues()				
'	Created on :				   10/03/2008
'	Author :						  Ashok Jakkani
'------------------------------------------------------------------------------------------------------------------------------------------
Function VerifyJButtonName(ByVal objButton , ByVal strExpName)
	On Error Resume Next
		'Variable Section Begin

			Dim strMethod  
			Dim strActName, blnstatus 
			blnstatus  = False
		'Variable Section End
		strMethod = strKeywordForReporting
			If CheckObjectExist(objButton) Then         
					strActName = objButton.GetRoProperty("label")
				
					If Vartype(strActName) <> vbEmpty  And Err.Number = 0 Then
							blnstatus = true
					End If
					If blnstatus =True Then
							If CompareStringValues(strExpName,strActName) = 0  Then
							     VerifyJButtonName = 0
							     WriteStatusToLogs "Expected Name : "&strExpName& """" &  " and " &""""&"Actual Name : "&strActName&""""&" matches as expected."
		
							Else
								 strActName = replace(strActName,"&","")
								 If CompareStringValues(strExpName,strActName) = 0  Then
										VerifyJButtonName = 0
										 WriteStatusToLogs "Expected Name : "&strExpName& """" &  " and " &""""&"Actual Name : "&strActName&""""&" matches as expected."
			
								 Else
										VerifyJButtonName = 2
										 WriteStatusToLogs "Expected Name : "&strExpName& """" &  " and " &""""&"Actual Name : "&strActName&""""&" does not match, Please verify."
								  End if 
								
							End If
					Else'blnStatus
							VerifyJButtonName = 1
							WriteStatusToLogs "An error occurred during the method VerifyJButtonName"
					End if
			Else 'CheckObjectExist
					VerifyJButtonName = 1
					WriteStatusToLogs "The specified Button does not exist, please verify."
			End If
			
	On Error GoTo 0
End Function


'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :			  DoubleClickJButton()
'	Purpose :				  Performs  DoubleClick operation on a specified Button.
'	Return Value :		 	  Integer(0/1)
'	Arguments :
'		Input Arguments :  	  objButton
'		Output Arguments : 
'	Function is called by :
'	Function calls :		  CheckObjectExist() , CheckJavaObjectEnabled()
'	Created on :			  10/03/2008
'	Author :				  Rathna Reddy 
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function DoubleClickJButton(ByVal objButton)
	On Error Resume Next

		'Variable Section Begin
	
			Dim strMethod  

		'Variable Section End
		strMethod = strKeywordForReporting
			If CheckObjectExist(objButton) Then 
			   If CheckJavaObjectEnabled(objButton) Then
					objButton.DblClick
					If Err.number = 0 Then
						DoubleClickJButton = 0
						WriteStatusToLogs "DoubleClick has been performed on button successfully."
					Else
						DoubleClickJButton = 1
						WriteStatusToLogs "DoubleClick could not be performed on button, please verify."
					End If 
				Else
					DoubleClickJButton = 1
					WriteStatusToLogs "The Button was disabled and DoubleClick could not be performed, please verify."
				End If
			  Else
                   DoubleClickJButton = 1
				   WriteStatusToLogs "The Button does not exist, please verify."
			  End If
			
		
	On Error GoTo 0
End Function

'=======================Code Section End=============================================
