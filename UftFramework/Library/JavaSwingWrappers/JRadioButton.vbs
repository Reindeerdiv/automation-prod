''Function SelectJRadioButton(ByVal objRadioButton)
''Function VerifyJRadioButtonSelected(ByVal objRadioButton)
''Function VerifyJRadioButtonDeSelected(ByVal objRadioButton)

'=======================Code Section Begin=============================================

Option Explicit 		'Forces explicit declaration of all variables in a script. 
'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :				  SelectJRadioButton()
' 	Purpose :						Selects a specified checkbox.
' 	Return Value :		 		   Integer(0/1/2)
' 	Arguments :
'		Input Arguments :  		objRadioButton 
'		Output Arguments :      
'	Function is called by :
'	Function calls :				CheckObjectExist()
'	Created on :				   11/03/2008
'	Author :						  Rathna Reddy 
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function SelectJRadioButton(ByVal objRadioButton)
	On Error Resume Next
		'Variable Section Begin
		
		Dim strMethod
		Dim  intActValue, blnstatus

		'Variable Section End
		strMethod = strKeywordForReporting
		blnstatus = False
        If CheckObjectExist(objRadioButton) Then
            If CheckJavaObjectEnabled(objRadioButton) Then
				 intActValue = objRadioButton.GetROProperty("value")
				 
				 If Vartype(intActValue) <> vbEmpty  Then
					blnstatus = true
				 End If
				 
				 If blnstatus  Then
							If  intActValue = "0"  Then
								objRadioButton.Set "ON"
								intActValue = objRadioButton.GetROProperty("value")
								If Err.Number = 0 Then
										If intActValue = "1" Then
											SelectJRadioButton = 0
											WriteStatusToLogs "The Radio Button got selected successfully."
										Else
											SelectJRadioButton = 1
											WriteStatusToLogs "The Radio Button could not get selected, please verify."
										End If
						  
							Else									'Err.Number = 0 
								SelectJRadioButton = 1
								WriteStatusToLogs "The Error occured during value set Radio Button to ON, please verify."
							End If
							Else									' intActValue = "0"   aleady selected
											SelectJRadioButton = 0
											WriteStatusToLogs "The Radio Button already selected."
							End If
								
					Else											' Vartype(intActValue) <> vbEmpty
					  SelectJRadioButton = 1
					  WriteStatusToLogs	"Could not capture Radio Button state, please verify."
					End If
                Else												'CheckJavaObjectEnabled
					SelectJRadioButton = 1
					WriteStatusToLogs	"The Radio Button was not enabled, please verify."
				End If
					
			Else													'CheckObjectExist
				SelectJRadioButton = 1
					WriteStatusToLogs	"The Radio Button does not exist, please verify."
			End If
					
	 On Error GoTo 0
End Function

'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :				  VerifyJRadioButtonSelected()
'	Purpose :					  Verifies whether the Radio Button is in checked state
'	Return Value :		 		   Integer( 0/1)
'	Arguments :
'		Input Arguments :  		  objRadioButton 
'		Output Arguments : 	      intActValue,intExpValue
'	Function is called by :
'	Function calls :				CheckObjectExist()
'	Created on :				   14/08/2008
'	Author :						  Ashok
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function VerifyJRadioButtonSelected(ByVal objRadioButton)
	On Error Resume Next
		'Variable Section Begin
		
			Dim strMethod
			Dim intActValue,intExpValue, blnstatus
                blnstatus = False
		'Variable Section End	
		 strMethod = strKeywordForReporting	
			If CheckObjectExist(objRadioButton) Then
			    intExpValue = "1"
				 intActValue = objRadioButton.GetROProperty("value") 
						 If Vartype(intActValue) <> vbEmpty  And Err.Number = 0 Then
							blnstatus = true
						 End If

						 If blnstatus  Then
								 If intActValue = intExpValue Then
										VerifyJRadioButtonSelected = 0
										WriteStatusToLogs "Radio Button was checked as expected."
									Else
										VerifyJRadioButtonSelected = 2
										WriteStatusToLogs "Radio Button was not checked, please verify."
								End if
						Else
						VerifyJRadioButtonSelected = 1
								WriteStatusToLogs "Could not capture Radio Button state, please verify."
						End If
					
			Else
			VerifyJRadioButtonSelected = 1
			WriteStatusToLogs "The Radio Button does not exist, please verify."

			End If
		
	 On Error GoTo 0
End Function	 

'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :				  VerifyJRadioButtonDeSelected()
'	Purpose :					  Verifies whether the Radio Button is in UnChecked state
'	Return Value :		 		   Integer( 0/1)
'	Arguments :
'		Input Arguments :  		objRadioButton 
'		Output Arguments : 	   intActValue,intExpVal
'	Function is called by :
'	Function calls :				CheckObjectExist()
'	Created on :				   11/03/2008
'	Author :						  Ashok Jakkani 
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function VerifyJRadioButtonDeSelected(ByVal objRadioButton)
	On Error Resume Next
		'Variable Section Begin
		
			Dim strMethod
			Dim intActValue,intExpValue, blnstatus 
			blnstatus  =False
			
		'Variable Section End	
		strMethod = strKeywordForReporting
			If CheckObjectExist(objRadioButton) Then
			    intExpValue = "0"
				 intActValue = objRadioButton.GetROProperty("value") 

					If Vartype(intActValue) <> vbEmpty  And Err.Number = 0 Then
						blnstatus = true
					 End If

					 If blnstatus  Then				 
					 
								 If intActValue = intExpValue Then
										VerifyJRadioButtonDeSelected = 0
										WriteStatusToLogs 	"Radio Button was Un-checked as expected."
									Else
										VerifyJRadioButtonDeSelected = 2
														WriteStatusToLogs 	"Radio Button was checked, please Verify."
								End if
					Else
					VerifyJRadioButtonDeSelected = 1
					WriteStatusToLogs	"Could not capture Radio Button state, please verify."
	
					
					End if 
			
			Else
					VerifyJRadioButtonDeSelected = 1
					WriteStatusToLogs	"The Radio Button does not exist, please verify."
		End If
		
	 On Error GoTo 0
End Function

'=======================Code Section End=============================================

