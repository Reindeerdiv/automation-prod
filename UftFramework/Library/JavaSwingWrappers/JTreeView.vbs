''Function SelectJTreeNode(byval objJavaTree , byval arrJTreeNodes)
''Function VerifyJTreeNodeExist(byval objJavaTree , byval arrJTreeNodes)
''Function ExpandJNode(ByVal objJavaTree, ByVal strTreeItems)
''Function CollapseJNode(ByVal objJavaTree , ByVal strTreeItems)
''Function VerifyNodeExistingInJTreeHierarchy(ByVal objJavaTree , Byval strParentHierarchy, ByVal strChildElementName)
''Function VerifyNodeNotExistingInJTreeHierarchy(ByVal objJavaTree , Byval strNodeHierarchy, ByVal NodeElementName)
''Function VerifyChildNodesExistingInJTreeParentNode(ByVal objJavaTree , Byval strNodeHierarchy, ByVal ArrNodeElementName)
''Function VerifyChildNodesNotExistingInJTreeParentNode(ByVal objJavaTree , Byval strNodeHierarchy, ByVal ArrNodeElementName)
''Function VerifyNodeSelectedInJTree(ByVal objJavaTree , Byval strNodeHierarchy)
''Function VerifyJTreeViewVisible(byval objJavaTree)


'=======================Code Section Begin=============================================
Option Explicit 
'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name :				 SelectJTreeNode()
' 	Purpose :						Selects a node in a tree
' 	Return Value :		 		   Integer(0/1)
' 	Arguments :
'		Input Arguments :  		objJavaTree , arrJTreeNodes
'		Output Arguments : 
'	Function is called by :
'	Function calls :			    CheckobjectExist()
'	Created on :				   12/03/2008
'	Author :						 Ashok Jakkani'
'------------------------------------------------------------------------------------------------------------------------------------------
Function SelectJTreeNode(byval objJavaTree , byval arrJTreeNodes)
On error resume next
    'Variable Section Begin
		Dim strMethod
		Dim strJTreeString 
		Dim intCnt , intErrNo
	'Variable Section End
	strMethod = strKeywordForReporting

		strJTreeString = ""
	 	If CheckobjectExist(objJavaTree) Then
				If  isnull( arrJTreeNodes) or isempty(arrJTreeNodes) then
						SelectJTreeNode = 1
						WriteStatusToLogs "The Java tree nodes data are invalid, please verify."
				Else
						
						If  isArray(arrJTreeNodes)Then
								For intCnt =0 to ubound(arrJTreeNodes)
										strJTreeString =  strJTreeString& arrJTreeNodes(intCnt) & ";"  										
								Next
								strJTreeString = Left(strJTreeString , Len(strJTreeString) - 1)
						Elseif not Isobject(arrJTreeNodes) Then 
								strJTreeString = arrJTreeNodes
						End If

						objJavaTree.Select strJTreeString
						intErrNo = err.number
						If intErrNo = 0 Then
								SelectJTreeNode = 0
								WriteStatusToLogs "The specified Java Tree node is selected."
						Elseif intErrNo = -2147220983 Then
								SelectJTreeNode = 2
								WriteStatusToLogs "The specified Java Tree node does not exist."
						Else
								SelectJTreeNode = 1
								WriteStatusToLogs "An error occurred in the action 'SelectJTreeNode' "	   
						End If
											   
				End If
		Else
				VerifyJTreeNodeExist = 1
				WriteStatusToLogs "The specified Java Tree does not exist."
		End If
On error goto 0
End Function

'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name :				 VerifyJTreeNodeExist()
' 	Purpose :						Selects a node in a tree
' 	Return Value :		 		   Integer(0/1)
' 	Arguments :
'		Input Arguments :  		objJavaTree , arrJTreeNodes
'		Output Arguments : 
'	Function is called by :
'	Function calls :			    CheckobjectExist()
'	Created on :				   12/03/2008
'	Author :						 Ashok Jakkani'
'------------------------------------------------------------------------------------------------------------------------------------------
Function VerifyJTreeNodeExist(byval objJavaTree , byval arrJTreeNodes)
On error resume next
    'Variable Section Begin
		Dim strMethod
		Dim strJTreeString 
		Dim intCnt , intErrNo
	'Variable Section End
	strMethod = strKeywordForReporting

		strJTreeString = ""
	 	If CheckobjectExist(objJavaTree) Then
				If  isnull( arrJTreeNodes) or isempty(arrJTreeNodes) then
						VerifyJTreeNodeExist = 1
						WriteStatusToLogs "The Java tree nodes data are invalid, please verify."
				Else
						
						If  isArray(arrJTreeNodes)Then
								For intCnt =0 to ubound(arrJTreeNodes)
										strJTreeString =  strJTreeString& arrJTreeNodes(intCnt) & ";"  										
								Next
								strJTreeString = Left(strJTreeString , Len(strJTreeString) - 1)
						Elseif not Isobject(arrJTreeNodes) Then 
								strJTreeString = arrJTreeNodes
						End If

						objJavaTree.activate strJTreeString
						intErrNo = err.number
						If intErrNo = 0 Then
								VerifyJTreeNodeExist = 0
								WriteStatusToLogs "The specified Java Tree nodes exist."
						Elseif intErrNo = -2147220983 Then
								VerifyJTreeNodeExist = 2
								WriteStatusToLogs "The specified Java Tree nodes does not exist."
						Else
								VerifyJTreeNodeExist = 1
								WriteStatusToLogs "An error occurred in the action 'VerifyJTreeNodeExist' "	   
						End If
											   
				End If
		Else
				VerifyJTreeNodeExist = 1
				WriteStatusToLogs "The specified Java Tree does not exist."
		End If
On error goto 0
End Function
''------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name :				 ExpandJNode()
' 	Purpose :					 Verifies  all the list items with our expected
' 	Return Value :		 		   Integer( 01)
' 	Arguments :
'		Input Arguments :  	  	objJavaTree,strExpItems
'		Output Arguments :     strActItems , arrActItems() , arrExpItems()
'	Function is called by :
'	Function calls :			   CheckobjectExist() , GetArray() , MatchEachArrData()
'	Created on :				  19/03/2008
'	Author :						Ashok Jakkani
'------------------------------------------------------------------------------------------------------------------------------------------
'Action: To be Deleted
Function ExpandJNode_OLD(ByVal objJavaTree, ByVal arrExpandTreeItems)
	On Error Resume Next
		'Variable Section Begin
			Dim strMethod
			Dim blnstatus, blnMatch, blnMatchNode
		   Dim strTreeViewItems, strHierarchy
		   Dim strActualTreeView, strInValidNodeName
		   Dim intMaxCount, intMaxitems, i , j, intCnt
		   Dim arrTreeViewItems, arrInvalidNodeName
		   Dim strJTreeString
		'Variable Section End	
		strMethod = strKeywordForReporting

			blnstatus = false
			blnMatch = False
			blnMatchNode= False
			strJTreeString = ""
			
			If CheckobjectExist(objJavaTree) Then

				If Not IsNull(arrExpandTreeItems) then
						strActualTreeView = arrExpandTreeItems
						MsgBox strActualTreeView
						If not IsNull(strActualTreeView) Then
						
						objJavaTree.Expand strActualTreeView
								If Err.Number = 0 Then
									ExpandJNode = 0
									 	WriteStatusToLogs "Expand operation successfully."
								Else
									
									ExpandJNode = 1
									WriteStatusToLogs "Error occurred during expand operation." & err.description
								End If
						Else
							ExpandJNode = 1
							strInValidNodeName =GetInValidJItemInHierarchy(objJavaTree, strValidNode)
							If Not IsNull(strInValidNodeName )  then
							 arrInValidNodeName = Split(strInValidNodeName, "~~")
							 WriteStatusToLogs "The Item ['"& arrInValidNodeName (0) &"'] is not existing at given index "&arrInValidNodeName (1)&"in Tree view. Please Verify,"
							End if
						End if
					Else
					ExpandJNode = 1
									WriteStatusToLogs "The given Tree input is not valid, please verify."
					End IF	
            Else
				ExpandJNode = 1
				WriteStatusToLogs "The Tree View object does not exist, please verify."
			End If
		On Error GoTo 0
End Function

'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name :				 CollapseJNode()
' 	Purpose :					 Verifies  all the list items with our expected
' 	Return Value :		 		   Integer( 01)
' 	Arguments :
'		Input Arguments :  	  	objJavaTree,strExpItems
'		Output Arguments :     strActItems , arrActItems() , arrExpItems()
'	Function is called by :
'	Function calls :			   CheckobjectExist() , GetArray() , MatchEachArrData()
'	Created on :				  19/08/2009
'	Author :						Jakkani Ashok
'objJavaTree.Collapse  strTreeViewItems 
'------------------------------------------------------------------------------------------------------------------------------------------
Function CollapseJNode(ByVal objJavaTree , ByVal arrCollapseTreeNodes)
	 On Error Resume Next
		'Variable Section Begin
			Dim strMethod
			Dim blnstatus, blnMatch, blnMatchNode
		   Dim strTreeViewItems, strHierarchy
		   Dim strActualTreeView
		   Dim intMaxCount, intMaxitems, intCnt
		   Dim arrTreeViewItems, i, j
		   Dim strJTreeString
		   Dim strValidNode
           
		'Variable Section End	
		strMethod = strKeywordForReporting

			blnstatus = false
			blnMatch = False
			blnMatchNode= False
			strJTreeString = ""
				
			strTreeItems = Trim(strTreeItems)
			If CheckobjectExist(objJavaTree) Then
				strValidNode = GetValidJTreeStringFromArray(arrCollapseTreeNodes)
				If Not IsNull(strValidNode ) then
'						strActualTreeView = GetItemFromTreeNode(objJavaTree, strValidNode)
'						If Not IsNull (strActualTreeView) Then
						strActualTreeView = GetInValidJItemInHierarchy(objJavaTree, strValidNode)
						If  IsNull (strActualTreeView) Then
						objJavaTree.Collapse join(arrCollapseTreeNodes,";")

								If Err.Number = 0 Then
									CollapseJNode = 0
									 	WriteStatusToLogs "Collapse operation successfully."
								Else
									CollapseJNode = 1
									WriteStatusToLogs "Error occurred during collapse operation, please verify."
								End If
						Else
						CollapseJNode = 1
							strInValidNodeName =GetInValidJItemInHierarchy(objJavaTree, strValidNode)
							If Not IsNull(strInValidNodeName )  then
							 arrInValidNodeName = Split(strInValidNodeName, "~~")
							 WriteStatusToLogs "The Item ['"& arrInValidNodeName (0) &"'] is not existing at given index "&arrInValidNodeName (1)&"in Tree view. Please Verify,"
							End if
						End if
					Else
					CollapseJNode = 1
									WriteStatusToLogs "The given Tree input is not valid, please verify."
					End IF	
			Else
				CollapseJNode = 1
				WriteStatusToLogs "The Tree View object does not exist, please verify."
			End If
		On Error GoTo 0
End Function

'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name :				 VerifyNodeExistingInJTreeHierarchy
' 	Purpose :					  Verifies whether the ListBox is Disabled.
'	Return Value :		 		  Integer( 0/1)
' 	Arguments :
'		Input Arguments :  		  objJavaTree 
'		Output Arguments : 
'	Function is called by :
'	Function calls :			  CheckobjectExist() , CheckobjectStatus()
'	Created on :				  14/03/2008
'	Author :					   Rathna Reddy 
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function VerifyNodeExistingInJTreeHierarchy_OLD(ByVal objJavaTree , Byval arrParentHierarchy, ByVal arrChildElementName)
	On Error Resume Next
			'Variable Section Begin
			Dim strMethod
	  		Dim blnstatus, blnMatch, blnMatchNode
			Dim  strParentNodehierarchy, strTempParent, strChildNodeHierarchy, strTempSelectedNode, strTempHierarchy
			Dim strcompletePath, strPathExists
			Dim arrExpectedSelectedNodes, i, j, arrCompltePath 
			Dim strTempSelectHierarchy, strTempcheck
			Dim  arrTreeViewItems, intMaxCount, intMaxitems
			Dim blnValidString, arrInValidNodeName
            

			blnstatus = false
			blnMatch = False
			blnValidString = True

			'blnDropMatch = False
		'Variable Section End
		strMethod = strKeywordForReporting
			If CheckobjectExist(objJavaTree) Then
						strParentNodehierarchy = GetValidJTreeStringFromArray(arrParentHierarchy)
						strChildNodeHierarchy = GetValidJTreeStringFromArray(arrChildElementName)
						If IsNull(strParentNodehierarchy) OR IsNull(strChildNodeHierarchy) Then
							blnValidString = False
						End If

					If blnValidString  Then
						'If  IsItemFindInTreeNode(objJavaTree, strParentNodehierarchy) Then
						If IsNull( GetInValidJItemInHierarchy(objJavaTree, strParentNodehierarchy)) Then
							blnstatus = True
						End If
						If blnstatus Then
                            strCompletePath =   strParentNodehierarchy & ";" & strChildNodeHierarchy
															'If IsItemFindInTreeNode(objJavaTree, strcompletePath)  Then
															If IsNull(GetInValidJItemInHierarchy(objJavaTree, strcompletePath)) Then
																		If   Err.Number = 0 Then
																		VerifyNodeExistingInJTreeHierarchy = 0
																		WriteStatusToLogs "The Item ['"& strcompletePath &"'] is existing in Tree view."
																		Else'blnMatch
																			VerifyNodeExistingInJTreeHierarchy = 1
																			WriteStatusToLogs "The Item ['"& strcompletePath &"'] is not existing in Tree view."
																		End If   
										
															Else					' IsItemFindInTreeNode(objJavaTree, strcompletePath)
															VerifyNodeExistingInJTreeHierarchy = 1

															strInValidNodeName =GetInValidJItemInHierarchy(objJavaTree, strValidNode)
																	If Not IsNull(strInValidNodeName )  then
																	 arrInValidNodeName = Split(strInValidNodeName, "~~")
																	 WriteStatusToLogs "The Item ['"& arrInValidNodeName (0) &"'] is not existing at given index "&arrInValidNodeName (1)&"in Tree view. Please Verify,"
                                                                       
																	End If
														 End if     												''' IsItemFindInTreeNode(objJavaTree, strcompletePath)
								   
					   Else					'blnStatus
					    VerifyNodeExistingInJTreeHierarchy = 1
						strInValidNodeName =GetInValidJItemInHierarchy(objJavaTree, strValidNode)
									If Not IsNull(strInValidNodeName )  then
									 arrInValidNodeName = Split(strInValidNodeName, "~~")
									 WriteStatusToLogs "The Item ['"& arrInValidNodeName (0) &"'] is not existing at given index "&arrInValidNodeName (1)&"in Tree view. Please Verify,"
										
								End If
					End if													'Not IsNull(strTempCompletePath ) 			   
			Else				'IsNot Null
			VerifyNodeExistingInJTreeHierarchy = 1
				WriteStatusToLogs "The tree path of the node is not valid, please verify."
			End if 		'IsNot Null

			Else
				VerifyNodeExistingInJTreeHierarchy = 1
				WriteStatusToLogs "The Tree View object does not exist, please verify."
			End If
		On Error GoTo 0
End Function



'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name :				 VerifyNodeNotExistingInJTreeHierarchy
' 	Purpose :					 To check whether the node exists in the TreeView
'	Return Value :		 		  Integer( 0/1)
' 	Arguments :
'		Input Arguments :  		  objJavaTree 
'		Output Arguments : 
'	Function is called by :
'	Function calls :			  CheckobjectExist() , CheckobjectStatus()
'	Created on :				  14/03/2008
'	Author :					   Rathna Reddy 
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function VerifyNodeNotExistingInJTreeHierarchy(ByVal objJavaTree , Byval arrNodeHierarchy, ByVal arrNodeElementName)
    On Error Resume Next
			'Variable Section Begin
	
			Dim strMethod 
	  		Dim blnstatus, blnMatch
			Dim  strParentNodehierarchy, strTempParent, strChildNodeHierarchy, strTempSelectedNode, strTempHierarchy
			Dim strcompletePath, strPathExists
			Dim arrExpectedSelectedNodes, i
			Dim strTempSelectHierarchy, strTempcheck
			Dim blmatchcounter, blnEapandMatch, blnValidData

			blnstatus = false
			blnMatch = False
			blnEapandMatch =False
			blnValidData = True
			blmatchcounter = 0
			
		'Variable Section End
		strMethod = strKeywordForReporting
			If CheckobjectExist(objJavaTree) Then
			  			strParentNodehierarchy = GetValidJTreeStringFromArray(arrNodeHierarchy)
						strChildNodeHierarchy = GetValidJTreeStringFromArray(arrNodeElementName)
					If IsNull(strParentNodehierarchy) Or IsNull(strChildNodeHierarchy)  Then
						blnValidData = False
					End If
					If blnValidData Then

						strcompletePath = strParentNodehierarchy & ";" & strChildNodeHierarchy
						strPathExists = GetInValidJItemInHierarchy(objJavaTree, strcompletePath)
							If Not  IsNull(strPathExists) Then
									blnEapandMatch  = True
									If blnEapandMatch  And Err.Number = 0 Then
										VerifyNodeNotExistingInJTreeHierarchy = 0
                                    Else
										VerifyNodeNotExistingInJTreeHierarchy = 1
										WriteStatusToLogs "The Item ['"& strcompletePath &"'] is existing in Tree view."
									End If    
							
                            Else'blnMatch
								VerifyNodeNotExistingInJTreeHierarchy = 1
								WriteStatusToLogs "The Tree View Node ['"& arrNodeElementName &"'] not found."
							End If    		
							Else			'blnValidData
							 			VerifyNodeNotExistingInJTreeHierarchy = 1
										WriteStatusToLogs "The tree path of the node is not valid, please verify."
							End if				    'blnValidData
			Else
				VerifyNodeNotExistingInJTreeHierarchy = 1
				WriteStatusToLogs "The Tree View object does not exist, please verify."
			End If
		On Error GoTo 0
End Function


'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name :				 VerifyChildNodesExistingInJTreeParentNode
' 	Purpose :					  Verifies the child nodes of a parent node
'	Return Value :		 		  Integer( 0/1)
' 	Arguments :
'		Input Arguments :  		  objJavaTree 
'		Output Arguments : 
'	Function is called by :
'	Function calls :			  CheckobjectExist() , CheckobjectStatus()
'	Created on :				  14/03/2008
'	Author :					   Rathna Reddy 
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function VerifyChildNodesExistingInJTreeParentNode(ByVal objJavaTree , Byval arrNodeHierarchy, ByVal ArrNodeElementName)
	On Error Resume Next
		'Variable Section Begin
	
		Dim strMethod
		Dim blnstatus, blnMatch
		Dim  strParentNodehierarchy, strTempParent, strChildNodeHierarchy, strTempSelectedNode, strTempHierarchy
		Dim strcompletePath, strPathExists
		Dim arrExpectedSelectedNodes, i, arrChildNodes
		Dim strTempSelectHierarchy, strTempcheck
		Dim strAllChilds, strchildelement , blnchildelement, blnelementmatch , arrelement
		Dim arrInValidNodeName, strInValidNodeName
		Dim blnValidData
		Dim strExpectedChildNodehierarchy
		blnstatus = false
		blnMatch = False
		blnelementmatch  = False
		blnValidData = True

		strMethod = strKeywordForReporting
		
		If CheckobjectExist(objJavaTree) Then
				'strParentNodehierarchy = Replace(Trim(arrNodeHierarchy),"^",";")
				strParentNodehierarchy  = GetValidJTreeStringFromArray(arrNodeHierarchy)
				strExpectedChildNodehierarchy  = GetValidJTreeStringFromArray(ArrNodeElementName)
				If IsNull(strParentNodehierarchy) OR IsNull(strExpectedChildNodehierarchy) Then
					blnValidData  = False
				End If
				If blnValidData  Then
				strTempParent = GetInValidJItemInHierarchy(objJavaTree, strParentNodehierarchy)
							If IsNull(strTempParent) Then
								blnMatch  = True
							End If
					'arrChildNodes = GetArray(ArrNodeElementName, "^")
					If blnMatch Then
											If Not  IsArray(ArrNodeElementName) Then
											ArrNodeElementName = Array(ArrNodeElementName)
											End if
											arrChildNodes = ArrNodeElementName
											For each arrelement in arrChildNodes
												'strchildelement = arrelement
												strchildelement = strParentNodehierarchy &";"&arrelement
												blnchildelement = GetInValidJItemInHierarchy(objJavaTree, strchildelement)
												If  Not IsNull(blnchildelement ) Then
													blnelementmatch = True
													Exit for
												End If
											Next

														If  Not blnelementmatch  Then
															VerifyChildNodesExistingInJTreeParentNode = 0
														Else
															VerifyChildNodesExistingInJTreeParentNode = 1
															WriteStatusToLogs "Action is not supported in QTP/UFT version: "& Environment.Value("ProductVer") & "ownwards. Use an older version."
														End If
							Else
							   VerifyChildNodesExistingInJTreeParentNode = 1
								strInValidNodeName =GetInValidJItemInHierarchy(objJavaTree, strParentNodehierarchy)
									If Not IsNull(strInValidNodeName )  then
									 arrInValidNodeName = Split(strInValidNodeName, "~~")
									 WriteStatusToLogs "Action is not supported in QTP/UFT version: "& Environment.Value("ProductVer") & "ownwards. Use an older version."
									End If
							End if
			Else
			VerifyChildNodesExistingInJTreeParentNode = 1
			WriteStatusToLogs "Action is not supported in QTP/UFT version: "& Environment.Value("ProductVer") & "ownwards. Use an older version."
			End if
		Else
			VerifyChildNodesExistingInJTreeParentNode = 1
			WriteStatusToLogs "The Tree View object does not exist, please verify."
		End If
	On Error GoTo 0
End Function

'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name :				 VerifyChildNodesNotExistingInJTreeParentNode
' 	Purpose :					  Verifies the child nodes of a parent node
'	Return Value :		 		  Integer( 0/1)
' 	Arguments :
'		Input Arguments :  		  objJavaTree 
'		Output Arguments : 
'	Function is called by :
'	Function calls :			  CheckobjectExist() , CheckobjectStatus()
'	Created on :				  14/03/2008
'	Author :					   Rathna Reddy 
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function VerifyChildNodesNotExistingInJTreeParentNode(ByVal objJavaTree, Byval arrNodeHierarchy, ByVal ArrNodeElementName)
	On Error Resume Next
		Dim strMethod
		Dim blnstatus, blnMatch
		Dim  strParentNodehierarchy, strTempParent, strChildNodeHierarchy, strTempSelectedNode, strTempHierarchy
		Dim strcompletePath, strPathExists
		Dim arrExpectedSelectedNodes, i, arrChildNodes
		Dim strTempSelectHierarchy, strTempcheck
		Dim strAllChilds, strchildelement , blnchildelement, blnelementmatch , arrelement
		Dim blnValidData

		blnstatus = false
		blnMatch = False
		blnelementmatch  = False
		blnValidData = True

		strMethod = strKeywordForReporting
		
		If CheckobjectExist(objJavaTree) Then
				'strParentNodehierarchy = Replace(Trim(arrNodeHierarchy),"^",";")
				strParentNodehierarchy = GetValidJTreeStringFromArray(arrNodeHierarchy)
				strExpectedChildNodehierarchy  = GetValidJTreeStringFromArray(ArrNodeElementName)
				If IsNull(strParentNodehierarchy) OR IsNull(strExpectedChildNodehierarchy) Then
					blnValidData  = False
				End If
				If blnValidData Then
                strTempParent = GetInValidJItemInHierarchy(objJavaTree, strParentNodehierarchy)
				End if
					If IsNull(strTempParent) Then
						blnMatch  = True
					End if 
					  					If blnMatch Then
											If Not  IsArray(ArrNodeElementName) Then
											ArrNodeElementName = Array(ArrNodeElementName)
											End if
											arrChildNodes = ArrNodeElementName
											For each arrelement in arrChildNodes
												'strchildelement = arrelement
												strchildelement = strParentNodehierarchy &";"&arrelement
												blnchildelement = GetInValidJItemInHierarchy(objJavaTree, strchildelement)
												If  IsNull(blnchildelement ) Then
													blnelementmatch = True
													Exit for
												End If
											Next



									If  Not blnelementmatch  Then
										VerifyChildNodesNotExistingInJTreeParentNode = 0
									Else
										VerifyChildNodesNotExistingInJTreeParentNode = 1
																WriteStatusToLogs "Action is not supported in QTP/UFT version: "& Environment.Value("ProductVer") & "ownwards. Use an older version."
									End If
				Else
						VerifyChildNodesNotExistingInJTreeParentNode = 1
														strInValidNodeName =GetInValidJItemInHierarchy(objJavaTree, strParentNodehierarchy)
									If Not IsNull(strInValidNodeName )  then
									 arrInValidNodeName = Split(strInValidNodeName, "~~")
									 WriteStatusToLogs "Action is not supported in QTP/UFT version: "& Environment.Value("ProductVer") & "ownwards. Use an older version."
										End If
											
				End if
		Else
			VerifyChildNodesNotExistingInJTreeParentNode = 1
			WriteStatusToLogs "Action is not supported in QTP/UFT version: "& Environment.Value("ProductVer") & "ownwards. Use an older version."
		End If
	On Error GoTo 0
		
End Function

'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name :				 VerifyNodeSelectedInJTree
' 	Purpose :					  Verifies whether the ListBox is Disabled.
'	Return Value :		 		  Integer( 0/1)
' 	Arguments :
'		Input Arguments :  		  objJavaTree 
'		Output Arguments : 
'	Function is called by :
'	Function calls :			  CheckobjectExist() , CheckobjectStatus()
'	Created on :				  14/03/2008
'	Author :					   Rathna Reddy 
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function VerifyNodeSelectedInJTree(ByVal objJavaTree , Byval strNodeHierarchy)
	On Error Resume Next
		'Variable Section Begin
		 Dim strMethod	
		 Dim blnstatus, blnMatch
		 Dim strTreeViewItems, strTempSelectHierarchy
		 Dim strTempSelectedNode
		 Dim blnValidData
		
			blnstatus = false
			blnMatch = False
			blnValidData = True
			'blnDropMatch = False
		'Variable Section End

		strMethod = strKeywordForReporting

			If CheckobjectExist(objJavaTree) Then
						strNodeHierarchy = Trim(strNodeHierarchy)
						'strTreeViewItems = Replace(Trim(strNodeHierarchy),"^",";")
						strTreeViewItems  = GetValidJTreeStringFromArray(strNodeHierarchy)						
                        
								If IsNull(strTreeViewItems) Then
									blnValidData =False
								End if
						If  blnValidData Then
												strTempSelectedNode = objJavaTree.GetRoProperty("value")
													If CompareStringValues(strTreeViewItems,strTempSelectedNode ) = 0 And Err.Number = 0 Then
                                                    VerifyNodeSelectedInJTree = 0
                                                   Else
													  VerifyNodeSelectedInJTree = 2
															WriteStatusToLogs "The Node is not selected or focused."
													End If
							
		
			
											Else'blnMatch
											VerifyNodeSelectedInJTree = 1
											WriteStatusToLogs "The given tree Node is not valid, please verify."
										End If   		   

			Else
				VerifyNodeSelectedInJTree = 1
				WriteStatusToLogs "The Tree View object does not exist, please verify."
			End If
		On Error GoTo 0
End Function
'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name				:	VerifyJTreeViewVisible()
' 	Purpose						:	Verifies the JavaTree View is visible or not
' 	Return Value				:   Integer( 0/1)
' 	Arguments					:
'		Input Arguments			:  	objJavaTree
'		Output Arguments		:   
'	Function is called by		:
'	Function calls				:   CheckobjectExist(),CheckJavaobjectVisible()
'	Created on					:   19/08/2009
'	Author						:	Vidyasagar
'
'------------------------------------------------------------------------------------------------------------------------------------------

Function VerifyJTreeViewVisible(byval objJavaTree)
On error resume next
    'Variable Section Begin
		
		Dim strMethod

	'Variable Section End
		
		strMethod = strKeywordForReporting

	 	If CheckobjectExist(objJavaTree) Then
				If CheckJavaobjectVisible(objJavaTree) then
					 VerifyJTreeViewVisible = 0
					 WriteStatusToLogs "The JavaTree View is visible as expected."
				Else
					VerifyJTreeViewVisible = 1
					WriteStatusToLogs "The JavaTree View is not visible, please verify."				
				End If
		Else
			VerifyJTreeViewVisible = 1
			WriteStatusToLogs "The specified JavaTree View does not exist."
		End If
On error goto 0
End Function


'--------------------------------------------------------
' 	Function Name				:	SelectJavaTree()
' 	Purpose						:	Expands the specified node in the provided java tree.
' 	Return Value				:   Integer( 0/1)
' 	Arguments					:
'		Input Arguments			:  	objTree
'		Output Arguments		:   
'	Function is called by		:
'	Function calls				:   CheckobjectExist(),CheckJavaobjectVisible()
'	Created on					:   19/08/2009
'	Author						:	rajneesh
'	
'-------------------------------------------------
Function SelectJavaTree(byVal objTree, byVal hierarchy)
	On error resume next
	Dim strMethod

	err.number = 0


	strMethod = strKeywordForReporting
	
	If isNull(hierarchy) or isempty(hierarchy) Then 
		WriteStatusToLogs "Parameter is either null or empty." 
		SelectJavaTree= 1
		on error goto 0
		Exit Function
	End if
	
	If CheckobjectExist(objTree) Then 
			'err.number = 0
			'on error resume next
			objTree.Activate
			On error resume next
			objTree.Expand hierarchy
			If err.number = 0 or err.number = 450 Then
				SelectJavaTree = 0
				WriteStatusToLogs "Node is selected in javatree."
			else

				SelectJavaTree=1
				WriteStatusToLogs "Error occurred. "& err.description
			End If
		
	Else
		WriteStatusToLogs "jTree is not found on application."
			SelectJavaTree = 1
			Exit Function
	End If
	
	On error goto 0

End Function


Function Q_StoreJTreeNodeValueByIndex(byval objJavaTree ,byval strKey, byval intJTreeNodesIndex)
On error resume next
    'Variable Section Begin
		Dim strMethod
		Dim strIndexValue
		
	'Variable Section End
	strMethod = strKeywordForReporting
	
	if not isNumeric(intJTreeNodesIndex) or strKey = "" then
		Q_StoreJTreeNodeValueByIndex = 1
		WriteStatusToLogs "Invalid parameters."
		Exit Function
	End IF
	
	If Not Isempty(strKey) or Not isnull(strKey)  or trim(strKey)="" Then
        	If CheckobjectExist(objJavaTree) Then
        	
				If  isnull( intJTreeNodesIndex) or isempty(intJTreeNodesIndex) then
                    	Q_StoreJTreeNodeValueByIndex = 1
						WriteStatusToLogs "The Java tree nodes data are invalid, please verify."
				Else
						
						strIndexValue=objJavaTree.GetItem( intJTreeNodesIndex )
						AddItemToStorageDictionary strKey,strIndexValue
						
						intErrNo = err.number
						If intErrNo = 0 Then
								Q_StoreJTreeNodeValueByIndex = 0
								WriteStatusToLogs "The specified Java Tree node Index position is '"&strIndexValue&"'is stored in '"&strKey&"'successfully."
													
						Elseif intErrNo = -2147220983 Then
								Q_StoreJTreeNodeValueByIndex = 2
								WriteStatusToLogs "An error occurred in the action 'StoreJTreeNodeValueByIndex' "	
						Else
								Q_StoreJTreeNodeValueByIndex = 1
								WriteStatusToLogs "The specified Java Tree node Index position is '"&strIndexValue&"'is  not able to stored in '"&strKey&"', please verify."
						End If
											   
				End If
		Else
				Q_StoreJTreeNodeValueByIndex = 1
				WriteStatusToLogs "The specified Java Tree does not exist."
			End if
        Else
		Q_StoreJTreeNodeValueByIndex = 1
		WriteStatusToLogs 	"The specified '"&strKey&"' is null, please verify."
		End If
On error goto 0
End Function

'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name :				  Q_StoreTreeNodeCount()
' 	Purpose :					  Store the count of the tree node
' 	Return Value :		 		  Integer(0/1)
' 	Arguments :
'		Input Arguments :  		objJavaTree , strKey 
'		Output Arguments : 
'	Function is called by :
'	Function calls :			    CheckobjectExist()
'	Created on :				   27/06/2016
'	Author :						 Qualitia'
'------------------------------------------------------------------------------------------------------------------------------------------
Function Q_StoreTreeNodeCount(byval objJavaTree ,byval strKey)
On error resume next
    'Variable Section Begin
		Dim strMethod
		Dim strTreeNodeCount
		
	'Variable Section End
	strMethod = strKeywordForReporting

	If Not Isempty(strKey) or Not isnull(strKey)  or trim(strKey)="" Then
        	If CheckobjectExist(objJavaTree) Then
        	
						strTreeNodeCount=objJavaTree.GetROProperty("items count")
						AddItemToStorageDictionary strKey,strTreeNodeCount
						
						intErrNo = err.number
						If intErrNo = 0 Then
								Q_StoreTreeNodeCount = 0
								WriteStatusToLogs "The Count of Java Tree Node '"&strTreeNodeCount&"'is stored in '"&strKey&"'successfully."
													
						Else intErrNo = 1
								Q_StoreTreeNodeCount = 1
								WriteStatusToLogs "Couldn't able to store tree node count, please verify."						
						End If
											   
			Else
				Q_StoreTreeNodeCount = 1
				WriteStatusToLogs "The specified Java Tree does not exist."
			End if
        Else
		Q_StoreTreeNodeCount = 1
		WriteStatusToLogs 	"The specified '"&strKey&"' is null, please verify."
		End If
On error goto 0
End Function



Function Q_StoreJTreeNodeValueByIndex(byval objJavaTree ,byval strKey, byval intJTreeNodesIndex)
On error resume next
    'Variable Section Begin
		Dim strMethod
		Dim strIndexValue
		
	'Variable Section End
	strMethod = strKeywordForReporting

	if not isNumeric(intJTreeNodesIndex) or strKey = "" then
		Q_StoreJTreeNodeValueByIndex = 1
		WriteStatusToLogs "Invalid parameters."
		Exit Function
	End IF
	
	If Not Isempty(strKey) or Not isnull(strKey)  or trim(strKey)="" Then
        	If CheckobjectExist(objJavaTree) Then
        	
				If  isnull( intJTreeNodesIndex) or isempty(intJTreeNodesIndex) then
                    	Q_StoreJTreeNodeValueByIndex = 1
						WriteStatusToLogs "The Java tree nodes data are invalid, please verify."
				Else
						
						strIndexValue=objJavaTree.GetItem( intJTreeNodesIndex )
						AddItemToStorageDictionary strKey,strIndexValue
						
						intErrNo = err.number
						If intErrNo = 0 Then
								Q_StoreJTreeNodeValueByIndex = 0
								WriteStatusToLogs "The specified Java Tree node Index position is '"&strIndexValue&"'is stored in '"&strKey&"' successfully."
													
						Elseif intErrNo = -2147220983 Then
								Q_StoreJTreeNodeValueByIndex = 2
								WriteStatusToLogs "An error occurred in the action 'StoreJTreeNodeValueByIndex'."	
						Else
								Q_StoreJTreeNodeValueByIndex = 1
								WriteStatusToLogs "The specified Java Tree node Index position is '"&strIndexValue&"'is  not able to stored in '"&strKey&"', please verify."
						End If
											   
				End If
		Else
				Q_StoreJTreeNodeValueByIndex = 1
				WriteStatusToLogs "The specified Java Tree does not exist."
			End if
        Else
		Q_StoreJTreeNodeValueByIndex = 1
		WriteStatusToLogs 	"The specified '"&strKey&"' is null, please verify."
		End If
On error goto 0
End Function

'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name :				  Q_StoreTreeNodeCount()
' 	Purpose :					  Store the count of the tree node
' 	Return Value :		 		  Integer(0/1)
' 	Arguments :
'		Input Arguments :  		objJavaTree , strKey 
'		Output Arguments : 
'	Function is called by :
'	Function calls :			    CheckobjectExist()
'	Created on :				   27/06/2016
'	Author :						 Qualitia'
'------------------------------------------------------------------------------------------------------------------------------------------
Function Q_StoreTreeNodeCount(byval objJavaTree ,byval strKey)
On error resume next
    'Variable Section Begin
		Dim strMethod
		Dim strTreeNodeCount
		
	'Variable Section End
	strMethod = strKeywordForReporting

	If Not Isempty(strKey) or Not isnull(strKey)  or trim(strKey)="" Then
        	If CheckobjectExist(objJavaTree) Then
        	
						strTreeNodeCount=objJavaTree.GetROProperty("items count")
						AddItemToStorageDictionary strKey,strTreeNodeCount
						
						intErrNo = err.number
						If intErrNo = 0 Then
								Q_StoreTreeNodeCount = 0
								WriteStatusToLogs "The Count of Java Tree Node '"&strTreeNodeCount&"'is stored in '"&strKey&"'successfully."
													
						Else intErrNo = 1
								Q_StoreTreeNodeCount = 1
								WriteStatusToLogs "Couldn't able to store tree node count, please verify."						
						End If
											   
			Else
				Q_StoreTreeNodeCount = 1
				WriteStatusToLogs "The specified Java Tree does not exist."
			End if
        Else
		Q_StoreTreeNodeCount = 1
		WriteStatusToLogs 	"The specified '"&strKey&"' is null, please verify."
		End If
On error goto 0
End Function

'--------------------------------------------------------
' 	Function Name				:	ExpandJNode()
' 	Purpose						:	Expands the specified node in the provided java tree.
' 	Return Value				:   Integer( 0/1)
' 	Arguments					:
'		Input Arguments			:  	objTree
'		Output Arguments		:   
'	Function is called by		:
'	Function calls				:   CheckobjectExist(),CheckJavaobjectVisible()
'	Created on					:   19/08/2009
'	Author						:	rajneesh
'	
'-------------------------------------------------
Function ExpandJNode(byVal objTree, byVal hierarchy)
	On error resume next
	Dim strMethod

	err.number = 0
	

	strMethod = strKeywordForReporting
	
	If isNull(hierarchy) or isempty(hierarchy) Then 
		WriteStatusToLogs "Parameter is either null or empty." 
		ExpandJNode= 1
		on error goto 0
		Exit Function
	End if
	
	If CheckobjectExist(objTree) Then 
			'err.number = 0
			'on error resume next
			objTree.Activate
			On error resume next
			objTree.Expand join(hierarchy, ";")
			If err.number = 0 or err.number = 450 Then
				ExpandJNode = 0
				WriteStatusToLogs "JavaTree node is expanded."
			else

				ExpandJNode=1
				WriteStatusToLogs "Error occurred. "& err.description
			End If
		
	Else
		WriteStatusToLogs "JavaTree is not found on application."
			ExpandJNode = 1
			Exit Function
	End If
	
	On error goto 0

End Function



Function VerifyNodeExistingInJTreeHierarchy(ByVal objJavaTree , Byval arrParentHierarchy, ByVal arrChildElementName, ByVal blnExistance)
	On Error Resume Next
			'Variable Section Begin
			Dim strMethod
	  		Dim blnstatus, blnMatch, blnMatchNode
			Dim  strParentNodehierarchy, strTempParent, strChildNodeHierarchy, strTempSelectedNode, strTempHierarchy
			Dim strcompletePath, strPathExists
			Dim arrExpectedSelectedNodes, i, j, arrCompltePath 
			Dim strTempSelectHierarchy, strTempcheck
			Dim  arrTreeViewItems, intMaxCount, intMaxitems
			Dim blnValidString, arrInValidNodeName, pass, fail, status
            

			blnstatus = false
			blnMatch = False
			blnValidString = True
			
			If Ucase(blnExistance) = "TRUE" Then
				pass = 0
				fail = 1
				status = "Passed"
				else
				pass = 1
				fail =0
				status = "Failed"
			End If

		'Variable Section End
		strMethod = strKeywordForReporting
			If CheckobjectExist(objJavaTree) Then
						strParentNodehierarchy = GetValidJTreeStringFromArray(arrParentHierarchy)
						strChildNodeHierarchy = GetValidJTreeStringFromArray(arrChildElementName)
						If IsNull(strParentNodehierarchy) OR IsNull(strChildNodeHierarchy) OR strParentNodehierarchy = "" or strChildNodeHierarchy = "" or instr("true_false", Lcase(blnExistance)) < 1 Then
							blnValidString = False
						End If

						If blnValidString  Then
							
                            strCompletePath =   strParentNodehierarchy & ";" & strChildNodeHierarchy
							err.number=0								
							objJavaTree.Activate strCompletePath
							If err.number = 0 Then
								VerifyNodeExistingInJTreeHierarchy = pass
								WriteStatusToLogs "The Item ['"& strcompletePath &"'] is existing in Tree view."
							Else			
							VerifyNodeExistingInJTreeHierarchy = fail
								WriteStatusToLogs "The Item ['"& strcompletePath &"'] is not existing in Tree view."
							End if   
								   
						 Else					'blnStatus
						    VerifyNodeExistingInJTreeHierarchy = 1
							strInValidNodeName =GetInValidJItemInHierarchy(objJavaTree, strValidNode)
								WriteStatusToLogs "Invalid parameters."

					     End if															   

			Else
				VerifyNodeExistingInJTreeHierarchy = 1
				WriteStatusToLogs "The Tree View object  does not exist, please verify."
			End If
		On Error GoTo 0
End Function

'=======================Code Section End=============================================
