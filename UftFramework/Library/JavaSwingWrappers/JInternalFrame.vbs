'Function CloseJInternalFrame(ByVal objIntFrame)

'=======================Code Section Begin=============================================
Option Explicit		'Forces explicit declaration of all variables in a script. 
'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name				:	CloseJInternalFrame()
' 	Purpose						:	Closes the Java Internal Frame Window
' 	Return Value				:   Integer( 0/1)
' 	Arguments					:
'		Input Arguments			:  	objIntFrame
'		Output Arguments		:   
'	Function is called by		:
'	Function calls				:   CheckObjectExist()
'	Created on					:   18/08/2009
'	Author						:	Vidyasagar
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function CloseJInternalFrame(ByVal objIntFrame)
	On Error Resume Next
	'Variable Section Begin
	
			Dim strMethod 

		'Variable Section End
		strMethod = strKeywordForReporting
		If CheckObjectExist(objIntFrame)  Then
			objIntFrame.Activate
			objIntFrame.Close
				If Err.Number=0 Then
					CloseJInternalFrame = 0
					WriteStatusToLogs "The JavaInternalFrame is closed as expected."
				Else
					CloseJInternalFrame = 2
					WriteStatusToLogs "JavaInternalFrame Window is not closed, please verify."
				End If
		Else
			CloseJInternalFrame = 1
			WriteStatusToLogs "JavaInternalFrame window doesn't exist, please verify."
		End If			
    On Error GoTo 0
End Function



'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :			  verifyJFrameExistence()
'	Purpose :				  Verifies if a frame exists for not
'	Return Value :		 	  Integer(0/1/2)
'	Arguments :
'		Input Arguments :  	  objButton , existence
'		Output Arguments : 
'	Function is called by :
'	Function calls :		    CheckObjectExist() 
'	Created on :			    01/02/2011
'	Author :					Sanam Chugh
'
'------------------------------------------------------------------------------------------------------------------------------------------

Function verifyJFrameExistence(ByVal objButton, ByVal existence)
	On Error Resume Next

		'Variable Section Begin
	
			Dim strMethod 

		'Variable Section End
		strMethod = strKeywordForReporting

			
				
		
		If StrComp(existence,"") =1 Then

		 If IsBoolean(existence) = 0 Then
		 

			If CheckObjectExist(objButton) Then 
			   If existence then
						verifyJFrameExistence = 0
						WriteStatusToLogs "The frame exists."
				Else
						verifyJFrameExistence = 2
						WriteStatusToLogs "The frame exists."
				End If 
			Else 
				If Not existence then
					verifyJFrameExistence = 0
					WriteStatusToLogs "The frame does not exist."
				
				Else
                   verifyJFrameExistence = 2
				   WriteStatusToLogs "The frame does not exists."
			  End If
			
		end If

		Else
			
				WriteStatusToLogs "The existence parameter must be boolean 'true' or 'false'.Please verify"
				verifyJFrameExistence=1

			End if
		Else
		verifyJFrameExistence=1
				WriteStatusToLogs "The existence parameter is empty.Please verify"

		End If
	On Error GoTo 0
End Function




'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name				:	CompareJFrameTitle()
' 	Purpose						:	verify the frame title with the expected
' 	Return Value				:   Integer( 0/1)
' 	Arguments					:
'		Input Arguments			:  	objWindow,strExpTitle
'		Output Arguments		:   strActTitle
'	Function is called by		:
'	Function calls				:   CheckObjectExist() , CompareStringValues() 
'	Created on					:   01/02/2011
'	Author						:	Sanam Chugh
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function CompareJFrameTitle(ByVal objFrame , ByVal strExpTitle)
	On Error Resume Next
		'Variable Section Begin	
			Dim strMethod
			Dim strActTitle		
		'Variable Section End
		strMethod = strKeywordForReporting


		If CheckObjectExist(objFrame)  Then
			strActTitle = objFrame.GetROProperty("title")
				If CompareStringValues(strExpTitle,strActTitle)=0 Then
					CompareJFrameTitle = 0
					WriteStatusToLogs "The Frame title is same as the expected."
				Else
					CompareJFrameTitle = 2
					WriteStatusToLogs "The Frame title is different from the expected, please verify."
				End If
		Else
			CompareJFrameTitle = 1
			WriteStatusToLogs "The specified  Frame doesn't exist, please verify."
		End If			
    On Error GoTo 0
End Function



'=======================Code Section End=============================================