
'========================Code Section Begin=====================================================================
Option Explicit	'Forces explicit declaration of all variables in a script.

'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :				 ClickJavaObject()
'	Purpose :					 Clicks on a java object.
'	Return Value :		 		 Integer(0/1)
'	Arguments :
'		Input Arguments :  		 JavaObject , X , Y
'		Output Arguments : 
'	Function is called by :
'	Function calls :			 CheckObjectExist() 
'	Created on :				 28/08/2010
'	Author :					 Iohbha K 
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function ClickJavaObject(ByVal objJavaObj , ByVal XCoord , ByVal YCoord)
	On Error Resume Next
		'Variable Section Begin
		   Dim strMethod
		   Dim intX,intY

		'Variable Section End
		strMethod = strKeywordForReporting
		intX = IsWholeNumber(XCoord)
		intY = IsWholeNumber(YCoord)
		If intX = 0 And intY=0 Then
			If CheckObjectExist(objJavaObj) Then 
				If CheckJavaObjectEnabled(objJavaObj) Then 
					objJavaObj.Click CInt(XCoord),CInt(YCoord)
					If Err.number = 0 Then
						ClickJavaObject = 0
						WriteStatusToLogs "The java object is clicked successfully."
					Else
						ClickJavaObject = 1
						WriteStatusToLogs "Fail to click the java object. Please verify. An error occurred, "&Err.description
					End if
				Else
					ClickJavaObject = 1
					WriteStatusToLogs "The java object is not enabled, please verify."
					
				End if
			Else
				ClickJavaObject = 1
				WriteStatusToLogs "The java object does not exist, please verify."
					
			End If 
		Else
			ClickJavaObject = 1
			WriteStatusToLogs "The co-ordinates are not valid whole numbers, please verify."
		End if
	
	On Error GoTo 0
End Function

'=======================Code Section End======================================================================

