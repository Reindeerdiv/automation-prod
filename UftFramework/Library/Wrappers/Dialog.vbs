

'=======================Code Section Begin============================================

Option Explicit		 'Forces explicit declaration of all variables in a script. 


'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :				 VerifyDialogExist()
'	Purpose :					 Verifies the existence of Dialog in AUT.
'	Return Value :		 		  Integer(0/2)
'	Arguments :
'		Input Arguments :  		  objDialog 
'		Output Arguments : 
'	Function is called by :
'	Function calls :			 CheckObjectExist() 
'	Created on :				      15/01/2009
'	Author :						  Rathna Reddy 
'
'------------------------------------------------------------------------------------------------------------------------------------------

Function VerifyDialogExist(ByVal objDialog)
	On Error Resume Next
		'Variable Section Begin
	
		   Dim strMethod

		'Variable Section End
			strMethod = strKeywordForReporting
			If CheckObjectExist(objDialog) Then 
			   
					VerifyDialogExist = 0
					WriteStatusToLogs "The specified Dialog Exists."
					
			Else
					VerifyDialogExist = 2
					WriteStatusToLogs "The specified Dialog does not exist, please verify."
			End If 
	On Error GoTo 0
End Function

'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :				 VerifyDialogNotExist()
'	Purpose :					 Verifies whether the Dialog is NotExist or not in AUT?.
'	Return Value :		 		  Integer(0/2)
'	Arguments :
'		Input Arguments :  		  objDialog 
'		Output Arguments : 
'	Function is called by :
'	Function calls :			 CheckObjectExist() 
'	Created on :				   15/01/2009
'	Author :						  Rathna Reddy 
'
'------------------------------------------------------------------------------------------------------------------------------------------

Function VerifyDialogNotExist(ByVal objDialog)
	On Error Resume Next
		'Variable Section Begin
	
		   Dim strMethod

		'Variable Section End
			strMethod = strKeywordForReporting
			If  CheckObjectNotExist(objDialog) Then 
			   
					VerifyDialogNotExist = 0
					WriteStatusToLogs "The specified Dialog doesn't exist."
					
			Else
					VerifyDialogNotExist = 2
					WriteStatusToLogs "The specified Dialog exists, Please verify."
			End If 
		
	On Error GoTo 0
End Function

'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :			ActivateDialog()
'	Purpose :				Activates the Specified Dialog.	
'	Return Value :		 	Integer(0/1)
'	Arguments :
'		Input Arguments :   objDialog    
'		Output Arguments :  intVal
'	Function is called by : 
'	Function calls :		CheckObjectExist()
'	Created on : 			04/03/2008
'	Author :				Rathna Reddy
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function ActivateDialog(ByVal objDialog)
	On Error Resume Next
		'Variable Section Begin
			Dim strMethod
			Dim intVal,strTitle

		'Variable Section End
			strMethod = strKeywordForReporting
			If CheckObjectExist(objDialog) Then
				 intVal=objDialog.GetROProperty("hwnd")
				  
				 Window("hwnd:=" & intVal).Activate
				 
				   If Err.Number = 0 Then
						ActivateDialog  = 0
						WriteStatusToLogs "The specified Dialog is activated successfully."
				   Else
						ActivateDialog = 1 
						WriteStatusToLogs "The specified Dialog is not activated successfully, please verify."
				   End If
			
			Else
				ActivateDialog = 1 
				WriteStatusToLogs "The specified Dialog doesn't exist, please verify."
			End If
	  
   On Error GoTo 0 
End Function


'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :				  ClickButtonByNameInDialog()
' 	Purpose :					  Clicks a button inside the dialog box. The function supports IE and FF.	
' 	Return Value :		 		  Integer(0/1/2)
' 	Arguments :
'		Input Arguments :  		  objDialog , strBtnName
'		Output Arguments :        
'	Function is called by :
'	Function calls :			  CheckObjectExist(),CheckObjectClick()
'	Created on :				  18/02/2009
'	Author :					  Iohbha K
'
'------------------------------------------------------------------------------------------------------------------------------------------

Function ClickButtonByNameInDialog(byval objDialog , byval  strBtnName)

On Error Resume Next
	'Variable Section Begin		
	Dim strMethod
	Dim objParent , strBrowserMicClass , strBrowserVer , strBrowserType , objBtn ,strExpMicClass
	Dim objDialogChild , intFstCnt , intDialogChildCount ,objTemp , strProp , strBtnValue
	Dim flag , intIt , objChild , intChildCount , strChildMic , strChildValue , blnFound
	Dim strExecutionBtnString , strInnerText , strModifiedBtnName , strModifiedChildName
	flag = -1
	blnFound = -1

	'Variable Section End
	strMethod = strKeywordForReporting
	If CheckObjectExist(objDialog) Then
	
		Set objParent=objDialog.GetProperty("Parent")
		strBrowserMicClass = objParent.getROProperty("micClass")
		If UCase(Trim(strBrowserMicClass)) = UCase("Browser") Then
			strBrowserVer= UCase(objParent.getROProperty("version"))
			If InStr(strBrowserVer,"FIREFOX") >0 Then
				strBrowserType="firefox"
			ElseIf InStr(strBrowserVer,UCase("Internet explorer")) >0 Then
				strBrowserType="Iexplore"
			Else
				strBrowserType="Not Supported"
			End If
			
			If strBrowserType <> "Not Supported" Then
				Set objBtn = Description.Create() 

				If strBrowserType = "firefox" Then
					 objBtn("micClass").Value = "WebButton"
					 objBtn("type").Value = "xul:button"
					 objBtn("html tag").Value = "XUL:BUTTON"
					 strExpMicClass = "WebButton"
					 strInnerText = "value"
				End If

				If strBrowserType = "Iexplore" Then
					 objBtn("micClass").Value = "WinButton"
			    	 objBtn("object class").Value = "Button"
				     objBtn("nativeclass").Value = "Button"
					 strExpMicClass = "WinButton"
					 strInnerText = "text"
				End If

				Set objDialogChild = objDialog.ChildObjects
				intDialogChildCount = objDialogChild.Count
				For intFstCnt=0 To intDialogChildCount-1
					Set objTemp = objDialogChild(intFstCnt)
					strProp = objTemp.GetROProperty("micClass")
					If StrComp(strProp, strExpMicClass,1)=0 Then
						strBtnValue = objTemp.getROProperty(strInnerText) 
						strModifiedBtnName = Replace(strBtnValue,"&","")
						if UCase(Trim(strModifiedBtnName)) = UCase(Trim(strBtnName)) Then
							flag=0
							objBtn(strInnerText).Value = strBtnValue
							Set strExecutionBtnString=objDialog.WebButton(objBtn)
							Exit For
						Else
							flag = 1
						End If
					Else
						flag = 1
					End If
				Next
				
				If flag = 0 Then
					If CheckObjectClick(strExecutionBtnString) Then
						ClickButtonByNameInDialog = 0
						WriteStatusToLogs "Click operation is performed successfully."
					Else
						ClickButtonByNameInDialog = 2
						WriteStatusToLogs "The Action failed to perform the click operation successfully."
					End if
				ElseIf flag = 1 Then
					
					For intFstCnt = 0 To intDialogChildCount-1
						Set objChild = objDialogChild(intFstCnt).ChildObjects
						intChildCount = objChild.Count
						If intChildCount > 0 then
								For intIt = 0 To intChildCount-1
									strChildMic = objChild(intIt).getROProperty("micClass") 
									If UCase(Trim(strChildMic)) = Ucase(Trim(strExpMicClass)) Then
										strChildvalue = objChild(intIt).getROProperty(strInnerText) 
										strModifiedChildName = Replace(strChildvalue,"&","")
										If UCase(Trim(strModifiedChildName)) = UCase(Trim(strBtnName)) Then
											objBtn(strInnerText).Value = strChildvalue 
											Set strExecutionBtnString = objChild(intIt)
											blnFound = 0
											Exit For
										Else
											blnFound = 1
										End if
									Else
										blnFound = 1
									End if
									
								Next
						Else
								blnFound = 1
						End if
					Next
					
					If blnFound = 0 Then
						
						If CheckObjectClick(strExecutionBtnString) Then
							ClickButtonByNameInDialog = 0
							WriteStatusToLogs "Click operation is performed successfully."
						Else
							ClickButtonByNameInDialog = 2
							WriteStatusToLogs "The Action failed to perform the click operation successfully."
						End If
						
					ElseIf blnFound = 1 Then

						ClickButtonByNameInDialog = 1
						WriteStatusToLogs "The button does not exist."

					Else
						ClickButtonByNameInDialog = 1
						WriteStatusToLogs "An error occurred in the method '"&strMethod&_
								"'"
					End If
					
				Else
					ClickButtonByNameInDialog = 1
					WriteStatusToLogs "An error occurred in the method '"&strMethod&_
								"'"
				End If
				
			Else
				ClickButtonByNameInDialog = 1
				WriteStatusToLogs "An error occurred during the method, the browser type is not supported."
		
			End IF
		Else
			ClickButtonByNameInDialog = 1
			WriteStatusToLogs "An error occurred during the method , please check the parent object of the Dialog."
		End IF

	Else
		ClickButtonByNameInDialog = 1
		WriteStatusToLogs "The specified Dialog does not exist, please verify."
	End IF

On Error Goto 0
End Function




'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :				  CloseDialogIfExist()
' 	Purpose :					  The dialog is closed as many times it appears in specified time span.	
' 	Return Value :		 		  Integer(0/1)
' 	Arguments :
'		Input Arguments :  		  objDialog , timeout
'		Output Arguments :        
'	Function is called by :
'	Function calls :			  CheckObjectExist()
'	Created on :				  31/05/2011
'	Author :					  Amruta B
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function CloseDialogIfExist(ByVal objDialog,ByVal timeout)
	On Error Resume Next
		'Variable Section Begin
		   Dim strMethod
		   Dim sync , counter , intTimeout, intVal

		'Variable Section End
		strMethod = strKeywordForReporting
		intTimeout = SYNCTIMEINSECONDS

		If IsNull(timeout) Or Trim(timeout)= "" Then
				WriteStatusToLogs "The timeout data is '" & timeout &"'. Hence WaitTime parameter is considered " &_
									"as the default max sync time as per config setting :" & intTimeout & " seconds"

		ElseIf IsWholeNumber(timeout) = 0 Then

				intTimeout = CInt(timeout)

		Else
				CloseDialogIfExist = 1
				WriteStatusToLogs "The timeout " & timeout & " is not a valid positive integer. Please verify."
				Exit Function
		End if
			

		
		sync = 0
		counter = 0
		intTimeout = Cint(intTimeout)
		WriteStatusToLogs "Timeout = "& intTimeout &" seconds"
		Do
			If CheckObjectExist(objDialog) Then			   
				intVal=objDialog.GetROProperty("hwnd")
				Window("hwnd:=" & intVal).Close
				counter = counter + 1					
			End IF
			wait(5)
			sync = sync + 5
		Loop while (intTimeout > sync)
		
		CloseDialogIfExist = 0
		WriteStatusToLogs "The dialog existed "&counter&" times, and was closed when exist."	
			
	On Error GoTo 0
End Function