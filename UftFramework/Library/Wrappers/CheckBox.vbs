
'=======================Code Section Begin=============================================

Option Explicit 		'Forces explicit declaration of all variables in a script. 
'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :				  SelectCheckBox()
' 	Purpose :						Selects a specified checkbox.
' 	Return Value :		 		   Integer(0/1/2)
' 	Arguments :
'		Input Arguments :  		objCheckBox 
'		Output Arguments :      
'	Function is called by :
'	Function calls :				CheckObjectExist()
'	Created on :				   11/03/2008
'	Author :						  Rathna Reddy 
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function SelectCheckBox(ByVal objCheckBox)
	On Error Resume Next
		'Variable Section Begin
		Dim strMethod
		Dim  intActValue

		'Variable Section End
		strMethod = strKeywordForReporting
		 If CheckObjectExist(objCheckBox) Then
            If CheckObjectEnabled(objCheckBox) Then
				 intActValue = Cint(objCheckBox.GetROProperty("checked"))
				 If err.Number = 0 Then
						If intActValue = 0 Then
								objCheckBox.Set "On"
									intActValue = objCheckBox.GetROProperty("checked")
								If Err.Number = 0 Then
										If intActValue = 1 Then
											SelectCheckBox = 0
											WriteStatusToLogs	"The CheckBox is selected successfully."
										Else
											SelectCheckBox = 2
											WriteStatusToLogs	"The CheckBox is not selected successfully, please verify."
										End If
								Else
								   SelectCheckBox = 1
								   WriteStatusToLogs	"Could not capture checkbox state, please verify."

								End If
						Else
							SelectCheckBox = 1
							WriteStatusToLogs	"The CheckBox is already in checked state, please verify."
						End If
					Else
					  SelectCheckBox = 1
					  WriteStatusToLogs	"Could not capture checkbox state, please verify."
					End If
                Else
					SelectCheckBox = 1
					WriteStatusToLogs	"The CheckBox is not enabled, please verify."
				End If
					
			Else
				SelectCheckBox = 1
					WriteStatusToLogs	"The CheckBox does not exist, Please verify."
			End If
					
	 On Error GoTo 0
End Function



'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :				  ClearCheckBox()
' 	Purpose :						Clears a specified checkbox.
' 	Return Value :		 		   Integer(0/1/2)
' 	Arguments :
'		Input Arguments :  		objCheckBox 
'		Output Arguments : 
'	Function is called by :
'	Function calls :				CheckObjectExist()
'	Created on :				   11/03/2008
'	Author :						  Rathna Reddy 
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function ClearCheckBox(ByVal objCheckBox)
	On Error Resume Next
		'Variable Section Begin
		Dim strMethod
		Dim  intActValue

		'Variable Section End
		 strMethod = strKeywordForReporting
		 If CheckObjectExist(objCheckBox) Then
            If CheckObjectEnabled(objCheckBox) Then
				 intActValue = CInt(objCheckBox.GetROProperty("checked"))
				 If  err.Number = 0 Then
    					If intActValue = 1 Then
								objCheckBox.Set "Off"
									intActValue = objCheckBox.GetROProperty("checked")
									 If  err.Number = 0 Then
										If intActValue = 0 Then
											ClearCheckBox = 0
											WriteStatusToLogs "The CheckBox is de-selected successfully."
										Else
											ClearCheckBox = 2
											WriteStatusToLogs "The CheckBox is not de-selected, please verify."
										End If
									Else
										ClearCheckBox = 1
										  WriteStatusToLogs	"Could not capture checkbox state, please verify."

									End If
						Else
							ClearCheckBox = 1
							WriteStatusToLogs "The CheckBox is already de-selected, please verify."
						End If
					Else
						ClearCheckBox = 1
					    WriteStatusToLogs "Could not capture checkbox state, please verify."
						
					End If
                Else
					ClearCheckBox = 1
					WriteStatusToLogs "The CheckBox is not enabled, please verify."
				End If
					
			Else
				ClearCheckBox = 1
					WriteStatusToLogs "The CheckBox does not exist, please verify."
			End If
				
	 On Error GoTo 0
End Function
'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :				  VerifyCheckBoxChecked()
'	Purpose :					  Verifies whether the checkbox is in checked state
'	Return Value :		 		   Integer(0/1/2)
'	Arguments :
'		Input Arguments :  		  objCheckBox 
'		Output Arguments : 	      intActValue,intExpValue
'	Function is called by :
'	Function calls :				CheckObjectExist()
'	Created on :				   11/03/2008
'	Author :						  Rathna Reddy 
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function VerifyCheckBoxChecked(ByVal objCheckBox)
	On Error Resume Next
		'Variable Section Begin
			Dim strMethod
			Dim intActValue,intExpValue
                
		'Variable Section End	
			strMethod = strKeywordForReporting
			If CheckObjectExist(objCheckBox) Then
			    intExpValue = 1 
				 intActValue = objCheckBox.GetROProperty("checked") 
				 If CInt(intActValue) = CInt(intExpValue) Then
						VerifyCheckBoxChecked = 0
						WriteStatusToLogs 	"Specified CheckBox is checked as expected."
					Else
						VerifyCheckBoxChecked = 2
						WriteStatusToLogs 	"Specified CheckBox is not checked, please verify."
				End if
			
			Else
					VerifyCheckBoxChecked = 1
					WriteStatusToLogs	"The specified CheckBox does not exist, please verify."

			End If
		
	 On Error GoTo 0
End Function	 

'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :				  VerifyCheckBoxUnChecked()
'	Purpose :					  Verifies whether the checkbox is in UnChecked state
'	Return Value :		 		   Integer(0/1/2)
'	Arguments :
'		Input Arguments :  		objCheckBox 
'		Output Arguments : 	   intActValue,intExpVal
'	Function is called by :
'	Function calls :				CheckObjectExist()
'	Created on :				   11/03/2008
'	Author :						  Rathna Reddy 
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function VerifyCheckBoxUnChecked(ByVal objCheckBox)
	On Error Resume Next
		'Variable Section Begin
			Dim strMethod
			Dim intActValue,intExpValue
			
		'Variable Section End	
			strMethod = strKeywordForReporting
			If CheckObjectExist(objCheckBox) Then
			    intExpValue =0
				 intActValue = objCheckBox.GetROProperty("checked") 
				 If CInt(intActValue) = CInt(intExpValue) Then
						VerifyCheckBoxUnChecked = 0
						WriteStatusToLogs 	"Specified CheckBox is Un-checked as expected."
					Else
						VerifyCheckBoxUnChecked = 2
										WriteStatusToLogs 	"Specified CheckBox is checked, please Verify."
				End if
			
			Else
					VerifyCheckBoxUnChecked = 1
					WriteStatusToLogs	"The CheckBox does not exist, please verify."
		End If
		
	 On Error GoTo 0
End Function

'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name :				 VerifyCheckBoxEnabled()
' 	Purpose :					 Verifies whether the CheckBox is Enabled
' 	Return Value :		 		  Integer(0/1/2)
' 	Arguments :
'		Input Arguments :  		objCheckBox 
'		Output Arguments : 
'	Function is called by :
'	Function calls :				CheckObjectExist() , CheckObjectStatus()
'	Created on :				   11/03/2008
'	Author :						  Rathna Reddy 
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function VerifyCheckBoxEnabled(ByVal objCheckBox)

On Error Resume Next
	'Variable Section Begin
		
		Dim strMethod

	'Variable Section End
		strMethod = strKeywordForReporting
		If CheckObjectExist(objCheckBox) Then
			If CheckObjectEnabled(objCheckBox) Then
					VerifyCheckBoxEnabled = 0
					WriteStatusToLogs "The CheckBox is enabled as expected."
				Else
					VerifyCheckBoxEnabled = 2
					WriteStatusToLogs "The CheckBox is not enabled, please verify."
				End If
            
		Else
				VerifyCheckBoxEnabled = 1
				WriteStatusToLogs "The CheckBox does not exist, please verify."
		End if
		
	On Error GoTo 0
    
End Function

'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name :				 VerifyCheckBoxDisabled()
' 	Purpose :					 Verifies whether the CheckBox is Disabled
' 	Return Value :		 		  Integer(0/1/2)
' 	Arguments :
'		Input Arguments :  		objCheckBox 
'		Output Arguments : 
'	Function is called by :
'	Function calls :				CheckObjectExist() , CheckObjectStatus()
'	Created on :				   11/03/2008
'	Author :						  Rathna Reddy 
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function VerifyCheckBoxDisabled(ByVal objCheckBox)

On Error Resume Next
	'Variable Section Begin
		
		Dim strMethod

	'Variable Section End
		strMethod = strKeywordForReporting
		If CheckObjectExist(objCheckBox) Then
			If CheckObjectDisabled(objCheckBox) Then
					VerifyCheckBoxDisabled = 0
					WriteStatusToLogs "The CheckBox is disabled as expected."
				Else
					VerifyCheckBoxDisabled = 2
					WriteStatusToLogs "The CheckBox is not disabled, please verify."
				End If
            
		Else
				VerifyCheckBoxDisabled = 1
				WriteStatusToLogs "The CheckBox does not exist, please verify."
		End if
	On Error GoTo 0
End Function

'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :				 VerifyCheckBoxVisible()
'	Purpose :					 Verifies whether the CheckBox is visible.
'	Return Value :		 		  Integer(0/1/2)
'	Arguments :
'		Input Arguments :  		  objCheckBox 
'		Output Arguments : 
'	Function is called by :
'	Function calls :			 CheckObjectExist() , CheckObjectVisiblity()
'	Created on :				   10/03/2008
'	Author :						  Rathna Reddy 
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function VerifyCheckBoxVisible(ByVal objCheckBox)
	On Error Resume Next
		'Variable Section Begin
		
			Dim strMethod

		'Variable Section End
			strMethod = strKeywordForReporting
			If CheckObjectExist(objCheckBox)  Then 
				If CheckObjectVisible(objCheckBox) Then
					VerifyCheckBoxVisible = 0
					WriteStatusToLogs "The CheckBox is visible as expected."
				Else
					VerifyCheckBoxVisible = 2
					WriteStatusToLogs "The CheckBox is not visible, please verify."
				End If
			Else
					VerifyCheckBoxVisible = 1
					WriteStatusToLogs "The CheckBox does not exist, please verify."
			End If
		
	On Error GoTo 0
End Function
'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :				 VerifyCheckBoxInVisible()
'	Purpose :					 Verifies whether the CheckBox is Invisible.
'	Return Value :		 		  Integer(0/1/2)
'	Arguments :
'		Input Arguments :  		  objCheckBox 
'		Output Arguments : 
'	Function is called by :
'	Function calls :			 CheckObjectExist() , CheckObjectVisiblity()
'	Created on :				   10/03/2008
'	Author :						  Rathna Reddy 
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function VerifyCheckBoxInVisible(ByVal objCheckBox)
	On Error Resume Next
		'Variable Section Begin
		
			Dim strMethod

		'Variable Section End
			strMethod = strKeywordForReporting
			If CheckObjectExist(objCheckBox)  Then 
				If CheckObjectInVisible(objCheckBox) Then
					VerifyCheckBoxInVisible = 0
					WriteStatusToLogs "The CheckBox is not visible as expected."
				Else
					VerifyCheckBoxInVisible = 2
					WriteStatusToLogs "The CheckBox is visible, please verify."
				End If
			Else
					VerifyCheckBoxInVisible = 1
					WriteStatusToLogs "The CheckBox does not exist, please verify."
			End If
		
	On Error GoTo 0
End Function

'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :				 VerifyCheckBoxExist()
'	Purpose :					 Verifies whether the CheckBox Exists or not?
'	Return Value :		 		  Integer(0/2)
'	Arguments :
'		Input Arguments :  		  objCheckBox 
'		Output Arguments : 
'	Function is called by :
'	Function calls :			 CheckObjectExist() 
'	Created on :				   10/03/2008
'	Author :						  Rathna Reddy 
'
'------------------------------------------------------------------------------------------------------------------------------------------

Function VerifyCheckBoxExist(ByVal objCheckBox)
	On Error Resume Next
		'Variable Section Begin
	
		   Dim strMethod

		'Variable Section End
			strMethod = strKeywordForReporting
			If CheckObjectExist(objCheckBox) Then 
			   
					VerifyCheckBoxExist = 0
					WriteStatusToLogs "The CheckBox exists as expected."
					
			Else
					VerifyCheckBoxExist = 2
					WriteStatusToLogs "The CheckBox does NOT exist, please verify."
					
			End If 
	  
	
	On Error GoTo 0
End Function

'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :				 VerifyCheckBoxNotExist()
'	Purpose :					 Verifies whether the CheckBox Exists or not?
'	Return Value :		 		  Integer(0/2)
'	Arguments :
'		Input Arguments :  		  objCheckBox 
'		Output Arguments : 
'	Function is called by :
'	Function calls :			 CheckObjectExist() 
'	Created on :				   10/03/2008
'	Author :						  Rathna Reddy 
'
'------------------------------------------------------------------------------------------------------------------------------------------

Function VerifyCheckBoxNotExist(ByVal objCheckBox)
	On Error Resume Next
		'Variable Section Begin
	
		   Dim strMethod

		'Variable Section End
			strMethod = strKeywordForReporting
			If CheckObjectNotExist(objCheckBox) Then 
			   
					VerifyCheckBoxNotExist = 0
					WriteStatusToLogs "The CheckBox does not exist as expected"
					
			Else
					VerifyCheckBoxNotExist = 2
					WriteStatusToLogs "The CheckBox exists, please verify."
					
			End If 
	
  		
	On Error GoTo 0
End Function

'=======================Code Section End=============================================
