Const PROCESSRUNNING = "There is a running excel.exe process. Please close all open excel files/Excel Process from task manager before proceeding."
Const EXCELCLOSETIME = 1
Const IgnoreBlankColumn = True
Const IgnoreBlankRow = True
Const TRUE_ = "True"
Const FALSE_ = "False"
Const EXCELPROCESSNAME_ = "EXCEL.EXE"
KewWord = "Col::"
Overwrite = "Current"
Const maxRow = 100
Const maxCol = 50

Function WriteToColumn (fileName, sheetName, rowNumber, column, Data, Overwrite)
	On error resume next
	Dim strMethod, match, ColumnFound, objExcel, objWorkbook, objsheet, rows, cols, iCol, strValue, ColHeader, iCondition, condPair, cName, cVal, ColHeaderCond, CorrectData, countFound, msg
	Dim iRow, rowFound, foundCounter, procEndStatus, val, i
	err.number =0
	match =0
	ColumnFound = False
	countFound = 1
	msg = ""

	strMethod = strKeywordForReporting
	If isProcessRunning("EXCEL.exe") Then
		WriteToColumn = 1 'Invalid path
		WriteStatusToLogs PROCESSRUNNING
		Exit function	
	End if
	
	If not (IsNumeric(rowNumber) and cint(rowNumber)>0 ) Then       
		WriteToColumn = 1
		WriteStatusToLogs "Invalid rowNumber. Please enter a valid integer."
		Exit Function
	End If
	rowNumber = cint(rowNumber)
	err.number = 0
	Set objExcel = CreateObject("Excel.Application")   
	
	If err.number <> 0 Then
		WriteToColumn = 1 'Invalid path
		WriteStatusToLogs "Error while creating excel COM object. Verify if Excel is installed properly."
		Call WaitTillObjectReleasesMemory(objExcel)
		Exit function
	End If
	
	Set objWorkbook = objExcel.Workbooks.Open(fileName)

	If err.number <>0 Then
		WriteToColumn = 1 'Invalid path
		WriteStatusToLogs "Invalid excel file path: '"& fileName &"'"
		objWorkbook.Close
		objExcel.quit
		Set objWorkbook = Nothing
		Call WaitTillObjectReleasesMemory(objExcel)
		Exit function
	End If
	objExcel.visible=false		
	
	set objsheet = objWorkbook.Sheets.Item(sheetName)
	
	If err.number <>0 Then
		WriteToColumn = 1 'Invalid Sheet
		WriteStatusToLogs "Invalid sheet name: '"& sheetName &"'"
		objWorkbook.Close
		objExcel.quit
		set objsheet = Nothing
		Set objWorkbook =  Nothing
		Call WaitTillObjectReleasesMemory(objExcel)
		Exit function
	End If
	
	colNumber = cint(ResolveColumnNumber(objsheet,column))

	if cint(colNumber) < 1 then
		WriteToColumn = 1 'Invalid Sheet
		WriteStatusToLogs "Invalid columnID '"&column&"'. Please enter a valid input."
		objWorkbook.Close
		objExcel.quit
		set objsheet = Nothing
		Set objWorkbook =  Nothing
		Call WaitTillObjectReleasesMemory(objExcel)
		Exit function	
	End if
	
	If instr(1, Overwrite, "Next", vbtextcompare) Then
			colNumber = colNumber +1
			objsheet.Columns(colNumber).Insert()
			
	Elseif instr(1, Overwrite, "Previous", vbtextcompare) Then
			objsheet.Columns(colNumber).Insert()	
	Elseif instr(1, Overwrite, "Current", vbtextcompare) Then
		
	Elseif trim(Overwrite) <> "" Then
		WriteToColumn = 1 'Invalid Sheet
		WriteStatusToLogs "Invalid parameter 'Overwrite'. Please enter Next/Previous/Current. default is Current."
		objWorkbook.Close
		objExcel.quit
		set objsheet = Nothing
		Set objWorkbook =  Nothing
		Call WaitTillObjectReleasesMemory(objExcel)
		Exit function	
	End If 
	
	If isarray(Data) Then
	
		For i = lbound(Data) To ubound(Data)
				objsheet.Cells(cint(rowNumber+i),cint(colNumber)).value = Data(i)
		Next		
	Else
		objsheet.Cells(cint(rowNumber),cint((colNumber))).Value = Data
	End If
	
	objWorkbook.Save 	
	objWorkbook.Close
	objExcel.quit
	set objsheet = Nothing
	Set objWorkbook =  Nothing
	Set objExcel = nothing

	procEndStatus = WaitTillObjectReleasesMemory(objExcel)
		
	If err.number=0 and procEndStatus = 0 Then
			WriteToColumn = 0
			WriteStatusToLogs "Excel is written at row '"&rowNumber&"' and column '"&colNumber&"', Overwrite parameter is taken as '"& Overwrite&"'."

	Else
			WriteToColumn = 1
			WriteStatusToLogs "Error occurred while performing action on excel sheet. "&err.description
			
	End If

	On error goto 0
		
End function

Function WriteToRow (fileName, sheetName, rowNumber, column, Data, Overwrite)
	On error resume next
	Dim strMethod, match, ColumnFound, objExcel, objWorkbook, objsheet, rows, cols, iCol, strValue, ColHeader, iCondition, condPair, cName, cVal, ColHeaderCond, CorrectData, countFound, msg
	Dim iRow, rowFound, foundCounter, procEndStatus, val, i
	err.number =0
	match =0
	ColumnFound = False
	countFound = 1
	msg = ""

	strMethod = strKeywordForReporting
	If isProcessRunning("EXCEL.exe") Then
		WriteToRow = 1 'Invalid path
		WriteStatusToLogs PROCESSRUNNING
		Exit function	
	End if
	
	If not (IsNumeric(rowNumber) and cint(rowNumber)>0 ) Then       
		WriteToRow = 1
		WriteStatusToLogs "Invalid rowNumber. Please enter a valid integer."
		Exit Function
	End If
	rowNumber = cint(rowNumber)
	err.number = 0
	Set objExcel = CreateObject("Excel.Application")   
	
	If err.number <> 0 Then
		WriteToRow = 1 'Invalid path
		WriteStatusToLogs "Error while creating excel COM object. Verify if Excel is installed properly."
		Call WaitTillObjectReleasesMemory(objExcel)
		Exit function
	End If
	
	Set objWorkbook = objExcel.Workbooks.Open(fileName)

	If err.number <>0 Then
		WriteToRow = 1 'Invalid path
		WriteStatusToLogs "Invalid excel file path: '"& fileName &"'"
		objWorkbook.Close
		objExcel.quit
		Set objWorkbook = Nothing
		Call WaitTillObjectReleasesMemory(objExcel)
		Exit function
	End If
	objExcel.visible=false		
	
	set objsheet = objWorkbook.Sheets.Item(sheetName)
	
	If err.number <>0 Then
		WriteToRow = 1 'Invalid Sheet
		WriteStatusToLogs "Invalid sheet name: '"& sheetName &"'"
		objWorkbook.Close
		objExcel.quit
		set objsheet = Nothing
		Set objWorkbook =  Nothing
		Call WaitTillObjectReleasesMemory(objExcel)
		Exit function
	End If
	
	colNumber = cint(ResolveColumnNumber(objsheet,column))

	if cint(colNumber) < 1 then
		WriteToRow = 1 'Invalid Sheet
		WriteStatusToLogs "Invalid columnID '"&column&"'. Please enter a valid input."
		objWorkbook.Close
		objExcel.quit
		set objsheet = Nothing
		Set objWorkbook =  Nothing
		Call WaitTillObjectReleasesMemory(objExcel)
		Exit function	
	End if
	
	If instr(1, Overwrite, "Next", vbtextcompare) Then
			rowNumber = rowNumber +1
			objsheet.Rows(RowNumber).Insert()
			
	Elseif instr(1, Overwrite, "Previous", vbtextcompare) Then
			objsheet.Rows(RowNumber).Insert()
	Elseif instr(1, Overwrite, "Current", vbtextcompare) Then
		
	Elseif trim(Overwrite) <> "" Then
		WriteToRow = 1 'Invalid Sheet
		WriteStatusToLogs "Invalid parameter 'Overwrite'. Please enter Next/Previous/Current. default is Current."
		objWorkbook.Close
		objExcel.quit
		set objsheet = Nothing
		Set objWorkbook =  Nothing
		Call WaitTillObjectReleasesMemory(objExcel)
		Exit function
	End If 
	
	If isarray(Data) Then
	
		For i = lbound(Data) To ubound(Data)
				objsheet.Cells(cint(rowNumber),cint(colNumber+i)).value = Data(i)
				StartingColumnNumber = StartingColumnNumber +1
		Next		
	Else
		objsheet.Cells(cint(rowNumber),cint(colNumber)).Value = Data
	End If

	objWorkbook.Save 	
	objWorkbook.Close
	objExcel.quit
	set objsheet = Nothing
	Set objWorkbook =  Nothing
	Set objExcel = nothing
	procEndStatus = WaitTillObjectReleasesMemory(objExcel)
		
	If err.number=0 and procEndStatus = 0 Then
			WriteToRow = 0
			WriteStatusToLogs "Excel is written at row '"&rowNumber&"' and column '"&colNumber&"'."&"', Overwrite parameter is taken as '"& Overwrite&"'."

	Else
			WriteToRow = 1
			WriteStatusToLogs "Error occurred while performing action on excel sheet. "&err.description
			
	End If

	
	On error goto 0
		
End function

Function StoreCellData (fileName, sheetName, rowNumber, column, K)
	On error resume next
	Dim strMethod, match, ColumnFound, objExcel, objWorkbook, objsheet, rows, cols, iCol, strValue, ColHeader, iCondition, condPair, cName, cVal, ColHeaderCond, CorrectData, countFound, msg
	Dim iRow, rowFound, foundCounter, procEndStatus, val
	err.number =0
	match =0
	ColumnFound = False
	countFound = 1
	msg = ""

	strMethod = strKeywordForReporting
	If isProcessRunning("EXCEL.exe") Then
		StoreCellData = 1 'Invalid path
		WriteStatusToLogs PROCESSRUNNING
		Exit function	
	End if
	
	err.number = 0
	
	If not (IsNumeric(rowNumber) and cint(rowNumber)>0 ) Then       
		StoreCellData = 1
		WriteStatusToLogs "Invalid rowNumber. Please enter a valid integer."
		Exit Function
	End If
	Set objExcel = CreateObject("Excel.Application")   
	
	If err.number <> 0 Then
		StoreCellData = 1 'Invalid path
		WriteStatusToLogs "Error while creating excel COM object. Verify if Excel is installed properly."
		Call WaitTillObjectReleasesMemory(objExcel)
		Exit function
	End If
	
	Set objWorkbook = objExcel.Workbooks.Open(fileName)

	If err.number <>0 Then
		StoreCellData = 1 'Invalid path
		WriteStatusToLogs "Invalid excel file path: '"& fileName &"'"
		objWorkbook.Close
		objExcel.quit
		Set objWorkbook = Nothing
		Call WaitTillObjectReleasesMemory(objExcel)
		Exit function
	End If
	objExcel.visible=false		
	
	set objsheet = objWorkbook.Sheets.Item(sheetName)
	
	If err.number <>0 Then
		StoreCellData = 1 'Invalid Sheet
		WriteStatusToLogs "Invalid sheet name: '"& sheetName &"'"
		objWorkbook.Close
		objExcel.quit
		set objsheet = Nothing
		Set objWorkbook =  Nothing
		Call WaitTillObjectReleasesMemory(objExcel)
		Exit function
	End If
	
	colNumber = ResolveColumnNumber(objsheet,column)

		
	If colNumber>0 Then
		val = objsheet.Cells(cint(rowNumber),cint(colNumber)).value
		
		if isempty(val) or isnull(val) or val = "" Then
			val = ""
		End if
		
		Call AddItemToStorageDictionary(K,val)
	
		procEndStatus = WaitTillObjectReleasesMemory(objExcel)
		
		If err.number=0 and procEndStatus = 0 Then
				StoreCellData = 0
				WriteStatusToLogs "Key '"&K&"' is stored with data '" & val &"'."
	
		Else
				StoreCellData = 1
				WriteStatusToLogs "Error occurred while reading excel file. "&err.description
				
		End If
	else
				StoreCellData = 1
				WriteStatusToLogs "Invalid columnID '"&err.description & "'.  Please enter a valid input."

	End If
	
	objWorkbook.Close
	objExcel.quit
	set objsheet = Nothing
	Set objWorkbook =  Nothing
	Set objExcel = nothing
		
End function

'internal
function ResolveColumnNumber(objsheet,strData)
	On error resume next
	Dim regEx, Matches, colNumber, columnName, cols
	Set regEx = New RegExp
	regEx.IgnoreCase = True
	regEx.Global = False
	regEx.Pattern = "^"&KewWord&"\s*([0-9]*|[A-Z,a-z])*$"
	regEx.IgnoreCase = false         ' Set case insensitivity.
	regEx.Global = false         ' Set global applicability.
	Set Matches = regEx.Execute(strData)   ' Execute search.
	
	If Matches.count = 1 Then
		strData = mid(strData,len(KewWord)+1, len(strData)-len(KewWord))
		strData = trim(strData)
		colNumber = resolveColumn(objsheet,strData)
	ElseIf instr(1, mid(strData, 1, len(KewWord)), KewWord, vbTextCompare) > 0 Then
		colNumber = "-1"
	Else
		columnName =  strData
		cols = objsheet.UsedRange.Columns.count
		colNumber = getColNumber (objsheet, strData)
	End If 
	
	ResolveColumnNumber = colNumber
    On error goto 0
End function

'internal
function getColNumber (objsheet, ColumnName)
	 On error resume next
		Dim strValue, iCol, ColumnFound
		cols = objsheet.UsedRange.Columns.count
		For iCol = 1 To cols
			 strValue=CStr(objsheet.Cells( 1, cint(iCol)))
			 cols = objsheet.UsedRange.Columns.count					
			If (trim(strValue) = trim(ColumnName)) Then
					getColNumber = iCol
					Exit function
			End If
		Next
		getColNumber = "-1"
      On error goto 0	
End function

'internal
Function resolveColumn (objsheet, strData)
	On error resume next
	Dim regEx, Matches
	Set regEx = New RegExp
	regEx.IgnoreCase = True
	regEx.Global = False
	regEx.Pattern = "^\d*$"
	regEx.IgnoreCase = false         ' Set case insensitivity.
	regEx.Global = false         ' Set global applicability.
	Set Matches = regEx.Execute(strData)   ' Execute search.
	
	If Matches.count = 1 Then
		strData = strData
	Else
		strData = trim(strData)
		strData = LetterToNumber(objsheet, strData)
	End If 
	resolveColumn = strData
	 On error goto 0
End Function

'internal
function LetterToNumber(sheet, letters)
	On error resume next
	Dim lng
	lng = clng(lng)
	lng = sheet.Cells(1, letters).Column
	If err.number <> 0 Then
	lng = "-1"
	End If
	LetterToNumber = lng
	On error goto 0
End function

function StoreColumnDataOccurance(fileName,sheetName, Column,data, K) 
	On error resume next
	Dim strMethod, match, ColumnFound, objExcel, objWorkbook, objsheet, rows, cols, iCol, strValue, ColHeader, iCondition, condPair, cName, cVal, ColHeaderCond, CorrectData, countFound, msg
	Dim iRow, rowFound, foundCounter, procEndStatus, colNumber
	err.number =0
	match =0
	ColumnFound = False
	countFound = 1
	msg = ""

	strMethod = strKeywordForReporting
	If isProcessRunning("EXCEL.exe") Then
		StoreColumnDataOccurance = 1 'Invalid path
		WriteStatusToLogs PROCESSRUNNING
		Exit function	
	End if
	
	err.number = 0
	Set objExcel = CreateObject("Excel.Application")   
	
	If err.number <> 0 Then
		StoreColumnDataOccurance = 1 'Invalid path
		WriteStatusToLogs "Error while creating excel COM object. Verify if Excel is installed properly."
		Call WaitTillObjectReleasesMemory(objExcel)
		Exit function
	End If
	
	Set objWorkbook = objExcel.Workbooks.Open(fileName)

	If err.number <>0 Then
		StoreColumnDataOccurance = 1 'Invalid path
		WriteStatusToLogs "Invalid excel file path: '"& fileName &"'"
		objWorkbook.Close
		objExcel.quit
		Set objWorkbook = Nothing
		Call WaitTillObjectReleasesMemory(objExcel)
		Exit function
	End If
	objExcel.visible=false		
	
	set objsheet = objWorkbook.Sheets.Item(sheetName)
	
	If err.number <>0 Then
		StoreColumnDataOccurance = 1 'Invalid Sheet
		WriteStatusToLogs "Invalid sheet name: '"& sheetName &"'"
		objWorkbook.Close
		objExcel.quit
		set objsheet = Nothing
		Set objWorkbook =  Nothing
		Call WaitTillObjectReleasesMemory(objExcel)
		Exit function
	End If
	
	'---------------------
	rows = objsheet.UsedRange.Rows.count
	
	colNumber = ResolveColumnNumber(objsheet,column)
	
	if cint(colNumber) < 1 then
		StoreColumnDataOccurance = 1 'Invalid Sheet
		WriteStatusToLogs "Invalid columnID '"&column&"'.  Please enter a valid input."
		objWorkbook.Close
		objExcel.quit
		set objsheet = Nothing
		Set objWorkbook =  Nothing
		Call WaitTillObjectReleasesMemory(objExcel)
		Exit function	
	End if
	
	For irow = 1 To rows
		 'strValue=CStr(objsheet.Cells( cint(irow), cint(colNumber))).value
		 strValue=objsheet.Cells(cint(irow), cint(colNumber)).value
		If (trim(strValue) = trim(data)) Then
			foundCounter = foundCounter + 1
		End If
	Next
		
	
	objWorkbook.Save 
	objWorkbook.Close
	objExcel.quit
	set objsheet = Nothing
	Set objWorkbook = nothing
	
	If isempty(foundCounter) or foundCounter = "" Then
		foundCounter = 0
	End if
	Call AddItemToStorageDictionary(K,foundCounter)
	
	procEndStatus = WaitTillObjectReleasesMemory(objExcel)
	
	If err.number=0 and procEndStatus = 0 Then
			StoreColumnDataOccurance = 0
			WriteStatusToLogs "Key '"&K&"' is stored with occurrence count '" & foundCounter &"'."

	Else
			StoreColumnDataOccurance = 1
			WriteStatusToLogs "Error occurred while performing action on excel sheet. "&err.description
			
	End If
	
	on error goto 0
	
End  function

Function Excel_StoreRowCount(K, fileName,sheetName, ignoreBlankRows) 
	On error resume next
	Dim strMethod, match, ColumnFound, objExcel, objWorkbook, objsheet, rows, cols, iCol, strValue, ColHeader, iCondition, condPair, cName, cVal, ColHeaderCond, CorrectData
	Dim iRow, rowFound, procEndStatus, blankRows, rowCount, startCol, startRow
	err.number =0
	match =0
	ColumnFound = False
	blankRows = 0
	strMethod = strKeywordForReporting	
	If isProcessRunning("EXCEL.exe") Then
		Excel_StoreRowCount = 1 'Invalid path
		WriteStatusToLogs PROCESSRUNNING
		Exit function	
	End if
	
	If (instr(1, ignoreBlankRows, TRUE_, vbTextCompare) < 1) AND (instr(1, ignoreBlankRows, FALSE_, vbTextCompare) < 1) AND (ignoreBlankRows <> "") Then
		Excel_StoreRowCount = 1 'Invalid path
		WriteStatusToLogs "Invalid parameter ignoreBlankRows, Allowed values for this parameter is 'True/False or blank' ."
		Exit function	
	End if
	
	If ignoreBlankRows = "" Then
		ignoreBlankRows = IgnoreBlankRow
	End If
	
	Set objExcel = CreateObject("Excel.Application")   
	Set objWorkbook = objExcel.Workbooks.Open(fileName)

	If err.number <>0 Then
		Excel_StoreRowCount = 1 'Invalid path
		WriteStatusToLogs "Invalid excel file path: '"& fileName &"'"
		objWorkbook.Close
		objExcel.quit
		Set objWorkbook = Nothing
		Set objExcel = nothing
		Call WaitTillObjectReleasesMemory(objExcel)
		Exit function
	End If
	objExcel.visible=false		
	
	set objsheet = objExcel.Sheets.Item(sheetName)
	
	If err.number <> 0 Then
		Excel_StoreRowCount = 1 'Invalid Sheet
		WriteStatusToLogs "Invalid sheet name: '"& sheetName &"'"
		objWorkbook.Close
		objExcel.quit
		Set objWorkbook = Nothing
		Set objExcel = nothing
		Call WaitTillObjectReleasesMemory(objExcel)
		Exit function
	End If


	rows = objsheet.UsedRange.Rows.count
	cols = objsheet.UsedRange.Columns.count	
	startCol = getFirstNonBlanktColumn (objsheet)
	startRow = getFirstNonBlanktRow (objsheet, row, cols+startCol)
	
	If (startCol = -1) or (startRow = -1) then
		rowCount = 0
	Else
		If instr(1, ignoreBlankRows, IgnoreBlankRow, vbTextCompare) > 0 Then
			For r = startRow To rows+startRow-1
				If isRowBlank(objsheet, r, cols) Then
					blankRows = blankRows + 1
				End If
			Next
			'rowCount = rows - blankRows+1
			if blankRows <> 0 then
			'rowCount = rows - blankRows+1
			rowCount = rows - blankRows
			Else
			rowCount = rows
			End if

		Else
			rowCount = rows
		End If

	End if

	if rowCount = "" or isempty(rowCount) then
		rowCount = 0
	End if
	
	Call AddItemToStorageDictionary(K,rowCount)
	
	objExcel.quit
	set objsheet = Nothing
	Set objWorkbook = nothing
	Set objExcel = nothing
	procEndStatus = WaitTillObjectReleasesMemory(objExcel)
	
	If  err.number=0 and procEndStatus = 0 or rowCount = 0 Then
			Excel_StoreRowCount = 0
			WriteStatusToLogs "Key '"&K&"' is stored with row count '"&rowCount&"'. "&"', IgnoreBlankRow parameter is taken as '"& ignoreBlankRows&"'."
			
	Elseif instr(err.description,"Not found", vbtextcompare) > 0 then
			Excel_StoreRowCount = 0
			WriteStatusToLogs "Key '"&K&"' is stored with row count '"&rowCount&"'. "&"', IgnoreBlankRow parameter is taken as '"& ignoreBlankRows&"'."
				
	Else
			Excel_StoreRowCount = 1
			WriteStatusToLogs "Error occurred while getting the row count "&err.description
						
	End If
	
	On error goto 0
End Function

Function isRowBlank(objsheet, row, cols)
	Dim c
	For c = 1 To cols
		if objsheet.Cells(cint(row),cint(c)).value <> "" Then
			isRowBlank = false
			Exit function
			else
			isRowBlank = true
		End If 
	Next
End Function

Function Excel_StoreColumnCount_ARCHIEVE(K,fileName,sheetName,ignoreBlankColumns) 
	On error resume next
	Dim strMethod, match, ColumnFound, objExcel, objWorkbook, objsheet, rows, cols, strValue, ColHeader, iCondition, condPair, cName, cVal, ColHeaderCond, CorrectData
	Dim iRow, rowFound, procEndStatus, getHeaderVal, i, colCount
	err.number =0
	match =0
	ColumnFound = False
	strMethod = strKeywordForReporting	
	If isProcessRunning("EXCEL.exe") Then
		Excel_StoreColumnCount = 1 'Invalid path
		WriteStatusToLogs PROCESSRUNNING
		Exit function	
	End if
	
	If (instr(1, ignoreBlankColumns, TRUE_, vbTextCompare) < 1) AND (instr(1, ignoreBlankColumns, FALSE_, vbTextCompare)) < 1 AND (ignoreBlankColumns <> "") Then
		Excel_StoreColumnCount = 1 'Invalid path
		WriteStatusToLogs "Invalid parameter ignoreBlankColumns, Allowed values for this parameter is 'True/False or blank' ."
		Exit function	
	End if	
	
	If ignoreBlankColumns = "" Then
		ignoreBlankColumns = IgnoreBlankColumn
	End If
	
	Set objExcel = CreateObject("Excel.Application")   
	Set objWorkbook = objExcel.Workbooks.Open(fileName)

	If err.number <>0 Then
		Excel_StoreColumnCount = 1 'Invalid path
		WriteStatusToLogs "Invalid excel file path: '"& fileName &"'"
		objWorkbook.Close
		objExcel.quit
		Set objWorkbook = Nothing
		Call WaitTillObjectReleasesMemory(objExcel)
		Exit function
	End If
	objExcel.visible=false		
	
	set objsheet = objExcel.Sheets.Item(sheetName)
	
	If err.number <>0 Then
		Excel_StoreColumnCount = 1 'Invalid Sheet
		WriteStatusToLogs "Invalid sheet name: '"& sheetName &"'"
		objWorkbook.Close
		objExcel.quit
		Set objWorkbook = Nothing
		Call WaitTillObjectReleasesMemory(objExcel)
		Exit function
	End If
	cols = objsheet.UsedRange.Columns.count
	
	If instr(1, ignoreBlankColumns, IgnoreBlankColumn, vbTextCompare) > 0 Then
		For i = 1 To cols
			getHeaderVal = objsheet.Cells( 1, cint(i)).value
			If getHeaderVal <> "" Then
				colCount = colCount + 1
			End If
	    Next
	Else
		colCount = cols
	End If
	
	if colCount = "" or isempty(colCount) then
		colCount = 0
	End if
	
	Call AddItemToStorageDictionary(K,colCount)
	objExcel.quit
	set objsheet = Nothing
	Set objWorkbook = nothing

	procEndStatus = WaitTillObjectReleasesMemory(objExcel)

	If err.number=0 and procEndStatus = 0 Then
			Excel_StoreColumnCount = 0
			WriteStatusToLogs "Key '"&K&"' is stored with column count '"&colCount&"'. "&"', IgnoreBlankColumn parameter is taken as '"& ignoreBlankColumns&"'."
			
	Else
			Excel_StoreColumnCount = 1
			WriteStatusToLogs "Error occurred while getting the column count "&err.description
						
	End If
	
	On error goto 0
End Function

Function ReadCelldataFromExcelFile(fileName,sheetName, colName, Condition , saperator, strkey) '>>>>>>>>>> WriteToRow
	On error resume next
	Dim strMethod, match, ColumnFound, objExcel, objWorkbook, objsheet, rows, cols, iCol, strValue, ColHeader, iCondition, condPair, cName, cVal, ColHeaderCond, CorrectData
	Dim iRow, rowFound, procEndStatus
	err.number =0
	match =0
	ColumnFound = False
	strMethod = strKeywordForReporting
	If isProcessRunning("EXCEL.exe") Then
		ReadCelldataFromExcelFile = 1 'Invalid path
		WriteStatusToLogs PROCESSRUNNING
		Exit function	
	End if
	
	Set objExcel = CreateObject("Excel.Application")   
	Set objWorkbook = objExcel.Workbooks.Open(fileName)
	strMethod = strKeywordForReporting
	If err.number <>0 Then
		ReadCelldataFromExcelFile = 1 'Invalid path
		WriteStatusToLogs "Invalid excel file path: '"& fileName &"'"
		Exit function
	End If
	objExcel.visible=false		
	
	set objsheet = objExcel.Sheets.Item(sheetName)
	
	If err.number <>0 Then
		ReadCelldataFromExcelFile = 1 'Invalid Sheet
		WriteStatusToLogs "Invalid sheet name: '"& sheetName &"'"
		Exit function
	End If
	
	rows = objsheet.UsedRange.Rows.count
	cols = objsheet.UsedRange.Columns.count
			
	For iCol = 1 To cols
		 strValue=CStr(objsheet.Cells( 1, iCol))
				
		If trim(strValue) = trim(colName) Then
			ColHeader = iCol
			ColumnFound = true
			Exit for 
		End If
	Next
		
	If ColumnFound = False Then
		ReadCelldataFromExcelFile = 1 'Invalid column name
		WriteStatusToLogs "Invalid Column: '"& colName &"'"
		Exit function
	End If
	
	If isarray(Condition)  Then
	
		For iCondition = LBound(Condition) To UBound(Condition)
			condPair = 	Condition(iCondition)
			cName = split(condPair, saperator)(0)
			cVal = split(condPair, saperator)(1)
			
			For iCol = 1 To cols
				 strValue=CStr(objsheet.Cells( 1, iCol))
						
				If trim(strValue) = trim(cName) Then
					ColHeaderCond = iCol
					
					ColumnFound = true
					Exit for 
				End If
			Next
			
			
			If not rowFound Then
				For iRow = 1 To rows
					 strValue=CStr(objsheet.Cells(iRow,ColHeaderCond))
							
					If trim(strValue) = trim(cVal) Then
						match = match+1
						rowFound = iRow
						rowFound = true
						Exit for 
					ElseIf (iRow = rows) and (rowFound = false) Then
						ReadCelldataFromExcelFile = 1 'Row not found
						WriteStatusToLogs "Row not found with the given condition. verify details."
						Exit function
					End If
				Next
			Else 
				cellVal = objsheet.Cells(iRow,ColHeaderCond)
				
					If trim(cVal) = trim(cellVal) Then
						match = match +1
						rowFound = iRow
						rowFound = true
					Else
						ReadCelldataFromExcelFile = 1 'Row not found
						WriteStatusToLogs "Row not found with the given condition. verify details."
						Exit function
					End If
			End If
		Next
	End If
	
	If UBound(Condition) = match-1 Then
		CorrectData =CStr(objsheet.Cells(iRow, ColHeader))
		Call AddItemToStorageDictionary(strKey,CorrectData)
		If err.number=0 Then
				ReadCelldataFromExcelFile = 0
				WriteStatusToLogs "Key '"&strKey&"' is stored with the cell data '"&CorrectData&"'. Row number "&iRow
				Exit function
		Else
				ReadCelldataFromExcelFile = 1
				WriteStatusToLogs "Row found but error occurred while writing data to key. Row number: "&iRow
				Exit function				
		End If
	Else
				ReadCelldataFromExcelFile = 1 'Row not found
				WriteStatusToLogs "Row not found with the given condition. verify details."
				Exit function
	End If
	On error goto 0
End Function

Function Excel_WriteCellByIndex_REMOVE(fileName,sheetName, RowNumber, ColumnNumber, Data) '>>>>>>>>>>  StoreCellData
	On error resume next
	Dim strMethod, match, ColumnFound, objExcel, objWorkbook, objsheet, rows, cols, iCol, strValue, ColHeader, iCondition, condPair, cName, cVal, ColHeaderCond, CorrectData
	Dim iRow, rowFound, procEndStatus
	err.number =0
	match =0
	ColumnFound = False
	err.number = 0
	strMethod = strKeywordForReporting
	If isProcessRunning("EXCEL.exe") Then
		Excel_WriteCellByIndex = 1 'Invalid path
		WriteStatusToLogs PROCESSRUNNING
		Exit function	
	End if
	
	Set objExcel = CreateObject("Excel.Application")   
	
	If err.number <> 0 Then
		Excel_WriteCellByIndex = 1 'Invalid path
		WriteStatusToLogs "Error while creating excel COM object. Verify if Excel is installed properly."
		Set objExcel = Nothing
		Exit function
	End If
	Set objWorkbook = objExcel.Workbooks.Open(fileName)

	If err.number <>0 Then
		Excel_WriteCellByIndex = 1 'Invalid path
		WriteStatusToLogs "Invalid excel file path: '"& fileName &"'"
		objWorkbook.Close
		objExcel.quit
		Set objExcel = Nothing
		Exit function
	End If
	objExcel.visible=false		
	
	set objsheet = objWorkbook.Sheets.Item(sheetName)
	
	If err.number <>0 Then
		Excel_WriteCellByIndex = 1 'Invalid Sheet
		WriteStatusToLogs "Invalid sheet name: '"& sheetName &"'"
		objWorkbook.Close
		objExcel.quit
		set objsheet = Nothing
		Set objWorkbook = nothing
		Set objExcel = Nothing
		Exit function
	End If
	
	If RowNumber <> "" Then
	     	RowNumber = cint(RowNumber)		
	End If

	If (Not(IsNumeric(RowNumber) AND (CLng(RowNumber) = RowNumber))) or (err.number<>0) or (RowNumber < 1) Then
		Excel_WriteCellByIndex = 1 'Invalid Sheet
		WriteStatusToLogs "Invalid row number"& RowNumber &"'. Row number should be integer starting from 1."

		objWorkbook.Close
		objExcel.quit
		set objsheet = Nothing
		Set objWorkbook = nothing
		Set objExcel = Nothing
		Exit function
	End If

	If ColumnNumber <> "" Then
	     	ColumnNumber = cint(ColumnNumber)		
	End If
	If (Not(IsNumeric(ColumnNumber) AND (CLng(ColumnNumber) = ColumnNumber))) or (err.number<>0) or (ColumnNumber < 1) Then
		Excel_WriteCellByIndex = 1 'Invalid Sheet
		WriteStatusToLogs "Invalid column number"& ColumnNumber &"'. Column number should be integer starting from 1."

		objWorkbook.Close
		objExcel.quit
		set objsheet = Nothing
		Set objWorkbook = nothing
		Set objExcel = Nothing
		Exit function
	End If
	
	objsheet.Cells(RowNumber,ColumnNumber).Value = Data
	objWorkbook.Save 
	objWorkbook.Close
	objExcel.quit
	set objsheet = Nothing
	Set objWorkbook = nothing
	procEndStatus = WaitTillObjectReleasesMemory(objExcel)

	If err.number=0 and procEndStatus = 0 Then
			Excel_WriteCellByIndex = 0
			WriteStatusToLogs "Data '"&Data&"' is written to the excel sheet at row number: '"&RowNumber&"' and column number: '"&ColumnNumber&"'."

	Else
			Excel_WriteCellByIndex = 1
			WriteStatusToLogs "Error occurred during  writing to excel sheet. "&err.description
			
	End If

	On error goto 0
End Function

Function Excel_WriteCellByColumnName_REMOVE(fileName,sheetName, RowNumber, ColumnName, Data, ColumnIndex ) 
	On error resume next
	Dim strMethod, match, ColumnFound, objExcel, objWorkbook, objsheet, rows, cols, iCol, strValue, ColHeader, iCondition, condPair, cName, cVal, ColHeaderCond, CorrectData, countFound, msg
	Dim iRow, rowFound, procEndStatus
	err.number =0
	match =0
	ColumnFound = False
	countFound = 1
	msg = ""
	strMethod = strKeywordForReporting
	If isProcessRunning("EXCEL.exe") Then
		Excel_WriteCellByColumnName = 1 'Invalid path
		WriteStatusToLogs PROCESSRUNNING
		Exit function	
	End if
	
	err.number = 0
	Set objExcel = CreateObject("Excel.Application")   
	
	If err.number <> 0 Then
		Excel_WriteCellByColumnName = 1 'Invalid path
		WriteStatusToLogs "Error while creating excel COM object. Verify if Excel is installed properly."
		Set objExcel = Nothing
		Exit function
	End If
	
	Set objWorkbook = objExcel.Workbooks.Open(fileName)

	If err.number <>0 Then
		Excel_WriteCellByColumnName = 1 'Invalid path
		WriteStatusToLogs "Invalid excel file path: '"& fileName &"'"
		objWorkbook.Close
		objExcel.quit
		Set objWorkbook = Nothing
		Set objExcel = Nothing
		Exit function
	End If
	objExcel.visible=false		
	
	set objsheet = objWorkbook.Sheets.Item(sheetName)
	
	If err.number <>0 Then
		Excel_WriteCellByColumnName = 1 'Invalid Sheet
		WriteStatusToLogs "Invalid sheet name: '"& sheetName &"'"
		objWorkbook.Close
		objExcel.quit
		set objsheet = Nothing
		Set objWorkbook =  Nothing
		Set objExcel = Nothing
		Exit function
	End If
	
	If RowNumber <> "" Then
	     	RowNumber = cint(RowNumber)		
	End If
	If (Not(IsNumeric(RowNumber) AND (CLng(RowNumber) = RowNumber))) or (err.number<>0) or (RowNumber < 1) Then
		Excel_WriteCellByColumnName = 1 'Invalid Sheet
		WriteStatusToLogs "Invalid row number"& RowNumber &"'. Row number should be integer starting from 1."

		objWorkbook.Close
		objExcel.quit
		set objsheet = Nothing
		Set objWorkbook = nothing
		Set objExcel = Nothing
		Exit function
	End If
	
	If ColumnIndex <> "" Then
	     	ColumnIndex = cint(ColumnIndex)		
	End If
	If ColumnIndex = "" Then
		ColumnIndex = 1
	ElseIf (Not(IsNumeric(ColumnIndex) AND (CLng(ColumnIndex) = ColumnIndex))) or (err.number<>0) or (ColumnIndex < 1) Then

		Excel_WriteCellByColumnName = 1 'Invalid Sheet
		WriteStatusToLogs "Invalid column Index"& ColumnIndex &"'. Column index should be integer starting from 1."

		objWorkbook.Close
		objExcel.quit
		set objsheet = Nothing
		Set objWorkbook = nothing
		Set objExcel = Nothing
		Exit function
	End If

		'---------------------
	cols = objsheet.UsedRange.Columns.count
			
	For iCol = 1 To cols
		 strValue=CStr(objsheet.Cells( 1, iCol))
				
		If (trim(strValue) = trim(ColumnName)) Then
			If ColumnIndex <> countFound Then
				countFound= countFound+1
			Else
				ColHeader = iCol
				ColumnFound = true
				Exit for 
			End If

		End If
	Next
		
	If ColumnFound = False Then
		ReadCelldataFromExcelFile = 1 'Invalid column name
		WriteStatusToLogs "Invalid column name or column index."
		 	objWorkbook.Save 
		objWorkbook.Close
		objExcel.quit
		set objsheet = Nothing
		Set objWorkbook = nothing
		Set objExcel = Nothing
		Exit function
	End If
	
	'---------------------

	ColumnNumber = cint(ColHeader)
	
	objsheet.Cells(RowNumber,ColumnNumber).Value = Data
	objWorkbook.Save 
	objWorkbook.Close
	objExcel.quit
	set objsheet = Nothing
	Set objWorkbook = nothing
	procEndStatus = WaitTillObjectReleasesMemory(objExcel)

	If err.number=0 and procEndStatus = 0 Then
			Excel_WriteCellByColumnName = 0
			WriteStatusToLogs "Data '"&Data&"' is written to the excel sheet at row number: '"&RowNumber&"' and column number: '"&ColumnNumber&"'." & "<b>Switch to ""EXCEL.WriteCellByIndex"" action for better performance while writing to excel files.</b>"

	Else
			Excel_WriteCellByColumnName = 1
			WriteStatusToLogs "Error occurred during  writing to excel sheet. "&err.description
			
	End If


	On error goto 0
End Function

Function Excel_WriteAllColumnsByRowNumber_REMOVE(fileName,sheetName, RowNumber, Data, StartingColumnNumber) 
	On error resume next
	Dim strMethod, match, ColumnFound, objExcel, objWorkbook, objsheet, rows, cols, iCol, strValue, ColHeader, iCondition, condPair, cName, cVal, ColHeaderCond, CorrectData
	Dim iRow, rowFound, countFound, procEndStatus
	err.number =0
	match =0
	ColumnFound = False
	countFound = 1
	strMethod = strKeywordForReporting	
	If isProcessRunning("EXCEL.exe") Then
		Excel_WriteAllColumnsByRowNumber = 1 'Invalid path
		WriteStatusToLogs PROCESSRUNNING
		Exit function	
	End if

	err.number = 0
	Set objExcel = CreateObject("Excel.Application")   
	
	If err.number <> 0 Then
		Excel_WriteAllColumnsByRowNumber = 1 'Invalid path
		WriteStatusToLogs "Error while creating excel COM object. Verify if Excel is installed properly."
		Set objExcel = Nothing
		Exit function
	End If
	
	Set objWorkbook = objExcel.Workbooks.Open(fileName)

	If err.number <>0 Then
		Excel_WriteAllColumnsByRowNumber = 1 'Invalid path
		WriteStatusToLogs "Invalid excel file path: '"& fileName &"'"
		objWorkbook.Close
		objExcel.quit
		Set objWorkbook = Nothing
		Set objExcel = Nothing
		Exit function
	End If
	objExcel.visible=false		
	
	set objsheet = objWorkbook.Sheets.Item(sheetName)
	
	If err.number <>0 Then
		Excel_WriteAllColumnsByRowNumber = 1 'Invalid Sheet
		WriteStatusToLogs "Invalid sheet name: '"& sheetName &"'"
		objWorkbook.Close
		objExcel.quit
		set objsheet =  Nothing
		Set objWorkbook = Nothing
		Set objExcel = Nothing
		Exit function
	End If
	
	If StartingColumnNumber <> "" Then
	     StartingColumnNumber = cint(StartingColumnNumber)
	End If

	If StartingColumnNumber = "" Then
		StartingColumnNumber = 1
	ElseIf (Not(IsNumeric(StartingColumnNumber) AND (CLng(StartingColumnNumber) = StartingColumnNumber))) or (err.number<>0) or (StartingColumnNumber < 1) Then

		Excel_WriteAllColumnsByRowNumber = 1 'Invalid Sheet
		WriteStatusToLogs "Invalid StartingColumnNumber "& StartingColumnNumber &"'. it should be integer starting from 1."

		objWorkbook.Close
		objExcel.quit
		set objsheet = Nothing
		Set objWorkbook = nothing
		Set objExcel = Nothing
		Exit function
	End If
	
	If RowNumber <> "" Then
		RowNumber = cint(RowNumber)
	End If
	
	If (Not(IsNumeric(RowNumber) AND (CLng(RowNumber) = RowNumber))) or (err.number<>0) or (RowNumber < 1)Then

		Excel_WriteAllColumnsByRowNumber = 1 'Invalid Sheet
		WriteStatusToLogs "Invalid RowNumber "& RowNumber &"'. it should be integer starting from 1."

		objWorkbook.Close
		objExcel.quit
		set objsheet = Nothing
		Set objWorkbook = nothing
		Set objExcel = Nothing
		Exit function
	End If
	
	
	For i = lbound(Data) To ubound(Data)
			objsheet.Cells(RowNumber,StartingColumnNumber).Value = Data(i)
			StartingColumnNumber = StartingColumnNumber +1
	Next
	
	objWorkbook.Save 
	objWorkbook.Close
	objExcel.quit
	set objsheet = Nothing
	Set objWorkbook = nothing
	procEndStatus = WaitTillObjectReleasesMemory(objExcel)

	If err.number=0 and procEndStatus = 0 Then
			Excel_WriteAllColumnsByRowNumber = 0
			WriteStatusToLogs "Data is written to excel sheet at row number: "& RowNumber
			'Exit function
	Else
			Excel_WriteAllColumnsByRowNumber = 1
			WriteStatusToLogs "Error occurred during  writing to excel sheet. "&err.description
			'Exit function				
	End If

	'Set objExcel = Nothing
	On error goto 0
End Function

Function Excel_WriteAllRowsByColumnIndex_REMOVE(fileName,sheetName, ColumnNumber, Data, StartingRowNumber) 
	On error resume next
	Dim strMethod, match, ColumnFound, objExcel, objWorkbook, objsheet, rows, cols, iCol, strValue, ColHeader, iCondition, condPair, cName, cVal, ColHeaderCond, CorrectData
	Dim iRow, rowFound, procEndStatus
	err.number =0
	match =0
	ColumnFound = False
	err.number = 0
	strMethod = strKeywordForReporting	
	If isProcessRunning("EXCEL.exe") Then
		Excel_WriteAllRowsByColumnIndex = 1 'Invalid path
		WriteStatusToLogs PROCESSRUNNING
		Exit function	
	End if
	
	Set objExcel = CreateObject("Excel.Application")   
	
	If err.number <> 0 Then
		Excel_WriteAllRowsByColumnIndex = 1 'Invalid path
		WriteStatusToLogs "Error while creating excel COM object. Verify if Excel is installed properly."
		Set objExcel = Nothing
		Exit function
	End If

	Set objWorkbook = objExcel.Workbooks.Open(fileName)

	If err.number <>0 Then
		Excel_WriteAllRowsByColumnIndex = 1 'Invalid path
		WriteStatusToLogs "Invalid excel file path: '"& fileName &"'"
		 objWorkbook.Close
		 objExcel.quit
		 Set objWorkbook = Nothing
		Set objExcel = Nothing
		Exit function
	End If
	objExcel.visible=false		
	
	set objsheet = objWorkbook.Sheets.Item(sheetName)
	
	If err.number <>0 Then
		Excel_WriteAllRowsByColumnIndex = 1 'Invalid Sheet
		WriteStatusToLogs "Invalid sheet name: '"& sheetName &"'"
		 objWorkbook.Close
		 objExcel.quit
		set objsheet = Nothing
		Set objWorkbook = Nothing
		Set objExcel = Nothing
		Exit function
	End If
	
	If StartingRowNumber <> "" Then
	     StartingRowNumber = cint(StartingRowNumber)		
	End If

	If StartingRowNumber = "" Then
		StartingRowNumber = 1
	ElseIf (Not(IsNumeric(StartingRowNumber) AND (CLng(StartingRowNumber) = StartingRowNumber))) or (err.number<>0) or (StartingRowNumber < 1) Then

		Excel_WriteAllRowsByColumnIndex = 1 'Invalid Sheet
		WriteStatusToLogs "Invalid StartingRowNumber '"& StartingRowNumber &"'. it should be integer starting from 1."

		objWorkbook.Close
		objExcel.quit
		set objsheet = Nothing
		Set objWorkbook = nothing
		Set objExcel = Nothing
		Exit function
	End If
	
	
	rows = objsheet.UsedRange.Rows.count
	ColumnNumber = cint(ColumnNumber)
	If (Not(IsNumeric(ColumnNumber) AND (CLng(ColumnNumber) = ColumnNumber))) or (err.number<>0) or (ColumnNumber < 1)Then
		Excel_WriteAllRowsByColumnIndex = 1 'Invalid Sheet
		WriteStatusToLogs "Invalid column number '"& ColumnNumber &"'. Column number should be integer starting from 1."

		objWorkbook.Close
		objExcel.quit
		set objsheet = Nothing
		Set objWorkbook = nothing
		Set objExcel = Nothing
		Exit function
	End If
	
	For i = lbound(Data) To ubound(Data)
			objsheet.Cells(StartingRowNumber,ColumnNumber).Value = Data(i)
			StartingRowNumber = StartingRowNumber +1
	Next

	objWorkbook.Save 
	objWorkbook.Close
	objExcel.quit
	set objsheet = Nothing
	Set objWorkbook = nothing
	procEndStatus = WaitTillObjectReleasesMemory(objExcel)

	If err.number=0 and procEndStatus = 0 Then
			Excel_WriteAllRowsByColumnIndex = 0
			WriteStatusToLogs "Data is written to excel sheet at column number: "& ColumnNumber
			'Exit function
	Else
			Excel_WriteAllRowsByColumnIndex = 1
			WriteStatusToLogs "Error occurred during  writing to excel sheet. "&err.description
			'Exit function				
	End If

	On error goto 0
End Function

Function Excel_WriteAllRowsByColumnName_REMOVE(fileName,sheetName, ColumnName, Data, StartingRowNumber, ColumnIndex) 
	On error resume next
	Dim strMethod, match, ColumnFound, objExcel, objWorkbook, objsheet, rows, cols, iCol, strValue, ColHeader, iCondition, condPair, cName, cVal, ColHeaderCond, CorrectData
	Dim iRow, rowFound, countFound, procEndStatus
	err.number =0
	match =0
	ColumnFound = False
	countFound = 1
	strMethod = strKeywordForReporting
	If isProcessRunning("EXCEL.exe") Then
		Excel_WriteAllRowsByColumnName = 1 'Invalid path
		WriteStatusToLogs PROCESSRUNNING
		Exit function	
	End if
	
	err.number = 0
	Set objExcel = CreateObject("Excel.Application")   
	
	If err.number <> 0 Then
		Excel_WriteAllRowsByColumnName = 1 'Invalid path
		WriteStatusToLogs "Error while creating excel COM object. Verify if Excel is installed properly."
		Set objExcel = Nothing
		Exit function
	End If
	
	Set objWorkbook = objExcel.Workbooks.Open(fileName)

	If err.number <>0 Then
		Excel_WriteAllRowsByColumnName = 1 'Invalid path
		WriteStatusToLogs "Invalid excel file path: '"& fileName &"'"
		 Set objWorkbook = Nothing
		Set objExcel = Nothing
		Exit function
	End If
	objExcel.visible=false		
	
	set objsheet = objWorkbook.Sheets.Item(sheetName)
	
	If err.number <>0 Then
		Excel_WriteAllRowsByColumnName = 1 'Invalid Sheet
		WriteStatusToLogs "Invalid sheet name: '"& sheetName &"'"
		 	set objsheet =  Nothing
		 Set objWorkbook = Nothing
		Set objExcel = Nothing
		Exit function
	End If
	
	If StartingRowNumber <> "" Then
	     StartingRowNumber = cint(StartingRowNumber)		
	     
	End If

	If StartingRowNumber = "" Then
		StartingRowNumber = 1
	ElseIf (Not(IsNumeric(StartingRowNumber) AND (CLng(StartingRowNumber) = StartingRowNumber))) or (err.number<>0) or (StartingRowNumber < 1) Then

		Excel_WriteAllRowsByColumnName = 1 'Invalid Sheet
		WriteStatusToLogs "Invalid StartingRowNumber "& StartingRowNumber &"'. it should be integer starting from 1."

		objWorkbook.Close
		objExcel.quit
		set objsheet = Nothing
		Set objWorkbook = nothing
		Set objExcel = Nothing
		Exit function
	End If
	
'''
    If ColumnIndex <> "" Then
	ColumnIndex = cint(ColumnIndex)
     End If

		If ColumnIndex = "" Then
			ColumnIndex = 1
		ElseIf (Not(IsNumeric(ColumnIndex) AND (CLng(ColumnIndex) = ColumnIndex))) or (err.number<>0) or (ColumnIndex < 1) Then
	
			Excel_WriteCellByColumnName = 1 'Invalid Sheet
			WriteStatusToLogs "Invalid column Index '"& ColumnIndex &"'. Column index should be integer starting from 1."
	
			objWorkbook.Close
			objExcel.quit
			set objsheet = Nothing
			Set objWorkbook = nothing
			Set objExcel = Nothing
			Exit function
		End If
	
			'---------------------
		cols = objsheet.UsedRange.Columns.count
				
		For iCol = 1 To cols
			 strValue=CStr(objsheet.Cells( 1, iCol))
					
			If (trim(strValue) = trim(ColumnName)) Then
				If ColumnIndex <> countFound Then
					countFound= countFound+1
				Else
					ColHeader = iCol
					ColumnFound = true
					Exit for 
				End If
	
			End If
		Next
			
		If ColumnFound = False Then
			ReadCelldataFromExcelFile = 1 'Invalid column name
			WriteStatusToLogs "Invalid column name or column index."
			Exit function
		End If
		
		'---------------------
	
		ColumnNumber = cint(ColHeader)
'''
	
	For i = lbound(Data) To ubound(Data)
			objsheet.Cells(StartingRowNumber,ColumnNumber).Value = Data(i)
			StartingRowNumber = StartingRowNumber +1
	Next

	objWorkbook.Save 
	objWorkbook.Close
	objExcel.quit
	set objsheet =  Nothing
	Set objWorkbook = Nothing
	procEndStatus = WaitTillObjectReleasesMemory(objExcel)

	If err.number=0 and procEndStatus = 0 Then
			Excel_WriteAllRowsByColumnName = 0
			WriteStatusToLogs "Data is written to excel sheet in column name" & ColumnName 
			'Exit function
	Else
			Excel_WriteAllRowsByColumnName = 1
			WriteStatusToLogs "Error occurred during  writing to excel sheet. "&err.description
			'Exit function				
	End If

	'Set objExcel = Nothing
	On error goto 0
End Function

Function Excel_AddBlankRow_REMOVE(fileName,sheetName, RowNumber)
	On error resume next
	Dim strMethod, match, ColumnFound, objExcel, objWorkbook, objsheet, rows, cols, iCol, strValue, ColHeader, iCondition, condPair, cName, cVal, ColHeaderCond, CorrectData
	Dim iRow, rowFound, procEndStatus
	err.number =0
	match =0
	ColumnFound = False
	strMethod = strKeywordForReporting
	If isProcessRunning("EXCEL.exe")=1 Then
		Excel_AddBlankRow = 1 'Invalid path
		WriteStatusToLogs PROCESSRUNNING
		Exit function	
	End if
	
	err.number = 0
	Set objExcel = CreateObject("Excel.Application")   
	
	If err.number <> 0 Then
		Excel_AddBlankRow = 1 'Invalid path
		WriteStatusToLogs "Error while creating excel COM object. Verify if Excel is installed properly."
		Set objExcel = Nothing
		Exit function
	End If
	
	Set objWorkbook = objExcel.Workbooks.Open(fileName)

	If err.number <>0 Then
		Excel_AddBlankRow = 1 'Invalid path
		WriteStatusToLogs "Invalid excel file path: '"& fileName &"'"
		objWorkbook.Close
		objExcel.quit
		Set objExcel = Nothing
		Exit function
	End If
	objExcel.visible=false		
	
	set objsheet = objWorkbook.Sheets.Item(sheetName)
	
	If err.number <>0 Then
		Excel_AddBlankRow = 1 'Invalid Sheet
		WriteStatusToLogs "Invalid sheet name: '"& sheetName &"'"
		objWorkbook.Close
		objExcel.quit
		set objsheet = Nothing
		Set objWorkbook = nothing
		Set objExcel = Nothing
		Exit function
	End If
	
	If RowNumber <> "" Then
	     	RowNumber = cint(RowNumber)		
	End If

	If (Not(IsNumeric(RowNumber) AND (CLng(RowNumber) = RowNumber))) or (err.number<>0) or (RowNumber < 1) Then
		Excel_AddBlankRow = 1 'Invalid Sheet
		WriteStatusToLogs "Invalid row number"& RowNumber &"'. Row number should be integer starting from 1."

		objWorkbook.Close
		objExcel.quit
		set objsheet = Nothing
		Set objWorkbook = nothing
		Set objExcel = Nothing
		Exit function
	End If

	objsheet.Rows(RowNumber).Insert()
	
	objWorkbook.Save 
	objWorkbook.Close
	objExcel.quit
	set objsheet = Nothing
	Set objWorkbook = nothing
	procEndStatus = WaitTillObjectReleasesMemory(objExcel)
	
	If err.number=0 and procEndStatus =0 Then
			Excel_AddBlankRow = 0
			WriteStatusToLogs "Blank row is added at position '" & RowNumber & "'."
			'Exit function
	Else
			Excel_AddBlankRow = 1
			WriteStatusToLogs "Error occurred during  adding a row to excel sheet. "&err.description
			'Exit function				
	End If

	On error goto 0
End Function

Function Excel_InsertColumn_REMOVE (fileName, sheetName, ColumnNumber)
	On error resume next
	Dim strMethod, match, ColumnFound, objExcel, objWorkbook, objsheet, rows, cols, iCol, strValue, ColHeader, iCondition, condPair, cName, cVal, ColHeaderCond, CorrectData
	Dim iRow, rowFound, procEndStatus
	err.number =0
	match =0
	ColumnFound = False
	strMethod = strKeywordForReporting	
	If isProcessRunning("EXCEL.exe") = 1 Then
		Excel_InsertColumn = 1 'Invalid path
		WriteStatusToLogs PROCESSRUNNING
		Exit function	
	End if
	
	Set objExcel = CreateObject("Excel.Application")   
	Set objWorkbook = objExcel.Workbooks.Open(fileName)

	If err.number <>0 Then
		Excel_InsertColumn = 1 'Invalid path
		WriteStatusToLogs "Invalid excel file path: '"& fileName &"'"
		objWorkbook.Close
		objExcel.quit
		Set objExcel = Nothing
		Exit function
	End If
	objExcel.visible=false		
	
	set objsheet = objWorkbook.Sheets.Item(sheetName)
	
	If err.number <>0 Then
		Excel_InsertColumn = 1 'Invalid Sheet
		WriteStatusToLogs "Invalid sheet name: '"& sheetName &"'"
		objWorkbook.Close
		objExcel.quit
		set objsheet = Nothing
		Set objWorkbook = nothing
		Set objExcel = Nothing
		Exit function
	End If
	
	If ColumnNumber <> "" Then
	     	ColumnNumber = cint(ColumnNumber)		
	End If

	If (Not(IsNumeric(ColumnNumber) AND (CLng(ColumnNumber) = ColumnNumber))) or (err.number<>0) or (ColumnNumber < 1) Then
		Excel_InsertColumn = 1 'Invalid Sheet
		WriteStatusToLogs "Invalid column number"& ColumnNumber &"'. Column number should be integer starting from 1."

		objWorkbook.Close
		objExcel.quit
		set objsheet = Nothing
		Set objWorkbook = nothing
		Set objExcel = Nothing
		Exit function
	End If

	objsheet.Columns(ColumnNumber).Insert()

	objWorkbook.Save 
	objWorkbook.Close
	objExcel.quit
	set objsheet = Nothing
	Set objWorkbook = nothing
	Set objExcel = nothing
	procEndStatus = WaitTillObjectReleasesMemory(objExcel)

	If err.number=0 and procEndStatus = 0 Then
			Excel_InsertColumn = 0
			WriteStatusToLogs "New column is added at position '" & ColumnNumber & "'."
			'Exit function
	Else
			Excel_InsertColumn = 1
			WriteStatusToLogs "Error occurred during  writing to excel sheet. "&err.description
			'Exit function				
	End If

	On error goto 0
End Function

''Internal
function isProcessRunning(procName)
	Set objWMIService = GetObject("winmgmts:" _
	    & "{impersonationLevel=impersonate}!\\" & "." & "\root\cimv2")
	Set colProcessList = objWMIService.ExecQuery _
	    ("Select Name from Win32_Process WHERE Name='" & procName & "'")
	
	If colProcessList.count>0 then
	    isProcessRunning = true 
	else
	    isProcessRunning = False
	End if 
	
	Set objWMIService = Nothing
	Set colProcessList = Nothing
End function


Function WaitTillObjectReleasesMemoryOLD(Byref objExcel)
		on error resume next
		Dim objWMIService, colProcessList
		Set objExcel =  Nothing
		'SystemUtil.CloseProcessByName EXCELPROCESSNAME_
		Set objWMIService = GetObject("winmgmts:"& "{impersonationLevel=impersonate}!\\" & "." & "\root\cimv2")
		Set colProcessList = objWMIService.ExecQuery("Select Name from Win32_Process WHERE Name='" & "EXCEL.EXE" & "'")
		
		'wait EXCELCLOSETIME
        'Do  while (not (objExcel is nothing))
            'wait 1
        'loop
		
		do while (colProcessList.count>0)
			Set objExcel =  Nothing
			Set colProcessList = objWMIService.ExecQuery("Select Name from Win32_Process WHERE Name='" & "EXCEL.EXE" & "'")
			For Each objProcess in colProcess
				objProcess.Terminate()
			Next
			SystemUtil.CloseProcessByName "EXCEL.EXE"
			'wait EXCELCLOSETIME
			'msgbox "excel open"
		Loop
			
		If (objExcel is nothing) Then
			WaitTillObjectReleasesMemory = 0 
		else
			WaitTillObjectReleasesMemory = 1
		End If
End  function


Function WaitTillObjectReleasesMemory(Byref objExcel)
	dim objWMIService, colProcessList, objProcess
	Set objExcel = nothing
	Set objWMIService = GetObject("winmgmts:"& "{impersonationLevel=impersonate}!\\" & "." & "\root\cimv2")
	Set colProcessList = objWMIService.ExecQuery("Select Name from Win32_Process WHERE Name='" & "EXCEL.EXE" & "'")
	SystemUtil.CloseDescendentProcesses
	SystemUtil.CloseProcessByName "EXCEL.EXE"	
	For Each objProcess in colProcessList
		objProcess.Terminate()
		wait 1
	Next
	SystemUtil.CloseProcessByName "EXCEL.EXE"
	SystemUtil.CloseDescendentProcesses 
	if err.number =0 then
		WaitTillObjectReleasesMemory = 0
	else
		WaitTillObjectReleasesMemory = -1	
	end if

End  function
Function Excel_StoreColumnCount(K,fileName,sheetName,ignoreBlankColumns) 
	On error resume next
	Dim strMethod, match, ColumnFound, objExcel, objWorkbook, objsheet, rows, cols, strValue, ColHeader, iCondition, condPair, cName, cVal, ColHeaderCond, CorrectData
	Dim iRow, rowFound, procEndStatus, getHeaderVal, i, colCount, startCol, startRow
	err.number =0
	match =0
	ColumnFound = False
	strMethod = strKeywordForReporting	
	If isProcessRunning("EXCEL.exe") Then
		Excel_StoreColumnCount = 1 'Invalid path
		WriteStatusToLogs PROCESSRUNNING
		Exit function	
	End if
	
	If (instr(1, ignoreBlankColumns, TRUE_, vbTextCompare) < 1) AND (instr(1, ignoreBlankColumns, FALSE_, vbTextCompare)) < 1 AND (ignoreBlankColumns <> "") Then
		Excel_StoreColumnCount = 1 'Invalid path
		WriteStatusToLogs "Invalid parameter ignoreBlankColumns, Allowed values for this parameter is 'True/False or blank' ."
		Exit function	
	End if	
	
	If ignoreBlankColumns = "" Then
		ignoreBlankColumns = IgnoreBlankColumn
	End If
	
	Set objExcel = CreateObject("Excel.Application")   
	Set objWorkbook = objExcel.Workbooks.Open(fileName)

	If err.number <>0 Then
		Excel_StoreColumnCount = 1 'Invalid path
		WriteStatusToLogs "Invalid excel file path: '"& fileName &"'"
		objWorkbook.Close
		objExcel.quit
		Set objWorkbook = Nothing
		Call WaitTillObjectReleasesMemory(objExcel)
		Exit function
	End If
	objExcel.visible=false		
	
	set objsheet = objExcel.Sheets.Item(sheetName)
	
	If err.number <>0 Then
		Excel_StoreColumnCount = 1 'Invalid Sheet
		WriteStatusToLogs "Invalid sheet name: '"& sheetName &"'"
		objWorkbook.Close
		objExcel.quit
		Set objWorkbook = Nothing
		Set objExcel = nothing
		Call WaitTillObjectReleasesMemory(objExcel)
		Exit function
	End If
	cols = objsheet.UsedRange.Columns.count
	rows = objsheet.UsedRange.Rows.count
	startCol = getFirstNonBlanktColumn (objsheet)
	startRow = getFirstNonBlanktRow (objsheet, row, cols+startCol)

	If (startCol = -1) or (startRow = -1) then
		rowCount = 0
	Else
		If instr(1, ignoreBlankColumns, IgnoreBlankColumn, vbTextCompare) > 0 Then
			For i = startCol To cols
				For r = startRow To startRow+rows
				'For r = startRow To rows
					getHeaderVal = objsheet.Cells( r, cint(i)).value
					If getHeaderVal <> "" Then
						'msgbox "Val: " & getHeaderVal
						colCount = colCount + 1
						Exit for
					End If				
				Next

			Next
		Else
			colCount = cols
		End If
	End if
	

	if colCount = "" or isempty(colCount) then
		colCount = 0
	End if
	
	Call AddItemToStorageDictionary(K,colCount)
	objExcel.quit
	set objsheet = Nothing
	Set objWorkbook = nothing
	Set objExcel = nothing
	procEndStatus = WaitTillObjectReleasesMemory(objExcel)

	If err.number=0 and procEndStatus = 0 Then
			Excel_StoreColumnCount = 0
			WriteStatusToLogs "Key '"&K&"' is stored with column count '"&colCount&"'. "&"', IgnoreBlankColumn parameter is taken as '"& ignoreBlankColumns&"'."

	Elseif colCount = 0 then
			Excel_StoreColumnCount = 0
			WriteStatusToLogs "Key '"&K&"' is stored with column count '"&colCount&"'. "&"', IgnoreBlankColumn parameter is taken as '"& ignoreBlankColumns&"'."
		 
	Else
			Excel_StoreColumnCount = 1
			WriteStatusToLogs "Error occurred while getting the column count "&err.description
						
	End If
	
	On error goto 0
End Function


Function getFirstNonBlanktRow (objsheet, row, cols)
	Dim c, r

	For r = 1 To maxRow
		If not isRowBlank(objsheet, r, cols) Then
			getFirstNonBlanktRow = r
			Exit function
		End If
	Next
	
	getFirstNonBlanktRow = -1
End Function


Function getFirstNonBlanktColumn (objsheet)
	Dim c, r, Val
	For c = 1 To maxCol
		For r = 1 To maxRow
			Val = objsheet.Cells( r, cint(c)).value
			If not isempty(Val) Then
				getFirstNonBlanktColumn = c
				Exit function
			End If			
		Next
	Next
	
	getFirstNonBlanktColumn = -1
End Function


