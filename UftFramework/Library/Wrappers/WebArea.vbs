

'========================Code Section Begin=====================================================================
Option Explicit	'Forces explicit declaration of all variables in a script.
'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :				 VerifyWebAreaExist()
'	Purpose :					 Verifies the existence of WebArea in AUT.
'	Return Value :		 		  Integer( 0/2)
'	Arguments :
'		Input Arguments :  		  objWebArea 
'		Output Arguments : 
'	Function is called by :
'	Function calls :			 CheckObjectExist() 
'	Created on :				      15/01/2009
'	Author :						  Rathna Reddy 
'
'------------------------------------------------------------------------------------------------------------------------------------------

Function VerifyWebAreaExist(ByVal objWebArea)
	On Error Resume Next
		'Variable Section Begin
	
		 Dim strMethod

		'Variable Section End

		strMethod = strKeywordForReporting

			If CheckObjectExist(objWebArea) Then 
			   
					VerifyWebAreaExist = 0
					WriteStatusToLogs "The WebArea Exists."
					
			Else
					VerifyWebAreaExist = 2
					WriteStatusToLogs "The specified WebArea doesn't exist, please verify."
					
			End If 
	  
	
	On Error GoTo 0
End Function

'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :				 VerifyWebAreaNotExist()
'	Purpose :					 Verifies whether the WebArea Exist or not in AUT?.
'	Return Value :		 		  Integer( 0/2)
'	Arguments :
'		Input Arguments :  		  objWebArea 
'		Output Arguments : 
'	Function is called by :
'	Function calls :			 CheckObjectNotExist() 
'	Created on :				   15/01/2009
'	Author :						  Rathna Reddy 
'
'------------------------------------------------------------------------------------------------------------------------------------------

Function VerifyWebAreaNotExist(ByVal objWebArea)
	On Error Resume Next
		'Variable Section Begin
	
		   Dim strMethod

		'Variable Section End

		strMethod = strKeywordForReporting
		
			If  CheckObjectNotExist(objWebArea) Then 
			   
					VerifyWebAreaNotExist = 0
					WriteStatusToLogs "The WebArea doesn't exist."
					
			Else
					VerifyWebAreaNotExist = 2
					WriteStatusToLogs "The WebArea exists, please verify."
					
			End If 
	
  		
	On Error GoTo 0
End Function


'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name :				 ClickWebArea()
' 	Purpose :					Performs click operation on specified WebArea in a page.
' 	Return Value :		 		  Integer( 0/1)
' 	Arguments :
'		Input Arguments :  		objWebArea
'		Output Arguments : 
'	Function is called by :
'	Function calls :				CheckObjectExist() , CheckObjectClick()
'	Created on :				   14/03/2008
'	Author :						 Rathna Reddy 
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function ClickWebArea (ByVal objWebArea)
On Error Resume Next
	'Variable Section Begin
		
			Dim strMethod

	'Variable Section End

	strMethod = strKeywordForReporting

		If CheckObjectExist(objWebArea)  Then         
			If CheckObjectClick(objWebArea) Then
					ClickWebArea = 0
					WriteStatusToLogs "Click operation is performed on the WebArea successfully.."
			Else
					ClickWebArea = 1
					WriteStatusToLogs "Click operation could not be performed on the specified WebArea, please verify."
			End If 
		Else
				ClickWebArea = 1
				WriteStatusToLogs "The specified WebArea doesn't exist, please verify."
		End If
		'WriteStepResults ClickWebArea 
	On Error GoTo 0
End Function
'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name :				 VerifyWebAreaToolTip()
' 	Purpose :					 Verifies the tool tip of a specified WebArea with our expected .
' 	Return Value :		 		  Integer( 0/1/2)
' 	Arguments :
'		Input Arguments :  		objWebArea,strExpAltText
'		Output Arguments :      intAbsx,intAbsy,intVal,objDivRep,objWshShell,strActToolTip
'	Function is called by :
'	Function calls :			   CheckObjectExist(),CompareStringValues()
'	Created on :				  14/03/2008
'	Author :						 Rathna Reddy 
'
'------------------------------------------------------------------------------------------------------------------------------------------

Function VerifyWebAreaToolTip(byval objWebArea,byval strExpTooltip)
   On Error Resume Next
   'Variable Section Begin
	
		   Dim strMethod
		   Dim  strActTooltip

		'Variable Section End

		strMethod = strKeywordForReporting

			If CheckObjectExist(objWebArea) Then  
						strActTooltip=objWebArea.GetRoProperty("alt")
				       If Err.Number = 0 Then
									If CompareStringValues(strExpTooltip,strActTooltip) = 0 Then
										VerifyWebAreaToolTip = 0
								  
										WriteStatusToLogs  "The specified WebArea Tool tip match the expected data."
									Else
										VerifyWebAreaToolTip = 2
										WriteStatusToLogs  "WebArea Tool tip is different from the expected, please verify."
								   End If
					 Else
							VerifyWebAreaToolTip = 1
							WriteStatusToLogs  "Action fails to Capture the WebArea Tool tip, please verify WebArea alt property."
					 End If
									
		Else
			VerifyWebAreaToolTip = 1
			WriteStatusToLogs  "The specified WebArea does not exist, please verify."
		End If
		
	On Error GOTO 0
End Function
'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name :				 CompareWebAreaURL()
' 	Purpose :					 Compares the WebArea URL with our expected URL.
' 	Return Value :		 		 Integer( 0/1/2)
' 	Arguments :
'		Input Arguments :  	   objWebArea , strExpUrl
'		Output Arguments :     strActUrl
'	Function is called by :
'	Function calls :			   CheckObjectExist() , CompareStringValues()
'	Created on :				  14/03/2008
'	Author :						Rathna Reddy 
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function CompareWebAreaURL(ByVal objWebArea , ByVal strExpUrl)
	On Error Resume Next
		'Variable Section Begin
		
			Dim strMethod
			Dim strActUrl

		'Variable Section End

		strMethod = strKeywordForReporting

			If CheckObjectExist(objWebArea) Then
					strActUrl=objWebArea.GetRoProperty("url")
					If Err.Number = 0 Then 
						   If	CompareStringValues(strExpUrl,strActUrl) = 0 Then 
							 
								CompareWebAreaURL = 0
								 WriteStatusToLogs "The WebArea URL is same the expected."
						  Else
								CompareWebAreaURL = 2
								WriteStatusToLogs "The specified WebArea URL is different from the expected."
						 End If
				  Else
						CompareWebAreaURL = 1
						WriteStatusToLogs "Action fails to Capture the specified WebArea URL, please verify WebArea URL Property."
				  End If
			
			Else
					CompareWebAreaURL = 1
					WriteStatusToLogs  "The specified WebArea does not exist, please verify."
			End If
	On Error GoTo 0
End Function
'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :				 VerifyWebAreaVisible()
'	Purpose :					 Verifies whether the WebArea is visible.
'	Return Value :		 		  Integer( 0/1/2)
'	Arguments :
'		Input Arguments :  		  objWebArea 
'		Output Arguments : 
'	Function is called by :
'	Function calls :			 CheckObjectExist() , CheckObjectVisiblity()
'	Created on :				   10/03/2008
'	Author :						  Rathna Reddy 
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function VerifyWebAreaVisible(ByVal objWebArea)
	On Error Resume Next
		'Variable Section Begin
		
		Dim strMethod

		'Variable Section End

		strMethod = strKeywordForReporting

			If CheckObjectExist(objWebArea)  Then 
					If CheckObjectVisible(objWebArea) Then
						VerifyWebAreaVisible = 0
						WriteStatusToLogs "The WebArea is visible."
					Else
						VerifyWebAreaVisible = 2
						WriteStatusToLogs "The WebArea is not visible."
					End If
			Else
					VerifyWebAreaVisible = 1
					WriteStatusToLogs "The specified WebArea doesn't exist."
			End If
		
	On Error GoTo 0
End Function
'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :				 VerifyWebAreaInVisible()
'	Purpose :					 Verifies whether the WebArea is Invisible.
'	Return Value :		 		  Integer( 0/1/2)
'	Arguments :
'		Input Arguments :  		  objWebArea 
'		Output Arguments : 
'	Function is called by :
'	Function calls :			 CheckObjectExist() , CheckObjectVisiblity()
'	Created on :				   10/03/2008
'	Author :						  Rathna Reddy 
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function VerifyWebAreaInVisible(ByVal objWebArea)
	On Error Resume Next
		'Variable Section Begin
		
		Dim strMethod

		'Variable Section End

		strMethod = strKeywordForReporting

			If CheckObjectExist(objWebArea)  Then 
					If CheckObjectInVisible(objWebArea) Then
						VerifyWebAreaInVisible = 0
						WriteStatusToLogs "The WebArea is not visible."
					Else
						VerifyWebAreaInVisible = 2
						WriteStatusToLogs "The WebArea is visible."
					End If
			Else
					VerifyWebAreaInVisible = 1
					WriteStatusToLogs "The specified WebArea doesn't exist."
			End If
		
	On Error GoTo 0
End Function


'=======================Code Section End=============================================
