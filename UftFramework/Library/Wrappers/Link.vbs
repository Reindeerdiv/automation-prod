
'=======================Code Section Begin=============================================

Option Explicit 'Forces explicit declaration of all variables in a script. 
'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name :				ClickLink()
' 	Purpose :						Performs Click operation on a specified Link
' 	Return Value :		 		  Integer( 0/1)
' 	Arguments :
'		Input Arguments :  		objLink
'		Output Arguments :    
'	Function is called by :
'	Function calls :				CheckObjectExist() , CheckObjectClick()
'	Created on :				   17/03/2008
'	Author :						 Rathna Reddy 
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function ClickLink (ByVal objLink)
	On Error Resume Next
		'Variable Section Begin

				Dim strMethod

		'Variable Section End
	
		strMethod = strKeywordForReporting

			If  CheckObjectExist(objLink)  Then  
			     If CheckObjectEnabled(objLink) Then
					If CheckObjectClick(objLink) Then
						ClickLink = 0
						WriteStatusToLogs "Click operation was performed on the specified Link."
					Else
						ClickLink = 1
						WriteStatusToLogs "Click operation was not performed on the specified Link, Please verify."
					End If 
			    Else
						ClickLink = 1
						WriteStatusToLogs "The specified Link was disabled, Please verify."
				End If
			Else
					ClickLink = 1
					WriteStatusToLogs "The specified Link does not exist."
			End If
		   
	On Error GoTo 0
End Function
'------------------------------------------------------------------------------------------------------------------------------------------
'  	Function Name :				 CompareLinkName()
' 	Purpose :						 Compares The Link Name with our expected name.
' 	Return Value :		 		   Integer( 0/1/2)
' 	Arguments :
'		Input Arguments :  		objLink , strExpName
'		Output Arguments :     strActName
'	Function is called by :
'	Function calls :				CheckObjectExist(),CompareStringValues()
'	Created on :				   17/03/2008
'	Author :						  Rathna Reddy 
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function CompareLinkName(ByVal objLink, ByVal strExpName)
On Error Resume Next
	'Variable Section Begin
		
		Dim strMethod
		Dim strActName

	'Variable Section End

	strMethod = strKeywordForReporting

		If CheckObjectExist(objLink) Then
				strActName = objLink.GetRoProperty("innertext")
				If Err.number=0 Then
						If CompareStringValues(strExpName,strActName) = 0 Then
								CompareLinkName  = 0
								WriteStatusToLogs """"&"Expected Name : "&strExpName& """" &  "   and   " &""""&"Actual Name : "&strActName&""""&"  are equal."


						Else
								CompareLinkName = 2
								 WriteStatusToLogs """"&"Expected Name : "&strExpName& """" &  "   and   " &""""&"Actual Name : "&strActName&""""&"  are not equal , Please verify."

						End If
				Else
							CompareLinkName = 1 
							WriteStatusToLogs "Action fails to Capture the specified Link's InnerText,Please verify Object Properties."

				End if 
		Else
				CompareLinkName=1
				WriteStatusToLogs "The specified Link does not exist, Please verify."
		End If
		'WriteStepResults VerifyLinkName 
	On Error GoTo 0
End Function
'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name :				 CompareLinkURL()
' 	Purpose :						Compares the Link URL with our expected URL.
' 	Return Value :		 		  Integer( 0/1/2)
' 	Arguments :
'		Input Arguments :  	   objLink , strExpUrl
'		Output Arguments :     
'	Function is called by :    strActUrl
'	Function calls :				CheckObjectExist() , CheckObjectURL()
'	Created on :				   17/03/2008
'	Author :						 Rathna Reddy 
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function CompareLinkURL(ByVal objLink , ByVal strExpUrl)' Url of Page
	On Error Resume Next
		'Variable Section Begin
		
			Dim strMethod
			Dim strActUrl

		'Variable Section End

		strMethod = strKeywordForReporting

			If CheckObjectExist(objLink) Then
			    strActUrl=objLink.GetRoProperty("url")
				If Err.number=0 Then
						If CompareStringValues(strExpUrl , strActUrl) = 0 Then 
							CompareLinkURL = 0
							WriteStatusToLogs """"&"Expected Name : "&strExpUrl& """" &  "   and   " &""""&"Actual Name : "&strActUrl&""""&"  are equal."
						Else
							CompareLinkURL =2
							WriteStatusToLogs """"&"Expected Name : "&strExpUrl& """" &  "   and   " &""""&"Actual Name : "&strActUrl&""""&"  are not equal, Please verify."
						End If
				Else
						CompareLinkURL = 1 
						WriteStatusToLogs "Action fails to Capture the specified Link's URL,Please verify Object Properties."

				End if 
			Else
						CompareLinkURL =1
						WriteStatusToLogs "The specified Link does not exist, Please verify."
			End If
	On Error GoTo 0
End Function
'-----------------------------------------------------------------------------------------
'  	Function Name :				VerifyLinkToolTip()
' 	Purpose :					 Verifies the tool tip of  a specifed link with our expected.
' 	Return Value :		 		  Boolean( 0/1/2)
' 	Arguments :
'		Input Arguments :  	   objLink,strExpToolTip
'		Output Arguments :    strActToolTip
'	Function is called by :
'	Function calls :			   CheckObjectExist()
'	Created on :				 19/03/2008
'	Author :						Rathna Reddy 
'------------------------------------------------------------------------------------------------------------------------------------------
Function VerifyLinkToolTip(byval objLink,byval strExpToolTip)
   On Error Resume Next
   'Variable Section Begin
	
		   Dim strMethod
		   Dim strActToolTip
		  

		'Variable Section End


		 strMethod = strKeywordForReporting

			If CheckObjectExist(objLink) Then  
					strActToolTip = objLink.object.title
					If Err.number=0 Then
								If CompareStringValues(strExpToolTip,strActToolTip) = 0 Then
									VerifyLinkToolTip = 0
							       WriteStatusToLogs """"&"Expected ToolTip : "&strExpToolTip& """" &  "   and   " &""""&"Actual TooTip : "&strActToolTip&""""&"  are equal."
								Else
									VerifyLinkToolTip = 2
										WriteStatusToLogs """"&"Expected ToolTip : "&strExpToolTip& """" &  "   and   " &""""&"Actual TooTip : "&strActToolTip&""""&"  are  not equal,Please verify."
								End If
					Else
						VerifyLinkToolTip = 1 
						WriteStatusToLogs "Action fails to Capture the specified Link's tool tip,Please verify Object Properties."

					End if 
			Else
					VerifyLinkToolTip = 1
					WriteStatusToLogs "The specified Link does not exist, Please verify."
			End If
	On Error GoTo 0
End Function
'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :				 VerifyLinkVisible()
'	Purpose :					 Verifies whether the Link is visible.
'	Return Value :		 		  Integer( 0/1/2)
'	Arguments :
'		Input Arguments :  		  objLink 
'		Output Arguments : 
'	Function is called by :
'	Function calls :			 CheckObjectExist() , CheckObjectVisiblity()
'	Created on :				   10/03/2008
'	Author :						  Rathna Reddy 
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function VerifyLinkVisible(ByVal objLink)
	On Error Resume Next
		'Variable Section Begin
		
			Dim strMethod

		'Variable Section End

		 strMethod = strKeywordForReporting

			If CheckObjectExist(objLink)  Then 
					If CheckObjectVisible(objLink) Then
						VerifyLinkVisible = 0
						WriteStatusToLogs "The Link is Visible."
					Else
						VerifyLinkVisible = 2
						WriteStatusToLogs "The specified Link was not Visible, Please verify."
					End If
			Else
					VerifyLinkVisible = 1
					WriteStatusToLogs "The specified Link does not exist, Please verify."
			End If
		
	On Error GoTo 0
End Function
'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :				 VerifyLinkInVisible()
'	Purpose :					 Verifies whether the Link is Invisible.
'	Return Value :		 		  Integer( 0/1/2)
'	Arguments :
'		Input Arguments :  		  objLink 
'		Output Arguments : 
'	Function is called by :
'	Function calls :			 CheckObjectExist() , CheckObjectVisiblity()
'	Created on :				   10/03/2008
'	Author :						  Rathna Reddy 
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function VerifyLinkInVisible(ByVal objLink)
	On Error Resume Next
		'Variable Section Begin
		
			Dim strMethod

		'Variable Section End

		strMethod = strKeywordForReporting

			If CheckObjectExist(objLink)  Then 
					If CheckObjectInVisible(objLink) Then
						VerifyLinkInVisible = 0
						WriteStatusToLogs "The specified Link was not Visible."
					Else
						VerifyLinkInVisible = 2
						WriteStatusToLogs "The Link was Visible, Please verify."
					End If
			Else
					VerifyLinkInVisible = 1
					WriteStatusToLogs "The specified Link does not exist, Please verify."
			End If
		
	On Error GoTo 0
End Function

'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :				 VerifyLinkExist()
'	Purpose :					 Verifies whether the Link Exists or not?
'	Return Value :		 		  Integer( 0/2)
'	Arguments :
'		Input Arguments :  		  objObject 
'		Output Arguments : 
'	Function is called by :
'	Function calls :			 CheckObjectExist() 
'	Created on :				   10/03/2008
'	Author :						  Rathna Reddy 
'
'------------------------------------------------------------------------------------------------------------------------------------------

Function VerifyLinkExist(ByVal objLink)
	On Error Resume Next
		'Variable Section Begin
	
		   Dim strMethod

		'Variable Section End

		strMethod = strKeywordForReporting

			If CheckObjectExist(objLink) Then 
			   
					VerifyLinkExist = 0
					WriteStatusToLogs "The Link Exist."
					
			Else
					VerifyLinkExist = 2
					WriteStatusToLogs "The specified Link does NOT Exist, Please Verify."
					
			End If 
	  
	
	On Error GoTo 0
End Function

'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :				 VerifyLinkNotExist()
'	Purpose :					 Verifies whether the Link Exists or not?
'	Return Value :		 		  Integer( 0/2)
'	Arguments :
'		Input Arguments :  		  objObject 
'		Output Arguments : 
'	Function is called by :
'	Function calls :			 CheckObjectExist() 
'	Created on :				   10/03/2008
'	Author :						  Rathna Reddy 
'
'------------------------------------------------------------------------------------------------------------------------------------------

Function VerifyLinkNotExist(ByVal objLink)
	On Error Resume Next
		'Variable Section Begin
	
		   Dim strMethod

		'Variable Section End

		strMethod = strKeywordForReporting
		
			If  CheckObjectNotExist(objLink) Then 
			   
					VerifyLinkNotExist = 0
					WriteStatusToLogs "The Link does not Exist."
					
			Else
					VerifyLinkNotExist = 2
					WriteStatusToLogs "The specified Link Exist, Please Verify."
					
			End If 
	
  		
	On Error GoTo 0
End Function
'=======================Code Section End=============================================
