 Option Explicit		 'Forces explicit declaration of all variables in a script.
'=======================Code Section Begin=============================================


'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name :				  SelectRadioButton()
' 	Purpose :						 Selects a specified RadioButton from the RadioGroup
' 	Return Value :		 		   Integer( 0/1)
' 	Arguments :
'		Input Arguments :  		   objRadioButton,strExpValue
'		Output Arguments :
'	Function is called by :
'	Function calls :				CheckObjectExist()
'	Created on :				   18/03/2008
'	Author :						  Rathna Reddy
' Note:unselectRedioButton is no need,if we select one radiobutton,the ramaining automatically in unselectstate
'------------------------------------------------------------------------------------------------------------------------------------------
Function SelectRadioButton(Byval objRadioButton,ByVal strExpValue)
	On Error Resume Next
	'Variable Section Begin
		
			Dim strMethod

		'Variable Section End

		strMethod = strKeywordForReporting

			If CheckObjectExist(objRadioButton) Then
				objRadioButton.Select strExpValue 'it is "value " property of radio button
					If err.Number = 0  Then
						 WriteStatusToLogs 	"Expected RadioButton Selected." 
							SelectRadioButton = 0
					Else
							 SelectRadioButton = 1
							 WriteStatusToLogs 	"RadioButton not Selected, Please Verify." 
					End If
			Else
					SelectRadioButton = 1
            End If
			'WriteStepResults SelectRadioButton
	On Error GoTo 0
End Function
'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :				 VerifyRadioButtonExist()
'	Purpose :					 Verifies whether the RadioButton Exists or not?
'	Return Value :		 		  Integer( 0/2)
'	Arguments :
'		Input Arguments :  		  objRadioButton 
'		Output Arguments : 
'	Function is called by :
'	Function calls :			 CheckObjectExist() 
'	Created on :				   10/03/2008
'	Author :						  Rathna Reddy 
'
'------------------------------------------------------------------------------------------------------------------------------------------

Function VerifyRadioButtonExist(ByVal objRadioButton)
	On Error Resume Next
		'Variable Section Begin
	
		   Dim strMethod

		'Variable Section End

		strMethod = strKeywordForReporting

			If CheckObjectExist(objRadioButton) Then 
			   
					VerifyRadioButtonExist = 0
					WriteStatusToLogs "The RadioButton Exist."
					
			Else
					VerifyRadioButtonExist = 2
					WriteStatusToLogs "The RadioButton does not Exist, Please Verify."
					
			End If 
	  
	
	On Error GoTo 0
End Function

'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :				 VerifyRadioButtonNotExist()
'	Purpose :					 Verifies whether the RadioButton Exists or not?
'	Return Value :		 		  Integer( 0/2)
'	Arguments :
'		Input Arguments :  		  objRadioButton 
'		Output Arguments : 
'	Function is called by :
'	Function calls :			 CheckObjectExist() 
'	Created on :				   10/03/2008
'	Author :						  Rathna Reddy 
'
'------------------------------------------------------------------------------------------------------------------------------------------

Function VerifyRadioButtonNotExist(ByVal objRadioButton)
	On Error Resume Next
		'Variable Section Begin
	
		   Dim strMethod

		'Variable Section End

		strMethod = strKeywordForReporting
		
			If CheckObjectNotExist(objRadioButton) Then 
			   
					VerifyRadioButtonNotExist = 0
					WriteStatusToLogs "The RadioButton does not Exist."
					
			Else
					VerifyRadioButtonNotExist = 2
					WriteStatusToLogs "The RadioButton Exists, Please Verify."
					
			End If 
	
  		
	On Error GoTo 0
End Function

'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name :				 VerifyRadioButtonEnabled()
' 	Purpose :					 Verifies whether the RadioButton is Enabled
' 	Return Value :		 		  Integer( 0/1/2)
' 	Arguments :
'		Input Arguments :  		objRadioButton 
'		Output Arguments : 
'	Function is called by :
'	Function calls :				CheckObjectExist() , CheckObjectStatus()
'	Created on :				   11/03/2008
'	Author :						  Rathna Reddy 
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function VerifyRadioButtonEnabled(ByVal objRadioButton)

On Error Resume Next
	'Variable Section Begin
		
		Dim strMethod

	'Variable Section End

	strMethod = strKeywordForReporting

		If CheckObjectExist(objRadioButton) Then
			If CheckObjectEnabled(objRadioButton) Then
					VerifyRadioButtonEnabled = 0
					WriteStatusToLogs "The RadioButton was in enabled state."
				Else
					VerifyRadioButtonEnabled = 2
					WriteStatusToLogs "The RadioButton was not in enabled state, Please verify."
				End If
            
		Else
				VerifyRadioButtonEnabled = 1
				WriteStatusToLogs "The RadioButton does not exist, Please verify."
		End if
		
	On Error GoTo 0
    
End Function

'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name :				 VerifyRadioButtonDisabled()
' 	Purpose :					 Verifies whether the RadioButton is Disabled
' 	Return Value :		 		  Integer( 0/1/2)
' 	Arguments :
'		Input Arguments :  		objRadioButton 
'		Output Arguments : 
'	Function is called by :
'	Function calls :				CheckObjectExist() , CheckObjectStatus()
'	Created on :				   11/03/2008
'	Author :						  Rathna Reddy 
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function VerifyRadioButtonDisabled(ByVal objRadioButton)

On Error Resume Next
	'Variable Section Begin
		
		Dim strMethod

	'Variable Section End

	 strMethod = strKeywordForReporting

		If CheckObjectExist(objRadioButton) Then
			If CheckObjectDisabled(objRadioButton) Then
					VerifyRadioButtonDisabled = 0
					WriteStatusToLogs "The RadioButton was in Disabled state."
				Else
					VerifyRadioButtonDisabled = 2
					WriteStatusToLogs "The RadioButton was not in Disabled state, Please verify."
				End If
            
		Else
				VerifyRadioButtonDisabled = 1
				WriteStatusToLogs "The RadioButton does not exist, Please verify."
		End if
	On Error GoTo 0
End Function

'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :				 VerifyRadioButtonVisible()
'	Purpose :					 Verifies whether the RadioButton is visible.
'	Return Value :		 		  Integer( 0/1/2)
'	Arguments :
'		Input Arguments :  		  objRadioButton 
'		Output Arguments : 
'	Function is called by :
'	Function calls :			 CheckObjectExist() , CheckObjectVisiblity()
'	Created on :				   10/03/2008
'	Author :						  Rathna Reddy 
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function VerifyRadioButtonVisible(ByVal objRadioButton)
	On Error Resume Next
		'Variable Section Begin
		
			Dim strMethod 

		'Variable Section End

		strMethod = strKeywordForReporting

			If CheckObjectExist(objRadioButton)  Then 
				If CheckObjectVisible(objRadioButton) Then
					VerifyRadioButtonVisible = 0
					WriteStatusToLogs "The RadioButton was in Visible mode."
				Else
					VerifyRadioButtonVisible = 2
					WriteStatusToLogs "The RadioButton was not in  Visible mode,Please verify."
				End If
			Else
					VerifyRadioButtonVisible = 1
					WriteStatusToLogs "The RadioButton does not exist, Please verify."
			End If
		
	On Error GoTo 0
End Function
'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :				 VerifyRadioButtonInVisible()
'	Purpose :					 Verifies whether the RadioButton is Invisible.
'	Return Value :		 		  Integer( 0/1/2)
'	Arguments :
'		Input Arguments :  		  objRadioButton 
'		Output Arguments : 
'	Function is called by :
'	Function calls :			 CheckObjectExist() , CheckObjectVisiblity()
'	Created on :				   10/03/2008
'	Author :						  Rathna Reddy 
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function VerifyRadioButtonInVisible(ByVal objRadioButton)
	On Error Resume Next
		'Variable Section Begin
		
			Dim strMethod 

		'Variable Section End

		strMethod = strKeywordForReporting

			If CheckObjectExist(objRadioButton)  Then 
				If CheckObjectInVisible(objRadioButton) Then
					VerifyRadioButtonInVisible = 0
					WriteStatusToLogs "The RadioButton was in InVisible mode."
				Else
					VerifyRadioButtonInVisible = 2
					WriteStatusToLogs "The RadioButton was not in InVisible mode, Please verify."
				End If
			Else
					VerifyRadioButtonInVisible = 1
					WriteStatusToLogs "The RadioButton does not exist, Please verify."
			End If
		
	On Error GoTo 0
End Function






'=======================Code Section End=============================================
