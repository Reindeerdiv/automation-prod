

'=======================Code Section Begin=============================================

Option Explicit 'Forces explicit declaration of all variables in a script. 

'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name :				 CompareListBoxDefaultValue()
' 	Purpose :			Compares Default value of specified Listbox with our expected value
' 	Return Value :		 		  Integer( 0/2/1)
' 	Arguments :
'		Input Arguments :  		objListBox,strExpValue
'		Output Arguments :		strActValue 
'	Function is called by :     
'	Function calls :			   CheckObjectExist(),MatchEachArrData()
'	Created on :				 19/03/2008
'	Author :						Rathna Reddy 
'
'------------------------------------------------------------------------------------------------------------------------------------------


Function CompareListBoxDefaultValue(ByVal objListBox , ByVal strExpValue)
	On Error Resume Next
		'Variable Section Begin
			
			Dim strMethod
			Dim strActValue , arrActItem , arrExpItem
			Dim cnt , intItem , i
			Dim strMsgExpArray
           
		 'Variable Section End

		 strMethod = strKeywordForReporting

		     If CheckObjectExist(objListBox) Then  

					strActValue=objListBox.GetRoProperty("default value")
					
					If not Isempty(strActValue) and Err.Number =0 Then

						arrActItem = GetArray(strActValue , ";")
						
						arrExpItem = strExpValue
						For i=0 to  Ubound(arrExpItem) 
							If  trim(arrExpItem(i)) = "" Then
								cnt = objListBox.GetROProperty("items count")
								For intItem=1 to cnt
									If trim(objListBox.getItem(intItem)) = "" then
										arrExpItem(i) = "#"& (intItem-1)
										Exit for
									End if
								Next
						   
							End If
						Next

                         strMsgExpArray = PrintArrayElements(arrExpItem)
						 If MatchEachArrData(arrExpItem, arrActItem )= true Then   
							 CompareListBoxDefaultValue = 0
							 WriteStatusToLogs "The ListBox item(s): '"&strActValue&"' match the expected default value '"&strMsgExpArray&"'"
						 Else
							CompareListBoxDefaultValue = 2
							 WriteStatusToLogs "The ListBox item(s): '"&strActValue&"' does not match the expected default value '"&strMsgExpArray&"'"
						 End If	  
					Else
						 CompareListBoxDefaultValue=1
						  WriteStatusToLogs "Could not capture ListBox default value, please verify"
					End If

			Else
					 CompareListBoxDefaultValue = 1
					 WriteStatusToLogs "The ListBox does not exist, please verify"
            End If
			 
    On Error GoTo 0
End Function

'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name :				  SelectListItem()
' 	Purpose :					  Selects a specified item from the ListBox
' 	Return Value :		 		  Integer(0/1)
' 	Arguments :
'		Input Arguments :  		  objListBox , strSelectItem
'		Output Arguments :        strActItem
'	Function is called by :
'	Function calls :			  CheckObjectExist(),CheckObjectStatus()
'	Created on :				  19/03/2008
'	Author :					  Rathna Reddy 
' 
'------------------------------------------------------------------------------------------------------------------------------------------
Function SelectListItem(ByVal objListBox , ByVal strSelectItem)
	On Error Resume Next
		'Variable Section Begin
		
			Dim strMethod
			Dim strActItem, strActItems, arrActItems, arrExpItems
			Dim flag, innerflag, cnt, intItem, newindex
			Dim strItemToBeSelect
	
		'Variable Section End

		strMethod = strKeywordForReporting
		strItemToBeSelect=strSelectItem

			flag = True
			 
			
					If left(strSelectItem,1) = "#" Then
							strActItems = objListBox.GetROProperty("all items")
							If Err.number = 0 then
							
								If Not IsEmpty(strActItems) Then
									   arrActItems = GetArray(strActItems , ";")
									   arrExpItems = GetArray(strSelectItem , ";")

									  if CompareArrays(arrExpItems,arrActItems) = 0 Then
											flag = True
											cnt = objListBox.GetROProperty("items count")
											For  intItem=1 to cnt
												 If UCase(Trim(objListBox.getItem(intItem))) = UCase(Trim(strSelectItem)) Then
													newindex = intItem - 1
													strSelectItem = "#"&newindex
													Exit For
												 End If

											Next
												
									  Else
										flag = False
									  End If

								End If
							End If
					End If
					If flag Then
					objListBox.Select strSelectItem
					End If
					
					If Err.number = 0 And flag = True Then
					
							strActItem = objListBox.GetRoProperty("selection")
							'WriteStatusToLogs "strActItem = "&strActItem
							If Not IsEmpty(strActItem) Then
									 If strSelectItem = "" Then
										strItemToBeSelect = objListBox.GetRoProperty("selection")
									 End If
									 If CompareStringValues(strItemToBeSelect, strActItem) = 0 Then
										SelectListItem = 0
										WriteStatusToLogs "Given value is selected in the ListBox"
									 Else

									 'new code added
											 If left(strActItem,1) = "#" Then
												Dim strnewActItem
												strnewActItem = mid(strActItem,2,len(strActItem))
												strnewActItem = cint(strnewActItem) + 1
													If err.number = 0 Then
														strnewActItem = objListBox.GetItem(strnewActItem) 
																If CompareStringValues(strItemToBeSelect, strnewActItem) = 0 Then
																		SelectListItem = 0
																		WriteStatusToLogs "Given value is selected in the ListBox"
																Else
																		SelectListItem = 1
																		WriteStatusToLogs "The specific data could not be selected, please verify item exist."
																End If
													Else
														SelectListItem = 1
														WriteStatusToLogs "The specific data could not be selected, please verify item exist."
																
													End If
												WriteStatusToLogs "strnewActItem = "&strnewActItem
											Else
									 'end of new code addition
											SelectListItem = 1
											WriteStatusToLogs "The specific data could not be selected, please verify item exist."
											End If
									 End If
							Else

								
								SelectListItem = 1
								WriteStatusToLogs "The ListBox item could not be selected, please verify."

							End If

					Else
							SelectListItem=1
							WriteStatusToLogs "The List item '"&Trim(strSelectItem)&"' does not exist, please verify"
					End If
			 
		On Error GoTo 0
End Function
'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name :				 VerifyAllListItems()
' 	Purpose :					 Verifies  all the list items with our expected
' 	Return Value :		 		   Integer( 0/1/2)
' 	Arguments :
'		Input Arguments :  	  	objListBox,strExpItems
'		Output Arguments :     strActItems , arrActItems() , arrExpItems()
'	Function is called by :
'	Function calls :			   CheckObjectExist() , GetArray() , MatchEachArrData()
'	Created on :				  19/03/2008
'	Author :						Rathna Reddy 
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function VerifyAllListItems(ByVal objListBox , ByVal strExpItems)
	On Error Resume Next
		'Variable Section Begin
		
			Dim strMethod
			Dim strActItems , arrActItems , arrExpItems
			
		'Variable Section End

		 strMethod = strKeywordForReporting

			If CheckObjectExist(objListBox)  Then   
				strActItems = objListBox.GetROProperty("all items")
				If Err.number = 0 then
				
					If Not IsEmpty(strActItems) Then
					   arrActItems = GetArray(strActItems , ";")
					   If IsArray(strExpItems) then
							arrExpItems = strExpItems
						ElseIf Not IsObject(strExpItems) then
							arrExpItems = GetArray(strExpItems , "")
						End If
					
						If MatchEachArrData(arrExpItems, arrActItems) = True Then
							VerifyAllListItems = 0
							WriteStatusToLogs "The specified list items match with the expected list item."
						Else
							VerifyAllListItems = 2
							WriteStatusToLogs "The specified list items does not match the expected list item."
						End IF
					Else
						VerifyAllListItems = 1
						WriteStatusToLogs "ListBox does NOT contain any item, please verify."
					End If

				Else
					VerifyAllListItems = 1
					WriteStatusToLogs "Could not retrieve all the items of the ListBox"
				End if
			Else
			      VerifyAllListItems = 1
				  WriteStatusToLogs "The ListBox does not exist, please verify."
			End If
			'WriteStepResults VerifyListItems
    On Error GoTo 0
End Function
'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name :				 VerifyListItemsInSequence()
' 	Purpose :					 Compres the list box items with our expected items in sequence order.
' 	Return Value :		 		 Integer( 0/1/2)
' 	Arguments :
'		Input Arguments :  		objListBox , strExpItems
'		Output Arguments :      strItems , arrActItems() , arrExpecteditems()
'	Function is called by :
'	Function calls :			  CheckObjectExist(),GetArray(),CompareArraysInSequence()
'	Created on :				   19/03/2008
'	Author :						 Rathna Reddy 
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function VerifyListItemsInSequence(ByVal objListBox , ByVal strExpItems)
	On Error Resume Next
		'Variable Section Begin
		
			Dim strMethod
			Dim strActItems , arrActItems , arrExpItems
	  

		'Variable Section End

		 strMethod = strKeywordForReporting

	If CheckObjectExist(objListBox) Then   
			strActItems = objListBox.GetROProperty("all items")
			
			If Not IsEmpty(strActItems) Then
				arrActItems = GetArray(strActItems , ";")
				If IsArray(strExpItems) then
							arrExpItems = strExpItems
				ElseIf Not IsObject(strExpItems) then
							arrExpItems = GetArray(strExpItems , "")
				End If
				If CompareArraysInSequence(arrExpItems , arrActItems) = 0 Then
					VerifyListItemsInSequence = 0
					WriteStatusToLogs "The ListBox items sequence matches with the expected list sequence."
				Else
					VerifyListItemsInSequence = 2
					WriteStatusToLogs "The ListBox items are NOT in sequence with the expected list."
				End IF

			Else
				 VerifyListItemsInSequence = 1
				 WriteStatusToLogs "The ListBox does NOT contain any item, please verify."
			End If
			
	Else
		VerifyListItemsInSequence = 1
		WriteStatusToLogs "The ListBox does not exist, please verify."
	End If
		'WriteStepResults VerifyListItemsInSequence
 On Error GoTo 0
End Function

'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name :				 VerifyListBoxDisabled()
' 	Purpose :					  Verifies whether the ListBox is Disabled.
'	Return Value :		 		  Integer( 0/1/2)
' 	Arguments :
'		Input Arguments :  		  objListBox 
'		Output Arguments : 
'	Function is called by :
'	Function calls :			  CheckObjectExist() , CheckObjectStatus()
'	Created on :				  14/03/2008
'	Author :					   Rathna Reddy 
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function VerifyListBoxDisabled(ByVal objListBox)

On Error Resume Next
	'Variable Section Begin
		
		Dim strMethod

	'Variable Section End

	strMethod = strKeywordForReporting

		If CheckObjectExist(objListBox) Then
			If Not(CheckObjectStatus(objListBox)) Then
					VerifyListBoxDisabled = 0
					WriteStatusToLogs "The Listbox is disabled."
				Else
					VerifyListBoxDisabled = 2
					WriteStatusToLogs "The Listbox is not disabled, please verify."
				End If
            
		Else
				VerifyListBoxDisabled = 1
				WriteStatusToLogs "The ListBox does not exist, please verify."
		End if
		
	On Error GoTo 0
    
End Function

'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name :			   VerifyNoDuplicationInListBox()
' 	Purpose :					  Verifies the duplicate items in the listbox
' 	Return Value :		 		Integer( 0/1/2)
' 	Arguments :
'		Input Arguments :  	 objListBox
'		Output Arguments : 	strItems , arrActItems() , strActItems
'	Function is called by : 
'	Function calls :			CheckObjectExist,GetArray(),VerifyDuplicateElemetsInArray()
'	Created on :				19/03/2008
'	Author :					   Rathna Reddy 
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function VerifyNoDuplicationInListBox(ByVal objListBox)
	On Error Resume Next
		'Variable Section Begin
		
			Dim strMethod
			Dim strActItems , arrActItems 
			
         'Variable Section End

		  strMethod = strKeywordForReporting

			If CheckObjectExist(objListBox)  Then   
				strActItems = objListBox.GetROProperty("all items")
                
					If Not IsEmpty(strActItems) Then
						arrActItems = GetArray(strActItems , ";")
						If Not IsNull(arrActItems) Then
					
								If VerifyDuplicateElementsInArray(arrActItems) = 0 Then
									VerifyNoDuplicationInListBox = 0
									  WriteStatusToLogs "ListBox has no duplicate item"
								Else
									VerifyNoDuplicationInListBox = 2
									  WriteStatusToLogs "ListBox contains duplicate item(s), please verify."
								End If

						 Else
								VerifyNoDuplicationInListBox = 1
								 WriteStatusToLogs "Action failed because the listItem array received is NULL."
						 End If
					
					Else
						VerifyNoDuplicationInListBox = 1
						WriteStatusToLogs "Action fails to get ListBox items, please verify."
					
					End if
			Else
				VerifyNoDuplicationInListBox = 1
				WriteStatusToLogs "ListBox does not exist, please verify."
            End If
			'WriteStepResults VerifyNoDuplicationInListBox 
	On Error GoTo 0
End Function
'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name :			    SelectListItemByIndex()
' 	Purpose :					To Select the List box item by index
' 	Return Value :		 		Integer(0/1)
' 	Arguments :
'		Input Arguments :  	    intIndex
'		Output Arguments : 	
'	Function is called by : 
'	Function calls :			CheckObjectExist(),CheckObjectStatus()
'	Created on :				19/03/2008
'	Author :					Rathna Reddy 
'
'------------------------------------------------------------------------------------------------------------------------------------------

Function SelectListItemByIndex(objListBox, byval intIndex)
	
	On Error Resume Next
		'Variable Section Begin
		
			Dim strMethod
			Dim intGetIndex, intItemCount , intSelectedIndex 
			
         'Variable Section End

		strMethod = strKeywordForReporting
		
		If intIndex = "" Then
				intGetIndex = 0
				WriteStatusToLogs "The Index is empty. Hence its set to the default value '0'"

		ElseIf IsInteger(intIndex) <> 0 Then
				SelectListItemByIndex = 1
				WriteStatusToLogs  "The index '" & intIndex & "' is not a valid integer, please verify."
				Exit Function

		Else
				intGetIndex=Cint(intIndex)
		End If
		

		If CheckObjectExist(objListBox)  Then  
			If CheckObjectStatus(objListBox) = True then					
				intItemCount = CInt(objListBox.GetROProperty("items count"))
				intSelectedIndex = 	intGetIndex
				intGetIndex = intGetIndex 'This is the only keyword in QTP web that have index starting from 0 and its a defect. Need to change.
				'The correct code is intGetIndex = intGetIndex -1 
				If intGetIndex < intItemCount Then
						objListBox.Select "#"&intGetIndex
						If Err.Number = 0 Then
							SelectListItemByIndex= 0
							WriteStatusToLogs "The item with index "& intSelectedIndex&" is selected"
						Else
							SelectListItemByIndex= 1
							WriteStatusToLogs "Error occured while selecting list item with index :' "&intSelectedIndex&" ' , please verify."
						End If
				Else
						SelectListItemByIndex= 1
						WriteStatusToLogs "The Index Number provided is out of range. There are only '"& intItemCount &"' items in the specified ListBox. Please Verify."
							
				End If
					
			Else
				SelectListItemByIndex = 1
				WriteStatusToLogs "The specied Listbox is disabled ,Please verify"
			End if
		Else
			SelectListItemByIndex = 1
			WriteStatusToLogs "The Listbox does not exist"
		End If
		
   On Error GOTO 0
End Function
'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :				 VerifyListBoxExist()
'	Purpose :					 Verifies whether the ListBox Exists or not?
'	Return Value :		 		  Integer( 0/2)
'	Arguments :
'		Input Arguments :  		  objListBox 
'		Output Arguments : 
'	Function is called by :
'	Function calls :			 CheckObjectExist() 
'	Created on :				   10/03/2008
'	Author :						  Rathna Reddy 
'
'------------------------------------------------------------------------------------------------------------------------------------------

Function VerifyListBoxExist(ByVal objListBox)
	On Error Resume Next
		'Variable Section Begin
	
		   Dim strMethod

		'Variable Section End

		strMethod = strKeywordForReporting

			If CheckObjectExist(objListBox) Then 
			   
					VerifyListBoxExist = 0
					WriteStatusToLogs "The ListBox Exist."
					
			Else
					VerifyListBoxExist = 2
					WriteStatusToLogs "The ListBox does not exist, please verify."
					
			End If 
	  
	
	On Error GoTo 0
End Function

'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :				 VerifyListBoxNotExist()
'	Purpose :					 Verifies whether the ListBox Exists or not?
'	Return Value :		 		  Integer( 0/2)
'	Arguments :
'		Input Arguments :  		  objListBox 
'		Output Arguments : 
'	Function is called by :
'	Function calls :			 CheckObjectExist() 
'	Created on :				   10/03/2008
'	Author :						  Rathna Reddy 
'
'------------------------------------------------------------------------------------------------------------------------------------------

Function VerifyListBoxNotExist(ByVal objListBox)
	On Error Resume Next
		'Variable Section Begin
	
		   Dim strMethod

		'Variable Section End

		strMethod = strKeywordForReporting
		
			If CheckObjectNotExist(objListBox) Then 
			   
					VerifyListBoxNotExist = 0
					WriteStatusToLogs "The ListBox does not Exist."
					
			Else
					VerifyListBoxNotExist = 2
					WriteStatusToLogs "The ListBox Exists, please verify."
					
			End If 
	
  		
	On Error GoTo 0
End Function


'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name :				  SelectMultipleListItems()
' 	Purpose :					  Selects multiple items from the ListBox
' 	Return Value :		 		  Integer(0/1/2)
' 	Arguments :
'		Input Arguments :  		  objListBox , arrItems
'		Output Arguments :        strActItem
'	Function is called by :
'	Function calls :			  CheckObjectExist(),CheckObjectStatus(),GetArray(),CompareArraysCaseSensitive()
'	Created on :				  19/01/2009
'	Author :					  Rathna Reddy 
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function SelectMultipleListItems(ByVal objListBox , ByVal arrItems)
On Error Resume Next
	'Variable Section Begin
	
		Dim strMethod
		Dim arrActItems,arrExpItems,intItem , flag , strItemNotPresent
		Dim arrTemp , i , strAllItems , arrAllItems , arrModifiedExp()
		Dim arrActItemsSelected , cnt , newindex , strMultiple
		flag = True
		strItemNotPresent =""
	'Variable Section End

	 strMethod = strKeywordForReporting

	If CheckObjectExist(objListBox) Then   
		If CheckObjectStatus(objListBox) = True then

			strMultiple = objListBox.object.multiple
			If  strMultiple = true Then
			
				If Not IsArray(arrItems) or IsNull (arrItems) Then
					SelectMultipleListItems = 1
					WriteStatusToLogs "Given expected data is not an array. Please verify"
							
				Else	
					
					arrExpItems = arrItems
					strAllItems = objListBox.GetROProperty("all items")
					arrAllItems = GetArray(strAllItems , ";")
					If CompareArraysCaseSensitive(arrExpItems,arrAllItems) = 0 Then
						cnt = objListBox.GetROProperty("items count")
						ReDim preserve arrModifiedExp(UBound(arrExpItems))
						For i=0 to Ubound(arrExpItems)
							If trim(arrExpItems(i))="" or left(arrExpItems(i),1) = "#"  Then
								   
								For  intItem=1 to cnt
									 If UCase(Trim(objListBox.getItem(intItem))) = UCase(Trim(arrExpItems(i))) Then
											newindex = intItem - 1
											arrModifiedExp(i) = "#"&newindex
											If trim(arrExpItems(i))="" Then
												arrExpItems(i) = "#"&newindex
											End if
											Exit For
									 End If
	
								Next
							Else
								arrModifiedExp(i) = arrExpItems(i)
							End If
	
						Next
	
					   
						For  intItem=0 to Ubound(arrModifiedExp)
							 If intItem = 0 Then
								objListBox.Select trim(arrModifiedExp(intItem))
							 Else
								objListBox.ExtendSelect trim(arrModifiedExp(intItem))
							 End If
						 Next
	
						arrActItemsSelected = objListBox.GetRoProperty("selection")
						arrActItemsSelected=GetArray(arrActItemsSelected,";")
						If Err.Number=0 And Not IsNull(arrActItemsSelected) Then
							If CompareArraysCaseSensitive(arrExpItems , arrActItemsSelected) = 0 Then
								SelectMultipleListItems = 0
								WriteStatusToLogs "The Expected Multiple Items are selected sucessfully"
							Else
								SelectMultipleListItems = 2
								WriteStatusToLogs "Expected Multiple Items have not been selected, please verify"
							End If
	
						Else
							 SelectMultipleListItems = 1
							 WriteStatusToLogs "Could not capture selected items from ListBox to verify"
						End If
	
	
					Else
						SelectMultipleListItems = 1
						WriteStatusToLogs "Either All or Some of the list items are not available in the list, please verify"
					End If
	
				 End If
			 Else
				SelectMultipleListItems = 1
				WriteStatusToLogs "Cannot perform the multiple select , since the specified ListBox is not a multiple select listbox, please verify"
			 End if
		Else	   
			SelectMultipleListItems = 1
			WriteStatusToLogs "The specified ListBox is disabled, please verify"
		End If

	Else
		SelectMultipleListItems = 1
		WriteStatusToLogs "The specified ListBox doesn't exist, please verify"
	End If

On Error GoTo 0
End Function

'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name :				  VerifyMultipleSelectedListBoxItems()
' 	Purpose :					  Verifies the  selected items in ListBox
' 	Return Value :		 		  Integer(0/1/2)
' 	Arguments :
'		Input Arguments :  		  objListBox , arrItems
'		Output Arguments :        strActItem
'	Function is called by :
'	Function calls :			  CheckObjectExist(),MatchEachArrData(),GetArray()
'	Created on :				  19/01/2009
'	Author :					  Rathna Reddy 
'
'------------------------------------------------------------------------------------------------------------------------------------------


Function VerifyMultipleSelectedListBoxItems(ByVal objListBox , ByVal arrItems)
On Error Resume Next

	'Variable Section Begin

		Dim strMethod
		Dim arrActItems,arrExpItems , intItem , i , j , cnt , newindex , strSelectItem , arrTemp

	'Variable Section End

	strMethod = strKeywordForReporting

	If CheckObjectExist(objListBox) Then   

		If Not IsArray(arrItems) or IsNull (arrItems) Then
			VerifyMultipleSelectedListBoxItems = 1
			WriteStatusToLogs "Given expected data is not an array. Please verify"
				
		Else
			arrExpItems = arrItems
			  
			arrActItems = objListBox.GetRoProperty("selection")
			arrActItems = GetArray(arrActItems,";")

				 
			For i=0 to Ubound(arrExpItems)
				If  trim(arrExpItems(i)) = "" Then
					cnt = objListBox.GetROProperty("items count")
					For  intItem=1 to cnt
						 If UCase(Trim(objListBox.getItem(intItem))) = UCase(Trim(arrExpItems(i))) Then
							newindex = intItem - 1
							strSelectItem = "#"&newindex
							arrExpItems(i) = strSelectItem
							Exit for 
						 End If
					Next
				End If
			Next
			   
					   
			If Err.number=0 And Not IsNull(arrActItems) Then

				If MatchEachArrData(arrExpItems , arrActItems) = true Then
					VerifyMultipleSelectedListBoxItems = 0
					WriteStatusToLogs "The ListBox multiple selected items match the expected."
				Else
					VerifyMultipleSelectedListBoxItems = 2
					WriteStatusToLogs "ListBox selected items are different from the expected, please verify"
				End If

			Else
				 VerifyMultipleSelectedListBoxItems = 1
				 WriteStatusToLogs "Action fails to get selected list items into an array , please verify"
			End If
			
		End if
		
	Else
			VerifyMultipleSelectedListBoxItems = 1
			WriteStatusToLogs "The specified ListBox doesn't exist, please verify"
	End If

On Error GoTo 0
End Function

'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name :				 VerifyListBoxNOTContainsItem	()
' 	Purpose :					 Verifies  whether the ListBox doesn't contain the specified Item.
' 	Return Value :		 		   Integer( 0/1/2)
' 	Arguments :
'		Input Arguments :  	  	objListBox,strExpItems
'		Output Arguments :     sstrActItems , arrActItems , arrExpItems,intCount,strExpItem,intActItem
'	Function is called by :
'	Function calls :			   CheckObjectExist() , GetArray() 
'	Created on :				  23/01/2009
'	Author :						Rathna Reddy 
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function VerifyListBoxNOTContainsItem(ByVal objListBox , ByVal strListItem)
	On Error Resume Next
		'Variable Section Begin

			Dim strMethod
			Dim strActItems , arrActItems , arrExpItems,strExpItem,intActItem, blnEqualFlag
			blnEqualFlag=true
						
		'Variable Section End

		strMethod = strKeywordForReporting

			If CheckObjectExist(objListBox)  Then   
				strActItems = objListBox.GetROProperty("all items")
				
                If Err.Number = 0 Then
						
											
								arrActItems = GetArray(strActItems , ";")
							   If Not IsNull (arrActItems) Then
								 
 													  For intActItem = 0 to Ubound(arrActItems)
																   If strListItem = arrActItems(intActItem) Then
																		blnEqualFlag=false
																		Exit for
																   End If
                                                      Next
														 
                    										 If blnEqualFlag = true  Then
																  VerifyListBoxNOTContainsItem = 0
																	WriteStatusToLogs "List Does not contain the specified item."
                                  
															  Else
																	 VerifyListBoxNOTContainsItem = 2
																	  WriteStatusToLogs "List contain the specified item, please verify."
														  End If
												   
									 
							Else
								 VerifyListBoxNOTContainsItem = 1
								 WriteStatusToLogs "An error occurred, action fails to get Actual items, please verify."
						 End If
						
				Else
						VerifyListBoxNOTContainsItem = 1
						WriteStatusToLogs "Action Fails to get Items from ListBox, please verify ListBox method and property"
				End If
			
	 Else
			VerifyListBoxNOTContainsItem = 1
			WriteStatusToLogs "ListBox doesn't exist, please verify."

	 End If
			'WriteStepResults VerifyListBoxNOTContainsItem
    On Error GoTo 0
End Function
'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name :				 VerifyListBoxContainsItem		()
' 	Purpose :					 Verifies  whether the listBox contains the specified Item.
' 	Return Value :		 		   Integer( 0/1/2)
' 	Arguments :
'		Input Arguments :  	  	objListBox,strExpItems
'		Output Arguments :     sstrActItems , arrActItems , arrExpItems,intCount,strExpItem,intActItem
'	Function is called by :
'	Function calls :			   CheckObjectExist() , GetArray() 
'	Created on :				  23/01/2009
'	Author :						Rathna Reddy 
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function VerifyListBoxContainsItem	(ByVal objListBox , ByVal strListItem)
	On Error Resume Next
		'Variable Section Begin
		
			Dim strMethod
			Dim strActItems , arrActItems , arrExpItems,intCount,strExpItem,intActItem , blnpresentFlag
			blnpresentFlag=false
			
			
		'Variable Section End

		strMethod = strKeywordForReporting

			If CheckObjectExist(objListBox)  Then   
				strActItems = objListBox.GetROProperty("all items")
				
                If Err.Number = 0 Then
						
											
					   arrActItems = GetArray(strActItems , ";")
							   If Not IsNull (arrActItems) Then
								 
   									  
														  For intActItem = 0 to Ubound(arrActItems)
																   If strListItem = arrActItems(intActItem) Then
																		blnpresentFlag=True
																		Exit for
																   End If
                                                         Next
                                                         
															 If blnpresentFlag = true  Then
																  VerifyListBoxContainsItem	 = 0
																	WriteStatusToLogs "ListBox Contains the expected item."
                                  
															  Else
																	 VerifyListBoxContainsItem	 = 2
																	  WriteStatusToLogs "List does not contain the expected item, please verify."
														  End If
												   
									 
							Else
								 VerifyListBoxContainsItem	 = 1
								 WriteStatusToLogs "Action fails to get Actual items into an array please verify."
						 End If
						
					Else
						VerifyListBoxContainsItem	 = 1
						WriteStatusToLogs "Action Fails to get Items from ListBox, please verify ListBox method and property"
				End If
			
	 Else
	 VerifyListBoxContainsItem	 = 1
	 WriteStatusToLogs "ListBox doesn't exist, please verify."

	 End If
			'WriteStepResults VerifyListBoxContainsItem	
    On Error GoTo 0
End Function

'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name :				 VerifyListBoxSize()
' 	Purpose :					 Verifies the size(no.of items) of ListBox
' 	Return Value :		 		   Integer( 0/1/2)
' 	Arguments :
'		Input Arguments :  	  	objListBox,intItemCount
'		Output Arguments :    intActCount,intCount
'	Function is called by :
'	Function calls :			   CheckObjectExist() , GetArray() 
'	Created on :				  23/01/2009
'	Author :						Rathna Reddy 
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function VerifyListBoxSize(ByVal objListBox , ByVal intExpectedSize)
	On Error Resume Next
		'Variable Section Begin

			Dim strMethod
			Dim intActCount,intCount
			
		'Variable Section End

		 strMethod = strKeywordForReporting

		If intExpectedSize = "" Then
				intCount=0
				WriteStatusToLogs "The expected Size data is empty. Hence its set to the default value '0'"

		ElseIf IsInteger(intExpectedSize) <> 0 Then
				VerifyListBoxSize = 1
				WriteStatusToLogs  "The expected Size '" & intExpectedSize & "' is not a valid integer, please verify."
				Exit Function

		Else
				intCount=Cint(intExpectedSize)
		End if


		If CheckObjectExist(objListBox)  Then  				

				intActCount = objListBox.GetROProperty("items count")
				If Not IsEmpty(intActCount)  Then
						If CompareStringValues(intCount,intActCount)=0 Then
							  VerifyListBoxSize = 0
							  WriteStatusToLogs "The specified ListBox Size is same as the expected size."
						Else
							   VerifyListBoxSize = 2
								WriteStatusToLogs "The specified ListBox Size is  " & intActCount & " different from the expected Size " & intExpectedSize & ", please verify."
						End If
				Else
						VerifyListBoxSize = 1
						WriteStatusToLogs "Action Fails to get ListBox Items Count, please verify ListBox and its method and property"
				End If
				
		Else
				VerifyListBoxSize = 1
				WriteStatusToLogs "The specified ListBox doesn't exist, please verify."

		End If
			'WriteStepResults VerifyListBoxNOTContainsItem
    On Error GoTo 0
End Function

'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name :				  DeSelectListBoxItem()
' 	Purpose :					  Deselect one Selected item  in  the ListBox.The listbox can have multiple selected items.
' 	Return Value :		 		  Integer( 0/1/2)
' 	Arguments :
'		Input Arguments :  		  objListBox , strListItem
'		Output Arguments :        strSelectedItem
'	Function is called by :
'	Function calls :			  CheckObjectExist(),CheckObjectStatus()
'	Created on :				  19/03/2008
'	Author :					  Rathna Reddy 
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function DeSelectListBoxItem(ByVal objListBox , ByVal strListItem)
	On Error Resume Next
		'Variable Section Begin
		
			Dim strMethod
			Dim strSelectedItem,arrExp(0), arrAct
		
	
		'Variable Section End

		strMethod = strKeywordForReporting

			If CheckObjectExist(objListBox) Then   
				If CheckObjectStatus(objListBox) = True then          
						strSelectedItem = Trim(objListBox.GetRoProperty("Selection"))
						If Not IsEmpty(strSelectedItem) Then
								arrAct=GetArray(strSelectedItem,";")
								arrExp(0)=strListItem
								If Not IsNull(arrExp) And Not IsNull(arrAct) Then
										If CompareArrays(arrExp,arrAct) = 0 Then 
												objListBox.Deselect Trim(strListItem)
												strSelectedItem = Trim(objListBox.GetRoProperty("Selection"))
												If strSelectedItem="" Then
														DeSelectListBoxItem = 0
														WriteStatusToLogs "The "&strListItem&" List Item  is deselected  successfully"
												Else
														arrAct=GetArray(strSelectedItem,";")
														If  CompareArrays(arrExp,arrAct) <> 0 Then
														  
																DeSelectListBoxItem = 0
																WriteStatusToLogs "The "&strListItem&" List Item  is deselected  successfully"
														Else
																DeSelectListBoxItem = 2
																WriteStatusToLogs "List  item  is not Deselected successfully, please verify."
														End If

												End IF

										Else
												 DeSelectListBoxItem = 2
												 WriteStatusToLogs "Either the expected item is not in selection mode OR the item does not exist in the list item, please verify."

										End If
								Else
										DeSelectListBoxItem = 1
										WriteStatusToLogs "An error occurred, action fails to get the Array of the Expected and Actual selected item , please verify."
								End IF
							
						Else
								DeSelectListBoxItem = 1
								WriteStatusToLogs "Either the Action fails to get the selected list item OR no item is selected in the list, please verify."
						End If
				 Else
			   
						DeSelectListBoxItem = 1
					    WriteStatusToLogs "The specified ListBox is disabled, please verify."
				End If
			 Else
			   
						DeSelectListBoxItem = 1
					    WriteStatusToLogs "ListBox does not exist, please verify."
			 End If
	On Error GoTo 0
End Function
'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name :				  DeSelectMultipleListBoxItems()
' 	Purpose :						 DeSelects  selected multiple items from the ListBox
' 	Return Value :		 		   Integer( 0/1)
' 	Arguments :
'		Input Arguments :  		objListBox , arrItems
'		Output Arguments :     strActItem
'	Function is called by :
'	Function calls :				CheckObjectExist(),CheckObjectStatus()
'	Created on :				   19/01/2009
'	Author :						 Rathna Reddy 
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function DeSelectMultipleListBoxItems(ByVal objListBox , ByVal arrItems)
	On Error Resume Next
		'Variable Section Begin
			
			Dim strMethod
			Dim arrActItems,arrExpItems,intRes,strExpItem
			Dim intCountBeforeDeselect,intCountAfterDeselect,intCount,intExpItemsSize
			
	
		'Variable Section End

		strMethod = strKeywordForReporting

		If CheckObjectExist(objListBox) Then   
			If CheckObjectStatus(objListBox) = True then  
				If  objListBox.object.multiple = true Then
				
					If IsArray(arrItems) then
						arrExpItems = arrItems
					ElseIf Not IsObject(arrItems) then
						arrExpItems = GetArray(arrItems , "")
					End If	

				   If Not IsNull(arrExpItems) Then
					   'Before Deselect
						   arrActItems = objListBox.GetRoProperty("selection")
						   intCountBeforeDeselect=objListBox.GetRoProperty("selected items count")
						   If ArrActItems <> Empty Then
											arrActItems=GetArray(arrActItems,";")
											If Not IsNull(arrActItems) Then
													  If  CompareArrays(arrExpItems, arrActItems) = 0 Then
															   For Each strExpItem in arrExpItems
																	objListBox.Deselect Trim(strExpItem)
															    Next

																arrActItems = objListBox.GetRoProperty("selection") 
																intCountAfterDeselect=objListBox.GetRoProperty("selected items count")  
																intCount=intCountBeforeDeselect-intCountAfterDeselect
																intExpItemsSize=Ubound(arrExpItems)
																
																  If arrActItems=Empty  or  intExpItemsSize = intCount-1  Then
																		  DeSelectMultipleListBoxItems = 0
																		  WriteStatusToLogs "The expected  List items are deselected successfully."
																
																  Else
																		 DeSelectMultipleListBoxItems =  2
																		 WriteStatusToLogs "The expected  List items are not deselected successfully, please verify."
																  End If



														
														Else
														DeSelectMultipleListBoxItems =  1
															WriteStatusToLogs "The Expected item(s) are either not in selected mode Or does not exist, please verify"
														End If

								
											 Else
											 DeSelectMultipleListBoxItems =  1
													WriteStatusToLogs "Action fails to get ListItems into an array, please verify ."
																			   
											 End If
							 
						   Else
						   DeSelectMultipleListBoxItems =  1
								 WriteStatusToLogs "Action fails to get List Items, please verify Listbox Method and property"
						  End If
				
				Else
						DeSelectMultipleListBoxItems =  1
						WriteStatusToLogs "Action fails to get Expected Items into an array, please verify"
				End If

			Else
				DeSelectMultipleListBoxItems =  1
				WriteStatusToLogs "Deselect action cannot be perform on a single select ListBox , please verify"	
			End if
		Else
			DeSelectMultipleListBoxItems =  1
			WriteStatusToLogs "The specified ListBox is disabled, please verify"
		End If

	Else
		DeSelectMultipleListBoxItems =  1
		WriteStatusToLogs "The ListBox doesn't exist, please verify"
	End If



	On Error GoTo 0
End Function
'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name :				 VerifyListBoxEnabled()
' 	Purpose :					 Verifies whether the ListBox is Enabled
' 	Return Value :		 		  Integer( 0/1/2)
' 	Arguments :
'		Input Arguments :  		objListBox 
'		Output Arguments : 
'	Function is called by :
'	Function calls :				CheckObjectExist() , CheckObjectStatus()
'	Created on :				   11/03/2008
'	Author :						  Rathna Reddy 
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function VerifyListBoxEnabled(ByVal objListBox)

On Error Resume Next
	'Variable Section Begin
		
		Dim strMethod

	'Variable Section End

	strMethod = strKeywordForReporting

		If CheckObjectExist(objListBox) Then
			If CheckObjectEnabled(objListBox) Then
					VerifyListBoxEnabled = 0
					WriteStatusToLogs "The ListBox was enabled."
				Else
					VerifyListBoxEnabled = 2
					WriteStatusToLogs "The ListBox was not enabled, please verify."
				End If
            
		Else
				VerifyListBoxEnabled = 1
				WriteStatusToLogs "The ListBox does not exist, please verify."
		End if
		
	On Error GoTo 0
    
End Function

'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :				 VerifyListBoxVisible()
'	Purpose :					 Verifies whether the ListBox is visible.
'	Return Value :		 		  Integer( 0/1/2)
'	Arguments :
'		Input Arguments :  		  objListBox 
'		Output Arguments : 
'	Function is called by :
'	Function calls :			 CheckObjectExist() , CheckObjectVisiblity()
'	Created on :				   10/03/2008
'	Author :						  Rathna Reddy 
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function VerifyListBoxVisible(ByVal objListBox)
	On Error Resume Next
		'Variable Section Begin
		
			Dim strMethod

		'Variable Section End

		 strMethod = strKeywordForReporting

			If CheckObjectExist(objListBox)  Then 
				If CheckObjectVisible(objListBox) Then
					VerifyListBoxVisible = 0
					WriteStatusToLogs "The ListBox was Visible."
				Else
					VerifyListBoxVisible = 2
					WriteStatusToLogs "The ListBox was not Visible, please verify."
				End If
			Else
					VerifyListBoxVisible = 1
					WriteStatusToLogs "The ListBox does not exist, please verify."
			End If
		
	On Error GoTo 0
End Function
'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :				 VerifyListBoxInVisible()
'	Purpose :					 Verifies whether the ListBox is Invisible.
'	Return Value :		 		  Integer( 0/1/2)
'	Arguments :
'		Input Arguments :  		  objListBox 
'		Output Arguments : 
'	Function is called by :
'	Function calls :			 CheckObjectExist() , CheckObjectVisiblity()
'	Created on :				   10/03/2008
'	Author :						  Rathna Reddy 
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function VerifyListBoxInVisible(ByVal objListBox)
	On Error Resume Next
		'Variable Section Begin
		
			Dim strMethod  

		'Variable Section End

		strMethod = strKeywordForReporting

			If CheckObjectExist(objListBox)  Then 
				If CheckObjectInVisible(objListBox) Then
					VerifyListBoxInVisible = 0
					WriteStatusToLogs "The ListBox was InVisible."
				Else
					VerifyListBoxInVisible = 2
					WriteStatusToLogs "The ListBox was not InVisible, please verify."
				End If
			Else
					VerifyListBoxInVisible = 1
					WriteStatusToLogs "The ListBox does not exist, please verify."
			End If
		
	On Error GoTo 0
End Function




'=======================Code Section End=============================================
