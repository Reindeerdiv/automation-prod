
'=======================Code Section Begin=============================================

Option Explicit 'Forces explicit declaration of all variables in a script. 
'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :			  ClickElement()
'	Purpose :					  Performs  Click opertion on a specified Element.
'	Return Value :		 		Integer(0/1/2)
'	Arguments :
'		Input Arguments :  	 objElement
'		Output Arguments : 
'	Function is called by :
'	Function calls :		     CheckObjectExist() , CheckObjectClick()
'	Created on :			    10/03/2008
'	Author :					   Rathna Reddy 
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function ClickElement(ByVal objElement)
	On Error Resume Next
		'Variable Section Begin
	
			Dim strMethod

		'Variable Section End
			strMethod = strKeywordForReporting
			If CheckObjectExist(objElement) Then   
			     If CheckObjectEnabled(objElement) Then
						If CheckObjectClick(objElement) Then
							ClickElement = 0
							WriteStatusToLogs "Click operation is performed on Element."
						Else
							ClickElement = 2
							WriteStatusToLogs "Click operation is not performed on Element, Please verify."
						End If 
				Else
                    ClickElement = 1
					WriteStatusToLogs "The specified Element is disabled, Please verify."
				End If
			Else
					ClickElement = 1
					WriteStatusToLogs "The specified Element does not exist."
		End If
		
	On Error GoTo 0
End Function
'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name :				CompareElementValue()
' 	Purpose :					Compares the Element Name  with our expected Name.
' 	Return Value :		 		  Integer(0/1/2)
' 	Arguments :
'		Input Arguments :  	   objElement , strExpName
'		Output Arguments : 	  strActName	
'	Function is called by :
'	Function calls :			   CheckObjectExist() , CompareStringValues()
'	Created on :				  13/03/2008
'	Author :						 Rathna Reddy 
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function CompareElementValue(ByVal objElement , ByVal strExpName)
	On Error Resume Next
		'Variable Section Begin
			Dim strMethod
			Dim strActName

		'Variable Section End
			strMethod = strKeywordForReporting
			If CheckObjectExist(objElement) Then         
					strActName = objElement.GetRoProperty("innertext")
					If Err.number=0 Then
					
							If CompareStringValues(strExpName,strActName) = 0  Then
							   CompareElementValue = 0
							   WriteStatusToLogs """"&"Expected Value : "&strExpName& """" &  "   and   " &""""&"Actual Value : "&strActName&""""&"  are equal."

							Else
								CompareElementValue = 2
								WriteStatusToLogs """"&"Expected Value : "&strExpName& """" &  "   and   " &""""&"Actual Value : "&strActName&""""&"  are not equal, Please verify."
							End If
					Else
							CompareElementValue = 1 
							WriteStatusToLogs "Action fails to Capture the specified Element's value,Please verify Object Properties."
					End If 
			Else
			CompareElementValue = 1
			WriteStatusToLogs "The specified Element does not exist, Please verify."
			End If
			
	On Error GoTo 0
End Function
'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :				 VerifyElementVisible()
'	Purpose :					 Verifies whether the Element is visible.
'	Return Value :		 		  Integer(0/1/2)
'	Arguments :
'		Input Arguments :  		  objElement 
'		Output Arguments : 
'	Function is called by :
'	Function calls :			 CheckObjectExist() , CheckObjectVisiblity()
'	Created on :				   10/03/2008
'	Author :						  Rathna Reddy 
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function VerifyElementVisible(ByVal objElement)
	On Error Resume Next
		'Variable Section Begin
		
			Dim strMethod

		'Variable Section End
			strMethod = strKeywordForReporting
			If CheckObjectExist(objElement)  Then 
				If CheckObjectVisible(objElement) Then
					VerifyElementVisible = 0
					WriteStatusToLogs "The specified Element is Visible"
				Else
					VerifyElementVisible = 2
					WriteStatusToLogs "The specified Element is not Visible,Please verify."
				End If
			Else
					VerifyElementVisible = 1
					WriteStatusToLogs "The specified Element does not exist, Please verify."
			End If
		
	On Error GoTo 0
End Function
'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :				 VerifyElementInVisible()
'	Purpose :					 Verifies whether the Element is Invisible.
'	Return Value :		 		  Integer(0/1/2)
'	Arguments :
'		Input Arguments :  		  objElement 
'		Output Arguments : 
'	Function is called by :
'	Function calls :			 CheckObjectExist() , CheckObjectVisiblity()
'	Created on :				   10/03/2008
'	Author :						  Rathna Reddy 
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function VerifyElementInVisible(ByVal objElement)
	On Error Resume Next
		'Variable Section Begin
		
			Dim strMethod

		'Variable Section End
			strMethod = strKeywordForReporting
			If CheckObjectExist(objElement)  Then 
				If CheckObjectInVisible(objElement) Then
					VerifyElementInVisible = 0
					WriteStatusToLogs "The specified Element is not visible"
				Else
					VerifyElementInVisible = 2
					WriteStatusToLogs "The specified Element is visible, Please verify."
				End If
			Else
					VerifyElementInVisible = 1
					WriteStatusToLogs "The specified Element does not exist, Please verify."
			End If
		
	On Error GoTo 0
End Function
'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :			VerifyElementContainsValue()
'	Purpose :				Verifies whether the element contains the sepcified  string
'	Return Value :		 	Integer(0/1/2)
'	Arguments :
'		Input Arguments :   objElement    
'		Output Arguments :  strActString
'	Function is called by : 
'	Function calls :		CheckObjectExist()
'	Created on : 			04/03/2008
'	Author :				Rathna Reddy
'
'-------------------------------------------------------------------------------------------

Function VerifyElementContainsValue (ByVal objElement , ByVal strSubString)
'	On Error Resume Next
		'Variable Section Begin
			Dim strMethod
			Dim strActString

		'Variable Section End
			strMethod = strKeywordForReporting
			If CheckObjectExist(objElement)  Then
					 strActString = trim(objElement.GetRoProperty("innertext"))
					If Err.Number = 0 Then
							 If InStr(strActString, strSubString ) > 0   Then 
								
								VerifyElementContainsValue = 0
								 WriteStatusToLogs "SubString  : " &""""& strSubString & """" &  " is  found within String : " & """"&strActString&""""
							 Else
								 VerifyElementContainsValue = 2
								 WriteStatusToLogs "SubString  : " &""""&strSubString& """" &  " is not  found within Stirng :  " & """"&strActString&""""&", Please verify."
							 End If
					 Else
							 VerifyElementContainsValue = 1
							 WriteStatusToLogs "Failed to Capture Element Value, Verify Object Properties."
					End If
					
			Else
				VerifyElementContainsValue = 1
				WriteStatusToLogs "Specified Element does not exist, please verify."
			End If
			
	On Error GoTo 0
End Function




'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :				 VerifyElementExist()
'	Purpose :					 Verifies whether the Element Exists or not?
'	Return Value :		 		  Integer(0/2)
'	Arguments :
'		Input Arguments :  		  objElement 
'		Output Arguments : 
'	Function is called by :
'	Function calls :			 CheckObjectExist() 
'	Created on :				   10/03/2008
'	Author :						  Rathna Reddy 
'
'------------------------------------------------------------------------------------------------------------------------------------------

Function VerifyElementExist(ByVal objElement)
	On Error Resume Next
		'Variable Section Begin
	
		   Dim strMethod

		'Variable Section End
			strMethod = strKeywordForReporting
			If CheckObjectExist(objElement) Then 
			   
					VerifyElementExist = 0
					WriteStatusToLogs "The Element Exists."
			Else
					VerifyElementExist = 2
					WriteStatusToLogs "The specified Element does NOT Exist, Please Verify."
					
			End If 
	  
	
	On Error GoTo 0
End Function

'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :				 VerifyElementNotExist()
'	Purpose :					 Verifies whether the Element Exists or not?
'	Return Value :		 		  Integer(0/2)
'	Arguments :
'		Input Arguments :  		  objElement 
'		Output Arguments : 
'	Function is called by :
'	Function calls :			 CheckObjectExist() 
'	Created on :				   10/03/2008
'	Author :						  Rathna Reddy 
'
'------------------------------------------------------------------------------------------------------------------------------------------

Function VerifyElementNotExist(ByVal objElement)
	On Error Resume Next
		'Variable Section Begin
	
		   Dim strMethod

		'Variable Section End
			strMethod = strKeywordForReporting
			If CheckObjectNotExist(objElement) Then 
			   
					VerifyElementNotExist = 0
					WriteStatusToLogs "The Element does not Exist."
					
			Else
					VerifyElementNotExist = 2
					WriteStatusToLogs "The specified Element Exists, Please Verify."
					
			End If 
	
  		
	On Error GoTo 0
End Function
'=======================Code Section End=============================================
