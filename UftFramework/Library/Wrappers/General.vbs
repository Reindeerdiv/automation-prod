'Function CloseAllOpenedFirefoxBrowsers()
'Function CloseAllOpenedIEBrowsers()
'Function CloseApplication(ByVal ProcessName)
'Function ComparePattern(byval strPattern , byval strData , byval blnCase)
'Function CompareString(byval strString1 , byval strString2 , byval blnCase)
'Function IncrementValue(byval strKey, byval Value, byval intIncrementValue)
'Function NavigateFireFoxBrowser(byval strURL)
'Function NavigateIEBrowser(byval strURL)
'Function OpenApplication(ByVal strPath)
'Function OpenFirefoxBrowser()
'Function OpenIEBrowser()
'Function OpenJavaApplication(ByVal strPath)
'Function PressKeys(byval strKey)
'Function PressKeys(byval strKey)
'Function StorePropertyFromObject (byval objObject, byval strKey , byval strPropName)
'Function StoreStringLength(byval strkey, byval strMainstr, byval intIncrement)
'Function StoreSubString (byval strKey, byval strMainString, byval intStart, byval intLen)
'Function StoreVariable(byval strKey, byval Value)
'Function VerifyDifference(Byval int1, Byval int2, Byval intDiff)
'Function VerifyStringContainsValue(byval strMainString , byval strSubString , byval blnCase)
'Function WaitFor(byval intExpTime)
'Function ExecuteDMLQueryAndStoreInKey(ByVal driver, ByVal url,ByVal userName,ByVal password,ByVal query,ByVal key)

'========================Code Section Begin=====================================================================
'Option Explicit	'Forces explicit declaration of all variables in a script. 
'
'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :				 OpenFirefoxBrowser()
'	Purpose :					 Opens a new blank Firefox Browser.
'	Return Value :		 		 Integer(0/1)
'	Arguments :
'		Input Arguments :  		 
'		Output Arguments :		objShell 
'	Function is called by :
'	Function calls :			  
'	Created on :				03/03/2008
'	Author :					Rathna Reddy 
''------------------------------------------------------------------------------------------------------------------------------------------

Function OpenFirefoxBrowser()
	On Error Resume Next 
		'Variable Section Begin
		
		Dim strMethod
		Dim objShell 
		
		'Variable Section End

		strMethod = strKeywordForReporting

		Set objShell=CreateObject("Wscript.shell")
		objShell.run "firefox about:blank",1
		wait(5)
				
		   If  err.Number = 0 Then 
				OpenFirefoxBrowser = 0
				WriteStatusToLogs "The firefox browser is open successfully."
		   Else
				OpenFirefoxBrowser = 1
				WriteStatusToLogs "The firefox browser is open not successfully."
				
		   End If
	On Error GoTo 0
End Function

'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :				 OpenIEBrowser()
'	Purpose :					 Opens a new blank Internet Explorer Browser.
'	Return Value :		 		 Integer(0/1)
'	Arguments :
'		Input Arguments :  		 
'		Output Arguments :		objShell 
'	Function is called by :
'	Function calls :			  
'	Created on :				03/03/2008
'	Author :					Rathna Reddy 
''------------------------------------------------------------------------------------------------------------------------------------------
Function OpenIEBrowser()
	On Error Resume Next 
		'Variable Section Begin
			
			Dim strMethod
			Dim objShell 
			
		'Variable Section End

		strMethod = strKeywordForReporting

			Set objShell=CreateObject("Wscript.shell")
				objShell.run "iexplore about:blank",1
				wait(5)
			
			   If  err.Number = 0 Then 
					OpenIEBrowser = 0
					WriteStatusToLogs "The IE browser is open successfully."
			   Else
					OpenIEBrowser = 1
					WriteStatusToLogs "The IE browser is not open successfully."
					
			   End If
	On Error GoTo 0
End Function


'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name :				 NavigateIEBrowser()
' 	Purpose :					 Opens a specified URL in the Browser.
' 	Return Value :		 		 Integer(0/1)
' 	Arguments :
'		Input Arguments :  		 objBrowser,strURL
'		Output Arguments : 
'	Function is called by :
'	Function calls :			 
'	Created on :				 03/03/2008
'	Author :					 Rathna Reddy 
'
'------------------------------------------------------------------------------------------------------------------------------------------

Function NavigateIEBrowser(byval strURL)
	'On Error Resume Next 
		'Variable Section Begin
			Dim strMethod
			Dim objDesc, obj, icount, i, ver, objShell
			Dim intfirefox, intcomp , blnCreateInstance
			Dim strLocation , objIE , objDesc1
			Dim intCounter , intMaxWaitCounter , blnIEInvoke
		   	intMaxWaitCounter = 4
			blnIEInvoke = false
			blnCreateInstance = true
		'Variable Section End
		   'Iohbha
		   Dim maxWaitTime , syncTime 
		   maxWaitTime = SYNCTIMEINSECONDS
		   syncTime = 5	

		strMethod = strKeywordForReporting
		
		If IsNull(strURL) Or IsEmpty(strURL) Or Trim(strURL)= "" Then
				NavigateIEBrowser = 1
				WriteStatusToLogs "The Url is invalid, please verify. "
				Exit Function
		End If

		'use descriptive program.
			Set objDesc=Description.Create
			objDesc("micclass").value="Browser"
			'objDesc("title").value="about:blank"
			objDesc("version").value = "internet explorer.*"
		   
			Set obj=Desktop.ChildObjects(objDesc)
			icount = obj.count
			If icount  > 0 Then
				For i = 0 to icount -1
						ver = trim(obj(i).getROProperty("version"))
						intcomp = instr(1,ver, "internet explorer",1)
						If  intcomp  > 0 Then
						   blnCreateInstance = false
						   obj(i).Navigate Trim(strURL)
						   wait(5)
						   Exit for
					   End If
				Next
			Else
				blnCreateInstance = true
			End If

			If blnCreateInstance = True Then
				Set objShell=CreateObject("Wscript.shell")
				objShell.run "iexplore about:blank",1
				wait(5)
				Set objDesc1=Description.Create
				objDesc1("micclass").value="Browser"
				objDesc1("version").value = "internet explorer.*"
			   
				
'				For intCounter = 1 to intMaxWaitCounter
'					Set objIE=Desktop.ChildObjects(objDesc1)
'					icount = objIE.count
'					If icount >0 Then
'						blnIEInvoke = True
'						Exit for
'					Else
'						wait(5)
'						intCounter = intCounter + 1
'					End if
'				Next

				While maxWaitTime > 0 
					Set objIE=Desktop.ChildObjects(objDesc1)
					icount = objIE.count
					If icount >0 Then
						blnIEInvoke = True
						maxWaitTime = -1
					Else
						wait(syncTime)
						maxWaitTime = maxWaitTime - syncTime
						WriteStatusToLogs "Waiting"
					End if
				Wend
				
				If blnIEInvoke = true Then
					objIE(0).Navigate Trim(strURL)
					wait(5)
				Else
					NavigateIEBrowser = 1
					WriteStatusToLogs "Action failed to invoke a blank IE browser to navigate the URL"				 
					Exit Function
				End If

			End If
			
			
			If  Err.number = 0 Then
				NavigateIEBrowser = 0 
				WriteStatusToLogs "The Url is navigated."
			Else
				NavigateIEBrowser = 1
				WriteStatusToLogs "The Url is not navigated."
				
			End If

	'On Error GoTo 0 
End Function

'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name :				 NavigateFireFoxBrowser()
' 	Purpose :					 Opens a specified URL in the FF Browser.
' 	Return Value :		 		 Integer(0/1)
' 	Arguments :
'		Input Arguments :  		 objBrowser,strURL
'		Output Arguments : 
'	Function is called by :
'	Function calls :			 
'	Created on :				 03/03/2008
'	Author :					 Rathna Reddy 
'
'------------------------------------------------------------------------------------------------------------------------------------------


Function NavigateFireFoxBrowser(byval strURL)
	On Error Resume Next 
		'Variable Section Begin

		Dim strMethod
		Dim objDesc, obj, icount, i, ver, objShell
		Dim intfirefox, intcomp , blnCreateInstance
		Dim objFF , objDesc1
		blnCreateInstance = true
		'Variable Section End

		strMethod = strKeywordForReporting

		If IsNull(strURL) Or IsEmpty(strURL) Or Trim(strURL)= "" Then
				NavigateFireFoxBrowser = 1
				WriteStatusToLogs "The Url is invalid, please verify."
				Exit Function
		End If
		
		'use descriptive program.
		Set objDesc=Description.Create
		objDesc("micclass").value="Browser"
		objDesc("version").value = "Mozilla Firefox.*"
		Set obj=Desktop.ChildObjects(objDesc)
		icount = obj.count

		If icount  > 0 Then
			For i = 0 to icount -1
					ver = trim(obj(i).getROProperty("version"))
					intcomp = instr(1,ver, "firefox",1)
					If  intcomp  > 0 Then
						blnCreateInstance = false
					    obj(i).Navigate Trim(strURL)
					    wait(5)
					    Exit for
				   End If
			Next
		Else
			blnCreateInstance = true	
		End If


		If blnCreateInstance = True Then
			Set objShell=CreateObject("Wscript.shell")
			objShell.run "firefox about:blank",1
			wait(5)
			Set objDesc1=Description.Create
			objDesc1("micclass").value="Browser"
			objDesc1("version").value = "Mozilla Firefox.*"

			Set objFF=Desktop.ChildObjects(objDesc1)
			icount = objFF.count
			If icount >0 Then
				objFF(0).Navigate Trim(strURL)
				wait(5)
			Else
				NavigateFireFoxBrowser = 1
				WriteStatusToLogs "Action failed to invoke a blank Fierefox browser to navigate the URL"				 
				Exit Function
			End If
			 
		End If
		
		If  err.Number = 0 Then
				NavigateFireFoxBrowser = 0 
				WriteStatusToLogs "The Url is navigated."
		Else
				NavigateFireFoxBrowser = 1
				WriteStatusToLogs "The Url is not navigated."
		End If		

	On Error GoTo 0 
End Function
			 
'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :				CloseAllOpenedIEBrowsers()
'	Purpose :					Closes all the Internet Explorer Browsers which are available on desktop  before start execution
'	Return Value :				Integer(0/1)
'	Arguments :
'	Input Arguments : 
'	Output Arguments : 
'	Function is called by :
'	Function calls :			   
'	Created on : 				  03/03/2008
'	Author :					  Rathna Reddy
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function CloseAllOpenedIEBrowsers()
	On Error Resume Next 
		'Variable Section Begin

			Dim strMethod

		'Variable Section End

		strMethod = strKeywordForReporting
		
			systemutil.CloseProcessByName("iexplore.exe")
				If  err.Number = 0 Then  
					CloseAllOpenedIEBrowsers = 0 
					WriteStatusToLogs "All open IE browser(s) are closed successfully."
				Else
					CloseAllOpenedIEBrowsers = 1  
					WriteStatusToLogs "Open IE browser(s) are not closed successfully."
					
				End If
     On Error GoTo 0 
End Function

'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :				CloseAllOpenedFirefoxBrowsers()
'	Purpose :					Closes all the Firefox Browsers which are available on desktop  before start execution
'	Return Value :				Integer( 0/1)
'	Arguments :
'	Input Arguments : 
'	Output Arguments : 
'	Function is called by :
'	Function calls :			   
'	Created on : 				  03/03/2008
'	Author :					  Rathna Reddy
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function CloseAllOpenedFirefoxBrowsers()
	On Error Resume Next 
		'Variable Section Begin

			Dim strMethod

		'Variable Section End

		strMethod = strKeywordForReporting

			systemutil.CloseProcessByName("firefox.exe")
				If  err.Number = 0 Then  
					CloseAllOpenedFirefoxBrowsers= 0 
					WriteStatusToLogs "Open Firefox browser(s) are closed successfully."
				Else
					CloseAllOpenedFirefoxBrowsers= 1  
					WriteStatusToLogs "Open Firefox browser(s) are not closed successfully."
				End If
     On Error GoTo 0 
End Function


'------------------------------------------------------------------------------------------------------------------------------------------
'Function Name :			WaitFor()
'Purpose :					Tool waits for specified seconds.
'Return Value :				Integer (0/1) 
'Arguments :
'Input Arguments :			intExpTime     
'Output Arguments :			intSecs   
'Function is called by :
'Function calls :
'Created on :				20/06/2008
'Author :                   Rathna Reddy
'------------------------------------------------------------------------------------------------------------------------------------------
Function WaitFor(byval intExpTime)
   On Error Resume Next 
			'Variable Section Begin
		
			 Dim strMethod	, intWaitTime

			'Variable Section End

			strMethod = strKeywordForReporting
			intWaitTime = SYNCTIMEINSECONDS
			If IsNull(intExpTime) Or Trim(intExpTime)= "" Then

					WriteStatusToLogs "The waitTime passed is '" & intExpTime &"'. Hence WaitTime parameter is considered " &_
										"as the default max sync time as per config setting :" & intWaitTime & " seconds"

			ElseIf IsWholeNumber(intExpTime) = 0 Then

					intWaitTime = intExpTime

			Else
					WaitFor = 1
					WriteStatusToLogs "The time '" & intExpTime & "' is not a valid positive integer, please verify."
					Exit Function
			End if
			
				
			wait(intWaitTime)
			If err.number =0 Then
					WaitFor = 0
					WriteStatusToLogs "Waited successfully for "&intWaitTime&" secs"
			Else
		   
					WaitFor = 1
					WriteStatusToLogs "An error occurred during the wait."
			End If
									
	On Error GoTo 0
End Function





'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :			  StorePropertyFromObject()
'	Purpose :				  Stores the property of the specified object
'	Return Value :		 	  Integer(0/1)
'	Arguments :
'		Input Arguments :  	  objObject , strKey , strPropName
'		Output Arguments : 
'	Function is called by :	  	
'	Function calls :		  GetPropertyToStore()
'	Created on :			  12/06/2009
'	Author :				  Iohbha K
'
'------------------------------------------------------------------------------------------------------------------------------------------

Function StorePropertyFromObject (byval objObject, byval strKey , byval strPropName)
On error resume Next
	'Variable Section Begin
	
		Dim strMethod
		Dim intVal, dicval

	'Variable Section End

	strMethod = strKeywordForReporting

	intVal = GetPropertyToStore	(objObject ,strKey ,strPropName)
	dicval = oDictStorage.item(ucase(strKey))
	
	If intVal = 0 Then
		StorePropertyFromObject  = 0
		WriteStatusToLogs "The '"&strPropName&"' property of the Object  is stored successfully in the Key '"&strKey&"'" &". Stored value: " & dicval

	Elseif intVal = 2 Then
		StorePropertyFromObject  = 1
		WriteStatusToLogs "The '"&strPropName&"' property is not available for the Object, please verify."
	ElseIf intVal = 1 Then
		StorePropertyFromObject  = 1
		WriteStatusToLogs "The specified object does not exist"	
	ElseIf intVal = 3 then
		StorePropertyFromObject  = 1
		WriteStatusToLogs "The PropertyName '"&strPropName&"' is invalid, please Verify."	
	ElseIf intVal = 4 Then
		StorePropertyFromObject  = 1
		WriteStatusToLogs "The Key passed is invalid.Please Verify."	
	End if

On error goto 0
End Function



'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name :				VerifyStringContainsValue()
' 	Purpose :					Validates that the substring is available within the main string.Does not validates is
'								the string isnull, isempty or is an object.If the substring is "" , then the function does not validate.
'								The sub-string is not trim in any case.			
' 	Return Value :		 		Integer(0/1/2)
' 	Arguments :
'		Input Arguments :  	    strMainString , strSubString , blnCase
'		Output Arguments : 	    
'	Function is called by :		
'	Function calls :			  
'	Created on :				13/06/2009
'	Author :					Iohbha K
'------------------------------------------------------------------------------------------------------------------------------------------

Function VerifyStringContainsValue(byval strMainString , byval strSubString , byval blnCase)
On error resume next
	'Variable Section Begin

		Dim strMethod
		Dim intCompare , intCase , blnCorrectCase
				
	'Variable Section End

	strMethod = strKeywordForReporting

	If isnull(strMainString) Or isobject(strMainString) or isnull(strSubString) Or isobject(strSubString) Then
		VerifyStringContainsValue = 1 
		WriteStatusToLogs  "Both / One  of the strings send are either Null or is not a string, please verify."		
	Else
		If blnCase = "" Then

			WriteStatusToLogs "The CaseSensitive data passed is empty. Hence its set to the default value 'True'"
			blnCorrectCase = True

		ElseIf IsBoolean(blnCase) = 1 Then 'If the case data is not boolean then assume default data
			blnCorrectCase = True
			WriteStatusToLogs "The CaseSensitive data passed is '" & blnCase & "'. Hence its set to the default value 'True'"

		Else
			blnCorrectCase = Trim(CBool(blnCase))
		End If				
					  		
						
		If blnCorrectCase Then
			intCase = VBBINARYCOMPARE
		Else
			intCase = VBTEXTCOMPARE
		End If

		intCompare =  Instr(1 , strMainString , strSubString ,intCase )
		If intCompare > 0 Then
			VerifyStringContainsValue = 0
			WriteStatusToLogs "The String '" & strMainString & "'  contains  the substring '"& strSubString&"'"&Vbtab&_
							 "With CaseSensitive comparison as : "&blnCorrectCase
		else
			VerifyStringContainsValue = 2
			WriteStatusToLogs "The String '" & strMainString & "'  does not contains  the substring '"& strSubString&"'"&Vbtab&_
							"With CaseSensitive comparison as : "&blnCorrectCase
		End If
													
	End If
    
On error goto 0
End Function



'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name :				CompareString()
' 	Purpose :					Compares two strings. The comparison can be either case sensitive / non case-sensitive.
'								The method can also compare strings having a newline character.The function
'								The spaces of both the strings are trimmed before comparing.
' 	Return Value :		 		Integer(0/1/2)
' 	Arguments :
'		Input Arguments :  	    strString1 , strString2 , blnCase
'		Output Arguments : 	    
'	Function is called by :		
'	Function calls :			CompareStringValuesCaseSensitive() ,  CompareStringValues()  
'	Created on :				13/06/2009
'	Author :					Iohbha K
'------------------------------------------------------------------------------------------------------------------------------------------

Function CompareString(byval strString1 , byval strString2 , byval blnCase)
On error resume next
	'Variable Section Begin

		Dim strMethod
		Dim intCompare , blnCorrectCase
				
	'Variable Section End

	strMethod = strKeywordForReporting

	If isnull(strString1) Or isobject(strString1) or isnull(strString2) or isobject(strString2) Then
		CompareString = 1 
		WriteStatusToLogs  "Both / One  of the strings send are either Null or is not a string, please verify."		
	Else
		If blnCase = "" Then

			WriteStatusToLogs "The CaseSensitive data passed is empty. Hence its set to the default value 'True'"
			blnCorrectCase = True

		ElseIf IsBoolean(blnCase) = 1 Then 'If the case data is not boolean then assume default data
			blnCorrectCase = True
			WriteStatusToLogs "The CaseSensitive data passed is '" & blnCase & "'. Hence its set to the default value 'True'"

		Else
			blnCorrectCase = Trim(CBool(blnCase))
		End if
							   
		If blnCorrectCase Then
			intCompare = CompareStringValuesCaseSensitive(strString1 ,strString2)
		else
			intCompare = CompareStringValues(strString1 ,strString2)
		End If

		If intCompare = 0 Then
			CompareString = 0
			WriteStatusToLogs "The String '" & strString1 & "' and '"&strString2&"' are equal."&vbtab&_
							"With CaseSensitive comparison as : "&blnCorrectCase
		Else
			CompareString = 2
			WriteStatusToLogs "The String '" & strString1 & "' and '"&strString2&"' are not equal."&vbtab&_
							 "With CaseSensitive comparison as : "&blnCorrectCase
	
		End If
				
	End If
    
On error goto 0
End Function


'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name :				ComparePattern()
' 	Purpose :					Validates that the strData passed matches the strPattern. 
' 	Return Value :		 		Integer(0/1/2)
' 	Arguments :
'		Input Arguments :  	    strPattern , strData , blnCase
'		Output Arguments : 	    
'	Function is called by :		
'	Function calls :			
'	Created on :				17/06/2009
'	Author :					Iohbha K
'------------------------------------------------------------------------------------------------------------------------------------------

Function ComparePattern(byval strPattern , byval strData , byval blnCase)
On error resume next
	'Variable Section Begin
	
		Dim strMethod
		Dim regEx ,Matches , blnMatch , strErrorMsg
		Dim blnCorrectCase
		
	'Variable Section End

	strMethod = strKeywordForReporting
			
		If Isnull(strPattern) or Isempty(strPattern) or Trim(strPattern)="" or  Isnull(strData) or Isempty(strData) Then
				ComparePattern = 1
				WriteStatusToLogs "Either the Pattern or the data passed is invalid , please verify."		
		Else
				If blnCase = "" Then

					WriteStatusToLogs "The CaseSensitive data passed is empty. Hence its set to the default value 'True'"
					blnCorrectCase = True

				ElseIf IsBoolean(blnCase) = 1 Then 'If the case data is not boolean then assume default data
					blnCorrectCase = True
					WriteStatusToLogs "The CaseSensitive data passed is '" & blnCase & "'. Hence its set to the default value 'True'"

				Else
					blnCorrectCase = Trim(CBool(blnCase))
				End If

				
				Set regEx = New RegExp         '
				regEx.Pattern = strPattern
				regEx.IgnoreCase = blnCorrectCase         '
				regEx.Global = True         
				blnMatch = regEx.Test(strData)
				If err.number = 0 Then
				
					If blnMatch Then
							 ComparePattern = 0
							 WriteStatusToLogs "The data '"&strData&"' matches the Pattern  '"&strPattern&"' with CaseSensitive comparison as : "&blnCorrectCase
					Else
							ComparePattern = 2
							WriteStatusToLogs "The data '"&strData&"' does not match the Pattern  '"&strPattern&"' with CaseSensitive comparison as : "&blnCorrectCase
					End If
				Else
					strErrorMsg = Err.description
					Err.clear
					ComparePattern = 1
					WriteStatusToLogs "A syntax error occurred in the pattern '"&strPattern&"' ,the error is : '"&strErrorMsg&"'"
				End if
						
				
    End If
On error goto 0
End Function

'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :			  PressKeys()
'	Purpose :				  Send the key input to the Browser	
'	Return Value :		 	  Integer(0/1)
'	Arguments :
'		Input Arguments :  	  objObject , strKey 
'		Output Arguments : 
'	Function is called by :
'	Function calls :		  CheckObjectExist()
'	Created on :			  12/06/2009
'	Author :				  Iohbha K
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function PressKeys(byval strKey)
	On error resume next
	'Variable Section Begin

	Dim strMethod
	Dim intActive , WshShell
	'Variable Section End

	strMethod = strKeywordForReporting

	If Isnull(strKey) or Isempty(strKey) or strKey="" Then
		PressKeys = 1
		WriteStatusToLogs "The Key send is invalid, please verify."
	Else
		Set WshShell = CreateObject("WScript.Shell")
		WshShell.SendKeys lcase(strKey)

        If err.number =0 Then
			PressKeys = 0
			WriteStatusToLogs "The Key is pressed successfully"
		Else
			PressKeys = 1
			WriteStatusToLogs "An error occurred in the action PressKeys. The key send is '"&strKey&"'"
		End If
	End if
	On error goto 0
End Function

Function StopTillWaitTime(tickTime)
	wait(ticktime)
	StopTillWaitTime=1
End Function
'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :			  	OpenApplication()
'	Purpose :				  	Starts the specified application.
'	Return Value :		 	  	Integer( 0/1/2)
'	Input Arguments :			strPath - The fully qualified path (with the executable) of the application.
'------------------------------------------------------------------------------------------------------------------------------------------
Function OpenApplication(ByVal strPath)
   On Error Resume Next
   'Variable Section Begin
	
			Dim strMethod 
			Dim WshShell, oExec
			Dim blnstatus
			Dim customMsg
			Dim intStatus

	'Variable Section End

	 
	strMethod = strKeywordForReporting

	If Isnull(strPath) or Isempty(strPath) or Trim(strPath)="" Then
		OpenApplication = 1
		WriteStatusToLogs "The path is invalid, please verify."
	Else

		blnstatus = False
		Set WshShell = CreateObject("WScript.Shell")
		Set oExec = WshShell.Exec(Trim(strPath))
		
		If vartype(oExec) = vbempty  Then
			blnstatus = False
		Else
			If oExec.Status = 0  Then
				blnstatus = True
			End If
		End If
		
		''msgbox "blnstatus >>>" & blnstatus
		
		If blnstatus AND Err.Number = 0  Then
		
			OpenApplication = STATUS_PASSED
			intStatus = STATUS_PASSED
			customMsg = "Application '"&Trim(strPath)&"' opened successfully."
		Else
			OpenApplication =STATUS_FAILED
			intStatus = STATUS_FAILED
			customMsg = " Error occured while attempting to open the Application. "&Err.description
		End If

		Call writeActionStatusToLog(intStatus,customMsg)
			
		Set WshShell  = Nothing
		Set oExec  = Nothing
	End If
	
On Error Goto 0
End Function

'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :			CloseApplication()
'	Purpose :				Closes the specified application
'	Input Arguments :		ProcessName - The process name to close.
'	Return Value :		 	Integer(0/1)
''------------------------------------------------------------------------------------------------------------------------------------------
Function CloseApplication(ByVal ProcessName)
  On Error Resume Next 
		'Variable Section Begin

			Dim strMethod
			Dim objWMIService, objProcess, colProcess
			Dim strComputer , strProcess
			Dim blnFound 
			strComputer = "."
			blnFound = false
			Dim customMsg
	 		Dim intStatus
		'Variable Section End

		strMethod = strKeywordForReporting

		If Isnull(ProcessName) or Isempty(ProcessName) or Trim(ProcessName)="" Then
			CloseApplication = 1
			WriteStatusToLogs "The Process name is invalid, please verify."
		Else

			strProcess = trim(ProcessName)
			Set objWMIService = GetObject("winmgmts:" & "{impersonationLevel=impersonate}!\\" & strComputer & "\root\cimv2")
			Set colProcess = objWMIService.ExecQuery ("Select * from Win32_Process")

			For Each objProcess in colProcess
				If  ucase( trim(objProcess.Name)) = ucase(trim(strProcess)) then
							blnFound = true
				End if
			Next

			If blnFound = true Then

				systemutil.CloseProcessByName(trim(strProcess))
				If  err.Number = 0 Then  
					CloseApplication= STATUS_PASSED 
					 intStatus = STATUS_PASSED
					customMsg =  "Application '"&strProcess&"' is closed successfully."
				Else
					CloseApplication= STATUS_FAILED
					intStatus = STATUS_FAILED
					customMsg = "Failed to close application '"&strProcess&"', please verify."
				End If
			Else
				CloseApplication= STATUS_FAILED 
				intStatus = STATUS_FAILED
				customMsg = "Failed to close the application '"&strProcess&"'. Either the application is not running or the application name is invalid, please verify."
			End If

			Call writeActionStatusToLog(intStatus,customMsg)
	End if
		   
On Error GoTo 0 
End Function



'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :			  PressKeys()
'	Purpose :				  Send the key input to the application.
'	Return Value :		 	  Integer(0/1)
'	Input Arguments :  	  	  strKey 
''------------------------------------------------------------------------------------------------------------------------------------------
Function PressKeys(byval strKey)
	On error resume next
	'Variable Section Begin

		Dim strMethod
		Dim WshShell
		Dim customMsg
			Dim intStatus
	'Variable Section End

	strMethod = strKeywordForReporting

	If Isnull(strKey) or Isempty(strKey) or strKey="" Then
		PressKeys = STATUS_FAILED
		intStatus = STATUS_FAILED
        customMsg = "The specified Key is invalid, please verify."
	Else
		Set WshShell = CreateObject("WScript.Shell")
		WshShell.SendKeys lcase(strKey)

        If err.number =0 Then
			PressKeys = STATUS_PASSED
			intStatus  = STATUS_FAILED
          customMsg = "The Key is pressed successfully."
		Else
			PressKeys = STATUS_FAILED
			intStatus = STATUS_FAILED
          customMsg = "An error occurred in the action PressKeys. The key send is '"&strKey&"'"
		End If
	End if
	 Call writeActionStatusToLog(intStatus,customMsg)
	On error goto 0
End Function

'=======================Code Section End======================================================================

Function StoreSubString (byval strKey, byval strMainString, byval intStart, byval intLen)
On error resume Next
	'Variable Section Begin
	
		Dim strMethod
		Dim strSub, intval, intLenMainStr

	 'Variable Section End

	strMethod = strKeywordForReporting

	If IsKey(strKey) <> 0 Then
		StoreSubString = 1
		WriteStatusToLogs "The key passed is either null/empty , please verify."
	Else
	
		If intStart = "" Then
				intStart = 1
				WriteStatusToLogs "The start index data is empty. Hence its set to the default value '1'"

		ElseIf IsInteger(intStart) = 1 Then

				StoreSubString = 1
				WriteStatusToLogs "The start index '" & intStart & "' is not a valid integer, please verify."
				Exit Function
		Else
				intStart = Trim(intStart)
		End If

		If intLen = "" Then
				intLen = Len(strMainString)
				WriteStatusToLogs "The upto length index passed is empty. Hence its set to the default value 'Main string length'= " & intLen

		ElseIf IsInteger(intLen) = 1 Then

				StoreSubString = 1
				WriteStatusToLogs "The upto length '" & intLen & "' is not a valid integer, please verify. The upto length starts from 1."
				Exit Function
		Else
				intLen = Trim(intLen)
		End If
			
		intLenMainStr = len(strMainString)
		
		If Cint(intLenMainStr) >= Cint(intLen) Then
				If Cint(intStart) > 0 and CInt(intStart)<=  CInt(intLenMainStr) Then
                    	strSub = mid(strMainString,intStart,intLen)
						If err.number = 0 Then
							intval = AddItemToStorageDictionary(strKey,strSub)
							If  intval = 0 Then
								StoreSubString = 0
								WriteStatusToLogs "The substring '"&strSub&"'  is stored successfully in the Key '"&strKey&"'" 
										  
							Else
								StoreSubString = 1
								WriteStatusToLogs "The substring '"&strSub&"'  is not stored successfully in the Key '"&strKey&"'" 
										  
							End If
									   
						Else
							StoreSubString = 1
							WriteStatusToLogs  "An Error occured in the action, please verify. "&Err.description	
						End If
					
				Else
					StoreSubString = 1
					WriteStatusToLogs "The start  position provided '"&intStart&"' is not a valid. either its less than 1 or exceed the length of the string '"&strMainString&"' , please verify."
				End If
		Else
			StoreSubString = 1
			WriteStatusToLogs "The length provided '"&intLen&"' is greater than the length of the String '"&strMainString&"' , please verify."
		End If
		

	End If

On Error goto 0
End Function

Function StoreStringLength(byval strkey, byval strMainstr, byval intIncrement)

   On Error Resume Next
	
	'Variable Section Begin

		Dim strMethod
		Dim intLength, intval
		
	'Variable Section End

	strMethod = strKeywordForReporting

	If IsKey(strkey) <> 0 Then
		StoreStringLength = 1
		WriteStatusToLogs "The key passed is either null/empty , please verify."
	Else

		If intIncrement = "" Then
				intIncrement = 0
				WriteStatusToLogs "The increment value is empty. Hence its set to the default value '0'"

		ElseIf IsInteger(intIncrement) = 1 Then

				StoreStringLength = 1
				WriteStatusToLogs "The value '" & intIncrement & "' is not a valid integer, please verify."
				Exit Function
		Else
				intIncrement = Trim(intIncrement)
		End If

		intLength = len(strMainstr)
		intLength = intLength + intIncrement

		intval = AddItemToStorageDictionary(strKey,intLength)
		If  intval = 0 Then
			StoreStringLength = 0
			WriteStatusToLogs "The Length with increment '"& intLength &"' of the string  '"& strMainstr &"'  is stored successfully in the Key '"&strKey&"'" 
					  
		Else
			StoreStringLength = 1
			WriteStatusToLogs "The Length with increment '"& intLength &"' of the string  '"& strMainstr &"'  is NOT stored successfully in the Key '"&strKey&"'" 
					  
		End If	
	End If

On Error goto 0
End Function


'-------------------------------------------------------------------------------------------------
'	Function Name :			  StoreVariable()
'	Purpose :				  To be initiate the variable and assign the value
'	Return Value :		 	  Integer(0/1)
'	Arguments :
'		Input Arguments :  	  1. strkey :key which is used to store the value
'							  2. intIncrement: incremental value to be store.
'		Output Arguments : 
'	Function is called by :	  	
'	Function calls :		  WriteStatusToLogs()
'-------------------------------------------------------------------------------------------------
Function StoreVariable(byval strKey, byval Value)
 On Error Resume Next
	
	'Variable Section Begin

	Dim strMethod
	Dim intval
	intval = 1
	'Variable Section End

	strMethod = strKeywordForReporting

	If IsKey(strKey) <> 0 Then
		StoreVariable = 1
		WriteStatusToLogs "The key passed is either null/empty , please verify."
	Else	

		intval = AddItemToStorageDictionary(strKey, Value)
		If  intval = 0 Then
			StoreVariable = 0
			
			If strKey <> "$$_STATUS" Then
				WriteStatusToLogs "The value  '"& Value &"'  is stored successfully in the Key '"&strKey&"'"
			End If					 
					  
		Else
			StoreVariable = 1
			If strKey <> "$$_STATUS" Then
				WriteStatusToLogs "The value  '"& Value &"'  is NOT stored successfully in the Key '"&strKey&"'"
			End If 
					  
		End If

	End If

On Error goto 0	
End Function

Function VerifyDifference(Byval int1, Byval int2, Byval intDiff)
On Error Resume Next

		'Variable Section Begin
			Dim lint1, lint2, lDiff
			Dim strMethod
	   'Variable Section End			

		strMethod = strKeywordForReporting

		If int1 = "" Then
			lint1 = 0
			WriteStatusToLogs "The first integer data is empty. Hence its set to the default value '0'"

		ElseIf IsInteger(int1) = 1 Then
			VerifyDifference = 1
			WriteStatusToLogs  "The integer '" & int1 & "' is not a valid integer, please verify."
			Exit Function

		Else
			int1 = Trim(int1)
			lint1 = CLng(int1)
		End if

		If int2 = "" Then
			lint2 = 0
			WriteStatusToLogs "The second integer data is empty. Hence its set to the default value '0'"

		ElseIf IsInteger(int2) = 1 Then
			VerifyDifference = 1
			WriteStatusToLogs  "The integer '" & int2 & "' is not a valid integer, please verify."
			Exit Function
			
		Else
			int2 = Trim(int2)
			lint2 = CLng(int2)
		End if
		
		If intDiff = "" Then
			lDiff = 0
			WriteStatusToLogs "The difference data is empty. Hence its set to the default value '0'"

		ElseIf IsNumber(intDiff) = 1 Then
			VerifyDifference = 1
			WriteStatusToLogs  "The difference '" & intDiff & "' is not a valid integer, please verify."
			Exit Function

		else
			intDiff = Trim(intDiff)
			lDiff = CLng(intDiff)
		End if	

   
		If lint1 > lint2 Then
			   If lint1 = lint2 + lDiff Then
						VerifyDifference = 0
						WriteStatusToLogs  "The Difference between "& lint1 &" and  "& lint2 &" is same as expected value."
				Else
						VerifyDifference = 1
						WriteStatusToLogs  "The Difference between "& lint1 &" and  "& lint2 &" is Not same as expected value."
			   End If
		Elseif lint1 < lint2 Then 
				If lint1 = lint2 - lDiff Then
						VerifyDifference = 0
						WriteStatusToLogs  "The Difference between "& lint1 &" and  "& lint2 &" is same as expected value."
				Else
						VerifyDifference = 1
						WriteStatusToLogs  "The Difference between "& lint1 &" and "& lint2 &" is Not same as expected value."
				End If
		ElseIf  lint1 = lint2 Then
				If lDiff=lint1-lint2 Then
						VerifyDifference = 0
						WriteStatusToLogs  "The Difference between "& lint1 &" and  "& lint2 &" is same as expected value."
				else
						 VerifyDifference = 1
						 WriteStatusToLogs  "The Difference between "& lint1 &" and  "& lint2 &" is Not same as expected value."
				End If
		End if 		   		  

  On Error goto 0
End Function

Function IncrementValue(byval strKey, byval Value, byval intIncrementValue)

   On Error Resume Next
	
	'Variable Section Begin

		Dim strMethod
		Dim resValue
	'Variable Section End

	strMethod = strKeywordForReporting

	resValue = Value + intIncrementValue
	intval = AddItemToStorageDictionary(strKey, resValue)
		  If  intval = 0 Then
				IncrementValue = 0
				WriteStatusToLogs "The Length with increment '"& intLength &"' of the string  '"& strMainstr &"'  is stored successfully in the Key '"&strKey&"'" 
						  
		  Else
				 IncrementValue = 1
				WriteStatusToLogs "The Length with increment '"& intLength &"' of the string  '"& strMainstr &"'  is NOT stored successfully in the Key '"&strKey&"'" 
						  
		  End If
End Function

Function OpenJavaApplication(ByVal strPath)
	On Error Resume Next
	Dim strMethod
	Dim fso, f
		Set fso = CreateObject("Scripting.FileSystemObject")
		Set f = fso.GetFile(strPath)

		strMethod = strKeywordForReporting

		SystemUtil.Run strPath,"",f.ParentFolder,"",1
   
		If  Err.Number = 0  Then
			OpenJavaApplication =0
			WriteStatusToLogs "Application '"&Trim(strPath)&"' opened successfully"
		Else
			OpenJavaApplication = 1
			WriteStatusToLogs "Error occurred during opening of Application. "&Err.description
		End If

	Set fso = Nothing
	Set f = Nothing

	On Error Goto 0

End Function

'DB actions added
Function OpenAndStroreDBConnection (ByVal strConnectString, ByVal strKey)
		On Error Resume Next

		Dim strMethod
		Dim newconnection

		strMethod = strKeywordForReporting

	   ' 'msgbox "in openDBConnection"
		Set newconnection = CreateObject("ADODB.Connection")
		
		  newconnection.Open strConnectString
		  
		' 'msgbox "in openDBConnection2"
		If  Err.Number = 0  Then
		
		AddItemToStorageDictionary strKey, newconnection
		OpenAndStroreDBConnection = 0
		WriteStatusToLogs "Database connection created and stored in key =  "&strKey
		Else
			OpenAndStroreDBConnection = 1
			WriteStatusToLogs "Error occurred while opening a Database connection "&Err.Description
		End If
		On Error Goto 0
End Function

Function VerifyFieldValueC (ByVal strConnectKey, ByVal strQuery, ByVal strExpVal )
On Error Resume Next
		Dim strMethod
		Dim myConnection, rs, strActual, intcomp
		
		Set myConnection = oDictStorage.Item(strConnectKey)

		Set rs = myConnection.Execute( strQuery ) 

		strMethod = strKeywordForReporting

		If  Err.Number = 0  Then
			 strActual = trim(rs.fields(0).value) ' access first field in the query
			intcomp = instr(1,strActual, strExpVal,0) 'case sensitive due to last argument which is 0.
			If intcomp > 0 Then
				VerifyFieldValueC = 0
				WriteStatusToLogs "Expected value : "&strExpVal&" matches with actual :"&strActual
			Else
				VerifyFieldValueC = 2
				WriteStatusToLogs "Expected value : "&strExpVal&" does not match with actual :"&strActual
			End If
			
		Else
			VerifyFieldValueC = 1
			WriteStatusToLogs "Error occurred while opening a Database connection "&Err.Description
		End If
		On Error Goto 0
End Function

Function FileCompare(byval strActualFilePath,byval strExpFilePath,byval strSep)
''msgbox strSep&" "&strActualFilePath&" "&strExpFilePath

	Dim strMethod
	Dim strActualContents,strExpContents
	Dim arrActualContents,arrExpContents
	
	Set objFSO = CreateObject("Scripting.FileSystemObject") ' Create File Object
	
	strMethod = strKeywordForReporting

	'Actual file are exist or not
	If Not objFSO.FileExists(strActualFilePath) Then
		FileCompare=2
	End If
	
	'Expected file are exist or not
	If Not objFSO.FileExists(strExpFilePath) Then
		FileCompare=2
	End If   
	
	Set objReadFile = objFSO.OpenTextFile(strActualFilePath,1)  ' Open actual File and create read file object
	Set objExpReadFile = objFSO.OpenTextFile(strExpFilePath,1) ' Open expected File and create read file object 	
	
	'Read all content
	strActualContents = objReadFile.ReadAll() 
	strExpContents=objExpReadFile.ReadAll()
	
	If Len(strActualContents) = 0 Then
		FileCompare=1
	End If 
	If Len(strExpContents)= 0 Then 
		FileCompare=1
	End If 
	arrActualContents=Split(strActualContents,strSep)
	arrExpContents=Split(strExpContents,strSep)
	
	If MatchEachArrData(arrExpContents,arrActualContents) = true Then 
		FileCompare=0
	else
		FileCompare=1
	End If 
End Function

' -------------------
Function VerifyFieldValue (ByVal strDsnName, ByVal strSQL, ByVal strExpVal)
On Error Resume Next
	Dim strMethod
	Dim ObjextCon, ObjResult, ObjSQL, strActual, objRecordset, strActVal, intcomp
	
set ObjextCon=Createobject("ADODB.Connection") 
Set objRecordset = CreateObject("ADODB.Recordset")

strMethod = strKeywordForReporting

strExpVal = strExpVal &"^"

ObjextCon.Open("DSN="&strDsnName&";")

objRecordset.Open "select KeywordId from keyword where KeywordName  like '%Browser%' and TypeId = 1", ObjextCon, 3, 3
objRecordset.Open strSQL, ObjextCon
while not objRecordset.eof
'Process the current record
		for each field in objRecordset.fields
			strActVal = strActVal&field &"^"			
		next
objRecordset.movenext

Wend

intcomp = instr(1,strActVal, strExpVal,0) 'case sensitive due to last argument which is 0.
			If intcomp > 0 Then
				VerifyFieldValue = 0
				msgbox "Action: "&strMethod&vbtab&_
								"Status: Passed" &Vbtab&_
								"Message: expected value : "&strExpVal&" matches with actual :"&strActVal
			Else
				VerifyFieldValue = 2
				msgbox "Action: "&strMethod&vbtab&_
								"Status: Failed" &Vbtab&_
								"Message: expected value : "&strExpVal&" does not match with actual :"&strActVal
			End If

End Function

' -------------------
Function WriteRestoFile(ByVal strConnectKey, ByVal strSQL, ByVal strFilePath,ByVal strSep)
On Error Resume Next
	Dim strMethod
	Dim ObjextCon, ObjResult, ObjSQL, strActual, objRecordset, strActVal, intcomp
	Dim mynewConnection, rsnew
		
		Set mynewConnection = oDictStorage.Item(strConnectKey)

		Set rsnew = mynewConnection.Execute( strSQL ) 
'set ObjextCon=Createobject("ADODB.Connection") 
'Set objRecordset = CreateObject("ADODB.Recordset")

Dim objDBALog           ' for writing DB res
Set objDBALog = CreateObject("Scripting.FileSystemObject")

strMethod = strKeywordForReporting

If Not objDBALog.Fileexists(strFilePath) Then
		objDBALog.CreateTextFile(strFilePath)
		If Err.Number > 0 Then 
			msgbox "Action: "&strMethod&vbtab&_
								"Status: FAILED" &Vbtab&_
								"Message: expected value : "&strExpVal&" matches with actual :"&strActVal
			WriteRestoFile = 2
		End If 
End If
Set objWriteFile = objDBALog.OpenTextFile(strFilePath,2,true)  

'ObjextCon.Open("DSN="&strDsnName&";")
If Err.Number= -2147467259 Then 	
	msgbox "Action: "&strMethod&vbtab&_
								"Status: FAILED" &Vbtab&_
								"Message: expected value : "&strExpVal&" matches with actual :"&strActVal
	WriteRestoFile = 2
End If 
'objRecordset.Open "select KeywordId from keyword where KeywordName  like '%Browser%' and TypeId = 1", ObjextCon, 3, 3
'objRecordset.Open strSQL, ObjextCon

If Err.Number = -2147217865 Then 
	msgbox "Action: "&strMethod&vbtab&_
								"Status: FAILED" &Vbtab&_
								"Message: expected value : "&strExpVal&" matches with actual :"&strActVal
	WriteRestoFile = 2
End If 
while not rsnew.eof
'Process the current record
		for each field in rsnew.Fields
			'field &"^"
			objWriteFile.Write(field&strSep)		
		next
rsnew.movenext

Wend
objWriteFile.Close()
If Err.Number = 0 Then 
WriteRestoFile=0
Else
WriteRestoFile=1
End If 

End Function


Function ReadWriteFile(byval strReadFilePath,byval strWriteFilePath,byval range,byval strSep)
Dim objFSO
Dim objReadFile
Dim objWriteFile
Dim readFile,writeFile
Dim strLine
Dim i
Dim range1,range2
Dim strValue
Dim strMethod

 readFile=1
 writeFile=2
Set objFSO = CreateObject("Scripting.FileSystemObject")

strMethod = strKeywordForReporting

If Not objFSO.FileExists(strReadFilePath) Then 
ReadWriteFile=1
End If 
Set objReadFile = objFSO.OpenTextFile(strReadFilePath,readFile)

If Not objFSO.FileExists(strWriteFilePath) Then
objFSO.CreateTextFile(strWriteFilePath)
End If
Set objWriteFile = objFSO.OpenTextFile(strWriteFilePath,writeFile)
Do While Not objReadFile.AtEndOfStream
'While objReadFile.AtEndOfStream
	strLine = objReadFile.ReadLine()
	 
	For i=0 to UBound(range)
	range1=range(i)
	i=i+1
	range2=range(i)-range1	
   strValue= mid (strLine,range1,range2)
   objWriteFile.Write(strValue&strSep)
	Next
loop 
If Err.Number = 0 Then 
ReadWriteFile=0
Else 
ReadWriteFile=1
End If 
End Function 


Function StoreSplitString (byval strKey, byval strMainString, byval delimiter, byval index)
On error resume Next
	'Variable Section Begin
	
		Dim strMethod
		Dim intLenMainStr , arr , idx, strItem	 
		
	 'Variable Section End
	
	strMethod = strKeywordForReporting

	If IsKey(strKey) = 0  Then
		
		If IsNull(delimiter) Or IsEmpty(delimiter) Or Trim(delimiter) = "" Then
			StoreSplitString = 1
			WriteStatusToLogs "The delimiter is invalid , please verify."
		Else
		
			If index = "" Then
				index = 1
				WriteStatusToLogs "The index data is empty. Hence its set to the default value '1' which is the first instance"
				
			ElseIf IsInteger(index) = 1 Then
				StoreSplitString = 1
				WriteStatusToLogs "The instance '" & index & "' is not a valid integer, please verify. Index starts at 1."
				Exit Function

			Else
				index = CInt(index)
			End if	  		

			intLenMainStr = len(strMainString)
			If intLenMainStr > 0 Then 'even after the strMainString	param is non-mandatory, still have kept this validation because splitting of zero len ("") 
										'will not give any data resultingl in exception if arr(0) is done
					
						
				arr = split(strMainString ,delimiter) 
				idx = CInt(index)
				If IsNull(arr) Then
						StoreSplitString = 1
						WriteStatusToLogs "Failed to split the string "&strMainString&", please verify.Error:"&Err.description
				Else
						If idx > UBound(arr)+1 Then
							StoreSplitString = 1
							WriteStatusToLogs "Cannot find the index "&index&" item. There are only "&UBound(arr)+1&" items after splitting by "&delimiter&", please Verify."
						Else
							idx = idx-1
							strItem = arr(idx)	
							AddItemToStorageDictionary strKey , strItem 			    
							StoreSplitString = 0
							WriteStatusToLogs "Successfully stored the item "& strItem &" in the key "&strKey
						End if							
				End If
						
					
			Else
				 StoreSplitString = 1
				 WriteStatusToLogs "The String is Empty, please verify"
			End If

		End If

	Else
		 StoreSplitString = 1
		 WriteStatusToLogs "The key is not valid, please verify"
	End If
	
End Function

'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name :				VerifyDataType()
' 	Purpose :					Verifies the data type of the incoming data
' 	Return Value :		 		intres
' 	Arguments :
'	Input Arguments :  			data , dataType
'	Output Arguments : 	  int
'	Function is called by :		
'	Function calls :			   
'	Created on :				  02/02/2010
'	Author :						 Sanam Chugh
'------------------------------------------------------------------------------------------------------------------------------------------
Function VerifyDataType(byval strData, byVal dataType)
	On Error Resume Next
	'Variable Section Begin
		Dim strMethod
		Dim isNum
		Dim strCData
	'Variable Section End

    isNum =False    
	strMethod = strKeywordForReporting
	strCData = CStr(strData)


	If VarType(strCData) <>vbEmpty Then
		If IsNumeric(strCData) then
		   isNum = True
		End If
	Else
		VerifyDataType = 1
		WriteStatusToLogs "Data to be verified is empty.Please verify."
		Exit function
	End If
										 	
	dataType = Trim(dataType)   
	If StrComp(dataType,"numeric",1)=0 Then
			If isNum Then
				VerifyDataType = 0
				WriteStatusToLogs "Data "&strCData& "  is numeric."
			Else
				VerifyDataType = 2
				WriteStatusToLogs "Data "&strCData&"  is not numeric."
			End If


	ElseIf StrComp(dataType,"string",1)=0 Then
			If  isNum Then
				VerifyDataType = 2
				WriteStatusToLogs "Data "&strCData& " is not a string."
					
			else 
				VerifyDataType = 0
				WriteStatusToLogs "Data "&strCData&"  is a string."
			End If
	Else
			VerifyDataType=1
			WriteStatusToLogs "The data type passed is empty. It should be either 'string' or 'numeric'.Please verify"
			
	End If

On Error GoTo 0 
End Function

'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name :				 IsArraySorted()
' 	Purpose :					 Verifies that the data is sorted in either ascending/descending direction
'	Return Value :		 		 Integer(0/1/2)
'   Arguements:	
'		Input Arguments :  		 arrData,dataType,direction
'		Output Arguments :       intRes
'	Function is called by :
'	Function calls :			 
'	Created on :				 28/03/2011
'	Author :					 Iohbha K
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function IsArraySorted(ByVal arrDataActual,  ByVal dataType , ByVal direction)
On Error Resume Next
	'Variable Section Begin

		 Dim strMethod	
		 Dim localDataType, min , max , expectedSortedArrData , actualSortedArrData , intComp , intRes
		 Dim directionString
				
	'Variable Section End
    strMethod = strKeywordForReporting

	If IsNull (dataType) Or IsEmpty(dataType) Or Trim(dataType)="" Then
		IsArraySorted = 1
		WriteStatusToLogs "The datatype is null/invalid, please verify." 
	ElseIf  IsNull (arrDataActual) Or IsEmpty(arrDataActual) Then
		IsArraySorted = 1
		WriteStatusToLogs "The array is null/invalid/empty, please verify." 

	ElseIf IsNull (direction) Then
			IsArraySorted = 1
			WriteStatusToLogs "The direction for sorting is null, please verify." 
	Else	
			expectedSortedArrData = arrDataActual

			If Trim(direction) = "" Then
				intComp = 1
				WriteStatusToLogs "The direction data is empty, hence its set to the default value '1' which is ascending"

			ElseIf Trim(direction) = "2" Then
				intComp = Trim(direction)
				directionString = "Descending"

			ElseIf Trim(direction) = "1" Then
				directionString = "Ascending"
				intComp = "1"
			Else
				IsArraySorted = 1
				WriteStatusToLogs "The direction for sorting is invalid, please verify." 				
				Exit function
			End IF
				
			If Trim(UCase(dataType)) = "STRING" Then
				localDataType =  Trim(UCase(dataType))
			ElseIf Trim(UCase(dataType)) = "INTEGER" Then
				localDataType =  Trim(UCase(dataType))
			Else
				'Set String to be the default datatype
				localDataType =  Trim(UCase(dataType))
			End If
					
			actualSortedArrData = SortArray(arrDataActual , localDataType ,intComp)
			If IsNull(actualSortedArrData) Then
				IsArraySorted = 0
				WriteStatusToLogs 	"Fail to sort the data. An error occurred, please verify."&Err.description
			Else
				intRes = CompareArraysInSequence(expectedSortedArrData, actualSortedArrData)
				If intRes = 0 Then
					IsArraySorted = 0
					WriteStatusToLogs 	"The array is sort in "&directionString&" order."

				Else
					IsArraySorted = 2
					WriteStatusToLogs 	"The array is not sort in "&directionString&" order."

				End If
			End IF
	End If	
	
On Error GoTo 0
End Function

' Function to execute sql query and store the results in a csv file
Function ExecuteDDLQueryAndStoreInFile(ByVal driver, ByVal dB,ByVal userName,ByVal password,ByVal query,ByVal fileName)
On Error Resume Next
	'Variable Section Begin
		Dim connStr
		Dim con, rs
		Dim result
		Dim paraCount, i
		Dim strMethod
	'Variable Section End

	strMethod = strKeywordForReporting	
	
	If IsNull(driver) Or Trim(driver) = "" Or IsNull(dB) or Trim(dB) = "" Or IsNull(query) or Trim(query) = "" Then
		ExecuteDDLQueryAndStoreInFile = 1
		WriteToErrorLog err,"Either the driver/DB/query is invalid, please verify."
		WriteStatusToLogs "Either the driver/DB/query is invalid, please verify."
		Exit Function
	
	ElseIf IsNull(userName) Or Trim(userName) = "" Or IsNull(password) or Trim(password) = "" Or IsNull(fileName) or Trim(fileName) = "" Then
		ExecuteDDLQueryAndStoreInFile = 1
		WriteToErrorLog err,"Either the username/password/filename is invalid, please verify."
		WriteStatusToLogs "Either the username/password/filename is invalid, please verify."
		Exit Function

	Else

		connStr = "DRIVER={"&driver&"};UID="&userName&";PWD="&password&";DBQ="&dB
		'	connStr = "DRIVER={Oracle in OraDb10g_home1};DBQ=DET_Cloud;UID=revera_ab_det;PWD=revera_ab_det"
		'	query = "select * from sales_master where AGENCY_CODE = '0010442'"

		Set con = createobject("adodb.connection")
		Set rs = createobject("adodb.recordset")
		
		con.Open connStr
		''msgbox connStr		

        If Err.Number <> 0 Then
			WriteToErrorLog Err, "Unable to open dB connection."
			WriteStatusToLogs "Error Message:" & Err.Message&Vbtab&_
			       "Error Number:"& Err.Number &Vbtab&_
				   "Unable to open dB connection."
			ExecuteDDLQueryAndStoreInFile = 1
			Exit Function
		End If

		rs.Open query, con
'		'msgbox "query executed"

'		Do while not rs.EOF
'			'msgbox rs.Fields.Item(0)
'			'msgbox rs.Fields.Item(1)
'			'msgbox rs.Fields.Item(2)
'			rs.MoveNext
'		Loop

		If Err.Number <> 0 Then
			WriteToErrorLog Err, "Unable to execute sql query."
			WriteStatusToLogs "Error Message:" & Err.Message&Vbtab&_
			       "Error Number:"& Err.Number &Vbtab&_
				   "Unable to execute sql query."

			ExecuteDDLQueryAndStoreInFile = 1
			Exit Function
		End If

		Call writeCSV(rs, fileName)
		
		rs.Close
		con.Close

        If Err.Number <> 0 Then
			WriteToErrorLog Err, "Unable to close dB connection."
			WriteStatusToLogs "Error Message:" & Err.Message&Vbtab&_
			       "Error Number:"& Err.Number &Vbtab&_
				   "Unable to close dB connection."
			ExecuteDDLQueryAndStoreInFile = 1
			Exit Function
		End If

	End If 
	
	ExecuteDDLQueryAndStoreInFile = 0

End Function

' Function to compare two csv files and display a report in an excel format
Function CompareTabularResults(ByVal File1, ByVal File2,byval format)

	On Error Resume Next
	Dim ArrayLength
	Dim str, status, msg
	Dim dictStatus
	Dim arr1, arr2
	Dim fs
	Dim row1, col1, row2, col2
	Dim strMethod

	Set fs = CreateObject("Scripting.FileSystemObject")

	strMethod = strKeywordForReporting

	If Trim(format) = "" Then
		format = "MSExcel"
		WriteStatusToLogs "The format is empty. Hence its set to the default value 'MSExcel'"
	End iF

	If IsNull(File1) Or Trim(File1) = "" or IsNull(File2) or Trim(File2) = "" Then
		WriteToErrorLog err,"Message : The firstfile and/or secondfile given are invalid, please verify."
		WriteStatusToLogs "The firstfile and/or secondfile given are invalid, please verify."
		ExecuteDDLQueryAndStoreInFile = 1
		Exit Function
	End if

	'Exit if the file format is not csv
	If strcomp(Lcase(fs.GetExtensionName(File1)), "csv",1) <> 0 or strcomp(Lcase(fs.GetExtensionName(File2)), "csv",1) <> 0  Then
		WriteToErrorLog err,"Message : This file type is not supported. Please specify .csv files."
		WriteStatusToLogs "This file type is not supported. Please specify .csv files."
		CompareTabularResults = 1
		Exit Function
	End if
	
	'Check if file exists or not
	If (Not fs.FileExists(File1)) And (Not fs.FileExists(File2)) Then
		
		WriteStatusToLogs "Please check, either Source or Target File does not exist or the file paths might be incorrect" 
		CompareDBResult=1
		Exit Function
	Else

		arr1 = ConvertCSVTo2DArray(File1, row1, col1)
		arr2 = ConvertCSVTo2DArray(File2, row2, col2)
		Set dictStatus = CompareTabularData(arr1, arr2, "ExactMatch", "true", true , format)

	End If

'	If (Not (dictStatus Is Nothing)) 'And IsObject(dicStatus)
		status = dictStatus.item("status")
		msg = dictStatus.item("message")
		'''msgbox "Status="&status& "msg="&msg
		If status = 0 Then 
			WriteStatusToLogs msg 
						CompareTabularResults=0

		Else If status = 1 then
			WriteStatusToLogs msg 
						 CompareTabularResults=1
		Else
			WriteStatusToLogs msg 
						 CompareTabularResults=2
		End If

'	Else 
'		WriteStatusToLogs "Action: CompareTabularResults "&vbtab&_
'						  "Status:" &  "Failed" &Vbtab&_
'						 "Message:" & "Error in comparing file data" 
	End If

On Error Goto 0

End Function
'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name :				 ExecuteDMLQueryAndStoreInKey()
' 	Purpose :					 Executes a DML query and stores the status of the execution of query in a variable.
'	Return Value :		 		 Integer(0/1)
'   Arguements:	
'		Input Arguments :  		 driver,url,userName,password,query,key
'		Output Arguments :       intStatus
'	Function is called by :
'	Function calls :			 
'	Created on :				 7/12/2011
'	Author :					 Parvin M
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function ExecuteDMLQueryAndStoreInKey(ByVal driver, ByVal url,ByVal userName,ByVal password,ByVal query,ByVal key)
On Error Resume Next
	Dim connStr
	Dim con
	Dim intval
	Dim strMethod 

	strMethod = strKeywordForReporting
	If IsNull(driver) Or IsEmpty(driver ) Or  Trim(driver)="" Or IsNull(query) Or IsEmpty(query ) Or Trim(query)="" Then
		ExecuteDMLQueryAndStoreInKey = 1
		WriteStatusToLogs "The driver or the query sent is invalid, please verify."
		Exit Function
	End If
	
	If IsNull(userName) Or IsEmpty(userName ) Or Trim(userName)="" Or IsNull(password) Or IsEmpty(password ) Or Trim(password)="" Then
		ExecuteDMLQueryAndStoreInKey = 1
		WriteStatusToLogs "The UserName or the password sent is invalid, please verify."
		Exit Function
	End If
	
	If IsNull(url) Or IsEmpty(url ) Or Trim(url)="" Then
		ExecuteDMLQueryAndStoreInKey = 1
		WriteStatusToLogs "The url is invalid, please verify."
		Exit Function
	End If

	If IsKey(key) <> 0 Then
		 ExecuteDMLQueryAndStoreInKey = 1
		 WriteStatusToLogs "The key  '"& key &"'  is invalid.Please verify." 
		 Exit Function
	End If
	
	connStr="DRIVER={" & driver &"};"&url&";UID="&userName &";PWD="& password 
	Set con = createobject("adodb.Connection")
	
	con.Open connStr
	If Err.Number <> 0 Then
		ExecuteDMLQueryAndStoreInKey = 1
		WriteStatusToLogs "Failed to connect to the database with connection string ="&connStr&" , please verify. Error description:"&Err.description 
		Exit Function
	End If
	
	con.Execute(query)
	
	If  Err.Number=0 Then
		intval = AddItemToStorageDictionary(key, 0)
		ExecuteDMLQueryAndStoreInKey = 0
		WriteStatusToLogs "The provided query '"& query &"'  has been executed successfully. Status 0 is stored in the key"
	Else 
		intval = AddItemToStorageDictionary(key, 1)
		 ExecuteDMLQueryAndStoreInKey = 1
		WriteStatusToLogs "An error occurred while executing the query '"&query&"'. Reason may be the provided query is incorrect or database does not exist. Error Number:"&Err.number&". Status 1 is stored in the key."
	   Err.description
	End If
	  	
	con.Close
	
On Error goto 0
End Function


Function ExecuteBatchFile(byval strBatchFilePath)
On Error Resume Next
		'Variable Section Begin
			Dim strMethod, oShell

		'Variable Section Ends

		strMethod = strKeywordForReporting
		If IsNull(strBatchFilePath) Or IsEmpty(strBatchFilePath) Or Trim(strBatchFilePath) = "" Then
				ExecuteBatchFile = 1
				WriteStatusToLogs "The file path is either null or empty, please verify."

		Else
				Set oShell = CreateObject("WSCript.shell") 
				oShell.Run """" &strBatchFilePath& """",1
				If Err.number = 0 Then
						ExecuteBatchFile = 0
						WriteStatusToLogs "The file '"& strBatchFilePath &"' is executed ." 
				Else
				
						ExecuteBatchFile = 1
						WriteStatusToLogs "An error occurred while executing the file '"& strBatchFilePath &"' .Error:"&Err.description&" , please verify." 					 
				End if
		End if
On Error Goto 0
End Function


'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name :				 TrimText()  
' 	Purpose :					 Trims new lines and spaces from both ends of a string and stores the resulting string in dictionary against strkey.
'	Return Value :		 		 Integer(0/1)
'   Arguements:	
'		Input Arguments :  		 strkey,strExp
'		Output Arguments :       intStatus
'	Function is called by :
'	Function calls :			 
'	Created on :				 7/12/2011
'	Author :					 Rajneesh Kumar
Function TrimText(ByVal strText, ByVal strKey)
		On error resume next
		
		dim strMethod
		
		strMethod = strKeywordForReporting
		
		If TypeName(strText) <> "String" then
			WriteStatusToLogs "Invalid text."
				TrimText = 1
				on error goto 0
				Exit Function
		End If
		
		If isEmpty(strKey) or isNull(strKey) or strKey = "" then
				WriteStatusToLogs "Invalid storage key."
				TrimText = 1
				on error goto 0
				Exit Function
		End If
		
		err.number = 0
		strText = trim(strText)
		
		AddItemToStorageDictionary strKey, strText
				
		If err.number = 0 then
			WriteStatusToLogs "Text is trimed successfully to '"& strText & "'" & " and stored in the key '"& strKey & "'"
				TrimText = 0	
		Else
			WriteStatusToLogs "Error occurred while triming text." 
				TrimText = 1		
		End If

		on error goto 0

End Function

'---------------------------------------------------------------------


Function Q_Replace(ByVal strMainString, ByVal strToBeReplaced,ByVal strToBeReplacedBy,ByVal blnCaseSensitive, ByVal strKey)
		On error resume next
		dim strMethod, MainString
		
		strMethod = strKeywordForReporting
		
		If isEmpty(strKey) or isNull(strKey) or strKey = "" then
				WriteStatusToLogs "Invalid storage key."
				Q_Replace = 1
				on error goto 0
				Exit Function
		End If
		
		If isEmpty(strToBeReplaced) or isNull(strToBeReplaced)then
				WriteStatusToLogs "Invalid parameter."
				Q_Replace = 1
				on error goto 0
				Exit Function
		End If
		
		err.number = 0
		If StrComp (blnCaseSensitive, "False", 1) = 0 then
			MainString = replace(strMainString,strToBeReplaced, strToBeReplacedBy, 1, -1, vbTextCompare)
		Elseif StrComp (blnCaseSensitive, "True", 1) = 0 then
			MainString = replace(strMainString,strToBeReplaced, strToBeReplacedBy, 1, -1)
		Else
			WriteStatusToLogs "Invalid parameter IsCaseSensitive."
				Q_Replace = 1
				on error goto 0
				Exit Function
		End If

		AddItemToStorageDictionary strKey, MainString

		If err.number = 0 then

			WriteStatusToLogs "Text is replaced successfully to '" & MainString & "'" &" and stored in the key '"& strKey & "'"
				Q_Replace = 0
				
		Else
			WriteStatusToLogs "Error occurred while replacing." 
				Q_Replace = 1		
		End If

		on error goto 0

End Function

'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name :				 StoreInstanceCount()
' 	Purpose :					 Gets the total number of existing instances of a given object description and stores it in dictionary against a given key.
'	Return Value :		 		 Integer
'   Arguements:	 key
'		Input Arguments :  		 objObject,strKey
'		Output Arguments :       intStatus
'	Function is called by :
'	Function calls :			 
'	Created on :				 7/12/2011
'	Author :					 Rajneesh Kumar

Function StoreInstanceCount(byVal strObject, byVal strKey)
	On error resume next
	
	Dim desc1, desc2, desc, i, objDesc, c, strMethod, instCount, j, val
	
	strMethod = strKeywordForReporting
		 
	If isNull(strKey) or isempty(strKey) or isNull(strObject) or isempty(strObject) Then 
		WriteStatusToLogs "parameters are either null or empty." 
		StoreInstanceCount= 1
		Exit Function
	End if
	
	desc = strObject
	desc1 = split (desc , ",")
	Set objDesc = Description.Create
	For i=0 to UBound(desc1)
		If (Replace(desc1(i), cstr(chr(34)),"")  = "") Then
				StoreInstanceCount= 1
			    WriteStatusToLogs "Wrong or invalid arguments: " & desc
				Exit Function
		End If
		desc2 = split(desc1(i), "=")

		If UBound(desc2) <> 1 or desc2(0)="" or desc2(1)="" Then
			StoreInstanceCount= 1
			WriteStatusToLogs "Wrong or invalid arguments: " & desc
				Exit Function

			Execute	"objDesc(" & DoubleQuotes(desc2(0))&").Value = " & val
		Else
			Execute	"objDesc(" & DoubleQuotes(desc2(0))&").Value = " & DoubleQuotes(desc2(1))
		End If
		
	Next
	
	
	
	err.number = 0
	c =  Desktop.ChildObjects(objDesc).Count 
	
	
	AddItemToStorageDictionary strKey, c
	instCount = c
	
	If instCount <= 0 then 
		instCount = "no instance found"
	End if
	
	If err.number = 0 Then
		StoreInstanceCount= 0
		WriteStatusToLogs "Total instance count: " & instCount
	Else 
		StoreInstanceCount= 1
		WriteStatusToLogs "Failed to get the instance count. "
	End If
	on error goto 0
End Function



'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name :				 SetObjectProperty()
' 	Purpose :					 Sets the specified property/value to obj.
'	Return Value :		 		 Integer
'   Arguements:	 key
'		Input Arguments :  		 objObject,strKey
'		Output Arguments :       intStatus
'	Function is called by :
'	Function calls :			 
'	Created on :				 7/12/2011
'	Author :					 Rajneesh Kumar

Function SetObjectProperty(obj, prop, val)
	On error resume next

	Dim strMethod, MainString
	strMethod = strKeywordForReporting		
	
	If not (prop = "" or isnull(prop) or isobject(obj)) Then
				
			WriteStatusToLogs "Invalid parameters."
				SetObjectProperty = 1
				on error goto 0
				Exit Function
	End If
	
	If err.number = 0 Then
		obj.settoproperty prop, val
		If err.number = 0 Then
			WriteStatusToLogs "Object property is successfully modified. '" &prop& "' is set to '" & val & "'"
				SetObjectProperty = 0
		Else
			SetObjectProperty= 1
			WriteStatusToLogs "Error occured. "& oDictErrorMessages.Item(cstr(err.number))
		End If
	End If
	On error goto 0
End Function


Function OpenURL(BrowserType, Url, BrowserVersion, Path)
	On error resume next
	Dim i, brwsrtype,  ChromeExecutablePath
	Dim oPages, oDesc, creationTime
	Dim fso , strMethod 
	strMethod = strKeywordForReporting	
	
	' Set fso = CreateObject("Scripting.FileSystemObject")
	
	' Set RepositoryDyn = CreateObject("Mercury.ObjectRepositoryUtil") 
	' Set oDesc = Description.Create
	' oDesc("micClass").Value="Browser"	
	' RepositoryDyn.Load ORpath

	If instr(1,BrowserType, "ie", 1) > 0 Then
		brwsrtype = "internet explorer "
		StoreVariable "$$_ISIE","True"
		SystemUtil.Run "iexplore.exe" , url ,,,3
			
	ElseIf instr(1,BrowserType, "firefox", 1) > 0 Then
		
		brwsrtype = "Mozilla Firefox "
		StoreVariable "$$_ISFIREFOX","True"
		SystemUtil.Run "firefox.exe" , url ,,, 3
	ElseIf instr(1,BrowserType, "chrome", 1) > 0 Then
		ChromeExecutablePath = Path
		If not fso.FileExists(ChromeExecutablePath) Then
			OpenURL = 1
			WriteStatusToLogs "Invalid path."
			Exit function
		End If
		brwsrtype = "Chrome "
		StoreVariable "$$_ISCHROME","True"
		
		SystemUtil.Run ChromeExecutablePath , url ,,,3
	Else
				OpenURL = 1
			WriteStatusToLogs "Unknown browser type."
				 Exit function
	End If 
	
	' oDesc("application version").Value= brwsrtype & ".*"
	' Set oPages = Desktop.ChildObjects(oDesc)
	' creationTime = oPages.Count-1
	
	' Set oBrowser = RepositoryDyn.GetAllObjectsByClass("Browser")
	' For i =0 to  oBrowser.Count()-1
		' If not (BrowserVersion = "") Then
			' oBrowser.Item(i).settoproperty "version", brwsrtype & BrowserVersion
			' oBrowser.Item(i).settoproperty "application version", brwsrtype & BrowserVersion
		' Else 
			' oBrowser.Item(i).settoproperty "application version", brwsrtype&".*"
		' End If
		
		' oBrowser.Item(i).settoproperty "CreationTime", creationTime
	' Next
	
	If err.number = 0 Then
		OpenURL = 0
			WriteStatusToLogs "Browser navigated successfully."
	Else
			OpenURL = 1
			WriteStatusToLogs "Browser not navigated. Please check parameters."
	End If
	On error goto 0
End Function

Function ReadCelldataFromExcelFile_OLD(fileName,sheetNum, rowNum, colNum, strkey) 
	'Dim objExcel, objWorkbook
	''msgbox "In"
	On Error Resume Next
	
	Dim objExcel, objWorkbook, strMethod, objsheet, strValue, intval
	
	If IsKey(strkey) <> 0 Then
		ReadCelldataFromExcelFile = 1
		WriteStatusToLogs "The key passed is either null/empty , please verify."
	Else
	
		strMethod = strKeywordForReporting
		
		
		Set objExcel = CreateObject("Excel.Application")   
		Set objWorkbook = objExcel.Workbooks.Open(fileName)
		
		If err.number > 0 Then
			ReadCelldataFromExcelFile = 1
		    WriteStatusToLogs err.Description
		Else
			objExcel.visible=false		
			'set objsheet=objExcel.ActiveWorkbook.Sheets(CLng(sheetNum))
			set objsheet = objExcel.Sheets.Item(sheetNum)
			If err.number > 0 Then
				ReadCelldataFromExcelFile = 1
		    	WriteStatusToLogs err.Description
			Else
				 'strValue=objsheet.Cells(rowNum,colNum).value
				 strValue=CStr(objsheet.Cells( CLng(rowNum), CLng(colNum)).value)
				
				''msgbox "strValue="&strValue
				intval = AddItemToStorageDictionary(strKey,strValue)
				If  intval = 0 Then
					ReadCelldataFromExcelFile = 0
					WriteStatusToLogs "The cell value '"&strValue&"' of row number '"& rowNum &"' and of column '"& colNum &"'  is stored successfully in the Key '"&strKey&"'"
				Else
					ReadCelldataFromExcelFile = 1
					WriteStatusToLogs "The cell value '"&strValue&"' of row number '"& rowNum &"' and of column '"& colNum &"'  is NOT stored successfully in the Key '"&strKey&"'" 
					  
				End If
				
				objExcel.ActiveWorkbook.Close
				objExcel.Workbooks.Close
				objExcel.Application.Quit
			End If			
		End If
	End If
End Function


Function StoreNodeValue(byval xmlFilePath,byval xPath, byVal strKey)	
	
	If IsKey(strKey) <> 0 Then
		StoreNodeValue = 1
		WriteStatusToLogs "The key passed is either null/empty , please verify."
	Else		
		Dim xmlDoc, strMethod, xNode,nodeValue,intval
		
		strMethod = strKeywordForReporting
		Set xmlDoc = CreateObject("Microsoft.XMLDOM")
		
		xmlDoc.Async = "False"
		xmlDoc.Load(xmlFilePath)
		If err.Number>0 Then			
			StoreNodeValue = 1
		    WriteStatusToLogs err.Description
		Else			
			set xNode=xmlDoc.selectSingleNode(xPath)			
			nodeValue=xNode.text
			
			If err.number>0 Then				
				StoreNodeValue = 1
			    WriteStatusToLogs "Xpath might be wrong."&err.description
			Else
				intval = AddItemToStorageDictionary(strKey,nodeValue)				
				If  intval = 0 Then
					StoreNodeValue = 0
					WriteStatusToLogs "The node value '"&nodeValue&"' is stored successfully in the key '"&strKey&"'."
				Else					
					StoreNodeValue = 1
					WriteStatusToLogs "The node value '"&nodeValue&"' doesn't stored successfully in the key '"&strKey&"'."
				End If
			End If
		End If
	End if
End Function

Function SetNodeValue(byval xmlFilePath,byval xPath, byVal strValue)
	
		Dim xmlDoc, strMethod, xNode,nodeValue
		
		strMethod = strKeywordForReporting
		Set xmlDoc = CreateObject("Microsoft.XMLDOM")
		
		xmlDoc.Async = "False"
		xmlDoc.Load(xmlFilePath)
		If err.Number>0 Then			
			SetNodeValue = 1
		    WriteStatusToLogs err.Description
		Else			
			set xNode=xmlDoc.selectSingleNode(xPath)			
			xNode.text=strValue
			
			If err.number>0 Then				
				SetNodeValue = 1
			    WriteStatusToLogs "Xpath might be wrong."&err.description
			Else
				nodeValue=xNode.text				
				xmlDoc.Save xmlFilePath
				If err.number > 0 Then
					SetNodeValue = 1
				    WriteStatusToLogs err.description
				Else 
					If  StrComp(nodeValue, strValue) = 0 Then
						SetNodeValue = 0
						WriteStatusToLogs "The node value '"&strValue&"' is set successfully."
					Else					
						SetNodeValue = 1
						WriteStatusToLogs "The node value '"&strValue&"' doesn't set."
					End If
				End If
				
			End If
		End If
End Function

Function ExecuteRestRequestAndStoreResponseInJson(ByVal URL, ByVal method, ByVal authType,ByVal userName,ByVal pwd,ByVal reqFilePath, ByVal resFilePath)
	On Error Resume Next
		Dim objFileToRead,strFileText, objXmlHttpMain, str,auth, strMethod
		
		strMethod=strKeywordForReporting
	
		If trim(URL) = "" or trim(method) = "" Then
			ExecuteRestRequestAndStoreResponseInJson= 1
					WriteStatusToLogs "One or more parameters are empty."
			Exit function
		End if
		
	
		Set objXmlHttpMain = CreateObject("Msxml2.ServerXMLHTTP") 
		
		objXmlHttpMain.open method,URL, False
		
		if (UCase(authType)="BASIC") then
			auth= Base64Encode(userName&":"&pwd)
			objXmlHttpMain.setRequestHeader "Authorization", "Basic "&auth	
		End if
		
		objXmlHttpMain.setRequestHeader "Content-Type", "application/json"
		
		If reqFilePath <> "" Then
			Set objFileToRead = CreateObject("Scripting.FileSystemObject").OpenTextFile(reqFilePath,1)
			strFileText = objFileToRead.ReadAll()
			objFileToRead.Close
			Set objFileToRead = Nothing
			objXmlHttpMain.send strFileText
		else
			objXmlHttpMain.send
		End If		
		
		If err.number>0 Then
			ExecuteRestRequestAndStoreResponseInJson= 1
					WriteStatusToLogs err.description
			Exit function
		End If
		
		str = objXmlHttpMain.responseText
		
		If str <> "" Then
			Dim FSO,resFile
			Set FSO = CreateObject("Scripting.FileSystemObject")
			set resFile= FSO.CreateTextFile(resFilePath,true)
			If err.number>0 Then
				ExecuteRestRequestAndStoreResponseInJson= 1
						WriteStatusToLogs err.description
				Exit function
			End If
			resFile.Write(str)
			resFile.Close()			
			if err.number>0 then
				ExecuteRestRequestAndStoreResponseInJson= 1
				WriteStatusToLogs err.description
			else				
				
				ExecuteRestRequestAndStoreResponseInJson= 0
				WriteStatusToLogs "Rest request executed successfully."
			End if
		Else 
		
				ExecuteRestRequestAndStoreResponseInJson= 0
				WriteStatusToLogs "Rest request executed successfully."
		End If
		
	On Error GoTo 0
End Function

Function ExecuteSoapRequest(ByVal url, ByVal requestType, ByVal reqXmlPath,ByVal methodName, ByVal resXmlPath,byVal authType,byval userName,byval password)
	On Error Resume Next		
	Dim objQualitiaCore, strMethod
	
	strMethod = strKeywordForReporting
	Set objQualitiaCore = CreateObject("QualitiaCore.JsonHelper")	
	if err.number > 0  then		
		ExecuteSoapRequest = 1
			WriteStatusToLogs err.Description
		Exit function
	End If
	objQualitiaCore.ExecuteSoapService url,requestType,reqXmlPath,methodName, resXmlPath,authType,userName,password
	
	'msgbox err.description
	
	
	if err.description <> "" then		
		ExecuteSoapRequest = 1
			WriteStatusToLogs err.Description
		Exit function
	End If
	
	ExecuteSoapRequest = 0
	WriteStatusToLogs "The soap request executed successfully."
	on error goto 0
End Function



Function Base64Encode(sText)
    Dim oXML, oNode
    Set oXML = CreateObject("Msxml2.DOMDocument.3.0")
    Set oNode = oXML.CreateElement("base64")
    oNode.dataType = "bin.base64"
    oNode.nodeTypedValue = Stream_StringToBinary(sText)
    Base64Encode = oNode.text
    Set oNode = Nothing
    Set oXML = Nothing
End Function

Private Function Stream_StringToBinary(Text)
  Const adTypeText = 2
  Const adTypeBinary = 1
  Dim BinaryStream 'As New Stream
  Set BinaryStream = CreateObject("ADODB.Stream")
  BinaryStream.Type = adTypeText
  BinaryStream.CharSet = "us-ascii"
  BinaryStream.Open
  BinaryStream.WriteText Text
  BinaryStream.Position = 0
  BinaryStream.Type = adTypeBinary
  BinaryStream.Position = 0
  Stream_StringToBinary = BinaryStream.Read
  Set BinaryStream = Nothing
End Function

Function StoreValueFromJsonFile(ByVal jsonFilePath,ByVal jpath,ByVal elementIndx,ByVal attributeName, ByVal strKey)
	On Error Resume Next
	Dim objQualitiaCore, strValue, intval, strMethod, intIdx
	
	strMethod = strKeywordForReporting
	If IsKey(strKey) <> 0 Then
		StoreValueFromJsonFile = 1
		WriteStatusToLogs "The key passed is either null/empty , please verify."
	Else
		Set objQualitiaCore = CreateObject("QualitiaCore.JsonHelper")
		if err.number > 0  then		
			StoreValueFromJsonFile = 1
				WriteStatusToLogs err.Description
			Exit function
		End If
		intIdx=Cint(elementIndx)-1
		strValue=objQualitiaCore.GetValueFromJson(jsonFilePath,intIdx,attributeName,jpath)
		
		''msgbox strValue
		if err.description <> "" then
		'If err.number>0 Then
			StoreValueFromJsonFile = 1
			    WriteStatusToLogs err.Description
			Exit function
		End If
		
		intval = AddItemToStorageDictionary(strKey,strValue)
		If  intval = 0 Then
			StoreValueFromJsonFile = 0
			WriteStatusToLogs "The json value '"&strValue&"' is stored successfully in the key '"&strKey&"'."
		Else					
			StoreValueFromJsonFile = 1
			WriteStatusToLogs "The json value '"&strValue&"' doesn't stored successfully in the key '"&strKey&"'."
		End If
	End if 
	on error goto 0
End Function

Function SetValueInJson(ByVal jsonFilePath,ByVal indx,ByVal attributeName,ByVal strValue, ByVal jsonPath)
	On Error Resume Next	
	
	Dim objQualitiaCore, strMethod, intIdx
	
	strMethod = strKeywordForReporting
	Set objQualitiaCore = CreateObject("QualitiaCore.JsonHelper")
	
	if err.number > 0  then		
		SetValueInJson = 1
			WriteStatusToLogs err.Description
		Exit function
	End If
	
	intIdx=Cint(indx)-1
	
	objQualitiaCore.SetValueFromJson jsonFilePath,intIdx,attributeName,strValue,jsonPath
	
	if err.description <> "" then		
		SetValueInJson = 1
			WriteStatusToLogs err.Description
		Exit function
	End If
	
	SetValueInJson = 0
	WriteStatusToLogs "The value '"&strValue&"' is updated successfully in json file '"&jsonFilePath&"'."		
	on error goto 0
End Function

Function StoreRandomString(ByVal K,ByVal l)
	On Error Resume Next	
	
	Dim objQualitiaCore, strMethod, intIdx, retVal, intval
	
	strMethod = strKeywordForReporting
	Set objQualitiaCore = CreateObject("QualitiaCore.JsonHelper")
	
	if err.number > 0  then		
		StoreRandomString = 1
			WriteStatusToLogs err.Description
		Exit function
	End If
	
	If IsNull(k) Or IsEmpty(k ) or k = "" Then
		StoreRandomString = 1
		WriteStatusToLogs "key is null or empty."
		Exit Function
	End If

	If not (IsNumeric(l) and cint(l)>0 ) Then       
		StoreRandomString = 1
		WriteStatusToLogs "Length is invalid. Please enter a numeric value."
		Exit Function
	End If
	
	retVal = objQualitiaCore.getRandomString(l)
	intval = AddItemToStorageDictionary(K, retVal)
	if err.description <> "" then		
		StoreRandomString = 1
			WriteStatusToLogs err.Description
		Exit function
	End If
	
	StoreRandomString = 0
	WriteStatusToLogs "Key '"&K&"' is stored with: '"&retVal&"'."		
	on error goto 0
End Function

Function StoreRandomAlphanumericString(ByVal K,ByVal l)
	On Error Resume Next	
	
	Dim objQualitiaCore, strMethod, intIdx, retVal, intval
	
	strMethod = strKeywordForReporting
	Set objQualitiaCore = CreateObject("QualitiaCore.JsonHelper")
	
	if err.number > 0  then		
		StoreRandomAlphanumericString = 1
			WriteStatusToLogs err.Description
		Exit function
	End If
	
	If IsNull(k) Or IsEmpty(k ) or k = "" Then
		StoreRandomAlphanumericString = 1
		WriteStatusToLogs "key is null or empty."
		Exit Function
	End If

	If not (IsNumeric(l) and cint(l)>0 ) Then       
		StoreRandomAlphanumericString = 1
		WriteStatusToLogs "Length is invalid. Please enter a numeric value."
		Exit Function
	End If
	
	retVal = objQualitiaCore.getRandomAlphanumericString(l)
	intval = AddItemToStorageDictionary(K, retVal)
	if err.number <> 0 then		
		StoreRandomAlphanumericString = 1
			WriteStatusToLogs err.Description
		Exit function
	End If
	
	StoreRandomAlphanumericString = 0
	WriteStatusToLogs "Key '"&K&"' is stored with: '"&retVal&"'."		
	on error goto 0
End Function
'----------------------DB Actions -------------------------------------
Function DBStoreDDLQueryExecutionWithSingleData(ByVal DriverString, ByVal query,ByVal KeyString)
	On Error Resume Next
	'Variable Section Begin
		Dim connStr
		Dim con, rs
		Dim result
		Dim paraCount, i
		Dim strMethod
		Dim RowCount 
		RowCount = 0
	'Variable Section End

	strMethod = strKeywordForReporting	
	If IsNull(DriverString) Or Trim(DriverString) = "" Or IsNull(query) or Trim(query) = "" Then
		DBStoreDDLQueryExecutionWithSingleData = 1
		WriteToErrorLog err,"Message : One of the parameters are null or empty."
		WriteStatusToLogs 	"One of the parameters are null or empty."
		Exit Function
	Else

		connStr =DriverString

		Set con = createobject("adodb.connection")
		Set rs = createobject("adodb.recordset")
		
		on error resume next
		con.Open connStr  'WORKING

        If Err.Number <> 0 Then
			WriteToErrorLog Err, "Unable to open dB connection."
			WriteStatusToLogs "Unable to open dB connection. Invalid connection string."
			DBStoreDDLQueryExecutionWithSingleData = 1
			Exit Function
		End If

		rs.Open query, con

		If Err.Number <> 0 Then
			WriteToErrorLog Err, "Unable to execute sql query."
			WriteStatusToLogs "Unable to execute sql query."

			DBStoreDDLQueryExecutionWithSingleData = 1
			Exit Function
		End If

		rs.BOF = true
		Do while not rs.EOF
			RowCount = RowCount+1
			rs.MoveNext
			if RowCount >1 then 
					DBStoreDDLQueryExecutionWithSingleData = 1
					WriteStatusToLogs "Execution has more than one records."
					exit function
			End If
		Loop
		
		rs.MoveFirst
		if rs.fields.count =1 Then
					DBStoreDDLQueryExecutionWithSingleData = 0
					intval = AddItemToStorageDictionary(KeyString, rs.Fields(0).Value)
					'WriteStatusToLogs "Passed Message:" & "Key is stored with data: "&rs.Fields(0).Value
					WriteStatusToLogs "Key is stored with data: "&rs.Fields(0).Value
				Exit function
		Elseif rs.fields.count >1 then
		            DBStoreDDLQueryExecutionWithSingleData = 1
					WriteStatusToLogs "Execution has more than one fields."
		Elseif rs.fields.count <1 then
				      DBStoreDDLQueryExecutionWithSingleData = 1
					WriteStatusToLogs "Execution has less than one records."
	
		End If
		

        If Err.Number <> 0 Then
			WriteToErrorLog Err, "Unable to close dB connection."
			WriteStatusToLogs "Unable to close dB connection."
			DBStoreDDLQueryExecutionWithSingleData = 1
			Exit Function
		End If

	End If 
	
	On error goto 0
End Function

Function DBExecuteDMLQueryAndStoreInKey(ByVal DriverString, ByVal query,ByVal key)
	On Error Resume Next
	Dim connStr
	Dim con
	Dim intval
	Dim strMethod 
	
	strMethod = strKeywordForReporting
	If IsNull(DriverString) Or IsEmpty(DriverString ) Or  Trim(DriverString)="" Or IsNull(DriverString) Or IsEmpty(query ) Or Trim(query)="" Then
		DBExecuteDMLQueryAndStoreInKey = 1
		WriteStatusToLogs "One of the parameters are empty."
		Exit Function
	End If
	
	If IsKey(key) <> 0 Then
		 DBExecuteDMLQueryAndStoreInKey = 1
		 WriteStatusToLogs "The key  '"& key &"'  is invalid.Please verify." 
		 Exit Function
	End If
	
	connStr=DriverString
	'connStr=  "DRIVER={"&driver&"};Database="&url&";UID="&userName &";PWD="& password 
	'connStr = "DRIVER={"&driver&"};UID="&userName&";PWD="&password&";Database="&dB
	'connStr = "DRIVER={"&driver&"};UID="&userName&";PWD="&password&";Database="&dB 
	
	Set con = createobject("adodb.Connection")
	
	con.Open connStr
	If Err.Number <> 0 Then
		DBExecuteDMLQueryAndStoreInKey = 1
		WriteStatusToLogs "Failed to connect to the database."
		Exit Function
	End If
	
	con.Execute(query)
	
	
	If  Err.Number=0 Then
		intval = AddItemToStorageDictionary(key, 0)
		DBExecuteDMLQueryAndStoreInKey = 0
		WriteStatusToLogs "The provided query '"& query &"'  has been executed successfully. Status 0 is stored in the key"
	Else 
		intval = AddItemToStorageDictionary(key, 1)
		 DBExecuteDMLQueryAndStoreInKey = 1
		WriteStatusToLogs "An error occurred while executing the query. Either query is incorrect or duplicate entry . Status 1 is stored in the key."
	   Err.description
	End If
	  	
	con.Close
	
	On Error goto 0
End Function

Function DBExecuteDDLQueryAndStoreInFile(ByVal DriverString, ByVal Query,ByVal FileName)
	On Error Resume Next
	'Variable Section Begin
		Dim connStr
		Dim con, rs
		Dim result
		Dim paraCount, i
		Dim strMethod
	'Variable Section End

	strMethod = strKeywordForReporting	
	If IsNull(DriverString) Or Trim(DriverString) = "" Or IsNull(Query) or Trim(Query) = "" Or IsNull(query) or Trim(query) = "" Or IsNull(FileName) or Trim(FileName) = "" Then
		DBExecuteDDLQueryAndStoreInFile = 1
		WriteToErrorLog err,"Message : One of the inputs are null or empty."
		WriteStatusToLogs 	"One of the inputs are null or empty."
		Exit Function
	Else

		connStr = DriverString   '>>WORKING

		Set con = createobject("adodb.connection")
		Set rs = createobject("adodb.recordset")
		
		con.Open connStr  'WORKING

        If Err.Number <> 0 Then
			WriteToErrorLog Err, "Unable to open dB connection."
			WriteStatusToLogs "Unable to open dB connection."
			DBExecuteDDLQueryAndStoreInFile = 1
			Exit Function
		End If

		rs.Open query, con

		If Err.Number <> 0 Then
			WriteToErrorLog Err, "Unable to execute sql query."
			WriteStatusToLogs "Unable to execute sql query."

			DBExecuteDDLQueryAndStoreInFile = 1
			Exit Function
		End If

		Call writeCSV(rs, fileName)
		
		rs.Close
		con.Close

        If Err.Number <> 0 Then
			WriteToErrorLog Err, err.description
			WriteStatusToLogs "Invalid filepath." 
			DBExecuteDDLQueryAndStoreInFile = 1
			Exit Function
		Else
			
			WriteStatusToLogs "Action executed and recordset data stored in file- "&FileName
			DBExecuteDDLQueryAndStoreInFile = 0
		End If

	End If 

End Function

Function CompareTwoImages( byval file1 , byval file2)
	On error resume next
	Dim objMercuryFilecompare, strMethod
	
	strMethod = strKeywordForReporting
	
	If not FileExist(file1) Then
			CompareTwoImages =1
		 	 WriteStatusToLogs "Invalid file path " &file1&"'"
			Exit function
	End If
	
	If not FileExist(file2)  Then
		CompareTwoImages = 1
				 	 WriteStatusToLogs "Invalid file path " &file2&"'"
						 Exit function
	End If
	

	Set objMercuryFilecompare = CreateObject("Mercury.FileCompare")

	if objMercuryFilecompare.IsEqualBin( file1 , file2 , 0,1) Then

	WriteStatusToLogs "Images are identical " 
		
		CompareTwoImages = 0
	else

		WriteStatusToLogs "Images are different. " 
	CompareTwoImages = 2
	end if

	Set objMercuryFilecompare = nothing
End Function


Function FileExist(FilePath)
	DIM fso
	Set fso = CreateObject("Scripting.FileSystemObject")
	If fso.FileExists(FilePath) Then
		FileExist = True
	
	Else
	FileExist= False
	End If
	Set fso = nothing
End Function
'----------------------------------------------------------------------

'Internal
Function ReadTextFile(textFile)
	Dim objFileToRead, strFileText
	Set objFileToRead = CreateObject("Scripting.FileSystemObject").OpenTextFile(textFile,1)
	
	On Error Resume Next
	strFileText = objFileToRead.ReadAll()
	If Err.Number <> 0 Then
		Err.Clear
		strFileText = ""
	End If
	On Error Goto 0
	
	objFileToRead.Close
	Set objFileToRead = Nothing
	ReadTextFile = strFileText
End Function

'Internal
Function FindTextInString(textString, textToFind)
	Dim pos
	pos = Instr(1, textString, textToFind, 1)
	If pos > 0 Then
		FindTextInString = True
	Else 
		FindTextInString = False
	End If
End Function

'Internal
Function PDF_to_TXT(pdfFile, txtFile)
	Dim objShell, errorCode, command, fileText
	Set objShell = CreateObject("WScript.Shell")

	command = "pdftotext """ + pdfFile + """ """ + txtFile + """"
	On Error Resume Next
	errorCode = objShell.Run(command,0,True)
	If Err.Number <> 0 Then
		Err.Clear
		On Error Goto 0
		Err.Raise vbObjectError + 102, _
		"PDF_to_TXT", _
		"File pdftotext not found. Please download and add it to the system PATH."
	End If
	On Error Goto 0
	
	If errorCode <> 0 Then
		'Err.Raise vbObjectError + 103, "PDF_to_TXT", "Error executing pdftotext."
		PDF_to_TXT = false
		else
		PDF_to_TXT = true
	End If
End Function

'Action
Function FindTextInPdfFile(pdfFile, textToFind)
	on error resume next
	Dim fileText
	Dim strMethod, tempFolderPath, tempFileName, tempTextFile
	strMethod = strKeywordForReporting
	
	
	If Not FileExist(pdfFile) Then
		FindTextInPdfFile = 1
		WriteStatusToLogs "Invalid pdf file- " & pdfFile
		Exit Function
	End If
	
	
	tempFolderPath = BasePath&"\Temp\"
	tempFileName = "Temp.txt"
	
	If  CreateTempFile(tempFolderPath,tempFileName) <> 0 Then
		FindTextInPdfFile = 1
		WriteStatusToLogs "Failed to create temp file. Please get full admin rights to execute this action."
		Exit Function	
	End If
	
	
	
	tempTextFile = tempFolderPath & tempFileName
	If Not PDF_to_TXT (pdfFile, tempTextFile) then
		FindTextInPdfFile = 1
		WriteStatusToLogs "Error occurred. Please download and add xpdf-tools to the system PATH."
		Exit Function
	End if
	
	fileText = ReadTextFile(tempTextFile)
	If err.number <> 0 then 
		FindTextInPdfFile = 1
		WriteStatusToLogs "Invalid parameters."
		Exit Function
	End if
	If FindTextInString(fileText,textToFind) Then
		FindTextInPdfFile = 0
		WriteStatusToLogs "Text is found in the given pdf file."	
	Else
		FindTextInPdfFile = 2
		WriteStatusToLogs "Text is not found in the given pdf file."	
	End If
	on error goto 0
End Function

'Action
Function FindTextInPdfPage(pdfFile, textToFind, pageNumber)
	On error resume next
	Dim objShell, options, command, errorCode, fileText
	Dim strMethod, tempFolderPath, tempFileName, tempTextFile
	
	strMethod = strKeywordForReporting
	
	
	If not IsNumeric(pageNumber) Then       
		FindTextInPdfPage = 1
		WriteStatusToLogs "PageNumber is not a number."
		Exit Function
	End If
	
	If cint(pageNumber) <1 Then       
		FindTextInPdfPage = 1
		WriteStatusToLogs "Invalid PageNumber. It should be greater than 1."
		Exit Function
	End If
	
	If Not FileExist(pdfFile) Then
		FindTextInPdfPage = 1
		WriteStatusToLogs "Invalid pdf file "& pdfFile
		Exit Function
	End If
	
	tempFolderPath = BasePath&"\Temp\"
	tempFileName = "FindTextInPdfPage.txt"
	
	If CreateTempFile(tempFolderPath,tempFileName)<>0 Then
		FindTextInPdfPage = 1
		WriteStatusToLogs "Failed to create temp file. Please get full admin rights to execute this action."
		Exit Function	
	End If
	
	tempTextFile = tempFolderPath & tempFileName
	
	If Not PDF_to_TXT (pdfFile, tempTextFile) then
		FindTextInPdfPage = 1
		WriteStatusToLogs "Error occurred. Please download and add xpdf-tools to the system PATH."
		Exit Function
	End if
	
	Set objShell = CreateObject("WScript.Shell")
	options = " -f " + CStr(pageNumber) + " -l " + CStr(pageNumber) + " "
	
	command = "pdftotext" + options + """" + pdfFile + """ """ + tempTextFile + """"
	On Error Resume Next
	errorCode = objShell.Run(command,0,True)
	If errorCode <> 0 Then
		Err.Clear
		WriteStatusToLogs "File pdftotext not found. Please download and add it to the system PATH."
		FindTextInPdfPage = 1
		exit function
	End If
	
	fileText = ReadTextFile(tempTextFile)
	if FindTextInString(fileText,textToFind) then 
			FindTextInPdfPage = 0
			WriteStatusToLogs "Text is found in pdf file."
	else
				FindTextInPdfPage = 2
				WriteStatusToLogs "Text is not found in pdf file."
	
	end if
	on error goto 0
End Function

'Action
Function IsPDFContentSameOLD(pdf1, pdf2, tempTextFile)
	On error resume next
	Dim fileText1, fileText2
	Dim strMethod
	
	strMethod = strKeywordForReporting
	
	If IsNull(pdf1) Or IsEmpty(pdf1 ) Or  Trim(pdf2)="" Or IsNull(pdf2) Or IsEmpty(tempTextFile ) Or Trim(tempTextFile)="" Then
		IsPDFContentSame = 1
		WriteStatusToLogs "One of the parameters are empty."
		Exit Function
	End If
	
	
	If Not FileExist(pdf1)  Then
		IsPDFContentSame = 1
		WriteStatusToLogs "Invalid pdf1 file path."
		Exit Function
	End If
	
	If Not FileExist(pdf2)  Then
		IsPDFContentSame = 1
		WriteStatusToLogs "Invalid pdf2 file path."
		Exit Function
	End If
	

	If Not PDF_to_TXT (pdf1, tempTextFile) then
		IsPDFContentSame = 1
		WriteStatusToLogs "Error occurred. Please download and install xpdf-tools in system variable path or verify temp file path."
		Exit Function
	End if
	
	If Not PDF_to_TXT (pdf2, tempTextFile) then
		IsPDFContentSame = 1
		WriteStatusToLogs "Error occurred. Please download and install xpdf-tools in system variable path or verify temp file path."
		Exit Function
	End if
	
	fileText1 = ReadTextFile(tempTextFile)
	
	fileText2 = ReadTextFile(tempTextFile)

	if err.number <> 0 then 
		IsPDFContentSame = 1
		WriteStatusToLogs "Invalid parameters. " 
	end if 
	If fileText1 = fileText2 Then
		IsPDFContentSame = 0
		WriteStatusToLogs "Files are identical. " 
	Else
		IsPDFContentSame = 2
		WriteStatusToLogs "Files are different. " 
	End If
	On error goto 0
End Function

Function IsPDFContentSame(pdf1, pdf2, diffTextFile)
	on error resume next
	Dim f1, f2
	Dim fileText
	Dim strMethod, tempFolderPath, tempFileName1,tempFileName2, RetValue
	Dim revStr,pos, folderPathReverse, folderPath
	
	strMethod = strKeywordForReporting
	
	If IsNull(pdf1) Or IsEmpty(pdf1 ) Or  Trim(pdf2)="" Or IsNull(pdf2) Or IsEmpty(diffTextFile ) Or Trim(diffTextFile)="" Then
		IsPDFContentSame = 1
		WriteStatusToLogs "One of the parameters are empty."
		Exit Function
	End If
	
	
	If Not FileExist(pdf1)  Then
		IsPDFContentSame = 1
		WriteStatusToLogs "Invalid pdf1 file path- " &pdf1
		Exit Function
	End If
	
	If Not FileExist(pdf2)  Then
		IsPDFContentSame = 1
		WriteStatusToLogs "Invalid pdf2 file path- "& pdf2
		Exit Function
	End If
	

	revStr = StrReverse(diffTextFile)
	pos = instr(revStr, "\")
	folderPathReverse = mid(revStr, pos, len(revStr))
	folderPath =  StrReverse(folderPathReverse)
	
	If VerifyFolderExistInternal(folderPath)  Then
		IsPDFContentSame = 1
		WriteStatusToLogs "Folder path for diffTextFile is invalid "& folderPath
		Err.number = 0
		Exit Function
	End If	
	
	tempFolderPath = BasePath&"\Temp\"
	tempFileName1 = "IsPDFContentSame1.txt"
	tempFileName2 = "IsPDFContentSame2.txt"
	
	If CreateTempFile(tempFolderPath,tempFileName1)<>0 Then
		IsPDFContentSame = 1
		WriteStatusToLogs "Failed to create temp file. Please get full admin rights to execute this action."
		Exit Function	
	End If
	
	f1 = tempFolderPath & tempFileName1
	
	If  CreateTempFile(tempFolderPath,tempFileName2)<>0 Then
		IsPDFContentSame = 1
		WriteStatusToLogs "Failed to create temp file. Please get full admin rights to execute this action."
		Exit Function	
	End If
	
	f2 = tempFolderPath & tempFileName2
	
	
	If Not PDF_to_TXT (pdf1, f1) then
		IsPDFContentSame = 1
		WriteStatusToLogs "Error occurred. Please download and install xpdf-tools in system variable path or verify temp file path."
		Exit Function
	End if
	
	If Not PDF_to_TXT (pdf2, f2) then
		IsPDFContentSame = 1
		WriteStatusToLogs "Error occurred. Please download and install xpdf-tools in system variable path or verify temp file path."
		Exit Function
	End if

	RetValue = TextFileDiff (f1, f2, diffTextFile)
	if RetValue = 1 Then
		IsPDFContentSame = 1
		WriteStatusToLogs "'diffFilePath' not a valid path OR File diff.exe not found. Please download GNU diff utils and add to system PATH."
		Exit Function
	elseif RetValue = 2 then
	IsPDFContentSame=1
			WriteStatusToLogs "Error executing diff.exe"
		Exit Function
	elseif RetValue = 0 then
			IsPDFContentSame = 0
			WriteStatusToLogs "Pdf diff is created and stored in- "& diffTextFile
		Exit Function
	end if
	
	On error goto 0
End Function

'Internal
Function TextFileDiff(f1,f2,fdiff)
	On error resume next
	Dim objShell, errorCode, command, fileText
	Set objShell = CreateObject("WScript.Shell")

	command = "cmd /c diff -u """ + f1 + """ """ + f2 + """ > """ + fdiff + """"
	On Error Resume Next
	errorCode = objShell.Run(command,0,True)
	If Err.Number <> 0 Then
		Err.Clear
		On Error Goto 0
		'Err.Raise vbObjectError + 104, _
		'"TextFileDiff", _
		'"File diff.exe not found. Please download GNU diff utils and add to system PATH."
		TextFileDiff = 1
		Exit function
	End If
	On Error Goto 0
	
	If errorCode = 2 Then
		'Err.Raise vbObjectError + 105, "TextFileDiff", "Error executing diff.exe."
		On Error Goto 0
		TextFileDiff = 2
		exit function
	Else
		TextFileDiff = 0
	End If
	
	On error goto 0
End Function

'-------------------------------------------------------
function StoreRandomNumber(ByVal minNumber, ByVal maxNumber, ByVal strKey)
    On error resume Next
    
    Dim rand, intval, strMethod
    
    strMethod = strKeywordForReporting
    Randomize
    rand = Int((maxNumber-minNumber+1)*Rnd+minNumber)
    
    if err.number > 0  then		
		StoreRandomNumber = 1
			WriteStatusToLogs err.Description
		Exit function
	End If
    
  	intval = AddItemToStorageDictionary(strKey,CInt(rand))
  	If  intval = 0 Then
		StoreRandomNumber = 0
		WriteStatusToLogs "The random value '"&rand&"' is stored successfully in the key '"&strKey&"'."
	Else					
		StoreRandomNumber = 1
		WriteStatusToLogs "The random value '"&rand&"' doesn't stored successfully in the key '"&strKey&"'."
	End If
End function
'---------------------------------------------------------------------

'----------------------------------------------------------------------
Function StoreModifiedDateTimeFormat(Inputdate, format, key)
	On error resume next
	Dim currentDate, newdate
	
	If not IsDate(cdate(InputDate)) Then
		StoreModifiedDateTimeFormat = 1
		WriteStatusToLogs "Invalid InputDate "& InputDate
		Exit Function
	End If
	
	If IsNumber(format)<> 0 Then
		StoreFileRowData = 1
		WriteStatusToLogs "format is non numeric."
		Exit Function
	End If
	
	If IsNull(key) Or IsEmpty(key ) or key = "" Then
		StoreModifiedDateTimeFormat = 1
		WriteStatusToLogs "key is null or empty."
		Exit Function
	End If
	
	currentDate = cdate(InputDate)
	newdate = FormatDateTime(currentDate, format)
	
	If err.number <> 0 Then
		StoreModifiedDateTimeFormat = 1
		WriteStatusToLogs "Error occurred while formatting date. verify parameters."

	else
		StoreModifiedDateTimeFormat = 0
		intval = AddItemToStorageDictionary(key,newdate)
		WriteStatusToLogs "Key is stored with modified format of date: " & newdate
	End If
	On error goto 0
End Function

Function StoreDateAdd (InputDate,Interval, Data, key )
	On error resume next
	Dim strMethod, intval, newadte
	
	If IsNumber(Data)<> 0 Then
		StoreDateAdd = 1
		WriteStatusToLogs "Data is non numeric."
		Exit Function
	End If
	
	
	If not IsDate(cdate(InputDate)) Then
		StoreDateAdd = 1
		WriteStatusToLogs "Invalid InputDate "& InputDate
		Exit Function
	End If
	
	If IsNull(Interval) Or IsEmpty(Interval ) or Interval = "" Then
		StoreDateAdd = 1
		WriteStatusToLogs "Interval is null or empty."
		Exit Function
	End If
	
	If IsNull(key) Or IsEmpty(key ) or key = "" Then
		StoreDateAdd = 1
		WriteStatusToLogs "key is null or empty."
		Exit Function
	End If
	
	newadte = DateAdd(Interval, Data, InputDate)
	
	If err.number <> 0 Then
		StoreDateAdd = 1
		WriteStatusToLogs "Error occurred while adding dates. verify parameters."

	else
		StoreDateAdd = 0
		intval = AddItemToStorageDictionary(key,newadte )
		WriteStatusToLogs "Key is stored with added date: " & newadte
	End If
	on error goto 0
End Function

Function StoreCharacterCount(FilePath, Key, ignoreWhitespace)
	On error resume next
	Dim FileSysObj
	Dim strMethod
	Dim CaseSens, found, l, intval
	found =0
	
	strMethod = strKeywordForReporting

	If IsNull(FilePath) Or IsEmpty(FilePath ) or FilePath = "" Then
		StoreCharacterCount = 1
		WriteStatusToLogs "File path is null or empty."
		Exit Function
	End If
	
	If IsNull(Key) Or IsEmpty(Key ) or Key = "" Then
		StoreCharacterCount = 1
		WriteStatusToLogs "Key is null or empty."
		Exit Function
	End If
	
	If IsBoolean(ignoreWhitespace) Then
		CompareFileContents = 1
		WriteStatusToLogs "IgnoreWhitespace is not boolean."
		Exit Function
	End If
	ignoreWhitespace = cbool(ignoreWhitespace)
	
	Set FileSysObj = CreateObject("Scripting.FileSystemObject")
	If Not FileSysObj.FileExists(FilePath) Then
		StoreCharacterCount = 1
		WriteStatusToLogs "Invalid path " & FilePath
		Exit Function
	End If
	
	Set OpenFile = FileSysObj.OpenTextFile(FilePath,1)
	
	strLine = OpenFile.ReadAll
	
	If ignoreWhitespace Then
		strLine = trim(strLine)
	End If
	
	l = Len(strLine)
	
	
	
	If err.number = 0 Then
	intval = AddItemToStorageDictionary(key,l )
		StoreCharacterCount = 0
		WriteStatusToLogs "Key '"&key&"' is stored with character count '"& l&"'."
	else
		StoreCharacterCount = 1
		WriteStatusToLogs "Error occurred while treading file."
	End If
	
	Set FileSysObj = nothing
	on error goto 0
End Function

Function CompareFileContents(File1, File2,CaseSensitive, IgnoreWhitespaces)
	On error resume next
	Dim FileSysObj
	Dim strMethod
	Dim CaseSens, Match, OpenFile1, OpenFile2, str1, str2, s1, s2
	Match =0
	
	strMethod = strKeywordForReporting

	If IsNull(File1) Or IsEmpty(File1 ) or File1 = "" Then
		CompareFileContents = 1
		WriteStatusToLogs "File path is null or empty."
		Exit Function
	End If
	
	If IsNull(File2) Or IsEmpty(File2 ) or File2 = "" Then
		CompareFileContents = 1
		WriteStatusToLogs "File path is null or empty."
		Exit Function
	End If
	
	If IsBoolean(CaseSensitive) Then
		CompareFileContents = 1
		WriteStatusToLogs "CaseSensitive is not boolean."
		Exit Function
	End If
	
	If StrComp(CaseSensitive, "true",1) = 0 Then
		CaseSens = 0
		else
		CaseSens = 1
	End If
	
	If IsBoolean(IgnoreWhitespaces) Then
		CompareFileContents = 1
		WriteStatusToLogs "IgnoreWhitespaces is not boolean."
		Exit Function
	End If
	IgnoreWhitespaces = CBool(IgnoreWhitespaces)
	
	Set FileSysObj = CreateObject("Scripting.FileSystemObject")
	If Not FileSysObj.FileExists(File1) Then
		CompareFileContents = 1
		WriteStatusToLogs "Invalid path " & File1
		Exit Function
	End If
	
	If Not FileSysObj.FileExists(File2) Then
		CompareFileContents = 1
		WriteStatusToLogs "Invalid path " & File2
		Exit Function
	End If
	
	Set OpenFile1 = FileSysObj.OpenTextFile(File1,1)
	Set OpenFile2 = FileSysObj.OpenTextFile(File2,1)
	
	str1 = OpenFile1.ReadAll
	str2 = OpenFile2.ReadAll
	
	If IgnoreWhitespaces Then
		s1 = trim(str1)
		s2 = trim(str2)
		else
		s1= str1
		s2=str2

	End If

	
	
	if StrComp(s1,s2,CaseSens) = 0 then
		Match = 1
	end if
	
	
	If Match = 1 Then
			CompareFileContents = 0
		WriteStatusToLogs "File contents are identical."
	else
		CompareFileContents = 2
		WriteStatusToLogs "File contents are different."
	End If
	
	Set FileSysObj = nothing
	on error goto 0
End Function

Function StoreFileRowData(FilePath, RowNumber, Key)
	On error resume next
	Dim FileSysObj
	Dim strMethod
	Dim CaseSens, found, i, pos, intval
	found =0
	
	strMethod = strKeywordForReporting

	If IsNull(FilePath) Or IsEmpty(FilePath ) or FilePath = "" Then
		StoreFileRowData = 1
		WriteStatusToLogs "File path is null or empty."
		Exit Function
	End If
	
	If IsNull(Key) Or IsEmpty(Key ) or Key = "" Then
		StoreFileRowData = 1
		WriteStatusToLogs "Key is null or empty."
		Exit Function
	End If
	
	If IsNumber(RowNumber)<> 0 Then
		StoreFileRowData = 1
		WriteStatusToLogs "RowNumber is non numeric."
		Exit Function
	End If
	

	
	Set FileSysObj = CreateObject("Scripting.FileSystemObject")
	If Not FileSysObj.FileExists(FilePath) Then
		StoreFileRowData = 1
		WriteStatusToLogs "Invalid path " & FilePath
		Exit Function
	End If
	
	Set OpenFile = FileSysObj.OpenTextFile(FilePath,1)
	
	Do Until OpenFile.AtEndOfStream
		i=i+1
	    strLine = OpenFile.ReadLine
		
		if (i=cint(RowNumber)) Then
			found = 1
			exit do
		
		End if
	
	Loop
	
	
	
	If found = 1 Then
	intval = AddItemToStorageDictionary(key,strLine )
		StoreFileRowData = 0
		WriteStatusToLogs "The row data '" & strLine & "' is successfully stored in the key '"& Key &"'." 
	else
		StoreFileRowData = 1
		WriteStatusToLogs "RowNumber larger than rows in file."
	End If
	
	Set FileSysObj = nothing
	on error goto 0
End Function

Function VerifyDataExistInFile(FilePath, Data, CaseSensitive)
	On error resume next
	Dim FileSysObj
	Dim strMethod
	Dim CaseSens, found, i, pos
	found =0
	
	strMethod = strKeywordForReporting

	If IsNull(FilePath) Or IsEmpty(FilePath ) or FilePath = "" Then
		VerifyDataExistInFile = 1
		WriteStatusToLogs "File path is null or empty."
		Exit Function
	End If
	
	If IsBoolean(CaseSensitive) Then
		VerifyDataExistInFile = 1
		WriteStatusToLogs "CaseSensitive is not boolean."
		Exit Function
	End If
	
	If StrComp(CaseSensitive, "true",1) = 0 Then
		CaseSens = 0
		else
		CaseSens = 1
	End If
	
	Set FileSysObj = CreateObject("Scripting.FileSystemObject")
	If Not FileSysObj.FileExists(FilePath) Then
		VerifyDataExistInFile = 1
		WriteStatusToLogs "Invalid path."
		Exit Function
	End If
	
	Set OpenFile = FileSysObj.OpenTextFile(FilePath,1)
	
	Do Until OpenFile.AtEndOfStream
		i=i+1
	    strLine = OpenFile.ReadLine
	
	    If InStr(1,strLine, Data, CaseSens) > 0 Then
	    	found = 1
	    	pos = InStr(1,strLine, Data, CaseSens)
	    	Exit do
	    End If
	Loop
	
	If found = 1 Then
			VerifyDataExistInFile = 0
		WriteStatusToLogs "Data found in file at row '"&i & "' and column '"&pos &"'"
	else
		VerifyDataExistInFile = 2
		WriteStatusToLogs "data not found in file."
	End If
	
	Set FileSysObj = nothing
	on error goto 0
End Function

Function VerifyFileExist(FilePath)
	on error resume next
	Dim FileSysObj
	Dim strMethod
	
	strMethod = strKeywordForReporting
	
	If IsNull(FilePath) Or IsEmpty(FilePath ) or FilePath = "" Then
		VerifyFileExist = 1
		WriteStatusToLogs "Null or empty parameter."
		Exit Function
	End If
	
	
	Set FileSysObj = CreateObject("Scripting.FileSystemObject")
	If FileSysObj.FileExists(FilePath) Then
		VerifyFileExist = 0
		WriteStatusToLogs "File exist at location- " & FilePath
	Else
		VerifyFileExist = 2
		WriteStatusToLogs "File does not exist at location- " & FilePath
	End If
	Set FileSysObj = nothing
	on error goto 0
End Function

Function VerifyFolderExist(FilePath)
	on error resume next
	Dim FileSysObj
	Dim strMethod
	
	strMethod = strKeywordForReporting
	
	If IsNull(FilePath) Or IsEmpty(FilePath ) or FilePath = "" Then
		VerifyFolderExist = 1
		WriteStatusToLogs "Null or empty parameter."
		Exit Function
	End If
	
	
	Set FileSysObj = CreateObject("Scripting.FileSystemObject")
	If FileSysObj.FolderExists(FilePath) Then
		VerifyFolderExist = 0
		WriteStatusToLogs "Folder exist at location- " & FilePath
	Else
		VerifyFolderExist = 2
		WriteStatusToLogs "Folder does not exist- " & FilePath
	End If
	Set FileSysObj = nothing
	on error goto 0
End Function
'----------------------------------------------------------------------


Function CreateTempFile(tempFolder,tempFile)
	On error resume next
	
	Dim fso, fullPath, objFolder, objFile, objTextFile
	err.number = 0
	
	Set fso = CreateObject("Scripting.FileSystemObject")
	fullPath = tempFolder & tempFile
	
	If fso.FolderExists(tempFolder) Then
	   Set objFolder = fso.GetFolder(tempFolder)
	Else
	   Set objFolder = fso.CreateFolder(tempFolder)
	   'WScript.Echo "Just created " & strDirectory
	End If
	
	Set objFolder = nothing 
	
	If fso.FileExists(fullPath) Then
	   Set objFile = fso.CreateTextFile(fullPath)
	Else
	   Set objFile = fso.CreateTextFile(fullPath)

	End If
	
	Set objFile = nothing
	
'	Set objTextFile = fso.OpenTextFile (fullPath, 2, True)
'	objTextFile.WriteLine(fileContent) 
'	
	if err.number = 0 then
	CreateTempFile = 0
	else
	CreateTempFile = 1
	End if
	
	Set objTextFile = nothing
	Set fso =  nothing
	
	on error goto 0
End function

'Internal
Function VerifyFolderExistInternal(tempFolder)
	On error resume next
	Dim fso, fullPath
	err.number = 0
	
	Set fso = CreateObject("Scripting.FileSystemObject")
	fullPath = tempFolder & tempFile
	
	If fso.FolderExists(tempFolder) Then
	   VerifyFolderExistInternal = 0
	Else
		VerifyFolderExistInternal = 1
	End If

	Set fso =  nothing
	
	on error goto 0
End function
'-------------------------------------------------------------------
'Below action are written as part of 4.3.6.
'These actions are required by CVC client for the poc

Function CompareNumbers(byval num1, byval ComparisionOperator, byval num2)
	On error resume next
	Dim strMethod
	
	strMethod = strKeywordForReporting
	err.clear	
	If not IsNumeric(num1) Then       
		CompareNumbers = 1
		WriteStatusToLogs "First argument is not a number."
		Exit Function
	End If
	
	If not IsNumeric(num2) Then       
		CompareNumbers = 1
		WriteStatusToLogs "Second argument is not a number."
		Exit Function
	End If
	
	If not (len(trim(ComparisionOperator)) = 1 and (ComparisionOperator = "=" or ComparisionOperator = ">" or ComparisionOperator = "<")) Then
		CompareNumbers = 1
		WriteStatusToLogs "Invalid ComparisionOperator provided. please select one from =,<,>"
		Exit Function
	End If
	
	
	
	If eval(num1 & ComparisionOperator &num2) Then
	CompareNumbers = 0
			If ComparisionOperator = trim("=") Then
							WriteStatusToLogs "First number '"&num1&"' is equal to second number '"&num2&"'."
			ElseIf ComparisionOperator = trim("<") Then
								WriteStatusToLogs "First number '"&num1&"' is smaller than second number '"&num2&"'."
			ElseIf ComparisionOperator = trim(">") Then
									WriteStatusToLogs "First number '"&num1&"' is greater than second number '"&num2&"'."
			End If
	
	else
		CompareNumbers = 2
			If ComparisionOperator = trim("=") Then
							WriteStatusToLogs "First number '"&num1&"' is not equal to second number '"&num2&"'."
			ElseIf ComparisionOperator = trim("<") Then
								WriteStatusToLogs "first number '"&num1&"'  is not smaller than second number '"&num2&"'."
			ElseIf ComparisionOperator = trim(">") Then
									WriteStatusToLogs "first number '"&num1&"' is not greater than second number '"&num2&"'."
			End If
	End If
		
	On error goto  0
End Function

Function StoreRoundNumber(k, num)
	On error resume next
	Dim strMethod, x
	
	strMethod = strKeywordForReporting	
	err.clear
	If not IsNumeric(num) Then       
		StoreRoundNumber = 1
		WriteStatusToLogs "First argument is not a number."
		Exit Function
	End If
	
	
	If IsNull(k) Or IsEmpty(k ) or k = "" Then
		StoreRoundNumber = 1
		WriteStatusToLogs "key is null or empty."
		Exit Function
	End If
	
	x = Round(num)
	
	intval = AddItemToStorageDictionary(k,x)
	
	If err.number = 0 Then
		StoreRoundNumber = 0
			WriteStatusToLogs "Number is rounded and the key '"& k &"' is stored with the round number: "&x
	else
		StoreRoundNumber = 1
			WriteStatusToLogs "error occurred while rounding the number."
	End If
	
	On error goto  0
End Function

Function StoreStringToInteger(k, num)
	On error resume next
	Dim strMethod, x
	
	strMethod = strKeywordForReporting	
	err.clear
	If not IsNumeric(num) Then       
		StoreStringToInteger = 1
		WriteStatusToLogs "First argument is not a number."
		Exit Function
	End If
	
	
	If IsNull(k) Or IsEmpty(k ) or k = "" Then
		StoreStringToInteger = 1
		WriteStatusToLogs "key is null or empty."
		Exit Function
	End If
	
	x = cint(num)
	
	intval = AddItemToStorageDictionary(k,x)
	
	If err.number = 0 Then
		StoreStringToInteger = 0
			WriteStatusToLogs "Converted string to integer successfully."
	else
		StoreStringToInteger = 1
			WriteStatusToLogs "error occurred while converting string to integer"
	End If
	
	On error goto  0
End Function

Function StoreFixNumber(k, num)
	On error resume next
	Dim strMethod, x
	
	strMethod = strKeywordForReporting	
	err.clear
	If not IsNumeric(num) Then       
		StoreFixNumber = 1
		WriteStatusToLogs "First argument is not a number."
		Exit Function
	End If
	
	
	If IsNull(k) Or IsEmpty(k ) or k = "" Then
		StoreFixNumber = 1
		WriteStatusToLogs "key is null or empty."
		Exit Function
	End If
	
	x = Fix(num)
	
	intval = AddItemToStorageDictionary(k,x)
	
	If err.number = 0 Then
		StoreFixNumber = 0
			WriteStatusToLogs "the fix number is: "&x
	else
		StoreFixNumber = 1
			WriteStatusToLogs "error occurred while fetching the fix number."
	End If
	
	On error goto  0
End Function

Function ReplaceStringBySubString(k, str,find,replacewith, start, count, strCompare)
	On error resume next
	Dim strMethod, x
	
	strMethod = strKeywordForReporting	
	err.clear
	If IsNull(k) Or Trim(k) = "" Or IsNull(str) or Trim(str) = "" Or IsNull(find) or Trim(find) = "" Or IsNull(replacewith) or Trim(replacewith) = "" Then
		ReplaceStringBySubString = 1
		WriteToErrorLog err,"Message : One of the mandatory parameter is null or empty."
		WriteStatusToLogs 	"One of the mandatory parameter is null or empty."
		Exit Function
	End If
	

	If not (IsNumeric(start) and IsNumeric(count) and IsNumeric(strCompare) ) Then       
		ReplaceStringBySubString = 1
		WriteStatusToLogs "One of the optional parameter is not a number."
		Exit Function
	End If
	
	If start = "" Then
		start = 1
	End If
	
	If count = "" Then
		count = -1
	End If
	
	If strCompare = "" Then
		strCompare = 0
	End If
	
	
	x = replace(str,find,replacewith, cint(start), cint(count), cint(strCompare))
	
	intval = AddItemToStorageDictionary(k,x)
	
	If err.number = 0 Then
		ReplaceStringBySubString = 0
			WriteStatusToLogs "String replace is successful. Key '"&k&"' is stored with new string : "&x
	else
		ReplaceStringBySubString = 1
			WriteStatusToLogs "error occurred while replacing the string. Check parameters."
	End If
	
	On error goto  0
End Function

Function StoreChangeCase(k, Str,caseToChange)
	On error resume next
	Dim strMethod, x
	
	strMethod = strKeywordForReporting	
	err.clear

	
	If IsNull(k) Or IsEmpty(k ) or k = "" or IsNull(Str) Or IsEmpty(Str) or Str = "" or IsNull(caseToChange) Or IsEmpty(caseToChange) or caseToChange = ""  Then
		StoreChangeCase = 1
		WriteStatusToLogs "parameter is null or empty."
		Exit Function
	End If
	
	If lcase(trim(caseToChange)) = "lower" Then
		x = lcase(Str)
	ElseIf lcase(trim(caseToChange)) = "upper" Then
		x = ucase(Str)
	else
		StoreChangeCase = 1
		WriteStatusToLogs "Invalid Case provided."
		Exit Function
	End If
	intval = AddItemToStorageDictionary(k,x)
	
	
	
	If err.number = 0 Then
		StoreChangeCase = 0
			WriteStatusToLogs "String case changed successfully. Key '"&k&"' is stored with new string : "&x
	else
		StoreChangeCase = 1
			WriteStatusToLogs "error occurred while changing the case. Check parameters."
	End If
	
	On error goto  0
End Function
'---------------------------------------------------------------------
Function StoreStringConcat(Str1,str2, k)
	On error resume next
	Dim strMethod, x
	
	strMethod = strKeywordForReporting	
	err.clear

	
	If IsNull(k) Or IsEmpty(k ) or k = "" Then
		StoreStringConcat = 1
		WriteStatusToLogs "Key is null or empty."
		Exit Function
	End If
	
	x = Str1&Str2
	
	If err.number = 0 Then
		StoreStringConcat = 0
			WriteStatusToLogs "Strings concatenated successfully. Key '"&k&"' is stored with new string : "&x
	else
		StoreStringConcat = 1
			WriteStatusToLogs "Error occurred while concatenating  strings. Check parameters."
	End If
	
	On error goto  0
End Function
'------------------------------------------------------------------
Function Array_JoinArrayAndStore(ArrayKey, k)
	
	On error resume next
	Dim strMethod, collectionData, intval, tempArr
	Dim textCase, found, c

	strMethod = strKeywordForReporting
	err.clear	
	
	'index = index+1
	
	If IsNull(k) Or IsEmpty(k ) or k = "" Then
		Array_JoinArrayAndStore = 1
		WriteStatusToLogs "key is null or empty."
		Exit Function
	End If



	If IsNull(ArrayKey) Or IsEmpty(ArrayKey) or ArrayKey = "" Then
		Array_JoinArrayAndStore = 1
		WriteStatusToLogs "ArrayKey is not a valid key."
		Exit Function
	End If
	
	tempArr = oDictStorage.item(ArrayKey)
	
	If not IsArray(tempArr) Then
		Array_JoinArrayAndStore  = 1
		WriteStatusToLogs "The key '" &ArrayKey& "' doesn't hold array."
		Exit function
	End if
	
	If error.number = 0 Then
		intval = AddItemToStorageDictionary(k,join(tempArr, " ^ "))
		If err.number = 0 Then
	       Array_JoinArrayAndStore  = 0
		   WriteStatusToLogs "The key '" &k& "' is stored with data '"&join(tempArr, " ^ ")&"' ." 
		Else 
	       Array_JoinArrayAndStore  = 1
		   WriteStatusToLogs "Error while storing in key. "& err.description	
		
		End If
	End If
	On error goto 0
End Function

Function Array_StoreElementIndex(ArrayKey, Element, Occurrence,CaseSensitive,k)
	On error resume next
	Dim strMethod, collectionData, intval, tempArr
	Dim textCase, found, c
	textCase = 0
	found=false

	strMethod = strKeywordForReporting
	err.clear	
	
	If not IsNumeric(Occurrence) and cint(Occurrence)<-1 Then       
		Array_StoreElementIndex = 1
		WriteStatusToLogs "Occurrence is not a number."
		Exit Function
	End If
	
	'index = index+1
	
	If IsNull(k) Or IsEmpty(k ) or k = "" Then
		Array_StoreElementIndex = 1
		WriteStatusToLogs "key is null or empty."
		Exit Function
	End If



	If IsNull(ArrayKey) Or IsEmpty(ArrayKey) or ArrayKey = "" Then
		Array_StoreElementIndex = 1
		WriteStatusToLogs "ArrayKey is not a valid key."
		Exit Function
	End If
	
	tempArr = oDictStorage.item(ArrayKey)
	
	If lcase(trim(CaseSensitive)) = "true" or ucase(trim(CaseSensitive))= "0" Then
	    textCase = 0
	ElseIf lcase(trim(CaseSensitive)) = "false" or ucase(trim(CaseSensitive))= "1" Then
	    textCase = 1
	End If
	
	If instr(1,join(tempArr), Element, textCase) > 0 Then
	
		For i = 0 To ubound(tempArr) 
			If instr(1,tempArr(i),Element, textCase) > 0 Then
				If not found Then
					c= c+1
					If c = cint(Occurrence) Then
						found = true
						Exit for
					End If
				End If
			End If
		Next
	End If
	

	
	If found Then
		intval = AddItemToStorageDictionary(k,i+1)
		If err.number = 0 Then
					Array_StoreElementIndex = 0
		            WriteStatusToLogs "Execution is successful. Key '"&k&"' is stored with '"&(i+1)&"'"
		else
					Array_StoreElementIndex = 1
		            WriteStatusToLogs "Failed to store the element in the key."		
		End If
	Else
					Array_StoreElementIndex = 1
		            WriteStatusToLogs "Failed to fetch element from array."		
	End If
	
End Function

function Array_StoreElementByIndex(ArrayKey, index, k)
	On error resume next
	Dim strMethod, collectionData, intval, tempArr

	strMethod = strKeywordForReporting
	err.clear	
	
	If not IsNumeric(index) Then       
		Array_StoreElementByIndex = 1
		WriteStatusToLogs "Index is not a number."
		Exit Function
	End If
	
	'index = index+1
	
	If IsNull(k) Or IsEmpty(k ) or k = "" Then
		Array_StoreElementByIndex = 1
		WriteStatusToLogs "key is null or empty."
		Exit Function
	End If


	If IsNull(ArrayKey) Or IsEmpty(ArrayKey) or ArrayKey = "" Then
		Array_StoreElementByIndex = 1
		WriteStatusToLogs "ArrayKey is not a valid key."
		Exit Function
	End If
	
	tempArr = oDictStorage.item(ArrayKey)
	
	collectionData = tempArr(index)
	
	If err.number= 0 Then
		intval = AddItemToStorageDictionary(k,collectionData)
		Array_StoreElementByIndex = 0
		WriteStatusToLogs "Execution is successful. Key '"&k&"' is stored with '"&collectionData&"'"
	else
		Array_StoreElementByIndex = 1
		WriteStatusToLogs "Error occurred. "& err.description
	End If
	
	On error goto 0

End  function

Function Array_StoreArrayLength(ArrayKey, K)
	On error resume next
	Dim strMethod
	Dim tempArr
	Dim textCase, intval
	textCase = 0

	strMethod = strKeywordForReporting
	err.clear

	If isnull(ArrayKey) or isnull(ArrayKey) Then       
		Array_StoreArrayLength  = 1
		WriteStatusToLogs "ArrayKey is null or empty."
		Exit Function
	End If
	
	tempArr = oDictStorage.item(ArrayKey)
	If err.number <> 0 Then
		Array_StoreArrayLength  = 1
		WriteStatusToLogs "ArrayKey does not hold array."
		Exit Function
	End If
	
	If not IsArray(tempArr) Then
		Array_StoreArrayLength  = 1
		WriteStatusToLogs "The key '" &ArrayKey& "' doesn't hold array."
		Exit function
	End if
	
	intval = AddItemToStorageDictionary(k,UBound(tempArr))
	If err.number = 0 and intval = 0 Then
		Array_StoreArrayLength  = 0
		WriteStatusToLogs "The key '" &k& "' is stored with arrayLength '"&UBound(tempArr)+1&"'."
	else
		Array_StoreArrayLength  = 1
		WriteStatusToLogs "Error occurred while finding the array length. "& err.description	
		
	End If
	
	On error goto 0
End Function

Function Array_IsArray(ArrayKey)
	On error resume next
	Dim strMethod
	Dim tempArr
	Dim textCase, intval
	textCase = 0

	strMethod = strKeywordForReporting
	err.clear

	If isnull(ArrayKey) or isnull(ArrayKey) Then       
		Array_IsArray  = 1
		WriteStatusToLogs "ArrayKey is null or empty."
		Exit Function
	End If
	
	tempArr = oDictStorage.item(ArrayKey)
	If err.number <> 0 Then
		Array_IsArray  = 1
		WriteStatusToLogs "ArrayKey does not hold array."
		Exit Function
	End If
	
	If IsArray(tempArr) Then
		Array_IsArray  = 0
		WriteStatusToLogs "The key '" &ArrayKey& "' holds array. Array length '"&ubound(tempArr)&"' and array items ["&join(tempArr, " ^ ")&"]."
	else
		Array_IsArray  = 1
		WriteStatusToLogs "The key '" &ArrayKey& "' does not hold array. "
	End If
	On error goto 0
End Function

Function Array_deleteItem(ArrayKey, index)
	On error resume next
	Dim strMethod
	Dim tempArr
	Dim textCase, intval
	textCase = 0

	strMethod = strKeywordForReporting
	err.clear

	If isnull(ArrayKey) or isnull(ArrayKey) Then       
		Array_deleteItem  = 1
		WriteStatusToLogs "ArrayKey is null or empty."
		Exit Function
	End If
	
	If not (IsNumeric(index) and cint(index)>0 ) Then       
		Array_deleteItem = 1
		WriteStatusToLogs "Array index is invalid."
		Exit Function
	End If
	
	tempArr = oDictStorage.item(ArrayKey)
	
	If not IsArray(tempArr) Then
		Array_deleteItem = 1
		WriteStatusToLogs "ArrayKey does not hold array."
		Exit Function
	End If
	
	index = index-1
	
	ReDim preserve tempArr(UBound(tempArr))
	For i = index To UBound(tempArr)-1
		tempArr(i) = tempArr(i+1)
	Next	
	
	ReDim preserve tempArr(UBound(tempArr)-1)
	
	
	If err.number = 0 Then
	
		intval = AddItemToStorageDictionary(ArrayKey,tempArr)
		If err.number = 0 Then
				Array_deleteItem = 0
		        WriteStatusToLogs "Execution is successful and key '" &ArrayKey& "' is stored with new array. Array size: '" & ubound(tempArr) &"' array items: ["& join(tempArr, " ^ ") &"]."
		Else
				Array_deleteItem = 1
		        WriteStatusToLogs "Failed to store the array in Key. Verify parameters."	
		End If

	Else
		Array_deleteItem = 1
		WriteStatusToLogs "Failed while processing the array. Verify params."
	End If
	On error goto 0
End Function

Function Array_AddItem(ArrayKey, Element)
	On error resume next
	Dim strMethod
	Dim tempArr
	Dim textCase
	textCase = 0

	strMethod = strKeywordForReporting
	err.clear

	If isnull(ArrayKey) or isnull(ArrayKey) Then       
		Array_AddItem  = 1
		WriteStatusToLogs "ArrayKey is null or empty."
		Exit Function
	End If
	
	tempArr = oDictStorage.item(ArrayKey)
	
	If not IsArray(tempArr) Then
		Array_AddItem = 1
		WriteStatusToLogs "ArrayKey does not hold array."
		Exit Function
	End If
	
	
	ReDim preserve tempArr(UBound(tempArr)+1)
	tempArr(UBound(tempArr)) = Element
	
	If err.number =0 Then
		intval = AddItemToStorageDictionary(ArrayKey,tempArr)
		Array_AddItem = 0
		WriteStatusToLogs "Execution is successful and key '" &k& "' is stored with new array. Array size: '" & ubound(tempArr) &"' array items: ["& join(tempArr, " ^ ") &"]."
	Else
		Array_AddItem = 1
		WriteStatusToLogs "Error occurred while finding matches. " & err.description
	
	End If
	
	
	
	On error goto 0
End Function

Function Array_VerifyElementExist(ArrayKey,Element,CaseSensitive)
	Dim strMethod
	Dim tempArr
	Dim textCase
	textCase = 0

	strMethod = strKeywordForReporting
	err.clear

	If isnull(ArrayKey) or isnull(ArrayKey) Then       
		Array_VerifyElementExist  = 1
		WriteStatusToLogs "ArrayKey is null or empty."
		Exit Function
	End If
	
	If isnull(CaseSensitive) or isnull(CaseSensitive) Then       
		Array_VerifyElementExist  = 1
		WriteStatusToLogs "ArrayKey is null or empty."
		Exit Function
	End If
	
	If lcase(trim(CaseSensitive)) = "true" or ucase(trim(CaseSensitive))= "0" Then
	    textCase = 0
	ElseIf lcase(trim(CaseSensitive)) = "false" or ucase(trim(CaseSensitive))= "1" Then
	    textCase = 1
	End If
	
	tempArr = oDictStorage.item(ArrayKey)
	
	If not IsArray(tempArr) Then
		Array_VerifyElementExist = 1
		WriteStatusToLogs "ArrayKey does not hold array."
		Exit Function
	End If
	
	If instr(1, join(tempArr),Element,textCase) > 0 Then
		Array_VerifyElementExist = 0
		WriteStatusToLogs "Element found in array."
		Else
		Array_VerifyElementExist = 1
		WriteStatusToLogs "Element not found in array."
	End If

	On error goto 0
	
End Function
 
Function Array_Sort(ArrayKey)
	On error resume next
	Dim strMethod
	Dim objRegEx, Match, Matches, StrReturnStr, intval, arrIndex
	Dim DataList
	
	err.clear
	strMethod = strKeywordForReporting

	Set DataList = CreateObject("System.Collections.ArrayList")

	If isnull(ArrayKey) or isnull(ArrayKey) Then       
		Array_Sort  = 1
		WriteStatusToLogs "ArrayKey is null or empty."
		Exit Function
	End If
	
	tempArr = oDictStorage.item(ArrayKey)
	If not IsArray(tempArr) Then
		Array_Sort  = 1
		WriteStatusToLogs "The key '" &ArrayKey& "' doesn't hold array."
		Exit function
	End if
	
	For i = 0 To UBound(tempArr) 
		DataList.Add tempArr(i)
	Next
	
	DataList.Sort()
	'DataList.Reverse()
	
	For i=0 To ubound(tempArr)
	  tempArr(i)=DataList(i)
	Next
	
	If err.number = 0 Then
		intval = AddItemToStorageDictionary(ArrayKey,tempArr)
		Array_Sort = 0
		WriteStatusToLogs "Execution is successful and key '" &ArrayKey& "' is stored with sorted array. Array size: '" & (ubound(tempArr)+1) &"' array items: ["& join(tempArr, " ^ ") &"]."
		else
		Array_Sort = 1
		WriteStatusToLogs "Error while sorting the array. "& err.description		
		
	End If
	
	On error goto 0
	
End Function

Function Array_Reverse(ArrayKey)
	On error resume next
	Dim strMethod
	Dim objRegEx, Match, Matches, StrReturnStr, intval, arrIndex
	Dim DataList
	
	err.clear
	strMethod = strKeywordForReporting

	Set DataList = CreateObject("System.Collections.ArrayList")

	If isnull(ArrayKey) or isnull(ArrayKey) Then       
		Array_Reverse  = 1
		WriteStatusToLogs "ArrayKey is null or empty."
		Exit Function
	End If
	
	tempArr = oDictStorage.item(ArrayKey)
	If not IsArray(tempArr) Then
		Array_Reverse  = 1
		WriteStatusToLogs "The key '" &ArrayKey& "' doesn't hold array."
		Exit function
	End if
	
	For i = 0 To UBound(tempArr) 
		DataList.Add tempArr(i)
	Next
	
	DataList.Reverse()
	'DataList.Reverse()
	
	For i=0 To ubound(tempArr)
	  tempArr(i)=DataList(i)
	Next
	
	If err.number = 0 Then
		intval = AddItemToStorageDictionary(ArrayKey,tempArr)
		Array_Reverse = 0
		WriteStatusToLogs "Execution is successful and key '" &ArrayKey& "' is stored with reversed array. Array size: '" & (ubound(tempArr)+1) &"' array items: ["& join(tempArr, " ^ ") &"]."
		else
		Array_Reverse = 1
		WriteStatusToLogs "Error while sorting the array. "& err.description		
		
	End If
	
	On error goto 0
	
End Function

function StoreMatchsFromRegExExecute (strMatchPattern, strPhrase, k)
	On error resume next
	Dim strMethod
	Dim objRegEx, Match, Matches, StrReturnStr, intval, arrIndex
	Dim arrPatrn()
	'Set myArrayList = CreateObject( "System.Collections.ArrayList" )
	
	strMethod = strKeywordForReporting
	err.clear	
	If isnull(strMatchPattern) or isnull(strPhrase) Then       
		StoreMatchsFromRegExExecute  = 1
		WriteStatusToLogs "Arguments are null."
		Exit Function
	End If
	
	If IsNull(k) Or IsEmpty(k ) or k = "" Then
		StoreMatchsFromRegExExecute = 1
		WriteStatusToLogs "key is null or empty."
		Exit Function
	End If

	'create instance of RegExp object
	Set objRegEx = New RegExp
	'set the pattern
	objRegEx.Pattern = strMatchPattern
	objRegEx.Global = True
	'create the collection of matches
	Set Matches = objRegEx.Execute(strPhrase)
	
	
	For each Match in Matches
		ReDim preserve arrPatrn(arrIndex)
		arrPatrn(arrIndex) = Match.value
		arrIndex = arrIndex+1
		'myArrayList.Add Match.value
	Next
	
	
	If err.number =0 Then
		intval = AddItemToStorageDictionary(k,arrPatrn)
		StoreMatchsFromRegExExecute = 0
		WriteStatusToLogs "Execution is successful and key '" &k& "' is stored with array of matching data. Array size: '" & Matches.count &"' array items: ["& join(arrPatrn, " ^ ") &"]."
	Else
		StoreMatchsFromRegExExecute = 1
		WriteStatusToLogs "Error occurred while finding matches. " & err.description
	
	End If
	On error goto 0
End function 



Function SystemUtil_Run(ByVal strPath, ByVal ApplicationParams, ByVal DefaultDir, ByVal Operation, ByVal Mode)
    On Error Resume Next
   'Variable Section Begin
	
	Dim strMethod 
	Dim WshShell, oExec
	Dim blnstatus
	Dim customMsg
	Dim intStatus

	'Variable Section End

	 
	strMethod = strKeywordForReporting
	
	if isnull(ApplicationParams) then
		ApplicationParams = ""
	End if
	
	if isnull(DefaultDir) then
		DefaultDir = ""
	End if
	
	if isnull(Operation) then
		Operation = ""
	End if
	
	if isnull(Mode) then
		Mode = ""
	End if
	
	

	If (Isnull(strPath) or Isempty(strPath) or Trim(strPath)="") Then
		SystemUtil_Run = 1
		WriteStatusToLogs "The path is invalid, please verify application path."
		Exit function
	End If 
	

'	If DefaultDir<>"" and 	FileExist(DefaultDir) Then
'		SystemUtil_Run = 1
'		WriteStatusToLogs "Method Name: "&strMethod&vbtab&_
'							"Status:" &  "Failed" &Vbtab&_
'							"Message:" & "The path is invalid, please verify default directory."
'		Exit function
'	End If 
	
	'msgbox Mode
	'If not (Mode<>"" and IsNumeric(cint(Mode))) Then
	'	SystemUtil_Run = 1
	'	WriteStatusToLogs "Method Name: "&strMethod&vbtab&_
	'						"Status:" &  "Failed" &Vbtab&_
	'						"Message:" & "Mode is not vaild, please verify application mode."
	'	Exit function
	'End If 
	
	blnstatus = False
	If Mode = "" Then
		If Operation = "" Then
			If DefaultDir = "" Then
				If ApplicationParams = "" Then
					SystemUtil.Run strPath
				Else
					SystemUtil.Run strPath, ApplicationParams
				End If
			Else
				SystemUtil.Run strPath, ApplicationParams, DefaultDir
			End If
		else
			SystemUtil.Run strPath, ApplicationParams, DefaultDir, Operation
		End If
	else
		SystemUtil.Run strPath, ApplicationParams, DefaultDir, Operation, cint(Mode)
	End If
	
	
	
	If Err.Number = 0  Then
	
		SystemUtil_Run = STATUS_PASSED
		intStatus = STATUS_PASSED
		customMsg = "Application '"&Trim(strPath)&"' opened successfully."
	Else
		SystemUtil_Run =STATUS_FAILED
		intStatus = STATUS_FAILED
		customMsg = " Error occurred while attempting to open the Application. "&Err.description
	End If

	Call writeActionStatusToLog(intStatus,customMsg)
			
	
	On Error Goto 0
End Function

'Internal
Function ReadAsciiMapping(oDictAscii)
	On error resume next
    If oDictAscii.Count<1 Then
		Dim asciiFilePath, objFileToRead, strFileText
		asciiFilePath = BasePath & "\Config\AsciiMapping.txt"
		Set objFileToRead = CreateObject("Scripting.FileSystemObject").OpenTextFile(asciiFilePath,1)
		'strFileText = objFileToRead.ReadAll()
		
		Do until objFileToRead.AtEndOfLine
		
			txtLine = objFileToRead.ReadLine
			arrtxtLine= split(txtLine, ",")
			
			If ubound(arrtxtLine) = 1 Then
				oDictAscii.Add arrtxtLine(0), arrtxtLine(1)
			End If
			
			'print txtLine
		loop
		
		objFileToRead.Close
		Set objFileToRead = Nothing
		If err.number <> 0 Then
			ReadAsciiMapping = 1
		Else
			ReadAsciiMapping = 0
		End If
	else
		ReadAsciiMapping = 0	
    End If
    On error goto 0
End  function


Function DeviceReplay_PressKey (KeyName)
	 On Error Resume Next
   'Variable Section Begin
	
	Dim strMethod 
	Dim myDeviceReplay, intStatus, customMsg, AskiiCode
	Dim oDictAscii 'Dictionary for (suiteprop, Suite Attribute), basically stores Suitename,Suite Schedule Name
    Set oDictAscii = CreateObject("Scripting.Dictionary")
 	
	strMethod = strKeywordForReporting

	
	 If oDictAsciiMapping.Count<1 Then
	 	LoadFunctionLibrary BasePath & "\Config\AsciiMapping.vbs"
	 End if
	
	AskiiCode = GetKeyCode(KeyName)
	
	If AskiiCode  =  "-1" Then
		DeviceReplay_PressKey = 1
		WriteStatusToLogs "Please select the correct key to be pressed. Go to action documentation to get the list of supported keys."
			 Exit function	
	End If
	
	If Err.Number <> 0  Then
		DeviceReplay_PressKey = 1
		WriteStatusToLogs "Invalid arguments. " & err.description
			 Exit function
	End If
	
	Set myDeviceReplay = CreateObject("Mercury.DeviceReplay")
	'AskiiCode = Asc(KeyName)
	myDeviceReplay.PressKey cint(AskiiCode)
	'Execute ("myDeviceReplay.PressKey ("&AskiiCode&")")
	
	If Err.Number = 0  Then
	
		DeviceReplay_PressKey = STATUS_PASSED
		intStatus = STATUS_PASSED
		customMsg = "TypeKey is successful for the keyboard input: '"&KeyName&"'"
	Else
		DeviceReplay_PressKey =STATUS_FAILED
		intStatus = STATUS_FAILED
		customMsg = " Error occurred during PressKey. "&Err.description
	End If

	Call writeActionStatusToLog(intStatus,customMsg)
		
End Function


Function DeviceReplay_SendString (StrData)
	On Error Resume Next
   'Variable Section Begin
	
	Dim strMethod 
	Dim myDeviceReplay, intStatus, customMsg, AskiiCode
 
	strMethod = strKeywordForReporting

	Set myDeviceReplay = CreateObject("Mercury.DeviceReplay")
	myDeviceReplay.SendString StrData
	
	If Err.Number = 0  Then
	
		DeviceReplay_SendString = STATUS_PASSED
		intStatus = STATUS_PASSED
		customMsg = "Keyboard input value '" &StrData&"' is typed successfully."
	Else
		DeviceReplay_SendString =STATUS_FAILED
		intStatus = STATUS_FAILED
		customMsg = " Error occurred during send string. "&Err.description
	End If

	Call writeActionStatusToLog(intStatus,customMsg)
		
	On error goto 0
End Function


Function DeviceReplay_DblClick (x, y, button)
	 On Error Resume Next
   'Variable Section Begin
	
	Dim strMethod 
	Dim myDeviceReplay, intStatus, customMsg, AskiiCode, btn
 
	strMethod = strKeywordForReporting
	
	If not (IsNumeric(x) and cint(x)>0 ) Then       
		DeviceReplay_DblClick = 1
		WriteStatusToLogs "invalid x coordinate. please enter a valid integer."
		Exit Function
	End If
	
	If not (IsNumeric(y) and cint(y)>0 ) Then       
		DeviceReplay_DblClick = 1
		WriteStatusToLogs "invalid y coordinate. please enter a valid integer."
		Exit Function
	End If
	
	If lcase(trim(button)) = "left" Then
	btn = "LEFT_MOUSE_BUTTON"
	ElseIf lcase(trim(button)) = "right"  Then
	btn = "RIGHT_MOUSE_BUTTON"
	ElseIf lcase(trim(button)) = "middle"  Then
	btn = "MIDDLE_MOUSE_BUTTON"
	else
		DeviceReplay_DblClick = 1
		WriteStatusToLogs "invalid button. Please select left,right, middle"
		Exit Function	
	End If
	
	Set myDeviceReplay = CreateObject("Mercury.DeviceReplay")
	'myDeviceReplay.MouseDblClick x,y,btn
	Execute "myDeviceReplay.MouseDblClick "& x &"," & y & "," &btn 
	
	If Err.Number = 0  Then
		DeviceReplay_DblClick = STATUS_PASSED
		intStatus = STATUS_PASSED
		customMsg = "DblClick is successful."
	Else
		DeviceReplay_DblClick =STATUS_FAILED
		intStatus = STATUS_FAILED
		customMsg = "Error occurred during DblClick. "&Err.description
	End If

	Call writeActionStatusToLog(intStatus,customMsg)

End Function

Function ClipboardClear()
	On Error Resume Next
   'Variable Section Begin
	
	Dim strMethod 
	Dim myDeviceReplay, intStatus, customMsg, AskiiCode
 
	strMethod = strKeywordForReporting
	
	Set clipboard = createobject("mercury.clipboard")
	clipboard.Clear
	
	If Err.Number = 0  Then
	
		ClipboardClear = STATUS_PASSED
		intStatus = STATUS_PASSED
		customMsg = "Clipboard is cleared successfully."
	Else
		ClipboardClear =STATUS_FAILED
		intStatus = STATUS_FAILED
		customMsg = "Error occurred while doing Clipboard clear."&Err.description
	End If

	Call writeActionStatusToLog(intStatus,customMsg)
		
	On error goto 0
End Function

Function ClipboardCopy(text)
	On Error Resume Next
   'Variable Section Begin
	
	Dim strMethod 
	Dim myDeviceReplay, intStatus, customMsg, AskiiCode
 
	strMethod = strKeywordForReporting
	
	Set clipboard = createobject("mercury.clipboard")
	clipboard.Clear
	clipboard.SetText (text)
	
	If Err.Number = 0  Then
	
		ClipboardCopy = STATUS_PASSED
		intStatus = STATUS_PASSED
		customMsg = "Data is copied to clipboard."
	Else
		ClipboardCopy =STATUS_FAILED
		intStatus = STATUS_FAILED
		customMsg = "Error occurred while coping data to clipboard."
	End If

	Call writeActionStatusToLog(intStatus,customMsg)
		
	On error goto 0
End Function

Function StoreClipboardData(key)
	On Error Resume Next
   'Variable Section Begin
	
	Dim strMethod 
	Dim myDeviceReplay, intStatus, customMsg, data
 
	strMethod = strKeywordForReporting
	
	Set clipboard = createobject("mercury.clipboard")
	data = clipboard.GetText 
	Call AddItemToStorageDictionary(key,data)
	If Err.Number = 0  Then
	
		StoreClipboardData = 0
		WriteStatusToLogs "Clipboard data is successfully stored in the key '"&key&"'."
	Else
		StoreClipboardData =1
		WriteStatusToLogs "Failed to store clipboard data: " & err.description
	End If
		
	On error goto 0
End Function
