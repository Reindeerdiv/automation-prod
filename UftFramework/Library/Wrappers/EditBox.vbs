'Function CompareEditBoxDefaultValue(ByVal objEditbox , ByVal strExpValue)
'Function SetValueInEditBox(ByVal objEditbox , ByVal strValue)
'Function VerifySetLength(ByVal objEditbox , ByVal strValue)
'Function VerifyEditBoxValue(ByVal objEditbox , ByVal strExpValue)
'Function VerifyEditBoxValueLength(ByVal objEditbox, ByVal intMaxLength , ByVal intMinLength)
'Function VerifyNumericDataInEditBox(ByVal objEditbox)
'Function VerifyStringDataInEditBox(ByVal objEditbox)
'Function VerifyEditBoxEnabled(ByVal objEditBox)
'Function VerifyEditBoxDisabled(ByVal objEditBox)
'Function VerifyEditBoxVisible(ByVal objEditBox)
'Function VerifyEditBoxInVisible(ByVal objEditBox)
'Function VerifyEditBoxExist(ByVal objEditBox)
'Function VerifyEditBoxNotExist(ByVal objEditBox)


'=======================Code Section Begin=============================================

Option Explicit		'Forces explicit declaration of all variables in a script. 
'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name :				 CompareEditBoxDefaultValue()
' 	Purpose :						Compares the EditBox Default value with our expected value.
' 	Return Value :		 		  Integer(0/1/2)
' 	Arguments :
'		Input Arguments :  		objEditbox , strExpValue
'		Output Arguments :     
'	Function is called by :
'	Function calls :				CheckObjectExist(),CompareStringValues(),
'	Created on :				   12/03/2008
'	Author :						 Rathna Reddy 
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function CompareEditBoxDefaultValue(ByVal objEditbox , ByVal strExpValue)
	On Error Resume Next
		'Variable Section Begin
			 Dim strMethod
			 Dim strActValue

		 'Variable Section End
		 strMethod = strKeywordForReporting
			If CheckObjectExist(objEditbox) Then
			   strActValue=objEditbox.GetRoProperty("default value")
			        If vartype(strActValue)<>VbEmpty Then
						If CompareStringValues(strExpValue, strActValue)=0  Then   
							CompareEditBoxDefaultValue = 0
							  WriteStatusToLogs """"&"Expected Default Value : "&strExpValue& """" &  " and " &""""&"Actual Default Value : "&strActValue&""""&" matches as expected."

					    Else
							CompareEditBoxDefaultValue = 2
							 WriteStatusToLogs """"&"Expected Default Value : "&strExpValue& """" &  " and " &""""&"Actual Default Value : "&strActValue&""""&" does not match, please verify."

						End If	  
				Else
				 CompareEditBoxDefaultValue = 1	
					WriteStatusToLogs "EditBox default value is empty, please verify."
				End If
			Else
					    CompareEditBoxDefaultValue = 1	
						WriteStatusToLogs "The EditBox does not exist, please verify."
			End If
			
	On Error GoTo 0
End Function
'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name :				 SetValueInEditBox()
' 	Purpose :						 Enters a value into editbox
' 	Return Value :		 		   Integer(0/1)
' 	Arguments :
'		Input Arguments :  		objEditbox , strValue
'		Output Arguments : 
'	Function is called by :
'	Function calls :			    CheckObjectExist()
'	Created on :				   12/03/2008
'	Author :						 Rathna Reddy 
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function SetValueInEditBox(ByVal objEditbox , ByVal strValue)
	On Error Resume Next
		'Variable Section Begin
			
				Dim strMethod

		'Variable Section End
		strMethod = strKeywordForReporting
			   If CheckObjectExist(objEditbox) Then
					If CheckObjectEnabled(objEditBox) Then
						If CheckObjectVisible(objEditBox) Then 
							 objEditbox.Click
'							 MsgBox "click in edit"
							 objEditbox.Set strValue
							 If err.Number = 0 Then
									SetValueInEditBox = 0
									WriteStatusToLogs "The value: "&strValue&" is set in EditBox Successfully."
							 Else
									SetValueInEditBox = 1
									WriteStatusToLogs "The value: "&strValue&" is not set in EditBox, please verify."
							  
							 End If
						Else
							SetValueInEditBox = 1
							WriteStatusToLogs "The EditBox is not Visible, please verify."

						End If
					Else
					SetValueInEditBox = 1
					WriteStatusToLogs "The EditBox is not Enabled, please verify."


					End If
				  
				Else
						SetValueInEditBox = 1
						WriteStatusToLogs "The EditBox does not exist, please verify."
				End If
	On Error GoTo 0
End Function




'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name :				 VerifySetLength()
' 	Purpose :						 Enters a value into editbox
' 	Return Value :		 		   Integer(0/1)
' 	Arguments :
'		Input Arguments :  		objEditbox , strValue
'		Output Arguments : 
'	Function is called by :
'	Function calls :			    CheckObjectExist()
'	Created on :				   12/03/2008
'	Author :						 Rathna Reddy 
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function VerifySetLength(ByVal objEditbox , ByVal strValue, ByVal intMaxValue)
	On Error Resume Next
		'Variable Section Begin
			Dim strMethod
			Dim blnExpError
			blnExpError = False
	
		'Variable Section End
		strMethod = strKeywordForReporting
			   If CheckObjectExist(objEditbox) Then
					If CheckObjectEnabled(objEditBox) Then
						If CheckObjectVisible(objEditBox) Then 
							 objEditbox.Set strValue
							 If CInt(Len(strValue)) > CInt(intMaxValue) Then
								blnExpError	= true
							 End If
							
							 If err.Number = 0 And blnExpError = False Then
									VerifySetLength = 0
									WriteStatusToLogs "The value: "&strValue&" is set in EditBox Successfully."
							 Else
							 'MsgBox "err = "&Err.description
								If blnExpError And err.Number = -2147024809 Then
									Err.clear
									VerifySetLength = 0
									WriteStatusToLogs "The value: "&strValue&" is not allowed to be set in EditBox as the length: "&Len(strValue)&" exceeds the max allowed limit = "&intMaxValue&"."
								Else
									If blnExpError And  err.Number = 0 Then
									VerifySetLength = 2
									WriteStatusToLogs "The value: "&strValue&" is allowed to set in EditBox which is more than the maximum limit. The actual length: "&Len(strValue)&" and the max allowed limit: "&intMaxValue&". Please verify."
											
									Else
										VerifySetLength = 1
									WriteStatusToLogs "Error occured while setting value in EditBox. Error Description: "&Err.description&". Please verify."
									End If
								End If
							 End If
						Else
							VerifySetLength = 1
							WriteStatusToLogs "The EditBox is not Visible, please verify."

						End If
					Else
					VerifySetLength = 1
					WriteStatusToLogs "The EditBox is not Enabled, please verify."


					End If
				  
				Else
						VerifySetLength = 1
						WriteStatusToLogs "The EditBox does not exist, please verify."
				End If
	On Error GoTo 0
End Function

'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name :				VerifyEditBoxValue()
' 	Purpose :					 Verifies the editbox value with our expected whenever required
' 	Return Value :		 		  Integer(0/1/2)
' 	Arguments :
'		Input Arguments :  		objEditbox,strExpValue
'		Output Arguments :     strActValue
'	Function is called by :
'	Function calls :				CheckObjectExist(),CompareStringValues()
'	Created on :				   12/03/2008
'	Author :						 Rathna Reddy 
'Note: This is not default value verification.
'------------------------------------------------------------------------------------------------------------------------------------------
Function VerifyEditBoxValue(ByVal objEditbox , ByVal strExpValue)
	On Error Resume Next
		 'Variable Section Begin
			Dim strMethod
			Dim strActValue

		'Variable Section End
		strMethod = strKeywordForReporting
			If CheckObjectExist(objEditbox) Then  
				strActValue = objEditbox.GetROProperty("Value")
				
				
				
					    If CompareStringValues(strExpValue,strActValue)  = 0 Then
							VerifyEditBoxValue = 0
							WriteStatusToLogs """"&"Expected Value : "&strExpValue& """" &  " and " &""""&"Actual Value : "&strActValue&""""&" matches as expected."

						Else
							VerifyEditBoxValue = 2
							WriteStatusToLogs """"&"Expected Value : "&strExpValue& """" &  " and " &""""&"Actual Value : "&strActValue&""""&" does not match, please verify."

						End If
			Else
					VerifyEditBoxValue = 1
					 WriteStatusToLogs 	"EditBox does not exist, please verify."
					
             End If
			'WriteStepResults CompareEditBoxValue
	On Error GOTO 0
End Function
'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name :				 VerifyEditBoxValueLength()
' 	Purpose :						Verifies the both minimum and maximum length of editbox Data
' 	Return Value :		 		  Integer(0/1/2)
' 	Arguments :
'		Input Arguments :  	    objEditbox , intMaxLength , intMinLength
'		Output Arguments :       intLength, strValue
'	Function is called by :
'	Function calls :				CheckObjectExist() , IsInteger()
'	Created on :				   12/03/2008
'	Author :						 Rathna Reddy 
'' 
'Note: if testcase does't specify the minimum length then Give "1"
'------------------------------------------------------------------------------------------------------------------------------------------

Function VerifyEditBoxValueLength(ByVal objEditbox, ByVal intMaxLength , ByVal intMinLength)
On Error Resume Next
	'Variable Section Begin
		Dim strMethod
		Dim intLength, strValue
		Dim intMaxIntStat , intMinIntStat

	'Variable Section End
	strMethod = strKeywordForReporting
	intMaxIntStat = IsInteger(intMaxLength)
	intMinIntStat = IsInteger(intMinLength)

	If intMaxIntStat = 0 And intMinIntStat = 0 Then

		If intMaxLength >0 And intMinLength >0 Then
			
			If intMinLength<=intMaxLength Then
				If CheckObjectExist(objEditbox) Then

					strValue= objEditbox.GetROProperty("Value")
					If err.Number = 0 Then

						intLength = Len(strValue)
						If Cint( intLength) <=Cint( intMaxLength)  And Cint(intMinLength) <=Cint( intLength)  Then
							VerifyEditBoxValueLength = 0
							WriteStatusToLogs "The Edit Box value length "&""""&intLength&""""&" is in between the Max length :"&""""& intMaxLength &""""&" and  Min Length:" &""""& intMinLength &""""
						Else
							 VerifyEditBoxValueLength = 2
							 WriteStatusToLogs "The Edit Box value length "&""""&intLength&""""&" is not  in between Max length :"&""""& intMaxLength &""""&" and Min Length:" &""""& intMinLength &""""
						End If

					Else
						VerifyEditBoxValueLength = 1
						WriteStatusToLogs "Action fails to get EditBox value, please verify."

					End If


				Else
					VerifyEditBoxValueLength = 1
					WriteStatusToLogs "The specified Edit Box does not exist, please verify.."
				End if
			Else
				VerifyEditBoxValueLength = 1
				WriteStatusToLogs "The Minimum length Should be less than Maximum Length."
			End If
			
		ElseIf intMaxLength <1 And intMinLength <1 Then
			VerifyEditBoxValueLength = 1
			WriteStatusToLogs "Given Maximum and Minimum value a should not be less than 1, please verify."
		ElseIf intMaxLength <1Then
			VerifyEditBoxValueLength = 1
			WriteStatusToLogs "Given Maximum value a should not be less than 1, please verify."
		ElseIf intMinLength <1 Then
			VerifyEditBoxValueLength = 1
			WriteStatusToLogs "Given Minimum value a should not be less than 1, please verify."
		
		End If
		
	ElseIf intMaxIntStat =  1 And intMinIntStat = 1 Then
			VerifyEditBoxValueLength = 1
			WriteStatusToLogs "Given Maximum and Minimum value are not Natural Numbers, please verify."
	ElseIf intMaxIntStat = 1 Then
		VerifyEditBoxValueLength = 1
		WriteStatusToLogs "Given Maximum value is not a Natural Number , please verify."
	ElseIf intMinIntStat = 1 Then
		VerifyEditBoxValueLength = 1
		WriteStatusToLogs "Given Manimum value is not a Natural Number, please verify."
	End if
			
On Error GoTo 0

End Function

'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name :				 VerifyNumericDataInEditBox()
' 	Purpose :						Verifies whether the editbox allows the numeric data by avoiding string data.
' Possible values for Arguement:
'   			strFlags:  "TRUE" or "FALSE"
'				Purpose of strFlag: if the strFlag is "TRUE" then whether the edit box can allow the numeric values. or else you are checking the edit box validation like for validating numeric data.
' 	Return Value :		 		  Integer(0/1/2)
' 	Arguments :
'		Input Arguments :  		objEditbox	
'		Output Arguments : 		intValue
'	Function is called by :
'	Function calls :				CheckObjectExist()
'	Created on :				   12/03/2008
'	Author :						 Rathna Reddy 
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function VerifyNumericDataInEditBox(ByVal objEditbox)
	On Error Resume Next
		 'Variable Section Begin
			  Dim strMethod
			  Dim intValue

			'Variable Section End
			strMethod = strKeywordForReporting
				 If CheckObjectExist(objEditbox) Then   
                    
						 intValue = objEditbox.GetRoProperty("Value")
						 If  VarType(intValue)<>VbEmpty Then
						       intValue=cdbl(intValue)
							If VarType(intValue)=VbDouble Then
                                 VerifyNumericDataInEditBox = 0
									 WriteStatusToLogs "EditBox contains Numeric value as expected."
								 Else
									VerifyNumericDataInEditBox = 2  
									WriteStatusToLogs "EditBox does not contain Numeric value, please verify."
								 End If
                                 
								 
							Else
								VerifyNumericDataInEditBox = 1
								WriteStatusToLogs  "EditBox is empty, please verify."
						
							End if

				Else
						VerifyNumericDataInEditBox = 1
						WriteStatusToLogs "EditBox does not exist, please verify."
				End If
	On Error GoTo 0
End Function


'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name :				 VerifyStringDataInEditBox()
' 	Purpose :						Verifies whether the editbox allows the string data by avoiding numeric data.
' Possible values for Arguement:
'   			strFlags:  "TRUE" or "FALSE"
'				Purpose of strFlag: if the strFlag is "TRUE" then whether the edit box can allow the String values. or else you are checking the edit box validation like for validating string data.
' 	Return Value :		 		  Integer(0/1/2)
' 	Arguments :
'		Input Arguments :  		objEditbox
'		Output Arguments : 	   strValue
'	Function is called by :
'	Function calls :				CheckObjectExist()
'	Created on :				   12/03/2008
'	Author :						 Rathna Reddy 
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function VerifyStringDataInEditBox(ByVal objEditbox)
	On Error Resume Next
		 'Variable Section Begin
				Dim strMethod
			   Dim strValue
			'Variable Section End
			strMethod = strKeywordForReporting
				 If CheckObjectExist(objEditbox) Then   
						strValue = objEditbox.GetRoProperty("Value")
						 
					      If  VarType(strValue)<>VbEmpty Then
								If IsNumeric(strValue) Then
									VerifyStringDataInEditBox = 2 
									WriteStatusToLogs "EditBox does not contain alphanumeric value, please verify."
							 
								Else
									  VerifyStringDataInEditBox = 0
									  WriteStatusToLogs "EditBox contains alphanumeric value as expected"
								End If
								
							
						  Else
								VerifyStringDataInEditBox = 1
								WriteStatusToLogs  "EditBox is empty, please verify."
						
						  End if
				Else
						VerifyStringDataInEditBox = 1  
						WriteStatusToLogs "EditBox does not exist, please verify."
			End If
	On Error GoTo 0
End Function
'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name :				 VerifyEditBoxEnabled()
' 	Purpose :					 Verifies whether the EditBox is Enabled
' 	Return Value :		 		  Integer(0/1/2)
' 	Arguments :
'		Input Arguments :  		objEditBox 
'		Output Arguments : 
'	Function is called by :
'	Function calls :				CheckObjectExist() , CheckObjectEnabled()
'	Created on :				   11/03/2008
'	Author :						  Rathna Reddy 
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function VerifyEditBoxEnabled(ByVal objEditBox)

On Error Resume Next
	'Variable Section Begin
		
		Dim strMethod

	'Variable Section End
	strMethod = strKeywordForReporting
		If CheckObjectExist(objEditBox) Then
			If CheckObjectEnabled(objEditBox) Then
					VerifyEditBoxEnabled = 0
					WriteStatusToLogs "The EditBox is enabled as expected."
				Else
					VerifyEditBoxEnabled = 2
					WriteStatusToLogs "The EditBox is not enabled, please verify."
				End If
            
		Else
				VerifyEditBoxEnabled = 1
				WriteStatusToLogs "The EditBox does not exist, please verify."
		End if
		
	On Error GoTo 0
    
End Function

'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name :				 VerifyEditBoxDisabled()
' 	Purpose :					 Verifies whether the EditBox is Disabled
' 	Return Value :		 		  Integer(0/1/2)
' 	Arguments :
'		Input Arguments :  		objEditBox 
'		Output Arguments : 
'	Function is called by :
'	Function calls :				CheckObjectExist() , CheckObjectStatus()
'	Created on :				   11/03/2008
'	Author :						  Rathna Reddy 
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function VerifyEditBoxDisabled(ByVal objEditBox)

On Error Resume Next
	'Variable Section Begin
		
		Dim strMethod

	'Variable Section End
	strMethod = strKeywordForReporting
		If CheckObjectExist(objEditBox) Then
			If CheckObjectDisabled(objEditBox) Then
					VerifyEditBoxDisabled = 0
					WriteStatusToLogs "The EditBox is disabled as expected."
				Else
					VerifyEditBoxDisabled = 2
					WriteStatusToLogs "The EditBox is not disabled, please verify."
				End If
            
		Else
				VerifyEditBoxDisabled = 1
				WriteStatusToLogs "The EditBox does not exist, please verify."
		End if
	On Error GoTo 0
End Function

'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :				 VerifyEditBoxVisible()
'	Purpose :					 Verifies whether the EditBox is visible.
'	Return Value :		 		  Integer(0/1/2)
'	Arguments :
'		Input Arguments :  		  objEditBox 
'		Output Arguments : 
'	Function is called by :
'	Function calls :			 CheckObjectExist() , CheckObjectVisiblity()
'	Created on :				   10/03/2008
'	Author :						  Rathna Reddy 
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function VerifyEditBoxVisible(ByVal objEditBox)
	On Error Resume Next
		'Variable Section Begin
		
			Dim strMethod

		'Variable Section End
		strMethod = strKeywordForReporting
			If CheckObjectExist(objEditBox)  Then 
				If CheckObjectVisible(objEditBox) Then
					VerifyEditBoxVisible = 0
					WriteStatusToLogs "The EditBox is visible as expected."
				Else
					VerifyEditBoxVisible = 2
					WriteStatusToLogs "The EditBox is not visible,Please verify."
				End If
			Else
					VerifyEditBoxVisible = 1
					WriteStatusToLogs "The EditBox does not exist, please verify."
			End If
		
	On Error GoTo 0
End Function
'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :				 VerifyEditBoxInVisible()
'	Purpose :					 Verifies whether the EditBox is Invisible.
'	Return Value :		 		  Integer(0/1/2)
'	Arguments :
'		Input Arguments :  		  objEditBox 
'		Output Arguments : 
'	Function is called by :
'	Function calls :			 CheckObjectExist() , CheckObjectVisiblity()
'	Created on :				   10/03/2008
'	Author :						  Rathna Reddy 
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function VerifyEditBoxInVisible(ByVal objEditBox)
	On Error Resume Next
		'Variable Section Begin
		
			Dim strMethod

		'Variable Section End
		strMethod = strKeywordForReporting
			If CheckObjectExist(objEditBox)  Then 
				If CheckObjectInVisible(objEditBox) Then
					VerifyEditBoxInVisible = 0
					WriteStatusToLogs "The EditBox is not visible as expected."
				Else
					VerifyEditBoxInVisible = 2
					WriteStatusToLogs "The EditBox is visible, please verify."
				End If
			Else
					VerifyEditBoxInVisible = 1
					WriteStatusToLogs "The EditBox does not exist, please verify."
			End If
		
	On Error GoTo 0
End Function
'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :				 VerifyEditBoxExist()
'	Purpose :					 Verifies whether the EditBox Exists or not?
'	Return Value :		 		  Integer(0/2)
'	Arguments :
'		Input Arguments :  		  objEditBox 
'		Output Arguments : 
'	Function is called by :
'	Function calls :			 CheckObjectExist() 
'	Created on :				   10/03/2008
'	Author :						  Rathna Reddy 
'
'------------------------------------------------------------------------------------------------------------------------------------------

Function VerifyEditBoxExist(ByVal objEditBox)
	On Error Resume Next
		'Variable Section Begin
	
		  Dim strMethod

		'Variable Section End
		strMethod = strKeywordForReporting
			If CheckObjectExist(objEditBox) Then 
			   
					VerifyEditBoxExist = 0
					WriteStatusToLogs "The EditBox exists as expected."
					
			Else
					VerifyEditBoxExist = 2
					WriteStatusToLogs "The EditBox does NOT exist, please verify."
					
			End If 
	
	On Error GoTo 0
End Function

'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :				 VerifyEditBoxNotExist()
'	Purpose :					 Verifies whether the EditBox Exists or not?
'	Return Value :		 		  Integer(0/2)
'	Arguments :
'		Input Arguments :  		  objEditBox 
'		Output Arguments : 
'	Function is called by :
'	Function calls :			 CheckObjectExist() 
'	Created on :				   10/03/2008
'	Author :						  Rathna Reddy 
'
'------------------------------------------------------------------------------------------------------------------------------------------

Function VerifyEditBoxNotExist(ByVal objEditBox)
	On Error Resume Next
		'Variable Section Begin
	
		   Dim strMethod

		'Variable Section End
			strMethod = strKeywordForReporting
		
			If CheckObjectNotExist(objEditBox) Then 
			   
					VerifyEditBoxNotExist = 0
					WriteStatusToLogs "The Edit Box does not exist as expected."
					
			Else
					VerifyEditBoxNotExist = 2
					WriteStatusToLogs "The Edit Box exists, please verify."
					
			End If 
	
  		
	On Error GoTo 0
End Function

Function SetFocus(ByVal objEditBox)
	On Error Resume Next
			If CheckObjectExist(objEditBox) Then
					objEditbox.Object.focus
					If Err.number = 0 Then
						SetFocus = 0
						WriteStatusToLogs "Set focus in EditBox Successfully"
					Else						
						SetFocus = 1
						WriteStatusToLogs "Not set focus in EditBox, please verify."
					End If

					
			Else
				SetFocus = 2
				WriteStatusToLogs "The Edit Box does not exist, please verify."				
					
			End If 	
  		
	On Error GoTo 0
End Function
'=======================Code Section End=============================================
