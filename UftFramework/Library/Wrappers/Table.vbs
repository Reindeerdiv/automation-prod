Option Explicit


'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name :				 VerifyDataExistInColumn()
' 	Purpose :					 Verifies the expected data in table column
'	Return Value :		 		  Integer( 0/1/2)
'   Arguements:	
'		Input Arguments :  		 objTable,intColNumber,strData
'		Output Arguments :       intRows, strActData
'	Function is called by :
'	Function calls :				CheckObjectExist()
'	Created on :				   21/01/2009
'	Author :						 Rathna Reddy 
'
'------------------------------------------------------------------------------------------------------------------------------------------

Function VerifyDataExistInColumn (Byval objTable,Byval intColNumber , Byval strData)
On Error Resume Next

	'Variable Section Begin

		Dim strMethod	
		Dim intLoopRow , intLoopCol			
		Dim intRowCount , intColumnCount  
		Dim strActCellData , blnFound , intRowFound
		blnFound = false
		
	'Variable Section End

	strMethod = strKeywordForReporting

	If IsPositiveInteger(intColNumber) = 0 Then
		If CheckObjectExist(objTable)  Then
			intRowCount = objTable.RowCount 
			If intRowCount > 0 Or Not IsEmpty(intRowCount) Or Not IsNull(intRowCount)Then
				
				For intLoopRow = 1 To intRowCount
					intColumnCount = objTable.ColumnCount(intLoopRow)	
					If Err.number = 0 Then
						If Cint(intColumnCount) >= Cint(intColNumber) Then
							strActCellData = objTable.GetCellData(intLoopRow,intColNumber)	
							If Trim(strActCellData) = Trim(strData) Then
								blnFound = True
								intRowFound = intLoopRow
								Exit for
							End if
						End If

					Else
						VerifyDataExistInColumn = 1
						WriteStatusToLogs "An error occurred while capturing the table cell data, please verify."	
						Exit function
					End If
					
				Next

				If blnFound = True Then
					VerifyDataExistInColumn = 0
					WriteStatusToLogs """"&strData&""""" exists in Table column number:"&intColNumber&" and table row number:"&intRowFound
						
				Else
					VerifyDataExistInColumn = 1
					WriteStatusToLogs """"&strData&""""" does not exist in Table column number:"&intColNumber&" , please verify."
						
				End if

			Else
				VerifyDataExistInColumn = 1
				WriteStatusToLogs "The specified table does not have rows, please verify."
			End If
			
		Else
			VerifyDataExistInColumn = 1
			WriteStatusToLogs "The specified table does not exist, please verify."
			
		End If
		
	Else
		VerifyDataExistInColumn = 1
		WriteStatusToLogs "Given column data '"&intColNumber&"' is not a natural number, please verify."	
	End if

 
On Error GoTo 0  
End Function


'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name :				 VerifyDataNotExistInColumn()
' 	Purpose :					 Verifies the expected data in table column
'	Return Value :		 		  Integer( 0/1/2)
'   Arguements:	
'		Input Arguments :  		 objTable,intColNumber,strData
'		Output Arguments :       intRows, strActData
'	Function is called by :
'	Function calls :				CheckObjectExist()
'	Created on :				   21/01/2009
'	Author :						 Rathna Reddy 
'
'------------------------------------------------------------------------------------------------------------------------------------------

Function VerifyDataNotExistInColumn (Byval objTable,Byval intColNumber , Byval strData)
On Error Resume Next

	'Variable Section Begin
		
		Dim strMethod
		Dim intLoopRow , intLoopCol			
		Dim intRowCount , intColumnCount  
		Dim strActCellData , blnFound , intRowFound
		blnFound = false
		
	'Variable Section End

	strMethod = strKeywordForReporting

	If IsPositiveInteger(intColNumber) = 0 Then
		If CheckObjectExist(objTable)  Then
			intRowCount = objTable.RowCount 
			If intRowCount > 0 Or Not IsEmpty(intRowCount) Or Not IsNull(intRowCount)Then
				
				For intLoopRow = 1 To intRowCount
					intColumnCount = objTable.ColumnCount(intLoopRow)	
					If Err.number = 0 Then
						If Cint(intColumnCount) >= Cint(intColNumber) Then
							strActCellData = objTable.GetCellData(intLoopRow,intColNumber)	
							If Trim(strActCellData) = Trim(strData) Then
								blnFound = True
								intRowFound = intLoopRow
								Exit for
							End if
						End If

					Else
						VerifyDataNotExistInColumn = 1
						WriteStatusToLogs "An error occurred while capturing the table cell data, please verify."	
						Exit function
					End If
					
				Next

				If blnFound = True Then
					VerifyDataNotExistInColumn = 2
					WriteStatusToLogs """"&strData&""""" exists in Table column number:"&intColNumber&" and table row number:"&intRowFound&" ,Please verify"
						
				Else
					VerifyDataNotExistInColumn = 0
					WriteStatusToLogs """"&strData&""""" does not exist in Table column number:"&intColNumber
						
				End if

			Else
				VerifyDataNotExistInColumn = 1
				WriteStatusToLogs "The specified table does not have rows, please verify."
			End If
			
		Else
			VerifyDataNotExistInColumn = 1
			WriteStatusToLogs "The specified table does not exist, please verify."
			
		End If
		
	Else
		VerifyDataNotExistInColumn = 1
		WriteStatusToLogs "Given column data '"&intColNumber&"' is not a natural number, please verify."	
	End if

 
On Error GoTo 0  
End Function


'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name :				 VerifyTableRowCount ()
' 	Purpose :					 Verifies the table row count with our expected
'	Return Value :		 		  Integer( 0/1/2)
'   Arguements:	
'		Input Arguments :  		 objTable,intColNumber,strData
'		Output Arguments :       intRows, strActData
'	Function is called by :
'	Function calls :				CheckObjectExist()
'	Created on :				   21/01/2009
'	Author :						 Rathna Reddy 
'
'------------------------------------------------------------------------------------------------------------------------------------------

Function VerifyTableRowCount(Byval objTable,Byval intTotalRowCount )

   On Error Resume Next

    'Variable Section Begin
		
		 Dim strMethod	
		 Dim  intActRowCount,intTotalRows
         intTotalRows=0		 

	'Variable Section End

		 strMethod = strKeywordForReporting

		If intTotalRowCount = "" Then
			intActRowCount = 0
			WriteStatusToLogs "The expected row count data is empty. Hence its set to the default value '0'"

		ElseIf IsInteger(intTotalRowCount) <> 0 Then
			VerifyTableRowCount = 1
			WriteStatusToLogs  "The expected row count '" & intTotalRowCount & "' is not a valid integer, please verify."
			Exit Function

		Else
			intActRowCount = Cint(intTotalRowCount)
		End if

		If CheckObjectExist(objTable)  Then 		  
				 
				intTotalRows=objTable.RowCount
				If intTotalRows > 0 Then

					If CompareStringValues(intActRowCount,intTotalRows) = 0 Then
						VerifyTableRowCount  = 0
						WriteStatusToLogs """"&"Expected RowCount : "&intActRowCount& """" &  " and " &""""&"Actual RowCount : "&intTotalRows&""""&" match"
					Else
						VerifyTableRowCount  = 2
						WriteStatusToLogs """"&"Expected RowCount : "&intActRowCount& """" &  " and " &""""&"Actual RowCount : "&intTotalRows&""""&" does not match, please verify."

					
					End If

				Else
					VerifyTableRowCount  = 1
					WriteStatusToLogs 	"Could not capture total row count of table, please verify table properties."
				End If	  			
		Else
			VerifyTableRowCount  = 1
			WriteStatusToLogs 	"Specified Table does not exist, please verify."
		End If
		
 On Error GoTo 0  
End Function
'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name :				 CompareTableRowData()
' 	Purpose :					 Compares specified Row Data with given data in a specified table.
'	Return Value :		 		  Integer( 0/1/2)
'   Arguements:	
'		Input Arguments :  		 objTable,strExpRowName,strExpData,intColNum
'		Output Arguments :       arrActualdata, arrExpData,intRes
'	Function is called by :
'	Function calls :				CheckObjectExist(),GetTableRowData(),GetArray(),CompareArrays()
'	Created on :				   20/03/2008
'	Author :						 Rathna Reddy 
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function CompareTableRowData(ByVal objTable,  ByVal intRowNumber, ByVal arrData)
On Error Resume Next
	'Variable Section Begin

		 Dim strMethod
		 Dim arrActualdata, arrExpData,intRowNum,arrExpectedData,arrActData
		 Dim intRes
		 
				
	'Variable Section End

	strMethod = strKeywordForReporting

   If CheckObjectExist(objTable)  Then 
		If IsPositiveInteger(intRowNumber) = 0 Then
				intRowNum = Cint(intRowNumber)
				arrActualdata = GetTableRowData(objTable,intRowNum)
				If  Not IsNull(arrActualdata)  Then
					
						arrExpData = arrData 
						If Not IsNull (arrExpData) Then
							intRes = MatchEachArrData(arrExpData, arrActualdata)
							arrExpectedData= PrintArrayElements (arrExpData)
							arrActData = PrintArrayElements (arrActualdata)
							If intRes = True Then
							  
								CompareTableRowData = 0
								WriteStatusToLogs """"&"Expected Row Data: "&arrExpectedData& """" &  " and " &""""&"Actual Row Data : "&arrActData&""""&" are equal."

							Else
								CompareTableRowData = 2
								WriteStatusToLogs """"&"Expected Row Data : "&arrExpectedData& """" &  "   and   " &""""&"Actual Row Data : "&arrActData&""""&"  are not equal, please verify."

							End If

						Else
							CompareTableRowData = 1
							WriteStatusToLogs "Action Fails to store expected values in an array, please verify."  
						
						End If
				Else
					CompareTableRowData = 1
					WriteStatusToLogs "Action Fails to get table Row data, please verify."
				End If

		Else
			CompareTableRowData = 1
			WriteStatusToLogs "Given Row Number "&intRowNumber&" is not an integer please verify."

		End If

	Else
		CompareTableRowData = 1
		WriteStatusToLogs "Specified table does not exist, please verify."

	End If

On Error GoTo 0
End Function


'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name :				 CompareTableColumnData()
' 	Purpose :					 Compares specified Row Data with given data in a specified table.
'	Return Value :		 		  Integer( 0/1/2)
'   Arguements:	
'		Input Arguments :  		 objTable,strExpRowName,strExpData,intColNum
'		Output Arguments :       arrActualdata, arrExpData,intRes
'	Function is called by :
'	Function calls :				CheckObjectExist(),GetTableRowData(),GetArray(),CompareArrays()
'	Created on :				   20/03/2008
'	Author :						 Rathna Reddy 
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function CompareTableColumnData(ByVal objTable,  ByVal intColNumber, ByVal arrData)
On Error Resume Next
	'Variable Section Begin

		 Dim strMethod
		 Dim arrActualdata, arrExpData,intColNum,intTotalCols
		 Dim intRes
				
	'Variable Section End

	strMethod = strKeywordForReporting

	If CheckObjectExist(objTable)  Then 

			
		If IsPositiveInteger(intColNumber)= 0 Then

			intColNum = CInt(intColNumber)
			intTotalCols=objTable.ColumnCount(1)
			
			If intTotalCols>=intColNum and  intColNum>0 Then
					arrActualdata = GetTableColData(objTable,intColNum)
						If  Not IsNull(arrActualdata)  Then
							
							 arrExpData	= arrData
							 If Not IsNull (arrExpData) Then
								  intRes = MatchEachArrData(arrExpData, arrActualdata)
								  If intRes = True Then
										CompareTableColumnData = 0
										WriteStatusToLogs 	"Table column Data is same as expected."
							
									Else
										CompareTableColumnData = 2
										WriteStatusToLogs 	"Table column Data does not match with expected value."

							
									End If
						Else
							CompareTableColumnData = 1
							WriteStatusToLogs 	"Action Fails to store expected values in an array, please verify."  
						
						End If
				Else
					CompareTableColumnData = 1
					WriteStatusToLogs 	"Action Fails to get table column data, please verify."
				End If


			Else
				CompareTableColumnData = 1
				WriteStatusToLogs 	"Column Number :" &""""&intColNum&""""&" was not found in Table, please verify total number of columns in table"

			End If

		

		Else
			CompareTableColumnData = 1
			WriteStatusToLogs 	"Given column Number "&intColNumber&" is not an integer please verify."

		End If

	Else
		CompareTableColumnData = 1
		WriteStatusToLogs 	"Table does not exist, please verify."

	End If
 	
On Error GoTo 0
End Function

'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name :				 DeSelectCheckBoxInTableCell()
' 	Purpose :					Deselects the CheckBox in a specified table cell.
'	Return Value :		 		  Integer( 0/1)
'   Arguements:	
'		Input Arguments :  		 intRowNum,intColNum
'		Output Arguments :       objChildCheckbox,intRow,intCol
'	Function is called by :
'	Function calls :				GetChildItemCountInTable,GetChildItem
'	Created on :				   20/03/2008
'	Author :						 Rathna Reddy 
'
'------------------------------------------------------------------------------------------------------------------------------------------

Function DeSelectCheckBoxInTableCell (byval objTable, byval intRowNumber, byval intColNumber)
    On Error Resume Next
		'Variable Section Begin
		
			Dim strMethod
			Dim objChildCheckbox,intRow,intCol,intTotalRowCount,intTotalColCount

		'Variable Section End

		strMethod = strKeywordForReporting
        
		If IsInteger(intRowNumber) <> 0 Then
				DeSelectCheckBoxInTableCell = 1
				WriteStatusToLogs  "The given Row  Number "&intRowNumber&" is not an Integer, please verify. Row number starts from 1."
				Exit Function	 
		End If
		
		If IsInteger(intColNumber) <> 0 Then
				DeSelectCheckBoxInTableCell = 1
				WriteStatusToLogs  "The given Column  Number "&intColNumber&" is not an Integer, please verify. Column number starts from 1."
				Exit Function	 
		End if


		If CheckObjectExist(objTable) Then 
			intRow=Cint(intRowNumber)
			intCol=Cint(intColNumber)
			   
			intTotalRowCount=objTable.RowCount
            If intRow>0  and  intRow<=intTotalRowCount Then
				intTotalColCount=objTable.ColumnCount(intRow)
				If  intCol>0 and  intCol<=intTotalColCount Then

					If GetChildItemCountInTable(objTable, intRow, intCol, "WebCheckBox") > 0 Then

						set objChildCheckbox = GetChildItem(objTable, intRow, intCol, "WebCheckBox")
						If IsObject(objChildCheckbox) then
							If CheckObjectEnabled(objChildCheckbox) Then
								If  objChildCheckbox.GetRoProperty("checked") =1 Then
									objChildCheckbox.Set "Off"
									If err.Number = 0 Then
										 DeSelectCheckBoxInTableCell = 0
										 WriteStatusToLogs 	"CheckBox was deselected sucessfully."
									Else
										   DeSelectCheckBoxInTableCell = 1
										   WriteStatusToLogs 	"CheckBox could not get deselected, please verify."
									End If
								Else
									DeSelectCheckBoxInTableCell = 1
								    WriteStatusToLogs 	"The CheckBox was already in UnChecked state, please verify."

								End If

							Else
								DeSelectCheckBoxInTableCell = 1
							    WriteStatusToLogs 	"The CheckBox was not in enabled state, please verify."

							End If	 							 
										
						Else
							DeSelectCheckBoxInTableCell = 1
							WriteStatusToLogs 	"The table cell does not contain WebCheckBox, please verify."
						 
						End if 

					Else
						DeSelectCheckBoxInTableCell = 1
						WriteStatusToLogs 	"The specified table cell does not contain WebCheckBox, please verify."
					End If

				Else
					DeSelectCheckBoxInTableCell = 1
					WriteStatusToLogs 	"The given Column  Number does not exist in table, please verify"

				End If			 

			Else
				DeSelectCheckBoxInTableCell = 1
				WriteStatusToLogs 	"The given Row Number does not exist in table, please verify"

			End If
			
		Else
			DeSelectCheckBoxInTableCell = 1	
			WriteStatusToLogs 	"Table does not exist, please verify."
		End If
      
   On Error GoTo 0 
End Function

'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :				 VerifyTableExist()
'	Purpose :					 Verifies whether the Tableis Existed or not?.
'	Return Value :		 		  Integer( 0/2)
'	Arguments :
'		Input Arguments :  		  objTable
'		Output Arguments : 
'	Function is called by :
'	Function calls :			 CheckObjectExist() 
'	Created on :				   10/03/2008
'	Author :						  Rathna Reddy 
'
'------------------------------------------------------------------------------------------------------------------------------------------

Function VerifyTableExist(ByVal objTable)
	On Error Resume Next
		'Variable Section Begin
	
		   Dim strMethod

		'Variable Section End
		
		strMethod = strKeywordForReporting

			If CheckObjectExist(objTable) Then 
			   
					VerifyTableExist = 0
					WriteStatusToLogs "The Table Exist."
					
			Else
					VerifyTableExist = 2
					WriteStatusToLogs "The Table Does not Exist, please verify"
					
			End If 
	  


		
	On Error GoTo 0
End Function

'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :				 VerifyTableNotExist()
'	Purpose :					 Verifies whether the Tableis Existed or not?.
'	Return Value :		 		  Integer( 0/2)
'	Arguments :
'		Input Arguments :  		  objTable
'		Output Arguments : 
'	Function is called by :
'	Function calls :			 CheckObjectNotExist() 
'	Created on :				   10/03/2008
'	Author :						  Rathna Reddy 
'
'------------------------------------------------------------------------------------------------------------------------------------------

Function VerifyTableNotExist(ByVal objTable)
	On Error Resume Next
		'Variable Section Begin
	
		   Dim strMethod

		'Variable Section End

		strMethod = strKeywordForReporting
		
			If CheckObjectNotExist(objTable) Then 
			   
					VerifyTableNotExist = 0
					WriteStatusToLogs "The Table Does not Exist"
					
			Else
					VerifyTableNotExist = 2
					WriteStatusToLogs "The Table Exist, please verify."
					
			End If 
	
 
		
	On Error GoTo 0
End Function


'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :				 VerifyTableCellData()
'	Purpose :					 Verifies the table cell data with our expected
'	Return Value :		 		  Integer( 0/1/2)
'	Arguments :
'		Input Arguments :  		  objTable,intRowNum,intColNum,strExpData
'		Output Arguments :      intRowNumber, intColNumber,intTotalRows,intTotalCols,strActData
'	Function is called by :
'	Function calls :			 CheckObjectNotExist() ,CompareStringValues()
'	Created on :				   10/03/2008
'	Author :						  Rathna Reddy 
'
'------------------------------------------------------------------------------------------------------------------------------------------

Function VerifyTableCellData (Byval objTable,Byval intRowNum , byval intColNum,Byval strExpData)
 On Error Resume Next

    'Variable Section Begin
		
		 Dim strMethod	
		 Dim  intRowNumber, intColNumber,intTotalRows,intTotalCols,strActData
	'Variable Section End

		strMethod = strKeywordForReporting

		If IsInteger(intRowNum) <> 0 Then
				VerifyTableCellData = 1
				WriteStatusToLogs  "The given Row  Number "&intRowNum&" is not an Integer, please verify. Row number starts from 1."
				Exit Function	 
		End If

		If IsInteger(intColNum) <> 0 Then
				VerifyTableCellData = 1
				WriteStatusToLogs  "The given Column  Number "&intColNum&" is not an Integer, please verify. Column number starts from 1."
				Exit Function	 
		End if

		If CheckObjectExist(objTable)  Then 
				intRowNumber=cint(intRowNum)
		    	intColNumber=cint(intColNum)				
				intTotalRows=objTable.RowCount
				
				If intRowNumber<=intTotalRows Then
				   
					intTotalCols=objTable.ColumnCount(intRowNumber) 					
					If intColNumber<=intTotalCols Then
							strActData=objTable.GetCellData(intRowNumber,intColNumber)
							If Trim(strExpData)=Trim(strActData) Then
									VerifyTableCellData = 0
									WriteStatusToLogs """"&"Expected cell data : "&strExpData& """" &  "   and   " &""""&"Actual cell data : "&strActData&""""&"  are equal."
							 Else
									VerifyTableCellData = 2
									WriteStatusToLogs """"&"Expected cell data: "&strExpData& """" &  "   and   " &""""&"Actual cell data : "&strActData&""""&"  are not  equal, please verify."
							 End If
					Else
							VerifyTableCellData = 1
							WriteStatusToLogs 	"Column Number :" &""""&intColNumber&""""&" was not found in Table, when total columns is:"& intTotalCols &" Please verify total number of columns in table"
					End If																

				Else
					VerifyTableCellData = 1
					WriteStatusToLogs 	"Row Number :" &""""&intRowNumber&""""&" was not found in Table, when total rows is:"& intTotalRows &", please verify."
				End If				
		Else
			VerifyTableCellData = 1
			WriteStatusToLogs  "Table does not exist,Please verify."

		End If
					  		
 On Error GoTo 0  
End Function




'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :			  StoreColumnCount()
'	Purpose :				  Stores the column count of the specified row of the table	
'	Return Value :		 	  Integer(0/1)
'	Arguments :
'		Input Arguments :  	  objObject , strKey , intRow
'		Output Arguments : 
'	Function is called by :
'	Function calls :		  GetTableRowColumnCount()
'	Created on :			  12/06/2009
'	Author :				  Iohbha K
'
'------------------------------------------------------------------------------------------------------------------------------------------

Function StoreColumnCount(byval objObject, byval strKey , byval intRow)
On error resume Next
	'Variable Section Begin
	
		Dim strMethod
		Dim intVal

	'Variable Section End

	strMethod = strKeywordForReporting

	intVal = GetTableRowColumnCount(objObject ,strKey ,intRow)
	If intVal = 0 Then
		StoreColumnCount = 0
		WriteStatusToLogs "Column Count of the specified table is stored successfully  in the Key '"&strKey&"'"

    ElseIf intVal = 1 then
		StoreColumnCount = 1
		WriteStatusToLogs "The specified object does not exist"	
	ElseIf intVal = 2 then
		StoreColumnCount = 1
		WriteStatusToLogs "The row '"&intRow&"' does not exist in the specified table.Please verify"
	ElseIf intVal = 3 then
		StoreColumnCount = 1
		WriteStatusToLogs "The Row data passed is invalid , please verify."
	ElseIf intVal = 4 then
		StoreColumnCount = 1
		WriteStatusToLogs "The Row '"&intRow&"' data passed is not a Natural Number. Please Verify."
	ElseIf intVal = 5 Then
		StoreColumnCount = 1
		WriteStatusToLogs "The Key passed is not valid. Please Verify."
	End if

On error goto 0
End Function



'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :			  StoreRowCount()
'	Purpose :				  Stores the row count of the specified table	
'	Return Value :		 	  Integer(0/1)
'	Arguments :
'		Input Arguments :  	  objObject , strKey
'		Output Arguments : 
'	Function is called by :
'	Function calls :		  GetTableRowCount()
'	Created on :			  12/06/2009
'	Author :				  Iohbha K
'
'------------------------------------------------------------------------------------------------------------------------------------------

Function StoreRowCount(byval objObject, byval strKey )
On error resume Next
	'Variable Section Begin
	
	Dim strMethod
	Dim intVal

	'Variable Section End

	strMethod = strKeywordForReporting

	intVal = GetTableRowCount(objObject ,strKey )
	If intVal = 0 Then
		StoreRowCount = 0
		WriteStatusToLogs "Row Count of the specified table is stored successfully  in the Key '"&strKey&"'"

    ElseIf intVal = 1 then
		StoreRowCount = 1
		WriteStatusToLogs "The specified object does not exist"	
	ElseIf intVal = 2 then
		StoreRowCount = 1
		WriteStatusToLogs "An error occured while retrieving the specified table's Row Count"	
	ElseIf intVal = 3 then
		StoreRowCount = 1
		WriteStatusToLogs "The Key passed is invalid , please verify."	
	End if

On error goto 0
End Function




'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :			  StoreInstanceOfDataFromColumn()
'	Purpose :				  Store the number of occurence of the strCellData in the particular column.
'							  The column number starts from 1. The comparison is casesensitive.
'							  Occurence 0 indicates that the data does not exist in the column , and the data will be pushed in the dictionary
'	Return Value :		 	  Integer(0/1)
'	Arguments :
'		Input Arguments :  	  objJTable , strKey , intColNo , strCellData
'		Output Arguments : 
'	Function is called by :
'	Function calls :		  GetInstanceOfDataInJColumn()
'	Created on :			  01/07/2009
'	Author :				  Iohbha K
'
'------------------------------------------------------------------------------------------------------------------------------------------

Function StoreInstanceOfDataFromColumn(ByVal objJTable , ByVal strKey , ByVal intColNo , ByVal strCellData)
On error resume Next
	'Variable Section Begin
	
		Dim strMethod
		Dim intVal

	'Variable Section End

	strMethod = strKeywordForReporting

	intVal = GetInstanceOfDataInColumn(objJTable , strKey , intColNo , strCellData)
	If intVal = 0 Then
		StoreInstanceOfDataFromColumn = 0
		WriteStatusToLogs "The data occurrence in the specified column is stored successfully."

    ElseIf intVal = 1 then
		StoreInstanceOfDataFromColumn = 1
		WriteStatusToLogs "The specified table does not exist"	
	ElseIf intVal = 2 then
		StoreInstanceOfDataFromColumn = 1
		WriteStatusToLogs "An error occurred in the action StoreInstanceOfDataFromColumn. Please Verify."
	ElseIf intVal = 3 then
		StoreInstanceOfDataFromColumn = 1
		WriteStatusToLogs "The Key passed is invalid , please verify."
	ElseIf intVal = 4 then
		StoreInstanceOfDataFromColumn = 1
		WriteStatusToLogs "The cell data passed is either null or empty , please verify."	
	ElseIf intVal = 5 then
		StoreInstanceOfDataFromColumn = 1
		WriteStatusToLogs "The column number '"&intColNo&"' is not a whole number, please verify."
	ElseIf intVal = 6 then
		StoreInstanceOfDataFromColumn = 1
		WriteStatusToLogs "Failed to retrieve data from the specified table, please verify."
	ElseIf intVal = 7 then
		StoreInstanceOfDataFromColumn = 1
		WriteStatusToLogs "The column number '"&intColNo&"' does not exist in the speified table, please verify."
	
	End if

On error goto 0
End Function




'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :				 StoreRowNumberHavingCellData()
'	Purpose :					 Stores the row number of the Nth occurence of the specified cell data within a specified column
'	Return Value :		 		 Integer(0/1/2)
'	Arguments :
'		Input Arguments :  		 objTable, strKey, intColNo, strCellData, instanc, increment
'		Output Arguments : 
'	Function is called by :
'	Function calls :			 IsWholeNumber(), GetRowNumberForNInstanceofDataInJColumn(), WriteStatusToLogs
'	Created on :				 03/07/2009
'	Author :					 Vaibhav Kelkar
'
'------------------------------------------------------------------------------------------------------------------------------------------

Function StoreRowNumberHavingCellData(ByVal objJTable,ByVal strKey,ByVal intColNo, ByVal strCellData , ByVal instanc , ByVal increment)

   On error resume Next
	'Variable Section Begin
		Dim strMethod
		Dim intVal
		Dim intN
		Dim strReportOccur
		intVal = 9
	'Variable Section End

	strMethod = strKeywordForReporting

	If instanc = "" Then
		instanc = 1
		WriteStatusToLogs "The Instance data is empty. Hence its set to the default value '1' which is the first instance"
		
	ElseIf IsPositiveInteger(instanc) <>0 Then
		StoreRowNumberHavingCellData = 1
		WriteStatusToLogs "The instance '" & instanc & "' is not a valid integer, please verify. Instance starts at 1."
		Exit Function

	Else
		instanc = CInt(instanc)
	End if	 

	If increment = "" Then
		increment = 0
		WriteStatusToLogs "The Increment data is empty. Hence its set to the default value '0'."
		
	ElseIf IsInteger(increment) <>0 Then
		StoreRowNumberHavingCellData = 1
		WriteStatusToLogs "The increment '" & increment & "' is not a valid integer, please verify."
		Exit Function

	Else
		increment = CInt(increment)
	End If 
    
	intN = instanc + increment
	intVal = GetRowNumberForNInstanceofDataInColumn(objJTable , strKey , intColNo , strCellData, intN)
	
	If intN = 1 Then
		strReportOccur = "1st "
	Elseif intN = 2 Then
		strReportOccur = "2nd  "
   Elseif intN = 3 Then
		 strReportOccur = "3rd  "
   Else
		 strReportOccur = intN&"th  "
	End If
	If intVal = 0 Then
		StoreRowNumberHavingCellData = 0
		WriteStatusToLogs "the Row Number for the "& strReportOccur&" occurence of the expected Cell Data : '"&strCellData& "', in the specified column, is stored successfully."

    ElseIf intVal = 1 then
		StoreRowNumberHavingCellData = 1
		WriteStatusToLogs "The specified table does not exist"	
	ElseIf intVal = 2 then
		StoreRowNumberHavingCellData = 2
		WriteStatusToLogs strReportOccur&" occurence of the expected Cell Data : '"&strCellData& "' was not found in the table. Please Verify."
	ElseIf intVal = 3 then
		StoreRowNumberHavingCellData = 1
		WriteStatusToLogs "The Key passed is invalid , please verify."
	ElseIf intVal = 4 then
		StoreRowNumberHavingCellData = 1
		WriteStatusToLogs "The cell data passed is either null or empty , please verify."	
	ElseIf intVal = 5 then
		StoreRowNumberHavingCellData = 1
		WriteStatusToLogs "The column number '"&intColNo&"' is not valid. Column number starts from 1. Please Verify."
	ElseIf intVal = 6 then
		StoreRowNumberHavingCellData = 1
		WriteStatusToLogs "Failed to retrieve data from the specified table, please verify."
	ElseIf intVal = 7 then
		StoreRowNumberHavingCellData = 1
		WriteStatusToLogs "The column number '"&intColNo&"' does not exist in the speified table, please verify."
	Elseif  intVal = 9 then
		StoreRowNumberHavingCellData = 1
		WriteStatusToLogs "The Instance or the Increment argument is not valid whole number, please verify."
	End if

On error goto 0

End Function

'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :			  StoreColumnNumber()
'	Purpose :				  Stores the column number of the specified column header of the  table	
'	Return Value :		 	  Integer(0/1)
'	Arguments :
'		Input Arguments :  	  objObject , strKey , strColName 
'		Output Arguments : 
'	Function is called by :
'	Function calls :		  GetJColumnNumber()
'	Created on :			  01/07/2009
'	Author :				  Iohbha K
'
'------------------------------------------------------------------------------------------------------------------------------------------

Function StoreColumnNumber(ByVal objJTable, ByVal strKey, ByVal strColName)
On error resume Next
	'Variable Section Begin
	
		Dim strMethod
		Dim intVal

	'Variable Section End

	strMethod = strKeywordForReporting

	intVal = GetColumnNumber(objJTable, strKey, strColName)
	If intVal = 0 Then
		StoreColumnNumber = 0
		WriteStatusToLogs "Column Count of the specified table is stored successfully  in the Key '"&Trim(strKey)&"'"

    ElseIf intVal = 1 then
		StoreColumnNumber = 1
		WriteStatusToLogs "The specified table does not exist"	
	ElseIf intVal = 2 then
		StoreColumnNumber = 1
		WriteStatusToLogs "The column header '"&strColName&"' is not available in the specified table"
	ElseIf intVal = 3 then
		StoreColumnNumber = 1
		WriteStatusToLogs "The Key passed is invalid , please verify."	
	ElseIf intVal = 4 then
		StoreColumnNumber = 1
		WriteStatusToLogs "The column header passed is either null or empty, please verify."	
	ElseIf intVal = 5 then
		StoreColumnNumber = 1
		WriteStatusToLogs "Failed to retrieve the specified table data"
	End if

On error goto 0
End Function

Function clickRow( ByVal objJTable, ByVal strRowNum, ByVal strColNum)
On error resume Next
'Variable declare begin
		Dim strMethod
		Dim actRowCnt, actColCnt, blnRownotFound, cell
'Variable declare End

strMethod = strKeywordForReporting

'Variable Init begin
		actRowCnt = 0
		actColCnt = 0
		blnRownotFound = True
'Variable Init End

		'If CheckObjectExist(objTable)  Then 
			Set cell = objJTable.ChildObjects
			'MsgBox "cell.Count()  = "&cell.Count() 
			For i=1 to  cell.Count() 
			  strprop =  cell(i).GetROProperty("html tag")
				If instr(1,strprop,"TR",1) >0  and blnRownotFound = true Then
				   ' cell(i).Click
				   
				   actRowCnt = actRowCnt + 1
				   'msgbox "in if"
				   If instr(1,cstr(actRowCnt),strRowNum-1,1) > 0 Then
						blnRownotFound = False
						'msgbox "row found"
				   End If
				 End If
				   If  blnRownotFound = False  Then
						If instr(1,strprop,"TD",1) >0   Then
							actColCnt = actColCnt + 1
							 If instr(1,actColCnt,strColNum,1) >0   Then
								 'msgbox "col found"
									 cell(i).Click
									Exit For
							 End If
					    End If
				   End If
				
				
			   ' msgbox " i = "&i
			Next
			
									
		If Err.number <> 0 Then 
		
			clickRow = 1
			WriteStatusToLogs  "Error occured,Please verify."&Err.description
		Else
		clickRow = 0
			WriteStatusToLogs  "Specified row was clicked."
		End If

On error goto 0
End Function

'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name :				 StoreColumnData()
' 	Purpose :					 Stores the data of the specified column
'	Return Value :		 		 Integer(0/1)
'   Arguements:	
'		Input Arguments :  		 objTable,intColNumber,intColNumber
'		Output Arguments :       intRes
'	Function is called by :
'	Function calls :			 CheckObjectExist(),GetTableRowData()
'	Created on :				 28/03/2011
'	Author :					 Iohbha K
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function StoreColumnData(ByVal objTable,  ByVal intColNumber, ByVal strKey)
On Error Resume Next
	'Variable Section Begin
		
		 Dim strMethod
		 Dim arrActualdata, intColNum,intTotalCols , strArrayData , intVal
		 Dim i
				
	'Variable Section End

	 strMethod = strKeywordForReporting

	If IsKey(strKey) <> 0 Then
		StoreColumnData = 1
		WriteStatusToLogs "The key is invalid. Please verify." 
    Else

		If CheckObjectExist(objTable)  Then 			
			If IsPositiveInteger(intColNumber)= 0 Then
				intColNum = CInt(intColNumber)
				intTotalCols=objTable.ColumnCount(1)			
				If CInt(intTotalCols)>=intColNum and  intColNum>0 Then
					arrActualdata = GetTableColData(objTable,intColNum)
					For i = 0 To UBound(arrActualdata) 
						 arrActualdata(i) = PrefixEscape(arrActualdata(i))
					Next
					If  Not IsNull(arrActualdata)  Then	
						'convert the array items into strings dilimited by array character ^, eg: abc^xyz^12abc
						strArrayData = PrintArrayElements(arrActualdata)	
						intval = AddItemToStorageDictionary(strKey,strArrayData)
						If  intval = 0 Then
							StoreColumnData = 0
							WriteStatusToLogs "The column "&intColNumber&" data is stored successfully in the Key '"&strKey&"'. The data are:"&strArrayData 
									  
						Else
							StoreColumnData = 1
							WriteStatusToLogs "The column "&intColNumber&" data is NOT stored successfully in the Key '"&strKey&"'" 
									  
						End If	
					Else
						StoreColumnData = 1
						WriteStatusToLogs 	"Action Fails to get table column "&intColNum&" data, please verify."
					End If
				Else
					StoreColumnData = 1
					WriteStatusToLogs 	"Column Number :" &""""&intColNum&""""&" was not found in Table, there are "&intTotalCols&" columns only. Please verify."
				End If
			Else
				StoreColumnData = 1
				WriteStatusToLogs 	"Given column number "&intColNumber&" is not a valid column number, please verify. Column number starts from 1."
			End If
		Else
			StoreColumnData = 1
			WriteStatusToLogs 	"Table does not exist, please verify."

		End If
	End If
 	
On Error GoTo 0
End Function

'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name :				 SetTextInCellEdit()
' 	Purpose :					 Deselects the WebEdit in a specified table cell.
'	Return Value :		 		 Integer( 0/1)
'   Arguements:	
'		Input Arguments :  		 intRowNum,intColNum,data
'		Output Arguments :       
'	Function is called by :
'	Function calls :			 GetChildItemCountInTable,GetChildItem
'	Created on :				 23/02/2011
'	Author :					 Iohbha K 
'
'------------------------------------------------------------------------------------------------------------------------------------------

Function SetTextInCellEdit(byval objTable, byval intRow, byval intCol , ByVal data)
    On Error Resume Next
		'Variable Section Begin

			Dim strMethod
			Dim objChildWebEdit,intTotalRowCount,intTotalColCount
			
		'Variable Section End
		
		strMethod = strKeywordForReporting
            
		If CheckObjectExist(objTable) Then 
			  If IsPositiveInteger(intRow) = 0 Then
				     intTotalRowCount=objTable.RowCount
					 intRow = Cint(intRow)
                     If intRow>0  and  intRow<=intTotalRowCount Then
                                         
							If IsPositiveInteger(intCol) = 0 Then
								intTotalColCount=objTable.ColumnCount(intRow) 
								 intCol = Cint(intCol)
						  		If  intCol>0 and  intCol<=intTotalColCount Then
										If GetChildItemCountInTable(objTable, intRow, intCol, "WebEdit") > 0 Then

												set objChildWebEdit = GetChildItem(objTable, intRow, intCol, "WebEdit")
												If IsObject(objChildWebEdit) then
													If CheckObjectEnabled(objChildWebEdit) Then
														objChildWebEdit.Set data
														If Err.number = 0 Then
															SetTextInCellEdit = 0
															WriteStatusToLogs 	"Successfully set the data "&data&" in the cell ("&intRow&","&intCol&")."
														Else
															SetTextInCellEdit = 1
															WriteStatusToLogs 	"Fail to set the data "&data&" in the cell ("&intRow&","&intCol&"), please verify."
														End If
													Else
														SetTextInCellEdit = 1
														WriteStatusToLogs 	"The WebEdit in the cell ("&intRow&","&intCol&") was not in enabled state, please verify."

													End If
												Else
													SetTextInCellEdit = 1
													WriteStatusToLogs 	"The table cell does not contain WebEdit, please verify."
												End if 
										Else
											SetTextInCellEdit = 1
											WriteStatusToLogs 	"The specified table cell ("&intRow&","&intCol&") does not contain WebEdit, please verify."
										End If

								Else
									SetTextInCellEdit = 1
									WriteStatusToLogs 	"The given Column  Number "&intCol&" does not exist in table, please verify"

								End If

					 Else
						SetTextInCellEdit = 1
						WriteStatusToLogs 	"The given Column  Number "&intCol&" is not an Integer, Please Enter Valid Column Number."

					End If

			Else
                SetTextInCellEdit = 1
				WriteStatusToLogs 	"The given Row Number "&intRow&" does not exist in table, please verify"

			End If
		Else
			  SetTextInCellEdit = 1
			  WriteStatusToLogs  "The Given Row  Number "&intRow&" is not an Integer, Please Enter Valid Row Number."
		End If
     Else
		SetTextInCellEdit = 1
		WriteStatusToLogs 	"Table does not exist, please verify."
	End If
      
   On Error GoTo 0 
End Function
'=======================Code Section End=============================================