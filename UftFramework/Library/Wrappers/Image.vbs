
'=======================Code Section Begin=============================================

Option Explicit 'Forces explicit declaration of all variables in a script. 

'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name :				 ClickImage()
' 	Purpose :					Performs click operation on specified Image in a page.
' 	Return Value :		 		  Integer(0/1/2)
' 	Arguments :
'		Input Arguments :  		objImage
'		Output Arguments : 
'	Function is called by :
'	Function calls :				CheckObjectExist() , CheckObjectClick(),CheckObjectEnabled()
'	Created on :				   14/03/2008
'	Author :						 Rathna Reddy 
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function ClickImage (ByVal objImage)
On Error Resume Next
	'Variable Section Begin
		
			Dim strMethod

	'Variable Section End
		strMethod = strKeywordForReporting
		If CheckObjectExist(objImage)  Then  
				If CheckObjectEnabled(objImage) Then
						If CheckObjectClick(objImage) Then
							ClickImage = 0
							WriteStatusToLogs "Click operation is performed on Image."
						Else
							ClickImage = 2
							WriteStatusToLogs "Click operation is not performed on Image, please verify."
						End If 
				Else
					ClickImage = 1
					WriteStatusToLogs "The specified Image is disabled, please verify."
				End If
		Else
				ClickImage = 1
				WriteStatusToLogs "The specified Image does not exist."
    	End If
	On Error GoTo 0
End Function
'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name :				 VerifyImageToolTip()
' 	Purpose :					 Verifies the tootip of a specified image with our expected .
' 	Return Value :		 		  Integer(0/1/2)
' 	Arguments :
'		Input Arguments :  		objImage,strExpToolTip
'		Output Arguments :      strActToolTip
'	Function is called by :
'	Function calls :			   CheckObjectExist(),CompareStringValues()
'	Created on :				  14/03/2008
'	Author :						 Rathna Reddy 
'
'------------------------------------------------------------------------------------------------------------------------------------------

Function VerifyImageToolTip(byval objImage,byval strExpToolTip)
   On Error Resume Next
   'Variable Section Begin
			Dim strMethod
		   Dim strActToolTip
		  

		'Variable Section End
			strMethod = strKeywordForReporting
			If CheckObjectExist(objImage) Then 
			
					strActToolTip = objImage.object.title
					If Err.number=0 Then
					
							If CompareStringValues(strExpToolTip,strActToolTip) = 0 Then
									 VerifyImageToolTip = 0
									 WriteStatusToLogs """"&"Expected ToolTip : "&strExpToolTip& """" &  "   and   " &""""&"Actual TooTip : "&strActToolTip&""""&"  are equal."
							Else
									VerifyImageToolTip = 2
									WriteStatusToLogs """"&"Expected ToolTip : "&strExpToolTip& """" &  "   and   " &""""&"Actual TooTip : "&strActToolTip&""""&"  are  not equal,Please verify."
							End If

					Else
							VerifyImageToolTip = 1 
							WriteStatusToLogs "Action fails to Capture the specified Image's tool tip, please verify Object Properties."

					End if
		 Else
						VerifyImageToolTip = 1
						WriteStatusToLogs "The specified Image does not exist, please verify."
		 End If
	On Error GoTo 0
End Function


'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name :				 CompareImageURL()
' 	Purpose :					 Compares the image source URL with our expected URL.
' 	Return Value :		 		 Integer(0/1/2)
' 	Arguments :
'		Input Arguments :  	   objImage , strExpUrl
'		Output Arguments :     strActUrl
'	Function is called by :
'	Function calls :			   CheckObjectExist() , CompareStringValues()
'	Created on :				  14/03/2008
'	Author :						Rathna Reddy 
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function CompareImageURL(ByVal objImage , ByVal strExpUrl)
	On Error Resume Next
		'Variable Section Begin
			Dim strMethod
			Dim strActUrl

		'Variable Section End
			strMethod = strKeywordForReporting
			If CheckObjectExist(objImage) Then
					strActUrl=objImage.GetRoProperty("src")
					If Err.number=0 Then
						   If	CompareStringValues(Trim(strExpUrl),Trim(strActUrl)) = 0 Then 
							 
								CompareImageURL = 0
								WriteStatusToLogs """"&"Expected Result : "&strExpUrl& """" &  "   and   " &""""&"Actual Result : "&strActUrl&""""&"  are equal."
						  Else
								CompareImageURL = 2
								WriteStatusToLogs """"&"Expected Result : "&strExpUrl& """" &  "   and   " &""""&"Actual Result : "&strActUrl&""""&"  are not equal, please verify."
						  End If

					Else
							CompareImageURL = 1 
							WriteStatusToLogs "Action fails to Capture the specified Image's source URL, please verify Object Properties."

					End if
			Else
					CompareImageURL = 1
					WriteStatusToLogs "The specified Image does not exist, please verify."
			End If
	On Error GoTo 0
End Function
'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :				 VerifyImageExist()
'	Purpose :					 Verifies whether the Image is Existed or not?.
'	Return Value :		 		  Integer(0/2)
'	Arguments :
'		Input Arguments :  		  objObject 
'		Output Arguments : 
'	Function is called by :
'	Function calls :			 CheckObjectExist() 
'	Created on :				   10/03/2008
'	Author :						  Rathna Reddy 
'
'------------------------------------------------------------------------------------------------------------------------------------------

Function VerifyImageExist(ByVal objImage)
	On Error Resume Next
		'Variable Section Begin
	
		   Dim strMethod

		'Variable Section End
			strMethod = strKeywordForReporting
			If CheckObjectExist(objImage) Then 
			   
					VerifyImageExist = 0
					WriteStatusToLogs "The specified Image Exists."
					
			Else
					VerifyImageExist = 2
					WriteStatusToLogs "The specified Image does NOT Exist, please verify."
			End If 
	On Error GoTo 0
End Function

'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :				 VerifyImageNotExist()
'	Purpose :					 Verifies whether the Image is Existed or not?.
'	Return Value :		 		  Integer(0/2)
'	Arguments :
'		Input Arguments :  		  objObject 
'		Output Arguments : 
'	Function is called by :
'	Function calls :			 CheckObjectExist() 
'	Created on :				   10/03/2008
'	Author :						  Rathna Reddy 
'
'------------------------------------------------------------------------------------------------------------------------------------------

Function VerifyImageNotExist(ByVal objImage)
	On Error Resume Next
		'Variable Section Begin
	
		   Dim strMethod

		'Variable Section End
			strMethod = strKeywordForReporting
			If CheckObjectNotExist(objImage) Then 
			   
					VerifyImageNotExist = 0
					WriteStatusToLogs "The specified Image does not Exist."
					
			Else
					VerifyImageNotExist = 2
					WriteStatusToLogs "The specified Image Exists, please verify."
					
			End If 
	
  		
	On Error GoTo 0
End Function

'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :				 VerifyImageVisible()
'	Purpose :					 Verifies whether the Image is visible.
'	Return Value :		 		  Integer(0/1/2)
'	Arguments :
'		Input Arguments :  		  objImage 
'		Output Arguments : 
'	Function is called by :
'	Function calls :			 CheckObjectExist() , CheckObjectVisible()
'	Created on :				   10/03/2008
'	Author :						  Rathna Reddy 
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function VerifyImageVisible(ByVal objImage)
	On Error Resume Next
		'Variable Section Begin
		
			Dim strMethod

		'Variable Section End
			strMethod = strKeywordForReporting
			If CheckObjectExist(objImage)  Then 
				If CheckObjectVisible(objImage) Then
					VerifyImageVisible = 0
					WriteStatusToLogs "The specified Image is visible"
				Else
					VerifyImageVisible = 2
					WriteStatusToLogs "The specified Image is not Visible"
				End If
			Else
					VerifyImageVisible = 1
					WriteStatusToLogs "The specified Image does not exist, please verify."
			End If
		
	On Error GoTo 0
End Function
'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :				 VerifyImageInVisible()
'	Purpose :					 Verifies whether the Image is Invisible.
'	Return Value :		 		  Integer(0/1/2)
'	Arguments :
'		Input Arguments :  		  objImage 
'		Output Arguments : 
'	Function is called by :
'	Function calls :			 CheckObjectExist() , CheckObjectInVisible()
'	Created on :				   10/03/2008
'	Author :						  Rathna Reddy 
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function VerifyImageInVisible(ByVal objImage)
	On Error Resume Next
		'Variable Section Begin
		
			Dim strMethod

		'Variable Section End
			strMethod = strKeywordForReporting
			If CheckObjectExist(objImage)  Then 
					If CheckObjectInVisible(objImage) Then
						VerifyImageInVisible = 0
						WriteStatusToLogs "The specified Image is not Visible"
					Else
						VerifyImageInVisible = 2
						WriteStatusToLogs "The specified Image is Visible, please verify."
					End If
			Else
					VerifyImageInVisible = 1
					WriteStatusToLogs "The specified Image does not exist, please verify."
			End If
		
	On Error GoTo 0
End Function






'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name :				VerifyImageEnability()
' 	Purpose :					Checks the enability of the image.
'								Returns 0:If image in enabled/disabled as expected
'								Returns 1:If an exception occures
'								Returns 2:If image is not enabled/disabled as expected
' 	Return Value :		 		Integer(0/1/2)
' 	Arguments :
'		Input Arguments :  		objImage,state
'		Output Arguments : 
'	Function is called by :		
'	Function calls :			CheckImageEnability(),IsBoolean(),CheckObjectExist(),CheckObjectVisible()
'	Created on :				31/05/2011
'	Author :					Amruta B 
'------------------------------------------------------------------------------------------------------------------------------------------
Function VerifyImageEnability(ByVal objImage, byval state)
	On Error Resume Next
			'Variable Section Begin
				Dim strMethod
				Dim intActState

		'Variable Section End
		strMethod = strKeywordForReporting
			
		If state = "" Then

			WriteStatusToLogs "The enability parameter is empty. Hence its set to the default value 'True'"
			state = True

		ElseIf IsBoolean(state) = 1 Then 'If the case data is not boolean then assume default data
			state = True
			WriteStatusToLogs "The enability parameter is '" & state & "'. Hence its set to the default value 'True'"

		Else
			state = Trim(CBool(state))
		End If

		
		If CheckObjectExist(objImage) Then 
			If CheckObjectVisible(objImage) Then
				intActState=CheckImageEnability(objImage) 
				If intActState=0 Then  ' object is enabled
							
					If UCase(state) = "TRUE" Then 
						VerifyImageEnability=0
						WriteStatusToLogs "The specified Image is Enabled as expected."					
					Else
						VerifyImageEnability=2
						WriteStatusToLogs "The specified Image is Enabled. Expected=Disabled and Actual=Enabled do not match, please verify."
					End If
				ElseIf intActState=2 Then ' object is disabled
					If  UCase(state)="TRUE" then
						VerifyImageEnability=2
						WriteStatusToLogs "The specified Image is Disabled.  Expected=Enabled and Actual=Disabled do not match, please verify."					
					Else
						VerifyImageEnability=0
						WriteStatusToLogs "The specified Image is Disabled as expected."
					End If
				Else
					VerifyImageEnability=1
					WriteStatusToLogs "Fail to verify the image enability property, an error occurred, please verify."
				End if
			
			Else' else of CheckObjectVisible
				VerifyImageEnability=1
				WriteStatusToLogs "The specified Image is not visible, please verify."
			End If'CheckObjectVisible		
		Else'CheckObjectExist
			VerifyImageEnability=1
			WriteStatusToLogs "The specified Image does not exist, please verify."						
		End If'CheckObjectExist							
			
	On Error GoTo 0
End Function

'=======================Code Section End=============================================  