
'========================Code Section Begin=====================================================================
Option Explicit	'Forces explicit declaration of all variables in a script.
'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :			ActivateBrowser()
'	Purpose :				Activates the Specified Browser.	
'	Return Value :		 	Integer(0/1)
'	Arguments :
'		Input Arguments :   objBrowser    
'		Output Arguments :  Status
'	Function is called by : 
'	Function calls :		CheckObjectExist()
'	Created on : 			04/03/2008
'	Author :				Rathna Reddy
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function ActivateBrowser(ByVal objBrowser)
	On Error Resume Next
		'Variable Section Begin
			Dim strMethod
			Dim intActive

		'Variable Section End
		strMethod = strKeywordForReporting
		If CheckObjectExist(objBrowser) Then
			   intActive=objBrowser.GetROProperty("hwnd")
			   Window("hwnd:=" & intActive).Activate
			   If err.Number = 0 Then
					ActivateBrowser  = 0
					WriteStatusToLogs "Specified Browser is activated."
		       Else
					ActivateBrowser = 1
					WriteStatusToLogs "Specified Browser is not activated, please verify."
			   End If
		Else
		    ActivateBrowser = 1 
			WriteStatusToLogs "Specified Browser does not exist, please verify."
		End If
   On Error GoTo 0 
End Function
'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name :			BackBrowser()
' 	Purpose :				Navigates to the  Previous page in the browser.
' 	Return Value :		 	Integer(0/1)
' 	Arguments :
'		Input Arguments :   objBrowser
'		Output Arguments : 
'	Function is called by :
'	Function calls :		 CheckObjectExist()
'	Created on :			 03/03/2008
'	Author :			     Rathna Reddy 
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function BackBrowser(ByVal objBrowser)
	On Error Resume Next 
		'Variable Section Begin
		
			Dim strMethod
		'Variable Section End
		strMethod = strKeywordForReporting
			If CheckObjectExist(objBrowser) Then  

				objBrowser.Back 
				
				If err.Number = 0 Then  
					BackBrowser = 0
					WriteStatusToLogs "Specified Browser is navigated back successfully."
				Else
					BackBrowser = 1
					WriteStatusToLogs "Specified Browser is not navigated BACK successfully, Please verify."
				End If
			Else
				 BackBrowser = 1
				 WriteStatusToLogs "Specified Browser does not exist, please verify."
			End If
	On Error GoTo 0 
End Function

'Not Required in Browser


'Not Required in Browser


'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :			CloseBrowser()
'	Purpose :				Closes the specified Browser.
'	Return Value :			Integer(0/1)
'	Arguments :
'		Input Arguments :	objBrowser
'		Output Arguments : 
'	Function is called by :
'	Function calls :		CheckObjectExist()
'	Created on :			03/03/2008
'	Author :				Rathna Reddy
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function CloseBrowser(ByVal objBrowser)
	On Error Resume Next
		'Variable Section Begin

			Dim strMethod

		'Variable Section End
		strMethod = strKeywordForReporting
			'If  CheckObjectExist(objBrowser) Then 
				objBrowser.Close 
				If err.Number = 0 Then 
					CloseBrowser = 0 
					WriteStatusToLogs "Specified Browser is closed successfully."
				Else
					CloseBrowser = 1 
					WriteStatusToLogs "Specified Browser is not closed successfully, please verify."
				End If
			'Else
				'CloseBrowser = 1 
				'WriteStatusToLogs "Action: "&strMethod&vbtab&_
							              '"Status:" &  "Failed" &Vbtab&_
										 '"Message:" & "Specified Browser does not exist, Please verify."
					
		'End If
	On Error GoTo 0
End Function 



'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :				 CompareBrowserTitle()
'	Purpose :					 Compares the browser title with our expected title.
'	Return Value :		 		 Integer(0/1/2)
'	Arguments :
'		Input Arguments :  		objBrowser , strExpTitle
'		Output Arguments :      strActTitle
'	Function is called by :
'	Function calls :			 CheckObjectExist() , CompareStringValues()
'	Created on : 				 03/03/2008
'	Author :					 Rathna Reddy
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function CompareBrowserTitle(ByVal objBrowser , ByVal strExpTitle)
	On Error Resume Next 
		'Variable Section Begin
			Dim strMethod
			Dim strActTitle
		'Variable Section End
		strMethod = strKeywordForReporting
			 If CheckObjectExist(objBrowser) Then
				  strActTitle = objBrowser.GetRoProperty("title")
                  If CompareStringValues(StrExpTitle, strActTitle)=0  Then 
						CompareBrowserTitle = 0
						WriteStatusToLogs """"&"Expected Title : "&strExpTitle& """" &  " and " &""""&"Actual Title : "&strActTitle&""""&" matches as expected."
			     Else
						CompareBrowserTitle = 2
						WriteStatusToLogs """"&"Expected Title : "&strExpTitle& """" &  " and " &""""&"Actual Title : "&strActTitle&""""&" does not match, please verify."
				 End If
			Else
				 CompareBrowserTitle = 1
				 WriteStatusToLogs "Specified Browser does not exist, please verify."
			End If
	On Error GoTo 0 
End Function

'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :				 CompareBrowserURL()
'	Purpose :					 Compares the current URL of  the Browser with our expected URL
'	Return Value :		 		  Integer(0/1/2)
'	Arguments :
'		Input Arguments :  		 objBrowser , strExpURL
'		Output Arguments :       strActURL
'	Function is called by :
'	Function calls :                CheckObjectExist() , CompareObjectURL()
'	Created on : 				   04/03/2008
'	Author :						 Rathna Reddy
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function CompareBrowserURL(ByVal objBrowser , ByVal strExpURL)
	On Error Resume Next
		'Variable Section Begin
			Dim strMethod
			Dim strActURL

		'Variable Section End
		strMethod = strKeywordForReporting
			If CheckObjectExist(objBrowser) Then
				strActURL = objBrowser.GetRoProperty("url")
				If	CompareStringValues(strExpURL, strActURL)= 0 Then 
					CompareBrowserURL = 0
					WriteStatusToLogs """"&"Expected URL : "&strExpURL& """" &  " and " &""""&"Actual URL : "&strActURL&""""&" matches as expected."
				Else
					CompareBrowserURL = 2
					WriteStatusToLogs """"&"Expected URL : "&strExpURL& """" &  " and " &""""&"Actual URL : "&strActURL&""""&" does not match, Please verify."
				End If
			Else
				 CompareBrowserURL = 1 
				 WriteStatusToLogs "Specified Browser does not exist, please verify."
			End If
	On Error GoTo 0 
End Function
'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :				 CompareBrowserVersion()
'	Purpose :					 Compares the  Browser version with our expected version.
'	Return Value :		 		  Integer(0/1/2)
'	Arguments :
'		Input Arguments :  		  objBrowser , dblExpBrowserVersion
'		Output Arguments :        dblActVersion
'	Function is called by :
'	Function calls :				CheckObjectExist(),CompareStringValues(0
'	Created on : 				   03/03/2008
'	Author :						 Rathna Reddy							 
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function CompareBrowserVersion (ByVal objBrowser , ByVal dblExpVersion)
	On Error Resume Next
		'Variable Section Begin
			Dim strMethod
			Dim dblActVersion

		'Variable Section End
		strMethod = strKeywordForReporting
			If CheckObjectExist(objBrowser) Then 
					dblActVersion=objBrowser.GetRoProperty("version")
					If CompareStringValues(dblExpVersion, dblActVersion) = 0 Then
							CompareBrowserVersion = 0
							WriteStatusToLogs """"&"Expected Version : "&dblExpVersion& """" &  "   and   " &""""&"Actual Version : "&dblActVersion&""""&" matches as expected."
					Else
							CompareBrowserVersion = 2 
							WriteStatusToLogs """"&"Expected Version : "&dblExpVersion& """" &  "   and   " &""""&"Actual Version : "&dblActVersion&""""&" does not match, Please verify."
					End If
			Else
					CompareBrowserVersion = 1 
					WriteStatusToLogs "Specified Browser does not exist, please verify."
					
			End If
			
	On Error GoTo 0
End Function



'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name :			   ForwardBrowser()
' 	Purpose :				   Navigates to the next  page in the Browser.
' 	Return Value :		 	   Integer(0/1)
' 	Arguments :
'		Input Arguments :  	  objBrowser
'		Output Arguments : 
'	Function is called by :
'	Function calls :		  CheckObjectExist()
'	Created on :			  03/03/2008
'	Author :				  Rathna Reddy 
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function ForwardBrowser(ByVal objBrowser)
	On Error Resume Next 
		'Variable Section Begin
		
			Dim strMethod

		'Variable Section End
		strMethod = strKeywordForReporting
			If CheckObjectExist(objBrowser) Then
				objBrowser.Forward 
					If err.Number = 0 Then 
						ForwardBrowser = 0 
						WriteStatusToLogs "FORWARD Browser Action passed."
					Else
						Err.Clear()
						 
						ForwardBrowser = 1
						WriteStatusToLogs "FORWARD Browser Action failed, please verify."
					
					End If 
			Else
				ForwardBrowser = 1
				WriteStatusToLogs "Browser does not exist, please verify."
					
			End If
	On Error GoTo 0 
End Function



'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :				DeleteCookiesFromBrowser()
'	Purpose :					Deletes the cookies from the specified Browser
'	Return Value :		 		Integer(0/1)
'	Arguments :
'		Input Arguments :		objBrowser
'		Output Arguments : 
'	Function is called by :
'	Function calls :			CheckObjectExist()
'	Created on :				04/03/2008
'	Author :					Rathna Reddy
'
'------------------------------------------------------------------------------------------------------------------------------------------
'Function DeleteCookiesFromBrowser(ByVal objBrowser)
'	On Error Resume Next 
'		'Variable Section Begin
'		
'			Dim strMethod
'
'		'Variable Section End
'		'strMethod = strKeywordForReporting
'			If  CheckObjectExist(objBrowser) Then 
'				webutil.DeleteCookies
'					If err.Number = 0 Then 
'						DeleteCookiesFromBrowser = 0
'						WriteStatusToLogs "Action: "&strMethod&vbtab&_
'							              "Status:" &  "Passed" &Vbtab&_
'										 "Message:" & "Cookies for the specified browser are deleted successfully."
'					
'					Else
'						DeleteCookiesFromBrowser = 1
'						WriteStatusToLogs "Action: "&strMethod&vbtab&_
'							              "Status:" &  "Failed" &Vbtab&_
'										 "Message:" & "Cookies for the specified browser are not deleted successfully, Please verify."
'					End If
'			Else
'				DeleteCookiesFromBrowser = 1
'				WriteStatusToLogs "Action: "&strMethod&vbtab&_
'							              "Status:" &  "Failed" &Vbtab&_
'										 "Message:" & "Specified Browser does not exist, Please verify."
'					
'					
'			End If
'	On Error GoTo 0 
'End Function
'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name :			 HomeBrowser()
' 	Purpose :				 Navigates to the Home page in the Browser.
' 	Return Value :		 	 Integer(0/1)
' 	Arguments :
'		Input Arguments :  	objBrowser
'		Output Arguments : 
'	Function is called by :
'	Function calls :	   CheckObjectExist()
'	Created on :		   03/03/2008
'	Author :			   Rathna Reddy 
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function HomeBrowser(ByVal objBrowser)
	On Error Resume Next 
		'Variable Section Begin
		
			Dim strMethod

		'Variable Section End
		strMethod = strKeywordForReporting
			If	CheckObjectExist(objBrowser) Then 
				   objBrowser.Home 
				   If err.Number = 0  Then 
						HomeBrowser = 0 
						WriteStatusToLogs "Specified Browser is navigated to HOME successfully"
				   Else
						HomeBrowser = 1 
						WriteStatusToLogs "Specified Browser is not navigated to HOME successfully, please verify."
				   End If
			Else
				HomeBrowser = 1 
				WriteStatusToLogs "Specified Browser does not exist, please verify."
					
			End If
	On Error GoTo 0 
End Function
'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :			MaximizeBrowser()
'	Purpose :				Maximizes the specified browser.
'	Return Value :			Integer(0/1)
'	Arguments :
'		Input Arguments :	objBrowser      
'		Output Arguments :	
'	Function is called by :
'	Function calls :		CheckObjectExist()
'	Created on :			04/03/2008
'	Author :				Rathna Reddy
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function MaximizeBrowser(ByVal objBrowser)
	On Error Resume Next
		'Variable Section Begin
			Dim strMethod
			Dim intMaxHandle

		'Variable Section End
		strMethod = strKeywordForReporting
		If CheckObjectExist(objBrowser) Then
			   intMaxHandle=objBrowser.GetROProperty("hwnd")
			   Window("hwnd:=" & intMaxHandle).Maximize
				If err.Number = 0 Then
					MaximizeBrowser  = 0
					WriteStatusToLogs "Specified Browser is maximized successfully."
		       Else
					MaximizeBrowser = 1 
					WriteStatusToLogs "Specified Browser is not maximized successfully, please verify."
					
			  End If
		Else
		     MaximizeBrowser = 1 
			 WriteStatusToLogs "Specified Browser does not exist, please verify."
					
	  End If
   
	On Error GoTo 0 
End Function
'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :			MinimizeBrowser()
'	Purpose :		        Minimizes the specified Browser.
'	Return Value :		 	Integer(0/1)
'	Arguments :
'		Input Arguments :	objBrowser      
'		Output Arguments :	
'	Function is called by :
'	Function calls :		CheckObjectExist()
'	Created on : 			04/03/2008
'	Author :				Rathna Reddy
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function MinimizeBrowser(ByVal objBrowser)
	On Error Resume Next
		'Variable Section Begin
			Dim strMethod
			Dim intMinHandle

		'Variable Section End
		strMethod = strKeywordForReporting
			If CheckObjectExist(objBrowser) Then
			   intMinHandle=objBrowser.GetROProperty("hwnd")
			   Window("hwnd:=" & intMinHandle).Minimize
				If err.Number = 0 Then
					 MinimizeBrowser  = 0
					WriteStatusToLogs "Specified Browser is minimized successfully."
		       Else
					MinimizeBrowser = 1 
					WriteStatusToLogs "Specified Browser is not minimized successfully, please verify."
					
			  End If
		Else
		     MinimizeBrowser = 1 
			 WriteStatusToLogs "Specified Browser does not exist, please verify."
	  End If
   On Error GoTo 0 
End Function


'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name :			    RefreshBrowser()
' 	Purpose :		  			Refreshes the objects in the Browser.
' 	Return Value :			  	Integer(0/1)
' 	Arguments :
'		 Input Arguments :	    objBrowser
'		 Output Arguments : 
'	Function is called by :
'	Function calls :		 	CheckObjectExist()
'	Created on :			 	03/03/2008
'	Author :					Rathna Reddy 
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function RefreshBrowser(ByVal objBrowser)
	On Error Resume Next
		'Variable Section Begin
		
                Dim strMethod

		'Variable Section End
		strMethod = strKeywordForReporting
		    If  CheckObjectExist(objBrowser) Then	   
				    objBrowser.Refresh						  
					If	err.Number = 0 Then				   
						RefreshBrowser = 0 
						WriteStatusToLogs "Specified Browser is Refreshed successfully."
					Else
						RefreshBrowser = 1	
						WriteStatusToLogs "Specified Browser is not Refreshed successfully, please verify."
					
					End	If
			Else
				RefreshBrowser = 1
				WriteStatusToLogs "Specified Browser does not exist, please verify."
	        End If
	On Error GoTo 0                          
End	Function

'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name :				StopBrowser()
' 	Purpose :					Stops the Browser Navigation.
' 	Return Value :		 		Integer(0/1)
' 	Arguments :
'		Input Arguments :  		objBrowser
'		Output Arguments : 
'	Function is called by :
'	Function calls :			CheckObjectExist()
'	Created on :				03/03/2008
'	Author :					Rathna Reddy 
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function StopBrowser(ByVal objBrowser)
	On Error Resume Next 
		'Variable Section Begin
		
			Dim strMethod

		'Variable Section End
		strMethod = strKeywordForReporting
			If CheckObjectExist(objBrowser) Then 
					objBrowser.Stop 
					If	err.Number = 0 Then 
						StopBrowser = 0
						WriteStatusToLogs "The specified Browser is STOP successful."
					Else
						StopBrowser = 1
						WriteStatusToLogs "STOP Browser is not successful, please verify."
					
					
					End	If
			Else
				StopBrowser = 1
				WriteStatusToLogs "Specified Browser does not exist, please verify."
					
					
			End	If		
			'WriteStepResults StopBrowser 
	On Error GoTo 0 
End	Function
'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :				SyncBrowser()
'	Purpose :					Waits until current navigation is completed.
'	Return Value :		 		Integer(0/1)
'	Arguments :
'		Input Arguments :  		objBrowser
'		Output Arguments : 
'	Function is called by :
'	Function calls :             CheckObjectExist(),CheckObjectSync()
'	Created on : 				 04/03/2008
'	Author :					 Rathna Reddy
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function SyncBrowser(ByVal objBrowser)
	On Error Resume Next 
			'Variable Section Begin
		
				Dim strMethod

			'Variable Section End
			strMethod = strKeywordForReporting
				If  CheckObjectExist(objBrowser) Then 
					If  CheckObjectSync(objBrowser) And err.Number = 0 Then
						 SyncBrowser = 0 
						 WriteStatusToLogs "Specified Browser is Synchronised successfully."
					Else
						SyncBrowser = 1 
						WriteStatusToLogs "Specified Browser is not Synchronised successfully, please verify."
					End If
				Else
					SyncBrowser = 1
					WriteStatusToLogs "Specified Browser does not exist, please verify."
					
				End If
	On Error GoTo 0 
End Function

'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :				 VerifyBrowserExist()
'	Purpose :					 Verifies the existance of Browser in AUT.
'	Return Value :		 		  Integer(0/2)
'	Arguments :
'		Input Arguments :  		  objBrowser 
'		Output Arguments : 
'	Function is called by :
'	Function calls :			 CheckObjectExist() 
'	Created on :				      15/01/2009
'	Author :						  Rathna Reddy 
'
'------------------------------------------------------------------------------------------------------------------------------------------

Function VerifyBrowserExist(ByVal objBrowser)
	On Error Resume Next
		'Variable Section Begin
	
		   Dim strMethod

		'Variable Section End
		strMethod = strKeywordForReporting
			If CheckObjectExist(objBrowser) Then 
			   
					VerifyBrowserExist = 0
					WriteStatusToLogs "The specified Browser exists as expected."
					
			Else
					VerifyBrowserExist = 2
					WriteStatusToLogs "The specified Browser does not exist, please verify"
					
			End If 
	On Error GoTo 0
End Function

'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :				 VerifyBrowserNotExist()
'	Purpose :					 Verifies whether the Browser is NotExist or not in AUT?.
'	Return Value :		 		  Integer(0/2)
'	Arguments :
'		Input Arguments :  		  objBrowser 
'		Output Arguments : 
'	Function is called by :
'	Function calls :			 CheckObjectExist() 
'	Created on :				   15/01/2009
'	Author :						  Rathna Reddy 
'
'------------------------------------------------------------------------------------------------------------------------------------------

Function VerifyBrowserNotExist(ByVal objBrowser)
	On Error Resume Next
		'Variable Section Begin
	
		   Dim strMethod

		'Variable Section End
			strMethod = strKeywordForReporting
			If  CheckObjectNotExist(objBrowser) Then 
			   
					VerifyBrowserNotExist = 0
					WriteStatusToLogs "The specified Browser does not exist as expected."
					
			Else
					VerifyBrowserNotExist = 2
					WriteStatusToLogs "The specified Browser exists, please verify."
					
			End If 
	
  		
	On Error GoTo 0
End Function






'=======================Code Section End======================================================================

