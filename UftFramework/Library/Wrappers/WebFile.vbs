

'=======================Code Section Begin=============================================

Option Explicit		'Forces explicit declaration of all variables in a script. 
'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name :				 CompareWebFileDefaultValue()
' 	Purpose :						Compares the WebFile Default value with our expected value.
' 	Return Value :		 		  Integer( 0/1/2)
' 	Arguments :
'		Input Arguments :  		objWebFile , strExpValue
'		Output Arguments :     
'	Function is called by :
'	Function calls :				CheckObjectExist(),CompareStringValues()
'	Created on :				   15/01/2009
'	Author :						 Rathna Reddy 
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function CompareWebFileDefaultValue(ByVal objWebFile , ByVal strExpValue)
	On Error Resume Next
		'Variable Section Begin

			 Dim strMethod		
			 Dim strActValue

		 'Variable Section End

			strMethod = strKeywordForReporting

			If CheckObjectExist(objWebFile) Then
				   strActValue=objWebFile.GetRoProperty("default value")
					If Err.Number = 0 Then
							 If CompareStringValues(strExpValue,strActValue)=0  Then   
								CompareWebFileDefaultValue = 0
								WriteStatusToLogs "WebFile default value is same as the expected."
							 Else
								CompareWebFileDefaultValue = 2
								WriteStatusToLogs "WebFile default value is different from our expect, please verify."
							End If	
					Else
							CompareWebFileDefaultValue = 1	
							WriteStatusToLogs "Action fails to Capture the specified WebFile's default value, please verify  webfile default value property."

					End If
			Else
					CompareWebFileDefaultValue = 1		
					WriteStatusToLogs "The specified WebFile doesn't exist, please verify."
			End If
			
	On Error GoTo 0
End Function
'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name :				 SetValueInWebFile()
' 	Purpose :						 Enters a value into WebFile
' 	Return Value :		 		   Integer( 0/1/2)
' 	Arguments :
'		Input Arguments :  		objWebFile , strValue
'		Output Arguments : 
'	Function is called by :
'	Function calls :			    CheckObjectExist()
'	Created on :				   15/01/2009
'	Author :						 Rathna Reddy 
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function SetValueInWebFile(ByVal objWebFile , ByVal strValue)
	On Error Resume Next
		'Variable Section Begin
			
				Dim strMethod
				Dim strActValue

		'Variable Section End

		strMethod = strKeywordForReporting

			   If CheckObjectExist(objWebFile) Then   
					objWebFile.Object.focus
					objWebFile.Set strValue


					If Err.Number = 0 Then
						strActValue = objWebFile.GetRoProperty("value")
						If Err.Number = 0 Then
					
							If CompareStringValues(strValue,strActValue)=0  Then  
									SetValueInWebFile = 0

									WriteStatusToLogs "The Expected value was set in the specified WebFile successfully."
							Else
						
									SetValueInWebFile = 2
									WriteStatusToLogs "The Expected value was not set in the specified Webfile, please verify."
						  
							 End If
						Else
							SetValueInWebFile = 1
							WriteStatusToLogs "Action fails to Capture the specified WebFile's value, please verify Object value Properties."
						End If
					Else
		        		 SetValueInWebFile = 1
						 WriteStatusToLogs "Action fails to set the value in the specified WebFile, please verify."

				   End if

				   
				Else
					SetValueInWebFile = 1
					WriteStatusToLogs "The specified WebFile doesn't exist, please verify."
				End If
	On Error GoTo 0
End Function

'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name :				VerifyWebFileValue()
' 	Purpose :					 Verifies the WebFile value with our expected whenever required
' 	Return Value :		 		  Integer( 0/1/2)
' 	Arguments :
'		Input Arguments :  		objWebFile,strExpValue
'		Output Arguments :     strActValue
'	Function is called by :
'	Function calls :				CheckObjectExist(),CompareStringValues()
'	Created on :				   15/01/2009
'	Author :						 Rathna Reddy 
'Note: This is not default value verification.
'------------------------------------------------------------------------------------------------------------------------------------------
Function VerifyWebFileValue(ByVal objWebFile , ByVal strExpValue)
	On Error Resume Next
		 'Variable Section Begin
		
			Dim strMethod
			Dim strActValue

		'Variable Section End

		 strMethod = strKeywordForReporting

			If CheckObjectExist(objWebFile) Then  
					strActValue = objWebFile.GetROProperty("Value")
			
				   If Err.Number = 0 Then
                       
							If CompareStringValues(strExpValue,strActValue)  = 0 Then
								VerifyWebFileValue = 0
								WriteStatusToLogs "The specified WebFile value is same as the expected."

							Else
								VerifyWebFileValue = 2
								WriteStatusToLogs "The specified WebFile's value is different from the expect."

							End If
					Else
					     VerifyWebFileValue = 1
						 WriteStatusToLogs "Action fails to Capture WebFile  value, please verify Object Properties."

                    End If
			Else
					VerifyWebFileValue = 1
					WriteStatusToLogs "The specified WebFile doesn't exist, please verify."
            End If
			'WriteStepResults CompareWebFileValue
	On Error GOTO 0
End Function

'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name :				VerifyWebFileEnabled()
' 	Purpose :					 Verifies whether the WebFile is enabled.
' 	Return Value :		 		 Integer( 0/1/2)
'  	Arguments :
'		Input Arguments :  	   objWebFile 
'		Output Arguments : 
'	Function is called by :
'	Function calls :			   CheckObjectExist() , CheckObjectStatus()
'	Created on :				  15/01/2009
'	Author :						Rathna Reddy 
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function VerifyWebFileEnabled(ByVal objWebFile)

On Error Resume Next
	'Variable Section Begin
		
		Dim strMethod

	'Variable Section End

	 strMethod = strKeywordForReporting

		If CheckObjectExist(objWebFile) Then
			If CheckObjectEnabled(objWebFile) Then
					VerifyWebFileEnabled = 0
					WriteStatusToLogs "The WebFile is Enabled."
				Else
					VerifyWebFileEnabled = 2
					WriteStatusToLogs "The WebFile is Disabled."
				End If
            
		Else
				VerifyWebFileEnabled = 1
				WriteStatusToLogs "The specified WebFile doesn't exist."
		End if
		
	On Error GoTo 0
    
End Function
'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name :				VerifyWebFileDisabled()
' 	Purpose :					 Verifies whether the WebFile is Disabled.
' 	Return Value :		 		 Integer( 0/1/2)
'  	Arguments :
'		Input Arguments :  	   objWebFile 
'		Output Arguments : 
'	Function is called by :
'	Function calls :			   CheckObjectExist() , CheckObjectStatus()
'	Created on :				  15/01/2009
'	Author :						Rathna Reddy 
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function VerifyWebFileDisabled(ByVal objWebFile)

On Error Resume Next
	'Variable Section Begin
		
		Dim strMethod

	'Variable Section End

	strMethod = strKeywordForReporting

		If CheckObjectExist(objWebFile) Then
			If CheckObjectDisabled(objWebFile) Then
					VerifyWebFileDisabled = 0
					WriteStatusToLogs "The WebFile is Disabled."
				Else
					VerifyWebFileDisabled = 2
					WriteStatusToLogs "The WebFile is Enabled."
				End If
            
		Else
				VerifyWebFileDisabled = 1
				WriteStatusToLogs "The specified WebFile doesn't exist."
		End if
		
	On Error GoTo 0
    
End Function

'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :				 VerifyWebFileVisible()
'	Purpose :					 Verifies whether the WebFile is visible.
'	Return Value :		 		  Integer( 0/1/2)
'	Arguments :
'		Input Arguments :  		  objWebFile 
'		Output Arguments : 
'	Function is called by :
'	Function calls :			 CheckObjectExist() , CheckObjectVisiblity()
'	Created on :				  15/01/2009
'	Author :						  Rathna Reddy 
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function VerifyWebFileVisible(ByVal objWebFile)
	On Error Resume Next
		'Variable Section Begin
		
			Dim strMethod 

		'Variable Section End

		strMethod = strKeywordForReporting

			If CheckObjectExist(objWebFile)  Then 
				If CheckObjectVisible(objWebFile) Then
					VerifyWebFileVisible = 0
					WriteStatusToLogs "The WebFile is visible."
				Else
					VerifyWebFileVisible = 2
					WriteStatusToLogs "The WebFile is not visible, please verify."
				End If
			Else
					VerifyWebFileVisible = 1
					WriteStatusToLogs "The specified WebFile doesn't exist."
			End If
		
	On Error GoTo 0
End Function

'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :				 VerifyWebFileInVisible()
'	Purpose :					 Verifies whether the WebFile is Invisible.
'	Return Value :		 		  Integer( 0/1/2)
'	Arguments :
'		Input Arguments :  		  objWebFile 
'		Output Arguments : 
'	Function is called by :
'	Function calls :			 CheckObjectExist() , CheckObjectVisiblity()
'	Created on :				   15/01/2009
'	Author :						  Rathna Reddy 
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function VerifyWebFileInVisible(ByVal objWebFile)
	On Error Resume Next
		'Variable Section Begin
		
			Dim strMethod 

		'Variable Section End

		strMethod = strKeywordForReporting

			If CheckObjectExist(objWebFile)  Then 
				If CheckObjectInVisible(objWebFile) Then
					VerifyWebFileInVisible = 0
					WriteStatusToLogs "The WebFile is not visible."
				Else
					VerifyWebFileInVisible = 2
					WriteStatusToLogs "The WebFile is Visible,Please verify."
				End If
			Else
					VerifyWebFileInVisible = 1
					WriteStatusToLogs "The specified WebFile doesn't exist."
			End If
		
	On Error GoTo 0
End Function

'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :				 VerifyWebFileExist()
'	Purpose :					 Verifies whether the WebFile Exists or not?
'	Return Value :		 		  Integer( 0/2)
'	Arguments :
'		Input Arguments :  		  objWebFile 
'		Output Arguments : 
'	Function is called by :
'	Function calls :			 CheckObjectExist() 
'	Created on :				   15/01/2009
'	Author :						  Rathna Reddy 
'
'------------------------------------------------------------------------------------------------------------------------------------------

Function VerifyWebFileExist(ByVal objWebFile)
	On Error Resume Next
		'Variable Section Begin
	
		   Dim strMethod

		'Variable Section End

		strMethod = strKeywordForReporting

			If CheckObjectExist(objWebFile) Then 
			   
					VerifyWebFileExist = 0
					WriteStatusToLogs "The WebFile Exists."
					
			Else
					VerifyWebFileExist = 2
					WriteStatusToLogs "The specified WebFile does not exist, please verify."
					
			End If 
	  
	
	On Error GoTo 0
End Function

'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :				 VerifyWebFileNotExist()
'	Purpose :					 Verifies whether the WebFile Exists or not?
'	Return Value :		 		  Integer( 0/2)
'	Arguments :
'		Input Arguments :  		  objWebFile 
'		Output Arguments : 
'	Function is called by :
'	Function calls :			 CheckObjectExist() 
'	Created on :				   15/01/2009
'	Author :						  Rathna Reddy 
'
'------------------------------------------------------------------------------------------------------------------------------------------

Function VerifyWebFileNotExist(ByVal objWebFile)
	On Error Resume Next
		'Variable Section Begin
	
		   Dim strMethod

		'Variable Section End

		strMethod = strKeywordForReporting
		
			If CheckObjectNotExist(objWebFile) Then 
			   
					VerifyWebFileNotExist = 0
					WriteStatusToLogs "The WebFile does not exist."
					
			Else
					VerifyWebFileNotExist = 2
					WriteStatusToLogs "The WebFile exists, please verify."
					
			End If 
	
  		
	On Error GoTo 0
End Function
'=======================Code Section End=============================================
