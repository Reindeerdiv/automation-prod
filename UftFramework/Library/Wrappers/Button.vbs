
'=======================Code Section Begin============================================

Option Explicit		 'Forces explicit declaration of all variables in a script. 

'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :			  ClickButton()
'	Purpose :				  Performs  Click operation on a specified Button.
'	Return Value :		 	  Integer(0/1/2)
'	Arguments :
'		Input Arguments :  	 objButton
'		Output Arguments : 
'	Function is called by :
'	Function calls :		     CheckObjectExist() , CheckObjectClick()
'	Created on :			    10/03/2008
'	Author :					   Rathna Reddy 
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function ClickButton(ByVal objButton)
	On Error Resume Next
		'Variable Section Begin
	
			Dim strMethod 
		
		'Variable Section End
		strMethod = strKeywordForReporting
			If CheckObjectExist(objButton) Then 
			   If CheckObjectEnabled(objButton) AND CheckObjectVisible(objButton) Then
					If CheckObjectClick(objButton)  Then
						ClickButton = 0
						WriteStatusToLogs "Click is performed on the specified button successfully."
					Else
						ClickButton = 2
						WriteStatusToLogs "Click could not be performed on the specified button, please verify."
					End If 
				Else
					ClickButton = 1
					WriteStatusToLogs "The specified Button is disabled or its not visible and click could not be performed, please verify."
				End If
			  Else
                   ClickButton = 1
				   WriteStatusToLogs "The specified Button does not exist, please verify."
			  End If
			
		
	On Error GoTo 0
End Function
'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name :				 CompareButtonName()
' 	Purpose :						Compares the Button Name with our expected Name .
' 	Return Value :		 		  Integer(0/1/2)
' 	Arguments :
'		Input Arguments :  		objButton , strExpName
'		Output Arguments : 	   strActName
'	Function is called by :
'	Function calls :				CheckObjectExist(),CompareStringValues()				
'	Created on :				   10/03/2008
'	Author :						  Rathna Reddy 
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function CompareButtonName(ByVal objButton , ByVal strExpName)
	On Error Resume Next
		'Variable Section Begin
			Dim strMethod 
			Dim strActName

		'Variable Section End
		strMethod = strKeywordForReporting
			If CheckObjectExist(objButton) Then         
					strActName = objButton.GetRoProperty("name")
					If CompareStringValues(strExpName,strActName) = 0  Then
					   CompareButtonName = 0
					   WriteStatusToLogs """"&"Expected Name : "&strExpName& """" &  " and " &""""&"Actual Name : "&strActName&""""&" matches as expected."

					Else
						CompareButtonName = 2
						 WriteStatusToLogs """"&"Expected Name : "&strExpName& """" &  " and " &""""&"Actual Name : "&strActName&""""&" does not match, please verify."
					End If
				Else
					CompareButtonName = 1
					WriteStatusToLogs "The specified Button does not exist, please verify."
			End If
			
	On Error GoTo 0
End Function
'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name :				 VerifyButtonEnabled()
' 	Purpose :					 Verifies whether the Button is Enabled
' 	Return Value :		 		  Integer(0/1/2)
' 	Arguments :
'		Input Arguments :  		objButton 
'		Output Arguments : 
'	Function is called by :
'	Function calls :				CheckObjectExist() , CheckObjectEnabled()
'	Created on :				   11/03/2008
'	Author :						  Rathna Reddy 
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function VerifyButtonEnabled(ByVal objButton)

On Error Resume Next
	'Variable Section Begin
		
		Dim strMethod 

	'Variable Section End
	strMethod = strKeywordForReporting
		If CheckObjectExist(objButton) Then
			If CheckObjectEnabled(objButton) Then
					VerifyButtonEnabled = 0
					WriteStatusToLogs "The specified Button is enabled as expected."
				Else
					VerifyButtonEnabled = 2
					WriteStatusToLogs "The specified Button is not enabled, please verify."
				End If
            
		Else
				VerifyButtonEnabled = 1
				WriteStatusToLogs "The specified Button does not exist, please verify."
		End if
		
	On Error GoTo 0
    
End Function

'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name :				 VerifyButtonDisabled()
' 	Purpose :					 Verifies whether the Button is Disabled
' 	Return Value :		 		  Integer(0/1/2)
' 	Arguments :
'		Input Arguments :  		objButton 
'		Output Arguments : 
'	Function is called by :
'	Function calls :				CheckObjectExist() , CheckObjectDisabled()
'	Created on :				   11/03/2008
'	Author :						  Rathna Reddy 
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function VerifyButtonDisabled(ByVal objButton)

On Error Resume Next
	'Variable Section Begin
		
		Dim strMethod 

	'Variable Section End
	strMethod = strKeywordForReporting
		If CheckObjectExist(objButton) Then
			If CheckObjectDisabled(objButton) Then
					VerifyButtonDisabled = 0
					WriteStatusToLogs "The specified Button is disabled as expected."
				Else
					VerifyButtonDisabled = 2
					WriteStatusToLogs "The specified Button is not disabled, please verify."
				End If
            
		Else
				VerifyButtonDisabled = 1
				WriteStatusToLogs "The specified Button does not exist, please verify."
		End if
	On Error GoTo 0
End Function

'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :				 VerifyButtonVisible()
'	Purpose :					 Verifies whether the Button is visible.
'	Return Value :		 		  Integer(0/1/2)
'	Arguments :
'		Input Arguments :  		  objButton 
'		Output Arguments : 
'	Function is called by :
'	Function calls :			 CheckObjectExist() , CheckObjectVisible()
'	Created on :				   10/03/2008
'	Author :						  Rathna Reddy 
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function VerifyButtonVisible(ByVal objButton)
	On Error Resume Next
		'Variable Section Begin
		
			Dim strMethod 

		'Variable Section End
		strMethod = strKeywordForReporting
			If CheckObjectExist(objButton)  Then 
				If CheckObjectVisible(objButton) Then
					VerifyButtonVisible = 0
					WriteStatusToLogs "The specified Button is visible as expected."
				Else
					VerifyButtonVisible = 2
					WriteStatusToLogs "The specified Button is not visible, please verify."
				End If
			Else
					VerifyButtonVisible = 1
					WriteStatusToLogs "The specified Button does not exist, please verify."
			End If
		
	On Error GoTo 0
End Function
'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :				 VerifyButtonInVisible()
'	Purpose :					 Verifies whether the Button is Invisible.
'	Return Value :		 		  Integer(0/1/2)
'	Arguments :
'		Input Arguments :  		  objButton 
'		Output Arguments : 
'	Function is called by :
'	Function calls :			 CheckObjectExist() , CheckObjectInVisible()
'	Created on :				   10/03/2008
'	Author :						  Rathna Reddy 
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function VerifyButtonInVisible(ByVal objButton)
	On Error Resume Next
		'Variable Section Begin
		
			Dim strMethod 

		'Variable Section End
		strMethod = strKeywordForReporting
			If CheckObjectExist(objButton)  Then 
				If CheckObjectInVisible(objButton) Then
					VerifyButtonInVisible = 0
					WriteStatusToLogs "The specified Button is in-visible as expected."
				Else
					VerifyButtonInVisible = 2
					WriteStatusToLogs "The specified Button is not in-visible, please verify."
				End If
			Else
					VerifyButtonInVisible = 1
					WriteStatusToLogs "The specified Button does not exist, please verify."
			End If
		
	On Error GoTo 0
End Function
'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :				 VerifyButtonExist()
'	Purpose :					 Verifies whether the Button Exists.
'	Return Value :		 		  Integer(0/2)
'	Arguments :
'		Input Arguments :  		  objObject 
'		Output Arguments : 
'	Function is called by :
'	Function calls :			 CheckObjectExist() 
'	Created on :				   10/03/2008
'	Author :						  Rathna Reddy 
'
'------------------------------------------------------------------------------------------------------------------------------------------

Function VerifyButtonExist(ByVal objButton)
	On Error Resume Next
		'Variable Section Begin
	
		   Dim strMethod 

		'Variable Section End
		strMethod = strKeywordForReporting
			If CheckObjectExist(objButton) AND CheckObjectVisible(objButton) Then 
			   
					VerifyButtonExist = 0
					WriteStatusToLogs "The specified Button exists as expected."
					
			Else
					VerifyButtonExist = 2
					WriteStatusToLogs "The specified Button does NOT exist, please verify."
			End If 
	
	On Error GoTo 0
End Function

'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :				 VerifyButtonNotExist()
'	Purpose :					 Verifies whether the Button does NOT Exists.
'	Return Value :		 		  Integer(0/2)
'	Arguments :
'		Input Arguments :  		  objObject 
'		Output Arguments : 
'	Function is called by :
'	Function calls :			 CheckObjectExist() 
'	Created on :				   10/03/2008
'	Author :						  Rathna Reddy 
'
'------------------------------------------------------------------------------------------------------------------------------------------

Function VerifyButtonNotExist(ByVal objButton)
	On Error Resume Next
		'Variable Section Begin
	
		   Dim strMethod 

		'Variable Section End
			strMethod = strKeywordForReporting
			If CheckObjectNotExist(objButton) Then 
			   
					VerifyButtonNotExist = 0
					WriteStatusToLogs "The specified Button does not exist as expected"
					
			Else
					VerifyButtonNotExist = 2
					WriteStatusToLogs "The specified Button exists, please Verify."
					
			End If 
	
  		
	On Error GoTo 0
End Function

'=======================Code Section End=============================================
