

'=======================Code Section Begin=============================================
Option Explicit		 'Forces explicit declaration of all variables in a script. 
'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name :				 ComparePageTitle()
' 	Purpose :					 Compares the Page title with our expected title
' 	Return Value :		 		 Integer( 0/1/2)
' 	Arguments :
'		Input Arguments :  		objPage , strExpTitle
'		Output Arguments :		strActTitle 
'	Function is called by :
'	Function calls :				CheckObjectExist() , CompareStringValues()
'	Created on :				   05/03/2008
'	Author :						  Rathna Reddy 
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function ComparePageTitle (ByVal objPage , ByVal strExpTitle)
	On Error Resume Next
		'Variable Section Begin
		
			Dim strMethod
			Dim strActTitle

		'Variable Section End

		strMethod = strKeywordForReporting

			If CheckObjectExist(objPage) Then  
			
					 strActTitle = objPage.GetRoProperty("title")

					 If Err.number = 0 Then
					 
							If CompareStringValues(strExpTitle, strActTitle) = 0  Then
								
								ComparePageTitle = 0
								WriteStatusToLogs """"&"Expected Title : "&strExpTitle& """" &  "   and   " &""""&"Actual Title : "&strActTitle&""""&"  are equal."
							Else
								ComparePageTitle = 2
								WriteStatusToLogs """"&"Expected Title : "&strExpTitle& """" &  "   and   " &""""&"Actual Title : "&strActTitle&""""&"  are not equal, please verify."
							End If

					 Else
							ComparePageTitle = 1
							WriteStatusToLogs "The Action fails to capture the title of the specified Page, please verify the object properties."
					 End If 
			Else
					ComparePageTitle = 1
					WriteStatusToLogs "The specified Page does not exist, please verify."
			End If
            
	On Error GoTo 0
End Function

'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name :				 ComparePageURL()
' 	Purpose :					 Compares the Page URL with our expected URL
' 	Return Value :		 		  Integer( 0/1/2)
' 	Arguments :
'		Input Arguments :  		objPage , strExpUrl
'		Output Arguments :      strActUrl
'	Function is called by :
'	Function calls :			   CheckObjectExist(),CompareStringValues()
'	Created on :				  05/03/2008
'	Author :						Rathna Reddy 
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function ComparePageURL(Byval objPage , ByVal strExpUrl)' Url of Page
	On Error Resume Next
		'Variable Section Begin
		
			Dim strMethod
			Dim strActUrl

		'Variable Section End

		strMethod = strKeywordForReporting

			If CheckObjectExist(objPage) Then
				
				strActUrl = objPage.GetRoProperty("URL")
				If Err.number = 0 Then

						If CompareStringValues(strExpUrl, strActUrl)=0  Then 
							ComparePageURL = 0
							WriteStatusToLogs """"&"Expected URL : "&strExpUrl& """" &  "   and   " &""""&"Actual URL : "&strActUrl&""""&"  are equal."
						Else
							ComparePageURL = 2
							WriteStatusToLogs """"&"Expected URL : "&strExpUrl& """" &  "   and   " &""""&"Actual URL : "&strActUrl&""""&"  are not equal, please verify."
						End If

				Else
						ComparePageURL = 1
						WriteStatusToLogs "The Action fails to capture the specified Page's URL, please verify object properties."
				End If
			Else
					ComparePageURL = 1
					 WriteStatusToLogs "The specified Page does not exist, please verify."
			End If
	On Error GoTo 0
End Function

'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name :				 SyncPage()
' 	Purpose :						Waits until specified page is loaded
' 	Return Value :		 		  Integer( 0/1)
' 	Arguments :
'		Input Arguments :  		objPage
'		Output Arguments : 
'	Function is called by :
'	Function calls :				CheckObjectExist() , CheckObjectSync()
'	Created on :				   05/03/2008
'	Author :						 Rathna Reddy 
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function SyncPage(byval objPage)
	On Error Resume Next
		'Variable Section Begin
		
			Dim strMethod 

		'Variable Section End

		strMethod = strKeywordForReporting
		
			If CheckObjectExist(objPage) Then 
					If CheckObjectSync(objPage) Then
						SyncPage = 0
						WriteStatusToLogs "The specified object sync pass"
					Else
						SyncPage = 1
						WriteStatusToLogs "The specified object sync fails, please verify."
					End If
			Else
					SyncPage = 1
					WriteStatusToLogs "The specified object doesn't exist, please verify."
			End If
	
	On Error GoTo 0
End Function
'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :				  ClickLinkByNameInPage()
' 	Purpose :						Performs click operation on specified Link in a page.
' 	Return Value :		 		 	Integer( 0/1)
' 	Arguments :
'		Input Arguments :  		 objPage , strLinkName
'		Output Arguments :      intLinkCount,intCount,strObjObjects,strObjDesc
'	Function is called by :
'	Function calls :				CheckObjectExist(),CheckObjectClick()
'	Created on :				   20/03/2008
'	Author :						 Rathna Reddy 
'
'------------------------------------------------------------------------------------------------------------------------------------------

Function ClickLinkByNameInPage(ByVal objPage, ByVal strLinkName)
    On Error Resume Next
     'Variable Section Begin
		
			Dim strMethod
			Dim  intLinkCount,intCount,strObjObjects,strObjDesc,intVal , strInnerText
			intCount=0

		'Variable Section End

		strMethod = strKeywordForReporting
	 
	If CheckObjectExist(objPage) Then     
			 Set strObjDesc = Description.Create()
			 strObjDesc("micclass").Value = "Link"
			 Set strObjObjects= objPage.ChildObjects(strObjDesc) 
			 If Err.Number = 0 Then
						  intLinkCount= strObjObjects.count
						  If intLinkCount > 0 Then

									For  intCount=0 to intLinkCount-1
											  strInnerText=strObjObjects(intCount).GetROProperty("innertext")
											  If Err.Number = 0 then
													  If Trim(strInnerText) = Trim(Cstr(strLinkName)) Then
																intVal=intVal+1
																If CheckObjectEnabled(strObjObjects(intCount)) Then	
																		If CheckObjectClick (strObjObjects(intCount)) Then
																		
																			 ClickLinkByNameInPage = 0
																			 WriteStatusToLogs "Click Operation was performed on: "&""""&strLinkName&""""&" LINK successfully."
																															
																		Else
																					
																			 ClickLinkByNameInPage = 1
																			 WriteStatusToLogs "Click Operation could not be performed on: "&""""&strLinkName&""""&" LINK successfully."
																					
																																		
																		End If

																Else
																		 ClickLinkByNameInPage = 1	
																		 WriteStatusToLogs "Link :"&""""&strLinkName&""""&" is in disabled state, please verify."
																				
																End If
																
																Exit for	
													  End If
												Else
														ClickLinkByNameInPage = 1
														WriteStatusToLogs "The Action fails to capture the link InnerText , please verify."
																					
												End If
									Next
									
									If intVal = 0 Then
											ClickLinkByNameInPage = 1
											WriteStatusToLogs "The specified Page Does not contain the Link: "&""""&strLinkName&""""
							        End If

						Else

							ClickLinkByNameInPage = 1
							WriteStatusToLogs "There are no links in the Specified Page, please verify."
						End If

			 Else
			
					ClickLinkByNameInPage = 1
					WriteStatusToLogs "An error occurred while checking the Link existence in page, please verify."
			End If

	Else
		 ClickLinkByNameInPage = 1
		 WriteStatusToLogs "Page does not exist, please verify."
	End If
  On Error GoTo 0 

End Function



' '------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :				  ClickButtonByNameInPage()
' 	Purpose :						Performs click operation on specified button  in page 
' 	Return Value :		 		 	Integer( 0/1)
' 	Arguments :
'		Input Arguments :  		 objPage , strButtonName
'		Output Arguments :      intButtonCount,strObjObjects,strObjDesc,intCount
'	Function is called by :
'	Function calls :				CheckObjectExist(),CheckObjectClick()
'	Created on :				   20/03/2008
'	Author :						 Rathna Reddy 
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function ClickButtonByNameInPage(ByVal objPage, ByVal strButtonName)
    On Error Resume Next
     'Variable Section Begin
		
			Dim strMethod
			Dim  intButtonCount,intCount,strObjObjects,strObjDesc,intVal, strActualBtnName
			intCount=0

		'Variable Section End

		strMethod = strKeywordForReporting
	 
	If CheckObjectExist(objPage) Then     
			 Set strObjDesc = Description.Create()
					strObjDesc("micclass").Value = "WebButton"
						  Set strObjObjects= objPage.ChildObjects(strObjDesc) 
						  If Err.Number = 0 Then
								  intButtonCount= strObjObjects.count
									  If intButtonCount > 0 Then

											For  intCount=0 to intButtonCount-1
													  strActualBtnName=strObjObjects(intCount).GetROProperty("name")
													  If Err.number = 0 Then
													  
															  If Trim(strActualBtnName) = Trim(Cstr(strButtonName)) Then
																	intVal=intVal+1
																	If CheckObjectEnabled(strObjObjects(intCount)) Then	
																			If CheckObjectClick (strObjObjects(intCount)) Then
																			
																				 ClickButtonByNameInPage = 0
																				 WriteStatusToLogs "Click Operation was performed on: "&""""&strButtonName&""""&" BUTTON successfully."
																			
																			Else
																						
																				 ClickButtonByNameInPage = 1
																				 WriteStatusToLogs "Click Operation could not be performed on: "&""""&strButtonName&""""&" BUTTON successfully."
																						
																																			
																			End If

																	Else
																			ClickButtonByNameInPage = 1
																			WriteStatusToLogs "Button :"&""""&strButtonName&""""&" is in disabled state, please verify."
																					
																	

																	
																	End If

																	Exit For 
																End If
													  Else
																ClickButtonByNameInPage = 1
																WriteStatusToLogs "Action fails to capture the inner text of the button having the index "&intCount&" , please verify."
																			
													  End If
											Next
											      

													 If intVal = 0 Then
															ClickButtonByNameInPage = 1
															WriteStatusToLogs "Page Does not contain the Button: "&""""&strButtonName&""""
											
											
													End If



										Else
												ClickButtonByNameInPage = 1
												WriteStatusToLogs "There is no Buttons in page, please verify."
										End If
						

						Else
		
        ClickButtonByNameInPage = 1
	    WriteStatusToLogs "An error occurred while checking the Buttons existence in page, please verify."



		End If

					
	Else
		ClickButtonByNameInPage = 1
		 WriteStatusToLogs "The specified Page does not exist, please verify."
	End If
  On Error GoTo 0 
End Function

''--------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :				  VerifyAllCheckBoxesUnCheckedInPage()
' 	Purpose :					  Verifies the  all the checkboxes unchecked state in a page.
' 	Return Value :		 		 	Integer( 0/1/2)
' 	Arguments :
'		Input Arguments :  		 objPage 
'		Output Arguments :       intCheckBoxCount,intCount,strObjObjects,strObjDesc,intVal
'	Function is called by :
'	Function calls :				CheckObjectExist()
'	Created on :				   20/03/2008
'	Author :						 Rathna Reddy 
'
'------------------------------------------------------------------------------------------------------------------------------------------


Function VerifyAllCheckBoxesUnCheckedInPage(ByVal objPage)

    On Error Resume Next
     'Variable Section Begin

	Dim strMethod
	Dim  intCheckBoxCount,intCount,strObjObjects,strObjDesc,intVal
	intVal=0
	
			      

	'Variable Section End

	strMethod = strKeywordForReporting
	 
    If CheckObjectExist(objPage) Then     
        Set strObjDesc = Description.Create()
		strObjDesc("micclass").Value = "WebCheckBox"
        Set strObjObjects= objPage.ChildObjects(strObjDesc)
		If err.Number = 0 Then
		
			intCheckBoxCount= strObjObjects.count
            If intCheckBoxCount > 0 Then
					For  intCount=0 to intCheckBoxCount-1
							If IsObject(strObjObjects(intCount)) And Not Isempty(strObjObjects(intCount))  Then
									'If strObjObjects(intCount).GetROProperty("disabled") = 0 Then 
											If strObjObjects(intCount).GetROProperty("Checked") = Cint("1")  Then 
												intVal=intVal+1
												Exit For
											End if
									'End If
							End If
					Next

				If intVal=cint(0) Then
					
						VerifyAllCheckBoxesUnCheckedInPage = 0
						WriteStatusToLogs "All CheckBoxes are in UnChecked State."
				Else
					'msgbox "fail"
						VerifyAllCheckBoxesUnCheckedInPage  = 2
						WriteStatusToLogs "All CheckBoxes are not in UnChecked state , please verify."
				End If
			Else
				VerifyAllCheckBoxesUnCheckedInPage = 1
				WriteStatusToLogs "There are no CheckBoxes in the specified Page, please verify."

			End If

      Else
	    VerifyAllCheckBoxesUnCheckedInPage = 1
	    WriteStatusToLogs "An error occurred while checking the CheckBoxes existence in page, please verify."

	  End If
			
			
	Else
			VerifyAllCheckBoxesUnCheckedInPage = 1
			WriteStatusToLogs "The specified Page does not exist, please verify."
	End If
    On Error GoTo 0 
End Function







''--------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :				  VerifyAllCheckBoxesCheckedInPage()
' 	Purpose :					  Verifies the  all the checkboxes checked state in a page.
' 	Return Value :		 		 	Integer( 0/1/2)
' 	Arguments :
'		Input Arguments :  		 objPage 
'		Output Arguments :       intCheckBoxCount,intCount,strObjObjects,strObjDesc,intVal
'	Function is called by :
'	Function calls :				CheckObjectExist()
'	Created on :				   20/03/2008
'	Author :						 Rathna Reddy 
'
'------------------------------------------------------------------------------------------------------------------------------------------

Function VerifyAllCheckBoxesCheckedInPage(ByVal objPage)

    On Error Resume Next
     'Variable Section Begin
		
	Dim strMethod
	Dim  intCheckBoxCount,intCount,strObjObjects,strObjDesc,intVal
	intVal=0
	
			      

	'Variable Section End

	strMethod = strKeywordForReporting
	 
    If CheckObjectExist(objPage) Then     
        Set strObjDesc = Description.Create()
		strObjDesc("micclass").Value = "WebCheckBox"
        Set strObjObjects= objPage.ChildObjects(strObjDesc)
	   	If err.Number = 0 Then
				intCheckBoxCount= strObjObjects.count
				If intCheckBoxCount > 0 Then
						 For  intCount=0 to intCheckBoxCount-1
								If IsObject(strObjObjects(intCount)) And Not Isempty(strObjObjects(intCount))  Then
										'If strObjObjects(intCount).GetROProperty("disabled") = 0 Then 
												If strObjObjects(intCount).GetROProperty("Checked") = Cint("0")  Then 
													intVal=intVal+1
													Exit For
												End if
									   ' End If
								End If
						Next

			   
				If intVal=cint(0) Then
					
						VerifyAllCheckBoxesCheckedInPage = 0
						WriteStatusToLogs "All CheckBoxes are in Checked State."
				Else
					'msgbox "fail"
						VerifyAllCheckBoxesCheckedInPage  = 2
						WriteStatusToLogs "All CheckBoxes are not in Checked State , please verify."
				End If
			Else
			VerifyAllCheckBoxesCheckedInPage = 1
			WriteStatusToLogs "There are no CheckBoxes in the specified Page, please verify."

			End If
		 Else
	    VerifyAllCheckBoxesCheckedInPage = 1
	    WriteStatusToLogs "An error occurred while checking the CheckBoxes existence in page, please verify."

	  End If
			
	Else
			VerifyAllCheckBoxesCheckedInPage = 1
			WriteStatusToLogs "The specified Page was not exist, please verify."
	End If
	
    On Error GoTo 0 
End Function

 
'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :				 VerifyPageExist()
'	Purpose :					 Verifies whether the Page Exists or not?
'	Return Value :		 		  Integer( 0/2)
'	Arguments :
'		Input Arguments :  		  objObject 
'		Output Arguments : 
'	Function is called by :
'	Function calls :			 CheckObjectExist() 
'	Created on :				   10/03/2008
'	Author :						  Rathna Reddy 
'
'------------------------------------------------------------------------------------------------------------------------------------------

Function VerifyPageExist(ByVal objPage)
	On Error Resume Next
		'Variable Section Begin
	
		   Dim strMethod

		'Variable Section End

		strMethod = strKeywordForReporting

			If CheckObjectExist(objPage) Then 
			   
					VerifyPageExist = 0
					WriteStatusToLogs "The Page  Exist."
					
			Else
					VerifyPageExist = 2
					WriteStatusToLogs "The specified Page Doesn't Exist."
					
			End If 
	  
	
	On Error GoTo 0
End Function

'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :				 VerifyPageNotExist()
'	Purpose :					 Verifies whether the Page Exists or not?
'	Return Value :		 		  Integer( 0/2)
'	Arguments :
'		Input Arguments :  		  objObject 
'		Output Arguments : 
'	Function is called by :
'	Function calls :			 CheckObjectExist() 
'	Created on :				   10/03/2008
'	Author :						  Rathna Reddy 
'
'------------------------------------------------------------------------------------------------------------------------------------------

Function VerifyPageNotExist(ByVal objPage)
	On Error Resume Next
		'Variable Section Begin
	
		   Dim strMethod

		'Variable Section End
		
		strMethod = strKeywordForReporting

			If CheckObjectNotExist(objPage) Then 
			   
					VerifyPageNotExist = 0
					WriteStatusToLogs "The Page Doesn't Exist"
					
			Else
					VerifyPageNotExist = 2
					WriteStatusToLogs "The Page  Exist, please verify."
					
			End If 
	
  		
	On Error GoTo 0
End Function

                    

'=======================Code Section End=============================================
