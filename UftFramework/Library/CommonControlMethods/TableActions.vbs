
Function StoreDescribeProperty(oTable, PropertyList, FilePath)
	On error resume next
	Dim strMethod
	Dim  arrData, objFSO, objExcel, objWorkbook, data, i
	
	strMethod = strKeywordForReporting
	If (PropertyList = "" ) Then
		StoreDescribeProperty = 1
		WriteStatusToLogs "Property list is empty."
		Exit Function
	End If
	
	If (filePath = "" ) Then
		StoreDescribeProperty = 1
		WriteStatusToLogs "File path is empty."
		Exit Function
	End If
	
	err.clear
	If CheckObjectExist(oTable) Then
			data = oTable.Describe(PropertyList)
	End If
	
	

	If err.number <> 0 Then
	    StoreDescribeProperty = 1
		WriteStatusToLogs err.description
		Exit Function
	Else
	

		arrData = split(Data, vbcrlf)
		' Check if file exists.
		Set objFSO = CreateObject("Scripting.FileSystemObject")
		If (objFSO.FileExists(FilePath) = True) Then
		  objFSO.DeleteFile(FilePath)
			Set objFSO = Nothing
		End If
		' Open specified spreadsheet and select the first worksheet.
		
		Set objExcel = CreateObject("Excel.Application")
		Set objWorkbook = objExcel.Workbooks.Add()
		objWorkbook.SaveAs(FilePath)
		
		objExcel.WorkBooks.Open FilePath
		Set objSheet = objExcel.ActiveWorkbook.Worksheets(1)
		
		For i = 0 To Ubound(arrData)
			objSheet.Cells(i+1,1) = arrData(i)
		Next
		' Save and quit.
		objExcel.ActiveWorkbook.Save
		objExcel.ActiveWorkbook.Close
		objExcel.Application.Quit
		Set objWorkbook = Nothing
		Set objExcel = Nothing
		If err.number <> 0 Then
		StoreDescribeProperty = 1
		WriteStatusToLogs "Failed to save data in excel file: "& err.description
		Else
		StoreDescribeProperty = 0
		WriteStatusToLogs "Successfully stored describe property."
		End If
	End If

	On error goto 0
End Function

Function SelectCellandType(oTable, row,column,data)
	On error resume next
	Dim strMethod
	
	strMethod = strKeywordForReporting
	If (row = "" or column = "") Then
		SelectCellandType = 1
		WriteStatusToLogs "Parameters are empty."
		Exit Function
	End If
	
	err.clear
	oTable.ActivateCell row,column
	If err.number <> 0 Then
	    SelectCellandType = 1
		WriteStatusToLogs err.description
		Exit Function
	End If
	oTable.Type data
	If err.number <> 0 Then
	    SelectCellandType = 1
		WriteStatusToLogs err.description
		Else 
		SelectCellandType = 0
		WriteStatusToLogs "Table cell is selected and data entered. "
	End If

End Function

Function StoreRowNumberByColumnData(byval objTable, byval arrColumns, byval arrColumnValues, ByVal key )
	 On Error Resume Next
	 Dim  rows,r,val, found, strMethod, colum
	 Dim NumberOfTimesTextIsMatched,rowFound, intval, TblColumns
	 
	 strMethod = strKeywordForReporting
	 rowFound = False
	 	
	
	If Not (isobject(objTable) or isempty(arrColumns) or isempty(arrColumnValues) or trim(Key) = "") Then
	
		WriteStatusToLogs "Invalid parameters" 
		StoreRowNumberByColumnData= 1
		Exit Function
	End If
	
	If isarray(arrColumns) Then
		If NOT(Ubound(arrColumns) = UBound(arrColumnValues)) Then
			WriteStatusToLogs "Columns and ColumnValue array length do not match." 
			StoreRowNumberByColumnData= 1
			on error goto 0
			Exit Function
		End If
	End If
	

	 If CheckObjectExist(objTable)  Then
		  rows = objTable.Object.ApproxCount
		  Set TblColumns = objTable.Object.Columns
			err.number = 0
			For r = 1 To rows
						If r=1 Then
							objTable.Object.MoveFirst
						Else
							objTable.Object.MoveNext
						End If
						NumberOfTimesTextIsMatched =0
				   For colum = 0 To UBound(arrColumns)
				   		
				   		val =Trim(TblColumns(arrColumns(cint(colum))))
				   		If StrComp(val,arrColumnValues(colum),1) = 0 Then
				   			NumberOfTimesTextIsMatched = NumberOfTimesTextIsMatched +1
				   		Else
				   			Exit for
				   		End If
				   Next
				   
				   If NumberOfTimesTextIsMatched-1 = UBound(arrColumns) Then 'Matching row is found
				   	rowFound = true
				   	Exit for 
				   End If
			Next
		  
			  If rowFound Then
			   intval = AddItemToStorageDictionary(key,r)
				If intval = 0 then
					StoreRowNumberByColumnData = 0
					  WriteStatusToLogs "Row number is successfully stored " & r
				Else
					StoreRowNumberByColumnData = 1
					objTable.Object.MoveFirst
					  WriteStatusToLogs "Row number could not be stored."
				End If
			  Else
			  	objTable.Object.MoveFirst
			   	StoreRowNumberByColumnData = 1
				WriteStatusToLogs "Could not find the specified cell in table."
			  End If
	 Else
	    StoreRowNumberByColumnData = 1
	    WriteStatusToLogs "Table object does not found. "
	 End If
	on error goto 0
End function


Function StoreRowCountNativeProperty(objTable,key)
	 On Error Resume Next
	 Dim  rows, intval, strMethod
	 Dim NumberOfTimesTextIsMatched,rowFound, TblColumns
	 
	 strMethod = strKeywordForReporting
	 
	 
	If Not (isobject(objTable) or trim(Key) = "") Then
		WriteStatusToLogs "Invalid parameters" 
		StoreRowCountNativeProperty= 1
		Exit Function
	End If
	err.clear
	rows = objTable.Object.ApproxCount
	
	If err.number = 0 Then 
		intval = AddItemToStorageDictionary(key,rows)
		If intval = 0 then 
		StoreRowCountNativeProperty = 0
				WriteStatusToLogs "Row count '"&rows&"' is successfully stored in the key: " & key
					  
		Else
		StoreRowCountNativeProperty = 1
				WriteStatusToLogs "Error occurred while storing the row count."
		
		End If
	Else
		StoreRowCountNativeProperty = 1
			WriteStatusToLogs "Error occurred while finding row count, " & err.description
	End If
	
	on error goto 0
End Function

Function StoreArrayOfColumDataUsingLookup(oTable, ReferenceColumnNumber, LookupData, TargetColumnNumber, TextComparision, arrUser)
	
	On error resume Next
	Dim arrIndex, rowcount, arrInternal, i,j, referenceData, getCellDatatargetCol, strMethod, found
	found = 0
	logs.Debug ("Action Execution Start >>> StoreArrayOfColumDataUsingLookup")
	strMethod = strKeywordForReporting
	arrIndex =0
	

	arrInternal = Array()
	
	If arrUser="" Then
	StoreArrayOfColumDataUsingLookup =1
		 WriteStatusToLogs "Invalid Parameters"
		  Exit Function
	End If
	If ucase(TextComparision) = "TRUE" Then
		TextComparision = 1
	ElseIf  ucase(TextComparision) = "FALSE" Then
		TextComparision = 0
	Else
		StoreArrayOfColumDataUsingLookup =1
		WriteStatusToLogs "Invalid Parameter 'TextComparision'"
		  Exit Function
	End If
	
	If CheckObjectExist(oTable) Then
		rowcount = oTable.RowCount
		
			If isempty(rowcount) Then
				StoreArrayOfColumDataUsingLookup =1
				WriteStatusToLogs "Table has no rows."
				  Exit Function
			End If			  
		logs.Debug ("Total row count of the table >>> "& rowcount)
	Else
		StoreArrayOfColumDataUsingLookup =1
	    WriteStatusToLogs "Object does not exist."
		  Exit Function
	End If
	For i =1 To rowcount
		err.clear	
		referenceData = oTable.GetCellData(i,ReferenceColumnNumber)
		If referenceData = "ERROR: The specified cell does not exist." Then
				j = j+1
		End if 
		 
		If err.number <> 0 Then
			StoreArrayOfColumDataUsingLookup =1
			WriteStatusToLogs err.description
			Exit Function
		End If
		If StrComp(referenceData,LookupData,TextComparision) = 0 Then
		found = 1
			err.clear
			getCellDatatargetCol = oTable.GetCellData(i,TargetColumnNumber)
			If err.number <> 0 or getCellDatatargetCol = "ERROR: The specified cell does not exist." Then
			StoreArrayOfColumDataUsingLookup =1
			WriteStatusToLogs "The specified cell does not exist in target column." & err.description
			  Exit Function
			End If
			ReDim preserve arrInternal(arrIndex)
			arrInternal(arrIndex) = getCellDatatargetCol
			arrIndex = arrIndex + 1
		End If
	Next
	if j=rowcount then
			StoreArrayOfColumDataUsingLookup =1
			WriteStatusToLogs "Specified cell is not found in reference column." 
			  Exit Function
	Elseif found =0 Then
			StoreArrayOfColumDataUsingLookup =1
			WriteStatusToLogs "Specified cell is not found in target column." 
			  Exit Function
	End if
	execute (arrUser & "= Array()")
	execute (arrUser & "= arrInternal")
	'Execute ("StoreArrayOfColumDataUsingLookup = " & arrUser)
	If err.number =0 Then
		StoreArrayOfColumDataUsingLookup =0
		WriteStatusToLogs "Array values are:" & join(arrInternal,",")
	Else
			StoreArrayOfColumDataUsingLookup =1
			WriteStatusToLogs Error.description
	End If
	logs.Debug ("Action Execution Finished >> StoreArrayOfColumDataUsingLookup.")
	On error goto 0
End Function


Function StoreRowNumberMatchingPattern(byval objTable, byval arrColumns, ByVal arrPattern ,byVal IgnoreCase, ByVal key )
	
	 On Error Resume Next
	 Dim  rows,r,val, found, strMethod, colum, re
	 Dim NumberOfTimesTextIsMatched,rowFound, intval, TblColumns
	 
	 strMethod = strKeywordForReporting
	 rowFound = False
	 arrColumns = split(arrColumns, ",")
	 arrPattern = split(arrPattern, ",")
	 
	 If Not (UCase(IgnoreCase) = "TRUE" or ucase(IgnoreCase) = "FALSE") Then
		StoreRowNumberMatchingPattern = 1
		  WriteStatusToLogs "Invalid parameter."
		  exit function
	 End If
	 
	 If not(ubound(arrColumns)  = ubound(arrPattern)) Then
		StoreRowNumberMatchingPattern = 1
		  WriteStatusToLogs "Invalid parameter. Length of arrays are not equal. "
		  exit function
	 End If
	
	 If objTable.exist  Then
		  rows = objTable.RowCount
'		  Set TblColumns = objTable.Object.Columns
		  colCount = objTable.ColumnCount
			err.number = 0
			For r = 1 To rows
						NumberOfTimesTextIsMatched =0
				   For colum = 0 To UBound(arrColumns)
				   
				   		  Set re = New RegExp
						  With re
						      .Pattern    = arrPattern(colum)
						      .IgnoreCase = IgnoreCase
						      .Global     = False
						  End With
				   		val = objTable.GetCellData(r, arrColumns(colum))
'				   		val =Trim(TblColumns(arrColumns(cint(colum))))
				   		If re.Test( val ) Then
				   			NumberOfTimesTextIsMatched = NumberOfTimesTextIsMatched +1
				   		Else
				   			Exit for
				   		End If
						Set re = nothing
				   Next
				   
				   If NumberOfTimesTextIsMatched-1 = UBound(arrColumns) Then 'Matching row is found
				   	rowFound = true
				   	Exit for 
				   End If
			Next
		  
			  If rowFound Then
			   intval = AddItemToStorageDictionary(key,r)
				If intval = 0 then
					StoreRowNumberMatchingPattern = 0
					  WriteStatusToLogs "Row number is successfully stored " & r
				Else
					StoreRowNumberMatchingPattern = 1
					
					  WriteStatusToLogs "Row number could not be stored."
				End If
			  Else
			  	
			   	StoreRowNumberMatchingPattern = 1
				WriteStatusToLogs "Could not find the specified cell in table."
			  End If
	 Else
	    StoreRowNumberMatchingPattern = 1
	    WriteStatusToLogs "Table object does not exists. "
	 End If
	on error goto 0
End function