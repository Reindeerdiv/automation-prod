Option Explicit

Dim StrHtmlPrefix
StrHtmlPrefix = "<html><head><meta http-equiv='ontent-Type' content='text/html; charset=iso-8859-1'/><title>Qualitia Test Execution Report</title><style type='text/css' media='all'>html, body, TABLE{height: 100%;margin: 0;padding: 0;font-size: 11px;font-family:Tahoma;color:Black;overflow-x:hidden;}</style></head><body>" 

'---------------------------------------------------------------------------------------------------------------------------
'Function Name:         ModifyStringForHtml()
'Purpose:               
'Return Value:          
'Arguments:		
'Input Arguments:	
'Written BY: 			Pravin Patil	
'Function is called by:	ExecuteSuite
'Function calls:		
'---------------------------------------------------------------------------------------------------------------------------
Function ModifyStrForHtml(byval aText)
	Dim szResult 
	
	szResult = ""
	
	Dim i
	Dim cchar
	Dim nChars
	
	For i = 1 To Len(aText)
		cchar = Mid(aText,i,1)
	
		Select Case cchar
			Case "<"
				nChars = "&lt;"
			Case ">"
				nChars = "&gt;"
			Case "&"
				nChars = "&amp;"
			Case Chr(34) 'char "
				nChars = "&quot;"
			Case "'"
				nChars = "&#39;"
			Case Else
				nChars = cchar
		End Select
	
		szResult = szResult & nChars
	Next
	
	ModifyStrForHtml = szResult
End Function

Function CreateLogFolderPath
	On Error Resume Next
			Dim status
			Dim strLogpath , strBitmapPath
			status = False				  	
			
			strLogpath = InitLogFolderPathHierarchy 
			strBitmapPath = strLogpath & "\" & CAPTUREBITMAP
			''Comment::Did not call CreateFolders strLogpath , because the below LOC will create the folder in strLogpath anyways  
			CreateFolders strLogpath & "\" & CAPTUREBITMAP
			If Err.number = 0 Then
					oDictLOGPATH.add LOGPATH , strLogpath
					oDictLOGPATH.add BITMAPLOGPATH , strBitmapPath
					status = true
			End IF
			CreateLogFolderPath = status
	On Error Goto 0
End Function

Function InitializeTCLogs(byval strTCName)
   On error resume next
			Dim strLogPath , status
			status = false
			strLogPath = oDictLOGPATH.item(LOGPATH)
			status=CreateTCLogFolder(strLogPath , strTCName)
'            If  CreateTCLogFolder(strLogPath , strTCName) Then
'					CreateTCLogFiles( oDictLOGPATH.item(TCLOGPATH) )
'					status = true
'            End if	
			InitializeTCLogs = status	 
   On error goto 0
End Function

Function CreateTCLogFolder(byval strLogpath , byval strTcName)
   On error resume next
   Dim testcaseLogPath,status
   status=false
   testcaseLogPath=strLogpath & "\" & strTcName
   logs.CreateLogs testcaseLogPath
					If  oDictLOGPATH.Exists(TCLOGPATH) Then
						oDictLOGPATH.item(TCLOGPATH) = testcaseLogPath
					Else
						oDictLOGPATH.add TCLOGPATH , testcaseLogPath
					End If
					status=true
'			Dim objTcFolder , testcaseLogPath , status
'			status = false
'			testcaseLogPath = strLogpath & "\" & strTcName
'			Set objTcFolder = CreateObject("Scripting.FileSystemObject")
'            
'			If not objTcFolder.FolderExists( testcaseLogPath ) then 
'					CreateFolders testcaseLogPath
'                    If  objTcFolder.FolderExists(testcaseLogPath) Then
'							status = true
'							''Add the TCLogPath in the global dictionary
'							If  oDictLOGPATH.Exists(TCLOGPATH) Then
'								oDictLOGPATH.item(TCLOGPATH) = testcaseLogPath
'							Else
'								oDictLOGPATH.add TCLOGPATH , testcaseLogPath
'							End If
'					End If
'            Else
'					status = true
'					''Add the TCLogPath in the global dictionary
'					oDictLOGPATH.add TCLOGPATH , testcaseLogPath
'			End If
			CreateTCLogFolder = status
   On error goto 0
End Function


Sub CreateTCLogFiles(byval strTCLogFolderPath)
'	On Error Resume Next
'	
'	Dim objFolder , objLogFile
'
'	Set objFolder = CreateObject("Scripting.FileSystemObject")
'	Set objLogFile = CreateObject("Scripting.FileSystemObject")
'
'	If  CREATEINFOLOG Then
'			If objFolder.FolderExists(strTCLogFolderPath) = True Then
'					Set Infologobj = objLogFile.CreateTextFile(strTCLogFolderPath & "\" & INFOLOGFILE, True)
'					Infologobj.writeline StrHtmlPrefix
'			Else
'					''Comments: Bad code :: Enhancement :: Need to write into the log that the TC Log folder does not exist.
'			End if
'	End If
'
'	If  CREATEDEBUGLOG Then
'			If objFolder.FolderExists(strTCLogFolderPath) = True Then
'					Set Debuglogobj = objLogFile.CreateTextFile(strTCLogFolderPath & "\" & DEBUGLOGFILE, True)
'					Debuglogobj.writeline StrHtmlPrefix
'					WriteConfigToLog Debuglogobj
'			Else
'					''Comments: Bad code :: Enhancement :: Need to write into the log that the TC Log folder does not exist.
'			End if
'	End If
'
'	If  CREATEERRORLOG Then
'			If objFolder.FolderExists(strTCLogFolderPath) = True Then
'					Set Errorlogobj = objLogFile.CreateTextFile(strTCLogFolderPath & "\" & ERRORLOGFILE, True)
'					Errorlogobj.writeline StrHtmlPrefix
'			Else
'					''Comments: Bad code :: Enhancement :: Need to write into the log that the TC Log folder does not exist.
'			End if
'	End If
'	Set objFolder = Nothing
'	Set objLogFile = Nothing
'	
'	On Error Goto 0
End Sub

'****************************************************************************************************************************

Function LogStartTime(objFileHandle)
	On error Resume Next
	objFileHandle.writeline "Start Time&nbsp;&nbsp;:&nbsp;&nbsp;" & GetResLongDate & "<br/>"
	On Error GOTO 0
End Function

Function LogEndTime(objFileHandle)
	On Error Resume Next
	objFileHandle.writeline "End Time" & "&nbsp;&nbsp;:&nbsp;&nbsp;" & GetResLongDate & "<br/>"
	On Error Goto 0
End Function

Function LogExecutionDate(objFileHandle)
	On Error Resume Next
	objFileHandle.writeline "Execution Date&nbsp;&nbsp;:&nbsp;&nbsp;" & GetResLongDate & "<br/>"
	On Error Goto 0
End Function

'------------------------------------------------------------------------------------------------------------------------------------------
'Function Name:         WriteStepResults()
'Purpose:               Each wrapper status writes into the log files after checking the status of the log files in configuration file.
'Return Value:          Boolean( 0/1)
'Arguments:			
'Input Arguments:       Status
'Output Arguments: 
'Function is called by: ExecuteSuite
'Function calls:        
'Created on:            19/03/2008
'                       Note When ever we want any properties for the all the logs just we can call this function.
'------------------------------------------------------------------------------------------------------------------------------------------
Function WriteStepResults(byval Status)
	On Error Resume Next
	Dim s,s_info
	If oDictConfiguration.item(UCase(PH_CREATEDEBUGLOG)) Then
		 If Not IsNull(Debuglogobj) Then
			If Status = STATUS_DEFAULT Then
				'Debuglogobj.writeline "<span style='color:#6b6b47;' name='ExecStatus'><img alt='' align='middle' src='../Images/Not_Executed.png'/>&nbsp;&nbsp;Execution Status: -1</span></br>"
				s = "<span style='color:#6b6b47;' name='ExecStatus'><img alt='' align='middle' src='../Images/Not_Executed.png'/>&nbsp;&nbsp;Execution Status: -1</span></br>"
			ElseIf Status = STATUS_PASSED Then
				'Debuglogobj.writeline "<span style='color:#006600;' name='ExecStatus'><img alt='' align='middle' src='../Images/passed.png'/>&nbsp;&nbsp;Execution Status: 0</span></br>"
				s = "<span style='color:#006600;' name='ExecStatus'><img alt='' align='middle' src='../Images/passed.png'/>&nbsp;&nbsp;Execution Status: 0</span></br>"
			ElseIf Status = STATUS_FAILED Then
				'Debuglogobj.writeline "<span style='color:#ff6600;' name='ExecStatus'><img alt='' align='middle' src='../Images/fail.PNG'/>&nbsp;&nbsp;Execution Status: 1</span></br>"
				s = "<span style='color:#ff6600;' name='ExecStatus'><img alt='' align='middle' src='../Images/fail.PNG'/>&nbsp;&nbsp;Execution Status: 1</span></br>"
			ElseIf Status = STATUS_DEFECT Then
				'Debuglogobj.writeline "<span style='color:#e60000;' name='ExecStatus'><img alt='' align='middle' src='../Images/Defect.png'/>&nbsp;&nbsp;Execution Status: 2</span></br>"
				s = "<span style='color:#e60000;' name='ExecStatus'><img alt='' align='middle' src='../Images/Defect.png'/>&nbsp;&nbsp;Execution Status: 2</span></br>"
			End If	
		 End If
		 logs.writeToDebugLog s
	End If
	
	If oDictConfiguration.item(UCase(PH_CREATEINFOLOG)) Then
		If Not IsNull(Infologobj) Then
			If Status = STATUS_DEFAULT Then
				'Infologobj.writeline "<span style='color:#6b6b47;' name='ExecStatus'><img alt='' align='middle' src='../Images/Not_Executed.png'/>&nbsp;&nbsp;Execution Status: Not Executed</span></br>"
				s_info = "<span style='color:#6b6b47;' name='ExecStatus'><img alt='' align='middle' src='../Images/Not_Executed.png'/>&nbsp;&nbsp;Execution Status: Not Executed</span></br>"
			ElseIf Status = STATUS_PASSED Then
				'Infologobj.writeline "<span style='color:#006600;' name='ExecStatus'><img alt='' align='middle' src='../Images/passed.png'/>&nbsp;&nbsp;Execution Status: Passed</span></br>"
				s_info = "<span style='color:#006600;' name='ExecStatus'><img alt='' align='middle' src='../Images/passed.png'/>&nbsp;&nbsp;Execution Status: Passed</span></br>"
			ElseIf Status = STATUS_FAILED Then
				'Infologobj.writeline "<span style='color:#ff6600;' name='ExecStatus'><img alt='' align='middle' src='../Images/fail.PNG'/>&nbsp;&nbsp;Execution Status: Failed</span></br>"
				s_info ="<span style='color:#ff6600;' name='ExecStatus'><img alt='' align='middle' src='../Images/fail.PNG'/>&nbsp;&nbsp;Execution Status: Failed</span></br>"
			ElseIf Status = STATUS_DEFECT Then
				'Infologobj.writeline "<span style='color:#e60000;' name='ExecStatus'><img alt='' align='middle' src='../Images/Defect.png'/>&nbsp;&nbsp;Execution Status: Defect</span></br>"
				s_info = "<span style='color:#e60000;' name='ExecStatus'><img alt='' align='middle' src='../Images/Defect.png'/>&nbsp;&nbsp;Execution Status: Defect</span></br>"
			End If
		End If
		logs.writeToInfoLog s_info		
	End If
	
	If Err.Number <> 0 And CInt(Status) = 0 Then
		WriteToErrorLog Err, Message
	End If

	On Error Goto 0
End Function

'---------------------------------------------------------------------------------------------------------------------------
'Function Name:         WriteStatusToLogs()
'Purpose:               It writes any messaged to the all the logs files.
'Return Value:
'Arguments:
'Input Arguments:       Status: This is Result dictionary object
'                       Message: This can be string or cancatination of Strings
'Output Arguments:	
'Function is called by: Any common method we call
'Function calls:        WriteToDebugLog, WriteToErrorLog
'---------------------------------------------------------------------------------------------------------------------------
Function WriteStatusToLogs (byval Message)
	On Error Resume next
	
	If Err.number <> 0 Then
		WriteToErrorLog Err, Message
	End If
	
	If oDictConfiguration.item(UCase(PH_CREATEDEBUGLOG)) Then
		WriteToDebugLog(Message)
	End If
	
	If oDictConfiguration.item(UCase(PH_CREATEINFOLOG)) Then
		WriteToInfoLog(Message)
	End If
	
	On Error Goto 0
End Function


'---------------------------------------------------------------------------------------------------------------------------
'Function Name:         WriteToErrorLog()
'Purpose:               To Write the Information into Error Log
'Return Value:			
'Arguments:		
'Input Arguments:       errorobject, Strmessage
'                       errorobject: This is the error Object
'                       Strmessage: This is the Message
'Output Arguments:	
'Function is called by:	WriteStepResults
'Function calls:		
'---------------------------------------------------------------------------------------------------------------------------       
Sub WriteToErrorLog(byval errorobject, byval Strmessage)
	On Error Resume next
'	
'	If oDictConfiguration.item(UCase(PH_CREATEERRORLOG)) Then
'		If Not objLog.Fileexists(oDictLOGPATH.item(TCLOGPATH) & "\" & ERRORLOGFILE) Then
'			LogStartTime(Errorlogobj)
'			Errorlogobj.Writeline "Time:" & "&nbsp;&nbsp;&nbsp;&nbsp;" & GetLongDate
'			Errorlogobj.writeline "Message:" & "&nbsp;&nbsp;&nbsp;&nbsp;" & Strmessage
'			
'			If errorobject.Number <> 0 Then
'				Errorlogobj.writeline "Error Number:" & "&nbsp;&nbsp;&nbsp;&nbsp;" & errorobject.Number 
'				Errorlogobj.writeline "Error Description:" & "&nbsp;&nbsp;&nbsp;&nbsp;" & errorobject.Description
'				Errorlogobj.writeline "Source:" & "&nbsp;&nbsp;&nbsp;&nbsp;" & errorobject.Source
'			End If
'			
'			Errorlogobj.Writeline " "
'		End If
'	End If
'	
'	
'	dotNetCom.WriteLogs errorobject
'	dotNetCom.WriteLogs Strmessage
dotNetCom.WriteInfoLogs errorobject&" "&Strmessage
	On Error Goto 0
End Sub

'---------------------------------------------------------------------------------------------------------------------------
'Function Name:         WriteToDebugLog()
'Purpose:               To Write the String message to the Debug Log along with the time format
'Return Value:			
'Arguments:		
'Input Arguments:       Strmessage
'                       Strmessage: This is the Message
'				
'Output Arguments:	
'Function is called by:	WriteStepResults
'Function calls:		
'--------------------------------------------------------------------------------------------------------------------------- 
Sub WriteToDebugLog(byval Message)
	On Error Resume Next
	'logs.writeToDebugLog Message
	dotNetCom.WriteDebugLogs Message
'	If CREATEDEBUGLOG Then	
'		If objLog.Fileexists(oDictLOGPATH.item(TCLOGPATH) & "\" & DEBUGLOGFILE) Then
'			Debuglogobj.writeline Message & "<br>"
'		End If
'	End if	
'	
	On Error Goto 0
End Sub

'---------------------------------------------------------------------------------------------------------------------------
'Function Name:         WriteToInfoLog()
'Purpose:               To Write the String message to the info logs
'Return Value:			
'Arguments:		
'Input Arguments:       Strmessage
'                       Strmessage: This is the Message
'					
'Output Arguments:	
'Function is called by:	WriteStepResults
'Function calls:		
'--------------------------------------------------------------------------------------------------------------------------- 
Sub WriteToInfoLog(byval Message)
	On Error Resume Next
	'logs.writeToInfoLog "<span name='Message'>" & Message & "</span>"
	dotNetCom.WriteInfoLogs Message
'	If CREATEINFOLOG Then	
'		If objLog.Fileexists(oDictLOGPATH.item(TCLOGPATH) & "\" & INFOLOGFILE) Then
'			Infologobj.writeline "<span name='Message'>" & Message & "</span></br>"
'		End If
'		
'	End if
'	
	On Error Goto 0

End Sub

'---------------------------------------------------------------------------------------------------------------------------
'Function Name:         WriteCriticalErrorTOErrorLog()
'Purpose:               To Write the Information into the ErrorLogs.
'Return Value:			
'Arguments:		
'Input Arguments:       Message
'                       Message: This is Any type of string which consists of concatination of the data  			
'Output Arguments:	
'Function is called by: In any wrapper or in test controller file, suite controller file
'Function calls:
'--------------------------------------------------------------------------------------------------------------------------- 
Sub WriteCriticalErrorTOErrorLog(byval Message)
	On Error Resume Next

	If  CREATEERRORLOG Then	
		If objLog.Fileexists(oDictLOGPATH.item(TCLOGPATH) & "\" & ERRORLOGFILE) Then
			LogStartTime(errorlogobj)
			errorlogobj.writeline Message
			errorlogobj.Writeline " "
		End If
	End if
	
	On Error Goto 0
End Sub

'---------------------------------------------------------------------------------------------------------------------------
'Function Name:         WriteNewLineToDebugLog()
'Purpose:               To insert the New line into the Debug logs, and info Logs
'Return Value:
'Arguments:
'Input Arguments:
'Message:
'Output Arguments:
'Function is called by: In QTP Test controller
'Function calls:		
'--------------------------------------------------------------------------------------------------------------------------- 
Sub WriteNewLineToDebugLog
	On Error Resume Next

	If Not CREATEDEBUGLOG Then	
		If objLog.Fileexists(oDictLOGPATH.item(TCLOGPATH) & "\" & DEBUGLOGFILE) Then
			Debuglogobj.writeline ""
		End If
		
		If objLog.Fileexists(oDictLOGPATH.item(TCLOGPATH) & "\" & INFOLOGFILE) Then
			Infologobj.writeline "</br>"
		End If
	End if

	On Error Goto 0
End Sub

'---------------------------------------------------------------------------------------------------------------------------
'Function Name:         CaptureBitmapImageObject()
'Purpose:               To capture the bitmaps
'Return Value:			
'Arguments:		
'Input Arguments:	
'Output Arguments:	
'Function is called by: ExecuteSuite
'Function calls:        GetObjectString
'---------------------------------------------------------------------------------------------------------------------------
Sub CaptureBitmapImageObject(byval sStrCapturedImageFileName, byval Capturedpath)
	On Error Resume Next
   	Dim sStrcapturefilename
   	sStrcapturefilename = Capturedpath & "\" & sStrCapturedImageFileName & "."&SNAPSHOTFILETYPE
    
	If Not objLog.Fileexists(sStrcapturefilename) Then
		Desktop.CaptureBitmap sStrcapturefilename, True 
	End If
	
End Sub

Sub WritePropertiesTOInfoLog (byval logObjid)

	If Not CREATEINFOLOG Then	
		Exit Sub
	End If

	Dim arrwritepro, key
	Dim sStrParam, arrParam, param, sStrParamTask
	Dim intParamDataSetid
	Dim strElementparamDataID
		
	If Not IsNull(logObjid) Then
		arrwritepro = oDictPropertyValueCollection.item(logObjid).Keys
		Infologobj.write "<span name='pObjectId'>ObjectID Properties::</br>"
		
		For Each Key In arrwritepro
			Infologobj.Write "&nbsp;&nbsp;&nbsp;&nbsp;" & Key & "=" & oDictPropertyValueCollection.item(logObjid).item(Key) & ";</br>"
		Next
		
		Infologobj.write "</span>"
	End If
	
	Infologobj.Writeline "</br>"
End Sub


Function WriteActionStatusToLog(byval statusNo,byval customMessage)
  On Error Resume Next

	Dim statusCode 
	statusCode = Status(statusno)
	WriteStatusToLogs customMessage
   On Error GoTo 0
End Function
		

Function Status(byVal statusNo)
On Error Resume Next

	If  statusNo = STATUS_PASSED Then
		status = "Passed"
	Elseif statusNo = STATUS_DEFECT Then
		status = "Defect"
	ElseIf statusNo = STATUS_FAILED Then  
		status = "Failed"
	ElseIf status = STATUS_DEFAULT Then
		status = "Not Executed"
	Else
		status = "UnKnown"
	End If

	
 On Error GoTo 0
End Function

'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name :				clickSnapshot(byval intres)
' 	Purpose :					    Takes snapshot with ref to configuration settings and step results.
' 	Return Value :		 		  
' 	Arguments :
'		Input Arguments :  	   byval intres
'		Output Arguments : 	  
'------------------------------------------------------------------------------------------------------------------------------------------
Function clickSnapshot(byval intStepres , ByVal testcaseSeq, ByVal tcIteration , ByVal taskSeq , ByVal taskIteration , ByVal stepSeq)
	
	On Error Resume Next
	Dim sStrCaptureUniqefileName
	blnSnapshotClick = False
	sStrCaptureUniqefileName = SNAPSHOTFILENAME &"_"&testcaseSeq&"_"&tcIteration&"_"&taskSeq&"_"&taskIteration&"_"&stepSeq

	If  intStepres = STATUS_PASSED  and CAPTUREPASSBITMAP Then
			blnSnapshotClick = True
			 
	ElseIf  intStepres =STATUS_FAILED and CAPTUREFAILBITMAP Then
			blnSnapshotClick = True
		   
	ElseIf  intStepres =STATUS_DEFECT and CAPTUREDEFECTBITMAP Then
			blnSnapshotClick = True
	End If

    If blnSnapshotClick Then
		CaptureBitmapImageObject sStrCaptureUniqefileName, oDictLOGPATH.item(BITMAPLOGPATH)
	End If

On Error GoTo 0
End Function


Function InitLogFolderPathHierarchy
    On error resume next

	Dim strLogpath
	If DRYRUN = FALSE Then
		strLogpath = MAINLOGPATH &"\" & HOSTNAME & "\" & QUSERNAME &"\" & PROJECTNAME &"\" & RELEASENO & "\" & BUILDNO & "\" & "IterationNumber_" & SUITEITERATIONNO
	Else
		strLogpath = REPORTPATH
    End If
       
	If err.number = 0 Then
		InitLogFolderPathHierarchy = strLogpath
	else
		InitLogFolderPathHierarchy = null
	End If

    On error goto 0
End Function


'Function CreateReportTxtFile
'   On error resume next
'		Dim strFolder , status , objLogFile
'		strReporttxtPath =  oDictLOGPATH.item(LOGPATH)
'		Set strFolder =  CreateObject("Scripting.FileSystemObject")
'		Set objLogFile = CreateObject("Scripting.FileSystemObject")
'		status = false
'
'		If strFolder.folderExists(strReporttxtPath) Then
'				Set Statuslogobj = objLogFile.CreateTextFile (strReporttxtPath & "\" & REPORTFILE, True)
'				If err.number = 0 Then
'						status = true
'				else
'						status = false
'						''Bad : Logging should be done in a log file
'				End If
'		else
'				status = false
'				''Bad : Logging should be done in a log file
'		End If
'		Set strFolder = Nothing  
'        CreateReportTxtFile = status
'   On error goto 0
'End Function


Function CreateReportTxtFile
   On error resume next
'		Dim strFolder , status , objLogFile
		Dim strReporttxtPath
		strReporttxtPath =  oDictLOGPATH.item(LOGPATH)
'		Set strFolder =  CreateObject("Scripting.FileSystemObject")
'		Set objLogFile = CreateObject("Scripting.FileSystemObject")
'		status = false
'
'		If strFolder.folderExists(strReporttxtPath) Then
'				Set Statuslogobj = objLogFile.CreateTextFile (strReporttxtPath & "\" & REPORTFILE, True)
'				If err.number = 0 Then
'						status = true
'				else
'						status = false
'						''Bad : Logging should be done in a log file
'				End If
'		else
'				status = false
'				''Bad : Logging should be done in a log file
'		End If
'		Set strFolder = Nothing
'		Set Statuslogobj = nothing		
'        CreateReportTxtFile = status
   		logs.initializeReportTxt strReporttxtPath
		If err.number = 0 Then
				status = true
		else
				status = false
				
		End If
		CreateReportTxtFile = status
   On error goto 0
End Function

Function CloseLogger
	logs.DisposeLogger
End Function

Function WriteToReportLog(byval strData)
   On error resume next
			'Dim objReport , strReportPath, Statuslogobj
			'Set objReport = CreateObject("Scripting.FileSystemObject")
			'strReportPath = oDictLOGPATH.item(LOGPATH) & "\" & REPORTFILE
			
'			Set reportfile1 = fso.CreateTextFile(strReportPath, True)
'			reportfile1.WriteLine("qualitiareportpath="&ReportPath)
	
			'If objReport.FileExists(strReportPath) Then
			'		Set Statuslogobj = objReport.OpenTextFile(strReportPath, 8) ', True)  
			'		Statuslogobj.writeline strData
			'End If	
	logs.writeToReportTxt strData			
   On error goto 0
   'Set Statuslogobj = nothing
   'Set objReport = nothing
   
   
End Function

