'=======================Code Section Begin=============================================

Option Explicit 'Forces explicit declaration of all variables in a script. 
'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name :				 VerifyObjectDisabled()
' 	Purpose :					  Verifies whether the object  is Disabled.
'	Return Value :		 		  Integer( 0/1/2)
'------------------------------------------------------------------------------------------------------------------------------------------
Function VerifyObjectDisabled(ByVal object)
On Error Resume Next
	'Variable Section Begin
		   Dim customMsg
		   Dim status
	'Variable Section End
		If CheckObjectExist(object) Then
			If Not(CheckObjectStatus(object)) Then
					status = STATUS_PASSED
				  customMsg = "The object is disabled."
				Else
					status = STATUS_DEFECT
                   customMsg = "The object is not disabled, Please verify."
				End If
            
		Else
				status = STATUS_FAILED
                customMsg = "The object does not exist, Please verify." 'MSG_OBJECT_NOT_EXISTS
		End if
VerifyObjectDisabled=status
	call WriteActionStatusToLog(status, customMsg)
	On Error GoTo 0
    
End Function
'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name :				 VerifyObjectEnabled()
' 	Purpose :					 Verifies whether the object is Enabled
' 	Return Value :		 		  Integer( 0/1/2)
'------------------------------------------------------------------------------------------------------------------------------------------
Function VerifyObjectEnabled(ByVal object)

On Error Resume Next
	'Variable Section Begin
 Dim customMsg
 Dim status
	'Variable Section End
		If CheckObjectExist(object) Then
			If CheckObjectEnabled(object) Then
					status = STATUS_PASSED
					customMsg = "The object was enabled as expected."
				Else
					status = STATUS_DEFECT
					customMsg = "The object was not enabled, Please verify."
				End If
		Else
				status = STATUS_FAILED
				customMsg =  "The object does not exist, Please verify." 'MSG_OBJECT_NOT_EXISTS
		End if
VerifyObjectEnabled=status
  call WriteActionStatusToLog(status, customMsg)
		
	On Error GoTo 0
    
End Function


'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :				 VerifyObjectVisible()
'	Purpose :					 Verifies whether the Object is visible.
'	Return Value :		 		  Integer( 0/1/2)
'------------------------------------------------------------------------------------------------------------------------------------------
Function VerifyObjectVisible(ByVal object)
	On Error Resume Next
		'Variable Section Begin
		  Dim customMsg
		  		   Dim status
		'Variable Section End
			If CheckObjectExist(object)  Then 
				If CheckObjectVisible(object) Then
					status = STATUS_PASSED
					customMsg =  "The object was visible as expected."
				Else
					status = STATUS_DEFECT
					  customMsg  = "The object was not visible, Please verify."
				End If
			Else
					status = STATUS_DEFECT
					  customMsg  = "The object was not visible, Please verify."
				  ' customMsg =  "The object does not exist, Please verify." 'MSG_OBJECT_NOT_EXISTS
			End If
			VerifyObjectVisible=status
		   call WriteActionStatusToLog(status, customMsg)
	On Error GoTo 0
End Function
'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :				 VerifyObjectVisible()
'	Purpose :					 Verifies whether the Object is Invisible.
'	Return Value :		 		  Integer( 0/1/2)
'------------------------------------------------------------------------------------------------------------------------------------------
Function VerifyObjectInVisible(ByVal object)
	On Error Resume Next
		'Variable Section Begin
	     Dim customMsg
	     Dim status
		'Variable Section End
			If CheckObjectExist(object)  Then 
				If CheckObjectInVisible(object) Then
					status = STATUS_PASSED
					customMsg = "The object was invisible as expected."
				Else
					status = STATUS_DEFECT
					  customMsg = "The object was not invisible, Please verify." 
				End If
			Else
				    status = STATUS_PASSED
					customMsg = "The object was invisible as expected."
				   	'customMsg = "The object does not exist, Please verify." 'MSG_OBJECT_NOT_EXISTS
			End If
				   call WriteActionStatusToLog(status, customMsg)
				   VerifyObjectInVisible = status
	On Error GoTo 0
End Function
'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :				 VerifyObjectExist()
'	Purpose :					 Verifies whether the Object Exists.
'	Return Value :		 		  Integer( 0/1)
'------------------------------------------------------------------------------------------------------------------------------------------

Function VerifyObjectExist(ByVal object)
	On Error Resume Next
		'Variable Section Begin
			Dim customMsg
		     Dim status
		'Variable Section End
			If CheckObjectExist(object) Then 
					status =STATUS_PASSED
					  customMsg =  "The object exists as expected." 'MSG_OBJECT_EXISTS
			Else
					status = STATUS_DEFECT
				   customMsg = "The object does not exist, Please verify." 'MSG_OBJECT_NOT_EXISTS
			End If 
			VerifyObjectExist=status
	   call WriteActionStatusToLog(status, customMsg)
	On Error GoTo 0
End Function

'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :				 VerifyObjectNotExist()
'	Purpose :					 Verifies whether the Object does NOT Exists.
'	Return Value :		 		  Integer( 0/1)
'----------------------------------------------------------------------------------------------------------------------------------------

Function VerifyObjectNotExist(ByVal object)
	On Error Resume Next
		'Variable Section Begin
			Dim customMsg
			 Dim status
		'Variable Section End
		
			If CheckObjectNotExist(object) Then 
					status = STATUS_PASSED
				  	  customMsg = "The object does not exist as expected."  'MSG_OBJECT_NOT_EXISTS_PASS
					
			Else
					status = STATUS_DEFECT
				  customMsg = "The object exists, Please Verify." ' MSG_OBJECT_EXISTS_FAIL
					
			End If 
	VerifyObjectNotExist=status
	 call WriteActionStatusToLog(status, customMsg)
	On Error GoTo 0
End Function

'=======================Code Section End=============================================
