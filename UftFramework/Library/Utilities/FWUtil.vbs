

Sub CreateFolders( strPath )
	On Error Resume Next

	Dim objFSO 
    Set objFSO = CreateObject("Scripting.FileSystemObject")
	If strPath <> "" Then
		'If Not objFSO.FolderExists( objFSO.GetParentFolderName(strPath) ) Then 
		If Not objFSO.FolderExists(strPath) Then 
			Call CreateFolders( objFSO.GetParentFolderName(strPath) )
			objFSO.CreateFolder( strPath )
		End If
	End If
	On error goto 0
End Sub 


 Function DoubleQuotes(sStr)
	On error Resume Next
        If IsEmpty(sStr)  Then
	        WriteToErrorLog Err, "DoubleQuotes: String does not contain values."
			Err.Clear
        End If

        DoubleQuotes = """" & sStr & """"
	On error GoTo 0
End Function

'---------------------------------------------------------------------------------------------------------------------------
'Function Name:         GetLongDate()
'Purpose:               To get the timestamp
'Return Value:			
'Arguments:		
'Input Arguments:
'Output Arguments:	
'Function is called by: while writing into the Debuglog and while updating results into result folder.
'Function calls:
'Note here previosly
'---------------------------------------------------------------------------------------------------------------------------
Function GetLongDate
	'On error resume next
	'GetLongDate=FormatDateTime(Now, vbLongDate)&getDateTimeStamp(Timer())
	GetLongDate = getDateTimeStamp(Timer())
	'On Error GOTO 0
End Function

'---------------------------------------------------------------------------------------------------------------------------
'Function Name:         GetLongTime()
'Purpose:               To get the timestamp
'Return Value:			
'Arguments:		
'Input Arguments:
'Output Arguments:	
'Function is called by: while writing into the Debuglog and while updating results into result folder.
'Function calls:
'---------------------------------------------------------------------------------------------------------------------------
Function GetMySQLTimeStamp
	'On Error Resume next
	'GetLongTime = getDateTimeStamp(Timer())
	GetMySQLTimeStamp = GetMySQLDateTimeStamp(Timer())
	'On Error GOTO 0
End Function

Function GetResLongDate
	On Error Resume Next
	'*************For Ms-Access database****************
	'GetResLongDate = FormatDateTime(Now(), vbGeneralDate)
	'************************************************
	GetResLongDate = getDateTimeStamp(Timer())
	On Error Goto 0
End Function

'---------------------------------------------------------------------------------------------------------------------------
'Function Name:         GetMySQLDateTimeStamp()
'Purpose:               To get the timestamp in the MySQL format
'Return Value:			
'Arguments:		
'Input Arguments:       dblMilliSecond
'Output Arguments:      Time stamp
'Function is called by: while writing into the Debuglog and while updating results into result folder.
'Function calls:
'---------------------------------------------------------------------------------------------------------------------------
Function GetMySQLDateTimeStamp(byVal dblMilliSecond) 
	'On Error Resume next
	Dim strDate
	Dim intMilliSecond, intSecond, intMinute, intHour
	
	strDate = Year(Date()) & "-" & Month(Date()) & "-" & Day(Date())
	
	intMilliSecond = Int(dblMilliSecond * 1000) Mod 1000 
	
	intSecond = Int(dblMilliSecond) 
	
	intMinute = Int(intSecond / 60) 
	
	intSecond = intSecond Mod 60 
	
	intHour = Int(intMinute / 60) 
	
	intMinute = intMinute Mod 60 
	
	GetMySQLDateTimeStamp = strDate & " " & intHour & ":" & intMinute & ":" & intSecond 
	'On Error GOTO 0
End Function 

'---------------------------------------------------------------------------------------------------------------------------
'Function Name:         getDateTimeStamp()
'Purpose:               To get the timestamp in milliSeconds
'Return Value:			
'Arguments:		
'Input Arguments:       Seconds
'Output Arguments:      Time stamp in yyyy-mm-dd hh:mm:ss.000 format
'Function is called by: while writing into the Debuglog and NOT while updating results into results DB.
'Function calls:
'--------------------------------------------------------------------------------------------------------------------------
Function getDateTimeStamp(byVal SecondsData) 
	'On Error Resume next
	Dim strDate
	Dim intMilliSecond, intSecond, intMinute, intHour
	
	'strDate = Day(Date()) & "/" & Month(Date()) & "/" & Year(Date())
	strDate = Year(Date()) & "-" & Month(Date()) & "-" & Day(Date())
	
	intMilliSecond = Int(SecondsData * 1000) Mod 1000 
	If len(Cstr(intMilliSecond)) = 1 Then
		intMilliSecond = "00"&intMilliSecond
	elseif len(Cstr(intMilliSecond)) = 2 Then
		intMilliSecond = "0"&intMilliSecond
	End If

	intSecond = Int(SecondsData) 
	
	intMinute = Int(intSecond / 60) 
	
	intSecond = intSecond Mod 60 
	
	intHour = Int(intMinute / 60) 
	
	intMinute = intMinute Mod 60 

	If len(Cstr(intSecond)) = 1 Then
		intSecond = "0"&intSecond
	End if
	
	If len(Cstr(intMinute)) = 1 Then
		intMinute = "0"&intMinute
	End If

	If len(Cstr(intHour)) = 1 Then
		intHour = "0"&intHour
	End If
	
	getDateTimeStamp = strDate & " " & intHour & ":" & intMinute & ":" & intSecond & "." & intMilliSecond 
	'On Error GOTO 0
End Function 


'---------------------------------------------------------------------------------------------------------------------------
'Function Name:         ReadConfiguration()
'Purpose:               To Read the configuration from the ini file, these ini values first we get in the Public variables.
'Return Value:          oDictConfiguration
'Arguments:		
'Input Arguments:	
'Output Arguments:      oDictConfiguration
'Function is called by:	Call ReadConfiguration
'Function calls:		
'---------------------------------------------------------------------------------------------------------------------------
Function ReadConfiguration
    On Error Resume Next

	Set oDictConfiguration = CreateObject("Scripting.Dictionary")
	Dim strDBPassword , strDBUserName , strServerPort , strServerIP , strDbDriver
	Dim strCreateInfoLog , strCreateDebugLog, strCreateErrorLog , strCapturePassBitmap , strCaptureFailBitmap , strCaptureDefectBitmap
	Dim strSyncTime , strOfflinemode , strProjectname, strQualitiaUser, strXMLPath, strDryRun , strBuildNo , strReleaseNo , strHostname
	Dim ObjSecure
	Dim HIGHLGHT, strHighlight
	Dim strDBType, strWinAuth, strMasterDB
	Dim ORpath


    '''Following are logging related Config Data
    strOfflinemode = GetConfigValues(PH_OFFLINEMODE)
	If  IsNull(strOfflinemode) or IsEmpty(strOfflinemode) or Trim(strOfflinemode)="" Then
			OFFLINEMODE = False
	Else
			If Trim(Ucase(strOfflinemode))= "TRUE" Then
					OFFLINEMODE = True
                    Dim objFSO 
                    Set objFSO = CreateObject("Scripting.FileSystemObject")
                    if objFSO.FileExists(TEMP_QUALITIAOFFLINEPROPERTY_PATH) then
                        configObj.LoadQualitiaPropertiesFile(TEMP_QUALITIAOFFLINEPROPERTY_PATH)
                    end if

			Else 
					OFFLINEMODE = False
			End If
			oDictConfiguration.Add Ucase(PH_OFFLINEMODE) , OFFLINEMODE
	End If

    Set ObjSecure = CreateObject("QualitiaCore._SecureTestData")
	strCreateInfoLog = GetConfigValues(PH_CREATEINFOLOG)
	If  IsNull(strCreateInfoLog) or IsEmpty(strCreateInfoLog) or Trim(strCreateInfoLog)="" Then
			CREATEINFOLOG = False
	Else
			If Trim(Ucase(strCreateInfoLog))= "TRUE" Then
					CREATEINFOLOG = True
			Else 
					CREATEINFOLOG = False
			End If
			oDictConfiguration.Add Ucase(PH_CREATEINFOLOG) , CREATEINFOLOG
	End If		

	strCreateDebugLog = GetConfigValues(PH_CREATEDEBUGLOG)
	If  IsNull(strCreateDebugLog) or IsEmpty(strCreateDebugLog) or Trim(strCreateDebugLog)="" Then
			CREATEDEBUGLOG = False
	Else
			If Trim(Ucase(strCreateDebugLog))= "TRUE" Then
					CREATEDEBUGLOG = True
			Else 
					CREATEDEBUGLOG = False
			End If
			oDictConfiguration.Add Ucase(PH_CREATEDEBUGLOG) , CREATEDEBUGLOG
	End If
	 
	strCreateErrorLog = GetConfigValues(PH_CREATEERRORLOG)
	If  IsNull(strCreateErrorLog) or IsEmpty(strCreateErrorLog) or Trim(strCreateErrorLog)="" Then
			CREATEERRORLOG = False
	Else
			If Trim(Ucase(strCreateErrorLog))= "TRUE" Then
					CREATEERRORLOG = True
			Else 
					CREATEERRORLOG = False
			End If
			oDictConfiguration.Add Ucase(PH_CREATEERRORLOG) , CREATEERRORLOG
	End If

	strCapturePassBitmap = GetConfigValues(PH_CAPTUREPASSBITMAP)
	If  IsNull(strCapturePassBitmap) or IsEmpty(strCapturePassBitmap) or Trim(strCapturePassBitmap)="" Then
			CAPTUREPASSBITMAP = False
	Else
			If Trim(Ucase(strCapturePassBitmap))= "TRUE" Then
					CAPTUREPASSBITMAP = True
			Else 
					CAPTUREPASSBITMAP = False
			End If
			oDictConfiguration.Add Ucase(PH_CAPTUREPASSBITMAP) , CAPTUREPASSBITMAP
	End If

	strCaptureFailBitmap = GetConfigValues(PH_CAPTUREFAILBITMAP)
	If  IsNull(strCaptureFailBitmap) or IsEmpty(strCaptureFailBitmap) or Trim(strCaptureFailBitmap)="" Then
			CAPTUREFAILBITMAP = False
	Else
			If Trim(Ucase(strCaptureFailBitmap))= "TRUE" Then
					CAPTUREFAILBITMAP = True
			Else 
					CAPTUREFAILBITMAP = False
			End If
			oDictConfiguration.Add Ucase(PH_CAPTUREFAILBITMAP) , CAPTUREFAILBITMAP
	End If

	strCaptureDefectBitmap = GetConfigValues(PH_CAPTUREDEFECTBITMAP)
	If  IsNull(strCaptureDefectBitmap) or IsEmpty(strCaptureDefectBitmap) or Trim(strCaptureDefectBitmap)="" Then
			CAPTUREDEFECTBITMAP = False
	Else
			If Trim(Ucase(strCaptureDefectBitmap))= "TRUE" Then
					CAPTUREDEFECTBITMAP = True
			Else 
					CAPTUREDEFECTBITMAP = False
			End If
			oDictConfiguration.Add Ucase(PH_CAPTUREDEFECTBITMAP) , CAPTUREDEFECTBITMAP
	End If

	strMainLogpathPath = GetConfigValues(PH_LOGPATH)
	If  IsNull(strMainLogpathPath) or IsEmpty(strMainLogpathPath) or Trim(strMainLogpathPath)="" Then
			msgbox "ConfigFile :: The Logpath is either Null/Empty. Please verify."
	Else
			MAINLOGPATH = strMainLogpathPath 	
			oDictConfiguration.Add Ucase(PH_LOGPATH) , MAINLOGPATH
	End If

	'''Following are Qualitia Project Config related keys for execution

	strSyncTime = GetConfigValues(PH_SYNCTIMEINSECONDS)
	If  IsNull(strSyncTime) or IsEmpty(strSyncTime) or Trim(strSyncTime)="" Then
			msgbox "ConfigFile :: The SyncTimeInSeconds is either Null/Empty. Please verify."
	Else
			SYNCTIMEINSECONDS = strSyncTime 	
			oDictConfiguration.Add Ucase(PH_SYNCTIMEINSECONDS) , SYNCTIMEINSECONDS
	End If

	

	If OFFLINEMODE = True Then
			DRYRUN = False 
	Else
			strDryRun = GetConfigValues(PH_DRYRUN)
			If  IsNull(strDryRun) or IsEmpty(strDryRun) or Trim(strDryRun)="" Then
					DRYRUN = False
			Else
					If Trim(Ucase(strDryRun))= "TRUE" Then
							DRYRUN = True
					Else 
							DRYRUN = False
					End If
					oDictConfiguration.Add Ucase(PH_DRYRUN) , DRYRUN
			End If
				
				If NOT (GetConfigValues(PH_WINDOWSAUTHENTICATION) = "True" and UCASE(GetConfigValues(PH_DBTYPE)) = "SQLSERVER") Then
					strDBPassword = GetConfigValues(PH_DBPASSWORD)
					
					'strDBPassword = ObjSecure.getDecryptedPwd(strDBPassword)
					
					If  IsNull(strDBPassword) or IsEmpty(strDBPassword) or Trim(strDBPassword)="" Then
							''Comments: At this point of time no log file is created yet, hence putting a messge box for the same.
							''This is cumbersome, but willatleast  allow the user to correct before even the execution starts saving unwanted surprises
							msgbox "ConfigFile :: The DB Password is either Null/Empty. Please verify."
					Else
							DBPASSWORD = strDBPassword 	
		
							oDictConfiguration.Add Ucase(PH_DBPASSWORD) , GetConfigValues(PH_DBPASSWORD)
					End If
		
					strDBUserName = GetConfigValues(PH_DBUSERNAME)
					If  IsNull(strDBUserName) or IsEmpty(strDBUserName) or Trim(strDBUserName)="" Then
							msgbox "ConfigFile :: The DB Username is either Null/Empty. Please verify."
					Else
							DBUSERNAME = strDBUserName 	
							oDictConfiguration.Add Ucase(PH_DBUSERNAME) , DBUSERNAME
					End If
				End If

			''''strDBPassword = GetConfigValues(PH_DBPASSWORD)
			
			'''''strDBPassword = ObjSecure.getDecryptedPwd(strDBPassword)
			
			''''If  IsNull(strDBPassword) or IsEmpty(strDBPassword) or Trim(strDBPassword)="" Then
					''Comments: At this point of time no log file is created yet, hence putting a messge box for the same.
					''This is cumbersome, but willatleast  allow the user to correct before even the execution starts saving unwanted surprises
					''''msgbox "ConfigFile :: The DB Password is either Null/Empty. Please verify."
			''''Else
					''''DBPASSWORD = strDBPassword 	

					''''oDictConfiguration.Add Ucase(PH_DBPASSWORD) , GetConfigValues(PH_DBPASSWORD)
			''''End If

			''''strDBUserName = GetConfigValues(PH_DBUSERNAME)
			''''If  IsNull(strDBUserName) or IsEmpty(strDBUserName) or Trim(strDBUserName)="" Then
					''''msgbox "ConfigFile :: The DB Username is either Null/Empty. Please verify."
			''''Else
					''''DBUSERNAME = strDBUserName 	
					''''oDictConfiguration.Add Ucase(PH_DBUSERNAME) , DBUSERNAME
			''''End If

			strServerPort = GetConfigValues(PH_DBSERVERPORT)
			If  IsNull(strServerPort) or IsEmpty(strServerPort) or Trim(strServerPort)="" Then
					msgbox "ConfigFile :: The DB Server port is either Null/Empty. Please verify."
			Else
					DBSERVERPORT = strServerPort 	
					oDictConfiguration.Add Ucase(PH_DBSERVERPORT) , DBSERVERPORT
			End If

			strServerIP = GetConfigValues(PH_SERVER)
			If  IsNull(strServerIP) or IsEmpty(strServerIP) or Trim(strServerIP)="" Then
					msgbox "ConfigFile :: The DB Server is either Null/Empty. Please verify."
			Else
					DBSERVER = strServerIP 	
					oDictConfiguration.Add Ucase(PH_SERVER) , DBSERVER
			End If

			'strDbDriver = GetConfigValues(PH_DRIVERPATH)
			'If  IsNull(strDbDriver) or IsEmpty(strDbDriver) or Trim(strDbDriver)="" Then
					'msgbox "ConfigFile :: The DriverPath is either Null/Empty. Please verify."
			'Else
					'DRIVERPATH = strDbDriver 	
					'oDictConfiguration.Add Ucase(PH_DRIVERPATH) , DRIVERPATH
			'End If

	End If
	
	strProjectname = GetConfigValues(PH_PROJECTNAME)
	If  IsNull(strProjectname) or IsEmpty(strProjectname) or Trim(strProjectname)="" Then
			msgbox "ConfigFile :: The Project is either Null/Empty. Please verify."
	Else
			PROJECTNAME = strProjectname 	
			oDictConfiguration.Add Ucase(PH_PROJECTNAME) , PROJECTNAME
			PROJECT_RESULTDB = PROJECTNAME & "_resultdb"
			oDictConfiguration.Add Ucase(PH_PROJECT_RESULTDB) , PROJECT_RESULTDB
	End If

	strQualitiaUser = GetConfigValues(PH_QUSERNAME)
	If  IsNull(strQualitiaUser) or IsEmpty(strQualitiaUser) or Trim(strQualitiaUser)="" Then
			msgbox "ConfigFile :: The UserName is either Null/Empty. Please verify."
	Else
			QUSERNAME = strQualitiaUser 	
			oDictConfiguration.Add Ucase(PH_QUSERNAME) , QUSERNAME
	End If

	strXMLPath = GetConfigValues(PH_XMLPATH)
	If  IsNull(strXMLPath) or IsEmpty(strXMLPath) or Trim(strXMLPath)="" Then
			msgbox "ConfigFile :: The Project is either Null/Empty. Please verify."
	Else
			XMLPATH = strXMLPath 	
			oDictConfiguration.Add Ucase(PH_XMLPATH) , XMLPATH
	End If

    strBuildNo = GetConfigValues(PH_BUILDNO)
	If  IsNull(strBuildNo) or IsEmpty(strBuildNo) or Trim(strBuildNo)="" Then
			msgbox "ConfigFile :: The Build No is either Null/Empty. Please verify."
	Else
			BUILDNO = strBuildNo 	
			oDictConfiguration.Add Ucase(PH_BUILDNO) , BUILDNO
	End If

	strReleaseNo = GetConfigValues(PH_RELEASENO)
	If  IsNull(strReleaseNo) or IsEmpty(strReleaseNo) or Trim(strReleaseNo)="" Then
			msgbox "ConfigFile :: The Release No is either Null/Empty. Please verify."
	Else
			RELEASENO = strReleaseNo 	
			oDictConfiguration.Add Ucase(PH_RELEASENO) , RELEASENO
	End If
	
	strHighlight = GetConfigValues(PH_HILIGHT)
	If  IsNull(strHighlight) or IsEmpty(strHighlight) or Trim(strHighlight)="" Then
			msgbox "ConfigFile :: The Highlight flag is either Null/Empty. Please verify."
	Else
			HIGHLGHT = strHighlight 	
			oDictConfiguration.Add Ucase(PH_HILIGHT) , HIGHLGHT
	End If
	
	strHostname = GetHostName() 
	If  IsNull(strHostname) or IsEmpty(strHostname) or Trim(strHostname)="" Then
			msgbox "ConfigFile :: The HostName is either Null/Empty. Please verify."
	Else
			HOSTNAME = strHostname 	
			oDictConfiguration.Add Ucase(PH_HOSTNAME) , HOSTNAME
	End If
	
	
	ORpath = GetConfigValues(PH_REPOSITORYPATH)
	If  Not (IsNull(ORpath) or IsEmpty(ORpath) or Trim(ORpath)="") Then
		oDictConfiguration.Add Ucase(PH_REPOSITORYPATH) , ORpath
	End If
	
	If Not OFFLINEMODE Then
			strDBType = GetConfigValues(PH_DBTYPE)
			If  IsNull(strDBType) or IsEmpty(strDBType) or Trim(strDBType)="" Then
					msgbox "ConfigFile :: The DBType flag is either Null/Empty. Please verify."
			Else
					oDictConfiguration.Add Ucase(PH_DBTYPE) , strDBType
			End If
			
			strWinAuth = GetConfigValues(PH_WINDOWSAUTHENTICATION)
			If  IsNull(strWinAuth) or IsEmpty(strWinAuth) or Trim(strWinAuth)="" Then
					msgbox "ConfigFile :: The WINDOWS_AUTHENTICATION flag is either Null/Empty. Please verify."
			Else
					oDictConfiguration.Add Ucase(PH_WINDOWSAUTHENTICATION) , strWinAuth
			End If	
			
			strMasterDB = GetConfigValues(PH_MASTERDB)
			If  IsNull(strMasterDB) or IsEmpty(strMasterDB) or Trim(strMasterDB)="" Then
					msgbox "ConfigFile :: The MasterDB flag is either Null/Empty. Please verify."
			Else
					oDictConfiguration.Add Ucase(PH_MASTERDB) , strMasterDB
			End If	
	End If
	'-----------------------------------------------
	
	Set ReadConfiguration = oDictConfiguration
    If Err.Number <> 0 Then
			Msgbox "Could not read configuration information . Error No:" &err.number & " :: " &Err.description
    End If

	Set ObjSecure = Nothing
	On error goTo 0
End Function


Function WriteConfigToLog(byval objFile)
'   On error Resume next
'
'		Dim strItemData , status 
'		status = false
'        objFile.writeline "<br><div>"
'		objFile.writeline GetResLongDate
'		objFile.writeline "<br><span style='color:Gray;font-weight:normal;font-size:larger;'>Configuration Setting:</span>"&_
'										   "<TABLE><TR align='left'><TH>Configuration Setting</TH><TH>::</TH><TH>Configuration Value<TH></TR>"&_
'										   "<TR><TD>---------------------</TD><TD>::</TD><TD>-------------------</TD></TR>"
'		For each strkey in oDictConfiguration.keys
'				strItemData = oDictConfiguration.Item(strKey)		
'				objFile.writeline "<TR><TD>" + strkey + "</TD><TD>::</TD><TD>" + strItemData + "</TD></TR>"
'		Next
'		objFile.writeline "</TABLE></div>"
'		objFile.writeline "<br>"
'		If err.number = 0 Then
'				status = true
'		End If
'
'		WriteConfigToLog = status
'   On error goto 0   
WriteConfigToLog = true
End Function


'---------------------------------------------------------------------------------------------------------------------------
'Function Name:         ReplaceSpecialChanracter()
'Purpose:               To Replace the special charector by pass the string
'Return Value:          String
'Arguments:		
'Input Arguments:
'Message:
'Output Arguments: 
'Function is called by:	
'Function calls:	
'---------------------------------------------------------------------------------------------------------------------------
Function ReplaceSpecialCharacter(ByVal strData)
   On error resume next
	Dim strPattern
	Dim regEx , Matches '  Replace only the chars \ / : * ? "" <> | ' $ % ! _ - . ^ ~ # & @ { }
    strPattern = "[\'\\/:\*\?""<>|]"  '"[\.\^\~\;\\/:\*\?\#\&\=\@\{\}""<>|'$%!_-]" ' "[\\/:\*\?""<>|]"
	Set regEx = New RegExp         ' Create a regular expression.
    regEx.Pattern = strPattern        ' Set pattern.
	regEx.IgnoreCase = True         ' Set case insensitivity.
	regEx.Global = True         ' Set global applicability.
	Set Matches = regEx.Execute(strData)   ' Execute search.
	If Matches.count > 0 Then
	  strData =  regEx.replace(strData,"")
	End If
	ReplaceSpecialCharacter = strData
	On error goto 0
End Function


'---------------------------------------------------------------------------------------------------------------------------
'Function Name:         ReplaceSpecialChanracter()
'Purpose:               To Replace the special charector by pass the string
'Return Value:          String
'Arguments:		
'Input Arguments:
'Message:
'Output Arguments: 
'Function is called by:	
'Function calls:	
'---------------------------------------------------------------------------------------------------------------------------
Function ReplaceSpecialChanracter(ByVal strString, byval strSearchString, byval strRepalce)
    ReplaceSpecialChanracter = Replace(strString, strSearchString, strRepalce)
End Function

'---------------------------------------------------------------------------------------------------------------------------
'Function Name:         ReplaceSqlSplChanracters()
'Purpose:               To Replace the special charectors
'Return Value:          String with new special charector
'Arguments:		
'Input Arguments:	
'Output Arguments:	
'Function is called by:	
'Function calls:		IsRecordsetEmpty, InsertSuiteRecordsInResTable, ReadSuiteData, LogHeader
'---------------------------------------------------------------------------------------------------------------------------
function EscapeSqlCharacters(str)
	if str<>"" then
		str = Replace(str, "'", "''") 
		str = Replace(str, "\", "\\") 
	end if
	EscapeSqlCharacters = str
End Function


Function GetDatesDifferenceInMilliSecs(byval startDate, byval endDate)
   On error resume next

		Dim arrEndDate , endDateTime , endMillisecs
		 Dim arrStartDate , startDateTime , startMillisecs
		 Dim millisecs , diffInMillisecs

        arrEndDate = Split(endDate , ".")
		endDateTime = arrEndDate(0)
		endMillisecs = arrEndDate(1)

		arrStartDate = Split(startDate , ".")
		startDateTime = arrStartDate(0)
		startMillisecs = arrStartDate(1)

		millisecs = DateDiff("s" , startDateTime, endDateTime)*1000

		If Cint(endMillisecs)<Cint(startMillisecs) Then
			endMillisecs = endMillisecs + 1000
			millisecs = millisecs  -1000
		End If
		diffInMillisecs = endMillisecs - startMillisecs
        millisecs = millisecs + diffInMillisecs
		GetDatesDifferenceInMilliSecs = ConvertMillisecsToTimeStamp (millisecs)
   On error goto 0
End Function

''Output will look like : say   2011-07-25 10:40:00.200
Function ConvertMillisecsToTimeStamp(byVal intMilliSecond) 
	On Error Resume next
	
			Dim intSecond, intMinute, intHour , intMilliSecs
			
			intMilliSecs = intMilliSecond Mod 1000 
			If len(Cstr(intMilliSecs)) = 1 Then
				intMilliSecs = "00" & intMilliSecs
			elseif len(Cstr(intMilliSecs)) = 2 Then
				intMilliSecs = "0" & intMilliSecs
			End If
		
			intSecond = int((intMilliSecond mod 60000 ) / 1000)
		
			intMinute = Int(intMilliSecond / 60000) 
				
			intHour = Int(intMinute / 60) 
			
			intMinute = intMinute Mod 60 
		
			If len(Cstr(intSecond)) = 1 Then
				intSecond = "0"&intSecond
			End if
			
			If len(Cstr(intMinute)) = 1 Then
				intMinute = "0"&intMinute
			End If
		
			If len(Cstr(intHour)) = 1 Then
				intHour = "0"&intHour
			End If
			
			ConvertMillisecsToTimeStamp = intHour & ":" & intMinute & ":" & intSecond & "." & intMilliSecs 
	On Error GOTO 0
End Function 


Function AddExecutionTime(byval odictTimeStamps)
   On error resume next
			Dim milliSecs , arrTime , executionTime , i
			milliSecs = 0
			arrTime = odictTimeStamps.items
			For i=0 to Ubound(arrTime)
					milliSecs = milliSecs + ConvertTimestampToMilliseconds(arrTime(i))
			Next
			executionTime = ConvertMillisecsToTimeStamp(milliSecs)

			AddExecutionTime = executionTime
   On error goto 0
End Function


'Input : Timestamp format hh:mm:ss.mmm
Function ConvertTimestampToMilliseconds(byval timeStamp)
   On error resume next
			Dim arrTemp , strTime
			Dim millisecs , intHour , intMinute , intSeconds , calculatedMillisecs
			arrTemp = Split(timeStamp , ".")
			strTime = arrTemp(0) 
			millisecs = arrTemp(1) 
			intHour = Hour(strTime)
			intMinute = Minute(strTime)
			intSeconds = Second(strTime)
			calculatedMillisecs = (intHour * 60 * 60 * 1000  ) + (intMinute * 60 * 1000 ) + (intSeconds * 1000 )
			millisecs  = millisecs  + calculatedMillisecs
			ConvertTimestampToMilliseconds = millisecs
   On error goto 0
End Function

''The input dictionary will store the status as the keys.
''Checking the existence of the key we can 
Function GetRollUpStatus(byval oDictStatus)
   On error resume next
		 
			Dim rollUpStatus
			rollUpStatus = STATUS_DEFAULT

			If  oDictStatus.exists(STATUS_DEFAULT) and oDictStatus.Count = 1 Then ' oDictStatus.Count = 1 to ensure that if the dic has multiple k/v pairs then the rollup should not be -1
					rollUpStatus = STATUS_DEFAULT
			else
					if  oDictStatus.exists(STATUS_FAILED)  Then 
							rollUpStatus = STATUS_FAILED
					else
							if oDictStatus.exists(STATUS_DEFECT)  Then 
									rollUpStatus = STATUS_DEFECT
							else
									if oDictStatus.exists(STATUS_PASSED)  Then 
											rollUpStatus = STATUS_PASSED
									else
											rollUpStatus = STATUS_DEFAULT
									End if
							End if
					End if
			End if

			GetRollUpStatus = rollUpStatus
	On error goto 0
End Function
