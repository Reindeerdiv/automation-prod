
'=======================Code Section Begin=============================================

Option Explicit		'Forces explicit declaration of all variables in a script. 



'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name :				CheckObjectExist()
' 	Purpose :					 Checks the existence of Specified object in AUT.
' 	Return Value :		 		  Boolean( 0/1)
' 	Arguments :
'		Input Arguments :  	   objObject
'		Output Arguments : 
'	Function is called by :     Browser(),Page(),Button(),CheckBox(),EditBox(),Element(),Image(),Link(),ListBox(),RadioButton(),Table(),MouseRightClick()
'	Function calls :
'	Created on :				 06/03/2008
'	Author :					   Rathna Reddy
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function CheckObjectExist(ByVal objObject)
	On Error Resume Next
	'Variable Section Begin
	Dim objexistence
	'No variables
	
	'Variable Section End
	Err.Clear
	objexistence = objObject.Exist
	If Err.number = 0 Then
		If objexistence Then
			
			CheckObjectExist  = True
			WriteStatusToLogs  "The specified object exist."
			
		Else
			CheckObjectExist = False
			WriteStatusToLogs  "The specified Object does not exist on application. There is a possibility of object description change or object hasn't yet appeared on application."
			
			Call VerifyParent(objObject)
		End If
		
	Else
		CheckObjectExist = False
		
		WriteStatusToLogs  "Specified object is missing in repository."
	End If
	
	
	On Error Goto 0
End Function
'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name :				 CheckObjectStatus()
' 	Purpose :					Verifies  status(Enabled/Disabled) of an object.
' 	Return Value :		 		  Boolean( 0/1)
' 	Arguments :
'		Input Arguments :  		objObject
'		Output Arguments :     intActData
'	Function is called by :      Button(),CheckBox(),EditBox(),Element(),Image(),Link(),ListBox(),RadioButton()
'	Function calls :            
'	Created on :				  06/03/2008
'	Author :						Rathna Reddy
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function CheckObjectStatus(ByVal objObject)
	On Error Resume Next
	'Variable Section Begin
	
	Dim intActData
	
	'Variable Section End
	intActData = objObject.GetROProperty("disabled") 
	If intActData = 1 Then
		CheckObjectStatus = False
		WriteStatusToLogs "The specified Object is disabled."
	ElseIf intActData = 0  Then
		CheckObjectStatus = True
		WriteStatusToLogs "The specified Object is enabled."
	End If
	
	On Error Goto 0
	
End Function
'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name :				 CheckObjectClick()
' 	Purpose :						 Performs click operation on specified object..
' 	Return Value :		 		   Boolean( 0/1)
' 	Arguments :
'		Input Arguments :  		objObject
'		Output Arguments : 
'	Function is called by :      Button() , Image() , Link()
'	Function calls :
'	Created on :				  07/03/2008
'	Author :					 Rathna Reddy
'
'-------------------------------------------------------------------------------------------------------------------------------- ----------
Function CheckObjectClick(ByVal objObject)
	On Error Resume Next
	'Variable Section Begin
	
	'No Variables
	
	'Variable Section End
	objObject.Click
	If Err.Number = 0 Then
		CheckObjectClick = True 
		WriteStatusToLogs   "The Object is click successfully."
		'WriteToInfoLog "Object Name" & objObject.ToString 
	Else
		CheckObjectClick = False
		WriteStatusToLogs   "The Click operation could not be performed."
	End If
	On Error Goto 0
End Function
'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name :			   CheckObjectSync()
' 	Purpose :					  Waits until Specified Browser/Page  is displayed.
' 	Return Value :		 		Boolean( 0/1)
' 	Arguments :
'		Input Arguments :  	 objObject
'		Output Arguments : 
'	Function is called by :   Browser() , Page()
'	Function calls :
'	Created on :				07/03/2008
'	Author :					  Rathna Reddy
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function CheckObjectSync(ByVal objObject)
	On Error Resume Next
	'Variable Section Begin
	
	'No variables
	
	'Variable Section End
	objObject.Sync
	If Err.Number = 0 Then
		CheckObjectSync = True
		WriteStatusToLogs  "Object sync successful."
	Else
		CheckObjectSync = False
		WriteStatusToLogs  "Object sync failed."
	End If
	On Error Goto 0
End Function



'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name :				CheckObjectNotExist()
' 	Purpose :			        Verifies object not exist in AUT.
' 	Return Value :		 		  Boolean( 0/1)
' 	Arguments :
'		Input Arguments :  	   objObject
'		Output Arguments : 
'	Function is called by :     Browser(),Page(),Button(),CheckBox(),EditBox(),Element(),Image(),Link(),ListBox(),RadioButton(),Table(),MouseRightClick()
'	Function calls :
'	Created on :				 06/03/2008
'	Author :					   Rathna Reddy
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function CheckObjectNotExist(ByVal objObject)
	On Error Resume Next
	'Variable Section Begin
	
	'No variables
	
	'Variable Section End
	If Not objObject.Exist  Then
		
		CheckObjectNotExist  = True
		WriteStatusToLogs  "The specified object does not exist."
	Else
		CheckObjectNotExist = False
		WriteStatusToLogs  "The specified object exists."
	End If
	
	
	On Error Goto 0
End Function

'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name :				CheckObjectVisible()
' 	Purpose :				       Checks whether the object is visible.
' 	Return Value :		 		  Boolean( 0/1)
' 	Arguments :
'		Input Arguments :  	   objObject
'		Output Arguments :     blnActData
'	Function is called by :     Button(),CheckBox(),EditBox(),Element(),Image(),Link(),ListBox(),RadioButton()
'	Function calls :            CompareStringValues()
'	Created on :				 06/03/2008
'	Author :					Rathna Reddy
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function CheckObjectVisible(ByVal objObject )
	On Error Resume Next
	'Variable Section Begin
	
	Dim blnActData
	
	'Variable Section End
	blnActData=objObject.GetRoProperty("visible")
	If Err.Number = 0 Then
		If  blnActData = True  Then	 								
			CheckObjectVisible = True
			WriteStatusToLogs   "The specified Object is visible."
		Else
			CheckObjectVisible = False 
			WriteStatusToLogs  "The specified Object is not visible."
		End If
	Else
		CheckObjectVisible = False
		WriteStatusToLogs "An error occurred during the method 'CheckObjectVisible'."
	End If
	On Error Goto 0			
End Function

'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name :				CheckObjectInVisible()
' 	Purpose :					Checks whether the object is visible.
' 	Return Value :		 		  Boolean( 0/1)
' 	Arguments :
'		Input Arguments :  	   objObject
'		Output Arguments :     strActData
'	Function is called by :     Button(),CheckBox(),EditBox(),Element(),Image(),Link(),ListBox(),RadioButton()
'	Function calls :            CompareStringValues()
'	Created on :				 06/03/2008
'	Author :					Rathna Reddy
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function CheckObjectInVisible(ByVal objObject )
	On Error Resume Next
	'Variable Section Begin
	
	Dim blnActData
	
	'Variable Section End
	blnActData=objObject.GetRoProperty("visible")
	If Err.Number = 0 Then
		If  blnActData = False  Then	 								
			CheckObjectInVisible = True
			WriteStatusToLogs "The Object is not visible."
		Else
			CheckObjectInVisible = False 
			WriteStatusToLogs   "The Object is visible."
		End If
	Else
		CheckObjectInVisible = False
		WriteStatusToLogs "An error occurred during the method 'CheckObjectInVisible'."
	End If
	On Error Goto 0			
End Function
'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name :				 CheckObjectEnabled()
' 	Purpose :				   Checks whether the object is Enabled.
' 	Return Value :		 		  Boolean( 0/1)
' 	Arguments :
'		Input Arguments :  		objObject
'		Output Arguments :     intActData
'	Function is called by :      Button(),CheckBox(),EditBox(),Element(),Image(),Link(),ListBox(),RadioButton()
'	Function calls :            
'	Created on :				  06/03/2008
'	Author :						Rathna Reddy
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function CheckObjectEnabled(ByVal objObject)
	On Error Resume Next
	'Variable Section Begin
	
	Dim intActData
	
	'Variable Section End
	intActData = objObject.GetRoProperty("Disabled")
	If  Err.Number = 0 Then
		If  intActData = 0 Then
			CheckObjectEnabled = True
			WriteStatusToLogs "The specified object is enabled."
		Else 
			
			CheckObjectEnabled = False
			WriteStatusToLogs "The specified object is disabled."
		End If
	Else
		CheckObjectEnabled = False
		WriteStatusToLogs "An error occurred while capturing object value."
	End If	  
	
	On Error Goto 0
	
End Function


'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name :				 CheckObjectDisabled()
' 	Purpose :					Checks whether the object is Disabled.
' 	Return Value :		 		  Boolean( 0/1)
' 	Arguments :
'		Input Arguments :  		objObject
'		Output Arguments :     intActData
'	Function is called by :      Button(),CheckBox(),EditBox(),Element(),Image(),Link(),ListBox(),RadioButton()
'	Function calls :            
'	Created on :				  06/03/2008
'	Author :						Rathna Reddy
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function CheckObjectDisabled(ByVal objObject)
	On Error Resume Next
	'Variable Section Begin
	
	Dim intActData
	
	'Variable Section End
	intActData = objObject.GetROProperty("disabled") 
	If  Err.Number = 0 Then
		If intActData = 1 Then
			CheckObjectDisabled = True
			WriteStatusToLogs "The specified object is disabled."
		Else
			CheckObjectDisabled = False
			WriteStatusToLogs "The specified object is enabled."
		End If
	Else
		CheckObjectDisabled = False
		WriteStatusToLogs "An error occurred while capturing object value."
	End If
	
	
	On Error Goto 0
	
End Function



'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :			  GetPropertyToStore()
'	Purpose :				  Stores the property of the specified object in a Global Dictionary. If the key already exist , then overrides the previous data
'							  If the property Name is not available/applicable for the object , then NULL [not a literal]  is store in the Dictionary
'	Return Value :		 	  Integer(0/1/2/3/4)
'							  0 : Success 
'							  1 : Object not Exist
'							  2 : Property not exist
'							  3 : PropetyName passed is invalid 
'							  4 : The key is invalid
'	Arguments :
'		Input Arguments :  	  objObject , strKey , strPropName
'		Output Arguments : 
'	Function is called by :	  	
'	Function calls :		  CheckObjectExist() , GetObjectProperty() , AddItemToStorageDictionary() , IsKey()
'	Created on :			  12/06/2009
'	Author :				  Iohbha K
'
'------------------------------------------------------------------------------------------------------------------------------------------

Function GetPropertyToStore(byval objObject ,  byval strKey , byval strPropName)
On error resume next
	'Variable Section Begin
	
		Dim strPropertyValue 
       
	'Variable Section End

	If isempty(strPropName) or isnull(strPropName) or trim(strPropName)="" Then
		GetPropertyToStore = 3
		
	Else
		If IsKey(strKey) = 0 Then
			If CheckObjectExist(objObject) Then
			' Comment:
			' 1. The property of value can be retrieve in any state , provided it exist.
			' 2. State can be like:
			' invisible , disabled , not-activated[ie a window comes in front not giving focus to parent object]
			' Hence no need to check for visible/disabled.
			' 3. Also need to add key,value in both situation else exception occurs will retrieving
				strPropertyValue = GetObjectProperty(objObject,Trim(strPropName))
				' Escape any Qualitia special char , if any
				strPropertyValue = PrefixEscape(strPropertyValue)
				If IsNull(strPropertyValue) Then
					GetPropertyToStore = 2
					strPropertyValue = null
				Else
					GetPropertyToStore = 0
				End If
				
				AddItemToStorageDictionary strKey ,strPropertyValue 

			Else
				GetPropertyToStore = 1 
				'Logging here is not required since CheckObjectExist have already log one.

			End If
		Else
			GetPropertyToStore = 4	
		End if
	End If
    
On error goto 0
End Function


'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :			  GetInstanceOfDataInColumn()
'	Purpose :				  Store the number of occurence of the strCellData in the particular column.
'							  The column number starts from 1. The comparison is casesensitive.
'							  Occurence 0 indicates that the data does not exist in the column , and the data will be pushed in the dictionary
'	Return Value :		 	  Integer(0/1/2/3/4/5/6/7)
'							  0 : Success 
'							  1 : Object not Exist 
'							  2 : An error occurred
'							  3 : Key passed is invalid 
'							  4 : The cell data is either null or empty 
'							  5 : The column number passed is not a whole number 
'							  6 : Failed to retrieve data from the specified java table
'							  7 : The column number does not exist
'	Arguments :
'		Input Arguments :  	  objJTable , strKey , intColNo , strCellData
'		Output Arguments : 
'	Function is called by :	  StoreInstanceOfDataFromJColumn()	
'	Function calls :		  CheckObjectExist() , AddItemToStorageDictionary() , IsKey()
'	Created on :			  01/07/2009
'	Author :				  Iohbha K
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function GetInstanceOfDataInColumn(ByVal objJTable , ByVal strKey , ByVal intColNo , ByVal strCellData)
On Error Resume Next
	
	'Variable Section Begin
		Dim intActColCount	, arrColData
		Dim intInstance , intCnt
		
		intInstance = 0
	'Variable Section End

	If IsKey(strKey) = 0 Then
		If Not IsNull(strCellData) And Not IsEmpty(strCellData) Then
			If IsWholeNumber(intColNo) = 0 Then
				If CheckObjectExist(objJTable)  Then 
					intActColCount = objJTable.GetROProperty("cols")
					If Err.number = 0 And Not IsEmpty(intActColCount) And Not IsNull(intActColCount) Then
						
						intColNo = CInt(intColNo)
						intActColCount = CInt(intActColCount)

						If intColNo>0 And intColNo <= intActColCount Then
							arrColData = GetJTableColData(objJTable,intColNo)
							If Not IsNull(arrColData) And IsArray(arrColData) Then
								For intCnt = 0 To UBound(arrColData)
									If CompareStringValuesCaseSensitive(strCellData,arrColData(intCnt)) = 0 Then
										intInstance = intInstance + 1
									End if
								Next
								
								If Err.number = 0 Then
									AddItemToStorageDictionary strKey , intInstance
									GetInstanceOfDataInColumn = 0
								Else
									GetInstanceOfDataInColumn = 2
								End if

							Else
								GetInstanceOfDataInColumn = 6
							End if
						Else
							GetInstanceOfDataInColumn = 7
						End If
						
					Else
						GetInstanceOfDataInColumn = 6
					End If
					
				Else
					GetInstanceOfDataInColumn = 1
				End If
				
			Else
				GetInstanceOfDataInColumn = 5
			End if
		Else
			GetInstanceOfDataInColumn = 4
		End if
		
	Else
		GetInstanceOfDataInColumn = 3
	End if

On Error goto 0
End Function


'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name			:	GetRowNumberForNInstanceofDataInColumn()
' 	Purpose					:	Stores the row number of the Nth occurence of the specified cell data within a specified column
' 	Return Value			:	Boolean( 0/1/2/3/4/5/6/7)
' 	Arguments				:
'		Input Arguments		:  	objJTable, strKey, intColNo, strCellData, n
'		Output Arguments	:   
'	Function is called by	:   StoreRowNumberHavingCellData()
'	Function calls			:   IsWholeNumber(), CheckObjectExist(), CompareStringValuesCaseSensitive(), AddItemToStorageDictionary()
'	Created on				:	03/07/2009
'	Author					:	Vaibhav Kelkar
'
'------------------------------------------------------------------------------------------------------------------------------------------

Function GetRowNumberForNInstanceofDataInColumn(ByVal objJTable , ByVal strKey , ByVal intColNo , ByVal strCellData, byval n)
On Error Resume Next
	
	'Variable Section Begin
		Dim intActRowCount	, arrColData, intActColCount, intCntFound
		Dim intInstance , intCnt
		Dim intN
		Dim blnFound
		Dim strActCellData
		intInstance = 0
		blnFound = false 
	'Variable Section End
	'Variable Section End
	If IsKey(strKey) = 0 Then
		If Not IsNull(strCellData) Then
			If IsPositiveInteger(intColNo) = 0 Then
				If CheckObjectExist(objJTable)  Then 
				   
				 	intActColCount = objJTable.GetROProperty("cols")
					If Err.number = 0 And Not IsEmpty(intActColCount) And Not IsNull(intActColCount) Then
						intActColCount = cint(intActColCount)
						 intActRowCount = objJTable.GetROProperty("rows")
						  If Err.number = 0 And Not IsEmpty(intActRowCount) And Not IsNull(intActRowCount) Then
								intColNo = CInt(intColNo)
								intActRowCount = CInt(intActRowCount)
								intN = Cint(n)						
		
								If intColNo>0 And intColNo <= intActColCount Then
								  
									
										For intCnt = 1 To intActRowCount
											strActCellData = objJTable.GetCellData(intCnt,intColNo)
											If CompareStringValuesCaseSensitive(strCellData,strActCellData) = 0 Then
												intInstance = intInstance + 1
												If  intInstance = intN Then
															intCntFound = intCnt 
															AddItemToStorageDictionary strKey , intCntFound
															blnFound = true
															Exit for
														
												End If
											End if
										Next

										If  blnFound Then
											   GetRowNumberForNInstanceofDataInColumn = 0
										Else
											   GetRowNumberForNInstanceofDataInColumn = 2
										End If	 		
									
								Else
									GetRowNumberForNInstanceofDataInColumn = 7
								End If
						Else
								 GetRowNumberForNInstanceofDataInColumn = 6
						End If
						
					Else
						GetRowNumberForNInstanceofDataInColumn = 6
					End If
					
   
				Else
					GetRowNumberForNInstanceofDataInColumn = 1
				End If
				
			Else
				GetRowNumberForNInstanceofDataInColumn = 5
			End if
		Else
			GetRowNumberForNInstanceofDataInColumn = 4
		End if
		
	Else
		GetRowNumberForNInstanceofDataInColumn = 3
	End if

On Error goto 0
End Function


'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :			  GetColumnNumber()
'	Purpose :				  Stores the column number of the column header pass
'	Return Value :		 	  Integer(0/1/2/3/4/5)
'							  0 : Success 
'							  1 : Object not Exist 
'							  2 : The column header does not exist
'							  3 : Key passed is invalid 
'							  4 : The column header is either null or empty 
'							  5 : Failed to retrieve table data 
'	Arguments :
'		Input Arguments :  	  objJTable , strKey , strColName
'		Output Arguments : 
'	Function is called by :	  StoreJColumnNumber()	
'	Function calls :		  CheckObjectExist() , AddItemToStorageDictionary() , IsKey()
'	Created on :			  01/07/2009
'	Author :				  Iohbha K
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function GetColumnNumber(ByVal objJTable, ByVal strKey, ByVal strColName)
On Error Resume Next

	'Variable Section Begin
		Dim intActColCount , intCnt , strActColName
		Dim blnFound , intColnoFound
		blnFound = false
	'Variable Section End

	If IsKey(strKey) = 0 Then
		If Not IsNull(strColName) And Not IsEmpty(strColName) Then
			If CheckObjectExist(objJTable)  Then 

				intActColCount = objJTable.getROProperty("cols")
				If Err.number =0 And Not isempty(intActColCount) And Not IsNull(intActColCount) Then
					
					For intCnt = 0 To intActColCount-1
						strActColName = objJTable.GetColumnName(CInt(intCnt))
						If Err.number = 0 And Not IsEmpty(strActColName) Then
							If Trim(Ucase(strActColName)) = Trim(UCase(strColName)) Then
								blnFound = True
								intColnoFound = intCnt + 1
								Exit For
							End If
						Else
							GetColumnNumber = 5
							Exit Function
						End if
		
					Next

					If blnFound = True Then 
						AddItemToStorageDictionary strKey , intColnoFound 			    
						GetColumnNumber= 0
					Else
						GetColumnNumber= 2
					End if

				Else
					GetColumnNumber = 5	
				End if

			Else
				GetColumnNumber = 1
			End If
			
		Else
			GetColumnNumber = 4
		End if

	Else
		GetColumnNumber = 3
	End if

On Error goto 0
End Function



'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :			  GetTableRowColumnCount()
'	Purpose :				  Stores the column count of the specified row of the table. The first row
'							  of the table is 1. The row passed should be a natural number.	
'	Return Value :		 	  Integer(0/1/2/3/4/5)
'							  0 : Success 
'							  1 : Object not Exist 
'							  2 : Row does not exist
'							  3 : Row passed is invalid 
'							  4 : Row is not a Natural Number
'							  5 : Key passed is invalid
'	Arguments :
'		Input Arguments :  	  objObjectTable , strKey , intRow
'		Output Arguments : 
'	Function is called by :	  StoreColumnCount()
'	Function calls :		  CheckObjectExist() , IsWholeNumber() , AddItemToStorageDictionary(), IsKey()
'	Created on :			  12/06/2009
'	Author :				  Iohbha K
'
'------------------------------------------------------------------------------------------------------------------------------------------

Function GetTableRowColumnCount(byval objObjectTable , byval strKey , byval intRow)
On Error Resume Next
	'Variable Section Begin

		Dim intTableRowCount 
	   
	'Variable Section End
		
	 If isempty(intRow) or isnull(intRow)  or trim(intRow)="" Then
	     GetTableRowColumnCount = 3
		
	 Else
		If IsKey(strKey) = 0 Then
			If CheckObjectExist(objObjectTable)  Then 
					If IsPositiveInteger(intRow) = 0 Then
				   
							intTableRowCount =objObjectTable.ColumnCount (intRow)
							If err.Number = 0 Then
								GetTableRowColumnCount = 0
							Else
							
								GetTableRowColumnCount = 2
								intTableRowCount = null
							End if
		
							AddItemToStorageDictionary strKey , intTableRowCount
						
					Else
					
						GetTableRowColumnCount = 4
						
					End if
			Else
				GetTableRowColumnCount = 1
			End if 
		Else
			GetTableRowColumnCount = 5
		End if
	End if
	   		
On Error GoTo 0
End Function


'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :			  GetTableRowCount()
'	Purpose :				  Stores the row count of the specified table	
'	Return Value :		 	  Integer(0/1/2/3)
'							  0 : Success 
'							  1 : Object not Exist 
'							  2 : Could not retrieve row count
'							  3 : Key passed is invalid 
'	Arguments :
'		Input Arguments :  	  objObject , strKey
'		Output Arguments : 
'	Function is called by :	  StoreRowCount()	
'	Function calls :		  CheckObjectExist() , AddItemToStorageDictionary() , IsKey()
'	Created on :			  12/06/2009
'	Author :				  Iohbha K
'
'------------------------------------------------------------------------------------------------------------------------------------------

Function GetTableRowCount(byval objObjectTable , byval strKey)
On Error Resume Next
	'Variable Section Begin

	 Dim intTableRowCount 
	   
	'Variable Section End
		
	 If IsKey(strKey) = 0  Then
			 
		If CheckObjectExist(objObjectTable)  Then 
			intTableRowCount=objObjectTable.RowCount
			If err.Number = 0 Then

				AddItemToStorageDictionary strKey , intTableRowCount 			    
				GetTableRowCount= 0
			Else
				GetTableRowCount= 2
			End if
			
		Else
			GetTableRowCount= 1
		End if 

	Else
		GetTableRowCount = 3
	End if
					
On Error GoTo 0
End Function



'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :				 GetJTableColData()
'	Purpose :					 Returns the column data in an array. The column is assumed to exist or it is assumed to be 
'								 prechecked by the calling function. The column number starts from 1.
'	Return Value :		 		 Array / Null
'	Arguments :
'		Input Arguments :  		 objTable , intColNum
'		Output Arguments : 
'	Function is called by :
'	Function calls :			  
'	Created on :				 29/06/2009
'	Author :					 Iohbha K
'
'------------------------------------------------------------------------------------------------------------------------------------------

'Function GetJTableColData(byval objJTable , byval intColNum)
'On error resume next
'	Dim intRow , arrColData()
'	Dim strTemp , intCnt
'
'	intRow = objJTable.getROProperty("rows")
'
'	If  err.number = 0 and not isnull(intRow) and  not isempty(intRow) and intRow <> "" Then
'
'		Redim Preserve arrColData(cint(intRow-1))
'		For intCnt = 1 to intRow
'			strTemp = objJTable.GetCellData (Cint(intCnt) , Cint(intColNum))
'			If err.number = 0 Then
'				arrColData(intCnt-1) = strTemp
'			Else
'				GetJTableColData = null		
'				Exit Function
'            End If
'		Next
'		GetJTableColData = arrColData
'	Else
'		GetJTableColData = null		
'	End If
'	
'On error goto 0
'End Function



'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name :				CheckImageEnability()
' 	Purpose :					Checks the enability of the image.
'								Returns 0:If image in enabled
'								Returns 1:If an exception occures
'								Returns 2:If image in disabled
' 	Return Value :		 		Integer(0/1/2)
' 	Arguments :
'		Input Arguments :  		objImage
'		Output Arguments : 
'	Function is called by :
'	Function calls :			
'	Created on :				31/05/2011
'	Author :					Amruta B 
'------------------------------------------------------------------------------------------------------------------------------------------
Function CheckImageEnability(ByVal objObject)
On Error Resume Next
	'Variable Section Begin
	
		Dim intActState
	
	'Variable Section End
	intActState=objObject.Object.disabled
	If Err.Number=0  And Not IsNull(intActState) And Not IsEmpty(intActState) Then
		If UCase(Trim(intActState))="TRUE" Then
			CheckImageEnability=2	'Object is disabled
		Else UCase(Trim(intActState))="FALSE" 
			CheckImageEnability=0	'object is enabled
		End If
	Else 
		CheckImageEnability = 1 ' Exception occurred
	End If 

On Error Goto 0
End Function


'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name :				 SetSecure()
' 	Purpose :						 Enters a value into editbox
' 	Return Value :		 		   Integer(0/1)
' 	Arguments :
'		Input Arguments :  		objEditbox , strEncriptedValue
'		Output Arguments : 
'	Function is called by :
'	Function calls :			    CheckObjectExist()
'	Created on :				   15-June-2016
'	Author :						 Rajneesh Kumar 
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function SetSecure(ByVal objEditbox , ByVal strEncriptedValue)
	On Error Resume Next
		'Variable Section Begin
			
		Dim strMethod
		Dim ObjSecure
		'Variable Section End
		Set ObjSecure = CreateObject("QualitiaCore._SecureTestData")
		strMethod = strKeywordForReporting
			   If CheckObjectExist(objEditbox) Then
					If CheckObjectEnabled(objEditBox) Then
						If CheckObjectVisible(objEditBox) Then 
							 objEditbox.Click
'							 
							 strEncriptedValue = ObjSecure.getEncryptedData(strEncriptedValue)
							 objEditbox.Set strEncriptedValue
							 If err.Number = 0 Then
									SetSecure = 0
									WriteStatusToLogs "The value: "&strEncriptedValue&" is set-secured in EditBox successfully."
							 Else
									SetSecure = 1
									WriteStatusToLogs "The value: "&strEncriptedValue&" is not set-secured in EditBox, please verify."
							  
							 End If
						Else
							SetSecure = 1
							WriteStatusToLogs "The EditBox is not visible, please verify."

						End If
					Else
					SetSecure = 1
					WriteStatusToLogs "The EditBox is not enabled, please verify."


					End If
				  
				Else
						SetSecure = 1
						WriteStatusToLogs "The EditBox does not exist, please verify."
				End If
		Set ObjSecure = Nothing
	On Error GoTo 0
End Function


Function CheckObjectVisible(ByVal objObject)
	On Error Resume Next
	'Variable Section Begin
	Dim objvisibility
	'No variables
	
	'Variable Section End
	
	objvisibility = objObject.GetRoProperty("visible")  
	
	If Err.number = 0 Then
		If objvisibility Then
			
			CheckObjectVisible  = True
			WriteStatusToLogs  "The specified object visible."
			
		Else
			CheckObjectVisible = False
			WriteStatusToLogs  "The specified Object is not visible."
		End If
		
	Else
		CheckObjectVisible = False
		WriteStatusToLogs  "An Error occurred during the method 'CheckObjectVisible'."
	End If
	
	
	On Error Goto 0
End Function


'-------------------------------------------------------------------------
'Function name: VerifyParent(ByVal Obj)
'Written by Rajneesh

'Function VerifyParent(ByVal Obj)
'	On error resume next
'	
'	Dim objParent, parentName, blnParentExist, parentClass
'	
'	Set objParent = Obj.GetTOProperty("parent")
'	parentName = objParent.GetTOProperty("TestObjName")
'	parentClass = objParent.GetTOProperty("to_class")
'	
'	If objParent.Exist(0) Then
'		blnParentExist = true
'	Else
'		blnParentExist = false
'	End If
'
'	
'    If parentName <> "" and Not blnParentExist Then
'	    VerifyParent(objParent)
'	Else
'		if parentName = "" then
'			WriteStatusToLogs oDictErrorMessages.Item("NoParentFoundInHierarchy")
'		else
'			WriteStatusToLogs oDictErrorMessages.Item("ParentFoundInHierarchy") & parentClass & "(" & DoubleQuotes(parentName) & ")"
'		End if
'	Exit function
'    End If
'End Function

Function VerifyParent(ByVal Obj)
	On error resume next
	
	Dim objParent, parentName, blnParentExist, parentClass
	Dim i, objRE, desc
	
	Set objRE = New RegExp
	i= 0 
	Set objParent = Obj.GetTOProperty("parent")
	parentName = objParent.GetTOProperty("TestObjName")
	parentClass = objParent.GetTOProperty("to_class")
	
	With objRE
		.Pattern    = "\[ \w.* \]"
		.IgnoreCase = True
		.Global     = False
	End With
	
	If objRE.Test( parentName )  Then ''add code to generate logical desc for DP
		Set Props = objParent.gettoproperties
		    PropsCount = Props.Count
		For i =0 To PropsCount - 1
		    PropName = Props(i).Name
		    PropValue = Props(i).Value
		    If i=0  Then
		    	desc =  desc & PropName & " = " & PropValue
		    	Else
		    	desc =  desc & COND_BLOCK_SAPERATOR & " " & PropName & " = " & PropValue
		    End If
  
		Next
	End If
	
	If objParent.Exist(0) Then
		blnParentExist = true
	Else
		blnParentExist = false
	End If
	
    If parentName <> "" and Not blnParentExist Then
	    VerifyParent(objParent)
	Else
		If parentName = "" then
			WriteStatusToLogs oDictErrorMessages.Item("NoParentFoundInHierarchy")
		Else
			If i = 0 Then
				WriteStatusToLogs oDictErrorMessages.Item("ParentFoundInHierarchy") & parentClass & "(" & DoubleQuotes(parentName) & ")"
			Else
			 	WriteStatusToLogs oDictErrorMessages.Item("ParentFoundInHierarchyDP") & "(" & DoubleQuotes(desc) & ")"
			End If
			
		End if
		Set objRE = Nothing
		Exit function
    End If
End Function


Function CheckObjectExistEngine(ByVal objObject, ByVal actionname)
	On Error Resume Next
	Dim ErrMsg
	
	'Variable Section Begin
	
	'No variables
	
	'Variable Section End   WaitForObjectExist
	If ucase(actionname) = "WAITFOROBJECTEXIST"  Then
		CheckObjectExistEngine=true
		Exit function
	End If
	Err.Clear

	Dim prop
	prop=objObject.getToProperty("TestObjName")
		If isempty(prop) Then
			
			CheckObjectExistEngine  = False
			ErrMsg = ""													
			ErrMsg = ErrMsg & strExeTCName
			ErrMsg = ErrMsg & " Either the current object or one of the objects in object identification hierarchy is not found in UFT repository file."
			WriteToDebugLog ErrMsg
			WriteToInfoLog ErrMsg
		Else
			CheckObjectExistEngine = True
			'WriteStatusToLogs  "Method Name:" &"CheckObjectExistEngine"&vbTab&_
			'"Message:" & "Either the current object or one of the objects in object identification hierarchy is not found in UFT repository."
'			ErrMsg = ""													
'			ErrMsg = ErrMsg & "<br><span style='margin-left:3px;border:1px solid;padding:.2em;border-radius:25px;background-color:#F5A9A9;'><b>" 
'			ErrMsg = ErrMsg & strExeTCName
'			ErrMsg = ErrMsg & "</b> Either the current object or one of the objects in object identification hierarchy is not found in UFT repository file. </span>"
'			WriteToDebugLog ErrMsg
'			WriteToInfoLog ErrMsg
		End If

	
	On Error Goto 0
End Function
'=======================Code Section End=============================================

Function Q_FireEvent(ByVal objObject,ByVal eventType,ByVal XCoord,ByVal YCoord)
	On Error Resume Next

		'Variable Section Begin
	
			Dim strMethod 

		'Variable Section End
		strMethod = strKeywordForReporting
		If CheckObjectExist(objObject) Then 
		   If CheckJavaObjectEnabled(objObject) Then
			   If Not Isempty(intXCoordinate) or Not isnull(intXCoordinate) And Not Isempty(intYCoordinate) or Not isnull(intYCoordinate) Then
			   
						objObject.FireEvent eventType,0,"BUTTON1_MASK",XCoord,YCoord,1,"OFF" 
						
					If Err.Number = 0 Then
						Q_FireEvent = True
						WriteStatusToLogs "FireEvent has been performed on button successfully."
					Else
						Q_FireEvent = False
						WriteStatusToLogs "FireEvent could not be performed on button, please verify."
					End If
					
				Else
						Q_FireEvent = False
						WriteStatusToLogs "The ' "& intXCoordinate &"' and '"&intYCoordinate&"' is null or Invalid, please verify."
				End If	
					
			Else
				Q_FireEvent = 1
				WriteStatusToLogs "The Object was disabled and click could not be performed, please verify."
			End If
	    Else
            Q_FireEvent = 1
		    WriteStatusToLogs "The Object does not exist, please verify."
			  End If
			
		
	On Error GoTo 0
End Function

		
'Forces explicit declaration of all variables in a script. 

'----------------------------------------------------------------------------------------------------------------
' 	Function Name :				Q_StoreInstanceCount()
' 	Purpose :					Returns the count of total number of instances matching the given description.
' 	Return Value :		 		
' 	Arguments :
'		Input Arguments :  	   objObject
'		Output Arguments : 
'	Function calls :
'	Created on :				 17/08/2016
'	Author :					   Qualitia
'	Example:
'srtDesc: "is child window=False,is owned window=False", "Key"
'strKey: "Dict Key"
'Return: Dict Key.
'------------------------------------------------------------------------------------------------------------------------------------------
Function Q_StoreInstanceCount(ByVal obj,byVal srtDesc, byVal strKey)
 On error resume next
	'Variable Section Begin
		Dim desc1, desc2, desc, i, objDesc, c
	'Variable Section End
	
	desc = srtDesc
	desc1 = split (desc , ",")
	Set objDesc = Description.Create()
	For i=0 to UBound(desc1)
	desc2 = split(desc1(i), "=")
	Execute "objDesc(" & chr(34) & desc2(0) & chr(34) &").Value = " & chr(34)& desc2(1) & chr(34)
	Next
 
		c =  Desktop.ChildObjects(objDesc).Count
		AddItemToStorageDictionary strKey, c-1
 
	 
		 Q_StoreInstanceCount= 1
		 WriteStatusToLogs "The occurrence count: " & c-1
	 
End Function

'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :			  Q_PressKey()	
'	Purpose :				  Presses the specified key and modifier in the object
'	Return Value :		 	  Integer(0/1/2)
'	Arguments :
'		Input Arguments :  	  objObject,strKey,strModifier
'		Output Arguments : 
'	Function is called by :
'	Function calls :		    CheckObjectExist()
'	Created on :			    21/07/2016
'	Author :					Qualitia 
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function Q_PressKey(ByVal objObject,ByVal strKey)
	On Error Resume Next

		'Variable Section Begin
	
			Dim strMethod 

		'Variable Section End
		strMethod = strKeywordForReporting
		
		If isNull(strKey) or IsEmpty(strKey) or trim(strKey)= " "Then
			Q_PressKey = 1 
			WriteStatusToLogs  "The parameter is either null or empty, please verify."
		Exit Function
		End If	
		
		If CheckObjectExist(objObject) Then 
		      If Not Isempty(strKey) or Not isnull(strKey) = " "Then
			  
						objObject.PressKey strKey

						
					If Err.Number = 0 Then
						Q_PressKey=0
						WriteStatusToLogs "PressKey operation performed successfully."
					Else
						Q_PressKey = 1
						WriteStatusToLogs "PressKey operation could not be performed successfully, please verify."
					End If
					
				Else
						Q_PressKey = 1
						WriteStatusToLogs "The '"& strKey &"' is null or Invalid , please verify."
				End If	
					
			
	    Else
            Q_PressKey = 1
		    WriteStatusToLogs "The Object does not exist, please verify."
			  End If			
		 
	On Error GoTo 0
End Function

'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :			  Q_CaptureBitmap()	
'	Purpose :				  Screen capture of the object as a .png or .bmp image using the specified file name.
'	Return Value :		 	  Integer(0/1/2)
'	Arguments :
'		Input Arguments :  	  objObject,strFullFileName
'		Output Arguments : 
'	Function is called by :
'	Function calls :		    CheckObjectExist()
'	Created on :			    21/07/2016
'	Author :					Qualitia 
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function Q_CaptureBitmap(ByVal objObject,ByVal strFullFileName)
	On Error Resume Next

		'Variable Section Begin
	
			Dim strMethod 

		'Variable Section End
		strMethod = strKeywordForReporting
		If CheckObjectExist(objObject) Then 
		      If Not Isempty(strFullFileName) or Not isnull(strFullFileName) or trim(strFullFileName) = "" Then
			   
						objObject.CaptureBitmap StrFullFileName, True 
						
					If Err.Number = 0 Then
						Q_CaptureBitmap = 0
						WriteStatusToLogs "CaptureBitmap operation performed successfully."
					Else
						Q_CaptureBitmap = 1
						WriteStatusToLogs "CaptureBitmap operation could not be performed successfully, please verify."
					End If
					
				Else
						Q_CaptureBitmap = 1
						WriteStatusToLogs "The ' "& strFullFileName &"'is null or invalid, please verify."
				End If	
					
			
	    Else
            Q_CaptureBitmaps = 1
		    WriteStatusToLogs "The Object does not exist, please verify."
			  End If
			
		
	On Error GoTo 0
End Function

'------------------------------------------------------------------------------------------------------------------------------------------
'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :			  Q_DblClickAt()	
'	Purpose :				  Double-clicks the specified location with the specified mouse button
'	Return Value :		 	  Integer(0/1/2)
'	Arguments :
'		Input Arguments :  	  objObject,intXCoordinate,intYCoordinate
'		Output Arguments : 
'	Function is called by :
'	Function calls :		    CheckObjectExist() , CheckJavaObjectEnabled()
'	Created on :			    21/06/2016
'	Author :					Qualitia 
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function Q_DblClickAt(ByVal objObject,ByVal intXCoordinate,ByVal intYCoordinate)
	On Error Resume Next

		'Variable Section Begin
	
			Dim strMethod 

		'Variable Section End
		strMethod = strKeywordForReporting
		If CheckObjectExist(objObject) Then 
		   If CheckJavaObjectEnabled(objObject) Then
			   If Not Isempty(intXCoordinate) or Not isnull(intXCoordinate) And Not Isempty(intYCoordinate) or Not isnull(intYCoordinate) Then
			   
						objObject.DblClick intXCoordinate,intYCoordinate 
						
					If Err.Number = 0 Then
						Q_DblClickAt = 0
						WriteStatusToLogs "Q_DblClick has been performed on button successfully."
					Else
						Q_DblClickAt = 1
						WriteStatusToLogs "Q_DblClick could not be performed on button, please verify."
					End If
					
				Else
						Q_DblClickAt = 1
						WriteStatusToLogs "The ' "& intXCoordinate &"' and '"&intYCoordinate&"' is null or invalid, please verify."
				End If	
					
			Else
				Q_DblClickAt = 1
				WriteStatusToLogs "The Object was disabled and click could not be performed, please verify."
			End If
	    Else
            Q_DblClickAt = 1
		    WriteStatusToLogs "The Object  does not exist, please verify."
			  End If
			
		
	On Error GoTo 0
End Function

'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :			  Q_Refresh()	
'	Purpose :				  Re-identify the object in the application the next time a step refers to this object.
'	Return Value :		 	  Integer(0/1/2)
'	Arguments :
'		Input Arguments :  	  objObject
'		Output Arguments : 
'	Function is called by :
'	Function calls :		    CheckObjectExist() , CheckJavaObjectEnabled()
'	Created on :			    21/06/2016
'	Author :					Qualitia 
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function Q_Refresh(ByVal objObject)
	On Error Resume Next

		'Variable Section Begin
	
			Dim strMethod 

		'Variable Section End
		strMethod = strKeywordForReporting
			If CheckObjectExist(objObject) Then 
			   If CheckJavaObjectEnabled(objObject) Then
			   
						objObject.RefreshObject
						
					If Err.Number = 0 Then
						Q_Refresh = 0
						WriteStatusToLogs "Object Refresh successfully."
					Else
						Q_Refresh = 1
						WriteStatusToLogs "Object not Refresh successfully, please verify."
					End If
				Else
					Q_Refresh = 1
					WriteStatusToLogs "The Object was disabled and click could not be performed, please verify."
				End If
			  Else
                   Q_Refresh = 1
				   WriteStatusToLogs "The Object does not exist, please verify."
			  End If
			
		
	On Error GoTo 0
End Function

'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :			  Q_Type()	
'	Purpose :				  Types the specified text in the object.
'	Return Value :		 	  Integer(0/1/2)
'	Arguments :
'		Input Arguments :  	  objObject,strValue
'		Output Arguments : 
'	Function is called by :
'	Function calls :		    CheckObjectExist(),CheckJavaObjectEnabled()
'	Created on :			    21/06/2016
'	Author :					Qualitia 
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function Q_Type(ByVal objObject,ByVal strValue)
	On Error Resume Next

		'Variable Section Begin
	
			Dim strMethod 

		'Variable Section End
		strMethod = strKeywordForReporting
			If CheckObjectExist(objObject) Then 
			   If CheckJavaObjectEnabled(objObject) Then
			   
						objObject.Type strValue
						
					If Err.Number = 0 Then
						Q_Type = 0
						WriteStatusToLogs "Specified text has been set successfully."
					
					Else
						Q_Type = 1
						WriteStatusToLogs "Specified text could not set successfully, please verify."
					End If
				Else
					Q_Type = 1
					WriteStatusToLogs "The objObject was disabled and click could not be performed, please verify."
				End If
			  Else
                   Q_Type = 1
				   WriteStatusToLogs "The objObject does not exist, please verify."
			  End If
			
		
	On Error GoTo 0
End Function


'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :			  Q_StoreChildCount()	
'	Purpose :				  The method returns the child objects that fit the description specified (using the Properties object). 
'	Return Value :		 	  Integer(0/1/2)
'	Arguments :
'		Input Arguments :  	  objObject,strPropertyName,strPropertyValue,strKey
'		Output Arguments : 
'	Function is called by :
'	Function calls :		    CheckObjectExist(),CheckJavaObjectEnabled()
'	Created on :			    21/06/2016
'	Author :					Qualitia 
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function Q_StoreChildCount(ByVal objObject,ByVal strKey,ByVal strPropertyName,ByVal strPropertyValue)
	On Error Resume Next
'msgbox err.number
		'Variable Section Begin
	
			Dim strMethod 
			Dim Objectcount

		'Variable Section End
		strMethod = strKeywordForReporting
		
		If strKey = "" or strPropertyName = "" or strPropertyValue = "" Then
			Q_StoreChildCount = 1						
			WriteStatusToLogs "Invalid parameters."
		End If
		
			If CheckObjectExist(objObject) Then 
			   If CheckJavaObjectEnabled(objObject) Then
						
						Set mobj=Description.Create
						mobj(strPropertyName).value= strPropertyValue
						Set ocollection=objObject.ChildObjects(mobj)
						Objectcount=ocollection.count
						AddItemToStorageDictionary strKey ,Objectcount
					
					If Err.Number = 0 Then
						Q_StoreChildCount = 0
						WriteStatusToLogs "The child objects counts is '"& Objectcount &"' has stored successfully in the Key '"&strKey&"'"
										 
									
					Else
						Q_StoreChildCount = 1						
						WriteStatusToLogs "The object property name'"& strPropertyName &"' and property value '"&strPropertyValue&"' are invalid, please verify."
				
					End If				
				
				Else
					Q_StoreChildObjectCounts = 1
					WriteStatusToLogs "The Object was disabled and click could not be performed, please verify."
				End If
			  Else
                    Q_StoreChildObjectCounts = 1
				   WriteStatusToLogs "The Object does not exist, please verify."
			  End If
			
		
	On Error GoTo 0
End Function
'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :			  Q_MouseDrag()	
'	Purpose :				  Performs a mouse drag and drop operation from the specified X1, Y1 coordinates to the specified Xn, Yn coordinates
'	Return Value :		 	  Integer(0/1/2)
'	Arguments :
'		Input Arguments :  	  objObject,intX1Coord,intY1Coord,intX2Coord,intY2Coord
'		Output Arguments : 
'	Function is called by :
'	Function calls :		    CheckObjectExist(),CheckJavaObjectEnabled()
'	Created on :			    21/06/2016
'	Author :					Qualitia 
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function Q_MouseDrag(ByVal objObject,ByVal intX1Coord,ByVal intY1Coord,ByVal intX2Coord,ByVal intY2Coord)
	On Error Resume Next

		'Variable Section Begin
	
			Dim strMethod 

		'Variable Section End
		strMethod = strKeywordForReporting
			If CheckObjectExist(objObject) Then 
			   If CheckJavaObjectEnabled(objObject) Then
			   
						objObject.MouseDrag intX1Coord,intY1Coord,intX2Coord,intY2Coord
						
					If Err.Number = 0 Then
						Q_MouseDrag = 0
						WriteStatusToLogs "MouseDrag Function performed successfully."
										
					Else
						Q_MouseDrag = 1
						WriteStatusToLogs "Operation could not be performed successfully, please verify."
					End If
				Else
					Q_MouseDrag = 1
					WriteStatusToLogs "The Object was disabled and click could not be performed, please verify."
				End If
			  Else
                    Q_MouseDrag = 1
				   WriteStatusToLogs "The Object  does not exist, please verify."
			  End If
			
		
	On Error GoTo 0
End Function

'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :			  Q_StoreText()	
'	Purpose :				  Store Text from object
'	Return Value :		 	  Integer(0/1)
'	Arguments :
'		Input Arguments :  	  objObject,strKey
'		Output Arguments : 
'	Function is called by :
'	Function calls :		    CheckObjectExist(),CheckJavaObjectEnabled()
'	Created on :			    21/06/2016
'	Author :					Qualitia 
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function Q_StoreText(ByVal objObject,ByVal strKey)
	On Error Resume Next

		'Variable Section Begin
	
			Dim strMethod 
			Dim strValue
		'Variable Section End
		strMethod = strKeywordForReporting
			If CheckObjectExist(objObject) Then 
			   If CheckJavaObjectEnabled(objObject) Then
			   
						strValue=objObject.GetROProperty("value")
						AddItemToStorageDictionary strKey,strValue
						
					If Err.Number = 0 Then
						Q_StoreText = 0
						WriteStatusToLogs "The Text '"& strValue &"' is stored successfully in the Key '"&strKey&"'"
										
					Else
						Q_StoreText = 1
						WriteStatusToLogs "The Text ' "& strValue &" ' is not stored successfully in the Key ' "&strKey&" ', please verify."
					End If
				Else
					Q_StoreText = 1
					WriteStatusToLogs "The Object was disabled and click could not be performed, please verify."
				End If
			  Else
                    Q_StoreText = 1
				   WriteStatusToLogs "The Object does not exist, please verify."
			  End If
		
		
	On Error GoTo 0
End Function
'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :			  Q_SetFocus()	
'	Purpose :				  Sets the focus on the edit box.  
'	Return Value :		 	  Integer(0/1)
'	Arguments :
'		Input Arguments :  	  objObject
'		Output Arguments : 
'	Function is called by :
'	Function calls :		    CheckObjectExist(),CheckJavaObjectEnabled()
'	Created on :			    21/06/2016
'	Author :					Qualitia 
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function Q_SetFocus(ByVal objObject)
	On Error Resume Next

		'Variable Section Begin
	
			Dim strMethod 
			Dim strValue
		'Variable Section End
		strMethod = strKeywordForReporting
		If CheckObjectExist(objObject) Then 
			   If CheckJavaObjectEnabled(objObject) Then
			   
						objObject.SetFocus					
					If Err.Number = 0 Then
						Q_SetFocus = 0
						WriteStatusToLogs "Set Focus operation performed successfully."
										
					Else
						Q_SetFocus = 1
						WriteStatusToLogs "Set Focus operation could not performed successfully, please verify."
					End If
				Else
					Q_SetFocus = 1
					WriteStatusToLogs "The Object was disabled and click could not be performed, please verify."
				End If
			  Else
                    Q_SetFocus = 1
				   WriteStatusToLogs "The Object does not exist, please verify."
			  End If
		
	On Error GoTo 0
End Function

'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :			  Q_WaitForObject()	
'	Purpose :				  Checks whether the specified object property achieves the specified value within the specified timeout
'	Return Value :		 	  Integer(0/1/2)
'	Arguments :
'		Input Arguments :  	  objObject,strPropertyName,strPropertyValue,intTimeOut
'		Output Arguments : 
'	Function is called by :
'	Function calls :		    CheckObjectExist(),CheckJavaObjectEnabled()
'	Created on :			    22/06/2016
'	Author :					Qualitia 
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function Q_WaitForObject(ByVal objObject,ByVal strPropertyName,ByVal strPropertyValue,ByVal intTimeOut)
	On Error Resume Next

		'Variable Section Begin
	
			Dim strMethod 
			Dim strValue
		'Variable Section End
		strMethod = strKeywordForReporting
		If CheckObjectExist(objObject) Then 
			If CheckJavaObjectEnabled(objObject) Then
				If  Not IsNull(strPropertyName) or Not IsEmpty(strPropertyName) And Not IsNull(strPropertyValue) or  Not  IsEmpty(strPropertyValue)  Then
			     
							objObject.CheckProperty strPropertyName,strPropertyValue,intTmieout
												
							If Err.Number = 0 Then
								Q_WaitForObject = 0
								WriteStatusToLogs "The Property '"& strPropertyName &"' is the achieves value: '"&strPropertyValue&"'"										
							Else
								Q_WaitForObject = 1
								WriteStatusToLogs "The Property '"& strPropertyName &"' is not able the achieves value: '"&strPropertyValue&"', please verify."										
							End If
				Else
					Q_WaitForObject=1
					WriteStatusToLogs "The specified Object '"&strPropertyName&"' And '"&strPropertyValue&"' is Empty, please verify."
				End If
			Else
				Q_WaitForObject = 1
				WriteStatusToLogs "The Object was disabled and click could not be performed, please verify."
			End If
	    Else
            Q_WaitForObject = 1
		    WriteStatusToLogs "The Object does not exist, please verify."
		End If
			
		
	On Error GoTo 0
End Function

'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :			  Q_Maximize()	
'	Purpose :				  Q_Maximizes a window to fill the entire screen.
'	Return Value :		 	  Integer(0/1/2)
'	Arguments :
'		Input Arguments :  	  objObject
'		Output Arguments : 
'	Function is called by :
'	Function calls :		    CheckObjectExist(),CheckJavaObjectEnabled()
'	Created on :			    22/06/2016
'	Author :					Qualitia 
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function Q_Maximize(ByVal objObject)
	On Error Resume Next

		'Variable Section Begin
	
			Dim strMethod 
			Dim strValue
		'Variable Section End
		strMethod = strKeywordForReporting
			If CheckObjectExist(objObject) Then 
			   If CheckJavaObjectEnabled(objObject) Then
			     
							objObject.Maximize
						
					If Err.Number = 0 Then
						Q_Maximize = 0
						WriteStatusToLogs "Window Maximize successfully."										
					Else
						Q_Maximize = 1
						WriteStatusToLogs "Window not Maximize successfully, please verify."
					End If
				Else
					Q_Maximize = 1
					WriteStatusToLogs "The Object was disabled and click could not be performed, please verify."
				End If
			  Else
                    Q_Maximize = 1
				   WriteStatusToLogs "The Object does not exist, please verify."
			  End If
			
		
	On Error GoTo 0
End Function

'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :			  Q_Move()	
'	Purpose :				  Q_Moves a window to the specified absolute location on the screen.
'	Return Value :		 	  Integer(0/1/2)
'	Arguments :
'		Input Arguments :  	  objObject,intX-Coord,intY-Coord
'		Output Arguments : 
'	Function is called by :
'	Function calls :		    CheckObjectExist(),CheckJavaObjectEnabled()
'	Created on :			    22/06/2016
'	Author :					Qualitia 
'
'--------------------------------------------------------------------------------------------------------------------'----------------------
Function Q_Move(ByVal objObject,ByVal intXCoord,ByVal intYCoord)
	On Error Resume Next

		'Variable Section Begin
	
			Dim strMethod 
			Dim strValue
		'Variable Section End
		strMethod = strKeywordForReporting
		
		If Not (isnumeric(intXCoord) or IsNumeric(intYCoord)) Then
			Q_Move = 1
			WriteStatusToLogs "Parameters are not positive number."
			exit function
		End If
			If CheckObjectExist(objObject) Then 
			   If CheckJavaObjectEnabled(objObject) Then
			     If Not Isempty(intXCoord) or Not isnull(intXCoord) And Not Isempty(intYCoord) or Not isnull(intYCoord) Then
				 
							objObject.Move intXCoord,intYCoord
							
					If Err.Number = 0 Then
						Q_Move = 0
						WriteStatusToLogs "Window Moved to the specified absolute location on the screen successfully."										
					Else
						Q_Move = 1
						WriteStatusToLogs "Error occurred, " & err.description
					End If
					
				Else
						Q_Move = 1
						WriteStatusToLogs "The ' "& intXCoord&"' and '"&intYCoord&"' is null or Invalid , please verify."
				End If
				Else
					Q_Move = 1
					WriteStatusToLogs "The object was disabled and click could not be performed, please verify."
				End If
			  Else
                    Q_Move = 1
				   WriteStatusToLogs "The object does not exist, please verify."
			  End If
			
		
	On Error GoTo 0
End Function
'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :			  Q_Minimize()	
'	Purpose :				  Q_Minimizes a window to an icon.
'	Return Value :		 	  Integer(0/1/2)
'	Arguments :
'		Input Arguments :  	  objObject
'		Output Arguments : 
'	Function is called by :
'	Function calls :		    CheckObjectExist(),CheckJavaObjectEnabled()
'	Created on :			    22/06/2016
'	Author :					Qualitia 
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function Q_Minimize(ByVal objObject)
	On Error Resume Next

		'Variable Section Begin
	
			Dim strMethod 
			Dim strValue
		'Variable Section End
		strMethod = strKeywordForReporting
			If CheckObjectExist(objObject) Then 
			   If CheckJavaObjectEnabled(objObject) Then
			     
							objObject.Minimize
						
					If Err.Number = 0 Then
						Q_Minimize = 0
						WriteStatusToLogs "Window Minimize successfully."										
					Else
						Q_Minimize = 1
						WriteStatusToLogs "Window not Minimize successfully, please verify."
					End If
				Else
					Q_Minimize = 1
					WriteStatusToLogs "The object was disabled and click could not be performed, please verify."
				End If
			  Else
                    Q_Minimize = 1
				   WriteStatusToLogs "The object does not exist, please verify."
			  End If
			
		
	On Error GoTo 0
End Function

'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :			  Q_Resize()	
'	Purpose :				  Q_Resizes a window to the specified dimensions.
'	Return Value :		 	  Integer(0/1/2)
'	Arguments :
'		Input Arguments :  	  objObject,intHeightPixel,intWidthPixel
'		Output Arguments : 
'	Function is called by :
'	Function calls :		    CheckObjectExist(),CheckJavaObjectEnabled()
'	Created on :			    22/06/2016
'	Author :					Qualitia 
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function Q_Resize(ByVal objObject,ByVal intHeightPixel,ByVal intWidthPixel)
	On Error Resume Next

		'Variable Section Begin
	
			Dim strMethod 
			Dim strValue
		'Variable Section End
		If not (isnumeric(intHeightPixel) or isnumeric(intWidthPixel)) Then
			Q_Resize = 1
			WriteStatusToLogs "Not able to Resizes a window to the specified dimensions., Please verify."
			exit function
		End If
		
		strMethod = strKeywordForReporting
			If CheckObjectExist(objObject) Then 
				If CheckJavaObjectEnabled(objObject) Then
					If Not Isempty(intHeightPixel) or Not isnull(intHeightPixel) And Not Isempty(intWidthPixel) or isnull(intWidthPixel)  Then
				 
							objObject.Resize intHeightPixel,intWidthPixel
						
						If Err.Number = 0 Then
						Q_Resize = 0
						WriteStatusToLogs "Resizes a window to the specified dimensions successfully"										
						Else
						Q_Resize = 1
						WriteStatusToLogs "Not able to Resizes a window to the specified dimensions., Please verify."
						End If
					Else
					Q_Resize = 1
					WriteStatusToLogs "The ' "& intHeightPixel&"' and '"&intWidthPixel&"' is null or Invalid , please verify."
					End If
				Else
					Q_Resize = 1
					WriteStatusToLogs "The object was disabled and click could not be performed, please verify."
				End If
			  Else
                    Q_Resize = 1
				   WriteStatusToLogs "The object does not exist, please verify."
			  End If
			
		
	On Error GoTo 0
End Function

'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :			  Q_Restore()	
'	Purpose :				  Q_Restores a window to its previous size.
'	Return Value :		 	  Integer(0/1/2)
'	Arguments :
'		Input Arguments :  	  objObject
'		Output Arguments : 
'	Function is called by :
'	Function calls :		    CheckObjectExist(),CheckJavaObjectEnabled()
'	Created on :			    22/06/2016
'	Author :					Qualitia 
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function Q_Restore(ByVal objObject)
	On Error Resume Next

		'Variable Section Begin
	
			Dim strMethod 
			Dim strValue
		'Variable Section End
		strMethod = strKeywordForReporting
			If CheckObjectExist(objObject) Then 
			   If CheckJavaObjectEnabled(objObject) Then
			     
							objObject.Restore
						
					If Err.Number = 0 Then
						Q_Restore = 0
						WriteStatusToLogs "Restores a window to its previous size successfully."										
					Else
						Q_Restore = 1
						WriteStatusToLogs "Not able to restores a window to its previous size, please verify."
					End If
				Else
					Q_Restore = 1
					WriteStatusToLogs "The object was disabled and click could not be performed, please verify."
				End If
			 Else
                    Q_Restore = 1
				   WriteStatusToLogs "The object does not exist, please verify."
			  End If
			
		
	On Error GoTo 0
End Function

'----------------------------------------------------------------------------------------------------------------

' 	Function Name :				Q_VerifyExistence()
' 	Purpose :					 Checks the existence of Specified object in AUT.
' 	Return Value :		 		  Boolean( 0/1)
' 	Arguments :
'		Input Arguments :  	   objObject,strExistence
'		Output Arguments : 
'	Function calls :
'	Created on :				 28/07/2016
'	Author :					   Qualitia
'
'------------------------------------------------------------------------------------------------------------------------------------------
'This function no longer required, hence commenting it for now just to observe the impact of removing it. 14-4-2017
Function Q_VerifyExistence_TO_BE_DELETED(ByVal objObject,ByVal blnExistence)
	On Error Resume Next
	'Variable Section Begin
	Dim objexistence, blnParam
	'Variable Section End
	strMethod = strKeywordForReporting
	blnParam = cbool(blnExistence)
	If IsEmpty(blnParam) or IsNumeric (blnExistence) or blnExistence = "" Then
		Q_VerifyExistence = 1 
		WriteStatusToLogs  "Invalid parameter, please verify."
	Exit Function	
	End If
	
	objexistence = objObject.Exist
	objexistence = blnExistence	
	
	If Err.number = 0 Then
		If objexistence Then
			
			Q_VerifyExistence  = 0

			if cbool(blnExistence) = true then
				WriteStatusToLogs	"The specified object exist."
			else 
				WriteStatusToLogs	"The specified object does not exist."
			End if
		Else
			Q_VerifyExistence = 1
			if cbool(blnExistence) = true then
				WriteStatusToLogs	"The specified object does not exist."
			else 
				WriteStatusToLogs	"The specified object exist."
			End if
		End If
		
	Else
		Q_VerifyExistence = 1
		WriteStatusToLogs "An Error occurred during the method 'Q_VerifyExistence'"
	End If	
	On Error Goto 0
End Function


'----------------------------------------------------------------------------------------------------------------
'	Function Name :				  Q_VerifyEnability()
'	Purpose :					  Verifies whether the specified object is enabled or not.
'	Return Value :		 		   Integer( 0/1)
'	Arguments :
'		Input Arguments :  		objObject,objObject 
'		Output Arguments : 	   intActValue
'	Function is called by :
'	Function calls :				CheckObjectExist(),CheckJavaObjectEnabled()
'	Created on :				   01/08/2016
'	Author :					   Qualitia 
'----------------------------------------------------------------------------------------------------------------
Function Q_VerifyEnability(ByVal objObject,ByVal blnEnability)
	On Error Resume Next
		'Variable Section Begin
		
		Dim strMethod 
		Dim  intActValue, blnstatus, blnParam, blnActValue

		'Variable Section End
		strMethod = strKeywordForReporting
		blnstatus = False
		
		err.clear
		blnParam = cbool(blnEnability)
		'If not (isNull(blnEnability) and IsEmpty(blnEnability) and trim(blnEnability)= "") and IsEmpty(blnParam) and not IsNumeric (blnParam) Then
		If IsEmpty(blnParam) or IsNumeric (blnEnability) or blnEnability = ""  Then
			Q_VerifyEnability = 1 
			WriteStatusToLogs  "Invalid parameter, please verify."
		Exit Function	
		End If	
		
        If CheckObjectExist(objObject) Then
        
				intActValue = objObject.GetROProperty("enabled")	
				
				if intActValue = 0 then 
					blnActValue = False
				elseif intActValue = 1 then
					blnActValue = True
				end if
							
					If Vartype(intActValue) <> vbEmpty  Then
						blnstatus = blnEnability
					End If
				 
					If blnstatus and blnActValue Then			 		
						Q_VerifyEnability = 0
						if blnstatus then
						WriteStatusToLogs	"The specified Object is enable."
						else 
						WriteStatusToLogs	"The specified Object is not enable."
						End if
					Else
					
						Q_VerifyEnability = 1
						if blnstatus then
						 WriteStatusToLogs  "The specified Object is not enable."
						else
						 WriteStatusToLogs  "The specified Object is enable."
						End if
			 End If
						
		Else
			Q_VerifyEnability = 1
			WriteStatusToLogs	"The object does not exist, please verify"
		End If
					
	 On Error GoTo 0
End Function
'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name			:	Q_VerifyVisibility()
'	Purpose					:	Verifies whether the objRadioButton is visible.
'	Return Value			:	Integer( 0/1/2)
'	Arguments				:
'		Input Arguments		:  	objObject 
'		Output Arguments	: 
'	Function is called by	:
'	Function calls			:	CheckObjectExist() , CheckObjectVisiblity()
'	Created on				:	06/07/2016
'	Author					:	Qualitia
'----------------------------------------------------------------------------------------------------------------------------------------------
Function Q_VerifyVisibility(ByVal objObject,ByVal blnEnability)
	On Error Resume Next
		'Variable Section Begin
		
		Dim strMethod 
		Dim  intActValue, blnstatus, blnParam, blnActValue

		'Variable Section End
		strMethod = strKeywordForReporting
		blnstatus = False
		
		err.clear
		blnParam = cbool(blnEnability)
		If IsEmpty(blnParam) or IsNumeric (blnEnability) or blnEnability = ""  Then 
			Q_VerifyVisibility = 1 
			WriteStatusToLogs  "Invalid parameter, please verify."
		Exit Function	
		End If	
		
        If CheckObjectExist(objObject) Then
        
				intActValue = objObject.GetROProperty("displayed")	
				
				if intActValue = 0 then 
					blnActValue = False
				elseif intActValue = 1 then
					blnActValue = True
				end if
							
					If Vartype(intActValue) <> vbEmpty  Then
						blnstatus = blnEnability
					End If
				 
					If blnstatus and blnActValue Then			 		
						Q_VerifyVisibility = 0
						if blnstatus then
						WriteStatusToLogs	"The specified Object is visible."
						else 
						WriteStatusToLogs	"The specified Object is not visible."
						End if
					Else
					
						Q_VerifyVisibility = 1
						if blnstatus then
						 WriteStatusToLogs  "The specified Object is not visible"
						else
						 WriteStatusToLogs  "The specified Object is visible"
						End if
			 End If
						
		Else
			Q_VerifyVisibility = 1
			WriteStatusToLogs	"The object does not exist, please verify"
		End If
					
	 On Error GoTo 0
End Function

Function Q_VerifyVisibility_old(ByVal objObject,ByVal blnVisibility )
	On Error Resume Next
		'Variable Section Begin

			Dim blnActData, blnstatus
			blnstatus = false

		'Variable Section End  '//visibility validation add by Nitin
		If isNull(blnVisibility) or IsEmpty(blnVisibility) or trim(blnVisibility)= " "Then
			Q_VerifyVisibility = 1 
			WriteStatusToLogs  "The parameter is either null or empty, please verify"
		Exit Function	
		End If
		
		 If CheckObjectExist(objObject) Then
			 blnActData=objObject.GetRoProperty("displayed")
				
			 'blnActData=objObject.GetRoProperty("enabled")
				If Vartype(blnActData) <> vbEmpty  And Err.Number = 0 Then				
					blnstatus = blnVisibility					
				End If
			 If blnstatus Then			 		
						Q_VerifyVisibility = 0
						WriteStatusToLogs	"The specified Object is visible"
					Else
					
						Q_VerifyVisibility = 0 
						 WriteStatusToLogs  "The specified Object is not visible"
			 End If
		Else
			Q_VerifyVisibility = 1
			WriteStatusToLogs	"The object does not exist, please verify"
		End If	 
			
On Error GoTo 0			
End Function
'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name			:	Q_StoreCurrentDateInFormat()
'	Purpose					:	Store current date in expected format.
'	Return Value			:	Integer( 0/1/2)
'	Arguments				:
'		Input Arguments		:  	
'		Output Arguments	: 
'	Function is called by	:
'	Function calls			:	
'	Created on				:	28/08/2016
'	Author					:	Qualitia
'----------------------------------------------------------------------------------------------------------------------------------------------
'TO DO: add the code for Q_StoreCurrentDateInFormat
'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name			:	Q_ComputeExpression()
'	Purpose					:	Evalute expected expression and return output  .
'	Return Value			:	Integer( 0/1/2)
'	Arguments				:
'		Input Arguments		:  	
'		Output Arguments	: 
'	Function is called by	:
'	Function calls			:	
'	Created on				:	28/08/2016
'	Author					:	Qualitia
'----------------------------------------------------------------------------------------------------------------------------------------------
Function Q_ComputeExpression(Byval strKey,Byval strExpression)
	On error resume next
	
	 'variable section begin
	 Dim strMethod
	 Dim strResult,ActExpression,errordescription,errorNumber
	 'variable section ends
	 
	 If Not IsNull(strExpression) or Not IsEmpty(strExpression) And trim(strExpression)="" Then
			
	 		If Not IsNull(strKey) or Not IsEmpty(strKey) or  trim(strKey) = "" Then
	 		
	 				strResult = Eval(strExpression)
													
						If Err.Description ="Invalid number" or Err.Description ="Syntax error" or Err.Description="Invalid character" Then
						
							Q_ComputeExpression = 1
	 						WriteStatusToLogs "The Expression is not valid , please Verify"
						Else
							AddItemToStorageDictionary strKey,strResult
	 						Q_ComputeExpression = 0
	 						WriteStatusToLogs "The Expression '"&strExpression&"' is evaluated and result '"&strResult&"' stored in key '"&strKey&"'Successfully"
						End If
			Else
				 Q_ComputeExpression = 1
	 			WriteStatusToLogs "The Key passed is not valid. Please verify."
	 		End If
	 	Else	 		
			Q_ComputeExpression = 1
	 		WriteStatusToLogs "The Expression passed is not valid. Please verify."
	 End If
	On Error GoTo 0
End Function
'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name			:	Q_CalculateDifferenceInDateAndStore()
'	Purpose					:	Store the differences between two dates based on it intervals
'	Return Value			:	Integer( 0/1/2)
'	Arguments				:
'		Input Arguments		:  	
'		Output Arguments	: 
'	Function is called by	:
'	Function calls			:	
'	Created on				:	28/08/2016
'	Author					:	Qualitia
'----------------------------------------------------------------------------------------------------------------------------------------------
Function Q_CalculateDifferenceInDateAndStore(Byval strKey,Byval interval,Byval fromDate,Byval toDate)
	
	On Error Resume Next
	'variable section begin
	 Dim strMethod,StoreActDifference
	 Dim intervalby,errorNumber,errordescription
	 'variable section End
	 
		intervalby=LCase(interval)
		
	 If Not isEmpty(strKey) or Not IsNull(strKey) or trim(strKey) ="" Then
	 
	 	If Not isEmpty(intervalby) or Not IsNull(intervalby) or trim(intervalby) ="" Then
		
	 		If Not isEmpty(fromDate) or Not IsNull(fromDate) or trim(fromDate) ="" Then
			
	 			If Not isEmpty(toDate) or Not IsNull(toDate) or trim(toDate) ="" Then
				
	 		
	 					StoreActDifference=DateDiff(intervalby,fromDate,toDate)
						errordescription=err.description
						errornumber=err.number						
						AddItemToStorageDictionary strKey, StoreActDifference
			 				
	 					If (StoreActDifference < 0) Then	
							
	 						Q_CalculateDifferenceInDateAndStore=1
	 						WriteStatusToLogs "The parameter passed as date are not valid, please verify"
											  
						ElseIf(errornumber = 0 and StoreActDifference >= 0 and intervalby="q")then
							
							Q_CalculateDifferenceInDateAndStore=0
	 						WriteStatusToLogs "Quarter difference is '"&StoreActDifference&"' which is stored in Key '"&strKey&"' successfully."
											  
						ElseIf(errornumber = 0 and StoreActDifference >= 0 and intervalby="yyyy")then
						
							Q_CalculateDifferenceInDateAndStore=0
	 						WriteStatusToLogs "Year difference is '"&StoreActDifference&"' which is stored in Key '"&strKey&"' successfully."
											  
						ElseIf(errornumber = 0 and StoreActDifference >= 0 and intervalby="m")then
						
							Q_CalculateDifferenceInDateAndStore=0
	 						WriteStatusToLogs "Month difference is '"&StoreActDifference&"' which is stored in Key '"&strKey&"' successfully."
											  
	 					ElseIf(errornumber = 0 and StoreActDifference >= 0 and intervalby="y")then
						
							Q_CalculateDifferenceInDateAndStore=0
	 						WriteStatusToLogs "Day of year difference is '"&StoreActDifference&"' which is stored in Key '"&strKey&"' successfully."
											  
						ElseIf(errornumber = 0 and StoreActDifference >= 0 and intervalby="d")then
						
							Q_CalculateDifferenceInDateAndStore=0
	 						WriteStatusToLogs "Day differecnce is '"&StoreActDifference&"' which is stored in Key '"&strKey&"' successfully."
											  
						ElseIf(errornumber = 0 and StoreActDifference >= 0 and intervalby="w")then
						
							Q_CalculateDifferenceInDateAndStore=0
	 						WriteStatusToLogs "Weekday Differecnce is '"&StoreActDifference&"' which is stored in Key '"&strKey&"' successfully."
											  
						ElseIf(errornumber = 0 and StoreActDifference >= 0 and intervalby="ww")then
						
							Q_CalculateDifferenceInDateAndStore=0
	 						WriteStatusToLogs "Week of year Differecnce is '"&StoreActDifference&"' which is stored in Key '"&strKey&"' successfully."
											  
						ElseIf(errornumber = 0 and StoreActDifference >= 0 and intervalby="h")then
						
							Q_CalculateDifferenceInDateAndStore=0
	 						WriteStatusToLogs "Hour Differecnce is '"&StoreActDifference&"' which is stored in Key '"&strKey&"' successfully."
										
 						ElseIf(errornumber = 0 and StoreActDifference >= 0 and intervalby="n")then
						
							Q_CalculateDifferenceInDateAndStore=0
	 						WriteStatusToLogs "Minute Differecnce is '"&StoreActDifference&"' which is stored in Key '"&strKey&"' successfully."
											  
						ElseIf(errornumber = 0 and StoreActDifference >= 0 and intervalby="s") then
						
							Q_CalculateDifferenceInDateAndStore=0
	 						WriteStatusToLogs "Second Differecnce is '"&StoreActDifference&"' which is stored in Key '"&strKey&"' successfully."
											  
						ElseIf(errornumber = 5 and errordescription="Invalid procedure call or argument") then
						
							Q_CalculateDifferenceInDateAndStore=1
	 						WriteStatusToLogs "The Interval passed is invalid , please verify."
						End If					  
						
	 				Else
						Q_CalculateDifferenceInDateAndStore=1
	 					WriteStatusToLogs "The pass  todate '"&toDate&"' is not valid, please Verify"	 			
						 		
		 			End If		
		 		Else
					Q_CalculateDifferenceInDateAndStore=1
	 				WriteStatusToLogs "The pass fromdate'"&FromDate&"' is not valid, please Verify"		 		
		 		End If
		 	Else
				Q_CalculateDifferenceInDateAndStore=1
	 			WriteStatusToLogs "The pass interval '"&interval&"' is not valid, please Verify"		 				 	 	 			
		 	End If
		Else
			Q_CalculateDifferenceInDateAndStore=1
	 		WriteStatusToLogs "The pass key'"&strKey&"' is not valid, please Verify"		 		
		End If
	On Error GoTo 0	 		
End Function

'Added
Function IsObjectState(oObject, blnState, key)

	On error resume next
	Dim strMethod,  propertyName,  propValue
	Dim  pass, fail, intval, status
	
	Const PreText = "Is"
	
	If ucase(blnState)  = "TRUE" Then
		pass = 0
		fail = 1
		status = "Passed"
	Else
		pass = 1
		fail = 0
		status = "Failed"
	End If
	
	strMethod = strKeywordForReporting
	propertyName = mid(strMethod, InStr(1, strMethod, PreText,1)+2, len(strMethod)-len(PreText))
	err.clear
	propValue = oObject.getroproperty(propertyName)
	
	If err.number=0 Then
		intval = AddItemToStorageDictionary(key,propValue)
		
		If (propValue = 1 or propValue = true) and intval = 0 Then
			
			IsObjectState = pass
			WriteStatusToLogs "Object is " & propertyName & ". '"&propValue & "' is stored in the key: " & key
		Else
			IsObjectState = fail
			WriteStatusToLogs "Object is not " & propertyName 	& ". '"&propValue & "' is stored in the key: " & key	
		End If

	Else 
			IsObjectState = 1
			WriteStatusToLogs "Object does not support '" & propertyName & "'. Exception message:" & err.description
	End If
	On error goto 0
End Function

Function ClickWaitAndType(oObj, intWaitForSecond, Text)
	On error resume next
	Dim strMethod, synctime
	strMethod = strKeywordForReporting
	
	err.clear
	synctime = cint(intWaitForSecond)
	
	If err.number <> 0 Then
		ClickWaitAndType =1
		WriteStatusToLogs "WaitForSecond is not numeric." & Err.description
		Exit Function
	End If
	err.clear
	If CheckObjectExist(oObj)  Then
		oObj.Object.SetFocus
		oObj.Click
		wait synctime
		oObj.Type Text
		If err.number = 0 Then
			ClickWaitAndType = 0
			WriteStatusToLogs "Edit box is clicked, waited for " & synctime & " seconds and entered the value "&Text
		Else
			ClickWaitAndType =1
			WriteStatusToLogs "Operation failed." & err.description
		End If
	End If
	On error goto 0
End Function


Function SelectTab(ByRef test_object, ByVal strCaption)
	On Error Resume Next
	
	'Declare Variables
	Dim TabCount, i, TabFound
	Dim currentTab
	
	'Initialize Variables
	TabCount = test_object.object.TabsPerRow
	TabFound = false
	
	'ActivateParentObject(test_object)
	
	For i = 0 To TabCount Step 1
		currentTab=UCase(Replace(Trim(test_object.object.TabCaption(i)),"&",""))
		'If inStr(test_object.object.Tabs(i-1).caption, strCaption) > 0 Then
		If currentTab=UCase(strCaption) Then
			test_object.object.Tab=i
			TabFound = true
			Exit For
		End If		
	Next
	
	If TabFound = false Then
			SelectTab = 1
			WriteStatusToLogs  "Tab is not found."
			Exit function
	else
		SelectTab = 0
		WriteStatusToLogs  "Tab '"& strCaption & "' is selected."
	End If
	
	'err.clear
	'test_object.object.Tabs(i-1).selected = true
	'If err.number = 0 Then
	'		SelectTab = 0
	'		WriteStatusToLogs  "Method Name:" &"CheckObjectExistEngine"&vbTab&_
	'		"Message:" & "Tab '"& strCaption & "' is selected."
	'Else
	'		SelectTab = 1
	'		WriteStatusToLogs  "Method Name:" &"CheckObjectExistEngine"&vbTab&_
	'		"Message:" & "Tab '"& strCaption & "' could not be selected."
	'End If
	on error goto 0
End Function

Function ClearText(ByVal objEditbox)
	On Error Resume Next
		'Variable Section Begin
			
		Dim strMethod
		Dim currentVal

		'Variable Section End
		strMethod = strKeywordForReporting
			   If CheckObjectExist(objEditbox) Then
							 objEditbox.Click
							 currentVal = objEditbox.GetROProperty("text")
							 objEditbox.SetSelection 0,  len(currentVal)

'							 MsgBox "click in edit"
							 objEditbox.Set ""
							 If err.Number = 0 Then
									ClearText = 0
									WriteStatusToLogs "EditBox is cleared successfully."
							 Else
									ClearText = 1
									WriteStatusToLogs "EditBox could not be cleared."
							  
							 End If
				Else
						ClearText = 1
						WriteStatusToLogs "The EditBox does not exist, please verify."
				End If
	On Error GoTo 0
End Function


Function Q_VerifyExistence(ByVal objObject,ByVal blnExistence)
	On Error Resume Next
	'Variable Section Begin
	Dim objexistence, blnParam, pass,defect, status
	'Variable Section End
	strMethod = strKeywordForReporting
	
	pass = 0
	defect = 2

	blnParam = cbool(blnExistence)
	If IsEmpty(blnParam) or IsNumeric (blnExistence) or blnExistence = "" Then
		Q_VerifyExistence = 1 
		WriteStatusToLogs  "Invalid parameter, please verify"
	Exit Function	
	End If
	
	objexistence = objObject.Exist
	
	If objexistence Then
		If cbool (blnExistence) Then
			status = "Passed"
			Else 
			status = "Defect"
		End If
	Else 
		If cbool (blnExistence) Then
			status = "Defect"
			Else 
			status = "Passed"
		End If
	End If
	
	If Err.number = 0 Then
		If UCase(blnExistence)  = "TRUE"Then
			If objexistence Then
				Q_VerifyExistence = pass
				WriteStatusToLogs	"The specified object exist."
			Else
				Q_VerifyExistence = defect
				WriteStatusToLogs	"The specified object does not exist."
			End If
		Else
			If objexistence Then
				Q_VerifyExistence = defect
				WriteStatusToLogs	"The specified object exist."
			Else
				Q_VerifyExistence = pass
					WriteStatusToLogs	"The specified object does not exist."
			End If
			
		End If
	End If	
	

	On Error Goto 0
End Function

Function Q_StoreExistence(ByVal objObject,ByVal SyncTime, ByVal strKey)
	On Error Resume Next
	
	'Variable Section Begin
	 Dim objexistence, intews, strMethod
	'Variable Section End
	strMethod = strKeywordForReporting
	
	If isNull(strKey) or IsEmpty(strKey) or trim(strKey)= ""Then
		Q_StoreExistence = 1 
		WriteStatusToLogs  "The parameter is either null or empty, please verify."
	Exit Function	
	End If	
	objexistence = objObject.Exist(SyncTime)
	intews = AddItemToStorageDictionary (strKey,objexistence)
	
	 If Err.number = 0 Then
				Q_StoreExistence  = 0
				WriteStatusToLogs "The existence of specified object is '"&objexistence&"' and store in Key '"&strKey&"'"
	 Else
		Q_StoreExistence = 1
		WriteStatusToLogs "Error occurred, please verify parameters."
	End If	
	On Error Goto 0
End Function

 Function WriteTextToFile(byval strFile, byval strText, byval mode)
	On error resume next
	Dim objFSO, objFile, objTextFile, ForAppending, ForWriting, strMethod, filemode

	strMethod = strKeywordForReporting
	 If ucase(mode) = "OVERWRITE" Then
	 	filemode = 2
	 ElseIf ucase(mode) = "APPEND" or mode = "" Then
	 	filemode = 8
	 Else
		WriteTextToFile = 1
		WriteStatusToLogs  "Bad file mode. Please choose file mode as OVERWRITE or APPEND. "
		Exit Function
	 End If
	' Create the File System Object
	Set objFSO = CreateObject("Scripting.FileSystemObject")
	err.number = 0
	If not objFSO.FileExists(strFile) Then
	   Set objFile = objFSO.CreateTextFile(strFile)
	End If 
	
	If err.number <> 0 Then
		WriteTextToFile = 1
		WriteStatusToLogs err.description
		Exit Function
	End If
	set objFile = nothing
	Set objTextFile = objFSO.OpenTextFile (strFile, filemode, True)
	
	objTextFile.WriteLine(strText)
	objTextFile.Close
	
	If err.number = vbEmpty then
		WriteTextToFile = 0
     	If filemode = 8 Then
     	WriteStatusToLogs  "File is successfully appended."
		Else 
		   WriteStatusToLogs  "File is successfully written."
     	End If
	Else 
		WriteTextToFile = 1
     	WriteStatusToLogs  "Error while writing to file. File is already open or this file format is not supported. Only flat files are supported by this action. " 
	End If
End Function

'----------------------------------------------------------------------
Function WriteLog(status,msg)
	On error resume next
	Dim strMethod
	If ucase(status) = "PASS" then
		WriteLog = 0
		WriteStatusToLogs  msg
	Elseif ucase(status) = "FAIL" then
		WriteLog = 1
		WriteStatusToLogs  msg
	Elseif ucase(status) = "DEFECT" then
		WriteLog = 2
		WriteStatusToLogs  msg
	Else 
		WriteLog = 1
		WriteStatusToLogs  "Invalid parameter." 
	End If
	On Error GoTo 0
End Function

Function StoreArrayItem(Key, ArrayName, Index)
On error resume next
Dim ArrVal, strMethod
  strMethod = strKeywordForReporting
  Err.clear
  
  	If ArrayName="" or Not IsNumeric(Index) Then
	StoreArrayItem =1
		 WriteStatusToLogs "Invalid Parameters"
		  Exit Function
	End If
	
	
  logs.Debug ("Action Execution Start >>> StoreArrayItem.")
  logs.Debug ("Action execution string >>> ArrVal = " & ArrayName & "(" & Index & ")")
  Execute ("ArrVal = " & ArrayName & "(" & Index & ")")
 
  If err.number = 0 Then
  	  intval = AddItemToStorageDictionary (Key,ArrVal)
  	  If intval = 0 Then
		StoreArrayItem =0  	  	
  	  	WriteStatusToLogs "Array value '"&ArrVal &"' is stored in the key: " & Key
		  Else
		StoreArrayItem =1  	  	
  	  	WriteStatusToLogs "Value could not be store in the array."	  
  	  End If

  	Else
	
  		StoreArrayItem =1
  		  WriteStatusToLogs "Varify parameters. " & err.description
  End If
 logs.Debug ("Action Execution Finished >>> StoreArrayItem.")
On error goto 0
End Function


Function Q_WaitForObject(ByVal objObject,ByVal strPropertyName,ByVal strPropertyValue,ByVal intTimeOut)
	On Error Resume Next
	
		'Variable Section Begin
			Dim strMethod 
			Dim strValue, returnValue
		'Variable Section End
		strMethod = strKeywordForReporting
		
		If  IsNull(strPropertyName) or IsEmpty(strPropertyName) And IsNull(strPropertyValue) or  IsEmpty(strPropertyValue)  Then
					Q_WaitForObject=1
					WriteStatusToLogs "Invalid parameters."
					Exit function
		End If 
		intTimeOut = clng(intTimeOut)
		If CheckObjectExist(objObject) Then 
				returnValue = objObject.WaitProperty (strPropertyName,strPropertyValue,intTimeOut)
												
							If Err.Number = 0 Then
								If returnValue Then
									Q_WaitForObject = 0
									WriteStatusToLogs "PropertyName '"& strPropertyName &"' has achieved the value '"&strPropertyValue&"'"		
								Elseif not returnValue Then 
									Q_WaitForObject = 1
									WriteStatusToLogs "Property could not achieve the specified PropertyValue in specified timeout period."	
								End If
								
							Else
								Q_WaitForObject = 1
								WriteStatusToLogs "Error occurred: "& err.description
							End If
				Else

            Q_WaitForObject = 1
		    WriteStatusToLogs "The Object does not exist, please verify."
		End If
			
		
	On Error GoTo 0
End Function

Function WaitForObjectExist(obj, timeout)
	On error resume next
	Dim strMethod, objExist,i
  	strMethod = strKeywordForReporting
  	
	err.clear
	timeout = cint(timeout)
	If err.number <> 0 Then
			 WaitForObjectExist =1
	  		  WriteStatusToLogs "Invalid parameter."
			  Exit Function
	End If
	Do
		wait(1)
		objExist = obj.Exist
		i = i+1
	Loop While objExist=False and i<=timeout
	objExist = obj.Exist
	If objExist = true Then
		  		WaitForObjectExist =0
	  		  WriteStatusToLogs "Object exists."
	ElseIf objExist = false Then
			  WaitForObjectExist =1
	  		  WriteStatusToLogs "Object does not appear in specified timeout period."
	End If
	On error goto 0
End Function
'=======================Code Section End=============================================
