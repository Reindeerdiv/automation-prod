'Function CheckJavaObjectEnabled(ByVal objObject)
'Function GetJTableRowData(byval objJTable , byval intRowNum)
'Function GetJTableColCount(byval objJavaTable , byval strKey)
'Function GetJTableRowCount(byval objJTable , byval strKey)
'Function GetJColumnNumber(ByVal objJTable, ByVal strKey, ByVal strColName)
'Function GetInstanceOfDataInJColumn(ByVal objJTable , ByVal strKey , ByVal intColNo , ByVal strCellData)
'Function CheckJavaObjectVisible(ByVal objObject )
'Function GetItemFromJavaObject(byval objObject, byval ItemName)
'Function CheckJavaObjectInVisible(ByVal objObject )
'Function GetRowNumberForNInstanceofDataInJColumn(ByVal objJTable , ByVal strKey , ByVal intColNo , ByVal strCellData, byval n)
'Function CheckJavaObjectStatus(ByVal objObject)
'Function GetObjectItemPropertyName(byval objObject, byval ItemName, byval PropertyName)
'Function GetValidJTreeStringFromArray(Byval arrJTreeNodes)
'Public function GetInValidJItemInHierarchy(Byval ObjTreeView,  Byval strExpString)





'=======================Code Section Begin=============================================

Option Explicit		'Forces explicit declaration of all variables in a script. 
'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :				 CheckJavaObjectEnabled()
'	Purpose :					 Returns the column data in an array. The column is assumed to exist or it is assumed to be 
'								 prechecked by the calling function. The column number starts from 1.
'	Return Value :		 		 Array / Null
'	Arguments :
'		Input Arguments :  		 objTable , intColNum
'		Output Arguments : 
'	Function is called by :
'	Function calls :			  
'	Created on :				 29/06/2009
'	Author :					 Iohbha K
'
'------------------------------------------------------------------------------------------------------------------------------------------

Function CheckJavaObjectEnabled(ByVal objObject)
	On Error Resume Next
		'Variable Section Begin
		
			Dim strActData, blnstatus
			blnstatus = False
			

		'Variable Section End
              strActData = objObject.GetRoProperty("enabled")
			 
			   	If Vartype(strActData) <> vbEmpty  And Err.Number = 0 Then
						blnstatus =true
					 End If

			  If  blnstatus Then
					If  strActData = "1" Then
							CheckJavaObjectEnabled = True
							WriteStatusToLogs "The specified object is enabled."
					Else 
					
						   CheckJavaObjectEnabled = False
						  WriteStatusToLogs "The specified object is disabled."
				   End If
			Else
			   CheckJavaObjectEnabled = False
			  WriteStatusToLogs "An error occurred while capturing object value."
            End If	  
			
  On Error GoTo 0
		  
End Function



'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :				 GetJTableColData()
'	Purpose :					 Returns the column data in an array. The column is assumed to exist or it is assumed to be 
'								 prechecked by the calling function. The column number starts from 1.
'	Return Value :		 		 Array / Null
'	Arguments :
'		Input Arguments :  		 objTable , intColNum
'		Output Arguments : 
'	Function is called by :
'	Function calls :			  
'	Created on :				 29/06/2009
'	Author :					 Iohbha K
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function GetJTableColData(byval objJTable , byval intColNum)
On error resume next
	Dim intRow , arrColData()
	Dim strTemp , intCnt

	intRow = objJTable.getROProperty("rows")

	If  err.number = 0 and not isnull(intRow) and  not isempty(intRow) and intRow <> "" Then

		Redim Preserve arrColData(cint(intRow-1))
		For intCnt = 0 to intRow-1
			strTemp = objJTable.GetCellData (Cint(intCnt) , Cint(intColNum))
			If err.number = 0 Then
				arrColData(intCnt) = strTemp
			Else
				GetJTableColData = null		
				Exit Function
            End If
		Next
		GetJTableColData = arrColData
	Else
		GetJTableColData = null		
	End If
	
On error goto 0
End Function


'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :				 GetJTableRowData()
'	Purpose :					 Returns the row data in an array. The row is assumed to exist or it is
'								 prechecked by the calling function. The row number starts from 1.
'	Return Value :		 		 Array / Null
'	Arguments :
'		Input Arguments :  		 objTable , intRowNum
'		Output Arguments : 
'	Function is called by :
'	Function calls :			  
'	Created on :				 29/06/2009
'	Author :					 Iohbha K
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function GetJTableRowData(byval objJTable , byval intRowNum)
On error resume next
	Dim intCol , arrRowData()
	Dim strTemp , intCnt

	intCol = objJTable.getROProperty("cols")
	If  err.number = 0 and not isnull(intCol) and  not isempty(intCol) and intCol <> "" Then

		Redim Preserve arrRowData(cint(intCol-1))
		For intCnt = 0 to intCol-1
			strTemp = objJTable.GetCellData (Cint(intRowNum) , Cint(intCnt))
			If err.number = 0 Then
				arrRowData(intCnt) = strTemp
			Else
				GetJTableRowData = null		
				Exit Function
            End If
		Next
		GetJTableRowData = arrRowData
	Else
		GetJTableRowData = null		
	End If
	
On error goto 0
End Function



'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :			  GetJTableColCount()
'	Purpose :				  Stores the column count of the specified row of the table. The first row
'							  of the table is 1. The row passed should be a natural number.	
'	Return Value :		 	  Integer(0/1/2/3)
'							  0 : Success 
'							  1 : Object not Exist 
'							  2 : Could not retrieve column count
'							  3 : Key passed is invalid
'	Arguments :
'		Input Arguments :  	  objJavaTable , strKey , intRow
'		Output Arguments : 
'	Function is called by :	  StoreJColumnCount()
'	Function calls :		  CheckObjectExist() , AddItemToStorageDictionary(), IsKey()
'	Created on :			  12/06/2009
'	Author :				  Iohbha K
'
'------------------------------------------------------------------------------------------------------------------------------------------

Function GetJTableColCount(byval objJavaTable , byval strKey)
On Error Resume Next
	'Variable Section Begin

		Dim intTableColCount 
	   
	'Variable Section End
		
	 
		If IsKey(strKey) = 0 Then
			If CheckObjectExist(objJavaTable)  Then 
			   
					intTableColCount =objJavaTable.GetROProperty("cols")
					If err.Number = 0 And Not Isnull(intTableColCount) And Not IsEmpty(intTableColCount) And intTableColCount<>"" Then
						AddItemToStorageDictionary strKey , intTableColCount
						WriteStatusToLogs "Java table column count '"&intTableColCount&"'Stored successfully."
			           GetJTableColCount = 0
						
					Else
					
						GetJTableColCount = 2
						intTableColCount = null
					End if
				
			Else
				GetJTableColCount = 1
			End if 
		Else
			GetJTableColCount = 3
		End if

	   		
On Error GoTo 0
End Function




'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :			  GetJTableRowCount()
'	Purpose :				  Stores the row count of the specified java table	
'	Return Value :		 	  Integer(0/1/2/3)
'							  0 : Success 
'							  1 : Object not Exist 
'							  2 : Could not retrieve row count
'							  3 : Key passed is invalid 
'	Arguments :
'		Input Arguments :  	  objJTable , strKey
'		Output Arguments : 
'	Function is called by :	  StoreJRowCount()	
'	Function calls :		  CheckObjectExist() , AddItemToStorageDictionary() , IsKey()
'	Created on :			  12/06/2009
'	Author :				  Iohbha K
'
'------------------------------------------------------------------------------------------------------------------------------------------

Function GetJTableRowCount(byval objJTable , byval strKey)
On Error Resume Next
	'Variable Section Begin

	 Dim intTableRowCount 
	   
	'Variable Section End
		
	 If IsKey(strKey) = 0  Then
			 
		If CheckObjectExist(objJTable)  Then 
			intTableRowCount = objJTable.GetROProperty("rows")
			If err.Number = 0 And Not IsEmpty(intTableRowCount) And Not IsNull(intTableRowCount) And intTableRowCount<>"" Then

				AddItemToStorageDictionary strKey , intTableRowCount 			    
				GetJTableRowCount= 0
					WriteStatusToLogs "Row count: "& intTableRowCount
			Else
				GetJTableRowCount= 2
			End if
			
		Else
			GetJTableRowCount= 1
		End if 

	Else
		GetJTableRowCount = 3
	End if
					
On Error GoTo 0
End Function


'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :			  GetJColumnNumber()
'	Purpose :				  Stores the column number of the column header pass
'	Return Value :		 	  Integer(0/1/2/3/4/5)
'							  0 : Success 
'							  1 : Object not Exist 
'							  2 : The column header does not exist
'							  3 : Key passed is invalid 
'							  4 : The column header is either null or empty 
'							  5 : Failed to retrieve table data 
'	Arguments :
'		Input Arguments :  	  objJTable , strKey , strColName
'		Output Arguments : 
'	Function is called by :	  StoreJColumnNumber()	
'	Function calls :		  CheckObjectExist() , AddItemToStorageDictionary() , IsKey()
'	Created on :			  01/07/2009
'	Author :				  Iohbha K
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function GetJColumnNumber(ByVal objJTable, ByVal strKey, ByVal strColName)
On Error Resume Next

	'Variable Section Begin
		Dim intActColCount , intCnt , strActColName
		Dim blnFound , intColnoFound
		blnFound = false
	'Variable Section End

	If IsKey(strKey) = 0 Then
		If Not IsNull(strColName) And Not IsEmpty(strColName) Then
			If CheckObjectExist(objJTable)  Then 

				intActColCount = objJTable.getROProperty("cols")
				If Err.number =0 And Not isempty(intActColCount) And Not IsNull(intActColCount) Then
					
					For intCnt = 0 To intActColCount-1
						strActColName = objJTable.GetColumnName(CInt(intCnt))
						If Err.number = 0 And Not IsEmpty(strActColName) Then
							If Trim(Ucase(strActColName)) = Trim(UCase(strColName)) Then
								blnFound = True
								intColnoFound = intCnt + 1
								Exit For
							End If
						Else
							GetJColumnNumber = 5
							Exit Function
						End if
		
					Next

					If blnFound = True Then 
						AddItemToStorageDictionary strKey , intColnoFound 
						GetJColumnNumber= 0
						WriteStatusToLogs "Java table's Column Number '"& intColnoFound &"' stored successfully."
					Else
						GetJColumnNumber= 2
					End if

				Else
					GetJColumnNumber = 5	
				End if

			Else
				GetJColumnNumber = 1
			End If
			
		Else
			GetJColumnNumber = 4
		End if

	Else
		GetJColumnNumber = 3
	End if

On Error goto 0
End Function


'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :			  GetInstanceOfDataInJColumn()
'	Purpose :				  Store the number of occurence of the strCellData in the particular column.
'							  The column number starts from 1. The comparison is casesensitive.
'							  Occurence 0 indicates that the data does not exist in the column , and the data will be pushed in the dictionary
'	Return Value :		 	  Integer(0/1/2/3/4/5/6/7)
'							  0 : Success 
'							  1 : Object not Exist 
'							  2 : An error occurred
'							  3 : Key passed is invalid 
'							  4 : The cell data is either null or empty 
'							  5 : The column number passed is not a whole number 
'							  6 : Failed to retrieve data from the specified java table
'							  7 : The column number does not exist
'	Arguments :
'		Input Arguments :  	  objJTable , strKey , intColNo , strCellData
'		Output Arguments : 
'	Function is called by :	  StoreInstanceOfDataFromJColumn()	
'	Function calls :		  CheckObjectExist() , AddItemToStorageDictionary() , IsKey()
'	Created on :			  01/07/2009
'	Author :				  Iohbha K
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function GetInstanceOfDataInJColumn(ByVal objJTable , ByVal strKey , ByVal intColNo , ByVal strCellData)
On Error Resume Next
	
	'Variable Section Begin
		Dim intActColCount	, arrColData
		Dim intInstance , intCnt
		
		intInstance = 0
	'Variable Section End

	If IsKey(strKey) = 0 Then
		If Not IsNull(strCellData) And Not IsEmpty(strCellData) Then
			If IsWholeNumber(intColNo) = 0 Then
				If CheckObjectExist(objJTable)  Then 
					intActColCount = objJTable.GetROProperty("cols")
					If Err.number = 0 And Not IsEmpty(intActColCount) And Not IsNull(intActColCount) Then
						
						intColNo = CInt(intColNo)
						intActColCount = CInt(intActColCount)

						If intColNo>0 And intColNo <= intActColCount Then
							arrColData = GetJTableColData(objJTable,intColNo-1)
							If Not IsNull(arrColData) And IsArray(arrColData) Then
								For intCnt = 0 To UBound(arrColData)
									If CompareStringValuesCaseSensitive(strCellData,arrColData(intCnt)) = 0 Then
										intInstance = intInstance + 1
									End if
								Next
								
								If Err.number = 0 Then
									AddItemToStorageDictionary strKey , intInstance
									GetInstanceOfDataInJColumn = 0
									WriteStatusToLogs "An instance '"& intInstance &"'of data '"&strCellData&"'Stored successfully."
									
								Else
									GetInstanceOfDataInJColumn = 2
								End if

							Else
								GetInstanceOfDataInJColumn = 6
							End if
						Else
							GetInstanceOfDataInJColumn = 7
						End If
						
					Else
						GetInstanceOfDataInJColumn = 6
					End If
					
				Else
					GetInstanceOfDataInJColumn = 1
				End If
				
			Else
				GetInstanceOfDataInJColumn = 5
			End if
		Else
			GetInstanceOfDataInJColumn = 4
		End if
		
	Else
		GetInstanceOfDataInJColumn = 3
	End if

On Error goto 0
End Function


'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name           :	  CheckJavaObjectVisible()
' 	Purpose                 :	  Checks whether the object is visible.
' 	Return Value            :	  Boolean( 0/1)
' 	Arguments               :
'		Input Arguments     :  	  objObject
'		Output Arguments    :     blnActData
'	Function is called by   :     Button(),CheckBox(),EditBox(),Element(),Image(),Link(),ListBox(),RadioButton()
'	Function calls          :     CompareStringValues()
'	Created on              :	  30/06/2009
'	Author                  :	  Vidyasagar
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function CheckJavaObjectVisible(ByVal objObject )
	On Error Resume Next
		'Variable Section Begin

			Dim blnActData, blnstatus
			blnstatus = false

		'Variable Section End
			 blnActData=objObject.GetRoProperty("displayed")
			 'blnActData=objObject.GetRoProperty("enabled")
				If Vartype(blnActData) <> vbEmpty  And Err.Number = 0 Then
					blnstatus = true
				End If
			 If blnstatus Then
					If  blnActData = True Or blnActData = 1  Then	  'This step updated by Vidya							
						CheckJavaObjectVisible = True
						WriteStatusToLogs   "The specified Object is visible."
					Else
						CheckJavaObjectVisible = False 
						 WriteStatusToLogs  "The specified Object is not visible."
				   End If
			Else
				CheckJavaObjectVisible = False
					  WriteStatusToLogs "An error occurred during the method 'CheckJavaObjectVisible'"
		   End If
On Error GoTo 0			
End Function

'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name             :	 GetItemFromJavaObject()
' 	Purpose                   :	 To validate the given item found in the litem of the object
' 	Return Value              :	 Boolean( 0/1)
' 	Arguments                 :
'		Input Arguments       :  objObject
'		Output Arguments      :  intActData
'	Function is called by     :  Button(),CheckBox(),EditBox(),Element(),Image(),Link(),ListBox(),RadioButton()
'	Function calls            :            
'	Created on                :	 30/06/2009
'	Author                    :	 Vidyasagar
'
'------------------------------------------------------------------------------------------------------------------------------------------

Function GetItemFromJavaObject(byval objObject, byval ItemName)
   	On Error Resume Next
		'Variable Section Begin
			Dim blnstatus, blnMatch
            Dim strListContent, strActValue, strTempValue
			Dim arrListContents
			Dim intmaxlength, intcount
			
			blnstatus = false
			blnMatch = False
			
		   
		'Variable Section End

	   				strListContent = objObject.GetContent

				If Vartype(strListContent) <> vbempty  And Err.Number = 0 Then
					blnstatus = true
				End If
					If blnstatus  Then
							arrListContents = GetArray(strListContent, ";") 'This step modified by vidya
							intmaxlength = ubound(arrListContents)
							
							For intcount = 0 to intmaxlength
								If UCase(Trim(arrListContents(intcount)))  = UCase(Trim(ItemName))Then
									blnMatch =True
									strTempValue = arrListContents(intcount)
									Exit for
								End If
							Next
									If blnMatch  Then
										GetItemFromJavaObject = strTempValue 
										WriteStatusToLogs "Actual Item ['" &ItemName & "'] is found."
										Else
										GetItemFromJavaObject =Null
										    WriteStatusToLogs "Actual Item ['" &ItemName & "'] is not found."
									End If
				  Else'blnstatus
						GetItemFromJavaObject = Null
						WriteStatusToLogs "An error occurred while getting the Tree List node."
				End if

On Error Goto 0
End Function

'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name			:	CheckJavaObjectInVisible()
' 	Purpose					:	Checks whether the object is visible.
' 	Return Value			:	Boolean( 0/1)
' 	Arguments				:
'		Input Arguments		:  	objObject
'		Output Arguments	:   strActData
'	Function is called by	:   Button(),CheckBox(),EditBox(),Element(),Image(),Link(),ListBox(),RadioButton()
'	Function calls			:   CompareStringValues()
'	Created on				:	01/07/2009
'	Author					:	Vidyasagar
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function CheckJavaObjectInVisible(ByVal objObject )
	On Error Resume Next
		'Variable Section Begin

			Dim blnActData, blnstatus
			blnstatus = False

		'Variable Section End
			blnActData=objObject.GetRoProperty("displayed")
				If Vartype(blnActData) <> vbEmpty  And Err.Number = 0 Then
					blnstatus = true
				End If
			 If blnstatus Then
					If  blnActData = False Or blnActData = 0  Then	 								
						CheckJavaObjectInVisible = True
						WriteStatusToLogs   "The Object is not visible."
					Else
						CheckJavaObjectInVisible = False 
						WriteStatusToLogs   "The Object is  visible."
				   End If
			Else
			   CheckJavaObjectInVisible = False
			   WriteStatusToLogs "An error occurred during the method 'CheckJavaObjectInVisible'."
		    End If
On Error GoTo 0			
End Function


'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name			:	GetRowNumberForNInstanceofDataInJColumn()
' 	Purpose					:	Stores the row number of the Nth occurence of the specified cell data within a specified column
' 	Return Value			:	Boolean( 0/1/2/3/4/5/6/7)
' 	Arguments				:
'		Input Arguments		:  	objJTable, strKey, intColNo, strCellData, n
'		Output Arguments	:   
'	Function is called by	:   StoreRowNumberHavingCellData()
'	Function calls			:   IsWholeNumber(), CheckObjectExist(), CompareStringValuesCaseSensitive(), AddItemToStorageDictionary()
'	Created on				:	03/07/2009
'	Author					:	Vaibhav Kelkar
'
'------------------------------------------------------------------------------------------------------------------------------------------

Function GetRowNumberForNInstanceofDataInJColumn(ByVal objJTable , ByVal strKey , ByVal intColNo , ByVal strCellData, byval n)
On Error Resume Next
	
	'Variable Section Begin
		Dim intActRowCount	, arrColData, intActColCount, intCntFound
		Dim intInstance , intCnt
		Dim intN
		Dim blnFound
		Dim strActCellData
		intInstance = 0
		blnFound = false 
	'Variable Section End
	'Variable Section End
	If IsKey(strKey) = 0 Then
		If Not IsNull(strCellData) And Not IsEmpty(strCellData) Then
			If IsWholeNumber(intColNo) = 0 Then
				If CheckObjectExist(objJTable)  Then 
				   
					intActColCount = objJTable.GetROProperty("cols")
					If Err.number = 0 And Not IsEmpty(intActColCount) And Not IsNull(intActColCount) Then
						intActColCount = cint(intActColCount)
						 intActRowCount = objJTable.GetROProperty("rows")
						  If Err.number = 0 And Not IsEmpty(intActRowCount) And Not IsNull(intActRowCount) Then
								intColNo = CInt(intColNo)
								intActRowCount = CInt(intActRowCount)
								intN = Cint(n)						
		
								If intColNo>0 And intColNo <= intActColCount Then
								  
									
										For intCnt = 0 To intActRowCount-1
											strActCellData = objJTable.GetCellData(intCnt,intColNo-1)
											If CompareStringValuesCaseSensitive(strCellData,strActCellData) = 0 Then
												intInstance = intInstance + 1
												If  intInstance = intN Then
															intCntFound = intCnt + 1
															AddItemToStorageDictionary strKey , intCntFound
															blnFound = true
															Exit for
														
												End If
											End if
										Next

										If  blnFound Then
											   GetRowNumberForNInstanceofDataInJColumn = 0
										Else
											   GetRowNumberForNInstanceofDataInJColumn = 2
										End If
										
									   
		
									
								Else
									GetRowNumberForNInstanceofDataInJColumn = 7
								End If
						Else
								 GetRowNumberForNInstanceofDataInJColumn = 6
						End If
						
					Else
						GetRowNumberForNInstanceofDataInJColumn = 6
					End If
					
				Else
					GetRowNumberForNInstanceofDataInJColumn = 1
				End If
				
			Else
				GetRowNumberForNInstanceofDataInJColumn = 5
			End if
		Else
			GetRowNumberForNInstanceofDataInJColumn = 4
		End if
		
	Else
		GetRowNumberForNInstanceofDataInJColumn = 3
	End if

On Error goto 0
End Function

'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name			:	CheckJavaObjectStatus()
' 	Purpose					:	Verifies  status(enabled/disabled) of an object.
' 	Return Value			:	Boolean( 0/1)
' 	Arguments				:
'		Input Arguments		:  	objObject
'		Output Arguments	:   intActData
'	Function is called by	:   Button(),CheckBox(),EditBox(),Element(),Image(),Link(),ListBox(),RadioButton()
'	Function calls			:            
'	Created on				:	03/07/2009
'	Author					:	Vidyasagar
'
'------------------------------------------------------------------------------------------------------------------------------------------

Function CheckJavaObjectStatus(ByVal objObject)
	On Error Resume Next
		'Variable Section Begin
		
			Dim intActData, blnstatus 
			blnstatus  = false

		'Variable Section End
			intActData = objObject.GetROProperty("enabled") 
				If Vartype(intActData) <> vbEmpty  And Err.Number = 0 Then
					blnstatus = true
				End If
			If 	blnstatus then
					If intActData =  True Or intActData =  "1"  Then
						 CheckJavaObjectStatus = True
						 WriteStatusToLogs "The specified Object is enabled."
					ElseIf intActData = false  Then
						  CheckJavaObjectStatus = False
						  WriteStatusToLogs "The specified Object is disabled."
					End If
		Else
		 WriteStatusToLogs "An error occurred during the method 'CheckObjectStatus'"
		End if		
	
  On Error GoTo 0
		  
End Function

'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name :				 GetObjectItemPropertyName()
' 	Purpose :					To get the property name and  its value of the item which is existing in the object.
' 	Return Value :		 		  Boolean( 0/1)
' 	Arguments :
'		Input Arguments :  		objObject
'		Output Arguments :     intActData
'	Function is called by :      Button(),CheckBox(),EditBox(),Element(),Image(),Link(),ListBox(),RadioButton()
'	Function calls :            
'	Created on :				  06/03/2008
'	Author :						Rathna Reddy
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function GetObjectItemPropertyName(byval objObject, byval ItemName, byval PropertyName)
   	On Error Resume Next
		'Variable Section Begin
			Dim blnActData , blnstatus
            Dim strActValue
			
			blnstatus = false
			strActValue = Empty
		'Variable Section End

		strActValue = objObject.GetItemProperty(ItemName, PropertyName)
			If Vartype(strActValue) <> vbEmpty  And Err.Number = 0 Then
				blnstatus = true
			End If


			     If  blnstatus Then
					 GetObjectItemPropertyName = strActValue
					  WriteStatusToLogs "The item Name ["&ItemName&"]::  Property Name["&PropertyName&"] and PropertyValue ["&strActValue&"]"
					
			Else
			   GetObjectItemPropertyName = Null
			   WriteStatusToLogs "An error occurred while capturing Item ["& ItemName &"] property Name ["& PropertyName &"], please verify Property Name or Property Value."
            End If   
On Error Goto 0
End Function

'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name			:	GetRowNumberForNInstanceofDataInColumn()
' 	Purpose					:	Stores the row number of the Nth occurence of the specified cell data within a specified column
' 	Return Value			:	Boolean( 0/1/2/3/4/5/6/7)
' 	Arguments				:
'		Input Arguments		:  	objJTable, strKey, intColNo, strCellData, n
'		Output Arguments	:   
'	Function is called by	:   StoreRowNumberHavingCellData()
'	Function calls			:   IsWholeNumber(), CheckObjectExist(), CompareStringValuesCaseSensitive(), AddItemToStorageDictionary()
'	Created on				:	03/07/2009
'	Author					:	Vaibhav Kelkar
'
'------------------------------------------------------------------------------------------------------------------------------------------

'Function GetRowNumberForNInstanceofDataInColumn(ByVal objJTable , ByVal strKey , ByVal intColNo , ByVal strCellData, byval n)
'On Error Resume Next
	
	'Variable Section Begin
'		Dim intActRowCount	, arrColData, intActColCount, intCntFound
'		Dim intInstance , intCnt
'		Dim intN
'		Dim blnFound
'		Dim strActCellData
'		intInstance = 0
'		blnFound = false 
	'Variable Section End
	'Variable Section End
'	If IsKey(strKey) = 0 Then
'		If Not IsNull(strCellData) And Not IsEmpty(strCellData) Then
'			If IsWholeNumber(intColNo) = 0 Then
'				If CheckObjectExist(objJTable)  Then 
				   
'				 	intActColCount = objJTable.GetROProperty("cols")
'					If Err.number = 0 And Not IsEmpty(intActColCount) And Not IsNull(intActColCount) Then
'						intActColCount = cint(intActColCount)
'						 intActRowCount = objJTable.GetROProperty("rows")
'						  If Err.number = 0 And Not IsEmpty(intActRowCount) And Not IsNull(intActRowCount) Then
'								intColNo = CInt(intColNo)
'								intActRowCount = CInt(intActRowCount)
'								intN = Cint(n)						
		
'								If intColNo>0 And intColNo <= intActColCount Then
								  
									
'										For intCnt = 1 To intActRowCount
'											strActCellData = objJTable.GetCellData(intCnt,intColNo)
'											If CompareStringValuesCaseSensitive(strCellData,strActCellData) = 0 Then
'												intInstance = intInstance + 1
'												If  intInstance = intN Then
'															intCntFound = intCnt 
'															AddItemToStorageDictionary strKey , intCntFound
'															blnFound = true
'															Exit for
														
'												End If
'											End if
'										Next

'										If  blnFound Then
'											   GetRowNumberForNInstanceofDataInColumn = 0
'										Else
'											   GetRowNumberForNInstanceofDataInColumn = 2
'										End If
										
									   
		
									
'								Else
'									GetRowNumberForNInstanceofDataInColumn = 7
'								End If
'						Else
'								 GetRowNumberForNInstanceofDataInColumn = 6
'						End If
						
'					Else
'						GetRowNumberForNInstanceofDataInColumn = 6
'					End If
					
   
'				Else
'					GetRowNumberForNInstanceofDataInColumn = 1
'				End If
				
'			Else
'				GetRowNumberForNInstanceofDataInColumn = 5
'			End if
'		Else
'			GetRowNumberForNInstanceofDataInColumn = 4
'		End if
		
'	Else
'		GetRowNumberForNInstanceofDataInColumn = 3
'	End if

'On Error goto 0
'End Function


'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :			  GetColumnNumber()
'	Purpose :				  Stores the column number of the column header pass
'	Return Value :		 	  Integer(0/1/2/3/4/5)
'							  0 : Success 
'							  1 : Object not Exist 
'							  2 : The column header does not exist
'							  3 : Key passed is invalid 
'							  4 : The column header is either null or empty 
'							  5 : Failed to retrieve table data 
'	Arguments :
'		Input Arguments :  	  objJTable , strKey , strColName
'		Output Arguments : 
'	Function is called by :	  StoreJColumnNumber()	
'	Function calls :		  CheckObjectExist() , AddItemToStorageDictionary() , IsKey()
'	Created on :			  01/07/2009
'	Author :				  Iohbha K
'
'------------------------------------------------------------------------------------------------------------------------------------------
'Function GetColumnNumber(ByVal objJTable, ByVal strKey, ByVal strColName)
'On Error Resume Next

	'Variable Section Begin
'		Dim intActColCount , intCnt , strActColName
'		Dim blnFound , intColnoFound
'		blnFound = false
	'Variable Section End

'	If IsKey(strKey) = 0 Then
'		If Not IsNull(strColName) And Not IsEmpty(strColName) Then
'			If CheckObjectExist(objJTable)  Then 

'				intActColCount = objJTable.getROProperty("cols")
'				If Err.number =0 And Not isempty(intActColCount) And Not IsNull(intActColCount) Then
					
'					For intCnt = 0 To intActColCount-1
'						strActColName = objJTable.GetColumnName(CInt(intCnt))
'						If Err.number = 0 And Not IsEmpty(strActColName) Then
'							If Trim(Ucase(strActColName)) = Trim(UCase(strColName)) Then
'								blnFound = True
'								intColnoFound = intCnt + 1
'								Exit For
'							End If
'						Else
'							GetColumnNumber = 5
'							Exit Function
'						End if
		
'					Next

'					If blnFound = True Then 
'						AddItemToStorageDictionary strKey , intColnoFound 			    
'						GetColumnNumber= 0
'					Else
'						GetColumnNumber= 2
'					End if

'				Else
'					GetColumnNumber = 5	
'				End if

'			Else
'				GetColumnNumber = 1
'			End If
			
'		Else
'			GetColumnNumber = 4
'		End if

'	Else
'		GetColumnNumber = 3
'	End if

'On Error goto 0
'End Function
'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name :				 IsItemFindInJTreeNode()
' 	Purpose :					To validate the given item found in the litem of the object
' 	Return Value :		 		  Boolean( 0/1)
' 	Arguments :
'		Input Arguments :  		objObject
'		Output Arguments :     intActData
'	Function is called by :      
'	Function calls :            
'	Created on :				  06/03/2008
'	Author :						Ashok Jakkani
'
'------------------------------------------------------------------------------------------------------------------------------------------
'Function IsItemFindInJTreeNode(byval objObject, byval ItemName)
'   	On Error Resume Next
'		'Variable Section Begin
'			Dim blnstatus, blnMatch
'            Dim strListContent, strActItem
'			Dim arrListContents
'			Dim intItemsCount, intcount
'			
'			blnstatus = false
'			blnMatch = False
'		   
'		'Variable Section End
'
'	   			intItemsCount = objObject.GetRoProperty("items count")
'
'				If intItemsCount >0 And Err.Number = 0 Then
'					blnstatus = true
'				End If
'					If blnstatus  Then
'                           For intcount = 0 to intItemsCount-1
'							   strActItem = objObject.GetItem(intcount)
'								If UCase(Trim(strActItem))  = UCase(Trim(ItemName))Then
'									blnMatch =True
'									Exit for
'								End If
'							Next
'									If blnMatch  Then
'										IsItemFindInJTreeNode = True
'										WriteStatusToLogs  "Method Name:" & "IsItemFindInJTreeNode"&vbtab&_
'																				"Message:" & "The specified Item["& ItemName &"] is Found"
'										Else
'										IsItemFindInJTreeNode =False
'										
'									End If
'				  Else'blnstatus
'						IsItemFindInJTreeNode = False
'						WriteStatusToLogs "Method Name: IsItemFindInJTreeNode"&Vbtab&_
'												"Status:" &  "Failed" &Vbtab&_
'												"Message:" & "An error occurred while Getting the Tree List Nodes."
'				End if
'
'On Error Goto 0
'End Function

'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name :				 GetItemFromJTreeNode()
' 	Purpose :					To validate the given item found in the litem of the object
' 	Return Value :		 		  Boolean( 0/1)
' 	Arguments :
'		Input Arguments :  		objObject
'		Output Arguments :     intActData
'	Function is called by :      
'	Function calls :            
'	Created on :				  06/03/2008
'	Author :						Rathna Reddy
'
'------------------------------------------------------------------------------------------------------------------------------------------
'Function GetItemFromJTreeNode(byval objObject, byval ItemName)
'   	On Error Resume Next
'		'Variable Section Begin
'			Dim blnstatus, blnMatch
'            Dim strListContent, strActValue, strTempValue
'			Dim arrListContents
'			Dim intmaxlength, intcount
'			Dim intItemsCount, strActItem
'			Dim i
'			
'			blnstatus = false
'			blnMatch = False
'			
'		   
'		'Variable Section End
'
'	   				'strListContent = objObject.GetROProperty("list_item_name")
'					intItemsCount = objObject.GetRoProperty("items count")
'
'				If intItemsCount > 0 And Err.Number = 0 Then
'					blnstatus = true
'				End If
'					If blnstatus  Then
'                            For intcount = 0 to intItemsCount-1
'								strActItem = objObject.GetItem(intcount)
'								If UCase(Trim(strActItem))  = UCase(Trim(ItemName))Then
'									blnMatch =True
'									strTempValue = strActItem
'									Exit for
'								End If
'							Next
'									If blnMatch  Then
'										GetItemFromJTreeNode = strTempValue 
'										WriteStatusToLogs "Method Name: " & "GetItemFromJTreeNode"&Vbtab&_
'											  "Message: Actual Item ['" &ItemName & "'] is Found"
'										Else
'										GetItemFromJTreeNode =Null
'										    WriteStatusToLogs "Method Name: " & "GetItemFromJTreeNode"&Vbtab&_
'											  "Message: Actual Item ['" &ItemName & "'] is Not Found"
'									End If
'				  Else'blnstatus
'						GetItemFromJTreeNode = Null
'						WriteStatusToLogs "Method Name: GetItemFromJTreeNode"&Vbtab&_
'												"Status:" &  "Failed" &Vbtab&_
'												"Message:" & "An error occurred while Getting the Tree List Nodes."
'				End if
'
'On Error Goto 0
'End Function

'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name				:	GetValidJTreeStringFromArray()
' 	Purpose						:	Verifies tThe the given input is wrong or not?
' 	Return Value				:1.    Null : if the element path is not existing in hierachy
'											:2.  String : if the element is existing in hierarchy.
' 	Arguments					:
'		Input Arguments			:  	objJavaTree
'		Output Arguments		:   
'	Function is called by		:
'	Function calls				:   CheckObjectExist(),CheckJavaObjectVisible()
'	Created on					:   19/08/2009
'	Author						:	Vidyasagar
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function GetValidJTreeStringFromArray(Byval arrJTreeNodes)
   On Error Resume Next
   Dim strJTreeString
   Dim intCnt
   strJTreeString = ""
   
   				If  isnull( arrJTreeNodes) or isempty(arrJTreeNodes) then
						GetValidJTreeStringFromArray = Null
						WriteStatusToLogs "The tree path of the node is invalid, please verify."
				Else
						
						If  isArray(arrJTreeNodes)Then
								For intCnt =0 to ubound(arrJTreeNodes)
										strJTreeString =  strJTreeString& arrJTreeNodes(intCnt) & ";"  										
								Next
								strJTreeString = Left(strJTreeString , Len(strJTreeString) - 1)
								GetValidJTreeStringFromArray = strJTreeString
						Elseif not Isobject(arrJTreeNodes) Then 
								strJTreeString = arrJTreeNodes
								GetValidJTreeStringFromArray  =  strJTreeString
						End If
				End if
				On Error Goto 0
End Function
'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name				:	GetInValidJItemInHierarchy()
' 	Purpose						:	Verifies The the given input is wrong or not?
' 	Return Value				:   1. Null If the element exists in the hierarchy
'												2.	returen index and element name if not exist in the hierarchy
' 	Arguments					:
'		Input Arguments			:  	objJavaTree
'		Output Arguments		:   
'	Function is called by		:
'	Function calls				:   CheckObjectExist(),CheckJavaObjectVisible()
'	Created on					:   19/08/2009
'	Author						:	ashok
'
'------------------------------------------------------------------------------------------------------------------------------------------
Public function GetInValidJItemInHierarchy(Byval ObjTreeView,  Byval strExpString)
   On Error Resume Next
						Dim arrExpItems
						Dim blnMatchNode, blnNotFound 
						Dim intMaxCount , i, intMaxitems, j, k
						Dim strParentHierarchy, strTempSelectHierarchy, strTempSelectelement, strExpandedItems

						blnMatchNode = False
						blnNotFound =False
						arrExpItems = Split(strExpString, ";")
						
						'arrTreeViewItems  = arrParentPath
						intMaxCount = ubound(arrExpItems)
								For i = 0 to intMaxCount
									strParentHierarchy = strParentHierarchy & arrExpItems(i)
								   intMaxitems =  ObjTreeView.GetRoProperty("items count")
                                   strExpandedItems = ObjTreeView.GetROProperty("list_item_name")
                                   
									For j=0 to intMaxitems-1
										blnMatchNode= False
                                        If strParentHierarchy  = ObjTreeView.GetItem(j)   Then
											blnMatchNode = True
											'&&&Modify
														If InStr(1,strExpandedItems, strParentHierarchy) = 1 then
																 If i <= intMaxCount-1 Then
																		ObjTreeView.Expand ObjTreeView.GetItem(j)
																End If
														Elseif Ubound(Split(strExpandedItems)) < Ubound(Split(strParentHierarchy) )Then
																		ObjTreeView.Expand ObjTreeView.GetItem(j)
														End if
											'&&&End Modify
											strParentHierarchy = strParentHierarchy &";"

											Exit for
											Elseif j= intMaxitems-1 Then
											blnNotFound = True
                                       End If
									Next
									If blnNotFound  Then
										Exit for
									End If

								Next
								If  blnNotFound Then
									GetInValidJItemInHierarchy = arrExpItems(i)&"~~"&i
									'WriteStatusToLogs "Action: GetInValidJItemInHierarchy"&Vbtab&_
									'													"Status:" &  "Failed" &Vbtab&_
									'													"Message:" & "The Item ['"& arrExpItems(i) &"'] at Position "& i &" in "&strParentHierarchy&" not existing in Tree view. Please Verify,"
									Else
									GetInValidJItemInHierarchy = NULL
								End If
	On Error Goto 0  	
End Function

'=======================Code Section End=============================================
