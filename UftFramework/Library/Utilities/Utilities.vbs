'Function GetConfigValues(byval Keyname)
'Function GetParentFld()
'Function GetINIFilepath()
'Function GetConfigFilepath()
'Function GetHostName()
'Function GetUserName()
'Function GetChildItemCountInTable(objTable, intRowNum, intColNum, strMicClassName)
'Function GetChildItem(objTable, intRowNum, intColNum, strMicClassName)
'Function GetTableColData(ByVal objTable, ByVal intColNumber)	
'Function GetTableRowData(ByVal objTable, ByVal intRowNumber)	
'Function GetArray(Byval strData,ByVal strSeperator)
'Function CompareArrays(Byval arrExpData,ByVal arrActData)
'Function CompareArraysInSequence(Byval arrExpData,ByVal arrActData)
'Function VerifyDuplicateElementsInArray(ByVal arrActArray)
'Function PrintArrayElements(Byval arrExpData)
'Function MatchEachArrData(Byval arrExpData,ByVal arrActData)
'Function CompareStringValues(byval strExpectedVal, byval strActualVal)
'Function CompareStringValuesCaseSensitive(byval strExpectedVal, byval strActualVal)
'Function GetObjectProperty(byval objObject ,byval strPropName)
'Function CheckNumber(ByVal intValue)
'Function IsInteger(byval intVal)
'Function IsKey(byval strkey)
'Function AddItemToStorageDictionary( byval strKey , byval strItem)
'Function CompareArraysCaseSensitive(Byval arrExpData,ByVal arrActData)
'Function IsWholeNumber(byval Val)
'Function FormatString(ByVal str, ByVal arrParams)
'Function CompareActualStringValues(byval strExpectedVal, byval strActualVal)
'Function MatchEachArrDataCS(Byval arrExpData,ByVal arrActData)
'Function CompareArraysInSequenceCS(Byval arrExpData,ByVal arrActData)
'Function VerifyDuplicateElementsInArrayCS(ByVal arrActArray)
'Function ConcateArrayItem(byval arrItems)
'Function IsItemExistInArrayCS(byval arrargListitems, byval argitem)
'Function CompareArraysCS(Byval arrExpData,ByVal arrActData)
'Function WriteReportPath(ByRef ReportPath)

Option Explicit
'=======================Code Section Begin=============================================
'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :				GetConfigValues()
'	Purpose :					To get the config values
'	Return Value :		 		string value
'	Arguments :
'	Input Arguments :  			Keyname
'	Output Arguments : 	   		strValue
'	Function is called by :
'	Function calls :			   
'	Created on : 				03/03/2008
'	Author :					Pravin Patil
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function GetConfigValues(byval Keyname)
	On error resume next
	Dim getConfigVal,BasePath,qualitiaPropertiesPath
	'msgbox Keyname
	err.clear
	'BasePath = GetCurrentPath()
	'qualitiaPropertiesPath= BasePath & "\Config\Qualitia.properties"
	
	'configObj.LoadQualitiaPropertiesFile(qualitiaPropertiesPath)
	getConfigVal = configObj.GetValueFromQualitiaProperties(Keyname)
	
	If err.number = 0 Then 
		GetConfigValues = getConfigVal
	Else
		'msgbox err.description
	End If
	' dim mxml
	' Dim keyNode
	' Dim temppath, fso

	' Set mxml = CreateObject("Microsoft.XMLDOM")
	' mxml.async = False
    
    ' mxml.load(QualitiaFolderPath & "\" & CONFIGFILEPATH)
	

    ' Set keyNode = mxml.selectSingleNode("//configuration/applicationSettings/Qualitia.Properties.Settings/setting[@name='" & Keyname & "']/value")
    

	
    ' if Not IsNull(keyNode) Then
       	 ' If Keyname = PH_XMLPATH or Keyname = PH_LOGPATH  Then
    		' If Not IsPathAbsolute(keyNode.text) Then
				' temppath = QualitiaFolderPath &"\"& keyNode.text
				' set fso = CreateObject("Scripting.FileSystemObject")
				' GetConfigValues = fso.GetAbsolutePathName(temppath)
				' set fso = Nothing
			' Else
				' GetConfigValues = keyNode.text
			' End If
		' Else
    	    ' GetConfigValues = keyNode.text
    	' End If
    	' logs.Debug "Reading config value ==> "& Keyname &" >> "& GetConfigValues
    ' Else
		' WriteToErrorLog Err, "Utility Name:&nbsp; GetConfigValues<br/>" &_
	                                 ' "Message:&nbsp;Failed to retrieve the value from config file for the key='" & Keyname & "'&nbsp;&nbsp;&nbsp;&nbsp;" &_
									 ' "Path&nbsp;of&nbsp;IniFile:" & CONFIGFILEPATH
    ' End If

	' If Err.number <> 0 Then
		' WriteToErrorLog Err, "Utility Name:&nbsp; GetConfigValues <br/>" &_
	                                 ' "Message:&nbsp;Failed to retrieve the value from config file for the key='" & Keyname & "'&nbsp;&nbsp;&nbsp;&nbsp;" &_
									 ' "Path&nbsp;of&nbsp;IniFile:" & CONFIGFILEPATH
	' End If
	On error goto 0
End Function

Function GetCurrentPath()
	Dim Path
	Dim fso
	Dim QTPFrameworkPath

	Path=Environment.Value("TestDir")
	Set fso=createobject("Scripting.FileSystemObject")
	GetCurrentPath = fso.GetParentFolderName(Path)
	
	If Err.number <> 0 Then
		WriteToErrorLog Err, "Failed to retrieve the ParentFolder path."
    End If
	On error goto 0
End Function

Function GetTempFolderPath()
	Dim fso
	Set fso=createobject("Scripting.FileSystemObject")
	GetTempFolderPath=fso.GetSpecialFolder(2)
	If Err.number <> 0 Then
		WriteToErrorLog Err, "Failed to retrieve the ParentFolder path."
    End If
On error goto 0
End Function

Function GetParentFld()
	Dim EnvGetCurTestPath
	Dim fso
	Dim QTPFrameworkPath

	EnvGetCurTestPath=Environment.Value("TestDir")
	Set fso=createobject("Scripting.FileSystemObject")
	
	QTPFrameworkPath = fso.GetParentFolderName(EnvGetCurTestPath)
	GetParentFld = fso.GetParentFolderName(QTPFrameworkPath)
	
	If Err.number <> 0 Then
		WriteToErrorLog Err, "Failed to retrieve the ParentFolder path."
    End If
End Function


Function GetINIFilepath()
	GetINIFilepath = GetParentFld & INIFILEPATH 
End Function

Function GetConfigFilepath()
	GetConfigFilepath = GetParentFld & INIFILEPATH 
End Function


'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :				 GetUsername()
'	Purpose :						To get the user name of the system in the network
'	Return Value :		 		  string value
'	Arguments :
'		Input Arguments :  		
'		Output Arguments : 	   strusername
'	Function is called by :
'	Function calls :			   
'	Created on : 				   03/03/2008
'	Author :						Rathna Reddy							 
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function GetUserName()
	Dim objWMIService, colItems
	Dim strComputer
	Dim strusername
	On error resume next
	strComputer = "."
	
	Set objWMIService = GetObject("winmgmts:\\" & strComputer & "\root\cimv2")
	Set colItems = objWMIService.ExecQuery("Select * From Win32_ComputerSystem")

    Dim objItem
		For Each objItem in colItems
			strusername= objItem.UserName
		Next
		GetUserName= strusername
		If Err.Number <> 0 Then
		WriteToErrorLog Err,"The UserName of the machine could not be retrieved."
		End If
End Function

'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :				 GetHostName()
'	Purpose :						To get the host name of the system in the network
'	Return Value :		 		  string value
'	Arguments :
'		Input Arguments :  		
'		Output Arguments : 	   sstrHostName
'	Function is called by :
'	Function calls :			   
'	Created on : 				   03/03/2008
'	Author :						Rathna Reddy							 
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function GetHostName()
	Dim objWMIService, objItem, colItems
	Dim strComputer
	Dim sstrHostName
	strComputer = "." 
	On Error Resume Next

	Set objWMIService = GetObject("winmgmts:\\" & strComputer & "\root\cimv2")
    Set colItems = objWMIService.ExecQuery("Select * from Win32_OperatingSystem",,48)

	For Each objItem in colItems 
	sstrHostName = objItem.CSName 
	Next

	GetHostName = sstrHostName
	If Err.Number <> 0 Then
			WriteToErrorLog Err, "The Hostname of the machine could not be retrieved."
	End If
End function



'**********************************************************************************


      
                                '******************************************************
                                '                                  Table Utilities
								
								'*****************************************************

'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :				  GetChildItemCountInTable()
' 	Purpose :						  Finds the number of objects of a specific type in the specified cell. 
' 	Return Value :		 		 	Integer( 0/1)
' 	Arguments :
'		Input Arguments :  		 objTable , strClassName , intRowNum , intColNum
'		Output Arguments :      intObjCount
'	Function is called by :
'	Function calls :				
'	Created on :				   20/03/2008
'	Author :						 Rathna Reddy 
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function GetChildItemCountInTable(objTable, intRowNum, intColNum, strMicClassName)
     On Error Resume Next
		'Variable Section Begin
		
			Dim intObjCount, strObjObjects
		'Variable Section End	
		     
					intObjCount= objTable.ChildItemCount(intRowNum , intColNum , strMicClassName)
                    
                    If intObjCount >0  Then
                            GetChildItemCountInTable = intObjCount
							WriteStatusToLogs "The specified table cell contains "&strMicClassName
																	   
																	  
                    Else
                            GetChildItemCountInTable = Null
							WriteStatusToLogs "The specified table cell does not contain "&strMicClassName 
							
					End If
					

                    
   On Error GoTo 0 
End Function

'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :				  GetChildItem()
' 	Purpose :						  Finds the number of objects of a specific type in the specified cell. 
' 	Return Value :		 		 	Integer( 0/1)
' 	Arguments :
'		Input Arguments :  		 objTable , strClassName , intRowNum , intColNum
'		Output Arguments :      intObjCount
'	Function is called by :
'	Function calls :				
'	Created on :				   20/03/2008
'	Author :						 Rathna Reddy 
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function GetChildItem(objTable, intRowNum, intColNum, strMicClassName)
     On Error Resume Next
		'Variable Section Begin
		
			Dim  objChildObject
			
		'Variable Section End	
		     
					set objChildObject= objTable.ChildItem(intRowNum , intColNum , strMicClassName, 0)
					
                   If UCase(Trim(objChildObject.GetRoProperty("micclass")))=UCase(Trim(strMicClassName)) Then
							If  err.Number = 0 Then
								  Set  GetChildItem = objChildObject
										WriteStatusToLogs "object of type "&strMicClassName&" exists and returned successfully from the specified table cell."
							Else
									set GetChildItem = Null
									WriteStatusToLogs "object of type "&strMicClassName&" does not  exist in the table cell."
							End If
				End If
   On Error GoTo 0 
End Function

'------------------------------------------------------------------------------------------------------------------------------------------


' 	Function Name :				 GetTableColData()
' 	Purpose :						 Retrieves data from specified Column of specified table and stores in a dictionary object
' 	Return Value :		 		   Array
' 	Arguments :
'		Input Arguments :  		 objTable,strExpColName,dictKey,intRowNum
'		Output Arguments :      intColCount,strActColName,intRow,intCol,intIndex,arrColData(),strColData
'	Function is called by :     CompareTableRowData()
'	Function calls :				WriteToDebugLog
'	Created on :				   20/03/2008
'	Author :						 Rathna Reddy 
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function GetTableColData(ByVal objTable, ByVal intColNumber)	
   On Error Resume Next
		'Variable Section Begin

			 Dim intRowCount,strColData,intRow,intCol,intIndex,arrColData()
             intIndex=0
			
		'Variable Section End
			
				intRowCount=objTable.RowCount
				For intRow=1 to intRowCount
						strColData = objTable.GetCellData(intRow,intColNumber)
						ReDim Preserve arrColData(intIndex)
						arrColData(intIndex) = Trim(strColData)
						intIndex=intIndex+1 

				Next

				
				If err.Number = 0 Then
					GetTableColData =  arrColData
					WriteStatusToLogs  "Table column data retrieved successfully."
				Else
				
					  GetTableColData = Null
					  WriteStatusToLogs "Utility fails to get table column data."
					
				End If
			
	On Error GoTo 0
End Function


'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name :				 GetTableRowData()
' 	Purpose :						 Retrieves data from specified Row of specified table and stores in a dictionary object
' 	Return Value :		 		   Integer( 0/1)
' 	Arguments :
'		Input Arguments :  		 objTable,strExpRowName,intColNum
'		Output Arguments :      intRowCount,intRow,intCol,strRowData,arrRowData(),intIndex,strActRowName,intColCount
'	Function is called by :      CompareTableColumnData()
'	Function calls :				GetArray(),CompareArrays()
'	Created on :				   20/03/2008
'	Author :						 Rathna Reddy 
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function GetTableRowData(ByVal objTable, ByVal intRowNumber)	
   On Error Resume Next
		'Variable Section Begin
	 
			 Dim intRowCount,intRow,intCol,strRowData,arrRowData(),intIndex,strActRowName,intColCount
             intIndex=0
			
		'Variable Section End
			
				 intColCount=objTable.ColumnCount(intRowNumber) 
					For intCol=1 to intColCount
						strRowData = objTable.GetCellData(intRowNumber,intCol)
							ReDim Preserve arrRowData(intIndex)
							arrRowData(intIndex) = Trim(strRowData)
							intIndex=intIndex+1 
					Next  

					If err.Number = 0 Then
                       GetTableRowData =  arrRowData


						WriteStatusToLogs  "Table row data retrieved successfully."
					Else
					
                          GetTableRowData = Null
						  
						  WriteStatusToLogs "Utility fails to get table row data."
						
					End If
					
				   
    On Error GoTo 0
End Function


                                '******************************************************
                                '                                  Array Utilities
								
								'*****************************************************



'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name :				GetArray()
' 	Purpose :						Forms an array collection.
' 	Return Value :		 		  arrCollection()
' 	Arguments :
'		Input Arguments :  	   strData , strSeperator
'		Output Arguments : 	  arrData
'	Function is called by :		CompareTableColData() , CompareTableRowData() , SumOfTableRowVals() , VerifyListItems() ,_
'                                       VerifyListItemsInSequence() , VerifyDuplicateItemsInListBox()                                         
'	Function calls :			   
'	Created on :				  13/03/2008
'	Author :						 Rathna Reddy
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function GetArray(Byval strData,ByVal strSeperator)
	On Error Resume Next
		 'Variable Section Begin
		
			Dim arrData

		'Variable Section End
		 
			arrData=Split(strData,strSeperator)
		   
			If Err.Number = 0 Then
				 GetArray=arrData
					WriteStatusToLogs "Expected Data:" &""""&strData&""""&"was returned as an array successfully."
			Else
				GetArray = Null
				WriteStatusToLogs "GetArray failed; Expected Data:" &""""&strData&""""&" was not returned as an array."
			End If
	On Error GoTo 0
End Function


'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name :				CompareArrays()
' 	Purpose :					    Compares Two Arrays. The data comparison is not case sensitive.
' 	Return Value :		 		  Boolean(0/1)
' 	Arguments :
'		Input Arguments :  	   arrExpData , arrActData
'		Output Arguments : 	  IntCount
'	Function is called by :		CompareTableColData() , CompareTableRowData() , VerifyListItems() 
'	Function calls :			   
'	Created on :				  13/03/2008
'	Author :						 Rathna Reddy
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function CompareArrays(Byval arrExpData,ByVal arrActData)
   On Error Resume Next
	 'Variable Section Begin
		
			
			Dim intCount, strExpItem, intActItem,strExpectedResult,strActualResult, flag
			flag=-1
			intCount=0

		'Variable Section End
		strExpectedResult = PrintArrayElements (arrExpData)
		strActualResult = PrintArrayElements (arrActData)
		For Each strExpItem  in arrExpData 
				For intActItem = 0 to Ubound(arrActData)
				
					  If CompareStringValues(strExpItem,arrActData(intActItem))= 0 Then
						  Exit for
					  Elseif intActItem = Ubound(arrActData) Then
                                intCount = intCount+1
					End If
				Next
               If (intCount > 0)  Then 
				  flag=1	
			      Exit For
			   End If
				flag=0
		 Next
	If flag = 0 Then
		CompareArrays = 0
        WriteStatusToLogs """"&"Expected Array Data : "&strExpectedResult& """" &  " contains " &""""&"Actual Array Data : "&strActualResult&""""
	Else
			CompareArrays = 2
			WriteStatusToLogs """"&"Expected Array Data : "&strExpectedResult& """" &  " does not contain " &""""&"Actual Array Data : "&strActualResult&""""
	End If
	 
  On Error GoTo 0
End Function
'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name :				CompareArraysInSequence()
' 	Purpose :					    Compares Two Arrays . The data comparison is not case sensitive.
' 	Return Value :		 		  Boolean(0/1)
' 	Arguments :
'		Input Arguments :  	   arrExpData , arrActData
'		Output Arguments : 	  IntCount
'	Function is called by :		CompareTableColData() , CompareTableRowData() , VerifyListItems() 
'	Function calls :			   
'	Created on :				  13/03/2008
'	Author :						 Rathna Reddy
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function CompareArraysInSequence(Byval arrExpData,ByVal arrActData)
   On Error Resume Next
		'Variable Section Begin
		
			
		Dim intCount, intItem,strExpectedResult,strActualResult
		intCount = 0
		
         'Variable Section End
		
		strExpectedResult =  "Expected Result= " & PrintArrayElements (arrExpData)&";"
		strActualResult = "Actual Result= "&PrintArrayElements (arrActData)&";"

        
		
		If Ubound(arrExpData) = Ubound(arrActData) Then

			For intItem = 0 to Ubound(arrExpData)
			   
					  If CompareStringValues(arrExpData(intitem),arrActData(intItem)) = 0 Then
						  intCount = 0
					  Else
						  intCount = intCount+1
                      End If
					If intCount > 0 Then
					 Exit For
					End If
			Next
			

			If intCount = 0 Then
					 CompareArraysInSequence =0
					WriteStatusToLogs "The Data Sequence of both the specified arrays are same."
			Else
					 CompareArraysInSequence = 1
					 WriteStatusToLogs "The expected and actual items does not match."
			End If
		Else
			CompareArraysInSequence = 1
			WriteStatusToLogs "The expected and actual items count does not match."&Vbtab&_
										"Expected Size: " & Ubound(arrExpData)&Vbtab&_
										"Actual Size: " & Ubound(arrActData)

		End if
		WriteStatusToLogs  strExpectedResult
		WriteStatusToLogs strActualResult
  On Error GoTo 0
End Function
''------------------------------------------------------------------------------------------------------------------------------------------
'' 	Function Name :				CompareArraysInSequence()
'' 	Purpose :					    Compares Two Array elements in sequence order.
'' 	Return Value :		 		  Boolean(0/1)
'' 	Arguments :
''		Input Arguments :  	   arrExpData , arrActData
''		Output Arguments : 	  IntCount
''	Function is called by :		CompareTableColData() , CompareTableRowData() , VerifyListItems() 
''	Function calls :			   
''	Created on :				  13/03/2008
''	Author :						 Rathna Reddy
''
''------------------------------------------------------------------------------------------------------------------------------------------
'Function CompareArraysInSequence(Byval arrExpData , ByVal arrActData)
'  On Error Resume Next
'          'Variable Section Begin
'		
'			
'			Dim intCount , intExpItem
'			intCount=0
'
'		'Variable Section End
'		 For intExpItem = 0 to Ubound(arrExpData)
'				If arrExpData(intExpItem) =arrActData(intExpItem) Then
'					''msgbox "Pass"	
'				Else
'				    intCount=intCount+1
'				End If  
'		Next
'			If  intCount > 0 Then
'				CompareArraysInSequence = 1
'			Else
'				CompareArraysInSequence = 0
'				WriteStatusToLogs  "Utility Name:" & "CompareArraysInSequence"&Vbtab&_
'										"Status:"&vbtab& CompareArraysInSequence &Vbtab&_
'                                        "Message:"&vbtab&"The CompareArraysInSequence Utility  is executed successfully"
'			End If
'			WriteStatusToLogs  "Utility Name:" & "CompareArraysInSequence"&Vbtab&_
'										"Status:"&vbtab& CompareArraysInSequence &Vbtab&_
'                                        "Message:"&vbtab&"The CompareArraysInSequence Utility  is not executed successfully"
'   On Error GoTo 0
'End Function
'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name :				VerifyDuplicateElemetsInArray()
' 	Purpose :					    Compares the Each element  with another element of array.
' 	Return Value :		 		  Boolean(0/1)
' 	Arguments :
'		Input Arguments :  	   arrActArray 
'		Output Arguments : 	  IntCount
'	Function is called by :		VerifyDuplicateItemsInListBox()
'	Function calls :			   
'	Created on :				  13/03/2008
'	Author :						 Rathna Reddy
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function VerifyDuplicateElementsInArray(ByVal arrActArray)
	On Error Resume Next
	
        'Variable Section Begin
			
			Dim countIDX,strActItem , intItem , intAheadItem , arrLen
		'Variable Section End
	
	countIDX=0
	arrLen=Ubound(arrActArray)
	For  intItem=0 to   arrLen
				strActItem=arrActArray(intItem)
				For  intAheadItem=intItem+1  to   arrLen
					If Ucase(Trim(strActItem) )=Ucase(Trim( arrActArray(intAheadItem)) )then
							   countIDX=countIDX+1 
							   Exit for
					End If
				Next
				If countIDX >0 then
					Exit for
				End If
	Next

	If countIDX > 0  Then
		VerifyDuplicateElementsInArray = 1
		WriteStatusToLogs  "The specified Array contains duplicate elements."
	Else
		VerifyDuplicateElementsInArray = 0
		WriteStatusToLogs  "The specified Array doesn't contain duplicate elements."
		
   End If
  On Error GoTo 0
End Function
'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name :				 PrintArrayElements()
' 	Purpose :					    Prints an array elements
' 	Return Value :		 		  Boolean(0/1)
' 	Arguments :
'		Input Arguments :  	   arrExpData , arrActData
'		Output Arguments : 	  IntCount
'	Function is called by :		CompareArrays()
'	Function calls :			   
'	Created on :				  13/03/2008
'	Author :						 Rathna Reddy
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function PrintArrayElements(Byval arrExpData)
   On Error Resume Next
	 'Variable Section Begin
			
			Dim strExpItem, strExpectedItems,intPosition,intLength,strArrayElements,strEmptyCell
             strEmptyCell="ERROR: The specified cell does not exist."
		'Variable Section End
		For  strExpItem = 0 to Ubound (arrExpData)
			If Trim(strEmptyCell)<>Trim(arrExpData(strExpItem)) Then
		
			    strExpectedItems=strExpectedItems& strDataSeparator & arrExpData(strExpItem)
		  End If
		Next
                intPosition=instr(strExpectedItems, strDataSeparator)
			    intLength=Len(strExpectedItems)
    			PrintArrayElements=mid(strExpectedItems,intPosition+1,intLength-intPosition)
      On Error GoTo 0
End Function




'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name :				MatchEachArrData()
' 	Purpose :					Compares Two Arrays in such a manner that both the array's data should have	a one To one mapping with each other.The data comparison is not case sensitive.
' 	Return Value :		 		Boolean(0/1)
' 	Arguments :
'		Input Arguments :  	    arrExpData , arrActData
'		Output Arguments : 	    
'	Function is called by :		VerifyAllListItems()  
'	Function calls :			   
'	Created on :				12/02/2009
'	Author :					Iohbha K
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function MatchEachArrData(Byval arrExpData,ByVal arrActData)
		
	On Error Resume Next
		'Variable Section Begin

		Dim lenActualArr,lenExpArr,strExpected,strActual,k,it,innerIt , strExpNotMatch ,strActNotMatch, strMessage
		Dim blnExpFlag , blnActFlag
		strMessage = "Message: "
		strExpNotMatch = " Following Expected data was not found in Actual array : "
		strActNotMatch = " Following Actual data was not found in Expected array : "

		'Variable Section End

		lenExpArr = Ubound(arrExpData)
		lenActualArr = Ubound(arrActData)
		
		If lenActualArr = lenExpArr Then
				  
					For it=0 to lenExpArr
							strExpected=arrExpData(it)
							For innerIt=0 to lenActualArr
										 strActual = arrActData(innerIt)
													
										If  CompareStringValuesWR(strExpected,strActual) = 0 Then
										    arrExpData(it)=""
											arrActData(innerIt)=""
											Exit for
										End If
							Next
					Next

					For k=0 to lenExpArr
							If   arrExpData(k) <>"" Then
								strExpNotMatch=strExpNotMatch&arrExpData(k)&", "
								blnExpFlag=true
							End If

					Next

					For k=0 to lenActualArr
							If    arrActData(k) <>"" Then
								strActNotMatch=strActNotMatch&arrActData(k)&", "
								blnActFlag=True
							End If
					Next

					If blnExpFlag = True  Or blnActFlag = True Then

						If blnExpFlag = True Then
						    strMessage = strMessage & strExpNotMatch
						End If

						If blnActFlag = True Then
							strMessage = strMessage & strActNotMatch
						End If
						
						MatchEachArrData = False
						
							
					Else 
						
						MatchEachArrData = True
						strMessage = strMessage& " Both the specified arrays are equal "
							
					End If
					WriteStatusToLogs  "MatchEachArrData "&Vbtab&strMessage
					
		Else
			MatchEachArrData = false
			WriteStatusToLogs  "The length of both the specified arrays are not equal."
		End If

	On Error GoTo 0

End Function





								'******************************************************
                                '                    String Utilities
								
								'*****************************************************

'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name :				CompareStringValues()
' 	Purpose :					Compares the two string. The comparison is not case sensitive. The method can also compare strings having a newline character.
' 	Return Value :		 		  intres
' 	Arguments :
'		Input Arguments :  	   strExpectedVal , strActualVal
'		Output Arguments : 	  int
'	Function is called by :		
'	Function calls :			   
'	Created on :				  13/03/2008
'	Author :						 Rathna Reddy
'Instruction : The strcomp is binary comparision so it is case sensitive
'------------------------------------------------------------------------------------------------------------------------------------------


Function CompareStringValues(byval strExpectedVal, byval strActualVal)
   On Error Resume Next

   	'Variable Section Begin
		
		  	Dim intres , strModifiedExp , strModifiedAct

		 'Variable Section End
			strModifiedExp=replace(strExpectedVal,vbCrLf,"")
			strModifiedAct=replace(strActualVal,vbCrLf,"")

			intres = StrComp(Trim(Ucase(strModifiedExp)),Trim(Ucase(strModifiedAct))) 
			Select Case intres

					Case 0
							WriteStatusToLogs """"&"Expected Result : "&strExpectedVal& """" &  "   and   " &""""&"Actual Result : "&strActualVal&""""&"  are equal."
											 
					Case 1
							WriteStatusToLogs """"&"Expected Result : "&strExpectedVal & """" &  "   and   " &""""&"Actual Result : "&strActualVal&""""&"  are not equal."
					
					Case -1
							WriteStatusToLogs  """"&"Expected Result : "&strExpectedVal& """" &  "   and   " &""""&"Actual Result : " &strActualVal&""""&"  are not equal."
												   
			End Select
			CompareStringValues = intres
  On Error GoTo 0
End Function




Function CompareStringValuesWR(byval strExpectedVal, byval strActualVal)
   On Error Resume Next

   	'Variable Section Begin
		
		  	Dim intres , strModifiedExp , strModifiedAct

		 'Variable Section End
			strModifiedExp=replace(strExpectedVal,vbCrLf,"")
			strModifiedAct=replace(strActualVal,vbCrLf,"")

			intres = StrComp(Trim(Ucase(strModifiedExp)),Trim(Ucase(strModifiedAct))) 
'			Select Case intres
'
'					Case 0
'							WriteStatusToLogs "Utility Name: " & "CompareStringValues"&Vbtab&_
'												"Message: "&""""&"Expected Result : "&strExpectedVal& """" &  "   and   " &""""&"Actual Result : "&strActualVal&""""&"  are equal."
'											 
'					Case 1
'							WriteStatusToLogs "Utility Name: " & "CompareStringValues"&Vbtab&_
'											  "Message: "&""""&"Expected Result : "&strExpectedVal & """" &  "   and   " &""""&"Actual Result : "&strActualVal&""""&"  are not equal"
'					
'					Case -1
'							WriteStatusToLogs  "Utility Name: " & "CompareStringValues"&Vbtab&_
'												"Message: "&""""&"Expected Result : "&strExpectedVal& """" &  "   and   " &""""&"Actual Result : " &strActualVal&""""&"  are not equal"
'												   
'			End Select
			CompareStringValuesWR = intres
  On Error GoTo 0
End Function





'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name :				CompareStringValuesCaseSensitive()
' 	Purpose :					Compares the two string. The comparison is not case sensitive. The method can also compare strings having a newline character.
' 	Return Value :		 		  intres
' 	Arguments :
'		Input Arguments :  	   strExpectedVal , strActualVal
'		Output Arguments : 	  int
'	Function is called by :		
'	Function calls :			   
'	Created on :				  13/03/2008
'	Author :						 Rathna Reddy
'Instruction : The strcomp is binary comparision so it is case sensitive
'------------------------------------------------------------------------------------------------------------------------------------------


Function CompareStringValuesCaseSensitive(byval strExpectedVal, byval strActualVal)
   On Error Resume Next

   	'Variable Section Begin
		
		  	Dim intres , strModifiedExp , strModifiedAct

		 'Variable Section End
			strModifiedExp=replace(strExpectedVal,vbCrLf,"")
			strModifiedAct=replace(strActualVal,vbCrLf,"")

			intres = StrComp(Trim(strModifiedExp),Trim(strModifiedAct)) 
			Select Case intres

					Case 0
							WriteStatusToLogs """"&"Expected Result : "&strExpectedVal& """" &  "   and   " &""""&"Actual Result : "&strActualVal&""""&"  are equal."
											 
					Case 1
							WriteStatusToLogs """"&"Expected Result : "&strExpectedVal & """" &  "   and   " &""""&"Actual Result : "&strActualVal&""""&"  are not equal."
					
					Case -1
							WriteStatusToLogs  """"&"Expected Result : "&strExpectedVal& """" &  "   and   " &""""&"Actual Result : " &strActualVal&""""&"  are not equal."
												   
			End Select
			CompareStringValuesCaseSensitive = intres
  On Error GoTo 0
End Function


'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :			  GetObjectProperty()
'	Purpose :				  Returns the property value of the object.
'							  If the property Name itself is not applicable/available for the object , then NULL[not a literal] is return
'	Return Value :		 	  String
'	Arguments :
'		Input Arguments :  	  strPropName 
'		Output Arguments : 
'	Function is called by :	  	
'	Function calls :		  
'	Created on :			  12/06/2009
'	Author :				  Iohbha K
'
'------------------------------------------------------------------------------------------------------------------------------------------

Function GetObjectProperty(byval objObject ,byval strPropName)
On Error Resume Next
	'Variable Section Begin
		Dim strPropValue
		
	'Variable Section End
	' Comment:
	' 1. When the property Name is available then value retrieve = ""
	'    When the property Name is not available , then value  retrieve = Empty 
	' 2. TOProperty : to acces objObject property from Object map. It returns what is there in object map
	'    ROproperty  : to access objObject property runtime that are available with the object
	'    Hence not considering TOProperty
	strPropValue = objObject.getROProperty(strPropName)
	If Not IsEmpty(strPropValue) Then
		GetObjectProperty = strPropValue
	Else
		
		'strPropValue = objObject.Object.strPropName
		strPropValue = eval("objObject.Object."&strPropName&"")
		If Not IsEmpty(strPropValue) Then
			GetObjectProperty = strPropValue
		Else
			GetObjectProperty = null
		End if
	
	End if
On Error goto 0
End Function










                                '******************************************************
                                '                    Verification Uitlities
								
								'*****************************************************



'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name :				CheckNumber()
' 	Purpose :					   Checks whether the given input is a Number.
' 	Return Value :		 		  strData
' 	Arguments :
'		Input Arguments :  	   intValue
'		Output Arguments : 	  strValue
'	Function is called by :		
'	Function calls :			   
'	Created on :				  13/03/2008
'	Author :						 Rathna Reddy
'
'------------------------------------------------------------------------------------------------------------------------------------------ 
Function CheckNumber(ByVal intValue)
   On Error Resume Next
   'Variable Section Begin
		  Dim strValue
		'Variable Section End
        strValue=IsNumeric(intValue)
		CheckNumber= strValue
  On Error GoTo 0
End Function




'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :			  IsInteger()
'	Purpose :				  Checks if the data passed in a Natural number.
'	Return Value :		 	  Integer(0/1)
'	Arguments :
'		Input Arguments :  	  intVal 
'		Output Arguments : 
'	Function is called by :	  	
'	Function calls :		  
'	Created on :			  12/06/2009
'	Author :				  Iohbha K
'
'------------------------------------------------------------------------------------------------------------------------------------------

Function IsInteger(byval intVal)
On error resume next
	'Variable Section Begin
		
		Dim blnStat

	 'Variable Section End

	   blnStat =  isnumeric(intVal)
	   If blnStat Then
			If instr(intVal,"-") > 0 or instr(intVal,".") > 0 then
				 IsInteger = 1
			Else
				 IsInteger = 0
			End if
	   Else
			IsInteger = 1
	   End If
	 
On error goto 0
End Function


'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :			  IsKey()
'	Purpose :				  Checks if the data passed in a Natural number.
'	Return Value :		 	  Integer(0/1/2)
'							  Return 0 : If the strkey passed is a proper key [valid key]
'							  Return 1 : If the strkey passed is a a zero length string [Invalid Key]
'							  Return 2 : If the strkey passed is not a proper key [Invalid Key]
'	Arguments :
'		Input Arguments :  	  strkey 
'		Output Arguments : 
'	Function is called by :	  	
'	Function calls :		  
'	Created on :			  17/06/2009
'	Author :				  Iohbha K
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function IsKey(byval strkey)
On error resume next
	'Variable Section Begin
		Dim dataType
	'Variable Section End
			
	dataType = Vartype(strkey)
	If  dataType = vbString then
		If trim(strkey) = "" Then
			IsKey = 1
		Else
			IsKey = 0
		End If
		
	else
		IsKey = 2
	End if
	
On error goto 0
End Function


'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :			  AddItemToStorageDictionary()
'	Purpose :				  Stores data against a unique, case-insensitive key in the oDictStorage dictionary.
'							  The data is prefix with escape char as required before pushing into the dictionary.
'	Return Value :		 	  Integer(0/1/2/3)
'							  Return 0 : If data is stored successfully in the dictionary
'							  Return 1 : If data is not stored successfully in the dictionary
'	Arguments :
'		Input Arguments :  	  strKey , strItem
'		Output Arguments : 
'	Function is called by :	  	
'	Function calls :		  
'	Created on :			  17/06/2009
'	Author :				  Iohbha K
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function AddItemToStorageDictionary( byval strKey , byval strItem)
On error resume next
	'Variable Section Begin
		'No Variable
	'Variable Section End
	strKey = Trim(Ucase(strKey))
	dotNetCom.AddKeyValue strKey, strItem 
	AddItemToStorageDictionary = 0
'	If oDictStorage.Exists(strKey) = true Then
'			oDictStorage.Item(strKey) = strItem					
'	Else
'			oDictStorage.Add strKey , strItem					
'	End if
'
'	If  oDictStorage.Exists(strKey) = true and err.number = 0 Then
'			' successfully added in dictionary
'			AddItemToStorageDictionary = 0
'	Else
'			' Not added into dictionary
'			AddItemToStorageDictionary = 1				
'	End If
		
On error goto 0
End Function


'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name :				CompareArraysCaseSensitive()
' 	Purpose :					    Compares Two Arrays. The data comparison is not case sensitive.
' 	Return Value :		 		  Boolean(0/1)
' 	Arguments :
'		Input Arguments :  	   arrExpData , arrActData
'		Output Arguments : 	  IntCount
'	Function is called by :		
'	Function calls :			   
'	Created on :				  13/03/2008
'	Author :						 Rathna Reddy
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function CompareArraysCaseSensitive(Byval arrExpData,ByVal arrActData)
   On Error Resume Next
	 'Variable Section Begin
		
			
			Dim intCount, strExpItem, intActItem,strExpectedResult,strActualResult, flag
			flag=-1
			intCount=0

		'Variable Section End
		strExpectedResult = PrintArrayElements (arrExpData)
		strActualResult = PrintArrayElements (arrActData)
		For Each strExpItem  in arrExpData 
				For intActItem = 0 to Ubound(arrActData)
				
					  If CompareStringValuesCaseSensitive(strExpItem,arrActData(intActItem))= 0 Then
						  Exit for
					  Elseif intActItem = Ubound(arrActData) Then
                                intCount = intCount+1
					End If
				Next
               If (intCount > 0)  Then 
				  flag=1	
			      Exit For
			   End If
				flag=0
		 Next
	If flag = 0 Then
		CompareArraysCaseSensitive = 0
        WriteStatusToLogs """"&"Expected Array Data : "&strExpectedResult& """" &  " contains " &""""&"Actual Array Data : "&strActualResult&""""
	Else
			CompareArraysCaseSensitive = 2
			WriteStatusToLogs """"&"Expected Array Data : "&strExpectedResult& """" &  " does not contain " &""""&"Actual Array Data : "&strActualResult&""""
	End If
	 
  On Error GoTo 0
End Function


'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :			  IsWholeNumber()
'	Purpose :				  Checks if the data passed in a whole number , ie, starting from 0 to any positive integer.
'	Return Value :		 	  Integer(0/1)
'	Arguments :
'		Input Arguments :  	  intVal 
'		Output Arguments : 
'	Function is called by :	  	
'	Function calls :		  
'	Created on :			  01/07/2009
'	Author :				  Iohbha K
'
Function IsWholeNumber(byval Val)
On error resume next
	'Variable Section Begin
		
		Dim regEx , blnMatch

	 'Variable Section End

	Set regEx = New RegExp         
	regEx.Pattern = "^[0-9]+$" 
	regEx.IgnoreCase = True         
	regEx.Global = True         
	blnMatch = regEx.Test(Val)   
	If blnMatch = True And Err.number=0 Then
		IsWholeNumber =0
	Else
		IsWholeNumber =1
	End if

On error goto 0
End Function

'------------------------------------------------------------------------------------------------------------------------------------------
'   Function Name :         FormatString()
'   Purpose :               Replaces all {n} values in a given string through the n-index field of an array.
'							Its just like the string.format method in .NET. so if you provide "my Name is {0}" as
'							input then the {0} will be replaced by the first field of the array. and so on.
'   Return Value :          [string] Formatted string with values replaced
'   Input Arguments :       str [string]: the source string
'							arrParams [array]: the array with values to be replaced
'------------------------------------------------------------------------------------------------------------------------------------------
Function FormatString(ByVal str, ByVal arrParams)
	On Error Resume Next
		Dim formatStr, nItems
		formatStr = str
		nItems = Ubound(arrParams)
		If Err.number = 0 Then
			for i = 0 to nItems
				formatStr = replace(formatStr, "{" & i & "}", cstr(arrParams(i)))
			next
		End If
		FormatString = formatStr		
	On Error GoTo 0
End Function


'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name :				CompareActualStringValues()
' 	Purpose :					Compares the two string. The comparison is not case sensitive. The method can also compare strings having a newline character.
' 	Return Value :		 		  intres
' 	Arguments :
'		Input Arguments :  	   strExpectedVal , strActualVal
'		Output Arguments : 	  int
'	Function is called by :		
'	Function calls :			   
'Instruction : The strcomp is binary comparision so it is case sensitive
'------------------------------------------------------------------------------------------------------------------------------------------


Function CompareActualStringValues(byval strExpectedVal, byval strActualVal)
   On Error Resume Next

   	'Variable Section Begin
		
		  	Dim intres , strModifiedExp , strModifiedAct

		 'Variable Section End
			strModifiedExp=replace(strExpectedVal,vbCrLf,"")
			strModifiedAct=replace(strActualVal,vbCrLf,"")

			intres = StrComp(strModifiedExp,strModifiedAct)
'			Select Case intres

'					Case 0
'							WriteStatusToLogs "Utility Name: " & "CompareStringValues"&Vbtab&_
'												"Message: "&""""&"Expected Result : "&strExpectedVal& """" &  "   and   " &""""&"Actual Result : "&strActualVal&""""&"  are equal."
											 
'					Case 1
'							WriteStatusToLogs "Utility Name: " & "CompareStringValues"&Vbtab&_
'											  "Message: "&""""&"Expected Result : "&strExpectedVal & """" &  "   and   " &""""&"Actual Result : "&strActualVal&""""&"  are not equal"
					
'					Case -1
'							WriteStatusToLogs  "Utility Name: " & "CompareStringValues"&Vbtab&_
'												"Message: "&""""&"Expected Result : "&strExpectedVal& """" &  "   and   " &""""&"Actual Result : " &strActualVal&""""&"  are not equal"
												   
'			End Select
			CompareActualStringValues = intres
  On Error GoTo 0
End Function



'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name :				MatchEachArrDataCS()
' 	Purpose :					Compares Two Arrays in such a manner that both the array's data should have	a one To one mapping with each other.The data comparison is  case sensitive.
' 	Return Value :		 		Boolean(0/1)
' 	Arguments :
'		Input Arguments :  	    arrExpData , arrActData
'		Output Arguments : 	    
'	Function is called by :		VerifyAllListItems()  
'	Function calls :			   

'------------------------------------------------------------------------------------------------------------------------------------------
Function MatchEachArrDataCS(Byval arrExpData,ByVal arrActData)
		
	On Error Resume Next
		'Variable Section Begin

		Dim lenActualArr,lenExpArr,strExpected,strActual,k,it,innerIt , strExpNotMatch ,strActNotMatch, strMessage
		Dim blnExpFlag , blnActFlag
		strMessage = "Message: "
		strExpNotMatch = " Following Expected data was not found in Actual array : "
		strActNotMatch = " Following Actual data was not found in Expected array : "

		'Variable Section End

		lenExpArr = Ubound(arrExpData)
		lenActualArr = Ubound(arrActData)
		
		If lenActualArr = lenExpArr Then
				  
					For it=0 to lenExpArr
							strExpected=arrExpData(it)
							For innerIt=0 to lenActualArr
										 strActual = arrActData(innerIt)
													
										If  CompareActualStringValues(strExpected,strActual) = 0 Then
										    arrExpData(it)=""
											arrActData(innerIt)=""
											Exit for
										End If
							Next
					Next

					For k=0 to lenExpArr
							If   arrExpData(k) <>"" Then
								strExpNotMatch=strExpNotMatch&arrExpData(k)&", "
								blnExpFlag=true
							End If

					Next

					For k=0 to lenActualArr
							If    arrActData(k) <>"" Then
								strActNotMatch=strActNotMatch&arrActData(k)&", "
								blnActFlag=True
							End If
					Next

					If blnExpFlag = True  Or blnActFlag = True Then

						If blnExpFlag = True Then
						    strMessage = strMessage & strExpNotMatch
						End If

						If blnActFlag = True Then
							strMessage = strMessage & strActNotMatch
						End If
						
						MatchEachArrDataCS = False
							
					Else 
						
						MatchEachArrDataCS = True
						strMessage = strMessage& " Both the specified arrays are equal."
							
					End If
					WriteStatusToLogs  "MatchEachArrData "&Vbtab&strMessage
					
		Else
			MatchEachArrDataCS = false
			WriteStatusToLogs  "The length of both the specified arrays are not equal."
		End If

	On Error GoTo 0

End Function




'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name :				CompareArraysInSequenceCS()
' 	Purpose :					    Compares Two Arrays . The data comparison is  case sensitive.
' 	Return Value :		 		  Boolean(0/1)
' 	Arguments :
'		Input Arguments :  	   arrExpData , arrActData
'		Output Arguments : 	  IntCount
'	Function is called by :		CompareTableColData() , CompareTableRowData() , VerifyListItems() 
  	   
'------------------------------------------------------------------------------------------------------------------------------------------
Function CompareArraysInSequenceCS(Byval arrExpData,ByVal arrActData)
   On Error Resume Next
		'Variable Section Begin
		
			
		Dim intCount, intItem,strExpectedResult,strActualResult
		intCount = 0
		
         'Variable Section End
		
		strExpectedResult =  "Expected Result= " & PrintArrayElements (arrExpData)&";"
		strActualResult = "Actual Result= "&PrintArrayElements (arrActData)&";"

        
		
		If Ubound(arrExpData) = Ubound(arrActData) Then

			For intItem = 0 to Ubound(arrExpData)
			   
					  If CompareActualStringValues(arrExpData(intitem),arrActData(intItem)) = 0 Then
						  intCount = 0
					  Else
						  intCount = intCount+1
                      End If
					If intCount > 0 Then
					 Exit For
					End If
			Next
			

			If intCount = 0 Then
					 CompareArraysInSequenceCS =0
					WriteStatusToLogs "The Data Sequence of both the specified arrays are same."
			Else
					 CompareArraysInSequenceCS = 1
					 WriteStatusToLogs "The expected and actual items does not match."
			End If
		Else
			CompareArraysInSequenceCS = 1
			WriteStatusToLogs "The expected and actual items count does not match."&Vbtab&_
										"Expected Size: " & Ubound(arrExpData)&Vbtab&_
										"Actual Size: " & Ubound(arrActData)

		End if
		WriteStatusToLogs  strExpectedResult
		WriteStatusToLogs strActualResult
  On Error GoTo 0
End Function



'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name :				VerifyDuplicateElementsInArrayCS()
' 	Purpose :					    Compares the Each element  with another element of array.
' 	Return Value :		 		  Boolean(0/1)
' 	Arguments :
'		Input Arguments :  	   arrActArray 
'		Output Arguments : 	  IntCount
'------------------------------------------------------------------------------------------------------------------------------------------
Function VerifyDuplicateElementsInArrayCS(ByVal arrActArray)
	On Error Resume Next
	
        'Variable Section Begin
			
			Dim countIDX,strActItem , intItem , intAheadItem , arrLen
		'Variable Section End
	
	countIDX=0
	arrLen=Ubound(arrActArray)
	For  intItem=0 to   arrLen
				strActItem=arrActArray(intItem)
				For  intAheadItem=intItem+1  to   arrLen
                  If  CompareActualStringValues(strActItem,arrActArray(intAheadItem)) = 0 Then
							   countIDX=countIDX+1 
							   Exit for
					End If
				Next
				If countIDX >0 then
					Exit for
				End If
	Next

	If countIDX > 0  Then
		VerifyDuplicateElementsInArrayCS = 1
		WriteStatusToLogs  "The specified Array contains duplicate elements."
	Else
		VerifyDuplicateElementsInArrayCS = 0
		WriteStatusToLogs  "The specified Array doesn't contain duplicate elements."
		
   End If
  On Error GoTo 0
End Function
'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name :				ConcateArrayItem()
' 	Purpose :					    Compares the Each element  with another element of array.
' 	Return Value :		 		  Boolean(0/1)
' 	Arguments :
'		Input Arguments :  	   arrActArray 
'		Output Arguments : 	  IntCount
'------------------------------------------------------------------------------------------------------------------------------------------
Function ConcateArrayItem(byval arrItems)
	On Error Resume Next
	Dim intarrLength,intIndex
	Dim concateString
	'concateString=""
		If isArray(arrItems) Then
			intarrLength=UBound(arrItems)
			For intIndex=0 To intarrLength
				If intIndex=0 Then
					concateString=arrItems(intIndex)
				Else		
					concateString=concateString&";"&arrItems(intIndex)
				End If 
			Next 
			ConcateArrayItem=concateString
		Else
			ConcateArrayItem=Null
		End If 
	On Error GoTo 0
End Function



'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name :				IsItemExistInArrayCS()
' 	Purpose :					   Checks the given item exists in the array
' 	Return Value :		 		  strData
' 	Arguments :
'		Input Arguments :  	   intValue

'------------------------------------------------------------------------------------------------------------------------------------------ 
Function IsItemExistInArrayCS(byval arrargListitems, byval argitem)
   On Error Resume Next

		Dim intcountelements
		Dim intItem
		Dim blnmatch
		Dim ss 
		blnmatch = False
		intcountelements = Ubound(arrargListitems)

   For intItem = 0 to intcountelements
	'	If arrargListitems(intItem) = argitem Then
	     If StrComp(arrargListitems(intItem), argitem ) = 0 Then 'CompareActualStringValues(arrargListitems(intItem), argitem ) Then
			blnmatch = True
		
			Exit For
	  
		End If
	Next


		If  blnmatch Then
			IsItemExistInArrayCS =true
			Else
			IsItemExistInArrayCS = False
		End If
On Error goto 0
   End Function
   
   
   
   '------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name :				CompareArraysCS()
' 	Purpose :					    Compares Two Arrays. The data comparison is  case sensitive.
' 	Return Value :		 		  Boolean(0/1)
' 	Arguments :
'		Input Arguments :  	   arrExpData , arrActData
'------------------------------------------------------------------------------------------------------------------------------------------
Function CompareArraysCS(Byval arrExpData,ByVal arrActData)
   On Error Resume Next
	 'Variable Section Begin
		
			
			Dim intCount, strExpItem, intActItem,strExpectedResult,strActualResult, flag
			flag=-1
			intCount=0

		'Variable Section End
		strExpectedResult = PrintArrayElements (arrExpData)
		strActualResult = PrintArrayElements (arrActData)
		For Each strExpItem  in arrExpData 
				For intActItem = 0 to Ubound(arrActData)
				
					  If CompareActualStringValues(strExpItem,arrActData(intActItem))= 0 Then
						  Exit for
					  Elseif intActItem = Ubound(arrActData) Then
                                intCount = intCount+1
					End If
				Next
               If (intCount > 0)  Then 
				  flag=1	
			      Exit For
			   End If
				flag=0
		 Next
	If flag = 0 Then
		CompareArraysCS = 0
	Else
		CompareArraysCS = 1
	End If
	 
  On Error GoTo 0
End Function

'------------------------------------------------------------------------------------------------------------------------------------------
Function IsBoolean(byval data)
   On error resume next
   Dim status , updatedData
   status = 1

   If Trim(Ucase(data)) = "TRUE" or  Trim(Ucase(data)) = "FALSE"  Then
		updatedData = CBool(Trim(data))
		If err.number = 0 Then
			status = 0
		else
			status = 1
		End If
	else	
		status = 1
	End If
	
   IsBoolean = status
   On error goto 0
End Function

''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'Sort two arrays
'Support data type: STRING,INTEGER
'Direction: Pass 1 or 2. 1 is ascending , 2 is descending
Function SortArray(ByVal arrData , ByVal dataType , ByVal direction)
	Dim min,max,arrValues,temp,value_j , i
	Dim sortedArrData
	sortedArrData = Null
	
	If CInt(direction) = 1 Then
		'Sort in ascending order
		intComp = -1
	ElseIf CInt(direction) = 2 Then
		'Sort in descending order
		intComp = 1
	End If
	
	If Trim(UCase(dataType)) = "STRING" Then
		localDataType = 0
	ElseIf Trim(UCase(dataType)) = "INTEGER" Then
		localDataType = 1
	End If

	
	arrValues = arrData
	min = lbound(arrValues)
	max = ubound(arrValues)
	For i = min To max - 1
		temp = arrValues(i)
		value_j = i
		For j = i + 1 To max
			select case localDataType
				case 0
					'STRING comparison
					' See if arrValues(j) is smaller. works with strings now.
					If strComp(arrValues(j),temp,vbTextCompare) = intComp Then
						' Save the new smallest temp.
						temp = arrValues(j)
						value_j = j
					End If
				case 1
					'Integer Comparison
					If CInt(direction) = 1 Then
						'Sort in ascending order
						If CInt(temp) > CInt(arrValues(j)) Then
							' Save the new smallest temp.
							temp = arrValues(j)
							value_j = j
						End If
					ElseIf CInt(direction) = 2 Then
						'Sort in descending order
						If CInt(temp) < CInt(arrValues(j)) Then
							' Save the new smallest temp.
							temp = arrValues(j)
							value_j = j
						End If
					End If
				'To be use if datatype comparison are Dates
'					if intComp = -1 then
'						if DateDiff("s",arrValues(j),temp) > 0 then
'							' Save the new smallest temp.
'							temp = arrValues(j)
'							value_j = j
'						end if
'					else
'						if DateDiff("s",arrValues(j),temp) < 0 then
'							' Save the new smallest temp.
'							temp = arrValues(j)
'							value_j = j
'						end if
'					end if
		   end select
		Next 'j
		If value_j <> i Then
				temp = arrValues(value_j)
				arrValues(value_j) = arrValues(i)
				arrValues(i) = temp
			
		End If
	Next 'i
	sortedArrData = arrValues
	SortArray = sortedArrData
End Function

Function CompareTabularData(byval table1, byval table2, byval matchType , byval caseSensitive , ByVal bTrim ,byval format)
   On error resume next
		Dim match , supportedFormatArray , statHM , i , supportedFormat , status , message , f
		Dim xmldoc , rootElement , row , col , trRow , statDictionary , tdCol
		Dim expected , actual , matchStat , caseSen , fail , total , k , j
		Dim regEx , blnRegExMatch , dimTable1 , dimTable2
		Dim data , expData , actualData
		Dim linkXML , resultXmlPath , VPArr , VPLinkArr , VPLink
		Dim oneD , twoD
		fail = 0
		total = 0
		matchStat= false
		message = ""
		match = ""
		status = "1"
		supportedFormat = false
		bTrim = cbool(bTrim) 

		Set statDictionary= createobject("Scripting.Dictionary")
		supportedFormatArray = Array("KSOExcel" , "MSExcel")

		If IsEmpty(format) Or IsNull(format) or Trim(format)="" Then
			status = "1"
			message = "Format is either null/empty, please verify. Following format are supported "
			for f=0 to Ubound(supportedFormatArray)-1
				message = message & " : " + supportedFormatArray(f)
			next
			statDictionary.add "status" ,status
			statDictionary.add "message", message
			set CompareTabularData = statDictionary
			Exit function
		End if
		
		matchType = Trim(Ucase(matchType))
		If IsEmpty(matchType) Or IsNull(matchType) or Trim(matchType)="" Then
			match = "ExactMatch"
		elseif matchType = "PARTIALMATCH" then
			match = "PartialMatch"
		Else
			match = "ExactMatch"
		End If

		For i=0 to Ubound(supportedFormatArray)
			If Trim(Ucase(format)) = Trim(Ucase(supportedFormatArray(i)))Then
				supportedFormat = true
				Exit for
			End If
		Next

		If supportedFormat<> true Then
			status = "1"
			message = "Format '"&format&"' is not supported.Following format are supported "
			for f=0 to Ubound(supportedFormatArray)-1
				message = message & " : " + supportedFormatArray(f)
			next
			statDictionary.add "status" ,status
			statDictionary.add "message", message
			set CompareTabularData = statDictionary
			Exit function
        End if

		dimTable1 = GetArrayDimension(table1)
		dimTable2 = GetArrayDimension(table2)
		If dimTable1 = 1 and dimTable2=1  Then
				oneD = true
				If  Ubound(Table1)<>Ubound(table2)Then
					status = "1"
					message = "The row count of the dataset does not match. Please verify."
					statDictionary.add "status" ,status
					statDictionary.add "message", message
					set CompareTabularData = statDictionary
					Exit function
				End if
		elseif dimTable1 = 2 and dimTable2=2 Then
				twoD = true
				If  Ubound(Table1)<>Ubound(table2)Then
					status = "1"
					message = "The row count of the dataset does not match. Please verify."
					statDictionary.add "status" ,status
					statDictionary.add "message", message
					set CompareTabularData = statDictionary
					Exit function
				else 
					If Ubound(Table1,2)<>Ubound(table2,2) then
						status = "1"
						message = "The column count of the dataset does not match. Please verify."
						statDictionary.add "status" ,status
						statDictionary.add "message", message
						set CompareTabularData = statDictionary
						Exit function
					End if
                End if
		else
				 status = "1"
				message = "Only 1-D and 2-D dataset are support. Please verify."
				statDictionary.add "status" ,status
				statDictionary.add "message", message
				set CompareTabularData = statDictionary
				Exit function
		End If
			
       Set xmldoc = CreateObject("Microsoft.XMLDOM")
		xmldoc.async = false
		xmldoc.loadXML("<?xml version='1.0'  encoding='UTF-8'?>")

		Set rootElement=xmldoc.createElement("table")  
		rootElement.setAttribute "VerificationType","StringVerification"
		rootElement.setAttribute "MatchType", match
		rootElement.setAttribute "CaseSensitiveMatch", caseSensitive

		If oneD = true Then
			For k =0 to Ubound(table1)
				Set trRow = xmldoc.createElement("tr")
				rootElement.appendChild(trRow)
				matchStat = false
				total =total+1
				Set tdCol = xmldoc.createElement("td")
				trRow.appendChild(tdCol)				
				expected = table1(k)
				actual = table2(k)

				If bTrim Then
					expected = Trim(expected)
					actual = Trim(actual)
				End If

				If Trim(Ucase(match))="PARTIALMATCH" Then
							Set regEx = New RegExp         
							regEx.Pattern = expected
							regEx.IgnoreCase = cbool(caseSensitive)         
							regEx.Global = True         
							blnRegExMatch = regEx.Test(actual)
							If  blnRegExMatch Then
								matchStat = true
							else
								matchStat = false
								fail =fail+1
							End If
						else
							If cbool(caseSensitive) = true Then
								caseSen = 0
							else
								caseSen = 1
							End If
							
							If strcomp(expected,actual,caseSen)=0 Then
								matchStat = true
							else
								matchStat = false
								fail =fail+1
							End If
						End If
						
						if matchStat then
								set  data = xmldoc.createElement("data")
								data.setAttribute "fontcolor", "black"
								data.setAttribute "type", ""
								data.appendChild(xmldoc.createTextNode(expected))
								tdCol.appendChild(data)
						  else
								
								set expData = xmldoc.createElement("data")
								expData.setAttribute "fontcolor", "green"
								expData.setAttribute "type", "expected"
								expData.appendChild(xmldoc.createTextNode(expected))
								tdCol.appendChild(expData)
								
								set actualData = xmldoc.createElement("data")
								actualData.setAttribute "fontcolor", "red"
								actualData.setAttribute "type", "actual"
								actualData.appendChild(xmldoc.createTextNode(actual))
								tdCol.appendChild(actualData)
						End if
           Next
		   
		Else
			matchStat = false
			' Changed by Ravi Nukala to Skip the header Row comparison. Did not replicate this change 
			' in 1D array condition as this will never be required in 1D array processing.
			' In fact 1D array condition will never ever occur as expected Col headers are mandatory and 1 row of data in 
			' file will mean no result to be compared which is absurd. Iohbha please confirm
			'For row=0 to Ubound(table1) 
			For row=1 to Ubound(table1) 
				Set trRow = xmldoc.createElement("tr")
				rootElement.appendChild(trRow)
				For col=0 to Ubound(table1,2)
					matchStat = false
					total =total+1
					Set tdCol = xmldoc.createElement("td")
					' added name attrbute with respective column header name to every td  *** Ravi Nukala
					tdCol.setAttribute "name", table1(0,col) 
					trRow.appendChild(tdCol)
					expected = table1(row,col)
					actual = table2(row,col)

					If bTrim Then
						expected = Trim(expected)
						actual = Trim(actual)
					End If

					If Trim(Ucase(match))="PARTIALMATCH" Then
						Set regEx = New RegExp         
						regEx.Pattern = expected
						regEx.IgnoreCase = cbool(caseSensitive)         
						regEx.Global = True         
						blnRegExMatch = regEx.Test(actual)
						If  blnRegExMatch Then
							matchStat = true
						else
							matchStat = false
							fail =fail+1
						End If
					else
						If cbool(caseSensitive) = true Then
							caseSen = 0
						else
							caseSen = 1
						End If
						
						If strcomp(expected,actual,caseSen)=0 Then
							matchStat = true
						else
							matchStat = false
							fail =fail+1
						End If
					End If
					
					if matchStat then
							set  data = xmldoc.createElement("data")
							data.setAttribute "fontcolor", "black"
							data.setAttribute "type", ""
							data.appendChild(xmldoc.createTextNode(expected))
							tdCol.appendChild(data)
					  else
							
							set expData = xmldoc.createElement("data")
							expData.setAttribute "fontcolor", "green"
							expData.setAttribute "type", "expected"
							expData.appendChild(xmldoc.createTextNode(expected))
							tdCol.appendChild(expData)
							
							set actualData = xmldoc.createElement("data")
							actualData.setAttribute "fontcolor", "red"
							actualData.setAttribute "type", "actual"
							actualData.appendChild(xmldoc.createTextNode(actual))
							tdCol.appendChild(actualData)
						End if
				Next
			Next
		end if
		xmldoc.appendChild(rootElement) 
		
		linkXML = "VP_"&GetResLongDate+".xml"
		linkXML = replace( linkXML , ":" , ".")
		VPLinkArr = split(linkXML , ".xml")
		VPLink = VPLinkArr(0)
		resultXmlPath = oDictLOGPATH.item(LOGPATH) & "\" &linkXML
		xmldoc.save(resultXmlPath)
		
		resultXmlPath = Replace(resultXmlPath ,"\","/")
		message = "</br><b>Settings:-</b> VerificationType: Format:"&format&" , String Verification , MatchType:"&match&" , CaseSensitiveMatch:"&caseSensitive
		message = message & "</br>Checked "&total&" cells. Passed:"&(total-fail)&" Failed:"&fail
		
		If fail<1 Then
			status ="0"
		else
			status = "2"
            message = message + "</br>Click the link to view the mismatch row(s) : <a id='"&VPLink&"' onClick=""window.parent.DatabaseVP('"&format&"','"&resultXmlPath&"')""><u>"&VPLink&"</u></a>"
        End If
	   
	    statDictionary.add "status",status
		statDictionary.add "message",message
				
		set CompareTabularData = statDictionary
   On error goto 0
End Function

Function GetArrayDimension(byval arr)
   On error resume next

   Dim i , dimension , dims
   
   'The maximum number dimensions allowed by VBScript is 32
   For i=1 to 32
		dimension = ubound(arr,i)
		If err.number=0 Then
			dims = i
        else
			Exit for
		End If
   Next
	GetArrayDimension = dims
	On error goto 0
End Function



' Function to write sql query results to a csv file
Function writeCSV(ByVal rs, ByVal strFile)
   Dim objFSO, objFolder, objShell, objTextFile, objFile
	Dim strDirectory, strText
	Dim fieldCount, tempStr, i
	Dim fld

	strDirectory = Mid(strFile, 1, InStrRev(strFile, "\")-1)
	''msgbox strDirectory
	strFile = Mid(strFile, InStrRev(strFile, "\")+1)
	''msgbox strFile

    'strDirectory = "d:\logs3"
	'strFile = "\"&strFile
	strText = "Book Another Holiday"
	
	' Create the File System Object
	Set objFSO = CreateObject("Scripting.FileSystemObject")
	
	' Check that the strDirectory folder exists
	If objFSO.FolderExists(strDirectory) Then
	   Set objFolder = objFSO.GetFolder(strDirectory)
	Else
	   Set objFolder = objFSO.CreateFolder(strDirectory)
	   'WScript.Echo "Just created " & strDirectory
	End If
	
	If objFSO.FileExists(strDirectory & "\" & strFile) Then
	   Set objFolder = objFSO.GetFolder(strDirectory)
	Else
	   Set objFile = objFSO.CreateTextFile(strDirectory & "\" & strFile)
	   'Wscript.Echo "Just created " & strDirectory & strFile
	End If
	
	set objFile = nothing
	set objFolder = nothing
	' OpenTextFile Method needs a Const value
	' ForAppending = 8 ForReading = 1, ForWriting = 2
	Const ForWriting = 2
	
	Set objTextFile = objFSO.OpenTextFile _
	(strDirectory & "\" & strFile, ForWriting, True)

	' To write column names in the result sheet
    For each fld in rs.Fields
		''msgbox fld.Name ' this will display the name of the field
        tempStr = tempStr&","&fld.Name
	Next
    tempStr = Mid(tempStr, 2) 
	objTextFile.WriteLine(tempStr)
	
	''msgbox "In function writeCSV"
	' Writes strText every time you run this VBScript
	Do while not rs.EOF
		fieldCount = rs.Fields.Count
		tempStr = ""&rs.Fields.Item(0)&""
		''msgbox rs.Fields.Count

		For i = 1 to fieldCount-1
			tempStr = tempStr&","&""&rs.Fields.Item(i)&""
		Next

		objTextFile.WriteLine(tempStr)
		rs.MoveNext
	Loop

	objTextFile.Close
	
'	' Bonus or cosmetic section to launch explorer to check file
'	If err.number = vbEmpty then
'	   Set objShell = CreateObject("WScript.Shell")
'	   objShell.run ("Explorer" &" " & strDirectory & "\" )
'	Else WScript.echo "VBScript Error: " & err.number
'	End If

End Function


' Function to convert csv file to a two dimensional array
Function ConvertCSVTo2DArray(CSVFilePath, numberOfRows, numberOfColoumns)
	On Error Resume next
   Dim data ' DYNAMIC ARRAY, REDIM LATER
   '''msgbox "ConvertCSVTo2DArray err = "&Err.description
   ' OPEN FILE
   Dim fs        'File System Object
   Dim file    'file Pointer Object
   Set fs = CreateObject("Scripting.FileSystemObject")
   Set file = fs.OpenTextFile(CSVFilePath, 1, 0)
   Dim str,i,j,ii,jj

   ' Determine array size required
   numberOfRows = 0
   numberOfColoumns = 0
   Dim line, lineSplitArray, lineColoumns
   Do While file.AtEndOfStream <> True
       line = file.readline
       numberOfRows = numberOfRows + 1
       lineSplitArray = Split(line, ",")
       lineColoumns = UBound(lineSplitArray) + 1
       ' CHECK IF GREATEST
       If ( lineColoumns > numberOfColoumns ) Then
           numberOfColoumns = lineColoumns
       End If
   Loop
'''msgbox "2"
   ' RESIZE ARRAY
   ReDim data( numberOfRows-1, numberOfColoumns-1)

   ' POPULATE
   Set file = fs.OpenTextFile(CSVFilePath, 1, 0)    ' REOPEN TO POINT TO START
   
   For ii = 0 To ( numberOfRows - 1 )
       line = file.readline
       lineSplitArray = Split(line, ",")
       lineColoumns = UBound(lineSplitArray)        
'        ''msgbox "Row " & ii & " data " & line
       For jj = 0 To lineColoumns
           data( ii, jj ) = lineSplitArray( jj )
'            ''msgbox "Coloumn " & jj & " data " & lineSplitArray( jj )
       Next
   Next

	' SET RETURN VALUE
    ConvertCSVTo2DArray = data
End Function

'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :			  IsPositiveInteger()
'	Purpose :				  Checks if the data passed is a number >0 and shud be a postive number and not a decimal no.
'	Return Value :		 	  Integer(0/1)
'	Arguments :
'		Input Arguments :  	  intVal 
'		Output Arguments : 
'	Function is called by :	  	
'	Function calls :		  
'	Created on :			  10/02/2012
'	Author :				  Iohbha K
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function IsPositiveInteger(byval intVal)
On error resume next
	'Variable Section Begin
		
		Dim blnStat

	 'Variable Section End

	   blnStat =  isnumeric(intVal)
	   If blnStat Then
			If instr(intVal,"-") > 0 or instr(intVal,".") > 0 then
				 IsPositiveInteger = 1
			Else
				 If CInt(intVal) > 0 Then
						 IsPositiveInteger = 0
				 Else
		 				 IsPositiveInteger = 1
				 End if
			End if
	   Else
			IsPositiveInteger = 1
	   End If
	 
On error goto 0
End Function


'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :			  IsItemExistInArray()
'	Purpose :				  Checks if the item exist in the array
'	Return Value :		 	  Boolean(true/false)
'	Arguments :
'		Input Arguments :  	  arrItems, strItem
'		Output Arguments : 
'	Function is called by :	  	
'	Function calls :		  
'	Created on :			  10/02/2012
'	Author :				  Iohbha K
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function IsItemExistInArray(ByVal arrItems, ByVal strItem)
On Error Resume Next
		Dim i , arrCount , blnFound
		blnFound = false

		arrCount = UBound(arrItems)
		If arrCount > -1 Then
				For i=0 To arrCount
						If CompareStringValues(arrItems(i),strItem) = 0 Then
								blnFound = true
								Exit for
						End if
				Next
				IsItemExistInArray = blnFound						
		Else
				IsItemExistInArray = false
		End if
		
On Error goto 0
End Function


'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :			  IsNumber()
'	Purpose :				  Checks if the data passed in a integer [negative, 0 , positive, no decimal] number.
'	Return Value :		 	  Integer(0/1)
'	Arguments :
'		Input Arguments :  	  intVal 
'		Output Arguments : 
'	Function is called by :	  	
'	Function calls :		  
'	Created on :			  31/08/2012
'	Author :				  Iohbha K
'
'------------------------------------------------------------------------------------------------------------------------------------------

Function IsNumber(byval Val)
On error resume next
	'Variable Section Begin
		
		Dim regEx , blnMatch

	 'Variable Section End

	Set regEx = New RegExp         
	regEx.Pattern = "^[-]*[0-9]+$" 'this pattern will match negative, 0 and positive number. It will not match decimals. 
	regEx.IgnoreCase = True         
	regEx.Global = True         
	blnMatch = regEx.Test(Val)   
	If blnMatch = True And Err.number=0 Then
		IsNumber =0
	Else
		IsNumber =1
	End if

On error goto 0
End Function
'------------------------------------------------------------------------------------------------------------------------------------------

Function WriteReportPath(ByRef ReportPath)
	Dim fso
	Dim reportfilepath, reportfile1
	Set fso = CreateObject("Scripting.FileSystemObject")

	reportfilepath = QualitiaFolderPath & "\" & QUALITIAREPORT '& "\QualitiaReport.properties"
	
	Set reportfile1 = fso.CreateTextFile(reportfilepath, True)
	reportfile1.WriteLine("qualitiareportpath="&ReportPath)
	
	reportfile1.Close
	Set reportfile1 = Nothing
	Set fso = nothing
	
	
End Function
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

Function SecureSet(ByVal objEditbox , ByVal strEncriptedValue)
	On Error Resume Next
		'Variable Section Begin
		''msgbox "Inside SecureSet"
		Dim strMethod
		Dim ObjSecure, strEncriptedValue1
		'Variable Section End
		Set ObjSecure = CreateObject("QualitiaCore._SecureTestData")
		strMethod = strKeywordForReporting
			   If CheckObjectExist(objEditbox) Then
					'If CheckObjectEnabled(objEditBox) Then
						'If CheckObjectVisible(objEditBox) Then 
							 objEditbox.Click
'							 
							 strEncriptedValue1 = ObjSecure.getEncryptedData(strEncriptedValue)
							 objEditbox.Set strEncriptedValue1
							 If err.Number = 0 or err.Number = 450 Then
									SecureSet = 0
									WriteStatusToLogs "The value: "&strEncriptedValue&" is set-secured in EditBox successfully."
							 Else
									SecureSet = 1
									WriteStatusToLogs "The value: "&strEncriptedValue&" is not set-secured in EditBox, please verify."
							  
							 End If
						'Else
							'SecureSet = 1
							'WriteStatusToLogs "Action: "&strMethod&vbtab&_
										'"Status:" &  "Failed" &Vbtab&_
										'"Message: "&"The EditBox is not Visible, Please verify."

						'End If
					'Else
					'SecureSet = 1
					'WriteStatusToLogs "Action: "&strMethod&vbtab&_
										'"Status:" &  "Failed" &Vbtab&_
										'"Message: "&"The EditBox is not Enabled, Please verify."


					'End If
				  
				Else
						SecureSet = 1
						WriteStatusToLogs "The EditBox does not exist, please verify."
				End If
		Set ObjSecure = Nothing
	On Error GoTo 0
End Function

'''''''''''''''''''''''''''''''''''''''''''''''''''''
 
Function IsPathAbsolute( testedPath)
	Dim oFSO
	set oFSO = CREATEOBJECT("Scripting.FileSystemObject")
	
	IsPathAbsolute = UCASE( testedPath) = UCASE( oFSO.GetAbsolutePathName(testedPath))

End Function

'''''''''''''''''''''''''''''''''''''''''''''''''''''
Function ExitVBScriptProcess
	logs.Debug "ExitVBScriptProcess is called. Execution will stop."
	ExitTest
End Function

Function ValidationForObject(oObject)
	On error resume next
	'TODO
	Dim strMethod
	strMethod = strKeywordForReporting
	If Not CheckObjectExist(oObject) Then
		ValidationForObject = False
			WriteStatusToLogs 	"The specified object does not exist, please verify."
			Exit Function
	Else 
		ValidationForObject = True
	End If
	On error goto 0
End Function

function ExecuteVBSStatement(oControl,strMethod, Action, sStrKeyword, sStrParam)
	On Error Resume Next	
	Dim i,j, tempParam,paramString
	Dim funcReturnVal, oName, arrayDictParameters
	Dim dictTemp
	Dim paramDataType, paramValue, CompleteparamString
	Dim StorageDictName
	Dim objParent
	Dim oRe, oMatches,Match,escapeCount, subS, oRegEx
	
	Set oRe = New RegExp
	oRe.IgnoreCase = True
	oRe.Global = False
	oRe.Pattern = "^~~*Enum::"
	
	If not (IsObject(oControl)) or IsEmpty(strMethod) or IsEmpty(Action) or IsEmpty(sStrKeyword) Then
		ExecuteVBSStatement = 1
		WriteStatusToLogs "Invalid parameters." 
		Exit function
	End If

	
	'Validations for object: exist, enable, visible
	If Not ValidationForObject(oControl) Then
		ExecuteVBSStatement =1
		WriteStatusToLogs "Object validation failed. Check if the specified object exists on application." 
		Exit Function
	End If
	'
	
	oName = oControl.toString
	

	If sStrParam <> "" Then
		sStrParam = Replace(sStrParam, """",",")
		
		arrayDictParameters = getParamArrayDictionary(sStrKeyword,sStrParam)
		
		For j=0 to ubound(arrayDictParameters)
			paramDataType = arrayDictParameters(j).Item("ParamType")
			paramValue = arrayDictParameters(j).Item("ParamValue")
			'-----------
			Set oMatches = oRe.Execute(paramValue)

			'-----------
			If oMatches.count >0 Then
			
				For each Match in oMatches
				      
					paramValue = replace(paramValue, "~", "~|")
					escapeCount =  split(paramValue,"|")
					
					escapeCount(0) = ""
					'escapeCount(1) = ""
					
					paramValue =  join(escapeCount, "")
					Exit for 
				Next
				CompleteparamString = paramValue
			
			ElseIf UCase(paramDataType) = "KEY" Then
				StorageDictName = paramValue
			Elseif  UCase(paramDataType) = "INTEGER" Then
				If CompleteparamString = "" Then
					CompleteparamString = paramValue & "þ"
					Else
'					CompleteparamString = CompleteparamString& "," & paramValue
					CompleteparamString = CompleteparamString & paramValue  & "þ"
				End If
			Elseif  UCase(paramDataType) = "BOOLEAN" Then
'				If IsBoolean(paramValue) <> 0 Then
'					ExecuteVBSStatement = 1
'					WriteStatusToLogs "Action: "&strMethod&Vbtab&_
'					"Status: Failed"&Vbtab&_
'					"Message: Invalid Datatype boolean." 
'					Exit function
'				End If
				If CompleteparamString = "" Then
'					CompleteparamString = cbool(paramValue) & "þ"
					CompleteparamString = paramValue & "þ"
					Else
'					CompleteparamString = CompleteparamString& "," & paramValue
'					CompleteparamString = CompleteparamString & cbool(paramValue)  & "þ"
					CompleteparamString = CompleteparamString & paramValue & "þ"
				End If
			Else

				If Instr(1, paramValue, "Enum::",0) > 0 Then
					 paramValue = replace(paramValue, "Enum::","")
					 CompleteparamString = CompleteparamString & paramValue & "þ"
				Else 
				If paramValue = "" Then
						 CompleteparamString = CompleteparamString & "þ"	
					Else
						 CompleteparamString = CompleteparamString & DoubleQuotes(paramValue) & "þ"				
				End If

				End if
			End If		
		Next
		
		If CompleteparamString <> "" Then
			
			subS = mid (CompleteparamString,len(CompleteparamString), 1)

			If subS = "þ" Then
				CompleteparamString =  mid(CompleteparamString, 1,len(CompleteparamString)-1)
			End If
			CompleteparamString = replace(CompleteparamString,"þ", ",")
		End If


	End If
	
	'--------------
	
	Set oRegEx = CreateObject("VBScript.RegExp")
	oRegEx.Global = True   
	oRegEx.IgnoreCase = True
	oRegEx.Pattern = ",+$"
	
	CompleteparamString = oRegEx.Replace(CompleteparamString, "")
	
	
	'--------------
	CompleteparamString = RemoveTrailingCharacters(CompleteparamString, ",")	
	
	if CompleteparamString="" then
		execute ("funcReturnVal = oControl." & Action)		
	else
		execute ("funcReturnVal = oControl." & Action  & "(" & CompleteparamString & ")") 
	End if
	
	
	If Environment.Value("ActionTest") = True Then
		ExecuteVBSStatement = err.number& "|" & err.description
		logs.Debug  err.number& "|" & err.description
		Exit function
	End If
	
	'Error handling
	If Err.number = 0 Then
		ExecuteVBSStatement = 0
		If isEmpty(funcReturnVal) Then
				WriteStatusToLogs "Action: '"&strMethod&"' is performed successfully on object '" & oName & "'." 
		Else
				AddItemToStorageDictionary StorageDictName , funcReturnVal
				WriteStatusToLogs "Action: '"&strMethod&"' is performed successfully on object '" & oName & "'. The value '"&funcReturnVal&"' is stored in key '" & StorageDictName &"'."
		End If

	Else
		ExecuteVBSStatement = 1
		
		If err.description <> "" Then
		WriteStatusToLogs "Keyword execution failed. Verify parameters. (" & err.description & ")"
		Else
		WriteStatusToLogs "Keyword execution failed. Verify parameters."		
		End If
	End If
	
	On error goto 0
End function



function ExecuteGenericActions (strMethod, Action, sStrKeyword, sStrParam)
	On Error Resume Next
	Dim i,j, tempParam,paramString
	Dim funcReturnVal, oName, arrayDictParameters
	Dim dictTemp
	Dim paramDataType, paramValue, CompleteparamString
	Dim StorageDictName
	Dim objParent
	Dim oRe, oMatches,Match,escapeCount, subS, oRegEx
	
	Set oRe = New RegExp
	oRe.IgnoreCase = True
	oRe.Global = False
	oRe.Pattern = "^~~*Enum::"
	

	If sStrParam <> "" Then
		sStrParam = Replace(sStrParam, """",",")
		arrayDictParameters = getParamArrayDictionary(sStrKeyword,sStrParam)
		
		For j=0 to ubound(arrayDictParameters)
			paramDataType = arrayDictParameters(j).Item("ParamType")
			paramValue = arrayDictParameters(j).Item("ParamValue")
			'-----------
			Set oMatches = oRe.Execute(paramValue)

			'-----------
			If oMatches.count >0 Then
			
				For each Match in oMatches
				      
					paramValue = replace(paramValue, "~", "~|")
					escapeCount =  split(paramValue,"|")
					
					escapeCount(0) = ""
					'escapeCount(1) = ""
					
					paramValue =  join(escapeCount, "")
					Exit for 
				Next
				CompleteparamString = paramValue
			
			ElseIf UCase(paramDataType) = "KEY" Then
				StorageDictName = paramValue
			Elseif  UCase(paramDataType) = "INTEGER" Then
				If CompleteparamString = "" Then
					CompleteparamString = paramValue & "þ"
					Else
'					CompleteparamString = CompleteparamString& "," & paramValue
					CompleteparamString = CompleteparamString & paramValue  & "þ"
				End If
			Elseif  UCase(paramDataType) = "BOOLEAN" Then
'				If IsBoolean(paramValue) <> 0 Then
'					ExecuteVBSStatement = 1
'					WriteStatusToLogs "Action: "&strMethod&Vbtab&_
'					"Status: Failed"&Vbtab&_
'					"Message: Invalid Datatype boolean." 
'					Exit function
'				End If
				If CompleteparamString = "" Then
'					CompleteparamString = cbool(paramValue) & "þ"
					CompleteparamString = paramValue & "þ"
					Else
'					CompleteparamString = CompleteparamString& "," & paramValue
'					CompleteparamString = CompleteparamString & cbool(paramValue)  & "þ"
					CompleteparamString = CompleteparamString & paramValue & "þ"
				End If
			Else

				If Instr(1, paramValue, "Enum::",0) > 0 Then
					 paramValue = replace(paramValue, "Enum::","")
					 CompleteparamString = CompleteparamString & paramValue & "þ"
				Else 
				If paramValue = "" Then
						 CompleteparamString = CompleteparamString & "þ"	
					Else
						 CompleteparamString = CompleteparamString & DoubleQuotes(paramValue) & "þ"				
				End If

				End if
			End If		
		Next
		
		If CompleteparamString <> "" Then
			
			subS = mid (CompleteparamString,len(CompleteparamString), 1)

			If subS = "þ" Then
				CompleteparamString =  mid(CompleteparamString, 1,len(CompleteparamString)-1)
			End If
			CompleteparamString = replace(CompleteparamString,"þ", ",")
		End If


	End If
	
	'--------------
	
	Set oRegEx = CreateObject("VBScript.RegExp")
	oRegEx.Global = True   
	oRegEx.IgnoreCase = True
	oRegEx.Pattern = ",+$"
	
	CompleteparamString = oRegEx.Replace(CompleteparamString, "")
	'--------------
	CompleteparamString = RemoveTrailingCharacters(CompleteparamString, ",")
	logs.Debug ("ExecuteGenericActions>>UFT API execution string :")	
	if CompleteparamString="" then
		execute ("funcReturnVal = " & Action)
		logs.Debug (Action)
	else
		execute ("funcReturnVal = " & Action  & "(" & CompleteparamString & ")") 
		logs.Debug (Action  & "(" & CompleteparamString & ")")

	End if
	
	If Environment.Value("ActionTest") = True Then
		ExecuteGenericActions = err.number& "|" & err.description
		logs.Debug  err.number& "|" & err.description
		Exit function
	End If
	'Error handling
	If Err.number = 0 Then
		ExecuteGenericActions = 0
		If isEmpty(funcReturnVal) Then
				WriteStatusToLogs "Action executed successfully." 
		Else
				AddItemToStorageDictionary StorageDictName , funcReturnVal
				WriteStatusToLogs "Action executed successfully. Execution storage key '" & StorageDictName & "' stores the value '"& funcReturnVal & "'."
		End If

	Else
		ExecuteGenericActions = 1
		
		If err.description <> "" Then
		WriteStatusToLogs "Action failed. Verify parameters. (" & err.description & ")"
		Else
		WriteStatusToLogs "Action failed. Verify parameters."		
		End If
	End If
	logs.Debug ("ExecuteGenericActions>>Execution finished.")
	On error goto 0
End function


Function getParamArrayDictionary(strKeywordID,sStrParamValues)
	
	Dim arrParams()
	Dim arrayStrParamValues
	Dim i, dictParams, Item, dictDataType, paramValue
	Dim oDictParamInnerDict
	
	If oDictKeywordParamCollection.Exists(strKeywordID) Then
		Set oDictParamInnerDict = oDictKeywordParamCollection.item(strKeywordID)
	Else
		Set oDictKeywordParamInnerDict = null
	End If
	
	arrayStrParamValues = split(sStrParamValues, ",")
	
	
	i=0
	ReDim arrParams(oDictParamInnerDict.count-1)
	
	'TODO: Check what happens when non-mandatory parameters are not provided by user.

	For i=0 to oDictParamInnerDict.count-1
		
		Set dictParams = CreateObject("Scripting.Dictionary")
		dictDataType = oDictParamInnerDict.Item(i+1)
		paramValue = arrayStrParamValues(i)
		dictParams.Add "ParamType", dictDataType
		dictParams.Add "ParamValue", paramValue
		'dictParams.Add "ParamName", paramName
		Set arrParams(i) = dictParams

	Next
	
	getParamArrayDictionary = arrParams
		
	
End Function


Function getQtpApiFromMap(sStrKeywordID)
	getQtpApiFromMap = oDictKeywordCollection.Item(sStrKeywordID)
End Function

Function RemoveTrailingCharacters(Str, TrailingChar)
Dim s, i, onechar, NewS
	s = StrReverse (Str)
	
	For i = 1 To len(s)
			onechar = mid(s,i,1)
			If onechar = TrailingChar Then
			
			Else
				Exit for 
			End If
	Next
	
	NewS = mid(s,i,len(s))
	RemoveTrailingCharacters = StrReverse(NewS)
End Function

Function DoubleQuotes(sStr)
	On error Resume Next
        If IsEmpty(sStr)  Then
	        WriteToErrorLog Err, "DoubleQuotes: String does not contain values."
			Err.Clear
        End If

        DoubleQuotes = """" & sStr & """"
	On error GoTo 0
End Function