
'=======================Code Section Begin=============================================

Option Explicit		'Forces explicit declaration of all variables in a script. 

'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name :				 SetValueInNetEditBox()
' 	Purpose :						 Enters a value into editbox
' 	Return Value :		 		   Integer( 0/1/2)
'		Input Arguments :  		objEditbox , strValue
'------------------------------------------------------------------------------------------------------------------------------------------
Function SetValueInNetEditBox(ByVal objEditbox , ByVal strValue)
	On Error Resume Next
		'Variable Section Begin
			   Dim customMsg
			   Dim intStatus
		'Variable Section End
			   If CheckObjectExist(objEditbox) Then
					If CheckObjectEnabled(objEditBox) Then
						objEditbox.Set strValue
						 If err.Number = 0 Then
							 intStatus = STATUS_PASSED
							customMsg =   FormatString(MSG_OPERATION_SUCCESSFUL,Array("Set Value","Edit Box"))
							'"The value was set in Edit Box successfully." 'MSG_OPERATION_SUCCESSFUL
							Else
								intStatus = STATUS_DEFECT
								customMsg =FormatString(MSG_OPERATION_UNSUCCESSFUL,Array("Set Value","Edit Box"))
								 '"The value was not set in Edit Box, Please verify."  'MSG_OPERATION_UNSUCCESSFUL
						  
							End If
					Else
					intStatus = STATUS_DEFECT
					customMsg = FormatString(MSG_OPERATION_FAILED_DISABLED,Array("Set Value","Edit Box"))
					'"The Edit Box was not enabled, Please verify." 'MSG_OPERATION_FAILED_DISABLED

					End If
				  
				Else
						intStatus = STATUS_FAILED
						customMsg =  FormatString(MSG_OBJECT_NOT_EXISTS,Array("Edit Box"))
						'"The specified Edit Box does not exist, Please verify."  'MSG_OBJECT_NOT_EXISTS
				End If
			 SetValueInNetEditBox =   intStatus
				    call WriteActionStatusToLog(intStatus, customMsg)
	On Error GoTo 0
End Function

'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name :				VerifyNetEditBoxValue()
' 	Purpose :					 Verifies the actual value of editbox with the expected value.
' 	Return Value :		 		  Integer( 0/1/2)
'		Input Arguments :  		objEditbox,strExpValue
'------------------------------------------------------------------------------------------------------------------------------------------
Function VerifyNetEditBoxValue(ByVal objEditbox , ByVal strExpValue)
	On Error Resume Next
		 'Variable Section Begin
			Dim strActValue
			Dim customMsg ,  intStatus

			
		'Variable Section End
		
			If CheckObjectExist(objEditbox) Then  
				strActValue = objEditbox.GetROProperty("text")
				
				If Vartype(strActValue) <> vbEmpty  And Err.Number = 0 Then
					If CompareActualStringValues(strExpValue,strActValue)  = 0 Then
						intStatus = STATUS_PASSED
					 customMsg = FormatString(MSG_TEXT_MATCHES,Array(strExpValue,strActValue))
				   '   "Expected Value : "& strExpValue & """" &  " and " &""""&"Actual Value : "& strActValue &""""&" matches as expected." 'MSG_TEXT_MATCHES
	   
					Else
						intStatus = STATUS_DEFECT
						   customMsg =FormatString(MSG_TEXT_NOT_MATCHES,Array(strExpValue,strActValue))
						   ' "Expected Value : "& strExpValue & """" &  " and " &""""&"Actual Value : "& strActValue &""""&" does not match, Please verify." 'MSG_TEXT_NOT_MATCHES
					End If
				Else
						intStatus = STATUS_FAILED
						customMsg = FormatString(MSG_ERR_FETCH_DATA , Array("Edit Box value"))
						'"An error occurred while capturing Edit Box value" 'MSG_ERR_FETCH_DATA
				End If
			Else
				intStatus = STATUS_FAILED
				customMsg =  FormatString(MSG_OBJECT_NOT_EXISTS,Array("Edit Box"))  ' "The specified Edit Box does not exist, Please verify."				
			End If
			VerifyNetEditBoxValue = intStatus
		     call WriteActionStatusToLog(intStatus, customMsg)
			'WriteStepResults CompareEditBoxValue
	On Error GOTO 0
End Function

'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name :				 VerifyNetEditBoxValueLength()
' 	Purpose :						Verifies the both minimum and maximum length of editbox Data
' 	Return Value :		 		  Integer( 0/1/2)
'		Input Arguments :  	    objEditbox , intMaxLength , intMinLength
'Note: if testcase does't specify the minimum length then Give "1"
'------------------------------------------------------------------------------------------------------------------------------------------
Function VerifyNetEditBoxValueLength(ByVal objEditbox, ByVal intMaxLength , ByVal intMinLength)
	On Error Resume Next
		 'Variable Section Begin
		
			Dim intLength, strValue,intMaxLen,intMinLen
			 Dim customMsg , intStatus
		'Variable Section End
			If CheckObjectExist(objEditbox) Then  
			intMaxLen=Cdbl(intMaxLength)
			intMinLen=Cdbl(intMinLength)

				   
				   If IsNumeric(Trim(intMaxLength))  Then
						  If IsNumeric(Trim(intMinLength))  Then
										 If intMinLen<intMaxLen Then
											strValue= objEditbox.GetROProperty("text")
														If err.Number = 0 Then
															 intLength = Len(strValue)
																	If  intLength <= intMaxLen And intLength >= intMinLen Then
																		intStatus =  STATUS_PASSED
																		customMsg =  FormatString(MSG_VALID_RANGE  , Array("Edit Box value length" , intLength ))
																	   '  "The Edit Box value length "&""""& intLength &""""&" is within the range of "&""""& intMaxLen &""""&" and " &""""& intMinLen &""""  'MSG_VALID_RANGE
																									
																	Else
																		 intStatus =STATUS_DEFECT
																		customMsg =  FormatString(MSG_INVALID_RANGE  , Array("Edit Box value length" , intLength))
																	  '   "The Edit Box value length "&""""&intLength&""""&" is not within the range of "&""""&intMaxLen&""""&" and " &""""&intMinLen&""""  'MSG_INVALID_RANGE
																	End If
															Else
																intStatus = STATUS_FAILED
																customMsg =  FormatString(MSG_ERR_FETCH_DATA , Array("Edit Box value"))

																  ' "An error occurred; Failed to retrieve Edit Box value, Please verify." 'MSG_ERR_FETCH_DATA
															End If
						
												Else
													intStatus = STATUS_DEFECT
													customMsg =   FormatString(MSG_INVALID_RANGE_SEQUENCE)
													'  "The Minimum length should be less than Maximum length, Please verify." 'MSG_INVALID_RANGE_SEQUENCE
												End If
								Else
									intStatus = STATUS_FAILED
									customMsg =  FormatString(MSG_ERR_NOT_NUMERIC , Array("Minimum Length"))
								   '  "An error occurred; The specified Minimum length is not a numeric value, Please verify." 'MSG_ERR_NOT_NUMERIC
								End If 

					Else
						intStatus = STATUS_FAILED
						  customMsg = FormatString(MSG_ERR_NOT_NUMERIC , Array("Maximum length"))
						'    "An error occurred; The specified Maximum length is not a numeric value, Please verify." 'MSG_ERR_NOT_NUMERIC
					End If 
            
			Else 'check object existance
				intStatus = STATUS_FAILED
			  customMsg =   FormatString(MSG_OBJECT_NOT_EXISTS,Array("Edit Box")) '"The specified Edit Box does not exist, Please verify."
								
		End If ' check object existance

		VerifyNetEditBoxValueLength = intStatus
	    call WriteActionStatusToLog(intStatus, customMsg)
			'WriteStepResults VerifyNetEditBoxValueLength
	On Error GoTo 0
End Function


'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name :				 VerifyNumericDataInNetEditBox()
' 	Purpose :						Verifies whether the editbox allows only numeric data.
' 	Return Value :		 		  Integer(0/1/2)
'	Input Arguments :  		objEditbox	

'------------------------------------------------------------------------------------------------------------------------------------------
Function VerifyNumericDataInNetEditBox(ByVal objEditbox)
	On Error Resume Next
		 'Variable Section Begin
		
			  Dim intValue, blnstatus
			  	 Dim customMsg , intStatus
           'Variable Section End
				 If CheckObjectExist(objEditbox) Then   
                    
						 intValue = objEditbox.GetRoProperty("text")
						 
						 If Vartype(intValue)<> Vbempty And Err.Number =0 Then
                              If intValue <> ""  Then
								 
													If IsNumeric(Trim(intValue)) Then
													
														 intStatus = STATUS_PASSED
														customMsg =    FormatString(MSG_CONTAINS_PASS,Array("Edit Box","numeric value"))
													  '  "The Edit Box contains numeric value as expected." 'MSG_CONTAINS_PASS
													 Else
														 intStatus = STATUS_DEFECT  
														customMsg =  FormatString(MSG_NOT_CONTAINS_FAIL,Array("Edit Box","numeric value"))

														 '"The Edit Box does not contain numeric value, Please verify." 'MSG_NOT_CONTAINS_FAIL
													 End If
									Else
											intStatus = STATUS_DEFECT
											customMsg =  FormatString(MSG_NOT_CONTAINS_FAIL,Array("Edit Box","numeric value"))

											'"The Edit Box does not contain any value, Please verify."
									End IF
					Else
						intStatus = STATUS_FAILED
						 customMsg = FormatString(MSG_ERR_FETCH_DATA  , Array("Edit Box value"))
						'  "An error occurred; Failed to retrieve Edit Box value, Please verify." 'MSG_ERR_FETCH_DATA
					End if								 
         Else
					intStatus = STATUS_FAILED
					customMsg =    FormatString(MSG_OBJECT_NOT_EXISTS,Array("Edit Box"))         '    "The specified Edit Box does not exist, Please verify."
				End If
					VerifyNumericDataInNetEditBox = intStatus
				    call WriteActionStatusToLog(intStatus, customMsg)
	On Error GoTo 0
End Function



'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name :				 VerifyStringDataInNetEditBox()
' 	Purpose :						Verifies whether the editbox allows only alphanumeric data.
' 	Return Value :		 		  Integer(0/1/2)
'	Input Arguments :  		objEditbox	

'------------------------------------------------------------------------------------------------------------------------------------------


Function VerifyStringDataInNetEditBox(ByVal objEditbox)
	On Error Resume Next
		 'Variable Section Begin
		
			  Dim intValue, blnstatus
			  	 Dim customMsg , intStatus
           'Variable Section End
				 If CheckObjectExist(objEditbox) Then   
                    
						 intValue = objEditbox.GetRoProperty("text")
						 
						 If Vartype(intValue)<> Vbempty And Err.Number =0 Then
                              If intValue <> ""  Then
								 
													If IsNumeric(Trim(intValue)) Then
													 	 intStatus = STATUS_DEFECT  
														customMsg =  FormatString(MSG_NOT_CONTAINS_FAIL,Array("Edit Box","alphanumeric  value"))
													  '  "The Edit Box contains numeric value as expected." 'MSG_CONTAINS_PASS
													 Else
                                                        intStatus = STATUS_PASSED
														customMsg =    FormatString(MSG_CONTAINS_PASS,Array("Edit Box","alphanumeric  value"))
														 '"The Edit Box does not contain numeric value, Please verify." 'MSG_NOT_CONTAINS_FAIL
													 End If
									Else
											intStatus = STATUS_DEFECT
											customMsg =  FormatString(MSG_NOT_CONTAINS_FAIL,Array("Edit Box","alphanumeric  value"))

											'"The Edit Box does not contain any value, Please verify."
									End IF
					Else
						intStatus = STATUS_FAILED
						 customMsg = FormatString(MSG_ERR_FETCH_DATA  , Array("Edit Box value"))
						'  "An error occurred; Failed to retrieve Edit Box value, Please verify." 'MSG_ERR_FETCH_DATA
					End if								 
         Else
					intStatus = STATUS_FAILED
					customMsg =    FormatString(MSG_OBJECT_NOT_EXISTS,Array("Edit Box"))         '    "The specified Edit Box does not exist, Please verify."
				End If
					VerifyStringDataInNetEditBox = intStatus
				    call WriteActionStatusToLog(intStatus, customMsg)
	On Error GoTo 0
End Function



''------------------------------------------------------------------------------------------------------------------------------------------
'' 	Function Name :				VerifyNetEditBoxValue()
'' 	Purpose :					 Verifies the editbox value with our expected whenever required
'' 	Return Value :		 		  Integer( 0/1/2)
''		Input Arguments :  		objEditbox,strExpValue
''------------------------------------------------------------------------------------------------------------------------------------------
'Function CompareEditBoxDefaultValue(ByVal objEditbox , ByVal strExpValue)
'	On Error Resume Next
'	 	Dim strActValue 
'		Dim customMsg
'		'Variable Section End
'			If CheckObjectExist(objEditbox) Then  
'				strActValue = objEditbox.GetROProperty("text")
'				
'				If Vartype(strActValue) <> vbEmpty  And Err.Number = 0 Then
'						If CompareActualStringValues(strExpValue,strActValue)  = 0 Then
'									VerifyNetEditBoxValue = STATUS_PASSED
'								 customMsg = "Expected Value : "&strExpValue& """" &  " and " &""""&"Actual Value : "&strActValue&""""&" matches as expected." 'MSG_TEXT_MATCHES
'				   
'								Else
'									VerifyNetEditBoxValue = STATUS_DEFECT
' 									   customMsg = "Expected Value : "&strExpValue& """" &  " and " &""""&"Actual Value : "&strActValue&""""&" does not match, Please verify." 'MSG_TEXT_NOT_MATCHES
'								End If
'					Else
'							VerifyNetEditBoxValue = STATUS_FAILED
'							customMsg = "An error occurred while capturing edit box value" 'MSG_ERR_FETCH_DATA
'					End If
'		Else
'								VerifyNetEditBoxValue = STATUS_FAILED
'							   	customMsg = "The specified Edit Box does not exist, Please verify."
'				
'		 End If
'		     call WriteActionStatusToLog(CompareEditBoxDefaultValue, customMsg)
'			'WriteStepResults CompareEditBoxValue
'	On Error GoTo 0
'			End Function
''------------------------------------------------------------------------------------------------------------------------------------------
'' 	Function Name :				CompareNetEditBoxDefaultValue()
'' 	Purpose :					 Verifies the editbox value with our expected whenever required
'' 	Return Value :		 		  Integer( 0/1/2)
''		Input Arguments :  		objEditbox,strExpValue
''------------------------------------------------------------------------------------------------------------------------------------------
Function CompareNetEditBoxDefaultValue(ByVal objEditbox , ByVal strExpValue)
	On Error Resume Next
	 	Dim strActValue 
		Dim customMsg,intStatus
		'Variable Section End
			If CheckObjectExist(objEditbox) Then  
				strActValue = objEditbox.GetROProperty("text")
				
				If Vartype(strActValue) <> vbEmpty  And Err.Number = 0 Then
						If CompareActualStringValues(strExpValue,strActValue)  = 0 Then
									intStatus = STATUS_PASSED
								 customMsg = "Expected Value : "&strExpValue& """" &  " and " &""""&"Actual Value : "&strActValue&""""&" matches as expected." 'MSG_TEXT_MATCHES
				   
								Else
									intStatus = STATUS_DEFECT
 									   customMsg = "Expected Value : "&strExpValue& """" &  " and " &""""&"Actual Value : "&strActValue&""""&" does not match, Please verify." 'MSG_TEXT_NOT_MATCHES
								End If
					Else
							intStatus = STATUS_FAILED
							customMsg = "An error occurred while capturing edit box value" 'MSG_ERR_FETCH_DATA
					End If
		Else
								intStatus = STATUS_FAILED
							   	customMsg = "The specified Edit Box does not exist, Please verify."
				
		 End If
	CompareNetEditBoxDefaultValue = intStatus
		     call WriteActionStatusToLog(intStatus, customMsg)
			'WriteStepResults CompareEditBoxValue
	On Error GoTo 0
End Function
'=======================Code Section End=============================================
