'=======================Code Section Begin=============================================
Option Explicit 

'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name :				 CollapseTreeViewNode()
' 	Purpose :					 Verifies  all the list items with our expected
' 	Return Value :		 		   Integer( 01)
'		Input Arguments :  	  	objNetTree,strExpItems
'------------------------------------------------------------------------------------------------------------------------------------------
Function CollapseNetTreeViewNode(ByVal objNetTree , ByVal strTreeItems)
	On Error Resume Next
		'Variable Section Begin
			Dim blnstatus, blnMatch
			Dim strListContent, strTreelistItems
			Dim arrItems, arrDropItems, arrListContents
			Dim intmaxlength, intcount
			Dim arrTreeListItems
			Dim intTemparrcount, intTreeItems

				Dim customMsg, intStatus
				
			strListContent = Null
			blnstatus = false
			blnMatch = False
			'blnDropMatch = False
		'Variable Section End
			If CheckObjectExist(objNetTree) Then
				strListContent = objNetTree.GetContent

				If Vartype(strListContent) <> vbNull  And Err.Number = 0 Then

						If IsArray(strTreeItems) Then
							strTreelistItems = ConcateArrayItem(strTreeItems)
						 else
								strTreelistItems = strTreeItems
						End If


'                   'arrItems = GetArray(strTreeItems, "^")
'				   arrItems = strTreeItems
'					intTreeItems = Ubound(arrItems)
'
'					arrListContents = GetArray(strListContent, chr(10))
'				
'					intmaxlength = ubound(arrListContents)
					
'					For intcount = 0 to intmaxlength
'							strTreelistItems = arrListContents(intcount)
'							arrTreeListItems = GetArray(strTreelistItems, ";")
'							  intTemparrcount = Ubound(arrTreeListItems)
'								If intTemparrcount = intTreeItems Then
'									 If CompareArraysInSequenceCS(arrItems, arrTreeListItems) = 0 Then
'										  blnMatch = true
'										 Exit for
'									End If
'								End If
'					Next

									'  If  blnMatch Then

															objNetTree.Collapse  strTreelistItems
																If err.Number = 0 Then
																	intStatus = STATUS_PASSED
																	customMsg = FormatString(MSG_OPERATION_SUCCESSFUL,Array("Collapse","Tree View"))
																	'customMsg = "Collapse."
																		
																		Else
																    intStatus = STATUS_DEFECT
																	customMsg = FormatString(MSG_OPERATION_UNSUCCESSFUL,Array("Collapse","Tree View"))
																	 'customMsg = "Unable to Collapse."
																		End If
			
'											Else'blnMatch
'											intStatus = STATUS_FAILED
'										    'customMsg =  "The  Items are not existing in the Tree."
'											customMsg = FormatString(MSG_NOT_CONTAINS_FAIL,Array("Tree View", "specified node"))
'										End If   		   
						Else'blnstatus
						intStatus =STATUS_FAILED
					     'customMsg =  "An error occurred while Getting the Tree List Nodes."
						 customMsg = FormatString(MSG_ERR_FETCH_DATA,Array("Tree View nodes"))
						End If
			Else
				intStatus = STATUS_FAILED
			    'customMsg = " The Tree View Object  does not exist, Please verify."
				customMsg = FormatString(MSG_OBJECT_NOT_EXISTS,Array("Tree View"))
			End If
				CollapseNetTreeViewNode = intStatus
			  call WriteActionStatusToLog(intStatus, customMsg)
		On Error GoTo 0
End Function

'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name :				 ExpandNetTreeViewNode()
' 	Purpose :					 Verifies  all the list items with our expected
' 	Return Value :		 		   Integer( 01)
'		Input Arguments :  	  	objNetTree,strExpItems
'------------------------------------------------------------------------------------------------------------------------------------------
Function ExpandNetTreeViewNode(ByVal objNetTree , ByVal strTreeItems)
	On Error Resume Next
		'Variable Section Begin
			Dim  blnMatch
			Dim strListContent, strTreelistItems
			Dim arrItems, arrDropItems, arrListContents
			Dim intmaxlength, intcount
			Dim arrTreeListItems
			Dim intTemparrcount, intTreeItems

			Dim customMsg , intStatus

			strListContent = Null
			blnMatch = False
			
			'blnDropMatch = False
	
				
		'Variable Section End
			If CheckObjectExist(objNetTree) Then
				strListContent = objNetTree.GetContent

				If Vartype(strListContent) <> vbNull  And Err.Number = 0 Then
					
					   If IsArray(strTreeItems) Then
							strTreelistItems = ConcateArrayItem(strTreeItems)
						 else
								strTreelistItems = strTreeItems
						 End If
					'MsgBox strTreelistItems
                   'arrItems = GetArray( strTreeItems, "^")
				   'arrItems = strTreeItems
				  '  intTreeItems = Ubound(arrItems)
				'	MsgBox intTreeItems

'					arrListContents = GetArray(strListContent, chr(10))
'					intmaxlength = ubound(arrListContents)
'					
'					For intcount = 0 to intmaxlength
'							strTreelistItems = arrListContents(intcount)
'							arrTreeListItems = GetArray(strTreelistItems, ";")
'							  intTemparrcount = Ubound(arrTreeListItems)
'								If intTemparrcount = intTreeItems Then
'									 If CompareArraysInSequenceCS(arrItems, arrTreeListItems) = 0 Then
'										  blnMatch = true
'										 Exit for
'									End If
'								End If
'					Next

'									  If  blnMatch Then
											objNetTree.Expand  strTreelistItems
												If err.Number = 0 Then
													intStatus = STATUS_PASSED
													customMsg = FormatString(MSG_OPERATION_SUCCESSFUL,Array("Expand","Tree View"))
													'customMsg =  "Expand."
												Else
													intStatus = STATUS_DEFECT
													customMsg = FormatString(MSG_OPERATION_UNSUCCESSFUL,Array("Expand","Tree View"))
													'customMsg =  "Unable to Expand."
												End If
												
'										Else'blnMatch
'											intStatus =STATUS_FAILED
'											'customMsg = "The Expand Items are not existing."
'											customMsg = FormatString(MSG_NOT_CONTAINS_FAIL,Array("Tree View", "specified node"))
'										End If   		   
						Else
						intStatus = STATUS_FAILED
					   customMsg = FormatString(MSG_ERR_FETCH_DATA,Array("Tree View nodes"))
						End If
			Else
				intStatus = STATUS_FAILED
			  customMsg = FormatString(MSG_OBJECT_NOT_EXISTS,Array("Tree View"))
			End If
       ExpandNetTreeViewNode = intStatus
			 call WriteActionStatusToLog(ExpandTreeViewNode, customMsg)
		On Error GoTo 0
End Function




'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name :				  VerifyNodeByIndex()
' 	Purpose :						 Verify node by  index
' 	Return Value :		 		   Integer( 0/1/2)
'		Input Arguments :  		objNetTree , intIndex, strExpNode
'------------------------------------------------------------------------------------------------------------------------------------------

Function VerifyNodeByIndex(ByVal objNetTree, ByVal intIndex, ByVal strExpNode)
    On Error Resume Next
    Dim actNode
    Dim intGetIndex
	Dim customMsg , intStatus
	intGetIndex =   intIndex
    If CheckObjectExist(objNetTree) Then
        intGetIndex = CInt(intIndex) 
            If Err.Number = 0 Then
                actNode = objNetTree.GetItem(intGetIndex)
                If Vartype(actNode) <> vbNull And Err.Number = 0 Then
                    If Vartype(strExpNode) <> vbNull And Err.Number = 0 Then
						If CompareActualStringValues(strExpNode, actNode) = 0 Then
                            intStatus = STATUS_PASSED
                            'customMsg = "The specified Tree view node is same as the expected node."
							customMsg = FormatString(MSG_ITEMS_MATCHES,Array("Tree View node","node"))
                        Else
                            intStatus = STATUS_DEFECT
                            'customMsg = "The specified Tree view node does not same as the expected node."
							customMsg = FormatString(MSG_ITEMS_NOT_MATCHES,Array("Tree View node","node"))
                        End If
                    Else
                        intStatus = STATUS_FAILED
                        'customMsg = "Action Fails to get Tree view node, Please Verify node and its index"
						customMsg = FormatString(MSG_ERR_NOT_STRING,Array("expected node"))
                    End If
                Else
                    intStatus = STATUS_FAILED
                    'customMsg = "Action Fails to get Tree view node, Please Verify node and its index"
					customMsg = FormatString(MSG_ERR_FETCH_DATA,Array("Tre View node"))
                End If
            Else
                intStatus = STATUS_FAILED
                'customMsg = "Action Fails to get Tree view node, Please Verify node and its index"
				customMsg = FormatString(MSG_ERR_NOT_NUMERIC,Array("index"))
            End If
    Else
        intStatus = STATUS_FAILED
        'customMsg = "Tree View does not exist"
		customMsg = FormatString(MSG_OBJECT_NOT_EXISTS,Array("Tree View"))
    End If
 VerifyNodeByIndex =   intStatus
 call WriteActionStatusToLog(intStatus, customMsg)
    On Error GoTo 0
End Function


'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name :				  SelectSingleNetTreeViewNode()
' 	Purpose :						 Selects a specified node from the TreeView
' 	Return Value :		 		   Integer( 0/1/2)
'		Input Arguments :  		objNetTree , strSelectItem
'------------------------------------------------------------------------------------------------------------------------------------------

Function SelectSingleNetTreeViewNode(ByVal objNetTree, ByVal strTreeItems)
    On Error Resume Next
    'Variable Section Begin
    Dim customMsg , intStatus , strTreelistItems , strListContent


    If CheckObjectExist(objNetTree) Then
        strListContent = objNetTree.GetContent
		'MsgBox strListContent
		
        If Vartype(strListContent) <> vbNull And Err.Number = 0 Then		
				
						If IsArray(strTreeItems) Then
							strTreelistItems = ConcateArrayItem(strTreeItems)
						 else
								strTreelistItems = strTreeItems
						End If
		objNetTree.Select(strTreelistItems)
            If err.Number = 0 Then
                intStatus = STATUS_PASSED
				customMsg = FormatString(MSG_OPERATION_SUCCESSFUL,Array("Select","Tree View"))
				   'customMsg = "Tree node   select ."
            Else
                intStatus = STATUS_DEFECT
				customMsg = FormatString(MSG_OPERATION_UNSUCCESSFUL,Array("Select","Tree View"))
                'customMsg = "Tree node does not select ."
            End If

        Else
            intStatus = STATUS_FAILED
            'customMsg("An error occurred while selecting the Tree List Nodes.")
			customMsg = FormatString(MSG_ERR_FETCH_DATA,Array("Tree View nodes"))
        End If
    Else
        intStatus = STATUS_FAILED
        'customMsg = " The Tree View Object  does not exist, Please verify."
		customMsg = FormatString(MSG_OBJECT_NOT_EXISTS,Array("Tree View"))
    End If
      SelectSingleNetTreeViewNode = intStatus
    call WriteActionStatusToLog(intStatus, customMsg)
    On Error GoTo 0
End Function


'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name :				  VerifyTreeCheckBoxesVisible()
' 	Purpose :						 Verify treeview has checkboxes
' 	Return Value :		 		   Integer( 0/1/2)
'		Input Arguments :  		objNetTree
'------------------------------------------------------------------------------------------------------------------------------------------
Function VerifyTreeCheckBoxesVisible(ByVal objNetTree)
    On Error Resume Next
    Dim actNode
    'Dim intGetIndex
    Dim customMsg , intStatus
    Dim strChecked
    intGetIndex = intIndex
    If CheckObjectExist(objNetTree) Then
        strChecked = objNetTree.GetROProperty("CheckBoxes")
        If strChecked = True Then
            intStatus = STATUS_PASSED
            'customMsg = "The Tree view contains checkboxes"
			customMsg = FormatString(MSG_VISIBLE_PASS,Array("check boxes"))
        Else
            intStatus = STATUS_DEFECT
            'customMsg = "The Tree view does not  checkboxes"
			customMsg = FormatString(MSG_NOT_VISIBLE_FAIL,Array("check boxes"))
        End If
    Else
        intStatus = STATUS_FAILED
        'customMsg = "Tree View does not exist"
		customMsg = FormatString(MSG_OBJECT_NOT_EXISTS,Array("Tree View"))
    End If

	 VerifyTreeCheckBoxesVisible = intStatus
    Call WriteActionStatusToLog(intStatus, customMsg)
    On Error GoTo 0

End Function



Function VerifyTreeCheckBoxesInvisible(ByVal objNetTree)
    On Error Resume Next

    Dim customMsg , intStatus
    Dim strChecked
    If CheckObjectExist(objNetTree) Then
        strChecked = objNetTree.GetROProperty("CheckBoxes")
        If strChecked = False Then
            intStatus = STATUS_PASSED
            'customMsg = "The Tree view  checkboxes are not visible"
			customMsg = FormatString(MSG_NOT_VISIBLE_PASS,Array("check boxes"))
        Else
            intStatus = STATUS_DEFECT
            'customMsg = "The Tree view  checkboxes are  visible"
			customMsg = FormatString(MSG_VISIBLE_FAIL,Array("check boxes"))
        End If
    Else
        intStatus = STATUS_FAILED
        'customMsg = "Tree View does not exist"
		customMsg = FormatString(MSG_OBJECT_NOT_EXISTS,Array("Tree View"))
    End If
  VerifyTreeCheckBoxesInvisible =  intStatus
    Call WriteActionStatusToLog(intStatus, customMsg)
    On Error GoTo 0

End Function








Function CheckNetTreeViewNode(ByVal objNetTree, ByVal strTreeItems)
    On Error Resume Next
    'Variable Section Begin
    Dim customMsg, intStatus , strTreelistItems
    'Variable Section End
    If CheckObjectExist(objNetTree) Then
        If err.Number = 0 Then
           If Vartype(strTreeItems) <> vbNull And Err.Number = 0 Then
							If IsArray(strTreeItems) Then
							strTreelistItems = ConcateArrayItem(strTreeItems)
						 else
								strTreelistItems = strTreeItems
						End If
               ' strTreeItems = Replace(strTreeItems, "^", ";")
                objNetTree.setItemState strTreelistItems, micChecked
                If err.Number = 0 Then
                    intStatus = STATUS_PASSED
					customMsg = FormatString(MSG_OPERATION_SUCCESSFUL,Array("check","Tree View"))
                Else
                    intStatus = STATUS_DEFECT
          customMsg = FormatString(MSG_OPERATION_UNSUCCESSFUL,Array("check","Tree View"))
                End If
            Else
                intStatus = STATUS_FAILED
                customMsg = "fail"
            End If

        Else
            intStatus = STATUS_FAILED
           	customMsg = FormatString(MSG_OBJECT_NOT_EXISTS,Array("Tree View"))
        End If
    End If

    CheckNetTreeViewNode = intStatus
   Call WriteActionStatusToLog(intStatus, customMsg)



    On Error GoTo 0
End Function



Function UncheckNetTreeViewNode(ByVal objNetTree, ByVal strTreeItems)
    On Error Resume Next
    'Variable Section Begin
    Dim customMsg, intStatus , strTreelistItems
    'Variable Section End
    If CheckObjectExist(objNetTree) Then
        If err.Number = 0 Then
           If Vartype(strTreeItems) <> vbNull And Err.Number = 0 Then
                'strTreeItems = Replace(strTreeItems, "^", ";")
				If IsArray(strTreeItems) Then
							strTreelistItems = ConcateArrayItem(strTreeItems)
						 else
								strTreelistItems = strTreeItems
						End If
                objNetTree.setItemState strTreelistItems, micUnchecked
                If err.Number = 0 Then
                    intStatus = STATUS_PASSED
				    customMsg = FormatString(MSG_OPERATION_SUCCESSFUL,Array("check","Tree View"))
                Else
                    intStatus = STATUS_DEFECT
				  customMsg = FormatString(MSG_OPERATION_UNSUCCESSFUL,Array("check","Tree View"))
                End If
            Else
                intStatus = STATUS_FAILED
                customMsg = FormatString(MSG_NOT_CONTAINS_FAIL,Array("Tree View", "specified node"))
            End If

        Else
            intStatus = STATUS_FAILED
          	customMsg = FormatString(MSG_OBJECT_NOT_EXISTS,Array("Tree View"))
        End If
    End If

    UncheckNetTreeViewNode = intStatus
     Call WriteActionStatusToLog(intStatus, customMsg)



    On Error GoTo 0
End Function






'=======================Code Section End=============================================
