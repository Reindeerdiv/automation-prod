Option Explicit 

'
'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name :				  SelectNetListBoxItem()
' 	Purpose :						 Selects a specified item from the ListBox
' 	Return Value :		 		   Integer( 0/1/2)
' 	Arguments :
'		Input Arguments :  		objListBox , strSelectItem

'------------------------------------------------------------------------------------------------------------------------------------------
Function SelectNetListBoxItem(ByVal objListBox , ByVal strSelectItem)
	On Error Resume Next
	'Variable Section Begin
	
	Dim strActItem, strAllitems , customMsg , SelectListItem
	Dim arritems 
	Dim blnstatus 
	blnstatus  = False
	
	'Variable Section End
	If CheckObjectExist(objListBox) Then
		If CheckObjectEnabled(objListBox) Then
			strallitems = objListBox.GetRoProperty("all items")
			
			If  VarType(strallitems) <> vbEmpty   Then
				arritems = Split(strAllitems, Chr(10))
				Dim arritemsdd 
				arritemsdd = UBound(arritems)
				If IsItemExistInArrayCS(arritems , strSelectItem)  Then					
					objListBox.Select strSelectItem	 	
					If Err.number = 0 Then
						strActItem = objListBox.GetSelection
						If Not IsEmpty(strActItem) Then																			
							
							If CompareActualStringValues(strSelectItem, strActItem) = 0 Then
								SelectListItem = STATUS_PASSED
								customMsg = FormatString(MSG_OPERATION_SUCCESSFUL,Array("Select","List Box"))
							Else
								SelectListItem = STATUS_DEFECT
								customMsg = FormatString(MSG_OPERATION_UNSUCCESSFUL,Array("Select","List Box"))
								
							End If
						Else
							
							SelectListItem = STATUS_DEFECT
							customMsg = FormatString(MSG_OPERATION_UNSUCCESSFUL,Array("Select","List Box"))
							
						End If
						
					Else
						SelectListItem = STATUS_DEFECT					
						customMsg = FormatString(MSG_OPERATION_UNSUCCESSFUL,Array("Select","List Box"))
					End If
				Else 'IsItemExistInArry
					SelectListItem= STATUS_DEFECT
					customMsg = FormatString(MSG_ERR_NOT_AVAILABLE)
					'customMsg = "Given List item is Not existing in ListBox, Please verify" 'MSG_ERR_NOT_AVAILABLE
				End If 'IsItemExistInArry
			Else 'blnstatus
				SelectListItem= STATUS_DEFECT
				'customMsg = "List item are Empty ListBox, Please verify" 'MSG_ERR_EMPTY
				customMsg = FormatString(MSG_ERR_EMPTY,Array("List Box"))
			End If 'blnstatus
			
			
		Else
			intStatus = STATUS_FAILED
			customMsg = FormatString(MSG_OPERATION_FAILED_DISABLED,Array("Select Item","List Box")) 
			
		End If
	Else
		SelectListItem= STATUS_FAILED
		customMsg = FormatString(MSG_OBJECT_NOT_EXISTS,Array("List Box"))
		
	End If
	SelectNetListBoxItem = SelectListItem
	Call WriteActionStatusToLog(SelectListItem, customMsg)
	On Error Goto 0
End Function

'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name :			   SelectNetListBoxItemByIndex()
' 	Purpose :					  To Select the List box item by index
' 	Return Value :		 		Integer( 0/1/2)
' 	Arguments :
'		Input Arguments :  	 intIndex
'------------------------------------------------------------------------------------------------------------------------------------------

Function SelectNetListBoxItemByIndex(objListBox, byval intIndex)
	
	On Error Resume Next
	'Variable Section Begin
	
	Dim   intGetIndex, intItemCount , customMsg, intStatus
	
	'Variable Section End
	If CheckObjectExist(objListBox)  Then  
		If CheckObjectEnabled(objListBox) Then
			intGetIndex = CInt(intIndex)- 1
			If VarType(intGetIndex) = vbInteger Then
				'objListBox.Select Cstr("#"&intGetIndex)
				If intGetIndex >= 0 Then
					intItemCount = CInt(objListBox.GetROProperty("items count"))
					
					If intGetIndex <= intItemCount Then
						
						objListBox.Select intGetIndex 
						If Err.Number = 0 Then
							intStatus= STATUS_PASSED
							customMsg = FormatString(MSG_OPERATION_SUCCESSFUL,Array("Select","List Box"))
							'"Message: The item with index "& CStr("#"&intGetIndex)&" is selected"
						Else
							intStatus = STATUS_DEFECT
							customMsg = FormatString(MSG_OPERATION_UNSUCCESSFUL,Array("Select","List Box"))
							'	customMsg = "Error occured while selecting list item with index :' "&intGetIndex&" ' , Please Verify." 'MSG_OPERATION_UNSUCCESSFUL
						End If
					Else
						intStatus= STATUS_FAILED	
						customMsg = FormatString(MSG_OUT_OF_RANGE,Array("index"))		
						'	customMsg = "The Index Number provided is out of range in the specified ListBox items Count, Please Verify." 'MSG_OUT_OF_RANGE
						
					End If
				Else
					intStatus = STATUS_FAILED
					customMsg = FormatString(MSG_OUT_OF_RANGE,Array("index"))
					'customMsg =  " The index ' "& intGetIndex&" ' is not a positive integer. Please verify" 'MSG_OUT_OF_RANGE
				End If
			Else
				
				intStatus = STATUS_FAILED
				customMsg = FormatString(MSG_ERR_NOT_NUMERIC,Array("index"))
				
				
			End If
		Else
			intStatus = STATUS_FAILED
			customMsg = FormatString(MSG_OPERATION_FAILED_DISABLED,Array("Select Item","List Box")) 
			
		End If
	Else
		intStatus = STATUS_FAILED
		customMsg = FormatString(MSG_OBJECT_NOT_EXISTS,Array("List Box"))
	End If
	SelectNetListBoxItemByIndex = intStatus
	Call WriteActionStatusToLog(intStatus, customMsg)
	On Error Goto 0
End Function


'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name :				 VerifyNetListBoxSize()
' 	Purpose :					 Verifies the size(no.of items) of ListBox
' 	Return Value :		 		   Integer( 0/1/2)
' 	Arguments :
'		Input Arguments :  	  	objListBox,intExpectedSize
'------------------------------------------------------------------------------------------------------------------------------------------
Function VerifyNetListBoxSize(ByVal objListBox , ByVal intExpectedSize)
	On Error Resume Next
	'Variable Section Begin
	
	Dim intActCount,intCount , customMsg , intStatus
	
	'Variable Section End
	If CheckObjectExist(objListBox)  Then   
		intCount=CInt(intExpectedSize)
		
		If VarType(intCount) = vbInteger Then
			
			intActCount = objListBox.GetROProperty("items count")
			If Not IsEmpty(intActCount)  Then
				If CompareActualStringValues(intCount,intActCount)=0 Then
					intStatus = STATUS_PASSED
					customMsg = FormatString(MSG_VALUE_MATCHES,Array(intExpectedSize ,intActCount))
					'customMsg = "The specified ListBox Size is same as the expected size." 'MSG_VALUE_MATCHES
					
				Else
					intStatus = STATUS_DEFECT
					customMsg = FormatString(MSG_VALUE_NOT_MATCHES,Array(intExpectedSize ,intActCount))
					'	customMsg = "The specified ListBox Size is  different from the expected Size, Please verify." 'MSG_VALUE_NOT_MATCHES
				End If
			Else
				intStatus = STATUS_FAILED
				customMsg = FormatString(MSG_ERR_EMPTY,Array("List Box"))
				'customMsg = "Action Fails to get ListBox Items Count, Please Verify ListBox and its method and property" 'MSG_ERR_EMPTY
			End If
		Else
			
			intStatus = STATUS_FAILED
			customMsg = FormatString(MSG_ERR_NOT_NUMERIC,Array("Size"))
			'	customMsg = "The specified List box's expected size is not an Integer, Please verify." 'MSG_ERR_NOT_NUMERIC
			
			
		End If
	Else
		intStatus = STATUS_FAILED
		customMsg = FormatString(MSG_OBJECT_NOT_EXISTS,Array("ListBox"))
		
	End If
	
	VerifyNetListBoxSize = intStatus
	Call WriteActionStatusToLog(intStatus, customMsg)
	On Error Goto 0
End Function




'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name :				  DeSelectNetListBoxItem()
' 	Purpose :					  Deselect one Selected item  in  the ListBox.The listbox can have multiple selected items.
' 	Return Value :		 		  Integer( 0/1/2)
' 	Arguments :
'		Input Arguments :  		objListBox , strListItem
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function DeSelectNetListBoxItem(ByVal objListBox , ByVal strListItem)
	On Error Resume Next
	'Variable Section Begin
	
	Dim strSelectedItem,arrExp, arrAct , customMsg , intStatus
	Dim strListDeselectedItem
	Dim arrListDeselectedItems
	
	
	'Variable Section End
	If CheckObjectExist(objListBox) Then   
		If CheckObjectEnabled(objListBox) Then
			
			strSelectedItem = objListBox.GetRoProperty("Selection")' Trim(objListBox.GetRoProperty("Selection"))
			
			If Not IsEmpty(strSelectedItem) Then
				
				arrAct=GetArray(strSelectedItem,Chr(10))
				If Not IsNull(arrAct) Then
					If IsItemExistInArrayCS(arrAct,strListItem)  Then 
						objListBox.Deselect strListItem
						
						If Err.Number = 0 Then
							strListDeselectedItem = objListBox.GetRoProperty("Selection")
							arrListDeselectedItems = GetArray(strListDeselectedItem,Chr(10))
							
							If Not  IsItemExistInArrayCS(arrListDeselectedItems, strListItem)Then
								intStatus = STATUS_PASSED
								customMsg = FormatString(MSG_OPERATION_SUCCESSFUL,Array("Deselect","List Box"))
								
							Else
								intStatus = STATUS_DEFECT
								
								customMsg = FormatString(MSG_OPERATION_UNSUCCESSFUL,Array("Deselect","List Box"))
								
							End If
						Else
							intStatus = STATUS_DEFECT
							customMsg = FormatString(MSG_OPERATION_UNSUCCESSFUL,Array("Deselect","List Box"))
							
						End If
						
					Else
						intStatus = STATUS_FAILED
						customMsg = FormatString(MSG_ERR_NOT_AVAILABLE)
						'	customMsg = "Either the expected  item is not in selection mode OR the item does not exist in the list item, please verify." 'MSG_ERR_NOT_AVAILABLE
						
					End If
				Else
					intStatus = STATUS_FAILED
					customMsg = FormatString(MSG_ERR_EMPTY ("ListBox selection"))
					'	customMsg = "An error occurred, action fails to get the Array of the Expected and Actual selected item , please verify." 'MSG_ERR_EMPTY (ListBox selection)
				End If
				
			Else
				intStatus = STATUS_FAILED
				customMsg = FormatString(MSG_ERR_EMPTY ("ListBox selection"))
				'	customMsg = "Either the Action fails to get the selected list item OR no item is selected in the list, please verify." 'MSG_ERR_EMPTY (ListBox selection)
			End If
			
		Else
			intStatus = STATUS_FAILED
			customMsg = FormatString(MSG_OPERATION_FAILED_DISABLED,Array("De Select Item","List Box")) 
			
		End If
		
	Else
		
		
		intStatus = STATUS_FAILED
		customMsg = FormatString(MSG_OBJECT_NOT_EXISTS,Array("ListBox"))
		
	End If
	DeSelectNetListBoxItem = intStatus
	Call WriteActionStatusToLog(intStatus, customMsg)
	On Error Goto 0
End Function



'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name :				  DeSelectMultipleNetListBoxItems()
' 	Purpose :						 DeSelects  selected multiple items from the ListBox
' 	Return Value :		 		   Integer( 0/1)
' 	Arguments :
'		Input Arguments :  		objListBox , arrItems
'
'------------------------------------------------------------------------------------------------------------------------------------------
Function DeSelectMultipleNetListBoxItems(ByVal objListBox , ByVal arrItems)
	On Error Resume Next
	'Variable Section Begin
	
	Dim arrActItems,arrExpItems,intRes,strExpItem , intStatus ,customMsg
	Dim intCountBeforeDeselect,intCountAfterDeselect,intCount,intExpItemsSize , lstActualselItems
	
	'Variable Section End
	If CheckObjectExist(objListBox) Then   
		If CheckObjectEnabled(objListBox) Then
			arrExpItems = arrItems 
			If Not IsNull(arrExpItems) Then
				'Before Deselect
				lstActualselItems = objListBox.GetRoProperty("selection")
				'MsgBox lstActualselItems
				arrActItems = lstActualselItems 'GetArray(ConcateArrayItem(arrItems) , ";")
				intCountBeforeDeselect= UBound(arrActItems) 'objListBox.GetRoProperty("selected items count")
				If ArrActItems <> Empty Then
					'arrActItems=GetArray(arrActItems,Chr(10))
					If Not IsNull(arrActItems) Then
						If  CompareArraysCS(arrExpItems, arrActItems) = 0 Then
							'MsgBox Err.Description
							For Each strExpItem In arrExpItems
								objListBox.Deselect strExpItem  'Trim(strExpItem)
							Next
							
							lstActualselItems = objListBox.GetRoProperty("selection") 
							arrActItems = GetArray(lstActualselItems , ";")
							intCountAfterDeselect= UBound(arrActItems) 'objListBox.GetRoProperty("selected items count")  
							intCount=intCountBeforeDeselect-intCountAfterDeselect
							intExpItemsSize=UBound(arrExpItems)
							
							If arrActItems=Empty  Or  intExpItemsSize = intCount-1  Then
								intStatus = STATUS_PASSED
								customMsg = FormatString(MSG_OPERATION_SUCCESSFUL,Array("Deselect","List Box"))
								
							Else
								intStatus =  STATUS_DEFECT
								customMsg = FormatString(MSG_OPERATION_UNSUCCESSFUL,Array("Deselect","List Box"))
							End If
							
						Else
							intStatus =  STATUS_DEFECT
							customMsg = FormatString(MSG_OPERATION_UNSUCCESSFUL,Array("Deselect","List Box"))
						End If
						
					Else
						intStatus =  STATUS_FAILED
						customMsg = FormatString(MSG_ERR_EMPTY,Array("ListBox selection")) 
						
					End If
					
				Else
					intStatus =  STATUS_FAILED
					'customMsg = "Action fails to get List Items, Please verify Listbox Method and property"
					customMsg = FormatString(MSG_ERR_EMPTY,Array("ListBox selection")) 
				End If
				
			Else
				intStatus =  STATUS_FAILED
				'customMsg = "Action fails to get Expected Items into an array, Please verify"
				customMsg = FormatString(MSG_ERR_EMPTY,Array("input item list")) 
			End If
			
			
		Else
			intStatus = STATUS_FAILED
			customMsg = FormatString(MSG_OPERATION_FAILED_DISABLED,Array("De Select Multiple Items","List Box")) 
			
		End If
	Else
		intStatus =  STATUS_FAILED
		customMsg = FormatString(MSG_OBJECT_NOT_EXISTS,Array("ListBox"))
	End If
	
	DeSelectMultipleNetListBoxItems = intStatus
	Call WriteActionStatusToLog(intStatus, customMsg)
	
	On Error Goto 0
End Function



'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name :				 VerifyNetListBoxContainsItem()
' 	Purpose :					 Verifies  whether the listBox contains the specified Item.
' 	Return Value :		 		   Integer( 0/1/2)
' 	Arguments :
'		Input Arguments :  	  	objListBox,strExpItem
'------------------------------------------------------------------------------------------------------------------------------------------
Function VerifyNetListBoxContainsItem(ByVal objListBox , ByVal strListItem)
	On Error Resume Next
	'Variable Section Begin
	'MsgBox "h"
	Dim strActItems , arrActItems , arrExpItems,intCount,strExpItem,intActItem , blnpresentFlag, blnstatus
	Dim intStatus ,customMsg
	blnpresentFlag=False
	blnstatus= False
	
	
	'Variable Section End
	If CheckObjectExist(objListBox)  Then   
		strActItems = objListBox.GetROProperty("all items")
		
		If VarType(strActItems) <> vbEmpty  And Err.Number = 0 Then					
			arrActItems = GetArray(strActItems , Chr(10))				  
			For intActItem = 0 To UBound(arrActItems)
				If strListItem = arrActItems(intActItem) Then
					blnpresentFlag=True
					Exit For
				End If
			Next
			
			If blnpresentFlag = True  Then
				intStatus	 = STATUS_PASSED
				'customMsg = "ListBox Contains  the expected item."
				customMsg =  FormatString(MSG_CONTAINS_SPECIFIC_PASS,Array("List Box", "item"))
			Else
				intStatus	 = STATUS_DEFECT
				'customMsg = "List does not contain the expected item, please verify."
				customMsg =  FormatString(MSG_CONTAINS_SPECIFIC_FAIL,Array("List Box", "item"))
			End If			
			
		Else
			intStatus	 = STATUS_FAILED
			customMsg = FormatString(MSG_ERR_FETCH_DATA,Array("List Box items")) 
			'	customMsg = "Action Fails to get Items from ListBox, Please Verify ListBox method and property"
		End If
		
	Else
		intStatus	 = STATUS_FAILED
		customMsg = FormatString(MSG_OBJECT_NOT_EXISTS,Array("ListBox"))
		
	End If
	VerifyNetListBoxContainsItem = intStatus
	Call WriteActionStatusToLog(intStatus, customMsg)
	'MsgBox customMsg&" "&intStatus&" "&Err.Description
	On Error Goto 0
End Function


'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name :				 VerifyNetListBoxNotContainsItem()
' 	Purpose :					 Verifies  whether the ListBox doesn't contain the specified Item.
' 	Return Value :		 		   Integer( 0/1/2)
' 	Arguments :
'		Input Arguments :  	  	objListBox,strExpItem
'------------------------------------------------------------------------------------------------------------------------------------------
Function VerifyNetListBoxNotContainsItem(ByVal objListBox , ByVal strListItem)
	On Error Resume Next
	'Variable Section Begin
	
	Dim strActItems , arrActItems , arrExpItems,strExpItem,intActItem, blnEqualFlag, blnstatus
	Dim intStatus ,customMsg
	blnEqualFlag=True
	blnstatus = False
	
	'Variable Section End
	If CheckObjectExist(objListBox)  Then   
		strActItems = objListBox.GetROProperty("all items")
		
		If Err.Number = 0 Then	
			arrActItems = GetArray(strActItems , Chr(10))
			If VarType(arrActItems) <> vbEmpty  And Err.Number = 0 Then								
				For intActItem = 0 To UBound(arrActItems)
					If strListItem = arrActItems(intActItem) Then
						blnEqualFlag=False
						Exit For
					End If
				Next
				
				If blnEqualFlag = True  Then
					intStatus = STATUS_PASSED
					'customMsg = "List Does not contain the specified item."
					customMsg =    FormatString(MSG_NOT_CONTAINS_SPECIFIC_PASS,Array("List Box","item"))
				Else
					intStatus = STATUS_DEFECT
					'	customMsg = "List contain the specified item, please verify."
					customMsg =   FormatString(MSG_CONTAINS_SPECIFIC_FAIL, Array("List Box","item")) 
				End If
				
				
			Else
				intStatus = STATUS_FAILED
				'customMsg = "An error occurred , Action fails to get Actual items , please verify."
				customMsg =  FormatString(MSG_ERR_FETCH_DATA,Array("List Box items")) 
			End If
			
		Else
			intStatus = STATUS_FAILED
			customMsg = FormatString(MSG_ERR_FETCH_DATA,Array("List Box items")) 
		End If
		
	Else
		intStatus = STATUS_FAILED
		customMsg = FormatString(MSG_OBJECT_NOT_EXISTS,Array("ListBox"))
		
	End If
	VerifyNetListBoxNotContainsItem = intStatus
	Call WriteActionStatusToLog(intStatus, customMsg)
	'WriteStepResults VerifyListBoxNOTContainsItem
	On Error Goto 0
End Function



'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name :				  VerifyMultipleSelectedNetListBoxItems()
' 	Purpose :						 Verifies the  selected items in ListBox
' 	Return Value :		 		   Integer( 0/1)
' 	Arguments :
'		Input Arguments :  		objListBox , arrItems
'------------------------------------------------------------------------------------------------------------------------------------------
Function VerifyMultipleSelectedNetListBoxItems(ByVal objListBox , ByVal arrItems)
	On Error Resume Next
	'Variable Section Begin
	
	Dim arrActItems,arrExpItems,intItem, blnstatus
	Dim intStatus ,customMsg
	Dim blnExpstatus
	blnExpstatus = False
	blnstatus = False
	
	'Variable Section End
	If CheckObjectExist(objListBox) Then   
		arrExpItems = arrItems 'GetArray(arrItems , "^")
		
		If VarType(arrExpItems) <> vbEmpty  And Err.Number = 0 Then
			blnExpstatus = True
		End If
		
		If Not IsNull (arrExpItems) Then
			arrActItems = objListBox.GetRoProperty("selection")
			
			If VarType(arrActItems) <> vbEmpty  And Err.Number = 0 Then
				blnstatus = True
			End If
			
			
			If blnstatus And blnExpstatus Then
				arrActItems=GetArray(arrActItems,Chr(10))
				If MatchEachArrDataCS(arrExpItems , arrActItems) = True Then
					intStatus = STATUS_PASSED
					'customMsg = "The ListBox mulptiple selected items match the expected."
					customMsg = FormatString(MSG_ITEMS_MATCHES,Array("ListBox items", "selection of items")) 
				Else
					intStatus = STATUS_DEFECT
					'customMsg = "ListBox selected items are different from the expected, Please verify"
					customMsg = FormatString(MSG_ITEMS_NOT_MATCHES,Array("ListBox items", "selection of items")) 
				End If
				
			Else
				intStatus = STATUS_FAILED
				customMsg = FormatString(MSG_ERR_EMPTY,Array("ListBox selection")) 
			End If
			
		Else
			intStatus = STATUS_FAILED
			customMsg = FormatString(MSG_ERR_EMPTY,Array("input item list")) 
		End If
	Else
		
		intStatus = STATUS_FAILED
		customMsg =     FormatString(MSG_OBJECT_NOT_EXISTS,Array("List Box"))
	End If
	VerifyMultipleSelectedNetListBoxItems = intStatus
	Call WriteActionStatusToLog(intStatus, customMsg)
	On Error Goto 0
End Function



'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name :				  SelectMultipleNetListItems()
' 	Purpose :						 Selects multiple items from the ListBox
' 	Return Value :		 		   Integer( 0/1/2)
' 	Arguments :
'		Input Arguments :  		objListBox , arrItems
'------------------------------------------------------------------------------------------------------------------------------------------
Function SelectMultipleNetListBoxItems(ByVal objListBox , ByVal arrItems)
	On Error Resume Next
	'Variable Section Begin
	
	Dim arrActItems,arrExpItems,intItem
	Dim intarritemscount, iCount
	Dim bStatusExistance
	Dim intStatusExistance
	Dim stritemNotExists
	Dim strallitems, strActAllListItems
	Dim blnstatus 
	Dim strreportitem
	
	Dim arrActAllListItems
	
	Dim customMsg , intStatus
	
	blnstatus  = False
	intStatusExistance = 0
	bStatusExistance = Null
	stritemNotExists = Null
	
	'Variable Section End
	If CheckObjectExist(objListBox) Then   
		'MsgBox "zero"
		If CheckObjectEnabled(objListBox) Then
			strActAllListItems = objListBox.GetRoProperty("all items")
			
			If  VarType(strActAllListItems) <> vbEmpty   Then
				blnstatus = True
			End If
			If blnstatus  Then
				
				arrActAllListItems = Split(strActAllListItems, Chr(10))
				
				arrExpItems = arrItems 
				intarritemscount = UBound(arrExpItems)
				For iCount = 0 To intarritemscount
					If IsItemExistInArrayCS(arrActAllListItems , arrExpItems(iCount)) Then
						
						bStatusExistance = True
					Else
						bStatusExistance = False
						stritemNotExists = arrExpItems(iCount)
					End If
					
					If bStatusExistance = False Then
						intStatusExistance = intStatusExistance+1
						strreportitem = arrExpItems(iCount)
					End If
				Next
				
				If intStatusExistance =  0 Then
					
					If Not IsNull(arrExpItems) Then
						For  intItem=0 To UBound(arrExpItems)
							If intItem = 0 Then
								objListBox.Select arrExpItems(intItem)
							Else
								objListBox.ExtendSelect arrExpItems(intItem)
							End If
							
						Next
						If Err.Number = 0 Then
							arrActItems = objListBox.GetRoProperty("selection")
							arrActItems=GetArray(arrActItems,Chr(10))
							If Err.Number=0 And Not IsNull(arrActItems) Then
								If CompareArraysCS(arrExpItems , arrActItems) = 0 Then
									intStatus = STATUS_PASSED
									customMsg = FormatString(MSG_OPERATION_SUCCESSFUL,Array("Multiselection","List Box"))
									'	customMsg =  "The Expected Multiple Items are selected sucessfully" 'MSG_OPERATION_SUCCESSFUL
								Else
									intStatus = STATUS_DEFECT
									customMsg = FormatString(MSG_OPERATION_UNSUCCESSFUL,Array("Multiselection","List Box"))
									'	customMsg = "Expected Multiple Items have not been selected, Please verify" 'MSG_OPERATION_UNSUCCESSFUL
								End If
								
							Else
								intStatus = STATUS_DEFECT
								customMsg = FormatString(MSG_OPERATION_UNSUCCESSFUL,Array("Multiselection","List Box"))
								'	customMsg = "Could not capture selected items from ListBox" 'MSG_OPERATION_UNSUCCESSFUL
							End If
						Else
							intStatus = STATUS_DEFECT
							customMsg = FormatString(MSG_OPERATION_UNSUCCESSFUL,Array("Multiselection","List Box"))
							'customMsg = "Action fails to get selected items, Please verify" 'MSG_OPERATION_UNSUCCESSFUL
						End If
						
					Else
						intStatus = STATUS_FAILED
						customMsg = FormatString(MSG_ERR_EMPTY,Array("input item list")) 
					End If
					
				Else
					intStatus = STATUS_FAILED
					customMsg = FormatString(MSG_ERR_NOT_AVAILABLE)
					'customMsg =  "List item ["&strreportitem &"] not Exist in the list box." 'MSG_ERR_NOT_AVAILABLE
					
				End If
			Else'blnstatus
				intStatus=STATUS_FAILED
				customMsg = FormatString(MSG_ERR_EMPTY,Array("List Box"))
			End if'blnstatus
			
		Else
			intStatus = STATUS_FAILED
			customMsg = FormatString(MSG_OPERATION_FAILED_DISABLED,Array("Select Multiple Items","List Box")) 
			
		End If
		
	Else
		intStatus = STATUS_FAILED
		customMsg =     FormatString(MSG_OBJECT_NOT_EXISTS,Array("List Box"))
	End If
	
	SelectMultipleNetListBoxItems =   intStatus
	Call WriteActionStatusToLog(intStatus, customMsg)
	On Error Goto 0
End Function



'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name :				 VerifyNetListBoxItemsInSequence()
' 	Purpose :					 Compare the List Box items with our expected items in sequence order.
' 	Return Value :		 		 Integer( 0/1/2)
'		Input Arguments :  		objListBox , strExpItems
'------------------------------------------------------------------------------------------------------------------------------------------
Function VerifyNetListBoxItemsInSequence(ByVal objListBox , ByVal strExpItems)
	On Error Resume Next
	'Variable Section Begin
	Dim strActItems , arrActItems , arrExpItems
	Dim customMsg , intStatus
	'Variable Section End
	If CheckObjectExist(objListBox) Then   
		strActItems = objListBox.GetROProperty("all items")
		
		If Not IsEmpty(strActItems) Then
			arrActItems = GetArray(strActItems , Chr(10))
			arrExpItems = strExpItems 'GetArray(strExpItems , "^")
			
			If CompareArraysInSequenceCS(arrExpItems , arrActItems) = 0 Then
				intStatus = STATUS_PASSED
				customMsg =     FormatString(MSG_ITEMS_MATCHES,Array("List Box items","List Box items")) 
			Else
				intStatus =STATUS_DEFECT
				customMsg =  FormatString(MSG_ITEMS_NOT_MATCHES,Array("List Box items","List Box items"))  
			End If
			
		Else
			intStatus = STATUS_FAILED
			customMsg =  FormatString(MSG_ERR_EMPTY,Array("List Box"))  
		End If
	Else
		intStatus = STATUS_FAILED
		customMsg = FormatString(MSG_OBJECT_NOT_EXISTS,Array("List Box")) 
	End If
	VerifyNetListBoxItemsInSequence =   intStatus
	Call WriteActionStatusToLog(intStatus, customMsg)
	On Error Goto 0
End Function


'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name :				 VerifyAllNetListBoxItems()
' 	Purpose :					 Verifies  all the list items with our expected
' 	Return Value :		 		   Integer( 0/1/2)
'		Input Arguments :  	  	objListBox,strExpItems
'------------------------------------------------------------------------------------------------------------------------------------------
Function VerifyAllNetListBoxItems(ByVal objListBox , ByVal strExpItems)
	On Error Resume Next
	'Variable Section Begin
	
	Dim strActItems , arrActItems , arrExpItems
	Dim customMsg , intStatus
	'Variable Section End
	If CheckObjectExist(objListBox)  Then   
		strActItems = objListBox.GetROProperty("all items")
		If Err.number = 0 Then
			
			If Not IsEmpty(strActItems) Then
				arrActItems = GetArray(strActItems , Chr(10))
				arrExpItems = strExpItems ' GetArray(strExpItems , "^")
				
				
				If MatchEachArrDataCS(arrExpItems, arrActItems) = True Then
					intStatus = STATUS_PASSED
					customMsg = FormatString(MSG_ITEMS_MATCHES,Array("List Box items", "List Box items")) 
					
				Else
					intStatus = STATUS_DEFECT
					customMsg =  FormatString(MSG_ITEMS_NOT_MATCHES,Array("List Box items", "List Box items")) 
					
				End If
			Else
				intStatus = STATUS_FAILED
				customMsg =  FormatString(MSG_ERR_EMPTY,Array("List Box")) 
			End If
		Else
			intStatus = STATUS_FAILED
			customMsg = FormatString(MSG_ERR_FETCH_DATA , Array("List Box items")) 
		End If
	Else
		intStatus = STATUS_FAILED
		customMsg =   FormatString(MSG_OBJECT_NOT_EXISTS,Array("List Box"))
	End If
	VerifyAllNetListBoxItems = intStatus
	Call WriteActionStatusToLog(intStatus, customMsg)
	On Error Goto 0
End Function


'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name :			   VerifyNoDuplicationInListBox()
' 	Purpose :					  Verifies the duplicate items in the ComboBox
' 	Return Value :		 		Integer( 0/1/2)
'		Input Arguments :  	 objComboBox
'------------------------------------------------------------------------------------------------------------------------------------------
Function VerifyNoDuplicationInNetListBox(ByVal objListBox)
	On Error Resume Next
	'Variable Section Begin
	
	Dim strActItems , arrActItems 
	Dim customMsg , intStatus
	'Variable Section End
	If CheckObjectExist(objListBox)  Then   
		strActItems = objListBox.GetROProperty("all items")
		
		If Not IsEmpty(strActItems) Then
			arrActItems = GetArray(strActItems , Chr(10))
			If Not IsNull(arrActItems) Then
				
				If VerifyDuplicateElementsInArrayCS(arrActItems) = 0 Then
					intStatus = STATUS_PASSED
					customMsg =   FormatString(MSG_NO_DUPLICATES,Array("List Box" , "items")) 
				Else
					intStatus = STATUS_DEFECT
					customMsg =   FormatString(MSG_DUPLICATES,Array("List Box" , "items")) 
				End If
				
			Else
				intStatus = STATUS_FAILED
				customMsg =   FormatString(MSG_ERR_FETCH_DATA,Array("List Box items")) 
			End If
			
		Else
			intStatus = STATUS_FAILED
			customMsg =   FormatString(MSG_ERR_FETCH_DATA,Array("List Box items"))  
			
		End If
	Else
		intStatus = STATUS_FAILED
		customMsg =   FormatString(MSG_OBJECT_NOT_EXISTS,Array("List Box")) 
	End If
	VerifyNoDuplicationInNetListBox =	intStatus
	Call WriteActionStatusToLog( intStatus, customMsg)
	On Error Goto 0
End Function



'
'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name :				  SelectAllNetListBoxItem()
' 	Purpose :						 Selects all item from the ListBox
' 	Return Value :		 		   Integer( 0/1/2)
'------------------------------------------------------------------------------------------------------------------------------------------
Function SelectAllNetListBoxItem(ByVal objListBox)
	On Error Resume Next
	'Variable Section Begin
	
	Dim strActItem, strAllitems , customMsg , intStatus
	Dim arritems , arrActItems ,strSingleItem ,intItem
	
	'Variable Section End
	If CheckObjectExist(objListBox) Then
		If CheckObjectEnabled(objListBox) Then
			strallitems = objListBox.GetRoProperty("all items")
			'MsgBox strallitems
			If  VarType(strallitems) <> vbEmpty   Then			
				'objListBox.Select 
				arrActItems = GetArray(strallitems , Chr(10))				
				'MsgBox UBound(arrActItems)
				For intItem = 0 To UBound(arrActItems)
					objListBox.ExtendSelect intItem
					
				Next
				
				strActItem = objListBox.GetSelection 	
				'MsgBox Err.Description					
				If Err.number = 0 Then	
					If Not IsEmpty(strActItem) Then															
						If CompareActualStringValues(strallitems, strActItem) = 0 Then
							intStatus = STATUS_PASSED
							customMsg = FormatString(MSG_OPERATION_SUCCESSFUL,Array("Select","List Box"))
						Else
							intStatus = STATUS_DEFECT
							customMsg = FormatString(MSG_OPERATION_UNSUCCESSFUL,Array("Select","List Box"))
							
						End If
					Else
						
						intStatus = STATUS_DEFECT
						customMsg = FormatString(MSG_OPERATION_UNSUCCESSFUL,Array("Select","List Box"))
						
					End If
					
				Else
					intStatus= STATUS_FAILED
					customMsg = FormatString(MSG_ERR_EMPTY,Array("ListBox selection")) 
				End If
			Else
				intStatus= STATUS_FAILED
				customMsg = FormatString(MSG_ERR_EMPTY,Array("ListBox")) 
			End If	
			
			
		Else
			intStatus = STATUS_FAILED
			customMsg = FormatString(MSG_OPERATION_FAILED_DISABLED,Array("Select All Items","List Box")) 
			
		End If
		
	Else
		intStatus= STATUS_FAILED
		customMsg = FormatString(MSG_OBJECT_NOT_EXISTS,Array("List Box"))
		
	End If		
	
	SelectAllNetListBoxItem = intStatus
	Call WriteActionStatusToLog(intStatus, customMsg)
	On Error Goto 0
End Function



'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name :				  VerifySelectedNetListBoxItem()
' 	Purpose :						 Verifies the  selected one item in ListBox
' 	Return Value :		 		   Integer( 0/1)
' 	Arguments :
'		Input Arguments :  		objListBox , arrItems
'------------------------------------------------------------------------------------------------------------------------------------------
Function VerifySelectedNetListBoxItem(ByVal objListBox , ByVal strExpItem)
	On Error Resume Next
	'Variable Section Begin
	
	Dim arrActItems,arrExpItems,intItem, blnstatus
	Dim intStatus ,customMsg
	Dim blnExpstatus
	blnExpstatus = False
	blnstatus = False
	
	'Variable Section End
	If CheckObjectExist(objListBox) Then   
		'arrExpItem = strExpItem 'GetArray(arrItems , "^")
		
		If VarType(strExpItem) <> vbEmpty  And Err.Number = 0 Then
			blnExpstatus = True
		End If
		
		If Not IsNull (strExpItem) Then
			arrActItems = objListBox.GetRoProperty("selection")
			
			If VarType(arrActItems) <> vbEmpty  And Err.Number = 0 Then
				blnstatus = True
			End If
			
			
			If blnstatus And blnExpstatus Then
				arrActItems=GetArray(arrActItems,Chr(10))
				
				If IsItemExistInArrayCS(arrActItems , strExpItem) = True Then
					intStatus = STATUS_PASSED
					'customMsg = "The ListBox  selected item match the expected."
					customMsg = FormatString(MSG_ITEMS_MATCHES,Array("ListBox item", "selected item")) 
				Else
					intStatus = STATUS_DEFECT
					'customMsg = "ListBox selected item is different from the expected, Please verify"
					customMsg = FormatString(MSG_ITEMS_NOT_MATCHES,Array("ListBox item", "selected item")) 
				End If
				
			Else
				intStatus = STATUS_FAILED
				customMsg = FormatString(MSG_ERR_EMPTY,Array("ListBox selection")) 
			End If
			
		Else
			intStatus = STATUS_FAILED
			customMsg = FormatString(MSG_ERR_EMPTY,Array("expected item")) 
		End If
	Else
		
		intStatus = STATUS_FAILED
		customMsg =     FormatString(MSG_OBJECT_NOT_EXISTS,Array("List Box"))
	End If
	VerifySelectedNetListBoxItem = intStatus
	Call WriteActionStatusToLog(intStatus, customMsg)
	On Error Goto 0
End Function

