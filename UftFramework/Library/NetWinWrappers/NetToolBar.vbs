Function VerifyNetToolBarItemByIndex(byval objToolBar,byval intIndex,byval strItemName)
	On Error Resume Next
	Dim actualtCount,actualValue
	Dim statusTest
	Dim customMsg 
	If CheckObjectExist(objToolBar) Then
		If IsToolStrip(objToolBar)Or IsToolBar(objToolBar) Then
			If IsToolBar(objToolBar)Then
				intIndex=CInt(intIndex)
			Else
				intIndex=CInt(intIndex)-1
			End If
			If  Err.number=0 Then
				actualtCount=objToolBar.GetItemsCount
				If Err.number=0 Then
					If  actualtCount>intIndex And intIndex>=0 Then
						actualValue=objToolBar.GetItem(intIndex)
						If Err.Number=0 Then
							If CompareActualStringValues(strItemName,actualValue)=0 Then
								statusTest=STATUS_PASSED
								customMsg = FormatString(MSG_ITEMS_MATCHES, Array(actualValue,strItemName)) 
							Else
								statusTest=STATUS_DEFECT
								customMsg = FormatString(MSG_ITEMS_NOT_MATCHES,  Array(actualValue,strItemName)) 
							End If
						Else
							statusTest=STATUS_FAILED
							customMsg = FormatString(MSG_OUT_OF_RANGE, Array(intIndex))
						End If						
					Else
						statusTest=STATUS_FAILED
						customMsg = FormatString(MSG_OUT_OF_RANGE, Array(intIndex+1))
					End If
				Else
					statusTest=STATUS_FAILED
					customMsg = FormatString(MSG_ERR_EMPTY, Array("ToolBar"))
				End If
			Else
				statusTest=STATUS_FAILED
				customMsg = FormatString(MSG_ERR_NOT_NUMERIC, Array(intIndex))
			End If
		Else
			statusTest=STATUS_FAILED
			customMsg = FormatString(MSG_INVALID_ACTION, Array("ToolBar"))
		End If
	Else
		statusTest=STATUS_FAILED
		customMsg = FormatString(MSG_OBJECT_NOT_EXISTS, Array("ToolBar"))
	End If
	VerifyNetToolBarItemByIndex=statusTest
	Call writeActionStatusToLog(statusTest,customMsg)
	'MsgBox statusTest&" "&customMsg&" "&Err.description
	On Error Goto 0
End Function

Function VerifyNetToolBarItemCount(byval objToolBar,byval intExpCount)
	On Error Resume Next
	Dim actualtCount,actualValue
	Dim statusTest
	Dim customMsg 
	If CheckObjectExist(objToolBar) Then
		If IsToolStrip(objToolBar)Or IsToolBar(objToolBar) Then
			intExpCount=CInt(intExpCount)
			If  Err.number=0 Then
				actualtCount=objToolBar.GetItemsCount
				If Err.number=0 Then
					If  intExpCount=actualtCount Then
						statusTest=STATUS_PASSED
						customMsg = FormatString(MSG_ITEMS_MATCHES, Array(actualtCount,intExpCount)) 
					Else
						statusTest=STATUS_DEFECT
						customMsg = FormatString(MSG_ITEMS_NOT_MATCHES,  Array(actualtCount,intExpCount)) 
					End If
				Else
					statusTest=STATUS_FAILED
					customMsg = FormatString(MSG_ERR_EMPTY, Array("ToolBar"))
				End If
			Else
				statusTest=STATUS_FAILED
				customMsg = FormatString(MSG_ERR_NOT_NUMERIC, Array(intIndex))
			End If
		Else
			statusTest=STATUS_FAILED
			customMsg = FormatString(MSG_INVALID_ACTION, Array("ToolBar"))
		End If
	Else
		statusTest=STATUS_FAILED
		customMsg = FormatString(MSG_OBJECT_NOT_EXISTS, Array("ToolBar"))
	End If
	VerifyNetToolBarItemCount=statusTest
	Call writeActionStatusToLog(statusTest,customMsg)
	'MsgBox statusTest&" "&customMsg&" "&Err.description
	On Error Goto 0
End Function


Function VerifyNetToolBarItemEnabled(byval objToolBar,byval intIndex)
	On Error Resume Next
	Dim actualtCount,actualValue
	Dim statusTest
	Dim customMsg 
	If CheckObjectExist(objToolBar) Then
		If IsToolStrip(objToolBar) Then
			intIndex=CInt(intIndex)-1
			If  Err.number=0 Then
				actualtCount=objToolBar.GetItemsCount
				If Err.number=0 Then
					If  actualtCount>intIndex And intIndex>=0 Then
						actualValue=objToolBar.GetItem(intIndex)
						If objToolBar.IsItemEnabled(actualValue)   Then
							statusTest=STATUS_PASSED
							customMsg = "The specified toolbar item is enabled as expected."  ' FormatString(MSG_ITEMS_MATCHES, Array(actualValue,strItemName)) 
						Else
							statusTest=STATUS_DEFECT
							customMsg = "The specified toolbar item is disabled, Please verify." 'FormatString(MSG_ITEMS_NOT_MATCHES,  Array(actualValue,strItemName)) 
						End If
					Else
						statusTest=STATUS_FAILED
						customMsg = FormatString(MSG_OUT_OF_RANGE, Array(intIndex+1))
					End If
				Else
					statusTest=STATUS_FAILED
					customMsg = FormatString(MSG_ERR_EMPTY, Array("ToolBar"))
				End If
			Else
				statusTest=STATUS_FAILED
				customMsg = FormatString(MSG_ERR_NOT_NUMERIC, Array(intIndex))
			End If
		Else 
		  If IsToolBar(objToolBar) Then
			intIndex=CInt(intIndex)
			If Err.number=0 Then
				actualtCount=objToolBar.GetItemsCount
				If Err.number=0 Then
					If  actualtCount>intIndex And intIndex>0 Then
						actualValue=objToolBar.GetItem(intIndex)
						If objToolBar.GetItemProperty(actualValue,"enabled")   Then
							statusTest=STATUS_PASSED
							customMsg = "The specified toolbar item is enabled as expected."  ' FormatString(MSG_ITEMS_MATCHES, Array(actualValue,strItemName)) 
						Else
							statusTest=STATUS_DEFECT
							customMsg = "The specified toolbar item is disabled, Please verify." 'FormatString(MSG_ITEMS_NOT_MATCHES,  Array(actualValue,strItemName)) 
						End If
					Else
						statusTest=STATUS_FAILED
						customMsg = FormatString(MSG_OUT_OF_RANGE, Array(intIndex))
					End If
				Else
					statusTest=STATUS_FAILED
					customMsg = FormatString(MSG_ERR_EMPTY, Array("ToolBar"))
				End If
			Else
				statusTest=STATUS_FAILED
				customMsg = FormatString(MSG_ERR_NOT_NUMERIC, Array(intIndex))
			End If	
		Else			
			statusTest=STATUS_FAILED
			customMsg = FormatString(MSG_INVALID_ACTION, Array("ToolBar"))
		End If
	end if
	Else
		statusTest=STATUS_FAILED
		customMsg = FormatString(MSG_OBJECT_NOT_EXISTS, Array("ToolBar"))
	End If
	VerifyNetToolBarItemEnabled=statusTest
	Call writeActionStatusToLog(statusTest,customMsg)
	'MsgBox statusTest&" "&customMsg&" "&Err.description
	On Error Goto 0
End Function

Function VerifyNetToolBarItemDisabled(byval objToolBar,byval intIndex)          
	On Error Resume Next
	Dim actualtCount,actualValue
	Dim statusTest
	Dim customMsg 
	If CheckObjectExist(objToolBar) Then       
		If IsToolStrip(objToolBar) Then		
			intIndex=CInt(intIndex)-1
			If  Err.number=0 Then
				actualtCount=objToolBar.GetItemsCount
				If Err.number=0 Then
					If  actualtCount>intIndex And intIndex>=0 Then
						actualValue=objToolBar.GetItem(intIndex)
						If Not objToolBar.IsItemEnabled(actualValue)   Then
							statusTest=STATUS_PASSED
							customMsg = "The specified toolbar item is disabled as expected."  ' FormatString(MSG_ITEMS_MATCHES, Array(actualValue,strItemName)) 
						Else
							statusTest=STATUS_DEFECT
							customMsg = "The specified toolbar item is enabled, Please verify." 'FormatString(MSG_ITEMS_NOT_MATCHES,  Array(actualValue,strItemName)) 
						End If
					Else
						statusTest=STATUS_FAILED
						customMsg = FormatString(MSG_OUT_OF_RANGE, Array(intIndex+1))
					End If
				Else
					statusTest=STATUS_FAILED
					customMsg = FormatString(MSG_ERR_EMPTY, Array("ToolBar"))
				End If
			Else
				statusTest=STATUS_FAILED
				customMsg = FormatString(MSG_ERR_NOT_NUMERIC, Array(intIndex))
			End If
		Else
			If IsToolBar(objToolBar) Then					
					intIndex=CInt(intIndex)
					If Err.number=0 Then
						actualtCount=objToolBar.GetItemsCount
						If Err.number=0 Then
							If  actualtCount>intIndex And intIndex>0 Then
								actualValue=objToolBar.GetItem(intIndex)
								If Err.Number=0 Then
									If Not objToolBar.GetItemProperty(actualValue,"enabled")   Then
										statusTest=STATUS_PASSED
										customMsg = "The specified toolbar item is disabled as expected."  ' FormatString(MSG_ITEMS_MATCHES, Array(actualValue,strItemName)) 
									Else
										statusTest=STATUS_DEFECT
										customMsg = "The specified toolbar item is enabled, Please verify." 'FormatString(MSG_ITEMS_NOT_MATCHES,  Array(actualValue,strItemName)) 
									End If
								Else
									statusTest=STATUS_DEFECT
									customMsg = "The specified toolbar item is enabled, Please verify." 'FormatString(MSG_ITEMS_NOT_MATCHES,  Array(actualValue,strItemName)) 
								End If 								
							Else
								statusTest=STATUS_FAILED
								customMsg = FormatString(MSG_OUT_OF_RANGE, Array(intIndex))
							End If
						Else
							statusTest=STATUS_FAILED
							customMsg = FormatString(MSG_ERR_EMPTY, Array("ToolBar"))
						End If
					Else
						statusTest=STATUS_FAILED
						customMsg = FormatString(MSG_ERR_NOT_NUMERIC, Array(intIndex))
					End If	
			else
				statusTest=STATUS_FAILED
				customMsg = FormatString(MSG_INVALID_ACTION, Array("ToolBar"))
			End If
			
		End If
	Else
		statusTest=STATUS_FAILED
		customMsg = FormatString(MSG_OBJECT_NOT_EXISTS, Array("ToolBar"))
	End If
	VerifyNetToolBarItemDisabled=statusTest
	Call writeActionStatusToLog(statusTest,customMsg)
	'MsgBox statusTest&" "&customMsg&" "&Err.description
	On Error Goto 0
End Function

Function VerifyNetToolBarItemHighlighted(byval objToolBar,byval intIndex)
	On Error Resume Next
	Dim actualtCount,actualValue
	Dim statusTest
	Dim customMsg 
	If CheckObjectExist(objToolBar) Then
		If IsToolBar(objToolBar) Then
			intIndex=CInt(intIndex)
			If  Err.number=0 Then
				actualtCount=objToolBar.GetItemsCount
				If Err.number=0 Then
					If  actualtCount>intIndex And intIndex>0 Then
						actualValue=objToolBar.GetItem(intIndex)
						If  objToolBar.GetItemProperty(actualValue,"Checked")   Then
							statusTest=STATUS_PASSED
							customMsg =  FormatString(MSG_OPERATION_SUCCESSFUL,Array("Verify item highlighted", "tool bar item"))
						Else
							statusTest=STATUS_DEFECT
							customMsg = FormatString(MSG_OPERATION_UNSUCCESSFUL, Array("Verify item highlighted", "tool bar item"))
						End If
					Else
						statusTest=STATUS_FAILED
						customMsg = FormatString(MSG_OUT_OF_RANGE, Array(intIndex))
					End If
				Else
					statusTest=STATUS_FAILED
					customMsg = FormatString(MSG_ERR_EMPTY, Array("ToolBar"))
				End If
			Else
				statusTest=STATUS_FAILED
				customMsg = FormatString(MSG_ERR_NOT_NUMERIC, Array(intIndex))
			End If
		Else
			statusTest=STATUS_FAILED
			customMsg = FormatString(MSG_INVALID_ACTION, Array("ToolBar"))
		End If
	Else
		statusTest=STATUS_FAILED
		customMsg = FormatString(MSG_OBJECT_NOT_EXISTS, Array("ToolBar"))
	End If
	VerifyNetToolBarItemHighlighted=statusTest
	Call writeActionStatusToLog(statusTest,customMsg)
	'MsgBox statusTest&" "&customMsg&" "&Err.description
	On Error Goto 0
End Function

Function VerifyNetToolBarItemNotHighlighted(byval objToolBar,byval intIndex)
	On Error Resume Next
	Dim actualtCount,actualValue
	Dim statusTest
	Dim customMsg 
	If CheckObjectExist(objToolBar) Then
		If IsToolBar(objToolBar) Then
			intIndex=CInt(intIndex)
			If  Err.number=0 Then
				actualtCount=objToolBar.GetItemsCount
				If Err.number=0 Then
					If  actualtCount>intIndex And intIndex>0 Then
						actualValue=objToolBar.GetItem(intIndex)
						If  not objToolBar.GetItemProperty(actualValue,"Checked")   Then
							statusTest=STATUS_PASSED
							customMsg =  FormatString(MSG_OPERATION_SUCCESSFUL,Array("Verify item not highlighted", "tool bar item"))
						Else
							statusTest=STATUS_DEFECT
							customMsg = FormatString(MSG_OPERATION_UNSUCCESSFUL, Array("Verify item not highlighted", "tool bar item"))
						End If
					Else
						statusTest=STATUS_FAILED
						customMsg = FormatString(MSG_OUT_OF_RANGE, Array(intIndex))
					End If
				Else
					statusTest=STATUS_FAILED
					customMsg = FormatString(MSG_ERR_EMPTY, Array("ToolBar"))
				End If
			Else
				statusTest=STATUS_FAILED
				customMsg = FormatString(MSG_ERR_NOT_NUMERIC, Array(intIndex))
			End If
		Else
			statusTest=STATUS_FAILED
			customMsg = FormatString(MSG_INVALID_ACTION, Array("ToolBar"))
		End If
	Else
		statusTest=STATUS_FAILED
		customMsg = FormatString(MSG_OBJECT_NOT_EXISTS, Array("ToolBar"))
	End If
	VerifyNetToolBarItemNotHighlighted=statusTest
	Call writeActionStatusToLog(statusTest,customMsg)
	'MsgBox statusTest&" "&customMsg&" "&Err.description
	On Error Goto 0
End Function

Function VerifyNetToolBarItemNotExist(byval objToolBar,byval intIndex)
	On Error Resume Next
	Dim actualtCount,actualValue
	Dim statusTest
	Dim customMsg 
	If CheckObjectExist(objToolBar) Then
		If IsToolStrip(objToolBar) Then
			intIndex=CInt(intIndex)-1
			If  Err.number=0 Then
				actualtCount=objToolBar.GetItemsCount
				If Err.number=0 Then
					If  actualtCount>intIndex And intIndex>=0 Then
						actualValue=objToolBar.GetItem(intIndex)
						If  objToolBar.ItemExists(actualValue)   Then
							statusTest=STATUS_DEFECT
							customMsg =  "Specified tool bar item is exist."'FormatString(MSG_OPERATION_SUCCESSFUL,Array("Verify item not highlighted", "tool bar item"))
						Else
							statusTest=STATUS_DEFECT
							customMsg = "Specified tool bar item is not exist as expected."'FormatString(MSG_OPERATION_UNSUCCESSFUL, Array("Verify item not highlighted", "tool bar item"))
						End If
					Else
						 	statusTest=STATUS_PASSED
							customMsg =  "Specified tool bar item is not exist as expected."
					End If
				Else
					statusTest=STATUS_FAILED
					customMsg = FormatString(MSG_ERR_EMPTY, Array("ToolBar"))
				End If
			Else
				statusTest=STATUS_FAILED
				customMsg = FormatString(MSG_ERR_NOT_NUMERIC, Array(intIndex))
			End If
		Else
			statusTest=STATUS_FAILED
			customMsg = FormatString(MSG_INVALID_ACTION, Array("ToolBar"))
		End If
	Else
		statusTest=STATUS_FAILED
		customMsg = FormatString(MSG_OBJECT_NOT_EXISTS, Array("ToolBar"))
	End If
	VerifyNetToolBarItemNotExist=statusTest
	Call writeActionStatusToLog(statusTest,customMsg)
	'MsgBox statusTest&" "&customMsg&" "&Err.description
	On Error Goto 0
End Function

Function VerifyNetToolBarItemExist(byval objToolBar,byval intIndex)
	On Error Resume Next
	Dim actualtCount,actualValue
	Dim statusTest
	Dim customMsg 
	If CheckObjectExist(objToolBar) Then
		If IsToolStrip(objToolBar) Then
			intIndex=CInt(intIndex)-1
			If  Err.number=0 Then
				actualtCount=objToolBar.GetItemsCount
				'MsgBox actualtCount
				If Err.number=0 Then
					If  actualtCount>intIndex And intIndex>=0 Then
						actualValue=objToolBar.GetItem(intIndex)
						If Err.Number=0 Then
							If  objToolBar.ItemExists(actualValue)   Then
								statusTest=STATUS_PASSED
								customMsg =  "Specified tool bar item is exist as expected."'FormatString(MSG_OPERATION_SUCCESSFUL,Array("Verify item not highlighted", "tool bar item"))
							Else
								statusTest=STATUS_DEFECT
								customMsg = "Specified tool bar item is not exist as expected."'FormatString(MSG_OPERATION_UNSUCCESSFUL, Array("Verify item not highlighted", "tool bar item"))
							End If
						Else	
							statusTest=STATUS_FAILED
							customMsg = FormatString(MSG_OUT_OF_RANGE, Array(intIndex+1))														
						End If						
					Else
						statusTest=STATUS_FAILED
						customMsg = FormatString(MSG_OUT_OF_RANGE, Array(intIndex+1))
					End If
				Else
					statusTest=STATUS_FAILED
					customMsg = FormatString(MSG_ERR_EMPTY, Array("ToolBar"))
				End If
			Else
				statusTest=STATUS_FAILED
				customMsg = FormatString(MSG_ERR_NOT_NUMERIC, Array(intIndex))
			End If
		Else
			statusTest=STATUS_FAILED
			customMsg = FormatString(MSG_INVALID_ACTION, Array("ToolBar"))
		End If
	Else
		statusTest=STATUS_FAILED
		customMsg = FormatString(MSG_OBJECT_NOT_EXISTS, Array("ToolBar"))
	End If
	VerifyNetToolBarItemExist=statusTest
	Call writeActionStatusToLog(statusTest,customMsg)
	'MsgBox statusTest&" "&customMsg&" "&Err.description
	On Error Goto 0
End Function


Function VerifyNetToolBarMenuItemEnabled(byval objToolBar,byval arrItems)
	On Error Resume Next
	Dim actualtCount,strActualValue
	Dim statusTest,customFlag
	Dim customMsg
	customFlag=False
	For n=0 to uBound(arrItems)
			If n=0 Then
				strActualValue=arrItems(n)
			else
					strActualValue=strActualValue&";"&arrItems(n)
			End If
	Next
	If CheckObjectExist(objToolBar) Then
		If IsMenuStrip(objToolBar) Then
		   customFlag= objToolBar.IsItemEnabled(strActualValue)
		   If err.number=0 Then
				If  customFlag Then
						statusTest=STATUS_PASSED
						customMsg = "Specified toolbar item is enabled as expected." 'FormatString(MSG_ITEMS_MATCHES, Array(actualValue,strItemName)) 
				else
						statusTest=STATUS_DEFECT
						customMsg = "Specified toolbar item is disabled, Please verify"  'FormatString(MSG_ITEMS_NOT_MATCHES,  Array(actualValue,strItemName)) 
				End If
			else
				statusTest=STATUS_DEFECT
				customMsg = "Specified toolbar item is not found, Please verify."  'FormatString(MSG_ITEMS_NOT_MATCHES,  Array(actualValue,strItemName)) 
			End If		
		Else
			statusTest=STATUS_FAILED
			customMsg = FormatString(MSG_INVALID_ACTION, Array("ToolBar"))
		End If
	Else
		statusTest=STATUS_FAILED
		customMsg = FormatString(MSG_OBJECT_NOT_EXISTS, Array("ToolBar"))
	End If
	VerifyNetToolBarMenuItemEnabled=statusTest
	Call writeActionStatusToLog(statusTest,customMsg)
	'MsgBox statusTest&" "&customMsg&" "&Err.description
	On Error Goto 0
End Function

Function VerifyNetToolBarMenuItemDisabled(byval objToolBar,byval arrItems)
	On Error Resume Next
	Dim actualtCount,strActualValue
	Dim statusTest,customFlag
	Dim customMsg
	customFlag=False
	For n=0 to uBound(arrItems)
			If n=0 Then
				strActualValue=arrItems(n)
			else
					strActualValue=strActualValue&";"&arrItems(n)
			End If
	Next
	If CheckObjectExist(objToolBar) Then
		If IsMenuStrip(objToolBar) Then
		   customFlag= objToolBar.IsItemEnabled(strActualValue)
		   If err.number=0 Then
				If  customFlag=false Then
						statusTest=STATUS_PASSED
						customMsg = "Specified toolbar item is disabled as expected." 'FormatString(MSG_ITEMS_MATCHES, Array(actualValue,strItemName)) 
				else
						statusTest=STATUS_DEFECT
						customMsg = "Specified toolbar item is enabled, Please verify"  'FormatString(MSG_ITEMS_NOT_MATCHES,  Array(actualValue,strItemName)) 
				End If
			else
				statusTest=STATUS_DEFECT
				customMsg = "Specified toolbar item is not found, Please verify."  'FormatString(MSG_ITEMS_NOT_MATCHES,  Array(actualValue,strItemName)) 
			End If		
		Else
			statusTest=STATUS_FAILED
			customMsg = FormatString(MSG_INVALID_ACTION, Array("ToolBar"))
		End If
	Else
		statusTest=STATUS_FAILED
		customMsg = FormatString(MSG_OBJECT_NOT_EXISTS, Array("ToolBar"))
	End If
	VerifyNetToolBarMenuItemDisabled=statusTest
	Call writeActionStatusToLog(statusTest,customMsg)
	'MsgBox statusTest&" "&customMsg&" "&Err.description
	On Error Goto 0
End Function

Function VerifyNetToolBarMenuItemExist(byval objToolBar,byval arrItems)
	On Error Resume Next
	Dim actualtCount,strActualValue
	Dim statusTest,customFlag
	Dim customMsg
	customFlag=False	
	For n=0 to uBound(arrItems)
			If n=0 Then
				strActualValue=arrItems(n)
			else
					strActualValue=strActualValue&";"&arrItems(n)
			End If
	Next	
	If CheckObjectExist(objToolBar) Then
		If IsMenuStrip(objToolBar) Then
		   customFlag= objToolBar.ItemExists(strActualValue)
		   If err.number=0 Then
				If  customFlag Then
						statusTest=STATUS_PASSED
						customMsg = "Specified toolbar item is exist as expected." 'FormatString(MSG_ITEMS_MATCHES, Array(actualValue,strItemName)) 
				else
						statusTest=STATUS_DEFECT
						customMsg = "Specified toolbar item is not exist, Please verify"  'FormatString(MSG_ITEMS_NOT_MATCHES,  Array(actualValue,strItemName)) 
				End If
			else
				statusTest=STATUS_DEFECT
				customMsg = "Specified toolbar item is not found, Please verify."  'FormatString(MSG_ITEMS_NOT_MATCHES,  Array(actualValue,strItemName)) 
			End If		
		Else
			statusTest=STATUS_FAILED
			customMsg = FormatString(MSG_INVALID_ACTION, Array("ToolBar"))
		End If
	Else
		statusTest=STATUS_FAILED
		customMsg = FormatString(MSG_OBJECT_NOT_EXISTS, Array("ToolBar"))
	End If
	VerifyNetToolBarMenuItemExist=statusTest
	Call writeActionStatusToLog(statusTest,customMsg)
	'MsgBox statusTest&" "&customMsg&" "&Err.description
	On Error Goto 0
End Function


Function VerifyNetToolBarMenuItemNotExist(byval objToolBar,byval arrItems)
	On Error Resume Next
	Dim actualtCount,strActualValue
	Dim statusTest,customFlag
	Dim customMsg,n
	customFlag=false
	For n=0 to uBound(arrItems)
			If n=0 Then
				strActualValue=arrItems(n)
			else
					strActualValue=strActualValue&";"&arrItems(n)
			End If
	Next
	If CheckObjectExist(objToolBar) Then
		If IsMenuStrip(objToolBar) Then
		   customFlag= objToolBar.ItemExists(strActualValue)
		   If err.number=0 Then
				If  customFlag=false Then
						statusTest=STATUS_PASSED
						customMsg = "Specified toolbar item is not exist as expected." 'FormatString(MSG_ITEMS_MATCHES, Array(actualValue,strItemName)) 
				else
						statusTest=STATUS_DEFECT
						customMsg = "Specified toolbar item is exist, Please verify"  'FormatString(MSG_ITEMS_NOT_MATCHES,  Array(actualValue,strItemName)) 
				End If
			else
				statusTest=STATUS_DEFECT
				customMsg = "Specified toolbar item is not found, Please verify."  'FormatString(MSG_ITEMS_NOT_MATCHES,  Array(actualValue,strItemName)) 
			End If		
		Else
			statusTest=STATUS_FAILED
			customMsg = FormatString(MSG_INVALID_ACTION, Array("ToolBar"))
		End If
	Else
		statusTest=STATUS_FAILED
		customMsg = FormatString(MSG_OBJECT_NOT_EXISTS, Array("ToolBar"))
	End If
	VerifyNetToolBarMenuItemNotExist=statusTest
	Call writeActionStatusToLog(statusTest,customMsg)
	'MsgBox statusTest&" "&customMsg&" "&Err.description
	On Error Goto 0
End Function

Function ClickNetToolBarItemByName(byval objToolBar,byval strItemName)          
	On Error Resume Next
	Dim actualtCount
	Dim statusTest
	Dim customMsg 
	If CheckObjectExist(objToolBar) Then		
			If IsToolStrip(objToolBar) Then
				If objToolBar.IsItemEnabled(strItemName) Then
						objToolBar.Press strItemName
						If err.number=0   Then
							statusTest=STATUS_PASSED
							customMsg =  FormatString(MSG_OPERATION_SUCCESSFUL, Array("Click","ToolBar Item")) 
						Else
							statusTest=STATUS_DEFECT
							customMsg = FormatString(MSG_OPERATION_UNSUCCESSFUL,  Array("Click","ToolBar Item")) 
						End If
				else
					statusTest=STATUS_FAILED
					customMsg = FormatString(MSG_OPERATION_FAILED_DISABLED, Array("ToolBar Item"))	
				End If
		Else
			If IsToolBar(objToolBar) Then
			'MsgBox "Before :"&Err.Description
				If  objToolBar.GetItemProperty(strItemName,"enabled")Then
							objToolBar.Press strItemName
								If err.number=0  Then
									statusTest=STATUS_PASSED
									customMsg =  FormatString(MSG_OPERATION_SUCCESSFUL, Array("Click","ToolBar Item")) 
								Else
									statusTest=STATUS_DEFECT
									customMsg = FormatString(MSG_OPERATION_UNSUCCESSFUL,  Array("Click","ToolBar Item")) 
								End If
				Else
				'MsgBox "After :"&Err.Description
					statusTest=STATUS_FAILED
					customMsg = FormatString(MSG_OPERATION_FAILED_DISABLED, Array("ToolBar Item"))	
				End If
								
			else
				statusTest=STATUS_FAILED
				customMsg = FormatString(MSG_INVALID_ACTION, Array("ToolBar"))
			End If
		End If	
	Else
		statusTest=STATUS_FAILED
		customMsg = FormatString(MSG_OBJECT_NOT_EXISTS, Array("ToolBar"))
	End If
	ClickNetToolBarItemByName=statusTest
	Call writeActionStatusToLog(statusTest,customMsg)
	'MsgBox statusTest&" "&customMsg&" "&Err.description
	On Error Goto 0
End Function


Function ClickNetToolBarItemByIndex(byval objToolBar,byval intIndex)          
	On Error Resume Next
	Dim actualtCount,actualValue
	Dim statusTest
	Dim customMsg 
	If CheckObjectExist(objToolBar) Then	  
		If IsToolStrip(objToolBar) Then
			intIndex=CInt(intIndex)-1
			If  Err.number=0 Then
				actualtCount=objToolBar.GetItemsCount
				If Err.number=0 Then
					If  actualtCount>intIndex And intIndex>=0 Then
						actualValue=objToolBar.GetItem(intIndex)
						If objToolBar.IsItemEnabled(strItemName) Then
									objToolBar.Press actualValue
								If err.number=0   Then
									statusTest=STATUS_PASSED
									customMsg =  FormatString(MSG_OPERATION_SUCCESSFUL, Array("Click","ToolBar Item")) 
								Else
									statusTest=STATUS_DEFECT
									customMsg = FormatString(MSG_OPERATION_UNSUCCESSFUL,  Array("Click","ToolBar Item")) 
								End If
						else
									statusTest=STATUS_FAILED
									customMsg = FormatString(MSG_OPERATION_FAILED_DISABLED, Array("ToolBar"))
						end if 
					Else
						statusTest=STATUS_FAILED
						customMsg = FormatString(MSG_OUT_OF_RANGE, Array(intIndex+1))
					End If
				Else
					statusTest=STATUS_FAILED
					customMsg = FormatString(MSG_ERR_EMPTY, Array("ToolBar"))
				End If
			Else
				statusTest=STATUS_FAILED
				customMsg = FormatString(MSG_ERR_NOT_NUMERIC, Array(intIndex))
			End If
		Else
			If IsToolBar(objToolBar) Then			
					intIndex=CInt(intIndex)
					If Err.number=0 Then
						actualtCount=objToolBar.GetItemsCount
						If Err.number=0 Then
							If  actualtCount>intIndex And intIndex>0 Then
								actualValue=objToolBar.GetItem(intIndex)								
								If  objToolBar.GetItemProperty(strItemName,"enabled")  Then
								 		objToolBar.Press actualValue
										If err.number=0  Then
											statusTest=STATUS_PASSED
											customMsg =  FormatString(MSG_OPERATION_SUCCESSFUL, Array("Click","ToolBar Item")) 
										Else
											statusTest=STATUS_DEFECT
											customMsg = FormatString(MSG_OPERATION_UNSUCCESSFUL,  Array("Click","ToolBar Item")) 
										End If															
							   else
										statusTest=STATUS_FAILED
										customMsg = FormatString(MSG_OPERATION_FAILED_DISABLED, Array("ToolBar"))
							   end if
							Else
								statusTest=STATUS_FAILED
								customMsg = FormatString(MSG_OUT_OF_RANGE, Array(intIndex))
							End If
						Else
							statusTest=STATUS_FAILED
							customMsg = FormatString(MSG_ERR_EMPTY, Array("ToolBar"))
						End If
					Else
						statusTest=STATUS_FAILED
						customMsg = FormatString(MSG_ERR_NOT_NUMERIC, Array(intIndex))
					End If	
			else
				statusTest=STATUS_FAILED
				customMsg = FormatString(MSG_INVALID_ACTION, Array("ToolBar"))
			End If
			
		End If
	Else
		statusTest=STATUS_FAILED
		customMsg = FormatString(MSG_OBJECT_NOT_EXISTS, Array("ToolBar"))
	End If
	ClickNetToolBarItemByIndex=statusTest
	Call writeActionStatusToLog(statusTest,customMsg)
	'MsgBox statusTest&" "&customMsg&" "&Err.Description 
	On Error Goto 0
End Function

Function SelectNetToolBarItemByName(byval objToolBar,byval arrItems)          
	On Error Resume Next
	Dim actualtCount,strActualValue,strShowDrop
	Dim statusTest,customFlag
	Dim customMsg,lenArr
	customFlag=true 
	If IsArray(arrItems) and not IsEmpty(arrItems) Then
			strShowDrop=arrItems(0)
	End If
	If CheckObjectExist(objToolBar) Then
		If IsToolStrip(objToolBar) or IsMenuStrip(objToolBar) Then
						If  objToolBar.ItemExists(strShowDrop) Then
							lenArr=uBound(arrItems)
	                            For n=0 to lenArr
										If n=0 Then
											strActualValue=arrItems(n)
										else
												strActualValue=strActualValue&";"&arrItems(n)
										End If
										If  not objToolBar.ItemExists(strActualValue) Then
												customFlag=false
												Exit for
										end if 
								Next	
								If  customFlag=true Then
									  If objToolBar.IsItemEnabled(strShowDrop) and objToolBar.IsItemEnabled(strActualValue) Then 
											objToolBar.ShowDropdown strShowDrop											
											If lenArr>0 Then
										   		objToolBar.Select strActualValue
										   	End If										   										   
										   If err.number=0   Then
												statusTest=STATUS_PASSED
												customMsg =  FormatString(MSG_OPERATION_SUCCESSFUL, Array("Select","ToolBar Item")) 
											else
												statusTest=STATUS_DEFECT
												customMsg = FormatString(MSG_OPERATION_UNSUCCESSFUL,  Array("Select","ToolBar Item")) 
											End If
										else
										     statusTest=STATUS_FAILED
											 customMsg = FormatString(MSG_OPERATION_FAILED_DISABLED, Array("ToolBar item"))
										End If
								else
									statusTest=STATUS_FAILED
									customMsg ="Specified toolbar item is not exist" 'FormatString(MSG_INVALID_ACTION, Array("ToolBar"))
								End If
						else
									statusTest=STATUS_FAILED
									customMsg ="Specified toolbar item is not exist" 'FormatString(MSG_INVALID_ACTION, Array("ToolBar"))
						End If
						
		Else
				statusTest=STATUS_FAILED
				customMsg = FormatString(MSG_INVALID_ACTION, Array("ToolBar"))
		End If
	Else
		statusTest=STATUS_FAILED
		customMsg = FormatString(MSG_OBJECT_NOT_EXISTS, Array("ToolBar"))
	End If
	SelectNetToolBarItemByName=statusTest
	Call writeActionStatusToLog(statusTest,customMsg)
	'sgBox statusTest&" "&customMsg&" "&Err.description
	On Error Goto 0
End Function


Function VerifyAllNetToolBarItemsByName(byval objToolBar,byval arrItems)          
	On Error Resume Next
	Dim arrActualItems,strActual,strExp
	Dim statusTest,customFlag
	Dim customMsg
	customFlag=true 
	If CheckObjectExist(objToolBar) Then
		If IsToolBar(objToolBar) Then
						arrActualItems=GetITooBartems(objToolBar)
						If not IsNull(arrActualItems) Then
							'strActual=PrintArrayElements(arrActualItems)
							'strExp=PrintArrayElements(arrItems)
							'msgbox "Actual :"&strActual&" Exp :"&strExp
							If CompareArraysCaseSensitive(arrItems,arrActualItems)=0 Then
                                statusTest=STATUS_PASSED
								customMsg=FormatString(MSG_OPERATION_SUCCESSFUL,Array("Verify","ToolItem(s)"))
							else
								statusTest=STATUS_DEFECT
								customMsg=FormatString(MSG_OPERATION_UNSUCCESSFUL,Array("Verify","ToolItem(s)"))
							End If
						else
								statusTest=STATUS_FAILED
								customMsg = FormatString(MSG_ERR_EMPTY, Array("ToolBar"))
						end if 
		Else
				statusTest=STATUS_FAILED
				customMsg = FormatString(MSG_INVALID_ACTION, Array("ToolBar"))
		End If
	Else
		statusTest=STATUS_FAILED
		customMsg = FormatString(MSG_OBJECT_NOT_EXISTS, Array("ToolBar"))
	End If
	VerifyAllNetToolBarItemsByName=statusTest
	Call writeActionStatusToLog(statusTest,customMsg)
	'MsgBox statusTest&" "&customMsg&" "&Err.description
	On Error Goto 0
End Function

Function GetITooBartems(byval objToolBar)
   On Error Resume Next
   Dim intItemCount,n,intItemIndex
   intItemCount=objToolBar.GetItemsCount
   Dim arrItem()
   ReDim arrItem(intItemCount-1)
   intItemIndex=1
   For n=0 to intItemCount
		arrItem(n)=objToolBar.GetItem(intItemIndex)
		intItemIndex=intItemIndex+1
   Next
   If IsEmpty(arrItem) Then
	   GetITooBartems=null
   else
	GetITooBartems=arrItem
   End If
  On Error Goto 0
End Function


Function IsToolBar(byval objToolBar)
   On Error Resume Next
	If objToolBar.GetROProperty("swftypename")="System.Windows.Forms.ToolBar" Then
		IsToolBar=True
	Else
		IsToolBar=False
	End If
	On Error Goto 0
End Function
Function IsMenuStrip(byval objToolBar)
   On Error Resume Next
	If objToolBar.GetROProperty("swftypename")="System.Windows.Forms.MenuStrip" Then
		IsMenuStrip=True
	Else
		IsMenuStrip=False
	End If
	On Error Goto 0
End Function
Function IsToolStrip(byval objToolBar)
   On Error Resume Next
	If objToolBar.GetROProperty("swftypename")="System.Windows.Forms.ToolStrip" Then
		IsToolStrip=True
	Else
		IsToolStrip=False
	End If
	 On Error Goto 0
End Function
