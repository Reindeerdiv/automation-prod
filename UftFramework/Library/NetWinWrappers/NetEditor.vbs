'=======================Code Section Begin============================================

Option Explicit		 'Forces explicit declaration of all variables in a script. 

'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :			  ClickNetEditor()
'	Purpose :				  Performs  Click operation on a specified editor.
'	Return Value :		 	  Integer(0/1)
'	Input Arguments :  	 	  objEditor
'------------------------------------------------------------------------------------------------------------------------------------------
Function ClickNetEditor(ByVal objEditor)
	On Error Resume Next
	'Variable Section Begin	
	Dim  customMsg  ,objectName
	Dim intStatus
	'Variable Section End
	
	If CheckObjectExist(objEditor) Then 
		If CheckObjectEnabled(objEditor) Then
			If CheckObjectClick(objEditor) Then
				intStatus = STATUS_PASSED
				customMsg = FormatString(MSG_OPERATION_SUCCESSFUL,Array("Click","Editor"))
			Else
				intStatus = STATUS_DEFECT
				customMsg = FormatString(MSG_OPERATION_UNSUCCESSFUL,Array("Click","Editor"))
			End If 
		Else			
			intStatus = STATUS_DEFECT
			customMsg = FormatString(MSG_OPERATION_FAILED_DISABLED,Array("Click","Editor"))
		End If
	Else              
		intStatus = STATUS_FAILED
		customMsg = FormatString(MSG_OBJECT_NOT_EXISTS,Array("Editor"))
	End If
	ClickNetEditor = intStatus
	Call WriteActionStatusToLog(intStatus, customMsg)
	On Error Goto 0
End Function


'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :			  SetValueInNetEditor()
'	Purpose :				  Performs  Click operation on a specified editor.
'	Return Value :		 	  Integer(0/1)
'	Input Arguments :  	 	  objEditor
'------------------------------------------------------------------------------------------------------------------------------------------
Function SetValueInNetEditor(ByVal objEditor, ByVal value, ByVal rowNo, ByVal columnNo)
	On Error Resume Next
	'Variable Section Begin	
	Dim  customMsg  ,objectName
	Dim intStatus , intRow , intColumn
	Dim strMethod , BlnSetCursor
	
	BlnSetCursor = true
	strMethod = "SetValueInNetEditor"
	'Variable Section End
	
	If Trim(rowNo) = "" Then
		intRow = 0
		BlnSetCursor = false
		WriteStatusToLogs "The row data is empty. Hence the cursor would not be set."
	ElseIf IsPositiveInteger(rowNo) <> 0 Then
		SetValueInNetEditor = 1
		WriteStatusToLogs  "The row data '" & rowNo & "' is not a valid integer, please verify. Row number starts from 1."
		Exit Function
	Else
		intRow = Cint(rowNo)
	End if
			
	If Trim(columnNo) = "" Then
		BlnSetCursor = false
		WriteStatusToLogs "The column data is empty. Hence the cursor would not be set."
	ElseIf IsPositiveInteger(columnNo) <> 0 Then
		SetValueInNetEditor = 1
		WriteStatusToLogs  "The column data '" & columnNo & "' is not a valid integer, please verify. Column number starts from 1."
		Exit Function
	Else
		intColumn = Cint(columnNo)
	End if
				
	If CheckObjectExist(objEditor) Then 
		If CheckObjectEnabled(objEditor) Then
			''Row,column in QTP starts from 0 hence doing -1
			If BlnSetCursor Then				
				objEditor.SetCaretPos intRow-1,intColumn-1				
			End If
			
			If err.number = 0 then
				objEditor.Type value
				If err.number = 0 then
					SetValueInNetEditor = 0
					WriteStatusToLogs  "Set the value '"&value&"'"
					Exit Function
				Else
					SetValueInNetEditor = 1
					WriteStatusToLogs  "An error occurred while setting the value " &value&" , please verify. Error: "& err.description
					Exit Function
				End if
			Else
				SetValueInNetEditor = 1
				WriteStatusToLogs  "An error occurred while setting the cursor position in the editor, please verify. Error: "& err.description
				Exit Function
			End If			
		Else			
			intStatus = STATUS_DEFECT
			customMsg = FormatString(MSG_OPERATION_FAILED_DISABLED,Array("Click","Editor"))
		End If
	Else              
		intStatus = STATUS_FAILED
		customMsg = FormatString(MSG_OBJECT_NOT_EXISTS,Array("Editor"))
	End If
	SetValueInNetEditor = intStatus
	Call WriteActionStatusToLog(intStatus, customMsg)
	On Error Goto 0
End Function

'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :			  SetSelectionAndStoreInNetEditor()
'	Purpose :				  Performs  Click operation on a specified editor.
'	Return Value :		 	  Integer(0/1)
'	Input Arguments :  	 	  objEditor
'------------------------------------------------------------------------------------------------------------------------------------------
Function SetSelectionAndStoreInNetEditor(ByVal objEditor, ByVal strKey, ByVal LineFrom, ByVal ColumnFrom, ByVal LineTo,ByVal ColumnTo)
	On Error Resume Next
	'Variable Section Begin	
	Dim  customMsg , intStatus
	Dim intLineFrom,intColumnFrom,intLineTo,intColumnTo
	Dim strMethod ,strSelection,intval
	
	'Variable Section End
	
	strMethod = strKeywordForReporting
	intStatus = STATUS_FAILED
	
	If IsKey(strKey) <> 0 Then
		SetSelectionAndStoreInNetEditor = 1
		WriteStatusToLogs "The key is invalid. Please verify." 
	End IF
	
	If IsPositiveInteger(LineFrom) <> 0 Then
		SetSelectionAndStoreInNetEditor = STATUS_FAILED
		WriteStatusToLogs  "The LineFrom '" & LineFrom & "' is not a valid integer, please verify. Line from number starts from 1."
		Exit Function
	Else
		intLineFrom = Cint(LineFrom)
	End if
	
	If IsPositiveInteger(ColumnFrom) <> 0 Then
		SetSelectionAndStoreInNetEditor = STATUS_FAILED
		WriteStatusToLogs  "The ColumnFrom '" & ColumnFrom & "' is not a valid integer, please verify. ColumnFrom number starts from 1."
		Exit Function
	Else
		intColumnFrom = Cint(ColumnFrom)
	End if
	
	If IsPositiveInteger(LineTo) <> 0 Then
		SetSelectionAndStoreInNetEditor = STATUS_FAILED
		WriteStatusToLogs  "The LineTo '" & LineTo & "' is not a valid integer, please verify. LineTo number starts from 1."
		Exit Function
	Else
		intLineTo = Cint(LineTo)
	End if
	
	If IsPositiveInteger(ColumnTo) <> 0 Then
		SetSelectionAndStoreInNetEditor = STATUS_FAILED
		WriteStatusToLogs  "The ColumnTo '" & ColumnTo & "' is not a valid integer, please verify. ColumnTo number starts from 1."
		Exit Function
	Else
		intColumnTo = Cint(ColumnTo)
	End if
	
	If CheckObjectExist(objEditor) Then 
		If CheckObjectEnabled(objEditor) Then
			''Row,column in QTP starts from 0 hence doing -1
			objEditor.SetSelection intLineFrom-1,intColumnFrom-1,intLineTo-1,intColumnTo
			strSelection = objEditor.GetROProperty("selection")
			If err.number <>0 then
				SetSelectionAndStoreInNetEditor = 1
				WriteStatusToLogs  "An error occurred, please verify ."&err.description
				Exit Function
			Else
				intval = AddItemToStorageDictionary(strKey,strSelection)
				If  intval = 0 Then
					SetSelectionAndStoreInNetEditor = 0
					WriteStatusToLogs "The selected data '"&strSelection&"' is stored successfully in the Key '"&strKey&"'."
					Exit Function		 
							  
				Else
					SetSelectionAndStoreInNetEditor = 1
					WriteStatusToLogs "Failed to store the selected data in the Key '"&strKey&"'" 
					Exit Function		  
				End If	
			End if
					
		Else			
			intStatus = STATUS_DEFECT
			customMsg = FormatString(MSG_OPERATION_FAILED_DISABLED,Array("Click","Editor"))
		End If
	Else              
		intStatus = STATUS_FAILED
		customMsg = FormatString(MSG_OBJECT_NOT_EXISTS,Array("Editor"))
	End If
	SetSelectionAndStoreInNetEditor = intStatus
	Call WriteActionStatusToLog(intStatus, customMsg)
	On Error Goto 0
End Function