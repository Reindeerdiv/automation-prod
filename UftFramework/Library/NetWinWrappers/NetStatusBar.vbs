'=======================Code Section Begin============================================

Option Explicit		 'Forces explicit declaration of all variables in a script. 

'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :			  VerifyStatusBarItemsCount()
'	Purpose :				  		Verify  status bar item count.
'	Return Value :		 	  	Integer( 0/1)
'	Input Arguments :  	 	objStatusBar,intItemCount
'------------------------------------------------------------------------------------------------------------------------------------------
Function VerifyStatusBarItemsCount(byval objStatusBar,byval intItemCount)
	On Error Resume Next
	Dim actualtCount
	Dim blnstatus,statusTest
	Dim customMsg 
	blnstatus=False
	If CheckObjectExist(objStatusBar) Then	
		actualtCount=objStatusBar.GetItemsCount
		If Err.Number=0 Then
			intItemCount=CInt(intItemCount)
			If VarType(intItemCount)=vbInteger And Err.number=0 Then
				blnstatus = True
			End If
			If blnstatus = True Then
				If actualtCount=intItemCount Then
					statusTest=STATUS_PASSED
					customMsg = FormatString(MSG_VALUE_MATCHES, Array(intItemCount,actualtCount)) 
				Else
					statusTest=STATUS_DEFECT
					customMsg = FormatString(MSG_VALUE_NOT_MATCHES, Array(intItemCount,actualtCount)) 
				End If
			Else    
				statusTest=STATUS_FAILED
				customMsg = FormatString(MSG_ERR_NOT_NUMERIC, Array("expected items count"))
			End If
		Else
			statusTest=STATUS_FAILED
			customMsg = FormatString(MSG_ERR_FETCH_DATA, Array(intItemCount))
		End If 
	Else
		statusTest=STATUS_FAILED
		customMsg = FormatString(MSG_OBJECT_NOT_EXISTS, Array("StatusBar"))
	End If
	VerifyStatusBarItemsCount=statusTest
	Call writeActionStatusToLog(statusTest,customMsg)
	'msgbox statusTest&" "&customMsg&" "&err.description
	On Error Goto 0
End Function
'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :			  VerifyAllNetStatusBarItems()
'	Purpose :				  		Verify  status bar items.
'	Return Value :		 	  	Integer( 0/1)
'	Input Arguments :  	 	objStatusBar,arrItems
'------------------------------------------------------------------------------------------------------------------------------------------
Function VerifyAllNetStatusBarItems(byval objStatusBar,byval arrItems)
	On Error Resume Next
	Dim arrActulItem,strActual,strExpected
	Dim blnstatus,statusTest
	Dim customMsg 
	blnstatus=False
	If CheckObjectExist(objStatusBar) Then
		arrActulItem=objStatusBar.GetROProperty("all items")
		arrActulItem=GetArray(arrActulItem,Chr(10))
		If Err.number=0 Then
			blnstatus = True
		End If
		If blnstatus = True Then
			strActual=PrintArrayElements(arrActulItem)
			strExpected=PrintArrayElements(arrItems)
			If CompareArraysCaseSensitive(arrActulItem,arrItems)=0 Then
				statusTest=STATUS_PASSED
				customMsg = FormatString(MSG_VALUE_MATCHES, Array(strExpected,strActual)) 
			Else
				statusTest=STATUS_DEFECT
				customMsg = FormatString(MSG_VALUE_NOT_MATCHES, Array(strExpected,strActual)) 
			End If
		Else    
			statusTest=STATUS_FAILED
			'customMsg = "Actual value in StatusBar is empty."'FormatString(MSG_ERR_NOT_NUMERIC, Array("expected count"))
			customMsg = FormatString(MSG_ERR_EMPTY, Array("StatusBar"))
		End If
	Else
		statusTest=STATUS_FAILED
		customMsg = FormatString(MSG_OBJECT_NOT_EXISTS, Array("StatusBar"))
	End If
	VerifyAllNetStatusBarItems=statusTest
	Call writeActionStatusToLog(statusTest,customMsg)
	'msgbox statusTest&" "&customMsg&" "&err.description
	On Error Goto 0
End Function
'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :			  VerifyStatusBarItemByIndex()
'	Purpose :				  		Verify  status bar item by item index.
'	Return Value :		 	  	Integer( 0/1)
'	Input Arguments :  	 	objStatusBar,intIndex,strExpName
'------------------------------------------------------------------------------------------------------------------------------------------
Function VerifyStatusBarItemByIndex(byval objStatusBar,byval intIndex,byval strExpName)
	On Error Resume Next
	Dim strActual,intCount
	Dim blnstatus,statusTest
	Dim customMsg 
	blnstatus=False
	intIndex=CInt(intIndex)-1
	If Err.number=0 Then
		If CheckObjectExist(objStatusBar) Then
			intCount=objStatusBar.GetItemsCount
			If intCount>intIndex And intIndex>=0 Then			
				strActual=objStatusBar.GetItem(intIndex)
				If Err.number=0 Then
					blnstatus = True
				End If
				If blnstatus = True Then				
					If CompareActualStringValues(strExpName,strActual)=0  Then
						statusTest=STATUS_PASSED
						customMsg = FormatString(MSG_VALUE_MATCHES, Array(strExpName,strActual)) 
					Else
						statusTest=STATUS_DEFECT
						customMsg = FormatString(MSG_VALUE_NOT_MATCHES, Array(strExpName,strActual)) 
					End If
				Else    
					statusTest=STATUS_FAILED
					customMsg = "Actual value in StatusBar is empty."'FormatString(MSG_ERR_NOT_NUMERIC, Array("expected count"))
				End If
			Else
				statusTest=STATUS_FAILED
				customMsg = FormatString(MSG_OUT_OF_RANGE, Array(intIndex+1))
			End If 
		Else
			statusTest=STATUS_FAILED
			customMsg = FormatString(MSG_OBJECT_NOT_EXISTS, Array("StatusBar"))
		End If
	Else
		statusTest=STATUS_FAILED
		customMsg = FormatString(MSG_ERR_NOT_NUMERIC, Array("expected count"))
	End If
	
	VerifyStatusBarItemByIndex=statusTest
	Call writeActionStatusToLog(statusTest,customMsg)
	'msgbox statusTest&" "&customMsg&" "&err.description
	On Error Goto 0
End Function
'=======================Code Section End=============================================