'========================Code Section Begin=====================================================================
'Option Explicit 'Forces explicit declaration of all variables in a script.
'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :			VerifyNetTableRowCount()
'	Purpose :				Verify table row count.	
'	Return Value :		 	Integer(0/1)
'	Arguments :
'		Input Arguments :   objTab,expCount   
'		Output Arguments :  Status
Function VerifyNetTableRowCount(byval objTab,ByVal expCount)
	On Error Resume Next
	Dim actualtCount,intRowCount
	Dim blnstatus,statusTest
	Dim customMsg 
	blnstatus=False
	If CheckObjectExist(objTab) Then
		intRowCount=CInt(expCount)
		actualtCount=objTab.RowCount
		If VarType(intRowCount)=vbInteger And Err.number=0 Then
			blnstatus = True
		End If
		If blnstatus = True Then
			If actualtCount=intRowCount Then
				VerifyNetTableRowCount = STATUS_PASSED
				statusTest=STATUS_PASSED
				customMsg = FormatString(MSG_VALUE_MATCHES, Array(expCount,actualtCount)) 
				'   customMsg="Expected Value : "&expCount&" and "&" Actual Value : "&actualtCount&" matches as expected." 'MSG_VALUE_MATCHES
			Else
				VerifyNetTableRowCount = STATUS_DEFECT
				statusTest=STATUS_DEFECT
				customMsg = FormatString(MSG_VALUE_NOT_MATCHES, Array(expCount,actualtCount)) 
				'customMsg = "Expected Value : "&expCount& """" &  " and " &""""&"Actual Value : "&actualtCount&""""&" does not match, Please verify." 'MSG_VALUE_NOT_MATCHES
			End If
		Else	
			VerifyNetTableRowCount = STATUS_FAILED
			statusTest=STATUS_FAILED
			customMsg = FormatString(MSG_ERR_NOT_NUMERIC, Array("expected count"))
			
		End If
	Else
		VerifyNetTableRowCount= STATUS_FAILED
		statusTest=STATUS_FAILED
		customMsg = FormatString(MSG_OBJECT_NOT_EXISTS, Array("Table"))
	End If
	
	Call writeActionStatusToLog(statusTest,customMsg)
	
	On Error Goto 0
End Function
'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :			VerifyNetTableColumnCount()
'	Purpose :				Verify table column count.
'	Return Value :		 	Integer(0/1)
'	Arguments :
'		Input Arguments :   objTab,expCount   
'		Output Arguments :  Status
Function VerifyNetTableColumnCount(byval objTab,ByVal expCount)
	On Error Resume Next
	Dim actualtCount,intRowCount
	Dim blnstatus,statusTest
	Dim customMsg 
	blnstatus=False
	If CheckObjectExist(objTab) Then
		intRowCount=CInt(expCount)
		actualtCount=objTab.ColumnCount
		If VarType(intRowCount)=vbInteger And Err.number=0 Then
			blnstatus = True
		End If
		If blnstatus = True Then
			If actualtCount=intRowCount Then
				VerifyNetTableColumnCount = STATUS_PASSED
				statusTest=STATUS_PASSED
				customMsg = FormatString(MSG_VALUE_MATCHES, Array(expCount,actualtCount)) 
				'customMsg="Expected Value : "&expCount&" and "&" Actual Value : "&actualtCount&" matches as expected." 'MSG_VALUE_MATCHES
			Else
				VerifyNetTableColumnCount = STATUS_DEFECT
				statusTest=STATUS_DEFECT
				customMsg = FormatString(MSG_VALUE_NOT_MATCHES, Array(expCount,actualtCount)) 
				'customMsg = "Expected Value : "&expCount& """" &  " and " &""""&"Actual Value : "&actualtCount&""""&" does not match, Please verify." 'MSG_VALUE_NOT_MATCHES
			End If
		Else	
			VerifyNetTableColumnCount = STATUS_FAILED
			statusTest=STATUS_FAILED
			customMsg = FormatString(MSG_ERR_NOT_NUMERIC, Array("intExpCount"))
			
		End If
	Else
		VerifyNetTableColumnCount= STATUS_FAILED
		statusTest=STATUS_FAILED
		customMsg = FormatString(MSG_OBJECT_NOT_EXISTS, Array("Table"))
	End If
	
	Call writeActionStatusToLog(statusTest,customMsg)
	On Error Goto 0
End Function

'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :			VerifyNetTableCellData()
'	Purpose :				Verify tabel specific cell data.
'	Return Value :		 	Integer(0/1)
'	Arguments :
'	Input Arguments :   objTab,intRowIndex,intColumnName,strExpValue   
'	Output Arguments :  Status
Function VerifyNetTableCellData(byval objTab,byval intRowIndex,byval intColumnName,byval strExpValue)
	On Error Resume Next
	Dim stractualtData
	Dim blnstatus,statusTest
	Dim customMsg 
	blnstatus=True
	Dim rowNumber,colNumber
	If CheckObjectExist(objTab) Then
		intRowIndex=CInt(intRowIndex)-1
		If Err.Number=0 Then			
			intColumnName=CInt(intColumnName)-1
			'MsgBox "Row :"&intRowIndex&" Column"&intColumnName
			'blnstatus=objTab.GetROProperty("Enabled")
			If Err.Number=0 Then
				'			If vartype(intRowIndex)=VbInteger and err.number=0 Then
				'					blnstatus = true
				'			End If
				If blnstatus = True Then
					rowNumber=objTab.RowCount
					colNumber=objTab.ColumnCount
					
					If rowNumber>intRowIndex And colNumber>intColumnName And intRowIndex >=0 And intColumnName>=0 Then
						stractualtData=objTab.GetCellData(intRowIndex,intColumnName)
						If Not IsNull(stractualtData)Then
							If stractualtData = strExpValue Then
								VerifyNetTableCellData = STATUS_PASSED
								statusTest=STATUS_PASSED
								customMsg = FormatString(MSG_VALUE_MATCHES, Array(stractualtData,strExpValue)) 
								'customMsg="Expected Value : "&stractualtData&" and "&" Actual Value : "&strExpValue&" matches as expected." 'MSG_VALUE_MATCHES
							Else
								VerifyNetTableCellData = STATUS_DEFECT
								statusTest=STATUS_DEFECT
								customMsg = FormatString(MSG_VALUE_NOT_MATCHES, Array(strExpValue,stractualtData)) 
								'customMsg = "Expected Value : "&stractualtData& """" &  " and " &""""&"Actual Value : "&strExpValue&""""&" does not match, Please verify." 'MSG_VALUE_NOT_MATCHES
							End If
						Else
							VerifyNetTableCellData = STATUS_DEFECT
							statusTest=STATUS_DEFECT
							customMsg = FormatString(MSG_VALUE_NOT_MATCHES, Array(strExpValue,stractualtData)) 
						End If 
						
					Else
						VerifyNetTabelCellData = STATUS_FAILED
						statusTest=STATUS_FAILED
						customMsg = FormatString(MSG_OUT_OF_RANGE, Array("row index"))
						'customMsg =   "An error occurred; expCount is empty or not number" 'MSG_ERR_NOT_NUMERIC
					End If
				Else	
					
					VerifyNetTableCellData = STATUS_FAILED
					statusTest=STATUS_FAILED
					customMsg = FormatString(MSG_OPERATION_FAILED_DISABLED, Array("Select", "Table"))	
					'customMsg =   "An error occurred; intRowIndex is greater then table row index" 'MSG_OUT_OF_RANGE
					
				End If
			Else
				VerifyNetTableCellData = STATUS_FAILED
				statusTest=STATUS_FAILED
				customMsg = FormatString(MSG_ERR_NOT_NUMERIC, Array("column index"))
			End If 
		Else				
				VerifyNetTableCellData = STATUS_FAILED
				statusTest=STATUS_FAILED
				customMsg = FormatString(MSG_ERR_NOT_NUMERIC, Array("row index"))
		End If
		
	Else
		VerifyNetTableCellData= STATUS_FAILED
		statusTest=STATUS_FAILED
		customMsg = FormatString(MSG_OBJECT_NOT_EXISTS, Array("Table"))
		'customMsg =       "The specified Table does not exist, please verify." 'MSG_OBJECT_NOT_EXISTS
	End If
	VerifyNetTableCellData=statusTest
	Call writeActionStatusToLog(statusTest,customMsg)
	
	On Error Goto 0
	
End Function

'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :			SelectSingleColumn()
'	Purpose :				Select specific column.
'	Return Value :		 	Integer(0/1)
'	Arguments :
'	Input Arguments :   objTab,strColumnName   
'	Output Arguments :  Status
Function SelectSingleColumn(byval objTable,byval strColumnName)
	On Error Resume Next
	Dim strActualtData,intColumnCount
	Dim blnstatus,statusTest
	Dim customMsg 
	blnstatus=True
	If CheckObjectExist(objTable) Then
		strColumnName=CInt(strColumnName)
		If Err.Number=0 Then
			intColumnCount=objTable.ColumnCount
			If intColumnCount>strColumnName And strColumnName>=0 Then
				'blnstatus=objTable.GetROProperty("Enabled")
				If blnstatus=True Then
					objTable.SelectColumn strColumnName-1
					If Err.number=0 Then
						SelectSingleColumn = STATUS_PASSED
						statusTest=STATUS_PASSED
						customMsg=FormatString(MSG_OPERATION_SUCCESSFUL, Array("Select", "Table Column:"&strColumnName))
						'customMsg="Expected  column name: "&strColumnName&" is selected sucessfully." 'MSG_OPERATION_SUCCESSFUL
					Else
						SelectSingleColumn = STATUS_DEFECT
						statusTest=STATUS_DEFECT
						customMsg=FormatString(MSG_OPERATION_UNSUCCESSFUL, Array("Select", "Table Column:"&strColumnName))
						'customMsg = "Expected  column could not selected, Please verify." 'MSG_OPERATION_UNSUCCESSFUL
					End If
				Else
					SelectSingleColumn = STATUS_DEFECT
					statusTest=STATUS_DEFECT
					customMsg=FormatString(MSG_OPERATION_UNSUCCESSFUL, Array("Select", "Table Column:"&strColumnName))
					'customMsg = "The NetTable was not in enabled state, Please verify." 'MSG_OPERATION_FAILED_DISABLED
				End If
			Else
				VerifyNetTabelCellData = STATUS_FAILED
				statusTest=STATUS_FAILED
				customMsg = FormatString(MSG_OUT_OF_RANGE, Array("column index"))	
			End If
		Else
			VerifyNetTableCellData = STATUS_FAILED
			statusTest=STATUS_FAILED
			customMsg = FormatString(MSG_ERR_NOT_NUMERIC, Array("column index"))
		End If	
		
	Else
		SelectSingleColumn= STATUS_FAILED
		statusTest=STATUS_FAILED
		customMsg = FormatString(MSG_OBJECT_NOT_EXISTS, Array("Table"))
	End If
	Call writeActionStatusToLog(statusTest,customMsg)
	On Error Goto 0
	
End Function

'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :			SelectSingleRow()
'	Purpose :				Select specific row.
'	Return Value :		 	Integer(0/1)
'	Arguments :
'	Input Arguments :   objTab,intRowIndex   
'	Output Arguments :  Status
Function SelectSingleRow(byval objTable,byval intRowIndex)
	On Error Resume Next
	Dim intRowCount
	Dim blnstatus,statusTest
	Dim customMsg 
	blnstatus=true
	If CheckObjectExist(objTable) Then
		'blnstatus=objTable.GetROProperty("Enabled")
		intRowCount=objTable.RowCount
		intRowIndex=CInt(intRowIndex)-1	
		If Err.number =0 And intRowCount>intRowIndex And intRowIndex >=0 Then
			If blnstatus=True Then
				objTable.SelectRow intRowIndex
				If Err.number=0 Then
					SelectSingleRow = STATUS_PASSED
					statusTest=STATUS_PASSED
					customMsg=FormatString(MSG_OPERATION_SUCCESSFUL, Array("Select", "Table Row"))
				Else
					SelectSingleRow = STATUS_DEFECT
					statusTest=STATUS_DEFECT
					customMsg=FormatString(MSG_OPERATION_UNSUCCESSFUL, Array("Select", "Table row"))
				End If
			Else
				SelectSingleRow = STATUS_DEFECT
				statusTest=STATUS_DEFECT
				customMsg = FormatString(MSG_OPERATION_FAILED_DISABLED, Array("Select", "Table"))
				'customMsg = "The NetTable was not in enabled state, Please verify." 'MSG_OPERATION_FAILED_DISABLED
			End If
		Else
			SelectSingleNetTableRow= STATUS_FAILED
			statusTest=STATUS_FAILED
			customMsg = FormatString(MSG_OUT_OF_RANGE, Array("row index"))
			'customMsg = "An error occurred; expected row index is greater then actual row index or expected row index not number , Please verify." 'MSG_OUT_OF_RANGE
		End If
		
	Else
		SelectSingleRow= STATUS_FAILED
		statusTest=STATUS_FAILED
		customMsg = FormatString(MSG_OBJECT_NOT_EXISTS, Array("Table"))
		'customMsg =       "The specified Table does not exist, please verify." 'MSG_OBJECT_NOT_EXISTS
	End If
	Call writeActionStatusToLog(statusTest,customMsg)
	'msgbox 	customMsg& " "& err.number		
	On Error Goto 0
End Function

'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :			SortByColumn()
'	Purpose :				Sort by specific column.
'	Return Value :		 	Integer(0/1)
'	Arguments :
'	Input Arguments :   objTab,strColumnName,strOrder   
'	Output Arguments :  Status
Function SortByColumn(byval objTable,byval strColumnName,byval strOrder)
	On Error Resume Next
	Dim intRowCount
	Dim blnstatus,statusTest
	Dim customMsg 
	blnstatus=True
	If CheckObjectExist(objTable) Then
		'blnstatus=objTable.GetROProperty("Enabled")
		If blnstatus=True Then
			objTable.Object.Sort  strColumnName, strOrder
			If Err.number=0 Then
				SortByColumn = STATUS_PASSED
				statusTest=STATUS_PASSED
				'customMsg="Expected  Column sort by "&strOrder&"." 'MSG_OPERATION_SUCCESSFUL
				customMsg=FormatString(MSG_OPERATION_SUCCESSFUL, Array("Sort", strColumnName))
			Else
				SortByColumn = STATUS_DEFECT
				statusTest=STATUS_DEFECT
				'							customMsg = "Expected  column could not selected, Please verify." 'MSG_OPERATION_UNSUCCESSFUL
				customMsg=FormatString(MSG_OPERATION_UNSUCCESSFUL, Array("Sort", strColumnName))
			End If
		Else
			SortByColumn = STATUS_FAILED
			statusTest=STATUS_FAILED
			customMsg = FormatString(MSG_OPERATION_FAILED_DISABLED, Array("Sort", strColumnName))
			'customMsg = "The NetTable was not in enabled state, Please verify." 'MSG_OPERATION_FAILED_DISABLED
		End If
		
		
	Else
		SortByColumn= STATUS_FAILED
		statusTest=STATUS_FAILED
		customMsg = FormatString(MSG_OBJECT_NOT_EXISTS, Array("Table"))
		'customMsg =       "The specified Table does not exist, please verify." 'MSG_OBJECT_NOT_EXISTS
	End If
	Call writeActionStatusToLog(statusTest,customMsg)
	'msgbox 	customMsg& " "& err.number		
	On Error Goto 0
End Function

'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :			SetValueInCell()
'	Purpose :				Set value in specific cell
'	Return Value :		 	Integer(0/1)
'	Arguments :
'	Input Arguments :   objTab,intRowNumber,strColumnName,strSetValue   
'	Output Arguments :  Status
Function SetValueInCell(byVal objTable,byval intRowNumber,byval strColumnName,byval strSetValue)
	On Error Resume Next
	Dim intRowCount,intColNumber
	Dim blnstatus,statusTest
	Dim customMsg 
	blnstatus=True
	If CheckObjectExist(objTable) Then
		'blnstatus=objTable.GetROProperty("Enabled")
		If blnstatus=True Then
			intRowCount=objTable.RowCount
			intColNumber=objTable.ColumnCount
			intRowNumber=CInt(intRowNumber) - 1
			If Err.number=0 Then
				strColumnName=CInt(strColumnName) - 1
				If Err.number=0 Then
					If intRowCount>=intRowNumber And intColNumber>=strColumnName Then
						objTable.ActivateCell intRowNumber ,strColumnName
						objTable.SetCellData intRowNumber ,strColumnName,strSetValue
						If Err.number=0 Then
							SetValueInCell = STATUS_PASSED
							statusTest=STATUS_PASSED
							'customMsg=  "The value: "&strSetValue&" is set in Cell  Successfully." 'MSG_OPERATION_SUCCESSFUL
							customMsg=FormatString(MSG_OPERATION_SUCCESSFUL, Array("SetValueInCell", strSetValue))
						Else
							SetValueInCell = STATUS_DEFECT
							statusTest=STATUS_DEFECT
							'customMsg="The value: "&strSetValue&" is not set in Cell, Please verify." 'MSG_OPERATION_UNSUCCESSFUL
							customMsg=FormatString(MSG_OPERATION_UNSUCCESSFUL, Array("SetValueInCell", strSetValue))
						End If
					Else
						SetValueInCell = STATUS_FAILED
						statusTest=STATUS_FAILED
						customMsg = FormatString(MSG_OUT_OF_RANGE, Array("row or column index"))
						'customMsg = "The intRowNumber is greater then actual row count, Please verify." 'MSG_OUT_OF_RANGE
					End If
				Else
					SetValueInCell = STATUS_FAILED
					statusTest=STATUS_FAILED
					'customMsg = "The intRowNumber in not number or empty, Please verify." 'MSG_ERR_NOT_NUMERIC
					customMsg = FormatString(MSG_ERR_NOT_NUMERIC, Array("column index"))
				End If
			Else
				SetValueInCell = STATUS_FAILED
				statusTest=STATUS_FAILED
				'customMsg = "The intRowNumber in not number or empty, Please verify." 'MSG_ERR_NOT_NUMERIC
				customMsg = FormatString(MSG_ERR_NOT_NUMERIC, Array("row index"))
			End If
		Else
			SetValueInCell = STATUS_FAILED
			statusTest=STATUS_FAILED
			customMsg = FormatString(MSG_OPERATION_FAILED_DISABLED, Array("Verify", "Table"))
			'customMsg = "The NetTable was not in enabled state, Please verify." 'MSG_OPERATION_FAILED_DISABLED
		End If
	Else
		SetValueInCell= STATUS_FAILED
		statusTest=STATUS_FAILED
		customMsg = FormatString(MSG_OBJECT_NOT_EXISTS, Array("Table"))
		'customMsg =       "The specified Table does not exist, please verify." 'MSG_OBJECT_NOT_EXISTS
	End If
	Call writeActionStatusToLog(statusTest,customMsg)
	'msgbox 	customMsg& " "& err.number		
	On Error Goto 0
End Function
'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :			ScrollToCell()
'	Purpose :				Set value in specific cell
'	Return Value :		 	Integer(0/1)
'	Arguments :
'	Input Arguments :   objTab,intRowNumber,intColumnNumber  
'	Output Arguments :  Status
Function ScrollToCell(Byval objTable,Byval intRowNumber,Byval intColumnNumber)
	On Error Resume Next
	Dim intRowCount,intColCount
	Dim blnstatus,statusTest
	Dim customMsg 
	blnstatus=true
	If CheckObjectExist(objTable) Then
		'blnstatus=objTable.GetROProperty("Enabled")
		If blnstatus=True Then
			intRowCount=objTable.RowCount
			intColCount=objTable.ColumnCount
			
			intRowNumber=CInt(intRowNumber) - 1
			If Err.number=0 Then
				intColumnNumber=CInt(intColumnNumber) - 1
				If Err.number=0 Then
					If intRowCount>intRowNumber And  intColCount>intColumnNumber And intRowNumber>=0 And intColumnNumber>=0 Then
						objTable.MakeCellVisible  intRowNumber,intColumnNumber
						If Err.number=0 Then
							ScrollToCell = STATUS_PASSED
							statusTest=STATUS_PASSED
							customMsg=FormatString(MSG_OPERATION_SUCCESSFUL, Array("ScrollToCell", "Table"))
							'customMsg=  "The value: "&strSetValue&" is set in Cell  Successfully." 'MSG_OPERATION_SUCCESSFUL
						Else
							ScrollToCell = STATUS_DEFECT
							statusTest=STATUS_DEFECT
							'customMsg="The value: "&strSetValue&" is not set in Cell, Please verify." 'MSG_OPERATION_UNSUCCESSFUL
							customMsg=FormatString(MSG_OPERATION_UNSUCCESSFUL, Array("ScrollToCell", "Table"))
						End If
					Else
						ScrollToCell = STATUS_FAILED
						statusTest=STATUS_FAILED
						customMsg = FormatString(MSG_OUT_OF_RANGE, Array("row or column index"))
						'customMsg = "The intRowNumber is greater then actual row count, Please verify." 'MSG_OUT_OF_RANGE
					End If
				Else
					ScrollToCell = STATUS_FAILED
					statusTest=STATUS_FAILED
					customMsg = FormatString(MSG_ERR_NOT_NUMERIC, Array("column index"))
				End If
			Else
				ScrollToCell = STATUS_FAILED
				statusTest=STATUS_FAILED
				customMsg = FormatString(MSG_ERR_NOT_NUMERIC, Array("row index"))
			End If
		Else
			ScrollToCell = STATUS_FAILED
			statusTest=STATUS_FAILED
			customMsg = FormatString(MSG_OPERATION_FAILED_DISABLED, Array("Select", "Table"))
		End If
	Else
		ScrollToCell= STATUS_FAILED
		statusTest=STATUS_FAILED
		customMsg = FormatString(MSG_OBJECT_NOT_EXISTS, Array("Table"))
	End If
	Call writeActionStatusToLog(statusTest,customMsg)
	'msgbox 	customMsg& " "& err.number	&" "&	err.description
	On Error Goto 0
End Function
'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :			ClickCell()
'	Purpose :				Set value in specific cell
'	Return Value :		 	Integer(0/1)
'	Arguments :
'	Input Arguments :   objTab,intRowIndex,intColumnIndex  
'	Output Arguments :  Status
Function ClickCell(byval objTable,byval intRowIndex,byval intColumnIndex)
	On Error Resume Next
	Dim intRowCount,intColCount
	Dim blnstatus,statusTest
	Dim customMsg 
	blnstatus=True
	If CheckObjectExist(objTable) Then
		'blnstatus=objTable.GetROProperty("Enabled")
		If blnstatus=True Then
			intRowCount=objTable.RowCount
			intColCount=objTable.ColumnCount
			
			intRowIndex=CInt(intRowIndex) - 1
			If Err.number=0 Then
				intColumnIndex=CInt(intColumnIndex) - 1
				If Err.number=0 Then
					If intRowCount>intRowIndex And  intColCount>intColumnIndex And intRowIndex >=0 And intColumnIndex>=0 Then
						objTable.ClickCell  intRowIndex,intColumnIndex
						If Err.number=0 Then
							ClickCell = STATUS_PASSED
							statusTest=STATUS_PASSED
							customMsg=FormatString(MSG_OPERATION_SUCCESSFUL, Array("Select", "Table"))
						Else
							ClickCell = STATUS_DEFECT
							statusTest=STATUS_DEFECT
							customMsg=FormatString(MSG_OPERATION_UNSUCCESSFUL, Array("Select", "Table"))
						End If
					Else
						ClickCell = STATUS_FAILED
						statusTest=STATUS_FAILED
						customMsg = FormatString(MSG_OUT_OF_RANGE, Array("row or column index"))
					End If
				Else
					ClickCell = STATUS_FAILED
					statusTest=STATUS_FAILED
					customMsg = FormatString(MSG_ERR_NOT_NUMERIC, Array("column index"))
				End If
			Else
				ClickCell = STATUS_FAILED
				statusTest=STATUS_FAILED
				customMsg = FormatString(MSG_ERR_NOT_NUMERIC, Array("row index"))
			End If
		Else
			ClickCell = STATUS_FAILED
			statusTest=STATUS_FAILED
			customMsg = FormatString(MSG_OPERATION_FAILED_DISABLED, Array("Select", "Table"))
		End If		
	Else
		ClickCell= STATUS_FAILED
		statusTest=STATUS_FAILED
		customMsg = FormatString(MSG_OBJECT_NOT_EXISTS, Array("Table"))
	End If
	Call writeActionStatusToLog(statusTest,customMsg)
	'msgbox 	customMsg& " "& err.number	&" "&	err.description
	On Error Goto 0	
End Function
'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :			VerifyAllCheckBoxSelected()
'	Purpose :				Set value in specific cell
'	Return Value :		 	Integer(0/1)
'	Arguments :
'	Input Arguments :   objTab,intCoumnNumber
'	Output Arguments :  Status
Function VerifyAllCheckBoxSelected(Byval objTable,byval intCoumnNumber)
	On Error Resume Next
	Dim intRowCount,intColCount,startIndex
	Dim blnstatus,statusTest,customFlag,errFlag
	Dim actualValue
	Dim customMsg 
	blnstatus=False
	customFlag=True
	errFlag=True
	If CheckObjectExist(objTable) Then
		intRowCount=objTable.RowCount
		intColCount=objTable.ColumnCount
		intCoumnNumber=CInt(intCoumnNumber) - 1
		If Err.number=0 Then
			If intColCount>intCoumnNumber And intCoumnNumber>=0 Then
				For startIndex=0 To intRowCount
					actualValue=objTable.GetCellData(startIndex,intCoumnNumber)
					If IsNull(actualValue)Or (actualValue <>True And actualValue <> False) Then
						errFlag=False
					End If
					If Not actualValue=True Or IsNull(actualValue) Then
						customFlag=False
						Exit For
					End If
				Next
				If errFlag=True Then				
					If customFlag=True Then
						VerifyAllCheckBoxSelected = STATUS_PASSED
						statusTest=STATUS_PASSED
						customMsg="All the check boxes are selected as expected."
						'customMsg="The value: "&strSetValue&" is  set in Cell."
					Else
						VerifyAllCheckBoxSelected = STATUS_DEFECT
						statusTest=STATUS_DEFECT
						customMsg="Not all the check boxes are selected, Please verify."
					'customMsg="The value: "&strSetValue&" is not set in Cell, Please verify."
					End If
				Else
					VerifyAllCheckBoxClear = STATUS_DEFECT
					statusTest=STATUS_DEFECT
					customMsg="Expected column not a check box, Please verify."
				End If 
			Else
				VerifyAllCheckBoxSelected= STATUS_FAILED
				statusTest=STATUS_FAILED
				customMsg = FormatString(MSG_OUT_OF_RANGE, Array("column index"))
				'customMsg ="The specified ColumnNumbre is greater then actual column index, please verify." 'MSG_OUT_OF_RANGE
			End If
		Else
			VerifyAllCheckBoxSelected= STATUS_FAILED
			statusTest=STATUS_FAILED
			customMsg = FormatString(MSG_ERR_NOT_NUMERIC, Array("column index"))
			'customMsg ="The specified ColumnNumbre is greater then actual column index, please verify." 'MSG_OUT_OF_RANGE
		End If
		
		
	Else
		VerifyAllCheckBoxSelected= STATUS_FAILED
		statusTest=STATUS_FAILED
		'customMsg ="The specified Table does not exist, please verify." 'MSG_OBJECT_NOT_EXISTS
		customMsg = FormatString(MSG_OBJECT_NOT_EXISTS, Array("Table"))
	End If
	Call writeActionStatusToLog(statusTest,customMsg)
	'msgbox 	customMsg& " "& err.number	&" "&	err.description
	On Error Goto 0
End Function


Function VerifyAllCheckBoxClear(Byval objTable,byval intCoumnNumber)
	On Error Resume Next
	Dim intRowCount,intColCount,startIndex
	Dim blnstatus,statusTest,customFlag,errFlag
	Dim actualValue
	Dim customMsg 
	blnstatus=False
	customFlag=True
	errFlag=True
	If CheckObjectExist(objTable) Then
		
		intRowCount=objTable.RowCount
		intColCount=objTable.ColumnCount
		intCoumnNumber=CInt(intCoumnNumber) - 1
		If Err.number=0 Then
			If intColCount>intCoumnNumber And intCoumnNumber>=0 Then
				For startIndex=0 To intRowCount
					actualValue=objTable.GetCellData(startIndex,intCoumnNumber)
					If IsNull(actualValue)Or (actualValue <>True And actualValue <> False) Then
						errFlag=False
					End If
					If Not actualValue=False Then
						customFlag=False
						Exit For
					End If
				Next
				If errFlag=True Then
					If customFlag=True Then
						VerifyAllCheckBoxClear = STATUS_PASSED
						statusTest=STATUS_PASSED
						customMsg="All the check boxes are cleared as expected."
						'customMsg="The value: "&strSetValue&" is  set in Cell."
					Else
						VerifyAllCheckBoxClear = STATUS_DEFECT
						statusTest=STATUS_DEFECT
						customMsg="Not all the check boxes are clear, Please verify."
						'customMsg="The value: "&strSetValue&" is not set in Cell, Please verify." 
					End If
				Else
					VerifyAllCheckBoxClear = STATUS_DEFECT
					statusTest=STATUS_DEFECT
					customMsg="Expected column not a check box, Please verify."
						
				End If
			Else
				VerifyAllCheckBoxClear= STATUS_FAILED
				statusTest=STATUS_FAILED
				customMsg = FormatString(MSG_OUT_OF_RANGE, Array("column index"))
			End If
		Else
			VerifyAllCheckBoxClear= STATUS_FAILED
			statusTest=STATUS_FAILED
			customMsg = FormatString(MSG_ERR_NOT_NUMERIC, Array("column index"))
		End If
	Else
		VerifyAllCheckBoxClear= STATUS_FAILED
		statusTest=STATUS_FAILED
		customMsg = FormatString(MSG_OBJECT_NOT_EXISTS, Array("Table"))
	End If
	Call writeActionStatusToLog(statusTest,customMsg)
	'msgbox 	customMsg& " "& err.number	&" "&	err.description
	On Error Goto 0
End Function













