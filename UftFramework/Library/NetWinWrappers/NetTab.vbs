Function VerifyTabItemCount(byval objTab,ByVal intExpCount)
   On Error Resume Next
dim actualtCount,inttabCount
Dim blnstatus,statusTest
Dim customMsg 
blnstatus=false
 If CheckObjectExist(objTab) Then
	inttabCount=cint(intExpCount)
			 actualtCount=objTab.GetItemsCount
			If vartype(inttabCount)=VbInteger Then
					blnstatus = true
			End If
			If blnstatus = true Then
					If actualtCount=inttabCount Then
									VerifyTabItemCount = STATUS_PASSED
									statusTest=STATUS_PASSED
									customMsg = FormatString(MSG_VALUE_MATCHES, Array(intExpCount,actualtCount)) 
									'customMsg= "Expected Value : "&intExpCount&" and "&" Actual Value : "&actualtCount&" matches as expected."  'MSG_VALUE_MATCHES
					else
							VerifyTabItemCount = STATUS_DEFECT
							statusTest=STATUS_DEFECT
							customMsg = FormatString(MSG_VALUE_NOT_MATCHES, Array(intExpCount,actualtCount)) 
							'customMsg = "Expected Value : "&expCount& """" &  " and " &""""&"Actual Value : "&actualtCount&""""&" does not match, Please verify." 'MSG_VALUE_NOT_MATCHES
					End If
			else	
							VerifyTabItemCount = STATUS_FAILED
							statusTest=STATUS_FAILED
							customMsg = FormatString(MSG_ERR_NOT_NUMERIC, Array("expected count"))
							'customMsg = "An error occurred; expCount is empty or not a number" 'MSG_ERR_NOT_NUMERIC
			End If
else
			VerifyTabItemCount= STATUS_FAILED
			statusTest=STATUS_FAILED
			customMsg = FormatString(MSG_OBJECT_NOT_EXISTS, Array("Tab"))
			'customMsg = "The specified Tab does not exist, Please verify." '
 End If

 Call writeActionStatusToLog(statusTest,customMsg)

  On Error GoTo 0
End Function

Function VerifyAllTabItems(byval objTab,byval arrTabItems)
	On Error Resume Next
	Dim actualItems,arrActualItems,arrExpItems
	Dim statusTest
	Dim customMsg

	If CheckObjectExist(objTab)Then
		actualItems=objTab.GetROProperty("all items")
		If Err.Number=0 Then
			If Not IsEmpty(arrTabItems) Then
				arrActualItems=GetArray(actualItems,chr(10))
			   'arrExpItems=GetArray(arrTabItems,"^")
			   arrExpItems=arrTabItems
			   	If MatchEachArrDataCS(arrExpItems, arrActualItems) = True Then
							VerifyAllTabItems = STATUS_PASSED
							statusTest=STATUS_PASSED
							customMsg = FormatString(MSG_ITEMS_MATCHES, Array("Tab items","Tab items"))
							'customMsg = "The specified tab items matches with the expected tab items."
				Else
							VerifyAllTabItems = STATUS_DEFECT
							statusTest=STATUS_DEFECT
							customMsg = FormatString(MSG_ITEMS_NOT_MATCHES, Array("Tab items","Tab items"))
							'customMsg = "The specified tab items does not match the expected tab items."
				End If
			Else
				VerifyAllTabItems = STATUS_FAILED
				statusTest=STATUS_FAILED
				customMsg = FormatString(MSG_ERR_EMPTY, Array("Tab"))
				'customMsg = "Tab does NOT contain any item, Please Verify."
			End If
			
		else
				VerifyAllTabItems = STATUS_FAILED
				statusTest=STATUS_FAILED
				customMsg = FormatString(MSG_ERR_FETCH_DATA, Array("Tab items"))
				'customMsg="Failed to retrieve all the items of the Tab."
		End If
		
	else
			VerifyAllTabItems=STATUS_FAILED
			statusTest=STATUS_FAILED
			customMsg = FormatString(MSG_OBJECT_NOT_EXISTS, Array("Tab"))
			'customMsg="The specified Tab does not exist, Please verify."
	End If
	Call writeActionStatusToLog(statusTest,customMsg)
	On Error GoTo 0
End Function

Function VerifySelectedTab(byval objTab,byval strExpName)
On Error Resume Next
Dim actualName,customMsg
Dim blnstatus, statusTest
blnstatus =false
If CheckObjectExist(objTab) Then
	   If Vartype(strExpName) <> vbEmpty  And Err.Number = 0  Then
					blnstatus = true
			End If
		 actualName=objTab.GetSelection
		 If blnstatus=true  Then
			If CompareActualStringValues(actualName, strExpName) = 0 Then
						  VerifySelectedTab = STATUS_PASSED
						  statusTest=STATUS_PASSED
						  customMsg = FormatString(MSG_VALUE_MATCHES, Array(strExpName,actualName))
			else
							VerifySelectedTab = STATUS_DEFECT
							statusTest=STATUS_DEFECT
							customMsg = FormatString(MSG_VALUE_NOT_MATCHES, Array(strExpName,actualName))
			
			End If
		 else
			VerifySelectedTab = STATUS_FAILED
			statusTest=STATUS_FAILED
			customMsg = FormatString(MSG_ERR_EMPTY, Array("expected name"))
		 End If
 else
			VerifySelectedTab = STATUS_FAILED
			statusTest=STATUS_FAILED
			customMsg = FormatString(MSG_OBJECT_NOT_EXISTS, Array("Tab"))
End If
Call writeActionStatusToLog(statusTest,customMsg)
On Error GoTo 0
End Function


Function SelectTabItem(byval objTab,byval strTabName)
   On Error Resume Next
	Dim actualName,customMsg
	Dim blnstatus ,flag,statusTest
   If CheckObjectExist(objTab) Then
			 If CheckObjectEnabled(objTab) Then
						If Vartype(strTabName) <> vbEmpty  And Err.Number = 0  Then
								blnstatus = true
						End If
						If  blnstatus=true  Then
								objTab.Select strTabName
								If Err.Number=0  Then
									SelectTabItem=STATUS_PASSED
									statusTest=STATUS_PASSED
									customMsg=FormatString(MSG_OPERATION_SUCCESSFUL, Array("Select", "Tab"))
									'customMsg="Tab was selected sucessfully."
								Else
									SelectTabItem=STATUS_DEFECT
									statusTest=STATUS_DEFECT
									customMsg=FormatString(MSG_OPERATION_UNSUCCESSFUL, Array("Select", "Tab"))
									'customMsg="Tab could not get seleted, Please verify."
								End If
						else
									SelectTabItem=STATUS_FAILED
									statusTest=STATUS_FAILED
									customMsg = FormatString(MSG_ERR_EMPTY, Array("Tab name"))
									'customMsg="An error occurred;strTabName is empty."
						End If
				Else
						SelectTabItem=STATUS_DEFECT
						statusTest=STATUS_DEFECT
						customMsg = FormatString(MSG_OPERATION_FAILED_DISABLED, Array("Select", "Tab"))
						'customMsg="The Tab  was not enabled, Please verify."
			  End If
  else
		SelectTabItem=STATUS_FAILED
		statusTest=STATUS_FAILED
		customMsg = FormatString(MSG_OBJECT_NOT_EXISTS, Array("Tab"))
		'customMsg="The specified Tab does not exist, please verify."
   End If
   SelectTabItem=statusTest
   Call writeActionStatusToLog(statusTest,customMsg)
   On Error GoTo 0
End Function

Function VerifyTabItemByIndex(byval objTab,byval intIndex,byval strExpName)
	Dim actualName,customMsg
	Dim blnstatus ,flag,statusTest
	Dim intItemCount,intExpIndex

	'Anu: subtract 1 from index
   If CheckObjectExist(objTab) Then
			 If CheckObjectEnabled(objTab) Then
						intItemCount=objTab.GetItemsCount
						intExpIndex=CInt(intIndex) - 1
						If Err.Number = 0 And intItemCount>intExpIndex And intExpIndex>=0 Then
								actualName=objTab.GetItem(intExpIndex)
								If Err.Number = 0 Then
									If Vartype(strExpName) <> vbEmpty Then
										blnstatus = true
									End If
									If  blnstatus=true  Then
											If actualName=strExpName  Then
														VerifyTabItemByIndex=STATUS_PASSED
														 statusTest=STATUS_PASSED
														 customMsg = FormatString(MSG_ITEMS_MATCHES, Array("Tab item","Tab item"))
														  'customMsg="The specified tab item matches with the expected tab items."
											Else
														VerifyTabItemByIndex=STATUS_DEFECT
														statusTest=STATUS_DEFECT
														customMsg = FormatString(MSG_ITEMS_NOT_MATCHES, Array("Tab item","Tab item"))
														'customMsg="The specified tab item does not match the expected tab items."
										  End If
									else
											VerifyTabItemByIndex=STATUS_FAILED
											statusTest=STATUS_FAILED
											customMsg = FormatString(MSG_ERR_EMPTY, Array("expected name"))
											'customMsg="An error occurred;strExpName is empty or DataTypeMisMatch"
									End If
								Else
									VerifyTabItemByIndex=STATUS_FAILED
									statusTest=STATUS_FAILED
									customMsg = FormatString(MSG_ERR_NOT_NUMERIC, Array("index"))
								End If
						else
							 VerifyTabItemByIndex=STATUS_FAILED
							  statusTest=STATUS_FAILED
							  customMsg = FormatString(MSG_OUT_OF_RANGE, Array("index"))
							  'customMsg="An error occurred;intIndex is greater  then TabItemIndex."
						End If
				Else
						VerifyTabItemByIndex=STATUS_DEFECT
						statusTest=STATUS_DEFECT
						customMsg = FormatString(MSG_OPERATION_FAILED_DISABLED, Array("Verify", "Tab"))
						'customMsg="The Tab  was not enabled, Please verify."
			  End If
  else
		VerifyTabItemByIndex=STATUS_FAILED
		statusTest=STATUS_FAILED
		customMsg = FormatString(MSG_OBJECT_NOT_EXISTS, Array("Tab"))
		'customMsg="The specified Tab does not exist, please verify."
   End If
 Call writeActionStatusToLog(statusTest,customMsg)
   On Error GoTo 0
End Function