'=======================Code Section Begin============================================

Option Explicit		 'Forces explicit declaration of all variables in a script. 

'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :			  ClickNetButton()
'	Purpose :				  		Performs  Click operation on a specified Button.
'	Return Value :		 	  	Integer( 0/1/2)
'	Input Arguments :  	 	objButton
'------------------------------------------------------------------------------------------------------------------------------------------
Function ClickNetButton(ByVal objButton)
	On Error Resume Next
		'Variable Section Begin
	
			dim  customMsg  ,objectName
   	Dim intStatus
		'Variable Section End
		
			If CheckObjectExist(objButton) Then 
			   If CheckObjectEnabled(objButton) Then
					If CheckObjectClick(objButton) Then
						ClickNetButton = STATUS_PASSED
						intStatus = STATUS_PASSED
						customMsg = FormatString(MSG_OPERATION_SUCCESSFUL,Array("Click","Button"))
					Else
						ClickNetButton = STATUS_DEFECT
						  intStatus = STATUS_DEFECT
						customMsg = FormatString(MSG_OPERATION_UNSUCCESSFUL,Array("Click","Button"))
					End If 
				Else
					ClickNetButton = STATUS_FAILED
					intStatus = STATUS_FAILED
					customMsg = FormatString(MSG_OPERATION_FAILED_DISABLED,Array("Click","Button"))
				End If
			  Else
                   ClickNetButton = STATUS_FAILED
				   intStatus = STATUS_FAILED
						customMsg = FormatString(MSG_OBJECT_NOT_EXISTS,Array("Button"))
			  End If
		  call WriteActionStatusToLog(intStatus, customMsg)
	On Error GoTo 0
End Function

'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name :				 CompareNetButtonName()
' 	Purpose :						Compares the actual Button Name with the expected Name .
' 	Return Value :		 		  Integer(0/1/2)
'	Input Arguments :  			objButton , strExpName
'-----------------------------------------------------------------------------------------------------------------------------------------
Function CompareNetButtonName(ByVal objButton , ByVal strExpName)
	On Error Resume Next
		'Variable Section Begin
			Dim strActName,  customMsg
		'Variable Section End
			If CheckObjectExist(objButton) Then         
					strActName = objButton.GetRoProperty("text")
				
					If Vartype(strActName) <> vbEmpty  And Err.Number = 0 Then
							If CompareActualStringValues(strExpName,strActName) = 0  Then
							    CompareNetButtonName = STATUS_PASSED
								intStatus = STATUS_PASSED
								customMsg = FormatString(MSG_TEXT_MATCHES,Array(strExpName,strActName))
							  '   "Expected Name : "&strExpName& """" &  " and " &""""&"Actual Name : "&strActName&""""&" matches as expected."  'MSG_TEXT_MATCHES
							Else
								 strActName = replace(strActName,"&","")
								 If CompareActualStringValues(strExpName,strActName) = 0  Then
										CompareNetButtonName =STATUS_PASSED
										intStatus= STATUS_PASSED
										customMsg = FormatString(MSG_TEXT_MATCHES,Array(strExpName,strActName))
									  '   "Expected Name : "&strExpName& """" &  " and " &""""&"Actual Name : "&strActName&""""&" matches as expected."  'MSG_TEXT_MATCHES
								 Else
										CompareNetButtonName = STATUS_DEFECT
										intStatus = STATUS_DEFECT
										customMsg = FormatString(MSG_TEXT_NOT_MATCHES,Array(strExpName,strActName))
							   ' 	customMsg = "Expected Name : "&strExpName& """" &  " and " &""""&"Actual Name : "&strActName&""""&" does not match, Please verify."  'MSG_TEXT_NOT_MATCHES
													   
								  End if 
							End If
					Else
							CompareNetButtonName = STATUS_FAILED
							intStatus = STATUS_FAILED
							 customMsg =FormatString(MSG_ERR_NOT_NUMERIC ,Array("strExpName")) '   "An error occurred during the method CompareNetButtonName."  	'MSG_ERR_NOT_NUMERIC
					End if

			Else
					CompareNetButtonName = STATUS_FAILED
					intStatus = STATUS_FAILED
					customMsg =  FormatString(MSG_OBJECT_NOT_EXISTS,Array("Button"))    '"The specified Button does not exist, Please verify."  'MSG_OBJECT_NOT_EXISTS
			End If
		     call WriteActionStatusToLog(intStatus, customMsg)
			 
	On Error GoTo 0
End Function

'=======================Code Section End=============================================
