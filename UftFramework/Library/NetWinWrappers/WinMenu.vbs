'	Function Name :			  VerifyMenuItemDisabled()
'	Purpose :				  Verify to specific menu item is disabled.
'	Return Value :		 	  	Integer( 0/1)
'	Input Arguments :  	 	objMenu,arrPath
'------------------------------------------------------------------------------------------------------------------------------------------
Function  VerifyMenuItemDisabled(byval objMenu,byval arrPath)
	On Error Resume Next
	Dim intRowCount,intColCount
	Dim blnstatus,statusTest
	Dim customMsg,intLen,n 
	Dim strPath
	'arrPath=ConcateArrayItem(arrPath)
	intLen=UBound(arrPath)
	
	If CheckObjectExist(objMenu) Then
		For n=0 To intLen
			If n=0 Then
				strPath=arrPath(n)
			Else		
				strPath=strPath&";"&arrPath(n)
			End If 
			If  objMenu.GetItemProperty(strPath,"Exists") Then
				If  objMenu.GetItemProperty(strPath,"Enabled")=False Then
					statusTest=STATUS_PASSED
					customMsg = FormatString(MSG_DISABLED_PASS, Array("Menu Item"))
					Exit For
				Else
					statusTest=STATUS_DEFECT
					customMsg = FormatString(MSG_ENABLED_FAIL, Array("Menu Item"))
				End If
			Else
				statusTest=STATUS_FAILED
				customMsg = FormatString(MSG_OBJECT_NOT_EXISTS, Array("Menu Item"))
				Exit For
			End If
			
		Next 
	Else
		statusTest=STATUS_FAILED
		customMsg = FormatString(MSG_OBJECT_NOT_EXISTS, Array("Menu"))
	End If
	VerifyMenuItemDisabled=statusTest
	'MsgBox statusTest&" "&customMsg&" "&Err.description
	Call writeActionStatusToLog(statusTest,customMsg)
	On Error Goto 0	
End Function
'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :			  VerifyMenuItemEnabled()
'	Purpose :				  Verify to specific menu item is enabled.
'	Return Value :		 	  	Integer( 0/1)
'	Input Arguments :  	 	objMenu,arrPath
'------------------------------------------------------------------------------------------------------------------------------------------
Function  VerifyMenuItemEnabled(byval objMenu,byval arrPath)
	On Error Resume Next
	Dim intRowCount,intColCount
	Dim blnstatus,statusTest
	Dim customMsg,intLen,n 
	Dim strPath
	'arrPath=ConcateArrayItem(arrPath)
	intLen=UBound(arrPath)
	
	If CheckObjectExist(objMenu) Then
		For n=0 To intLen
			If n=0 Then
				strPath=arrPath(n)
			Else		
				strPath=strPath&";"&arrPath(n)
			End If 
			If  objMenu.GetItemProperty(strPath,"Exists") Then
				If  objMenu.GetItemProperty(strPath,"Enabled")=False Then
					statusTest=STATUS_DEFECT
					customMsg = FormatString(MSG_DISABLED_FAIL, Array("Menu Item"))
					Exit For
				Else
					statusTest=STATUS_PASSED
					customMsg = FormatString(MSG_ENABLED_PASS, Array("Menu Item"))
				End If
			Else
				statusTest=STATUS_FAILED
				customMsg = FormatString(MSG_OBJECT_NOT_EXISTS, Array("Menu Item"))
				Exit For
			End If
			
		Next 
	Else
		statusTest=STATUS_FAILED
		customMsg = FormatString(MSG_OBJECT_NOT_EXISTS, Array("Menu"))
	End If
	VerifyMenuItemEnabled=statusTest
	'MsgBox statusTest&" "&customMsg&" "&Err.description
	Call writeActionStatusToLog(statusTest,customMsg)
	On Error Goto 0	
End Function
'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :			  VerifySubMenuCount()
'	Purpose :				  		Verify menu item count.
'	Return Value :		 	  	Integer( 0/1)
'	Input Arguments :  	 	objMenu,arrSubMenuItem,intExpItemCount
'------------------------------------------------------------------------------------------------------------------------------------------
Function VerifySubMenuCount(byval objMenu,byval arrSubMenuItem,byval intExpItemCount)
	On Error Resume Next
	Dim intActItemCount
	Dim blnstatus,statusTest
	Dim customMsg 
	arrSubMenuItem=ConcateArrayItem(arrSubMenuItem)
	If CheckObjectExist(objMenu) Then
		intExpItemCount=CInt(intExpItemCount)
		If Err.Number=0 Then			
			If  objMenu.GetItemProperty(arrSubMenuItem,"Exists") Then
				intActItemCount=objMenu.GetItemProperty(arrSubMenuItem,"SubMenuCount")
				If  intActItemCount=intExpItemCount Then
					statusTest=STATUS_PASSED
					customMsg=FormatString(MSG_VALUE_MATCHES,Array(intExpItemCount,intActItemCount))
				Else
					statusTest=STATUS_DEFECT
					customMsg=FormatString(MSG_VALUE_NOT_MATCHES,Array(intExpItemCount,intActItemCount))
				End If
			Else
				statusTest=STATUS_FAILED
				customMsg = FormatString(MSG_OBJECT_NOT_EXISTS, Array("Menu Item"))
			End If
		Else
			statusTest=STATUS_FAILED
			customMsg = FormatString(MSG_ERR_NOT_NUMERIC, Array("expected menu count"))
		End If
	Else
		statusTest=STATUS_FAILED
		customMsg = FormatString(MSG_OBJECT_NOT_EXISTS, Array("Menu"))
	End If
	VerifySubMenuCount=statusTest
	'MsgBox statusTest&" "&customMsg&" "&Err.description
	Call writeActionStatusToLog(statusTest,customMsg)
	On Error Goto 0	
End Function
'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :			  VerifyMenuItemExist()
'	Purpose :				  		Verify specific menu item is exist.
'	Return Value :		 	  	Integer( 0/1)
'	Input Arguments :  	 	objMenu,arrPath
'------------------------------------------------------------------------------------------------------------------------------------------
Function VerifyMenuItemExist(byval objMenu,byval arrPath)
	On Error Resume Next
	Dim intRowCount,intColCount
	Dim blnstatus,statusTest
	Dim customMsg 
	arrPath=ConcateArrayItem(arrPath)
	If CheckObjectExist(objMenu) Then
		If  objMenu.GetItemProperty(arrPath,"Exists") Then
			statusTest=STATUS_PASSED
			customMsg = FormatString(MSG_OBJECT_EXISTS_PASS, Array("Menu Item"))
		Else
			statusTest=STATUS_DEFECT
			customMsg = FormatString(MSG_OBJECT_NOT_EXISTS, Array("Menu Item"))
		End If
	Else
		statusTest=STATUS_FAILED
		customMsg = FormatString(MSG_OBJECT_NOT_EXISTS, Array("Menu"))
	End If
	VerifyMenuItemExist=statusTest
	'msgbox statusTest&" "&customMsg&" "&err.description
	Call writeActionStatusToLog(statusTest,customMsg)
	On Error Goto 0	
End Function

'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :			  VerifyMenuItemNotExist()
'	Purpose :				  		Verify specific menu item is not exist.
'	Return Value :		 	  	Integer( 0/1)
'	Input Arguments :  	 	objMenu,arrPath
'------------------------------------------------------------------------------------------------------------------------------------------
Function VerifyMenuItemNotExist(byval objMenu,byval arrPath)
	On Error Resume Next
	Dim intRowCount,intColCount
	Dim blnstatus,statusTest
	Dim customMsg 
	arrPath=ConcateArrayItem(arrPath)
	If CheckObjectExist(objMenu) Then
		If  objMenu.GetItemProperty(arrPath,"Exists") Then
			statusTest=STATUS_DEFECT
			customMsg = FormatString(MSG_OBJECT_EXISTS_FAIL, Array("Menu Item"))
		Else
			statusTest=STATUS_PASSED
			customMsg = FormatString(MSG_OBJECT_NOT_EXISTS_PASS, Array("Menu Item"))
		End If
	Else
		statusTest=STATUS_FAILED
		customMsg = FormatString(MSG_OBJECT_NOT_EXISTS, Array("Menu"))
	End If
	VerifyMenuItemNotExist=statusTest
	'msgbox statusTest&" "&customMsg&" "&err.description
	Call writeActionStatusToLog(statusTest,customMsg)
	On Error Goto 0	
End Function

'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :			  SelectMenuItem()
'	Purpose :				  		Perform specific menu item Select.
'	Return Value :		 	  	Integer( 0/1)
'	Input Arguments :  	 	objMenu,arrPath
'------------------------------------------------------------------------------------------------------------------------------------------
Function SelectMenuItem_RENAMED(byval objMenu,byval arrPath)
	On Error Resume Next
	Dim intRowCount,intColCount
	Dim blnstatus,statusTest
	Dim customMsg,intLen,n 
	Dim strPath
	'arrPath=ConcateArrayItem(arrPath)
	intLen=UBound(arrPath)
	
	If CheckObjectExist(objMenu) Then
		For n=0 To intLen
			If n=0 Then
				strPath=arrPath(n)
			Else		
				strPath=strPath&";"&arrPath(n)
			End If 
			If  objMenu.GetItemProperty(strPath,"Exists") Then
				If  objMenu.GetItemProperty(strPath,"Enabled")=True Then
					statusTest=STATUS_PASSED
				Else
					statusTest=STATUS_DEFECT
					customMsg = FormatString(MSG_DISABLED_FAIL, Array("Menu Item"))
					Exit For 
				End If 
			Else
				statusTest=STATUS_FAILED
				customMsg = FormatString(MSG_OBJECT_NOT_EXISTS, Array("Menu Item"))
				Exit For 
			End If
		Next 
		If statusTest=0 Then
			objMenu.Select strPath
			If Err.number=0 Then
				statusTest=STATUS_PASSED
				customMsg=FormatString(MSG_OPERATION_SUCCESSFUL,Array("Select",",Menu Item"))
			Else
				statusTest=STATUS_DEFECT
				customMsg=FormatString(MSG_OPERATION_UNSUCCESSFUL,Array("Select","Menu Item"))			
			End If	
		End If
	Else
		statusTest=STATUS_FAILED
		customMsg = FormatString(MSG_OBJECT_NOT_EXISTS, Array("Menu"))
	End If
	SelectMenuItem=statusTest
	'msgbox statusTest&" "&customMsg&" "&err.description
	Call writeActionStatusToLog(statusTest,customMsg)
	On Error Goto 0	
End Function

'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :			  VerifyMenuItemHasSubMenu()
'	Purpose :				  	Verify MenuItem has sub menu.
'	Return Value :		 	  	Integer( 0/1)
'	Input Arguments :  	 	objMenu,arrParentMenuItem,arrSubMenuItem
'------------------------------------------------------------------------------------------------------------------------------------------
Function VerifyMenuItemHasSubMenu(byval objMenu,byval arrParentMenuItem,byval arrSubMenuItem)
	On Error Resume Next
	Dim statusTest,customMsg,arrSub,strConCateArray,printArr
	Dim intLen,customFlag,n,concateString 
	customFlag=True
	If CheckObjectExist(objMenu) Then		
		strConCateArray=ConcateArrayItem(arrParentMenuItem)
		If strConCateArray Then 
			customFlag=False
		End If 
		intLen=UBound(arrParentMenuItem)
		For n=0 To intLen
			If n=0 Then
				concateString=arrParentMenuItem(n)
			Else		
				concateString=concateString&";"&arrParentMenuItem(n)
			End If 
			If Not objMenu.GetItemProperty(concateString, "Exists") Then
				customFlag=False
				Exit For
			End If 
		Next 
		If customFlag=True Then		
			If objMenu.GetItemProperty(strConCateArray, "HasSubMenu") Then			
				arrSub=GetActivatedSubMenuItems(objMenu,strConCateArray)
				If CompareArraysCaseSensitive(arrSubMenuItem,arrSub)=0 Then
					statusTest=STATUS_PASSED
					customMsg=FormatString(MSG_OPERATION_SUCCESSFUL,Array("Verify","Menu Item"))
				Else
					statusTest=STATUS_DEFECT
					customMsg=FormatString(MSG_OPERATION_UNSUCCESSFUL,Array("Verify","Menu Item"))
				End If
			Else
				'printArr=""""&PrintArrayElements(arrParentMenuItem)&""""
				statusTest=STATUS_FAILED			
				customMsg = FormatString(MSG_NOT_CONTAINS_SPECIFIC_FAIL, Array("Parent menu item", "sub menu item"))
			End If
		Else
				'printArr=""""&PrintArrayElements(arrParentMenuItem)&""""
				statusTest=STATUS_FAILED							
				customMsg = FormatString(MSG_OBJECT_NOT_EXISTS, Array("Parent menu item"))
		End If
	Else
		statusTest=STATUS_FAILED
		customMsg = FormatString(MSG_OBJECT_NOT_EXISTS, Array("Menu"))
	End If
	VerifyMenuItemHasSubMenu=statusTest
	'msgbox statusTest&" "&customMsg&" "&err.description
	Call writeActionStatusToLog(statusTest,customMsg)
	On Error Goto 0	
End Function

'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :			  GetActivatedSubMenuItems()
'	Purpose :				  	To Get sub menu items.
'	Return Value :		 	  	Array
'	Input Arguments :  	 	menuObj,itemPath
'------------------------------------------------------------------------------------------------------------------------------------------
Function GetActivatedSubMenuItems(byval menuObj, byval itemPath) 
	On Error Resume Next
	Dim blnStatus, lable,  intSubItemsCount, Paths
	Dim ArraySubMenuItems()
	Dim ArrayfinalMenuitems
	Dim intcount
	Dim n
	n=1
	blnStatus = False
	intcount = 1
	ReDim Preserve ArraySubMenuItems(intcount-1)
	blnStatus = menuObj.GetItemProperty(itemPath, "HasSubMenu") 
	If blnStatus Then 
		intSubItemsCount = menuObj.GetItemProperty(itemPath, "SubMenuCount") 
		For n = 1 To intSubItemsCount 
			Paths = menuObj.BuildMenuPath(itemPath, n) 
			lable = menuObj.GetItemProperty(Paths, "Label")
			ReDim Preserve ArraySubMenuItems(intcount-1)	    
			ArraySubMenuItems(intcount-1) = lable
			intcount = intcount+1
			If n = intSubItemsCount Then
				GetActivatedSubMenuItems = ArraySubMenuItems
				Exit Function
			End If
		Next 
	Else
		GetActivatedSubMenuItems = Empty
	End If 
	On Error Goto 0
End Function 
'=======================Code Section End=============================================
