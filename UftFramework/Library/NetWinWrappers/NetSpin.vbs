
Option Explicit	
'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :		SetNextSpinValue()
'	Purpose :					 Set Next Value of spin control.
'	Return Value :		 		 Integer( 0/1/2)
'	Arguments :
'		Input Arguments :  		objSpin 
'------------------------------------------------------------------------------------------------------------------------------------------


Function SetNextSpinValue(ByVal objSpin)
	On Error Resume Next
	'Variable Section Begin
	
	Dim intActive , strEnabled
	Dim customMsg , intStatus
	'Variable Section End
	If CheckObjectExist(objSpin) Then
		If CheckObjectEnabled(objSpin) Then
			objSpin.Next  
			If Err.Number = 0 Then
				intStatus  = STATUS_PASSED
				customMsg = FormatString(MSG_OPERATION_SUCCESSFUL,Array("Set Next","Spin"))
				'customMsg =  "The specified Spin next value set successfully." 'MSG_OPERATION_SUCCESSFUL
			Else
				intStatus = STATUS_DEFECT
				customMsg = FormatString(MSG_OPERATION_UNSUCCESSFUL,Array("Set Next","Spin"))
				'customMsg =  "spin value is not set, Please verify." 'MSG_OPERATION_UNSUCCESSFUL
			End If
			
		Else
			intStatus = STATUS_DEFECT
			customMsg = FormatString(MSG_OPERATION_FAILED_DISABLED,Array("Set Next","Spin"))
		End If
		
	Else
		intStatus = STATUS_FAILED
		customMsg =    FormatString(MSG_OBJECT_NOT_EXISTS,Array("Spin"))  
	End If
	
	SetNextSpinValue = intStatus
	Call WriteActionStatusToLog(intStatus, customMsg)
	On Error Goto 0 
End Function


'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :		SetPreviousSpinValue()
'	Purpose :					 Set Previous Value of spin control.
'	Return Value :		 		 Integer( 0/1/2)
'	Arguments :
'		Input Arguments :  		objSpin 
'------------------------------------------------------------------------------------------------------------------------------------------


Function SetPreviousSpinValue(ByVal objSpin)
	On Error Resume Next
	'Variable Section Begin
	
	Dim intActive , strEnabled
	Dim customMsg , intStatus
	'Variable Section End
	If CheckObjectExist(objSpin) Then
		If CheckObjectEnabled(objSpin) Then
			objSpin.Prev   
			If Err.Number = 0 Then
				intStatus  = STATUS_PASSED
				customMsg = FormatString(MSG_OPERATION_SUCCESSFUL,Array("Set Previous","Spin"))
				'customMsg =  "The specified Spin previous value set successfully." 'MSG_OPERATION_SUCCESSFUL
			Else
				intStatus = STATUS_DEFECT
				customMsg = FormatString(MSG_OPERATION_UNSUCCESSFUL,Array("Set Previous","Spin"))
				'	customMsg =  "spin value is not set, Please verify." 'MSG_OPERATION_UNSUCCESSFUL
			End If
			
		Else
			intStatus = STATUS_DEFECT
			customMsg = FormatString(MSG_OPERATION_FAILED_DISABLED,Array("Set Next","Spin"))
		End If
		
	Else
		intStatus = STATUS_FAILED
		customMsg =    FormatString(MSG_OBJECT_NOT_EXISTS,Array("Spin"))  
	End If
	
	SetPreviousSpinValue = intStatus
	Call WriteActionStatusToLog(intStatus, customMsg)
	On Error Goto 0 
End Function







'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :		SetSpinValue()
'	Purpose :					 Set  Value of spin control.
'	Return Value :		 		 Integer( 0/1/2)
'	Arguments :
'		Input Arguments :  		objSpin ,intValue
'------------------------------------------------------------------------------------------------------------------------------------------


Function SetSpinValue(ByVal objSpin , intValue)
	On Error Resume Next
	'Variable Section Begin
	
	Dim intActive , strEnabled
	Dim customMsg , intStatus
	'Variable Section End
	If CheckObjectExist(objSpin) Then
		If CheckObjectEnabled(objSpin) Then		
			If IsNumeric(intValue) Then
				objSpin.Set intValue   
				If Err.Number = 0 Then
					intStatus  = STATUS_PASSED
					customMsg = FormatString(MSG_OPERATION_SUCCESSFUL,Array("Set","Spin"))
					'	customMsg =  "The specified Spin  value set successfully." 'MSG_OPERATION_SUCCESSFUL
				Else
					intStatus = STATUS_DEFECT
					customMsg = FormatString(MSG_OPERATION_UNSUCCESSFUL,Array("Set","Spin"))
					'customMsg =  "spin value is not set, Please verify." 'MSG_OPERATION_UNSUCCESSFUL
				End If
				
			Else
				intStatus = STATUS_FAILED
				customMsg =FormatString(MSG_ERR_NOT_NUMERIC ,Array("Value"))
				'customMsg =  "Set value is not a numeric value, Please verify." 'MSG_ERR_NOT_NUMERIC
			End If
			
		Else
			intStatus = STATUS_DEFECT
			customMsg = FormatString(MSG_OPERATION_FAILED_DISABLED,Array("Set","Spin"))
		End If	
	Else
		intStatus = STATUS_FAILED
		customMsg =    FormatString(MSG_OBJECT_NOT_EXISTS,Array("Spin"))  
	End If
	
	SetSpinValue = intStatus
	Call WriteActionStatusToLog(intStatus, customMsg)
	On Error Goto 0 
End Function



'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :				 VerifySpinValue()
'	Purpose :					 Verify the value with  expected value.
'	Return Value :		 		 Integer( 0/1/2)
'	Arguments :
'		Input Arguments :  		objSpin , intValue
'------------------------------------------------------------------------------------------------------------------------------------------
Function  VerifySpinValue(ByVal objSpin , ByVal intValue)
	On Error Resume Next 
	'Variable Section Begin
	
	Dim strActValue
	Dim customMsg , intStatus
	'Variable Section End
	If CheckObjectExist(objSpin) Then
		If CheckObjectEnabled(objSpin) Then
			strActValue = objSpin.GetRoProperty("value")
			
			If CompareActualStringValues(intValue, strActValue)=0  Then 
				intStatus = STATUS_PASSED
				customMsg = FormatString(MSG_VALUE_MATCHES,Array(intValue,strActValue))
				'customMsg = "Expected Value : "&intValue& """" &  " and " &""""&"Actual value : "&strActValue&""""&" matches as expected." 'MSG_VALUE_MATCHES
				
			Else
				intStatus = STATUS_DEFECT
				customMsg = FormatString(MSG_VALUE_NOT_MATCHES,Array(intValue,strActValue))
			'	customMsg = "Expected Value : "&intValue& """" &  " and " &""""&"Actual value : "&strActValue&""""&" does not match, Please verify." 'MSG_VALUE_NOT_MATCHES
				
			End If
		Else
			intStatus = STATUS_DEFECT
			customMsg = FormatString(MSG_OPERATION_FAILED_DISABLED,Array("Set Next","Spin"))
		End If
	Else
		intStatus = STATUS_FAILED
		customMsg =    FormatString(MSG_OBJECT_NOT_EXISTS,Array("Spin"))  
	End If
	VerifySpinValue = intStatus
	Call WriteActionStatusToLog(intStatus, customMsg)
	On Error Goto 0 
End Function

