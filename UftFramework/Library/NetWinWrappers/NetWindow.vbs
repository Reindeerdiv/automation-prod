
'========================Code Section Begin=====================================================================
Option Explicit	'Forces explicit declaration of all variables in a script.
'---------------------------------------------------------------------------------------------------


''------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :			MaximizeWindow()
'	Purpose :				Maximizes the specified Window if it exist ,isEnabled and is Maximisable.
'	Return Value :			Integer(0/1/2)
'	Arguments :
'		Input Arguments :	objWindow      

'------------------------------------------------------------------------------------------------------------------------------------------
Function MaximizeNetWindow(ByVal objWindow)
	On Error Resume Next
	'Variable Section Begin
	
	Dim strEnabled , strMaximisable
	Dim customMsg , intStatus
	
	'Variable Section End
	If CheckObjectExist(objWindow) Then
		strEnabled = CheckObjectEnabled(objWindow)
		If strEnabled = True Then
			strMaximisable = objWindow.getROProperty("maximizable")
			If  Err.number = 0 And Not IsEmpty(strMaximisable)Then
				If  strMaximisable = True Then
					objWindow.Maximize 
					If Err.Number = 0 Then
						intStatus  = STATUS_PASSED
						customMsg = FormatString(MSG_OPERATION_SUCCESSFUL,Array("Maximize","Window"))
						
					Else
						intStatus = STATUS_DEFECT
						customMsg = FormatString(MSG_OPERATION_UNSUCCESSFUL,Array("Maximize","Window")) 
						''	customMsg = "The specified Window is not maximized, Please verify."
					End If
				Else
					intStatus = STATUS_DEFECT
					customMsg = FormatString(MSG_UNSUPPORTED_ACTION,Array("Maximize","Window")) 
					'	intStatus = STATUS_FAILED
					'	customMsg ="The specified Window has not maximize option, Please verify." 'MSG_UNSUPPORTED_ACTION
				End If
				
			Else
				intStatus  = STATUS_DEFECT
				customMsg = FormatString(MSG_UNSUPPORTED_ACTION,Array("Maximize","Window")) 
				'customMsg =  "Either an error occurred or the specified window is not maximizable , hence it cannot be maximized "  'MSG_UNSUPPORTED_ACTION
			End If
		Else
			intStatus  = STATUS_DEFECT
			customMsg = FormatString(MSG_OPERATION_FAILED_DISABLED,Array("Maximize","Window")) 
			'customMsg = "Either an error occurred or the specified window is not enabled , hence it could not be Maximize" 'MSG_OPERATION_FAILED_DISABLED
			
		End If
	Else
		intStatus = STATUS_FAILED
		customMsg =    FormatString(MSG_OBJECT_NOT_EXISTS,Array("Window"))  
		
	End If
	MaximizeNetWindow = intStatus
	Call WriteActionStatusToLog(intStatus, customMsg)
	On Error Goto 0 
End Function

'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :			MinimizeWindow()
'	Purpose :		        Minimizes the specified Window if it exist, isEnabled and is Minimisable.
'	Return Value :		 	Integer(0/1)
'	Arguments :
'		Input Arguments :	objWindow      
'------------------------------------------------------------------------------------------------------------------------------------------
Function MinimizeNetWindow(ByVal objWindow)
	On Error Resume Next
	'Variable Section Begin
	
	Dim strEnabled , strMinimisable
	Dim customMsg , intStatus
	'Variable Section End
	If CheckObjectExist(objWindow) Then
		strEnabled = CheckObjectEnabled(objWindow)
		If strEnabled = True Then
			strMinimisable = objWindow.getROProperty("minimizable")
			If  Err.number = 0 And Not IsEmpty(strMinimisable)Then
				If  strMinimisable = True Then
					objWindow.Minimize 
					If Err.Number = 0 Then
						intStatus  = STATUS_PASSED
						customMsg = FormatString(MSG_OPERATION_SUCCESSFUL,Array("Minimize","Window"))
					Else
						intStatus = STATUS_DEFECT
						customMsg = FormatString(MSG_OPERATION_UNSUCCESSFUL,Array("Minimize","Window"))
					End If
				Else
					intStatus = STATUS_DEFECT
					customMsg = FormatString(MSG_UNSUPPORTED_ACTION,Array("Minimize","Window"))
					'customMsg = "The specified Window is not Minimizable, Please verify." 'MSG_UNSUPPORTED_ACTION
				End If
				
			Else
				intStatus  = STATUS_DEFECT
				customMsg = FormatString(MSG_UNSUPPORTED_ACTION,Array("Minimize","Window"))
				'customMsg = "Either an error occurred or the specified window is not minimizable , hence it cannot be minimized " 'MSG_UNSUPPORTED_ACTION
			End If
		Else
			intStatus  = STATUS_DEFECT
			customMsg = FormatString(MSG_OPERATION_FAILED_DISABLED,Array("Minimize","Window"))
			'customMsg = "Either an error occurred or the specified window is not enabled , hence it could not be Minimize" 'MSG_OPERATION_FAILED_DISABLED
			
		End If
	Else
		intStatus = STATUS_FAILED
		customMsg =    FormatString(MSG_OBJECT_NOT_EXISTS,Array("Window"))  
		
	End If
	MinimizeNetWindow = intStatus
	Call WriteActionStatusToLog(intStatus, customMsg)
	On Error Goto 0 
End Function





'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :			MoveNetWindow()
'	Purpose :		        Move the specified Window if it exist.
'	Return Value :		 	Integer(0/1/2)
'	Arguments :
'		Input Arguments :	objWindow ,intHorizontalPosition,intVerticalPosition    
'------------------------------------------------------------------------------------------------------------------------------------------
Function MoveNetWindow(ByVal objWindow,intHorizontalPosition,intVerticalPosition)
	On Error Resume Next
	'Variable Section Begin
	
	Dim strEnabled , strMinimisable
	Dim customMsg , intStatus
	'Variable Section End
	If CheckObjectExist(objWindow) Then
		strEnabled = CheckObjectEnabled(objWindow)
		
		If strEnabled = True Then
			
			If objWindow.object.WindowState = 0 Then
				'MsgBox objWindow.object.WindowState
				If IsNumeric(intHorizontalPosition) Then
					
					If IsNumeric(intVerticalPosition) Then
						
						objWindow.Move intHorizontalPosition , intVerticalPosition
						
						If 	  Err.number = 0 Then
							intStatus  = STATUS_PASSED
							customMsg = FormatString(MSG_OPERATION_SUCCESSFUL,Array("move","Window"))
						Else
							intStatus  = STATUS_DEFECT
							customMsg = FormatString(MSG_OPERATION_UNSUCCESSFUL,Array("move","Window"))
							
						End If 
						
					Else
						intStatus  = STATUS_FAILED
						customMsg =    FormatString(MSG_ERR_NOT_NUMERIC,Array("VerticalPosition")) 					
						'customMsg = " Invalid Vertical Position line" 'MSG_ERR_NOT_NUMERIC
					End If
				Else
					intStatus = STATUS_FAILED
					customMsg =    FormatString(MSG_ERR_NOT_NUMERIC,Array("HorizontalPosition"))  
					'customMsg = "Invalid Horizontal position" 'MSG_ERR_NOT_NUMERIC
					
				End If
			Else
				
				intStatus = STATUS_FAILED
				customMsg =    FormatString(MSG_OPERATION_FAILED_GENERIC,Array("Window","Minimize"))  
				'	customMsg = "Window is in Minimize mode"  'MSG_OPERATION_FAILED_GENERIC
			End If 
			
		Else
			intStatus = STATUS_DEFECT
			customMsg = FormatString(MSG_OPERATION_FAILED_DISABLED,Array("Move","Window"))
			'customMsg = "Window is in disable mode" 'MSG_OPERATION_FAILED_DISABLED
		End If	
		
	Else
		
		intStatus = STATUS_FAILED			
		customMsg =    FormatString(MSG_OBJECT_NOT_EXISTS,Array("Window"))  
		
		
	End If
	MoveNetWindow = intStatus
	Call WriteActionStatusToLog(intStatus, customMsg)
	On Error Goto 0 
End Function


'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :			ActivateNetWindow()
'	Purpose :				Activates the Specified Window if it exist and is enabled.	
'	Return Value :		 	Integer(0/1)
'	Arguments :
'		Input Arguments :   objWindow    
'------------------------------------------------------------------------------------------------------------------------------------------
Function ActivateNetWindow(ByVal objWindow)
	On Error Resume Next
	'Variable Section Begin
	
	Dim intActive , strEnabled
	Dim customMsg , intStatus
	'Variable Section End
	If CheckObjectExist(objWindow) Then
		
		strEnabled = CheckObjectEnabled(objWindow)		
		If strEnabled = True Then
			objWindow.Activate  
			If Err.Number = 0 Then
				intStatus  = STATUS_PASSED
				customMsg = FormatString(MSG_OPERATION_SUCCESSFUL,Array("Activate","Window"))
			Else
				intStatus = STATUS_FAILED
				customMsg = FormatString(MSG_OPERATION_UNSUCCESSFUL,Array("Activate","Window"))
				'	customMsg = "Window is not activated, Please verify." 'MSG_OPERATION_UNSUCCESSFUL
			End If
		Else
			intStatus = STATUS_FAILED
			customMsg = FormatString(MSG_OPERATION_FAILED_DISABLED,Array("Activate","Window"))
			'customMsg = "Either an error occurred or the specified window is not enabled , hence it could not be activated" 'MSG_OPERATION_FAILED_DISABLED
		End If
		
	Else
		intStatus = STATUS_FAILED
		customMsg =    FormatString(MSG_OBJECT_NOT_EXISTS,Array("Window"))  
	End If
	
		ActivateNetWindow = intStatus
	Call WriteActionStatusToLog(intStatus, customMsg)
	On Error Goto 0 
End Function

'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :			CloseNetWindow()
'	Purpose :				Closes the specified Window.
'	Return Value :			Integer( 0/1)
'	Arguments :
'		Input Arguments :	objWindow

'------------------------------------------------------------------------------------------------------------------------------------------
Function CloseNetWindow(ByVal objWindow)
	On Error Resume Next
	'Variable Section Begin
	Dim customMsg , intStatus
	' None
	
	'Variable Section End
	If  CheckObjectExist(objWindow) Then 
		objWindow.Close 
		If Err.Number = 0 Then 
			intStatus = STATUS_PASSED
			customMsg = FormatString(MSG_OPERATION_SUCCESSFUL,Array("Close","Window"))
		Else
			intStatus = STATUS_FAILED
			customMsg = FormatString(MSG_OPERATION_UNSUCCESSFUL,Array("Close","Window"))
			'	customMsg = "Window was not Closed successfully, Please verify." 'MSG_OPERATION_UNSUCCESSFUL			
			
		End If
	Else
		intStatus = STATUS_FAILED
		customMsg =    FormatString(MSG_OBJECT_NOT_EXISTS,Array("Window"))  
		
	End If
	CloseNetWindow = intStatus
	Call WriteActionStatusToLog(intStatus, customMsg)
	
	On Error Goto 0
End Function 



'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :				 VerifyNetWindowTitle()
'	Purpose :					 Verify the Window title with our expected title.
'	Return Value :		 		 Integer( 0/1/2)
'	Arguments :
'		Input Arguments :  		objWindow , strExpTitle
'------------------------------------------------------------------------------------------------------------------------------------------
Function  VerifyNetWindowTitle(ByVal objWindow , ByVal strExpTitle)
	On Error Resume Next 
	'Variable Section Begin
	
	Dim strActTitle
	Dim customMsg , intStatus
	'Variable Section End
	If CheckObjectExist(objWindow) Then
		strActTitle = objWindow.GetRoProperty("title")
		If CompareActualStringValues(StrExpTitle, strActTitle)=0  Then 
			intStatus = STATUS_PASSED		
			customMsg = FormatString(MSG_VALUE_MATCHES,Array(strExpTitle,strActTitle))
			
		Else
			intStatus = STATUS_DEFECT
			customMsg = FormatString(MSG_VALUE_NOT_MATCHES,Array(strExpTitle,strActTitle))
			'customMsg = "Expected Title : "&strExpTitle& """" &  " and " &""""&"Actual Title : "&strActTitle&""""&" does not match, Please verify."
			
		End If
	Else
		intStatus = STATUS_FAILED
		customMsg =    FormatString(MSG_OBJECT_NOT_EXISTS,Array("Window"))  
	End If
	VerifyNetWindowTitle = intStatus
	Call WriteActionStatusToLog(intStatus, customMsg)
	On Error Goto 0 
End Function


'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :		RestoreNetWindow()
'	Purpose :					 Restore the Window .
'	Return Value :		 		 Integer( 0/1)
'	Arguments :
'		Input Arguments :  		objWindow , strExpTitle
'------------------------------------------------------------------------------------------------------------------------------------------


Function RestoreNetWindow(ByVal objWindow)
	On Error Resume Next
	'Variable Section Begin
	
	Dim intActive , strEnabled
	Dim customMsg , intStatus
	'Variable Section End
	If CheckObjectExist(objWindow) Then
		
		strEnabled = CheckObjectEnabled(objWindow)		
		If strEnabled = True Then
			objWindow.Restore  
			If Err.Number = 0 Then
				intStatus  = STATUS_PASSED
				customMsg = FormatString(MSG_OPERATION_SUCCESSFUL,Array("Restore","Window"))
				'	customMsg =  "The specified Window is restored successfully."
			Else
				intStatus = STATUS_DEFECT
				customMsg = FormatString(MSG_OPERATION_UNSUCCESSFUL,Array("Restore","Window"))
				'	customMsg =  "Window is not restore, Please verify."
			End If
		Else
			
			intStatus = STATUS_DEFECT
			customMsg = FormatString(MSG_OPERATION_FAILED_DISABLED,Array("Restore","Window"))
		End If
		
		
	Else
		intStatus = STATUS_FAILED
		customMsg =    FormatString(MSG_OBJECT_NOT_EXISTS,Array("Window"))  
	End If
	
	RestoreNetWindow = intStatus
	Call WriteActionStatusToLog(intStatus, customMsg)
	On Error Goto 0 
End Function





'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :			ResizeNetWindow()
'	Purpose :		        Move the specified Window if it exist.
'	Return Value :		 	Integer(0/1/2)
'	Arguments :
'		Input Arguments :	objWindow ,intWidth,intHeight     
'------------------------------------------------------------------------------------------------------------------------------------------
Function ResizeNetWindow(ByVal objWindow,intWidth,intHeight)
	On Error Resume Next
	'Variable Section Begin
	
	Dim strEnabled , strMinimisable
	Dim customMsg , intStatus
	'Variable Section End
	If CheckObjectExist(objWindow) Then
		strEnabled = CheckObjectEnabled(objWindow)
		
		If strEnabled = True Then
			MsgBox objWindow.object.WindowState
			If objWindow.object.WindowState = 0 Then
			
				If IsNumeric(intWidth) Then
					
					If IsNumeric(intHeight) Then
						
						objWindow.Resize intWidth , intHeight
						
						If 	  Err.number = 0 Then
							intStatus  = STATUS_PASSED
							customMsg = FormatString(MSG_OPERATION_SUCCESSFUL,Array("Resize","Window"))
						Else
							intStatus  = STATUS_DEFECT
							customMsg = FormatString(MSG_OPERATION_UNSUCCESSFUL,Array("Resize","Window"))
							
						End If 
						
					Else
						intStatus  = STATUS_FAILED
						customMsg =    FormatString(MSG_ERR_NOT_NUMERIC,Array("Height"))  
					'	customMsg = " Invalid Height Position line" 'MSG_ERR_NOT_NUMERIC
					End If
				Else
					intStatus = STATUS_FAILED
					customMsg =    FormatString(MSG_ERR_NOT_NUMERIC,Array("Width")) 
				'	customMsg = "Invalid width line" 'MSG_ERR_NOT_NUMERIC
					
				End If
			Else
				
				intStatus = STATUS_FAILED
				customMsg =    FormatString(MSG_OPERATION_FAILED_GENERIC,Array("Window","Minimize"))  		
				'customMsg = "Window is in Minimize mode" 'MSG_OPERATION_FAILED_GENERIC
			End If 
			
		Else
			intStatus = STATUS_DEFECT
				customMsg = FormatString(MSG_OPERATION_FAILED_DISABLED,Array("Resize","Window"))
		'	customMsg = "Window is in disable mode" 'MSG_OPERATION_FAILED_DISABLED
		End If	
		
	Else
		
		intStatus = STATUS_FAILED			
		customMsg =    FormatString(MSG_OBJECT_NOT_EXISTS,Array("Window"))  
		
		
	End If
	ResizeNetWindow = intStatus
	Call WriteActionStatusToLog(intStatus, customMsg)
	On Error Goto 0 
End Function