
Option Explicit	

'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :		SetNetCalendarDate()
'	Purpose :					 Set date Value of calender control.
'	Return Value :		 		 Integer( 0/1/2)
'	Arguments :
'		Input Arguments :  		objCalender, strDate
'------------------------------------------------------------------------------------------------------------------------------------------
Function SetNetCalendarDate(ByVal objCalender , strDate)
	On Error Resume Next
	'Variable Section Begin
	
	Dim intActive , strEnabled , strConvertedDate
	Dim customMsg , intStatus
	'Variable Section End
	If CheckObjectExist(objCalender) Then	
		'MsgBox objCalender.GetROProperty("enabled")
			strEnabled =  CheckObjectEnabled(objCalender) 'objCalender.GetROProperty("enabled")
		
		'Iohbha: To be fix. CheckObjectEnabled does not work for Calendar. Hence commented
		'If CheckObjectEnabled(objCalender) Then
		
	
		
		If ObjectTypeName(objCalender) = "DateTimePicker"  And	objCalender.GetROProperty("format") = 4 Then	 
			
			intStatus = STATUS_FAILED
			customMsg = FormatString(MSG_UNSUPPORTED_ACTION,Array("Set Date","DateTimePicker in Time mode"))
			
		Else			
			
			If IsvalidDate(strDate) Then
				strConvertedDate = GetDateInFormat(strDate)
				objCalender.SetDate  strConvertedDate
				If Err.Number = 0 Then
					intStatus  = STATUS_PASSED
					customMsg = FormatString(MSG_OPERATION_SUCCESSFUL,Array("Set Date","Calendar"))
				Else
					intStatus = STATUS_DEFECT
					customMsg = FormatString(MSG_OPERATION_UNSUCCESSFUL,Array("Set Date","Calendar"))
				End If
			Else 
				intStatus = STATUS_DEFECT
				customMsg = FormatString(MSG_ERR_INVALID_FORMAT,Array("Date"))
			End If 
			
			
		End If 
		
'				Else
'					intStatus = STATUS_FAILED
'					customMsg = FormatString(MSG_OPERATION_FAILED_DISABLED,Array("Set Date","Calendar")) 
'				
'				End If
	Else
		intStatus = STATUS_FAILED
		customMsg =    FormatString(MSG_OBJECT_NOT_EXISTS,Array("Calender"))  
	End If		
	SetNetCalendarDate = intStatus
	Call WriteActionStatusToLog(intStatus, customMsg)
	On Error Goto 0 
End Function



'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :		SetNetCalendarTime()
'	Purpose :					 Set date Value of calender control.
'	Return Value :		 		 Integer( 0/1/2)
'	Arguments :
'		Input Arguments :  		objCalender, strDate
'------------------------------------------------------------------------------------------------------------------------------------------


Function SetNetCalendarTime(ByVal objCalender , strTime)
	On Error Resume Next
	'Variable Section Begin
	
	Dim intActive , strEnabled 
	Dim customMsg , intStatus
	'Variable Section End
	If CheckObjectExist(objCalender) Then
		
		'strEnabled = objCalender.GetROProperty("enabled") 'CheckObjectEnabled(objCalender)
		'MsgBox Err.Description &"Enabled"
		'Iohbha: To be fix. CheckObjectEnabled does not work for Calendar. Hence commented
		'If CheckObjectEnabled(objCalender) Then
		
			If ObjectTypeName(objCalender) = "DateTimePicker"  Then
				If 	objCalender.GetROProperty("format") = 4  Then				
					If IsTime(strTime) Then
						objCalender.SetTime  strTime 
						If Err.Number = 0 Then
							intStatus  = STATUS_PASSED
							customMsg = FormatString(MSG_OPERATION_SUCCESSFUL,Array("Set Time","DateTimePicker"))
						Else
							intStatus = STATUS_DEFECT
							customMsg = FormatString(MSG_OPERATION_UNSUCCESSFUL,Array("Set Time","DateTimePicker"))
						End If
						
					Else
						intStatus = STATUS_FAILED
						
						customMsg = FormatString(MSG_ERR_INVALID_FORMAT,Array("Time"))
						
						
					End If 
				Else			
					intStatus = STATUS_FAILED
					customMsg = FormatString(MSG_UNSUPPORTED_ACTION,Array("Set Time","DateTimePicker in Date mode"))
									
				End If
			Else
				intStatus = STATUS_FAILED
				customMsg = FormatString(MSG_UNSUPPORTED_ACTION,Array("Set Time","Calendar in Date mode"))
				
			End If
			
			
'		Else
'			intStatus = STATUS_FAILED
'			customMsg = FormatString(MSG_OPERATION_FAILED_DISABLED,Array("Set Time","DateTimePicker")) 
'		End If
	Else
		intStatus = STATUS_FAILED
		customMsg =    FormatString(MSG_OBJECT_NOT_EXISTS,Array("DateTimePicker"))  
	End If
	
	SetNetCalendarTime = intStatus
	Call WriteActionStatusToLog(intStatus, customMsg)
	On Error Goto 0 
End Function




'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :		VerifyNetCalendarTime()
'	Purpose :					 verfiy date Value of calender control with expected value.
'	Return Value :		 		 Integer( 0/1/2)
'	Arguments :
'		Input Arguments :  		objCalender, strDate
'------------------------------------------------------------------------------------------------------------------------------------------


Function VerifyNetCalendarTime(ByVal objCalender , strTime)
	On Error Resume Next
	'Variable Section Begin
	
	Dim  strEnabled,strActTime
	Dim customMsg , intStatus
	'Variable Section End
	If CheckObjectExist(objCalender) Then
		'Iohbha: To be fix. CheckObjectEnabled does not work for Calendar. Hence commented
		'strEnabled = CheckObjectEnabled(objCalender)
		'If strEnabled = True Then
			
			If ObjectTypeName(objCalender) = "DateTimePicker"  Then
				
				If 	objCalender.GetROProperty("format") = 4  Then
					strActTime = objCalender.GetRoProperty("time")
					If VarType(strTime) <> vbEmpty  And IsTime(strTime)And Err.Number = 0 Then						
						
						If CompareActualStringValues(TimeValue(strTime),TimeValue(strActTime)) And Err.Number = 0 Then
							intStatus  = STATUS_PASSED
							customMsg = FormatString(MSG_OPERATION_SUCCESSFUL,Array("Verify Time","DateTimePicker"))
						
						Else
							intStatus = STATUS_DEFECT
							customMsg = FormatString(MSG_OPERATION_UNSUCCESSFUL,Array("Verify Time","DateTimePicker"))
						End If
						
						
						
					Else
						intStatus = STATUS_FAILED
						customMsg = FormatString(MSG_ERR_INVALID_FORMAT,Array("Time"))
						
					End If 
				Else			
					intStatus = STATUS_FAILED
					customMsg = FormatString(MSG_UNSUPPORTED_ACTION,Array("Verify Time","DateTimePicker in Date mode"))
				End If
			Else
				intStatus = STATUS_FAILED
				customMsg = FormatString(MSG_UNSUPPORTED_ACTION,Array("Verify Time","Calendar in Date mode"))
				
				
			End If
			
'		Else
'			intStatus = STATUS_FAILED
'			customMsg = FormatString(MSG_OPERATION_FAILED_DISABLED,Array("Verify Time","DateTimePicker")) 
'			'customMsg =  "Object is in disable mode., Please verify."
'		End If 
		
	Else
		intStatus = STATUS_FAILED
		customMsg =    FormatString(MSG_OBJECT_NOT_EXISTS,Array("DateTimePicker"))  
	End If
	
	VerifyNetCalendarTime = intStatus
	Call WriteActionStatusToLog(intStatus, customMsg)
	On Error Goto 0 
End Function


'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :		VerifyNetCalendarDate()
'	Purpose :					 verify date Value of calender control.
'	Return Value :		 		 Integer( 0/1/2)
'	Arguments :
'		Input Arguments :  		objCalender, strDate
'------------------------------------------------------------------------------------------------------------------------------------------


Function VerifyNetCalendarDate(ByVal objCalender , strDate)
	On Error Resume Next
	'Variable Section Begin
	
	Dim  strEnabled,strActdate
	Dim customMsg , intStatus
	'Variable Section End
	If CheckObjectExist(objCalender) Then
	'Iohbha: To be fix. CheckObjectEnabled does not work for Calendar. Hence commented

		'strEnabled = CheckObjectEnabled(objCalender)
		'If strEnabled = True Then
			
			If ObjectTypeName(objCalender) = "DateTimePicker"  And	objCalender.GetROProperty("format") = 4 Then	 
				
				intStatus = STATUS_FAILED
				customMsg = FormatString(MSG_UNSUPPORTED_ACTION,Array("Verify Date","DateTimePicker in Time mode"))
				
			Else				
				
				If VarType(strDate) <> vbEmpty  And IsvalidDate(strDate)And Err.Number = 0 Then						
					'	objCalender.SetTime    strTime 
					strActdate = objWindow.GetRoProperty("date")
					If CompareActualStringValues(CDate(strDate),CDate(strActdate)) And Err.Number = 0 Then
						
						'	objCalender.SetTime   strTime 
						
						intStatus  = STATUS_PASSED
						customMsg = FormatString(MSG_OPERATION_SUCCESSFUL,Array("Verify Date","Calendar"))
						
					Else
						intStatus = STATUS_DEFECT
						customMsg = FormatString(MSG_OPERATION_UNSUCCESSFUL,Array("Verify date","Calendar"))
					End If
					
				Else
					intStatus = STATUS_FAILED
					customMsg = FormatString(MSG_ERR_INVALID_FORMAT,Array("Date"))
					
				End If 
				
			End If
			
			
			
'		Else
'			intStatus = STATUS_FAILED
'			customMsg = FormatString(MSG_OPERATION_FAILED_DISABLED,Array("verfiy Date","Calendar")) 
'			
'		End If 
		
	Else
		intStatus = STATUS_FAILED
		customMsg =    FormatString(MSG_OBJECT_NOT_EXISTS,Array("Calendar"))  
	End If
	
	
	VerifyNetCalendarDate = intStatus
	Call WriteActionStatusToLog(intStatus, customMsg)
	On Error Goto 0 
End Function



'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :		SetNetCalendarDateRange()
'	Purpose :					Sets the date range of a .NET calendar. 
'	Return Value :		 		 Integer( 0/1/2)
'	Arguments :
'		Input Arguments :  		objCalender, strDate
'------------------------------------------------------------------------------------------------------------------------------------------
Function SetNetCalendarDateRange(ByVal objCalender , strStartDate , strEndDate)
	On Error Resume Next
	'Variable Section Begin
	
	Dim intActive , strEnabled
	Dim customMsg , intStatus
	Dim strConvertedStartDate, strConvertedEndDate
	'Variable Section End
	If CheckObjectExist(objCalender) Then	
		'MsgBox objCalender.GetROProperty("enabled")
		strEnabled = objCalender.GetROProperty("enabled")' CheckObjectEnabled(objCalender) 'objCalender.GetROProperty("enabled")
	'	MsgBox Err.Description
	
		'Iohbha: To be fix. CheckObjectEnabled does not work for Calendar. Hence commented

		'If CheckObjectEnabled(objCalender) Then	
			
			
			If ObjectTypeName(objCalender) = "MonthCalendar"   Then	 
				
				If IsvalidDate(strStartDate) Then
					
					If IsvalidDate(strEndDate) Then
						strConvertedStartDate = GetDateInFormat(strStartDate)
						strConvertedEndDate = GetDateInFormat(strEndDate)
						objCalender.SetDateRange "[" & strConvertedStartDate &" - " & strConvertedEndDate &"]"
						If Err.Number = 0 Then
							intStatus  = STATUS_PASSED
							customMsg = FormatString(MSG_OPERATION_SUCCESSFUL,Array("set date range","Calendar"))
							
						Else
							intStatus = STATUS_DEFECT
							customMsg = FormatString(MSG_OPERATION_UNSUCCESSFUL,Array("set date range","Calendar"))
						End If
						
					Else
						
						intStatus = STATUS_FAILED
						customMsg =  "Invalid  Start Date value" 
						
					End If
					
				Else 
					intStatus = STATUS_FAILED
					customMsg =  "Invalid end Date value"
				End If 
				
			Else			
				
				intStatus = STATUS_FAILED
			customMsg = FormatString(MSG_ERR_INVALID_FORMAT,Array("Date"))
				
				
			End If 
			
'		Else
'			intStatus = STATUS_FAILED
'			customMsg = FormatString(MSG_OPERATION_FAILED_DISABLED,Array("Set Date range","Calendar")) 
'			'customMsg =  "Object is in disable mode, Please verify."
'		End If
	Else
		intStatus = STATUS_FAILED
		customMsg =    FormatString(MSG_OBJECT_NOT_EXISTS,Array("Calender"))  
	End If		
	SetNetCalendarDateRange = intStatus
	Call WriteActionStatusToLog(intStatus, customMsg)
	On Error Goto 0 
End Function



'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :		GetDateInFormat()
'	Purpose :				convert date in dd-MMM-yyyy format
'	Return Value :		 	date
'	Arguments :
'		Input Arguments :   sDate
'------------------------------------------------------------------------------------------------------------------------------------------

Function GetDateInFormat(ByVal sDate)
	Dim actDate, convertedDate
	
	If IsDate(sDate) then
		actDate = CDate(sDate)
		convertedDate = Day(actDate) & "-" & _ 
		        MonthName(Month(actDate),true) & _ 
		        "-" & Year(actDate) 
		      GetDateInFormat = convertedDate
	Else
        GetDateInFormat = ""
       End If
End Function

'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :		IsvalidDate()
'	Purpose :				check whether specified value is date or not
'	Return Value :		 	string(true/false)
'	Arguments :
'		Input Arguments :   strDate
'------------------------------------------------------------------------------------------------------------------------------------------

Function IsvalidDate(Byval strDate)
	On Error Resume Next	
	If IsDate(strDate) Then
		IsvalidDate = True
		'MsgBox "true"
	Else 
		IsvalidDate = False
	'	MsgBox "False"
	End If	
	
End Function


'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :		IsvalidDate()
'	Purpose :				check whether specified value is date or not
'	Return Value :		 	string(true/false)
'	Arguments :
'		Input Arguments :   strDate
'------------------------------------------------------------------------------------------------------------------------------------------


Function ObjectTypeName(byval objCal)
	On Error Resume Next
	
	If objCal.GetROProperty("swftypename")="System.Windows.Forms.DateTimePicker"  Then
		'MsgBox Err.Description
		ObjectTypeName = "DateTimePicker"
		'	MsgBox "DateTimePicker"
	Else 
		If objCal.GetROProperty("swftypename")="System.Windows.Forms.MonthCalendar"  Then	 
			ObjectTypeName = "MonthCalendar"
		'	MsgBox "MonthCalendar" 
		Else
			ObjectTypeName = objCal
		End If
	End If 
End Function

'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :		IsTime()
'	Purpose :				check whether specified value is Time or not
'	Return Value :		 	string(true/false)
'	Arguments :
'		Input Arguments :   strDate
'------------------------------------------------------------------------------------------------------------------------------------------


Function IsTime(byval strTime)
	If strTime = "" Then
		IsTime = False
	Else
		On Error Resume Next
		TimeValue(strTime)
		If Err.number = 0 Then
			IsTime = True
		Else
			IsTime = False
		End If
		On Error Goto 0
	End If
End Function
