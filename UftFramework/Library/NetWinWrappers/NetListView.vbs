
Function SelectNetListViewItemsByName(ByVal objNetListView , ByVal arrItemName)
	On Error Resume Next
		'Variable Section Begin
			Dim customMsg, intStatus
			Dim arrLen,lstItemCount,Iterator
			Dim isItemSelected 
			intStatus=STATUS_FAILED
			arrLen=UBound(arrItemName)
			
			customMsg="Method Name:" &strKeywordForReporting&vbTab
			If CheckObjectExist(objNetListView) Then
				If CheckObjectVisible(objNetListView) Then
					lstItemCount= objNetListView.GetItemsCount
					If arrLen<=lstItemCount Then			
						For Iterator = 0 To UBound(arrItemName)
								objNetListView.ExtendSelect arrItemName(Iterator)
								isItemSelected=objNetListView.GetItemProperty(arrItemName(Iterator),"selected")
								If err.number <> 0 or not isItemSelected Then
									customMsg =  customMsg&"Message:" & " Failed to select the specified Item '"& arrItemName(Iterator) &"'. "&err.description
									intStatus = STATUS_FAILED
									Exit for
								Else
									customMsg =  customMsg&vbNewLine&"Message:" & "The specified Item '"& arrItemName(Iterator) &"' is selected."
									intStatus=STATUS_PASSED
								End If								
						Next
					Else
						customMsg=customMsg&"Message:" & "The specified Items list length '"&(arrLen+1)&"' is greater then list items count '"&lstItemCount&"'"
						intStatus = STATUS_FAILED						
					End If
				Else
					customMsg=customMsg&"Message:" & "The object is not visible."
					intStatus = STATUS_FAILED
				End If
			Else
				intStatus = STATUS_FAILED
				customMsg = FormatString(MSG_OBJECT_NOT_EXISTS,Array("List View"))
			End If
			
			SelectNetListViewItemsByName=intStatus
			call WriteActionStatusToLog(intStatus, customMsg)
		On Error GoTo 0
End Function


Function SelectNetListViewItemsByIndex(ByVal objNetListView , ByVal arrItemIndex)
	On Error Resume Next
		'Variable Section Begin
			Dim customMsg, intStatus
			Dim arrElement,lstItemCount, isItemSelected
			Dim Iterator
			intStatus=STATUS_FAILED
			
			customMsg="Method Name:" &strKeywordForReporting&vbTab
			If CheckObjectExist(objNetListView) Then
				If CheckObjectVisible(objNetListView) Then
					lstItemCount= objNetListView.GetItemsCount					
						For Iterator = 0 To UBound(arrItemIndex)
							arrElement=arrItemIndex(Iterator)
							If IsPositiveInteger(arrElement) =0 Then								
								If (CInt(arrElement)-1)<=lstItemCount Then									
									objNetListView.ExtendSelect (CInt(arrElement)-1)
									isItemSelected=objNetListView.GetItemProperty((CInt(arrElement)-1),"selected")
									If err.number <> 0 or not isItemSelected Then										
										customMsg =  customMsg&"Message:" & "Failed to select the specified Item index '"& arrElement &"' "&err.description
										intStatus = STATUS_FAILED
										exit for
									Else
										customMsg =  customMsg&vbNewLine&"Message:" & "The specified Item index '"& arrElement &"' is selected."
										intStatus=STATUS_PASSED
									End If								
								Else
									customMsg=customMsg&"Message:" & "The specified index list '"&arrElement&"'length is greater then list items count '"&lstItemCount&"'"
									intStatus = STATUS_FAILED
								End If
							Else
								customMsg=customMsg&"Message:" & "The specified element '"&arrElement&"'is not numeric. Index should be greater then zero."
								intStatus = STATUS_FAILED
							End If							
						Next
				Else
					customMsg=customMsg&"Message:" & "The object is not visible"
					intStatus = STATUS_FAILED
				End If
			Else
				intStatus = STATUS_FAILED
				customMsg = FormatString(MSG_OBJECT_NOT_EXISTS,Array("List View"))
			End If			
			SelectNetListViewItemsByIndex=intStatus
			call WriteActionStatusToLog(intStatus, customMsg)
			
		On Error GoTo 0
End Function

Function SelectAllNetListViewItems(ByVal objNetListView)
	On Error Resume Next
		'Variable Section Begin
			Dim customMsg, intStatus
			Dim lstItemCount
			Dim firstItem
			firstItem=0
			intStatus=STATUS_FAILED
			'arrLen=UBound(arrItemIndex)
			
			customMsg="Method Name:" &"SelectAllNetListViewItems"&vbTab
			If CheckObjectExist(objNetListView) Then
				If CheckObjectVisible(objNetListView) Then
					lstItemCount= objNetListView.GetItemsCount
					If lstItemCount>0 Then
						objNetListView.SelectRange firstItem,(lstItemCount-1)
						If err.number <> 0 Then
							customMsg=customMsg&" Message: Error occured in select all item. "&err.description							
							intStatus=STATUS_FAILED
						Else
							customMsg=customMsg&" Message: The Expected all items are selected sucessfully."
							intStatus=STATUS_PASSED
						End If
					Else
						customMsg=customMsg&" Message: The specified list view is empty"
						intStatus=STATUS_FAILED
					End If
				Else
					customMsg=customMsg&" Message:The object is not visible."
					intStatus = STATUS_FAILED
				End If
			Else
				intStatus = STATUS_FAILED
				customMsg = FormatString(MSG_OBJECT_NOT_EXISTS,Array("List View"))
			End If
			SelectAllNetListViewItems=intStatus
			call WriteActionStatusToLog(intStatus, customMsg)
			
		On Error GoTo 0
End Function

Function DeselectNetListViewItemsByName(ByVal objNetListView, ByVal arrItemName)
	On Error Resume Next
		'Variable Section Begin
			Dim customMsg, intStatus
			Dim arrLen,lstItemCount,Iterator
			dim isItemSelected
			intStatus = STATUS_FAILED
			
			arrLen=UBound(arrItemName)
			
			customMsg="Method Name:" &strKeywordForReporting&vbTab
			If CheckObjectExist(objNetListView) Then
				If CheckObjectVisible(objNetListView) Then
					lstItemCount= objNetListView.GetItemsCount
					If arrLen<=lstItemCount Then			
						For Iterator = 0 To UBound(arrItemName)
								objNetListView.Deselect arrItemName(Iterator)
								isItemSelected=objNetListView.GetItemProperty(arrItemName(Iterator),"selected")
								If err.number <> 0 or isItemSelected Then									
									customMsg =  customMsg&"Message:" & "Failed to deselect the specified Item '"& arrItemName(Iterator) &"'"&err.description
									intStatus = STATUS_FAILED
									exit for
								Else
									customMsg =  customMsg&vbNewLine&"Message:" & "The specified Item '"& arrItemName(Iterator) &"' is deselected."
									intStatus=STATUS_PASSED
								End If								
						Next
					Else
						customMsg=customMsg&"Message:" & "The specified Items list length '"&arrLen&"'is greater then list items count '"&lstItemCount&"'"
						intStatus = STATUS_FAILED						
					End If
				Else
					customMsg=customMsg&"Message:The object is not visible."
					intStatus = STATUS_FAILED
				End If
			Else
				intStatus = STATUS_FAILED
				customMsg = FormatString(MSG_OBJECT_NOT_EXISTS,Array("List View"))
			End If
			DeselectNetListViewItemsByName=intStatus
			call WriteActionStatusToLog(intStatus, customMsg)
		On Error GoTo 0
End Function

Function DeselectNetListViewItemByIndex(ByVal objNetListView , ByVal arrItemIndex)
	On Error Resume Next
		'Variable Section Begin
			Dim customMsg, intStatus
			Dim arrElement,lstItemCount, isItemSelected
			Dim Iterator
			intStatus=STATUS_FAILED
			
			customMsg="Method Name:" &strKeywordForReporting&vbTab
			If CheckObjectExist(objNetListView) Then
				If CheckObjectVisible(objNetListView) Then
					lstItemCount= objNetListView.GetItemsCount					
						For Iterator = 0 To UBound(arrItemIndex)
							arrElement=arrItemIndex(Iterator)
							If IsPositiveInteger(arrElement)= 0 Then								
								If (CInt(arrElement)-1)<=lstItemCount Then									
									objNetListView.Deselect (CInt(arrElement)-1)
									isItemSelected=objNetListView.GetItemProperty((CInt(arrElement)-1),"selected")
									If err.number <> 0 or isItemSelected Then										
										customMsg =  customMsg&"Message:" & "Failed to deselect the specified Item '"& arrElement &"' "&err.description
										intStatus = STATUS_FAILED
										exit for
									Else
										customMsg =  customMsg&vbNewLine&"Message:" & "The specified Item '"& arrElement &"' is deselected"
										intStatus=STATUS_PASSED
									End If								
								Else
									customMsg=customMsg&"Message:" & "The specified Items list '"&arrElement&"'length is greater than list items count"
									intStatus = STATUS_FAILED
								End If
							Else
								customMsg=customMsg&"Message:" & "The specified element '"&arrElement&"'is not numeric. Index should be greater than zero."
								intStatus = STATUS_FAILED
							End If							
						Next
				Else
					customMsg=customMsg&"Message:" & "The object is not visible."
					intStatus = STATUS_FAILED
				End If
			Else
				intStatus = STATUS_FAILED
				customMsg = FormatString(MSG_OBJECT_NOT_EXISTS,Array("List View"))
			End If			
			DeselectNetListViewItemByIndex=intStatus
			call WriteActionStatusToLog(intStatus, customMsg)
			
		On Error GoTo 0
End Function

Function StoreRowNoContainingNetCellText(ByVal objNetListView , ByVal key, ByVal cellText,ByVal colNumber)
	On Error Resume Next
		'Variable Section Begin
			Dim customMsg, intStatus,intColNum
			Dim arrElement,lstItemCount, lstColCount
			Dim lstRowCount,rowNum
			Dim strCellText
			Dim Iterator
			rowNum=-1
			intStatus=STATUS_FAILED
			
			customMsg="Method Name:" &strKeywordForReporting&vbTab
			If IsPositiveInteger(colNumber) <> 0 Then
				StoreRowNoContainingNetCellText = STATUS_FAILED
				WriteStatusToLogs "The given column  Number "&colNumber&" is not an Integer, Please verify. Column number starts from 1."
				Exit Function	 
			End If
			If IsKey(key) <> 0 Then
				StoreRowNoContainingNetCellText = STATUS_FAILED
				WriteStatusToLogs "The key is invalid. Please verify."
				 Exit Function
			end if
			
			If CheckObjectExist(objNetListView) Then
				If CheckObjectVisible(objNetListView) Then					
					intColNum=CInt(colNumber)-1
					lstColCount= objNetListView.ColumnCount
					lstRowCount= objNetListView.GetItemsCount
					If lstColCount>=(CInt(colNumber)-1) Then
						For Iterator = 0 To lstRowCount Step 1
							strCellText=objNetListView.GetSubItem(Iterator,intColNum)								
							If  instr(strCellText,cellText)>0 Then
								rowNum=Iterator+1
								Exit For
							End If
						Next
						If rowNum=-1 Then							
							customMsg=customMsg&"Message:" & "The expected cell text '"&cellText&"' is not found."
							intStatus = STATUS_FAILED
						Else
							AddItemToStorageDictionary key,rowNum
							customMsg=customMsg&"Message:" & "The row number '"&rowNum&"' is stored in the key '"&key&"'"
							intStatus=STATUS_PASSED
						End If
					Else 
						customMsg=customMsg&"Message:" & "The specified column number '"&colNumber&"' is greater then actul list view column count. '"&lstColCount&"'"
						intStatus = STATUS_FAILED
					End If										
				 Else
					customMsg=customMsg&"Message:" & "The object is not visible."
					intStatus = STATUS_FAILED
				End If
			Else
				intStatus = STATUS_FAILED
				customMsg = FormatString(MSG_OBJECT_NOT_EXISTS,Array("List View"))
			End If
			
			StoreRowNoContainingNetCellText=intStatus
			call WriteActionStatusToLog(intStatus, customMsg)
			
		On Error GoTo 0
End Function


Function StoreNetListViewRowCount(ByVal objNetListView , ByVal key)
	On Error Resume Next
		'Variable Section Begin
			Dim customMsg, intStatus			
			Dim lstRowCount
			
			intStatus=STATUS_FAILED
			
			customMsg="Method Name:" &strKeywordForReporting&vbTab
			If IsKey(key) <> 0 Then
				StoreNetListViewRowCount = STATUS_FAILED
				WriteStatusToLogs "The key is invalid. Please verify."
				 Exit Function
			end if
			If CheckObjectExist(objNetListView) Then
				If CheckObjectVisible(objNetListView) Then
					lstRowCount = objNetListView.GetItemsCount
					If err.number = 0 Then
						AddItemToStorageDictionary key,lstRowCount
						customMsg=customMsg&"Message:" & "The row count '"&lstRowCount&"' is stored in the key '"&key&"'"
						intStatus=STATUS_PASSED
					Else					
						customMsg=customMsg&"Message:" & "Error occurred to fetched row count."&err.description
						intStatus = STATUS_FAILED
					End If
					
				 Else
					customMsg=customMsg&"Message:" & "The object is not visible"
					intStatus = STATUS_FAILED
				End If
			Else
				intStatus = STATUS_FAILED
				customMsg = FormatString(MSG_OBJECT_NOT_EXISTS,Array("List View"))
			End If
			
			StoreNetListViewRowCount=intStatus
			call WriteActionStatusToLog(intStatus, customMsg)
			
		On Error GoTo 0
End Function

Function StoreNetListViewColumnCount(ByVal objNetListView , ByVal key)
	On Error Resume Next
		'Variable Section Begin
			Dim customMsg, intStatus			
			Dim lstColCount
			
			intStatus=STATUS_FAILED
			
			customMsg="Method Name:" &strKeywordForReporting&vbTab
			If IsKey(key) <> 0 Then
				StoreNetListViewColumnCount = STATUS_FAILED
				WriteStatusToLogs "The key is invalid. Please verify."
				 Exit Function
			end if
			If CheckObjectExist(objNetListView) Then
				If CheckObjectVisible(objNetListView) Then
					lstColCount = objNetListView.ColumnCount
					If err.number=0 Then
						AddItemToStorageDictionary key,lstColCount
						intStatus=STATUS_PASSED
						customMsg=customMsg&"Message:" & "The column count '"&lstColCount&"' is stored in the key '"&key&"'"
					Else
						customMsg=customMsg&"Message:" & "Error occurred to fetched column count."&err.description
						intStatus = STATUS_FAILED
					End If
					
				 Else
					customMsg=customMsg&"Message:" & "The object is not visible."
					intStatus = STATUS_FAILED
				End If
			Else
				intStatus = STATUS_FAILED
				customMsg = FormatString(MSG_OBJECT_NOT_EXISTS,Array("List View"))
			End If
			
			StoreNetListViewColumnCount=intStatus
			call WriteActionStatusToLog(intStatus, customMsg)
			
		On Error GoTo 0
End Function

Function StoreNetListViewCellData(ByVal objNetListView , ByVal key, ByVal rowNumber, ByVal colNumber)
	On Error Resume Next
		'Variable Section Begin
			Dim customMsg, intStatus			
			Dim lstColCount, listColCount,itemRowColunt
			Dim expColCount,expRowCount,strCellText
			
			intStatus=STATUS_FAILED
			
			customMsg="Method Name:" &strKeywordForReporting&vbTab
			If IsKey(key) <> 0 Then
				StoreNetListViewCellData = STATUS_FAILED
				WriteStatusToLogs "The key is invalid. Please verify."
				 Exit Function
			end if
			
			If IsPositiveInteger(colNumber) <> 0 Then
				StoreNetListViewCellData = STATUS_FAILED
				WriteStatusToLogs  "The given column  number "&colNumber&" is not an Integer, Please verify. Column number starts from 1."
				Exit Function	 
			End If
			
			If IsPositiveInteger(rowNumber) <> 0 Then
				StoreNetListViewCellData = STATUS_FAILED
				WriteStatusToLogs  "The given row  number "&rowNumber&" is not an Integer, Please verify. Row number starts from 1."
				Exit Function	 
			End If
			
			If CheckObjectExist(objNetListView) Then
				If CheckObjectVisible(objNetListView) Then				
					expRowCount=CInt(rowNumber)-1
					itemRowCount=objNetListView.GetItemsCount
					If itemRowCount>=expRowCount Then						
						expColCount=CInt(colNumber)-1
						listColCount=objNetListView.ColumnCount
						If listColCount>=expColCount Then
							strCellText=objNetListView.GetSubItem(expRowCount,expColCount)	
							If err.number=0 Then
								AddItemToStorageDictionary key,strCellText
								customMsg=customMsg&"Message:" & "The cell["&rowNumber&","&colNumber&"] data '"&strCellText&"' is stored in the key '"&key&"'"
								intStatus = STATUS_PASSED
							Else
								customMsg=customMsg&"Message: Error occured while featching the cell data. Please verify. "&err.description										
								intStatus = STATUS_FAILED
							End If
						Else								 
							 customMsg=customMsg&"Message: Specified column count "&colNumber&" is greater than the actual coloumn count '"&listColCount&"'"
							 intStatus = STATUS_FAILED
						End If						
					Else
						customMsg=customMsg&"Message: Specified row count "&rowNumber&" is greater than the actual row count '"&itemRowCount&"'"
						intStatus = STATUS_FAILED
					End If
				Else
					customMsg=customMsg&"Message:" & "The object is not visible"
					intStatus = STATUS_FAILED
				End If
			Else
				intStatus = STATUS_FAILED
				customMsg = FormatString(MSG_OBJECT_NOT_EXISTS,Array("List View"))
			End If
			
			StoreNetListViewCellData=intStatus
			call WriteActionStatusToLog(intStatus, customMsg)			
	On Error GoTo 0
End Function

Function RightClickOnNetListView(ByVal objNetListView)
	On Error Resume Next
		'Variable Section Begin
			Dim customMsg, intStatus
			
			intStatus=STATUS_FAILED
			
			customMsg="Method Name:" &strKeywordForReporting&vbTab
			If CheckObjectExist(objNetListView) Then
				If CheckObjectVisible(objNetListView) Then
					objNetListView.Click micNoCoordinate, micNoCoordinate, micRightBtn
					If err.number=0 Then						
						customMsg=customMsg&"Message: RightClick performed successfully."
						intStatus=STATUS_PASSED
					Else
						customMsg=customMsg&"Message:" & "RightClick could not be performed. Please verify "&err.description
						intStatus = STATUS_FAILED
					End If
					
				 Else
					customMsg=customMsg&"Message:" & "The object is not visible"
					intStatus = STATUS_FAILED
				End If
			Else
				intStatus = STATUS_FAILED
				customMsg = FormatString(MSG_OBJECT_NOT_EXISTS,Array("List View"))
			End If
			
			RightClickOnNetListView=intStatus
			call WriteActionStatusToLog(intStatus, customMsg)
			
		On Error GoTo 0
End Function
