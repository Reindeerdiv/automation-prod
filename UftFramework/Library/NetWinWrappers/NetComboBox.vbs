

'=======================Code Section Begin=============================================

Option Explicit 'Forces explicit declaration of all variables in a script. 

'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name :				  SelectListItem()
' 	Purpose :						 Selects a specified item from the ComboBox
' 	Return Value :		 		   Integer( 0/1/2)
'		Input Arguments :  		objComboBox , strSelectItem
'------------------------------------------------------------------------------------------------------------------------------------------
Function SelectNetComboItem(ByVal objComboBox , ByVal strSelectItem)
	On Error Resume Next
		'Variable Section Begin
		
		Dim strActItem
		 Dim  customMsg , intStatus
		'Variable Section End
			If CheckObjectExist(objComboBox) Then   
			  objComboBox.Select strSelectItem
				If Err.number = 0 Then
						intStatus = STATUS_PASSED
						customMsg =   FormatString(MSG_OPERATION_SUCCESSFUL,Array("Select","Combo Box")) ' "The specified item is selected in the ComboBox." ' MSG_OPERATION_SUCCESSFUL
'						strActItem = objComboBox.GetRoProperty("text")
'						If Not IsEmpty(strActItem) Then
'								 If CompareActualStringValues(strSelectItem, strActItem) = 0 Then
'								    
'								 Else
'										intStatus = STATUS_FAILED
'										customMsg =   FormatString(MSG_OPERATION_FAILED_MISSING,Array("Select","item")) ' "The specific data could not be selected, Please verify item exists." 'MSG_OPERATION_FAILED_MISSING
'								 End If
'						Else
'							intStatus = STATUS_DEFECT
'							customMsg =   FormatString(MSG_OPERATION_FAILED_MISSING,Array("Select","item")) ' "The Combo Box item could not be selected, Please verify." 'MSG_OPERATION_FAILED_MISSING
'						End If

				Else
							intStatus = STATUS_DEFECT
							customMsg =   FormatString(MSG_OPERATION_FAILED_MISSING,Array("Select","item")) ' "The Combo Box item could not be selected, Please verify." 'MSG_OPERATION_FAILED_MISSING
								
				End if
			Else
				intStatus = STATUS_FAILED
				customMsg =      FormatString(MSG_OBJECT_NOT_EXISTS,Array("Combo Box"))     ' "The specified Combo Box does not exist, Please verify."  'MSG_OBJECT_NOT_EXISTS
			End If

		 SelectNetComboItem =    intStatus 
			 call WriteActionStatusToLog(intStatus, customMsg)
		On Error GoTo 0
End Function
'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name :				 VerifyAllListItems()
' 	Purpose :					 Verifies  all the list items with our expected
' 	Return Value :		 		   Integer( 0/1/2)
'		Input Arguments :  	  	objComboBox,strExpItems
'------------------------------------------------------------------------------------------------------------------------------------------
Function VerifyAllNetComboItems(ByVal objComboBox , ByVal strExpItems)
	On Error Resume Next
		'Variable Section Begin
		
			Dim strActItems , arrActItems , arrExpItems
			Dim customMsg , intStatus
		'Variable Section End
			If CheckObjectExist(objComboBox)  Then   
				strActItems = objComboBox.GetROProperty("all items")
				If Err.number = 0 then
				
					If Not IsEmpty(strActItems) Then
					   arrActItems = GetArray(strActItems , chr(10))
					   'arrExpItems = GetArray(strExpItems , "^")
					   arrExpItems = strExpItems
					
						If MatchEachArrDataCS(arrExpItems, arrActItems) = True Then
							intStatus = STATUS_PASSED
							customMsg = FormatString(MSG_ITEMS_MATCHES,Array("Combo Box items", "Combo Box items")) '  " The specified list items matches with the expected list item."  'MSG_ITEMS_MATCHES
								
						Else
							intStatus = STATUS_DEFECT
					 		customMsg =  FormatString(MSG_ITEMS_NOT_MATCHES,Array("Combo Box items", "Combo Box items"))  ' "The specified list items does not match the expected list item." 'MSG_ITEMS_NOT_MATCHES
													
						End IF
					Else
						intStatus = STATUS_FAILED
							 customMsg =  FormatString(MSG_ERR_EMPTY,Array("Combo Box")) '"The Combo Box does not contain any item, Please Verify."   'MSG_ERR_EMPTY
					End If
				Else
					intStatus = STATUS_FAILED
					customMsg = FormatString(MSG_ERR_FETCH_DATA , Array("Combo Box items")) '"Could not retrieve all the items of the ComboBox" 'MSG_ERR_FETCH_DATA
				End if
			Else
			      intStatus = STATUS_FAILED
                 customMsg =   FormatString(MSG_OBJECT_NOT_EXISTS,Array("Combo Box"))  ' "The Combo Box does not exist, Please verify."
			End If
			VerifyAllNetComboItems = intStatus
				 call WriteActionStatusToLog(intStatus, customMsg)
    On Error GoTo 0
End Function
'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name :				 VerifyComboItemsInSequence()
' 	Purpose :					 Compare the lComboBox items with our expected items in sequence order.
' 	Return Value :		 		 Integer( 0/1/2)
'		Input Arguments :  		objComboBox , strExpItems
'------------------------------------------------------------------------------------------------------------------------------------------
Function VerifyNetComboItemsInSequence(ByVal objComboBox , ByVal strExpItems)
	On Error Resume Next
	'Variable Section Begin
        Dim strActItems , arrActItems , arrExpItems
  		Dim customMsg , intStatus
		'Variable Section End
	If CheckObjectExist(objComboBox) Then   
			strActItems = objComboBox.GetROProperty("all items")
			
			If Not IsEmpty(strActItems) Then
				arrActItems = GetArray(strActItems , chr(10))
				'arrExpItems = GetArray(strExpItems , "^")
				arrExpItems = strExpItems

				If CompareArraysInSequenceCS(arrExpItems , arrActItems) = 0 Then
					intStatus = STATUS_PASSED
					customMsg =     FormatString(MSG_ITEMS_MATCHES,Array("Combo Box items","Combo Box items"))                      '"The Combo Box items sequence matches with the expected list sequence."  'MSG_ITEMS_MATCHES
				Else
					intStatus =STATUS_DEFECT
				   customMsg =  FormatString(MSG_ITEMS_NOT_MATCHES,Array("Combo Box items","Combo Box items"))   '   "The Combo Box items are NOT in sequence with the expected list." 'MSG_ITEMS_NOT_MATCHES
				End IF

			Else
				 intStatus = STATUS_FAILED
				  customMsg =  FormatString(MSG_ERR_EMPTY,Array("Combo Box"))  ' "The Combo Box does NOT contain any item, Please Verify." 'MSG_ERR_EMPTY
			End If
	Else
		intStatus = STATUS_FAILED
		 customMsg =     FormatString(MSG_OBJECT_NOT_EXISTS,Array("Combo Box")) ' " The Combo Box does not exist, Please verify."
	End If
  VerifyNetComboItemsInSequence =   intStatus
	call WriteActionStatusToLog(intStatus, customMsg)
 On Error GoTo 0
End Function


'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name :			   VerifyNoDuplicationInComboBox()
' 	Purpose :					  Verifies the duplicate items in the ComboBox
' 	Return Value :		 		Integer( 0/1/2)
'		Input Arguments :  	 objComboBox
'------------------------------------------------------------------------------------------------------------------------------------------
Function VerifyNoDuplicationInNetComboBox(ByVal objComboBox)
	On Error Resume Next
		'Variable Section Begin
		
			Dim strActItems , arrActItems 
					   Dim customMsg , intStatus
         'Variable Section End
			If CheckObjectExist(objComboBox)  Then   
				strActItems = objComboBox.GetROProperty("all items")
                
					If Not IsEmpty(strActItems) Then
						arrActItems = GetArray(strActItems , chr(10))
						If Not IsNull(arrActItems) Then

								If VerifyDuplicateElementsInArrayCS(arrActItems) = 0 Then
									intStatus = STATUS_PASSED
                                    customMsg =   FormatString(MSG_NO_DUPLICATES,Array("Combo Box" , "items")) '"The Combo Box has no duplicate item."	'MSG_NO_DUPLICATES
								Else
									intStatus = STATUS_DEFECT
   								 customMsg =   FormatString(MSG_DUPLICATES,Array("Combo Box" , "items")) ' "The Combo Box contains duplicate item(s), Please verify." 'MSG_DUPLICATES
								End If

						 Else
								intStatus = STATUS_FAILED
								customMsg =   FormatString(MSG_ERR_FETCH_DATA,Array("Combo Box items"))   '"An error occurred; Failed to retrieve items from Combo Box, Please verify." 'MSG_ERR_FETCH_DATA
						 End If
					
					Else
						intStatus = STATUS_FAILED
					   	customMsg =   FormatString(MSG_ERR_FETCH_DATA,Array("Combo Box items"))  ' "An error occurred; Failed to retrieve items from Combo Box, Please verify." 'MSG_ERR_FETCH_DATA
					
					End if
			Else
				intStatus = STATUS_FAILED
	          customMsg =   FormatString(MSG_OBJECT_NOT_EXISTS,Array("Combo Box"))  '"The specified Combo Box does not exist, Please Verify."
            End If
		VerifyNoDuplicationInNetComboBox =	intStatus
		 	call WriteActionStatusToLog( VerifyNoDuplicationInComboBox, customMsg)
	On Error GoTo 0
End Function
'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name :			   SelectNetComboItemByIndex()
' 	Purpose :					  To Select the List box item by index
' 	Return Value :		 		Integer( 0/1/2)
'		Input Arguments :  	 intIndex - Index starting from 1.
'------------------------------------------------------------------------------------------------------------------------------------------
Function SelectNetComboItemByIndex(objComboBox, byval intIndex)
	
	On Error Resume Next
		'Variable Section Begin
		
	  Dim   intGetIndex 
	   Dim customMsg , intStatus 
         'Variable Section End
	If CheckObjectExist(objComboBox)  Then  
		intGetIndex = CInt(intIndex) - 1
		 If Err.Number = 0 Then
			objComboBox.Select intGetIndex 
			If Err.Number = 0 Then
				intStatus=  STATUS_PASSED
			  customMsg =   FormatString(MSG_OPERATION_SUCCESSFUL ,Array("Select","Combo Box")) ' "The item with index "& Cstr("#"&intGetIndex)&" is selected."	'MSG_OPERATION_SUCCESSFUL
			Else
			intStatus= STATUS_FAILED
			 customMsg =     FormatString(MSG_OUT_OF_RANGE ,Array("index"))          ' "The specified Index Number is out of range in the specified Combo Box items count, Please Verify." 'MSG_OUT_OF_RANGE
			End if
		Else
			intStatus=  STATUS_FAILED
			customMsg =  FormatString(MSG_ERR_NOT_NUMERIC ,Array("index"))     '  "Error in selecting Combo Box Item; the specified index is not a numeric value, Please Verify."  'MSG_ERR_NOT_NUMERIC
		End If
	Else
	    intStatus  = STATUS_FAILED
		customMsg =    FormatString(MSG_OBJECT_NOT_EXISTS,Array("Combo Box"))   ' "The specified Combo Box does not exist, Please verify."
	End if
	SelectNetComboItemByIndex = intStatus
  call WriteActionStatusToLog( intStatus, customMsg)
	
   On Error GOTO 0
End Function


'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name :				 VerifyComboBoxNotContainsItem()
' 	Purpose :					 Verifies whether the ComboBox does not contain the specified Item.
' 	Return Value :		 		   Integer( 0/1/2)
'		Input Arguments :  	  	objComboBox,strExpItems
'------------------------------------------------------------------------------------------------------------------------------------------
Function VerifyNetComboBoxNotContainsItem(ByVal objComboBox , ByVal strListItem)
	On Error Resume Next
		'Variable Section Begin
		
			Dim strActItems , arrActItems , arrExpItems,strExpItem,intActItem, blnEqualFlag, blnstatus
			blnEqualFlag=true
         Dim customMsg		, intStatus 
		'Variable Section End
			If CheckObjectExist(objComboBox)  Then   
				strActItems = objComboBox.GetROProperty("all items")
				
                If Err.Number = 0 Then
						
											
								arrActItems = GetArray(strActItems , chr(10))
							If Vartype(arrActItems) <> vbEmpty  And Err.Number = 0 Then
									For intActItem = 0 to Ubound(arrActItems)
										   If CompareActualStringValues(strListItem, arrActItems(intActItem)) = 0 Then
												blnEqualFlag=false
												Exit for
										   End If
                                     Next
														 
									 If blnEqualFlag = true  Then
										  intStatus = STATUS_PASSED
										customMsg =    FormatString(MSG_NOT_CONTAINS_SPECIFIC_PASS,Array("Combo Box","item")) '	"The Combo Box does not contain the specified item." 'MSG_NOT_CONTAINS_SPECIFIC_PASS
		  
										Else
											 intStatus = STATUS_DEFECT
											customMsg =   FormatString(MSG_CONTAINS_SPECIFIC_FAIL, Array("Combo Box","item")) ' "The Combo Box contains the specified item, Please verify." 'MSG_CONTAINS_SPECIFIC_FAIL
									End If
												   
									 
							Else
								 intStatus = STATUS_FAILED
                        customMsg =  FormatString(MSG_ERR_FETCH_DATA,Array("Combo Box items"))   ' "An error occurred; Failed to retrieve Combo Box items, Please verify." 'MSG_ERR_FETCH_DATA
						 End If
						
				Else
						intStatus = STATUS_FAILED
                     customMsg =  FormatString(MSG_ERR_FETCH_DATA,Array("Combo Box items"))   ' "An error occurred; Failed to retrieve Combo Box items, Please verify." 'MSG_ERR_FETCH_DATA
				End If
			
	 Else
			intStatus = STATUS_FAILED
          customMsg =   FormatString(MSG_OBJECT_NOT_EXISTS,Array("Combo Box"))  '"The specified Combo Box does not exist, Please Verify."

	 End If
VerifyNetComboBoxNotContainsItem = intStatus
 call WriteActionStatusToLog( VerifyComboBoxNotContainsItem, customMsg)
 
    On Error GoTo 0
End Function

'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name :				 VerifyComboBoxContainsItem		()
' 	Purpose :					 Verifies  whether the ComboBox contains the specified Item.
' 	Return Value :		 		   Integer( 0/1/2)
'------------------------------------------------------------------------------------------------------------------------------------------
	Function VerifyNetComboBoxContainsItem(ByVal objComboBox , ByVal strListItem)
		On Error Resume Next
			'Variable Section Begin
				Dim strActItems , arrActItems , arrExpItems,intCount,strExpItem,intActItem , blnpresentFlag, blnstatus
				blnpresentFlag=false
			  Dim customMsg	  , intStatus  	
			'Variable Section End
				If CheckObjectExist(objComboBox)  Then   
					strActItems = objComboBox.GetROProperty("all items")
					
					 If Vartype(strActItems) <> vbEmpty  And Err.Number = 0 Then
							arrActItems = GetArray(strActItems , chr(10))				  
												  For intActItem = 0 to Ubound(arrActItems)
												   If CompareActualStringValues(strListItem, arrActItems(intActItem)) = 0 Then
														blnpresentFlag=True
														Exit for
												   End If
												 Next
												 
												If blnpresentFlag = True  Then
														  intStatus	 = STATUS_PASSED
														customMsg =  FormatString(MSG_CONTAINS_SPECIFIC_PASS,Array("Combo Box", "item")) ' "The Combo Box contains the expected item." 'MSG_CONTAINS_SPECIFIC_PASS
													  Else
															 intStatus	 = STATUS_DEFECT
															customMsg =  FormatString(MSG_NOT_CONTAINS_SPECIFIC_FAIL, Array("Combo Box", "item")) ' "The Combo Box does not contain the expected item, Please verify." 'MSG_NOT_CONTAINS_SPECIFIC_FAIL
																				
												End If
								Else
									intStatus	 = STATUS_FAILED
									customMsg = FormatString(MSG_ERR_FETCH_DATA,Array("Combo Box items"))   ' "An error occurred; Failed to retrieve Combo Box items, Please verify." 'MSG_ERR_FETCH_DATA
							   End If
		 Else
					 intStatus	 = STATUS_FAILED
					  customMsg =     FormatString(MSG_OBJECT_NOT_EXISTS,Array("Combo Box")) ' "The specified Combo Box does not exist, Please Verify."
		 End If
		 VerifyNetComboBoxContainsItem = intStatus
			  call WriteActionStatusToLog( intStatus, customMsg)
		On Error GoTo 0
	End Function
	
'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name :				 VerifyComboBoxSize()
' 	Purpose :					 Verifies the size(no.of items) of  ComboBox
' 	Return Value :		 		   Integer( 0/1/2)
'		Input Arguments :  	  	objListBox,intItemCount
'------------------------------------------------------------------------------------------------------------------------------------------
Function VerifyNetComboBoxSize(ByVal objListBox , ByVal intExpectedSize)
	On Error Resume Next
		'Variable Section Begin
			Dim intActCount,intCount
			    Dim customMsg	  , intStatus  	
		'Variable Section End
			If CheckObjectExist(objListBox)  Then   
				intCount=Cint(intExpectedSize)
				
				If VarType(intCount) = VbInteger Then
				
				intActCount = objListBox.GetROProperty("items count")
					If Not IsEmpty(intActCount)  Then
							If CompareActualStringValues(intCount,intActCount)=0 Then
								  intStatus = STATUS_PASSED
									customMsg=     FormatString(MSG_ITEMS_MATCHES,Array("Combo Box size","Combo Box size" )) ' "The specified Combo Box Size matches the expected size." 'MSG_ITEMS_MATCHES
							Else
								   intStatus = STATUS_DEFECT
									customMsg=  FormatString(MSG_ITEMS_NOT_MATCHES,Array("Combo Box size","Combo Box size" ))  ' "The specified Combo Box Size does not match the expected Size, Please verify." 'MSG_ITEMS_NOT_MATCHES
						End If
					Else
							  intStatus =  STATUS_FAILED
                              customMsg=  FormatString(MSG_ERR_FETCH_DATA,Array("Combo Box items"))  '"An error occurred; Failed to fetch the Combo Box items count, Please verify." 'MSG_ERR_FETCH_DATA
				End If
		Else

		   intStatus =  STATUS_FAILED
           customMsg=   FormatString(MSG_ERR_NOT_NUMERIC,Array("Combo Box size")) '"An error occurred; The specified expected size is not a numeric value, Please verify." 'MSG_ERR_NOT_NUMERIC
		End If
		Else
			 intStatus =  STATUS_FAILED
			customMsg=     FormatString(MSG_OBJECT_NOT_EXISTS,Array("Combo Box")) ' "The specified Combo Box does not exist, Please Verify."
	 End If
	VerifyNetComboBoxSize =  intStatus
		  call WriteActionStatusToLog( intStatus, customMsg)
    On Error GoTo 0
End Function


'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name :				 VerifyNetComboSelectedText()
' 	Purpose :					 Verifies  selected text same as expected of  ComboBox
' 	Return Value :		 		   Integer( 0/1/2)
'		Input Arguments :  	  	objListBox,intItemCount
'------------------------------------------------------------------------------------------------------------------------------------------

Function VerifyNetComboSelectedText(ByVal objComboBox , ByVal strText)
	On Error Resume Next
		'Variable Section Begin
		
		Dim strActItem
		 Dim  customMsg , intStatus
		'Variable Section End
		    If CheckObjectExist(objComboBox) Then   
                    strActItem = objComboBox.GetRoProperty("text")
						If Not IsEmpty(strActItem) Then
								 If CompareActualStringValues(strText, strActItem) = 0 Then
								    intStatus = STATUS_PASSED
								  customMsg=     FormatString(MSG_ITEMS_MATCHES,Array("Combo Box item","Combo Box item" ))  
								 Else
										intStatus = STATUS_FAILED
										customMsg =  FormatString(MSG_ITEMS_NOT_MATCHES,Array("Combo Box item","Combo Box item" ))
								 End If
						Else
							intStatus = STATUS_DEFECT
						   customMsg=  FormatString(MSG_ERR_FETCH_DATA,Array("Combo Box items")) 
						End If
      Else
				intStatus = STATUS_FAILED
				customMsg =      FormatString(MSG_OBJECT_NOT_EXISTS,Array("Combo Box"))     ' "The specified Combo Box does not exist, Please verify."  'MSG_OBJECT_NOT_EXISTS
			End If

		 VerifyNetComboSelectedText =    intStatus 
		 call WriteActionStatusToLog(intStatus, customMsg)
		On Error GoTo 0
End Function

'=======================Code Section End=============================================
