'=======================Code Section Begin============================================

Option Explicit		 'Forces explicit declaration of all variables in a script. 

'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :			  PageUp()
'	Purpose :				  		Performs  Page up operation on a specified scroll bar.
'	Return Value :		 	  	Integer( 0/1)
'	Input Arguments :  	 	objScroll,intPages
'------------------------------------------------------------------------------------------------------------------------------------------
Function PageUp(byval objScroll,byval intPages)
On Error Resume Next
	Dim actualtCount
	Dim blnstatus,statusTest
	Dim customMsg 
	blnstatus=False
	If CheckObjectExist(objScroll) Then
		If CheckObjectEnabled(objScroll) Then
		
			intPages=CInt(intPages)
			If VarType(intPages)=vbInteger And Err.number=0 and intPages>=0 Then
				blnstatus = True
			End If
			If blnstatus = True Then
			   If IsVertical(objScroll)  Then
						objScroll.PrevPage  intPages
						If err.number=0 Then
							statusTest=STATUS_PASSED
							customMsg = FormatString(MSG_OPERATION_SUCCESSFUL, Array("PageUp","ScrollBar")) 
						else
							statusTest=STATUS_DEFECT
							customMsg = FormatString(MSG_OPERATION_UNSUCCESSFUL, Array("PageUp","ScrollBar")) 
						End If
			   else
					statusTest=STATUS_FAILED
					customMsg = FormatString(MSG_INVALID_ACTION, Array("horizontal ScrollBar")) 
			   End If
			Else    
				statusTest=STATUS_FAILED
				customMsg = FormatString(MSG_ERR_NOT_NUMERIC, Array("number of pages"))
			End If
		Else
			 statusTest=STATUS_FAILED
			customMsg = FormatString(MSG_OPERATION_FAILED_DISABLED, Array("PageUp","ScrollBar"))
		End If
	Else
		statusTest=STATUS_FAILED
		customMsg = FormatString(MSG_OBJECT_NOT_EXISTS, Array("ScrollBar"))
	End If
	PageUp=statusTest
	Call writeActionStatusToLog(statusTest,customMsg)
	'msgbox statusTest&" "&customMsg&" "&err.description
	On Error Goto 0
End Function

'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :			  ScrollUp()
'	Purpose :				  		Performs  Scroll up operation on a specified scroll bar.
'	Return Value :		 	  	Integer( 0/1)
'	Input Arguments :  	 	objScroll,intLines
'------------------------------------------------------------------------------------------------------------------------------------------
Function ScrollUp(byval objScroll,byval intLines)
On Error Resume Next
	Dim actualtCount
	Dim blnstatus,statusTest
	Dim customMsg 
	blnstatus=False
	If CheckObjectExist(objScroll) Then
		If CheckObjectEnabled(objScroll) Then
		
		intLines=CInt(intLines)
		If VarType(intLines)=vbInteger And Err.number=0 and intLines>=0 Then
			blnstatus = True
		End If
		If blnstatus = True Then
		   If IsVertical(objScroll)  Then
					objScroll.PrevLine  intLines
					If err.number=0 Then
						statusTest=STATUS_PASSED
						customMsg = FormatString(MSG_OPERATION_SUCCESSFUL, Array("ScrollUp","ScrollBar")) 
					else
						statusTest=STATUS_DEFECT
						customMsg = FormatString(MSG_OPERATION_UNSUCCESSFUL, Array("ScrollUp","ScrollBar")) 
					End If
		   else
				statusTest=STATUS_FAILED
				customMsg = FormatString(MSG_INVALID_ACTION, Array("horizontal ScrollBar"))
		   End If
		Else    
			statusTest=STATUS_FAILED
			customMsg = FormatString(MSG_ERR_NOT_NUMERIC, Array("number of lines"))
		End If
		else
			 statusTest=STATUS_FAILED
			customMsg = FormatString(MSG_OPERATION_FAILED_DISABLED, Array("ScrollUp","ScrollBar"))
		End If
	Else
		statusTest=STATUS_FAILED
		customMsg = FormatString(MSG_OBJECT_NOT_EXISTS, Array("ScrollBar"))
	End If
	ScrollUp=statusTest
	Call writeActionStatusToLog(statusTest,customMsg)
	'msgbox statusTest&" "&customMsg&" "&err.description
	On Error Goto 0
End Function
'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :			  ScrollDown()
'	Purpose :				  		Performs  Scroll down operation on a specified scroll bar.
'	Return Value :		 	  	Integer( 0/1)
'	Input Arguments :  	 	objScroll,intLines
'------------------------------------------------------------------------------------------------------------------------------------------
Function ScrollDown(byval objScroll,byval intLines)
	On Error Resume Next
	Dim actualtCount
	Dim blnstatus,statusTest
	Dim customMsg 
	blnstatus=False
	If CheckObjectExist(objScroll) Then
		If CheckObjectEnabled(objScroll) Then
		
		intLines=CInt(intLines)
		If VarType(intLines)=vbInteger And Err.number=0 and intLines>=0 Then
			blnstatus = True
		End If
		If blnstatus = True Then
		   If IsVertical(objScroll)  Then
					objScroll.NextLine intLines
					If err.number=0 Then
						statusTest=STATUS_PASSED
						customMsg = FormatString(MSG_OPERATION_SUCCESSFUL, Array("ScrollDown","ScrollBar")) 
					else
						statusTest=STATUS_DEFECT
						customMsg = FormatString(MSG_OPERATION_UNSUCCESSFUL, Array("ScrollDown","ScrollBar")) 
					End If
		   else
				statusTest=STATUS_FAILED
				customMsg = FormatString(MSG_INVALID_ACTION, Array("horizontal ScrollBar"))
		   End If
		Else    
			statusTest=STATUS_FAILED
			customMsg = FormatString(MSG_ERR_NOT_NUMERIC, Array("number of lines"))
		End If
		else
			 statusTest=STATUS_FAILED
			customMsg = FormatString(MSG_OPERATION_FAILED_DISABLED, Array("ScrollDown","ScrollBar"))
		End If
	Else
		statusTest=STATUS_FAILED
		customMsg = FormatString(MSG_OBJECT_NOT_EXISTS, Array("ScrollBar"))
	End If
	ScrollDown=statusTest
	Call writeActionStatusToLog(statusTest,customMsg)
	'msgbox statusTest&" "&customMsg&" "&err.description
	On Error Goto 0
End Function
'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :			  PageDown()
'	Purpose :				  		Performs  Page down operation on a specified scroll bar.
'	Return Value :		 	  	Integer( 0/1)
'	Input Arguments :  	 	objScroll,intPages
'------------------------------------------------------------------------------------------------------------------------------------------
Function PageDown(Byval objScroll,byval intPages)
   On Error Resume Next
	Dim actualtCount
	Dim blnstatus,statusTest
	Dim customMsg 
	blnstatus=False
	If CheckObjectExist(objScroll) Then
		If CheckObjectEnabled(objScroll) Then
		
		intPages=CInt(intPages)
		If VarType(intPages)=vbInteger And Err.number=0 and intPages>=0 Then
			blnstatus = True
		End If
		If blnstatus = True Then
		   If IsVertical(objScroll)  Then
					objScroll.NextPage  intPages
					If err.number=0 Then
						statusTest=STATUS_PASSED
						customMsg = FormatString(MSG_OPERATION_SUCCESSFUL, Array("PageDown","ScrollBar")) 
					else
						statusTest=STATUS_DEFECT
						customMsg = FormatString(MSG_OPERATION_UNSUCCESSFUL, Array("PageDown","ScrollBar")) 
					End If
			Else
				statusTest=STATUS_FAILED
				customMsg = FormatString(MSG_INVALID_ACTION, Array("horizontal ScrollBar"))
		   End If
		Else    
			statusTest=STATUS_FAILED
			customMsg = FormatString(MSG_ERR_NOT_NUMERIC, Array("number of pages"))
		End If
		else
			 statusTest=STATUS_FAILED
			customMsg = FormatString(MSG_OPERATION_FAILED_DISABLED, Array("PageDown","ScrollBar"))
		End If
	Else
		statusTest=STATUS_FAILED
		customMsg = FormatString(MSG_OBJECT_NOT_EXISTS, Array("ScrollBar"))
	End If
	PageDown=statusTest
	Call writeActionStatusToLog(statusTest,customMsg)
	'msgbox statusTest&" "&customMsg&" "&err.description
	On Error Goto 0
End Function

'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :			  ScrollRight()
'	Purpose :				  		Performs  Page scroll to right side operation on a specified scroll bar.
'	Return Value :		 	  	Integer( 0/1)
'	Input Arguments :  	 	objScroll,intLines
'------------------------------------------------------------------------------------------------------------------------------------------
Function ScrollRight(Byval objScroll,byval intLines)
   On Error Resume Next
	Dim actualtCount
	Dim blnstatus,statusTest
	Dim customMsg 
	blnstatus=False
	If CheckObjectExist(objScroll) Then
		If CheckObjectEnabled(objScroll) Then
		
		intLines=CInt(intLines)
		If VarType(intLines)=vbInteger And Err.number=0 and intLines>=0 Then
			blnstatus = True
		End If
		If blnstatus = True Then
		   If IsHorizontal(objScroll)  Then
					objScroll.NextLine  intLines
					If err.number=0 Then
						statusTest=STATUS_PASSED
						customMsg = FormatString(MSG_OPERATION_SUCCESSFUL, Array("ScrollRight","ScrollBar")) 
					else
						statusTest=STATUS_DEFECT
						customMsg = FormatString(MSG_OPERATION_UNSUCCESSFUL, Array("ScrollRight","ScrollBar")) 
					End If
		   else
				statusTest=STATUS_FAILED
				customMsg = FormatString(MSG_INVALID_ACTION, Array("vertical ScrollBar"))
		   End If
		Else    
			statusTest=STATUS_FAILED
			customMsg = FormatString(MSG_ERR_NOT_NUMERIC, Array("number of lines"))
		End If
		else
			 statusTest=STATUS_FAILED
			customMsg = FormatString(MSG_OPERATION_FAILED_DISABLED, Array("ScrollRight","ScrollBar"))
		End If
	Else
		statusTest=STATUS_FAILED
		customMsg = FormatString(MSG_OBJECT_NOT_EXISTS, Array("ScrollBar"))
	End If
	ScrollRight=statusTest
	Call writeActionStatusToLog(statusTest,customMsg)
	'msgbox statusTest&" "&customMsg&" "&err.description
	On Error Goto 0
End Function

'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :			  ScrollLeft()
'	Purpose :				  		Performs  Page scroll to left side operation on a specified scroll bar.
'	Return Value :		 	  	Integer( 0/1)
'	Input Arguments :  	 	objScroll,intLines
'------------------------------------------------------------------------------------------------------------------------------------------
Function ScrollLeft(byval objScroll,byval intLines)
On Error Resume Next
	Dim actualtCount
	Dim blnstatus,statusTest
	Dim customMsg 
	blnstatus=False
	If CheckObjectExist(objScroll) Then
		If CheckObjectEnabled(objScroll) Then
		
		intLines=CInt(intLines)
		If VarType(intLines)=vbInteger And Err.number=0 and intLines>=0 Then
			blnstatus = True
		End If
		If blnstatus = True Then
		   If IsHorizontal(objScroll)  Then
					objScroll.PrevLine  intLines
					If err.number=0 Then
						statusTest=STATUS_PASSED
						customMsg = FormatString(MSG_OPERATION_SUCCESSFUL, Array("ScrollLeft","ScrollBar")) 
					else
						statusTest=STATUS_DEFECT
						customMsg = FormatString(MSG_OPERATION_UNSUCCESSFUL, Array("ScrollLeft","ScrollBar")) 
					End If
		   else
				statusTest=STATUS_FAILED
				customMsg = FormatString(MSG_INVALID_ACTION, Array("vertical ScrollBar"))
		   End If
		Else    
			statusTest=STATUS_FAILED
			customMsg = FormatString(MSG_ERR_NOT_NUMERIC, Array("number of lines"))
		End If
		else
			 statusTest=STATUS_FAILED
			customMsg = FormatString(MSG_OPERATION_FAILED_DISABLED, Array("ScrollLeft","ScrollBar"))
		End If
	Else
		statusTest=STATUS_FAILED
		customMsg = FormatString(MSG_OBJECT_NOT_EXISTS, Array("ScrollBar"))
	End If
	ScrollLeft=statusTest
	Call writeActionStatusToLog(statusTest,customMsg)
	'msgbox statusTest&" "&customMsg&" "&err.description
	On Error Goto 0
End Function

'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :			  PageRight()
'	Purpose :				  		Performs  Page scroll to right side operation on a specified scroll bar.
'	Return Value :		 	  	Integer( 0/1)
'	Input Arguments :  	 	objScroll,intPages
'------------------------------------------------------------------------------------------------------------------------------------------
Function PageRight(Byval objScroll,byval intPages)
   On Error Resume Next
	Dim actualtCount
	Dim blnstatus,statusTest
	Dim customMsg 
	blnstatus=False
	If CheckObjectExist(objScroll) Then
		If CheckObjectEnabled(objScroll) Then
		
		intPages=CInt(intPages)
		If VarType(intPages)=vbInteger And Err.number=0 and intPages>=0 Then
			blnstatus = True
		End If
		If blnstatus = True Then
		   If IsHorizontal(objScroll)  Then
					objScroll.NextPage  intPages
					If err.number=0 Then
						statusTest=STATUS_PASSED
						customMsg = FormatString(MSG_OPERATION_SUCCESSFUL, Array("PageRight","ScrollBar")) 
					else
						statusTest=STATUS_DEFECT
						customMsg = FormatString(MSG_OPERATION_UNSUCCESSFUL, Array("PageRight","ScrollBar")) 
					End If
		   else
				statusTest=STATUS_FAILED
				customMsg = FormatString(MSG_INVALID_ACTION, Array("vertical ScrollBar"))
		   End If
		Else    
			statusTest=STATUS_FAILED
			customMsg = FormatString(MSG_ERR_NOT_NUMERIC, Array("number of pages"))
		End If
		else
			 statusTest=STATUS_FAILED
			customMsg = FormatString(MSG_OPERATION_FAILED_DISABLED, Array("PageRight","ScrollBar"))
		End If
	Else
		statusTest=STATUS_FAILED
		customMsg = FormatString(MSG_OBJECT_NOT_EXISTS, Array("ScrollBar"))
	End If
	PageRight=statusTest
	Call writeActionStatusToLog(statusTest,customMsg)
	'msgbox statusTest&" "&customMsg&" "&err.description
	On Error Goto 0
End Function

'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :			  PageLeft()
'	Purpose :				  		Performs  Page scroll to left side operation on a specified scroll bar.
'	Return Value :		 	  	Integer( 0/1)
'	Input Arguments :  	 	objScroll,intPages
'------------------------------------------------------------------------------------------------------------------------------------------
Function PageLeft(byval objScroll,byval intPages)
On Error Resume Next
	Dim actualtCount
	Dim blnstatus,statusTest
	Dim customMsg 
	blnstatus=False
	If CheckObjectExist(objScroll) Then
		If CheckObjectEnabled(objScroll) Then
		
		intPages=CInt(intPages)
		If VarType(intPages)=vbInteger And Err.number=0 and intPages>=0 Then
			blnstatus = True
		End If
		If blnstatus = True Then
		   If IsHorizontal(objScroll)  Then
					objScroll.PrevPage  intPages
					If err.number=0 Then
						statusTest=STATUS_PASSED
						customMsg = FormatString(MSG_OPERATION_SUCCESSFUL, Array("PageLeft","ScrollBar")) 
					else
						statusTest=STATUS_DEFECT
						customMsg = FormatString(MSG_OPERATION_UNSUCCESSFUL, Array("PageLeft","ScrollBar")) 
					End If
		   else
				statusTest=STATUS_FAILED
				customMsg = FormatString(MSG_INVALID_ACTION, Array("vertical ScrollBar"))
		   End If
		Else    
			statusTest=STATUS_FAILED
			customMsg = FormatString(MSG_ERR_NOT_NUMERIC, Array("number of pages"))
		End If
		else
			 statusTest=STATUS_FAILED
			customMsg = FormatString(MSG_OPERATION_FAILED_DISABLED, Array("PageLeft","ScrollBar"))
		End If
	Else
		statusTest=STATUS_FAILED
		customMsg = FormatString(MSG_OBJECT_NOT_EXISTS, Array("ScrollBar"))
	End If
	PageLeft=statusTest
	Call writeActionStatusToLog(statusTest,customMsg)
	'msgbox statusTest&" "&customMsg&" "&err.description
	On Error Goto 0
End Function
'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :			  IsVertical()
'	Purpose :				  		Verify scroll bar is Vertical.
'	Return Value :		 	  	Bool( True/False)
'	Input Arguments :  	 	objScroll
'------------------------------------------------------------------------------------------------------------------------------------------
Function IsVertical(byval objScroll)
   If objScroll.GetROProperty("swftypename")="System.Windows.Forms.VScrollBar"  Then
		IsVertical=true
  else
		IsVertical=false
   End If
End Function
'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :			  IsHorizontal()
'	Purpose :				  		Verify scroll bar is Horizontal.
'	Return Value :		 	  	Bool( True/False)
'	Input Arguments :  	 	objScroll
'------------------------------------------------------------------------------------------------------------------------------------------
Function IsHorizontal(byval objScroll)
   If objScroll.GetROProperty("swftypename")="System.Windows.Forms.HScrollBar"  Then
		IsHorizontal=true
  else
		IsHorizontal=false
   End If
End Function
'=======================Code Section End=============================================