'=======================Code Section Begin============================================

Option Explicit		 'Forces explicit declaration of all variables in a script. 

'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :			  ClickNetLabel()
'	Purpose :				  		Performs  Click operation on a specified Label.
'	Return Value :		 	  	Integer( 0/1/2)
'	Input Arguments :  	 	objLabel
'------------------------------------------------------------------------------------------------------------------------------------------
Function ClickNetLabel(ByVal objLabel)
	On Error Resume Next
	'Variable Section Begin	
	Dim  customMsg  ,objectName
	Dim intStatus
	'Variable Section End
	
	If CheckObjectExist(objLabel) Then 
		If CheckObjectEnabled(objLabel) Then
			If CheckObjectClick(objLabel) Then
				intStatus = STATUS_PASSED
				customMsg = FormatString(MSG_OPERATION_SUCCESSFUL,Array("Click","Label"))
			Else
				intStatus = STATUS_DEFECT
				customMsg = FormatString(MSG_OPERATION_UNSUCCESSFUL,Array("Click","Label"))
			End If 
		Else
			
			intStatus = STATUS_DEFECT
			customMsg = FormatString(MSG_OPERATION_FAILED_DISABLED,Array("Click","Label"))
		End If
	Else              
		intStatus = STATUS_FAILED
		customMsg = FormatString(MSG_OBJECT_NOT_EXISTS,Array("Label"))
	End If
	ClickNetLabel = intStatus
	Call WriteActionStatusToLog(intStatus, customMsg)
	On Error Goto 0
End Function

'------------------------------------------------------------------------------------------------------------------------------------------
' 	Function Name :				 VerifyNetLabelValue()
' 	Purpose :						Compares the actual Label Name with the expected Name .
' 	Return Value :		 		  Integer(0/1/2)
'	Input Arguments :  			objLabel , strExpName
'-----------------------------------------------------------------------------------------------------------------------------------------
Function VerifyNetLabelValue(ByVal objLabel , ByVal strExpName)
	On Error Resume Next
	'Variable Section Begin
	Dim strActName,  customMsg, intStatus
	'Variable Section End
	If CheckObjectExist(objLabel) Then         
		strActName = objLabel.GetRoProperty("text")
		If VarType(strActName) <> vbEmpty  And Err.Number = 0 Then
			If CompareActualStringValues(strExpName,strActName) = 0  Then
				intStatus = STATUS_PASSED
				customMsg = FormatString(MSG_VALUE_MATCHES,Array(strExpName,strActName))				
			Else
				
				intStatus = STATUS_DEFECT
				customMsg = FormatString(MSG_VALUE_NOT_MATCHES,Array(strExpName,strActName))		
				
			End If
		Else
			intStatus = STATUS_FAILED
			customMsg =FormatString(MSG_ERR_FETCH_DATA ,Array("Label value")) 
		End If
		
	Else
		intStatus = STATUS_FAILED
		customMsg =  FormatString(MSG_OBJECT_NOT_EXISTS,Array("Label"))   
	End If
	VerifyNetLabelValue = intStatus
	Call WriteActionStatusToLog(intStatus, customMsg)
	
	On Error Goto 0
End Function

'=======================Code Section End=============================================
