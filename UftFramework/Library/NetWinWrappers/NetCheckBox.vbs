
'Set objCheckBox   = SwfWindow("Z-TAF Automation - Administrat").SwfWindow("Projects").SwfCheckBox("chkIsActive")
'Call VerifyNetCheckBoxChecked(objCheckBox)
'========================Code Section Begin=====================================================================
'Explicit	'Forces explicit declaration of all variables in a script.
'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :			DeselectNetCheckBox()
'	Purpose :				Activates the Specified .Net Application.	
'	Return Value :		 	Integer(0/1)
'	Arguments :
'		Input Arguments :   objCheckBox   
'		Output Arguments :  Status
'	Function is called by : 
'	Function calls :		CheckObjectExist() or CheckObjectEnabled()
'	Created on : 			07/09/09
Function DeselectNetCheckBox(byval objCheckBox)
    On Error Resume Next
	Dim statusTest
    Dim customMsg
   If CheckObjectExist(objCheckBox)Then
		If CheckObjectEnabled(objCheckBox) Then
			   objCheckBox.Set "OFF"
			   If err.number=0 Then
									DeselectNetCheckBox=STATUS_PASSED
									statusTest=STATUS_PASSED
									'customMsg="The CheckBox is de-selected sucessfully.."
									customMsg=FormatString(MSG_OPERATION_SUCCESSFUL, Array("Clear", "Check Box"))
			  else
									DeselectNetCheckBox=STATUS_DEFECT
									statusTest=STATUS_DEFECT
									customMsg=FormatString(MSG_OPERATION_UNSUCCESSFUL, Array("Clear", "Check Box"))
			   End If
		else
				DeselectNetCheckBox = STATUS_FAILED
				statusTest=STATUS_FAILED
				'customMsg="The CheckBox  was not enabled, Please verify."
				customMsg = FormatString(MSG_OPERATION_FAILED_DISABLED, Array("Clear", "Check Box"))
		End If
   else
				DeselectNetCheckBox = STATUS_FAILED
				statusTest=STATUS_FAILED
				'customMsg="The specified CheckBox does not exist, please verify."
				customMsg = FormatString(MSG_OBJECT_NOT_EXISTS, Array("Check Box"))

   End If
 Call writeActionStatusToLog(statusTest,customMsg)
 On Error GoTo 0
End Function

'	Function Name :			SelectNetCheckBox()
'	Purpose :				Activates the Specified .Net Application.	
'	Return Value :		 	Integer(0/1)
'	Arguments :
'		Input Arguments :   objCheckBox   
'		Output Arguments :  Status
'	Function is called by : 
'	Function calls :		CheckObjectExist() or  CheckObjectEnabled() 
'	Created on : 			07/09/09
Function SelectNetCheckBox(byval objCheckBox)
    On Error Resume Next
	
	Dim statusTest, intActValue
    Dim customMsg
   If CheckObjectExist(objCheckBox)Then
		If CheckObjectEnabled(objCheckBox) Then
            intActValue = objCheckBox.GetROProperty("checked")
            	If intActValue=0 Then
						objCheckBox.Set "ON"
							If err.number=0 Then
									SelectNetCheckBox=STATUS_PASSED
									statusTest=STATUS_PASSED
									'customMsg="The CheckBox is selected sucessfully."
									customMsg=FormatString(MSG_OPERATION_SUCCESSFUL, Array("Select", "Check Box"))
							else
									SelectNetCheckBox=STATUS_DEFECT
									statusTest=STATUS_DEFECT
									'customMsg="The CheckBox is not selected sucessfully, Please verify."
									customMsg=FormatString(MSG_OPERATION_UNSUCCESSFUL, Array("Select", "Check Box"))
							End If
				else
									SelectNetCheckBox=STATUS_PASSED
									statusTest=STATUS_PASSED
									customMsg=FormatString(MSG_OPERATION_SUCCESSFUL, Array("Select", "Check Box"))
				End If
			   
		else
				SelectNetCheckBox = STATUS_FAILED
				statusTest=STATUS_FAILED
				customMsg = FormatString(MSG_OPERATION_FAILED_DISABLED, Array("Select", "Check Box"))
				'customMsg="The CheckBox  was not enabled, Please verify."
		End If
   else
				SelectNetCheckBox = STATUS_FAILED
				statusTest=STATUS_FAILED
				customMsg = FormatString(MSG_OBJECT_NOT_EXISTS, Array("Check Box"))
   End If
 Call writeActionStatusToLog(statusTest,customMsg)
  On Error GoTo 0
End Function

'	Function Name :			VerifyNetCheckBoxChecked()
'	Purpose :				Activates the Specified .Net Application.	
'	Return Value :		 	Integer(0/1)
'	Arguments :
'		Input Arguments :   objCheckBox   
'		Output Arguments :  Status
'	Function is called by : 
'	Function calls :		CheckObjectExist() or  CheckObjectEnabled() 
'	Created on : 			07/09/09
Function VerifyNetCheckBoxChecked(byval objCheckBox)
    On Error Resume Next
	
	Dim statusTest, intActValue
    Dim customMsg
   If CheckObjectExist(objCheckBox)Then
		If CheckObjectEnabled(objCheckBox) Then
            intActValue = objCheckBox.GetROProperty("checked")
            	If intActValue=true Then
								VerifyNetCheckBoxChecked=STATUS_PASSED
									statusTest=STATUS_PASSED
									'customMsg="Specified CheckBox is checked as expected."
									customMsg=FormatString(MSG_OPERATION_SUCCESSFUL, Array("Verify Checked", "Check Box"))
				else
									VerifyNetCheckBoxChecked=STATUS_DEFECT
									statusTest=STATUS_DEFECT
									'customMsg="Specified CheckBox is not checked, Please verify."
									customMsg=FormatString(MSG_OPERATION_UNSUCCESSFUL, Array("Verify Checked", "Check Box"))
				End If
			   
		else
				VerifyNetCheckBoxChecked = STATUS_FAILED
				statusTest=STATUS_FAILED
				'customMsg="The CheckBox  was not enabled, Please verify."
				customMsg = FormatString(MSG_OPERATION_FAILED_DISABLED, Array("Verify Checked", "Check Box"))
		End If
   else
				VerifyNetCheckBoxChecked = STATUS_FAILED
				statusTest=STATUS_FAILED
				customMsg = FormatString(MSG_OBJECT_NOT_EXISTS, Array("Check Box"))
   End If
  Call writeActionStatusToLog(statusTest,customMsg)
  On Error GoTo 0
End Function


'	Function Name :			VerifyNetCheckBoxUnChecked()
'	Purpose :				Activates the Specified .Net Application.	
'	Return Value :		 	Integer(0/1)
'	Arguments :
'		Input Arguments :   objCheckBox   
'		Output Arguments :  Status
'	Function is called by : 
'	Function calls :		CheckObjectExist() or  CheckObjectEnabled() 
'	Created on : 			07/09/09
Function VerifyNetCheckBoxUnChecked(byval objCheckBox)
    On Error Resume Next
	
	Dim statusTest, intActValue
    Dim customMsg
   If CheckObjectExist(objCheckBox)Then
		If CheckObjectEnabled(objCheckBox) Then
            intActValue = objCheckBox.GetROProperty("checked")
            	If intActValue=false Then
								VerifyNetCheckBoxUnChecked=STATUS_PASSED
									statusTest=STATUS_PASSED
									'customMsg="Specified CheckBox is checked as expected."
								customMsg=FormatString(MSG_OPERATION_SUCCESSFUL, Array("Verify Unchecked", "Check Box"))
				else
									VerifyNetCheckBoxUnChecked=STATUS_DEFECT
									statusTest=STATUS_DEFECT
									'customMsg="Specified CheckBox is not checked, Please verify."
													customMsg=FormatString(MSG_OPERATION_UNSUCCESSFUL, Array("Verify Unchecked", "Check Box"))
				End If
			   
		else
				VerifyNetCheckBoxUnChecked = STATUS_FAILED
				statusTest=STATUS_FAILED
				'customMsg="The CheckBox  was not enabled, Please verify."
				customMsg = FormatString(MSG_OPERATION_FAILED_DISABLED, Array("Verify Unchecked", "Check Box"))
		End If
   else
				VerifyNetCheckBoxUnChecked = STATUS_FAILED
				statusTest=STATUS_FAILED
				'customMsg="The specified CheckBox does not exist, please verify."
				customMsg = FormatString(MSG_OBJECT_NOT_EXISTS, Array("Check Box"))
   End If
 Call writeActionStatusToLog(statusTest,customMsg)
  On Error GoTo 0
End Function





