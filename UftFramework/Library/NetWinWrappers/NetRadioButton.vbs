'Set objCheckBox=SwfWindow("Test Development - TestWindow").SwfWindow("QTP Automation - Application").SwfRadioButton("Public Task")

'Call SelectNetRadaioButton(objCheckBox)

'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :			SelectRedaioButton()
'	Purpose :				Activates the Specified .Net Application.	
'	Return Value :		 	Integer(0/1)
'	Arguments :
'		Input Arguments :   objCheckBox   
'		Output Arguments :  Status
'	Function is called by : 
'	Function calls :		CheckObjectExist() or CheckObjectEnabled()
'	Created on : 			07/09/09
Function SelectNetRadaioButton(byval objCheckBox)
    On Error Resume Next
	Dim statusTest,intVarient
    Dim customMsg
   If CheckObjectExist(objCheckBox)Then
		If CheckObjectEnabled(objCheckBox) Then
				intVarient=objCheckBox.GetROProperty("Checked")
				If intVarient=false Then
						objCheckBox.Set
						If err.number=0 Then
									SelectNetRadaioButton=STATUS_PASSED
									statusTest=STATUS_PASSED
									'customMsg="Expected RadioButton Selected."
									customMsg=FormatString(MSG_OPERATION_SUCCESSFUL, Array("Select", "Radio Button"))
						else
									SelectNetRadaioButton=STATUS_DEFECT
									statusTest=STATUS_DEFECT
									'customMsg="RadioButton not Selected, Please Verify."
									customMsg=FormatString(MSG_OPERATION_UNSUCCESSFUL, Array("Select", "Radio Button"))
						End If
				else
								    SelectNetRadaioButton=STATUS_PASSED
									statusTest=STATUS_PASSED
									'customMsg="Expected RadioButton Selected."
									customMsg=FormatString(MSG_OPERATION_SUCCESSFUL, Array("Select", "Radio Button"))
				End If
			   
		else
				SelectNetRadaioButton = STATUS_FAILED
				statusTest=STATUS_FAILED
				customMsg = FormatString(MSG_OPERATION_FAILED_DISABLED, Array("Select", "Radio Button"))
		End If
   else
				SelectNetRadaioButton = STATUS_FAILED
				statusTest=STATUS_FAILED
				'customMsg="The specified RadioButton does not exist, please verify."
				customMsg = FormatString(MSG_OBJECT_NOT_EXISTS, Array("Radio Button"))

   End If
 Call writeActionStatusToLog(statusTest,customMsg)
 'msgbox statusTest,customMsg
 On Error GoTo 0
End Function
'------------------------------------------------------------------------------------------------------------------------------------------
'	Function Name :			VerifyRadioButtonSelected()
'	Purpose :				Activates the Specified .Net Application.	
'	Return Value :		 	Integer(0/1)
'	Arguments :
'		Input Arguments :   objCheckBox   
'		Output Arguments :  Status
'	Function is called by : 
'	Function calls :		CheckObjectExist() or CheckObjectEnabled()
'	Created on : 			07/09/09
Function VerifyNetRadioButtonSelected(byval objRadioButton)
   On Error Resume Next
	Dim statusTest,intVarient
    Dim customMsg
	If CheckObjectExist(objRadioButton)Then
			'If CheckObjectEnabled(objRadioButton)Then
					intVarient=objRadioButton.GetROProperty("Checked")
					If intVarient=true Then
									VerifyNetRadioButtonSelected=STATUS_PASSED
									statusTest=STATUS_PASSED
									'customMsg="Expected RadioButton Selected."
									customMsg=FormatString(MSG_OPERATION_SUCCESSFUL, Array("Verify select", "Radio Button"))
					else
									VerifyNetRadioButtonSelected=STATUS_DEFECT
									statusTest=STATUS_DEFECT
									'customMsg="RadioButton not Selected, Please Verify."
									customMsg=FormatString(MSG_OPERATION_UNSUCCESSFUL, Array("Verify select", "Radio Button"))
					End If
			'else
'						VerifyNetRadioButtonSelected = STATUS_FAILED
'						statusTest=STATUS_FAILED
'						customMsg = FormatString(MSG_OPERATION_FAILED_DISABLED, Array("Select", "RadioButton"))
			'End If
	else
				VerifyNetRadioButtonSelected = STATUS_FAILED
				statusTest=STATUS_FAILED
				'customMsg="The specified RadioButton does not exist, please verify."
				customMsg = FormatString(MSG_OBJECT_NOT_EXISTS, Array("Radio Button"))
	End If
	Call writeActionStatusToLog(statusTest,customMsg)
	 On Error GoTo 0
End Function










