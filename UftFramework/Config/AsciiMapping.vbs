Function GetKeyCode(KeyName)
	Dim keycode
	select case KeyName
	   case "Tab" keycode = 15
	   case "Enter" keycode = 28
	   case "Left_Alt" keycode = 56
	   case "F1" keycode = 59
	   case "F2" keycode = 60
	   case "F3" keycode = 61
	   case "F4" keycode = 62
	   case "F5" keycode = 63
	   case "F6" keycode = 64
	   case "F7" keycode = 65
	   case "F8" keycode = 66
	   case "F9" keycode = 67
	   case "F10" keycode = 68
	   case "F11" keycode = 69
	   case "F12" keycode = 70
	   case "Space" keycode = 57
	   case "CapsLock" keycode = 58
	   case "Delete" keycode = 211
	   case "Down" keycode = 208
	   case "End" keycode = 207
	   case "Home" keycode = 199
	   case "Page_Down" keycode = 209
	   case "Page_Up" keycode = 201	 
	   case "Shift" keycode = 54	
	   case "Right" keycode = 205	
	   case "Left" keycode = 203	
	   case "Up" keycode = 200	
	   case else  keycode = "-1"
	end select 

	GetKeyCode = keycode
End Function