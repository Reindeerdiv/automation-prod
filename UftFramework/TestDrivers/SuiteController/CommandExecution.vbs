Dim FSO
Dim keywordId,keywordName,qFunctionname,toolFunctionName,objectInfo,strDataTypeParam,params
Dim oDictExecutionCollection
Set oDictExecutionCollection = CreateObject("Scripting.Dictionary")

Function ExecuteCommand()
logs.Debug("Calling ExecuteCommand()") 
call LoadTSR(ORpath)

logs.Debug("Connecting java service::"&Port)
dotNetCom.connectClient Port,SYNCCMDTIME
If Err.Number <> 0 Then
 logs.Error("Error in connecting service::"&err.description)
End If

Set executeStatment = GetRef("executeUFTCommand")
Set oCallKeyword = GetRef("GetKeywordDetails")
Set oCallObject = GetRef("GetObjectDetails")
Set oCallParamDataType = GetRef("GetParamDataType")
Set oCallTestData = GetRef("GetTestData")
Set oCloseUFT=GetRef("CloseUFT")

dotNetCom.setUFTScript(executeStatment)
dotNetCom.setObjectFunction(oCallObject)
dotNetCom.setKeywordFunction(oCallKeyword)
dotNetCom.setParamDataType(oCallParamDataType)
dotNetCom.setTestData(oCallTestData)
dotNetCom.setCloseUFT(oCloseUFT)

logs.Debug("Sent UFT launched message to java")
dotNetCom.start()

End Function

Function CloseUFT()	
	dotNetCom=nothing
End Function

Function GetKeywordDetails(ByVal ukeywordId,ByVal ukeywordName,ByVal uqFunctionname,ByVal utoolFunctionName)
	keywordId=ukeywordId
	keywordName=ukeywordName
	qFunctionname=uqFunctionname
	toolFunctionName=utoolFunctionName
End Function

Function GetObjectDetails(ByVal uobjectInfo)
	objectInfo=uobjectInfo
End Function

Function GetParamDataType(ByVal ustrDataTypeParam)
	strDataTypeParam=ustrDataTypeParam
End Function

Function GetTestData(ByVal uparams)
	If uparams <> "" then
	 	BuildParamCollection keywordId,strDataTypeParam 
		params=ProcessOnParam(uparams)
		logs.debug("Params ::"&params)
	End If
End Function


Function ProcessOnParam(ByVal uparams)
	Dim arrParam,paramSeq
	Dim arrKeywordSignature
	Dim strExecutionString,strFuncSeperator,strSignature,paramslen
	Dim arrsParameters(0)
	paramslen=Len(uparams)
	
	If uparams <> "" and paramslen > 2 Then
		uparams=Mid(uparams,2,(paramslen-2))		
	End If
	
	arrParam=Split(uparams,"&ps;")	
	paramSeq=0
	arrKeywordSignature=GetKeywordSignature(keywordId)
	strExecutionString = ""
	strFuncSeperator = ","
	
	
	for each strParam in arrParam
		strSignature = Trim(UCase(arrKeywordSignature(paramSeq)))		
		
		paramSeq=paramSeq+1
		
	
		If strSignature = Trim(UCase(STRINGDATATYPE)) Then
			'strParam=ProcessDataSeperator(strParam)
			strExecutionString = strExecutionString & strParam &strFuncSeperator
		ElseIf strSignature = Trim(UCase(ARRAYDATATYPE)) Then
			strParam=replace(strParam,"&as;","^")
			strParam=replace(strParam,Chr(34),"")			
			strParam = ProcessDataSeperator(strParam)			
			If err.number <> 0 Then
				logs.Debug("Error::"&err.description)
			End If
			
			If IsArray(strParam) Then
				If  oDictExecutionCollection.exists(paramSeq) Then
					oDictExecutionCollection.item(paramSeq) = strParam
				Else
					oDictExecutionCollection.add paramSeq,strParam
				End If
				strExecutionString = strExecutionString & "oDictExecutionCollection.item("&paramSeq&")" &strFuncSeperator
			ElseIf Vartype(strParam) = vbString Then
				arrsParameters(0) = sParameters
				If  oDictExecutionCollection.exists(paramSeq) Then
					oDictExecutionCollection.item(paramSeq) = arrsParameters
				Else
					oDictExecutionCollection.add paramSeq,arrsParameters
				End If
				strExecutionString = strExecutionString & "oDictExecutionCollection.item("&paramSeq&")" &strFuncSeperator
			End If
		 ElseIf strSignature = Trim(Ucase(KEYDATATYPE)) Then
		 	strExecutionString = strExecutionString & strParam &strFuncSeperator
		 ElseIf strSignature = Trim(Ucase(BOOLEANDATATYPE)) Then
		 	strExecutionString = strExecutionString & strParam &strFuncSeperator
		 ElseIf strSignature = Trim(Ucase(OBJECTDATATYPE)) Then
		 	strExecutionString = strExecutionString & strParam &strFuncSeperator
		 ElseIf strSignature = Trim(Ucase(INTEGERDATATYPE)) Then
		 	strExecutionString = strExecutionString & strParam &strFuncSeperator
		End If
	next
	strExecutionString=Left(strExecutionString , Len(strExecutionString) - 1)
	logs.debug("ProcessOnParam string ::"&strExecutionString)
	ProcessOnParam=strExecutionString
End Function


Function GetKeywordSignature(ByVal strKeywordID)
	Dim strKey , arrKwdsignature
	Dim arrSignature() , intCnt
	Dim oDictKeywordParamInnerDict
	intCnt = 0
	If oDictKeywordParamCollection.Exists(strKeywordID) Then
		Set oDictKeywordParamInnerDict = oDictKeywordParamCollection.item(strKeywordID)
	Else
		Set oDictKeywordParamInnerDict = null
	End If

	If Not IsNull(oDictKeywordParamInnerDict) Then
	
		arrKwdsignature =  oDictKeywordParamInnerDict.keys
					
		For Each strKey In arrKwdsignature
			ReDim Preserve arrSignature(intCnt)
			arrSignature(intCnt) = oDictKeywordParamCollection.item(Trim(strKeywordID)).item(CInt(strKey))
			intCnt = intCnt + 1
		Next
			GetKeywordSignature = arrSignature
		Else
			GetKeywordSignature = null
			
	End If
	If Err.number = 0 then
		GetKeywordSignature = arrSignature
	Else
	GetKeywordSignature = null
	End If	
		
End Function

Function ProcessDataSeperator(byval strData)
	'On Error Resume Next
	logs.debug("Calling ProcessDataSeperator() "&strData)
	Dim strLen , charTemp , strTempData , strMidData
	Dim intCnt , intArrCnt , blnArray , intIdx
	Dim arrGenerated()
	Dim strUltimate
	intArrCnt = 0
	strTempData = ""
	blnArray = false
	intCnt = 1
	If Isnull(strData) or IsEmpty(strData) or strData="" Then
			ProcessDataSeperator = strData				
	Else
			strLen = Len(strData)
			If strLen > 0 Then
					While intCnt <= strLen and intCnt >0
							charTemp = Mid(strData , intCnt ,1 )
							If  charTemp = StrEscapeChar Then
								strTempData = strTempData&charTemp
								charTemp = Mid(strData , intCnt + 1  ,1 )
								strTempData = strTempData&charTemp			
								intCnt = intCnt +2							
							ElseIf  charTemp =  strDataSeparator Then
								ReDim Preserve arrGenerated(intArrCnt)
								arrGenerated(intArrCnt) = strTempData
								strTempData = ""
								intArrCnt = intArrCnt +1
								strData = Right (strData , strlen - intCnt )
								strLen = Len( strData )
								If strLen > 0 Then
									intCnt =1
								End if
								blnArray = true
							ElseIf charTemp = Chr(34) Then ' Handle " in string
								strTempData = strTempData & Chr(34) & Chr(34)
								intCnt = intCnt + 1
							Else 
								strTempData = strTempData&charTemp
								intCnt = intCnt +1
							End If
					Wend
					
					If blnArray = true  Then
							ReDim Preserve arrGenerated(intArrCnt)
							arrGenerated(intArrCnt) = strTempData
							For intIdx = 0 to Ubound(arrGenerated)
									arrGenerated(intIdx) =  RemoveEscape(arrGenerated(intIdx))		
							Next
							logs.debug("Size of array ::"&UBound(arrGenerated))
							
                            ProcessDataSeperator = arrGenerated
							
					Else
							ProcessDataSeperator = RemoveEscape(strTempData)
					End If
			Else
				ProcessDataSeperator = strData				
			End If
	End If
	
	'On Error GoTo 0
End Function

Function RemoveEscape(byval strData)
	
	Dim strLenOfData , intCounter , charTemp , strNextChar
	Dim strFinalData

	strFinalData = ""
	strLenOfData = Len(strData)
	If strLenOfData > 0  Then
		For intCounter = 1 to strLenOfData 
		  
			charTemp = Mid(strData , intCounter ,1 )
			If  Trim(charTemp) = Trim(StrEscapeChar) Then
				strNextChar = mid(strData , intCounter+1 , 1)
				strFinalData = strFinalData&strNextChar
				intCounter = intCounter + 1
			Else
				strFinalData = strFinalData&charTemp
			End If
			
		Next
	End if
   RemoveEscape = strFinalData   
End Function
'callMeBackWithAParameter "WpfWindow(""Micro Focus MyFlight Sample"").WpfEdit(""agentName"")","c2e424cfc75c48a58f8139654fcda1c6","john"


Function executeUFTCommand()

On Error Resume Next
	 Dim intres, msg
	 Dim sStrExecObj
	 Dim sStrGetexecutingString,blnflagObject
	 Dim isObjDeletedFromTSR
	 blnflagObject = false
	 logs.Debug("Calling executeUFTCommand")
	 If objectInfo <> "" Then
		logs.Debug("Object details ::"&objectInfo)
		WriteToDebugLog objectInfo
		Set sStrExecObj=eval(objectInfo)
		If err.number<>0 Then
			logs.Error("Object error::"&err.description)	
		End If
		isObjDeletedFromTSR=CheckObjectExistEngine(sStrExecObj,qFunctionname)
		If isObjDeletedFromTSR Then
				if CBool(ElementHighlight)=true then
					eval(sStrExecObj&".Highlight")
				end if 
		End If
	
		blnflagObject=true
		
	 Else
		Set sStrExecObj=null
	 End If 
 
 'BuildParamCollection keywordId,strDataTypeParam 
strKeywordForReporting=keywordName
If qFunctionname= "ExecuteVBSStatement" Then 
 	params= Replace(params,"""","")
 	If isObjDeletedFromTSR Then
 		sStrGetexecutingString = qFunctionname &"(sStrExecObj, " & DoubleQuotes(keywordName) & "," & DoubleQuotes(toolFunctionName) & "," & DoubleQuotes(keywordId) & "," & DoubleQuotes(params) &")"
 		logs.Debug("Execution string ::"&sStrGetexecutingString)
		intres = eval(sStrGetexecutingString)
	else
		intres=1
 	End If
 	
 ElseIf qFunctionname= "ExecuteGenericActions" Then
 	params= Replace(params,"""","")
 	sStrGetexecutingString = qFunctionname &"(" & DoubleQuotes(keywordName) & "," & DoubleQuotes(toolFunctionName) & "," & DoubleQuotes(keywordId) & "," & DoubleQuotes(params) &")"
 	intres = eval(sStrGetexecutingString)
 Else
 	If blnflagObject = True Then
 		If isObjDeletedFromTSR Then
	 		If params<> "" Then
	 			sStrGetexecutingString = qFunctionname &"(sStrExecObj, " & params &")"
	 		else
	 			sStrGetexecutingString = qFunctionname &"(sStrExecObj)"
	 		End If
	 		logs.Debug("Execution string ::"&sStrGetexecutingString)
	 		intres = eval(sStrGetexecutingString)
	 	else
	 		intres=1
 		End If
 		
 	Else
 		sStrGetexecutingString = qFunctionname&"( " & params &" )"
 		logs.Debug("Execution string ::"&sStrGetexecutingString)
 		intres = eval(sStrGetexecutingString)
 	End If
 End If
  	
	logs.Debug("Execution status ::"&intres)
	If Isempty(intres) Then
		intres=1
	End If
	If err.number <> 0 Then
		intres =1
		WriteStatusToLogs "Action '"& keywordName &"' failed."&err.number&"<br> Error ::" & MSG_GENERIC_FAIL 
	End If
 
 call clearAllData()
 dotNetCom.sendData CInt(intres) 
 
 dotNetCom.ClearLogs
 On Error goto 0	  
End Function

sub clearAllData
	keywordId=""
	keywordName=""
	qFunctionname=""
	toolFunctionName=""
	objectInfo=""
	strDataTypeParam=""
	params=""
	oDictExecutionCollection.removeall
End Sub


Function LoadTSR(FullTSRPath)

	On error resume next
	Dim fso 
	
	err.clear
	   
	Set fso = CreateObject("Scripting.FileSystemObject")

	If (fso.FileExists(FullTSRPath)) Then
		logs.Debug("Calling LoadTSR() >>"+FullTSRPath)
		RepositoriesCollection.Add FullTSRPath
	End If
End Function

Function BuildParamCollection(byval Id,byval strParam)
	logs.Debug("Calling BuildParamCollection() "&Id&" Params::"&strParam)
	Dim arrParam, innerDict, oDictInnerMandatory,arrParamDet,param
	If Not oDictKeywordParamCollection.Exists(Id) Then			
		Dim paramSeq
		paramSeq=1
		If (Not IsNull(strParam)) and strParam <> "" then
			arrParam=Split(strParam,"?")
			Set innerDict = CreateObject("Scripting.Dictionary")
			Set oDictInnerMandatory = CreateObject("Scripting.Dictionary")			
			
			for each param in arrParam
				If param <> "" Then
					arrParamDet=Split(param,";")
					innerDict.Add paramSeq, arrParamDet(0)
					oDictInnerMandatory.Add paramSeq, arrParamDet(1)			
			    	paramSeq=Cint(paramSeq)+1
				End If			
			next
			oDictKeywordParamCollection.Add Id, innerDict
			oDictKeywordMandatoryDetails.Add Id,oDictInnerMandatory
		End If
	End If
	logs.Debug("End BuildParamCollection()")
End Function
